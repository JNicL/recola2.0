//****************************************************************************//
//                                                                            //
//   recola.hpp                                                               //
//   is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2       //
//                                                                            //
//   Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and             //
//                           Sandro Uccirati                                  //
//                                                                            //
//   C++ interface developed by Benedikt Biedermann and Mathieu Pellen        //
//                                                                            //
//   RECOLA2 is licenced under the GNU GPL version 3,                         //
//   see COPYING for details.                                                 //
//                                                                            //
//****************************************************************************//

// Macro to add name-mangling bits to fortran symbols.

// Define GNU_NAME_MANGLING(INTEL_NAME_MANGLING) if Recola has been build with
// gfortran(ifort)

#if defined(GNU_NAME_MANGLING)
// gfortran
//#pragma message ("Name mangling according to GNU." )
#define MODFUNCNAME(mod,fname) __ ## mod ## _MOD_ ## fname
#elif defined(INTEL_NAME_MANGLING)
// ifort
//#pragma message ("Name mangling according to INTEL." )
#define MODFUNCNAME(mod,fname) mod ## _mp_ ## fname ## _
#else
#warning "Name mangling not set. Trying GNU."
#define MODFUNCNAME(mod,fname) __ ## mod ## _MOD_ ## fname
#endif // if __GNUC__

#define RCLMOD(mod,fname) MODFUNCNAME(mod ## _rcl, fname ## _rcl)

#ifndef RECOLA_HPP_
#define RECOLA_HPP_

#include <complex>
struct dcomplex{double dr,di;};

#define ivoid inline void

#ifdef __cplusplus
extern "C" {
#endif
  // module input_rcl
  void RCLMOD(input,set_qcd_rescaling)
       (const int*);
  void RCLMOD(input,set_fermionloop_optimization)
       (const int*);
  void RCLMOD(input,set_helicity_optimization)
       (const int*);
  void RCLMOD(input,set_colour_optimization)
       (const int*);
  void RCLMOD(input,set_compute_A12)
       (const int*);
  void RCLMOD(input,set_masscut)
       (const double*);
  void RCLMOD(input,set_light_particle)
       (const char*,long int);
  void RCLMOD(input,unset_light_particle)
       (const char*,long int);
  void RCLMOD(input,use_dim_reg_soft)
       ();
  void RCLMOD(input,use_mass_reg_soft)
       (const double*);
  void RCLMOD(input,use_recola_base)
       (const int*);
  void RCLMOD(input,set_mass_reg_soft)
       (const double*);
  void RCLMOD(input,set_delta_uv)
       (const double*);
  void RCLMOD(input,get_delta_uv)
       (const double*);
  void RCLMOD(input,set_delta_ir)
       (const double*, const double*);
  void RCLMOD(input,get_delta_ir)
       (const double*, const double*);
  void RCLMOD(input,set_mu_uv)
       (const double*);
  void RCLMOD(input,get_mu_uv)
       (const double*);
  void RCLMOD(input,set_mu_ir)
       (const double*);
  void RCLMOD(input,get_mu_ir)
       (const double*);
  void RCLMOD(input,set_mu_ms)
       (const double*);
  void RCLMOD(input,get_mu_ms)
       (const double*);
  void RCLMOD(input,get_renormalization_scale)
       (double*);
  void RCLMOD(input,get_flavour_scheme)
       (int*);
  void RCLMOD(input,set_complex_mass_scheme)
       ();
  void RCLMOD(input,set_on_shell_scheme)
       ();
  void RCLMOD(input,set_momenta_correction)
       (const int*);
  void RCLMOD(input,set_print_level_amplitude)
       (const int*);
  void RCLMOD(input,set_print_level_squared_amplitude)
       (const int*);
  void RCLMOD(input,set_print_level_correlations)
       (const int*);
  void RCLMOD(input,set_print_level_parameters)
       (const int*);
  void RCLMOD(input,set_draw_level_branches)
       (const int*);
  void RCLMOD(input,set_print_level_ram)
       (const int*);
  void RCLMOD(input,scale_coupling3)
       (dcomplex*,const char*,const char*, const char*,
        long int,long int,long int);
  void RCLMOD(input,scale_coupling4)
       (dcomplex*, const char*, const char*, const char*,const char*,
        long int,long int,long int,long int);
  //void RCLMOD(input,switchoff_coupling3)
  //     (const char*,const char*,const char*,long int,long int,long int);
  //void RCLMOD(input,switchoff_coupling4)
  //     (const char*,const char*,const char*,const char*,
  //      long int,long int,long int,long int);
  void RCLMOD(input,switchon_resonant_selfenergies)
       ();
  void RCLMOD(input,switchoff_resonant_selfenergies)
       ();
  void RCLMOD(input,set_dynamic_settings)
       (const int*);
  void RCLMOD(input,set_ifail)
       (const int*);
  void RCLMOD(input,get_ifail)
       (int*);
  void RCLMOD(input,set_iwarn)
       (const int*);
  void RCLMOD(input,get_iwarn)
       (int*);
  void RCLMOD(input,set_collier_mode)
       (const int*);
  void RCLMOD(input,set_collier_output_dir)
       (const char*,long int);
  void RCLMOD(input,set_cache_mode)
       (const int*);
  void RCLMOD(input,set_global_cache)
       (const int*);
  void RCLMOD(input,set_compute_ir_poles)
       (const int*);
  void RCLMOD(input,switch_global_cache)
       (const int*);
  void RCLMOD(input,set_output_file)
       (const char*,long int);
  void RCLMOD(input,set_log_mem)
       (const int*);
  void RCLMOD(input,set_crossing_symmetry)
       (const int*);
  void RCLMOD(input,set_init_collier)
       (const int*);
  void RCLMOD(input,set_parameter)
       (const char*, dcomplex*, long int);
  void RCLMOD(input,get_parameter)
       (const char*, dcomplex*, long int);
  void RCLMOD(input,set_renoscheme)
       (const char*, const char*, long int, long int);
  // get_renoscheme_rcl -> wrapper
  void RCLMOD(input,set_resonant_particle)
       (const char*,long int);
  void RCLMOD(input,set_quarkline)
       (const int*,const int*,const int*);
  void RCLMOD(input,reset_vertices)
       ();
  void RCLMOD(input,reset_couplings)
       ();
  void RCLMOD(input,reset_ctcouplings)
       ();
  void RCLMOD(input,print_collier_statistics)
       ();

  // module recola1_interface_rcl
  void RCLMOD(recola1_interface,set_pole_mass_w)
       (const double*,const double*);
  void RCLMOD(recola1_interface,set_onshell_mass_w)
       (const double*,const double*);
  void RCLMOD(recola1_interface,set_pole_mass_z)
       (const double*,const double*);
  void RCLMOD(recola1_interface,set_onshell_mass_z)
       (const double*,const double*);
  void RCLMOD(recola1_interface,set_pole_mass_h)
       (const double*,const double*);
  void RCLMOD(recola1_interface,set_pole_mass_top)
       (const double*,const double*);
  void RCLMOD(recola1_interface,set_pole_mass_bottom)
       (const double*,const double*);
  void RCLMOD(recola1_interface,set_pole_mass_charm)
       (const double*,const double*);
  void RCLMOD(recola1_interface,set_pole_mass_strange)
       (const double*);
  void RCLMOD(recola1_interface,set_pole_mass_up)
       (const double*);
  void RCLMOD(recola1_interface,set_pole_mass_down)
       (const double*);
  void RCLMOD(recola1_interface,set_pole_mass_tau)
       (const double*,const double*);
  void RCLMOD(recola1_interface,set_pole_mass_muon)
       (const double*,const double*);
  void RCLMOD(recola1_interface,set_pole_mass_electron)
       (const double*);
  void RCLMOD(recola1_interface,set_light_top)
       ();
  void RCLMOD(recola1_interface,unset_light_top)
       ();
  void RCLMOD(recola1_interface,set_light_bottom)
       ();
  void RCLMOD(recola1_interface,unset_light_bottom)
       ();
  void RCLMOD(recola1_interface,set_light_charm)
       ();
  void RCLMOD(recola1_interface,unset_light_charm)
       ();
  void RCLMOD(recola1_interface,set_light_strange)
       ();
  void RCLMOD(recola1_interface,unset_light_strange)
       ();
  void RCLMOD(recola1_interface,set_light_up)
       ();
  void RCLMOD(recola1_interface,unset_light_up)
       ();
  void RCLMOD(recola1_interface,set_light_down)
       ();
  void RCLMOD(recola1_interface,unset_light_down)
       ();
  void RCLMOD(recola1_interface,set_light_tau)
       ();
  void RCLMOD(recola1_interface,unset_light_tau)
       ();
  void RCLMOD(recola1_interface,set_light_muon)
       ();
  void RCLMOD(recola1_interface,unset_light_muon)
       ();
  void RCLMOD(recola1_interface,set_light_electron)
       ();
  void RCLMOD(recola1_interface,unset_light_electron)
       ();
  void RCLMOD(recola1_interface,set_light_fermions)
       (const double*);
  // use_gfermi_scheme_rcl -> wrapper
  void RCLMOD(recola1_interface,use_alpha0_scheme)
       (const double*);
  void RCLMOD(recola1_interface,use_alphaz_scheme)
       (const double*);
  void RCLMOD(recola1_interface,get_alpha)
       (double*);
  void RCLMOD(recola1_interface,set_alphas)
       (const double*,const double*,const int*);
  void RCLMOD(recola1_interface,get_alphas)
       (double*);
  void RCLMOD(recola1_interface,set_alphas_masses)
       (const double*,const double*,const double*,
        const double*,const double*,const double*);
  void RCLMOD(recola1_interface,compute_running_alphas)
       (const double*, const int*, const int*);
  void RCLMOD(recola1_interface,select_all_gs_powers_bornampl)
       (const int*);
  void RCLMOD(recola1_interface,unselect_all_gs_powers_bornampl)
       (const int*);
  void RCLMOD(recola1_interface,select_gs_power_bornampl)
       (const int*, const int*);
  void RCLMOD(recola1_interface,unselect_gs_power_bornampl)
       (const int*, const int*);
  void RCLMOD(recola1_interface,select_all_gs_powers_loopampl)
       (const int*);
  void RCLMOD(recola1_interface,unselect_all_gs_powers_loopampl)
       (const int*);
  void RCLMOD(recola1_interface,select_gs_power_loopampl)
       (const int*, const int*);
  void RCLMOD(recola1_interface,unselect_gs_power_loopampl)
       (const int*, const int*);
  void RCLMOD(recola1_interface,get_colour_correlation_r1)
       (const int*, const int*, const int*, const int*,
        const char*, double*, long int);
  void RCLMOD(recola1_interface,get_spin_colour_correlation_r1)
       (const int*, const int*, const int*, const int*, const char*,
        double*, long int);
  void RCLMOD(recola1_interface,get_spin_correlation_r1)
       (const int*, const int*, const char*, double*, long int);
  void RCLMOD(recola1_interface,get_spin_correlation_matrix_r1)
       (const int*, const int*, const char*, double[4][4], long int);
  // set_gs_power_rcl -> wrapper
  // get_amplitude_r1_rcl -> wrapper
  /* get_*_amplitude_r1_rcl ?:  <24-07-17, Jean-Nicolas Lang> */
  /* get_*_correlation_r1_rcl ?:  <24-07-17, Jean-Nicolas Lang> */


  // module process_definition_rcl
  void RCLMOD(process_definition,define_process)
       (const int*,const char*,const char*,long int,long int);
  void RCLMOD(process_definition,select_power_bornampl)
       (const int*,const char*, const int*,long int);
  void RCLMOD(process_definition,unselect_power_bornampl)
       (const int*,const char*,const int*,long int);
  void RCLMOD(process_definition,select_all_powers_bornampl)
       (const int*);
  void RCLMOD(process_definition,unselect_all_powers_bornampl)
       (const int*);
  void RCLMOD(process_definition,select_power_loopampl)
       (const int*,const char*,const int*,long int);
  void RCLMOD(process_definition,unselect_power_loopampl)
       (const int*,const char*,const int*,long int);
  void RCLMOD(process_definition,select_all_powers_loopampl)
       (const int*);
  void RCLMOD(process_definition,unselect_all_powers_loopampl)
       (const int*);
  void RCLMOD(process_definition,split_collier_cache)
       (const int*,const int*);

  // module process_generation_rcl
  void RCLMOD(process_generation,generate_processes)
       ();
  void RCLMOD(process_generation,process_exists)
       (const int*, int *);

  // module process_computation_rcl
  void RCLMOD(process_computation,set_resonant_squared_momentum)
       (const int*, const int*, const double*);
  void RCLMOD(process_computation,rescale_process)
       (const int*,const char*,double*,long int);
  void RCLMOD(process_computation,rescale_all_colour_correlations)
       (const int*);
  void RCLMOD(process_computation,rescale_all_colour_correlations_int)
       (const int*);
  void RCLMOD(process_computation,set_tis_required_accuracy)
       (const double*);
  void RCLMOD(process_computation,get_tis_required_accuracy)
       (double*);
  void RCLMOD(process_computation,set_tis_critical_accuracy)
       (const double*);
  void RCLMOD(process_computation,get_tis_critical_accuracy)
       (double*);
  void RCLMOD(process_computation,get_tis_accuracy_flag)
       (int*);
  void RCLMOD(process_computation,get_n_colour_configurations)
       (int*,int*);
  void RCLMOD(process_computation,get_n_helicity_configurations)
       (int*,int*);

  // module wrapper_rcl
  void RCLMOD(wrapper,set_alphas_masses_nowidtharg)
       (const double*,const double*,const double*);
  void RCLMOD(wrapper,use_gfermi_scheme_noarg)
       ();
  void RCLMOD(wrapper,use_gfermi_scheme_and_set_alpha)
       (const double*);
  void RCLMOD(wrapper,use_gfermi_scheme_and_set_gfermi)
       (const double*);
  void RCLMOD(wrapper,use_alpha0_scheme_noarg)
       ();
  void RCLMOD(wrapper,use_alphaz_scheme_noarg)
       ();
  void RCLMOD(wrapper,wrapper_set_gs_power)
       (const int*,const int[][2],const int*);
  void RCLMOD(wrapper,wrapper_set_outgoing_momenta)
       (const int*, double[][4], double[][4], const int*);
  void RCLMOD(wrapper,wrapper_compute_process)
       (const int*,const double[][4],const int*,
        const char*,double*,int*,long int);
  void RCLMOD(wrapper,wrapper_get_amplitude_r1)
       (const int*, const int*, const char*, const int[], const int[],
        const int*, dcomplex*, long int);
  void RCLMOD(wrapper,wrapper_get_amplitude_general)
       (const int*, const int[], const int*, const char*, const int[],
        const int[], const int*, dcomplex*, long int);
  void RCLMOD(wrapper,wrapper_get_squared_amplitude_r1)
       (const int *, const int*,
        const char*, double*, long int);
  void RCLMOD(wrapper,wrapper_get_squared_amplitude_general)
       (const int *, const int[], const int*,
        const char*, double*, long int);
  void RCLMOD(wrapper,wrapper_get_polarized_squared_amplitude_r1)
       (const int*, const int*, const char*, const int[],
        const int*, double*, long int);
  void RCLMOD(wrapper,wrapper_get_polarized_squared_amplitude_general)
       (const int*, const int[], const int*, const char*, const int[],
        const int*, double*, long int);
  void RCLMOD(wrapper,wrapper_compute_colour_correlation)
       (const int*, const double[][4], const int*, const int*,
        const int*, const char*, double*, int*, long int);
  void RCLMOD(wrapper,wrapper_compute_colour_correlation_int)
       (const int*, const double[][4], const int*, const int*,
        const int*, double*, int*);
  void RCLMOD(wrapper,wrapper_compute_all_colour_correlations)
       (const int*, const double[][4], const int*, const char*, int*, long int);
  void RCLMOD(wrapper,wrapper_compute_all_colour_correlations_int)
       (const int*, const double[][4], const int*, int*);
  void RCLMOD(wrapper,wrapper_rescale_colour_correlation)
       (const int*,const int*,const int*,const char*,double*,long int);
  void RCLMOD(wrapper,wrapper_get_colour_correlation_general)
       (const int*, const int[], const int*, const int*,const int*,
        const char*, double*, long int);
  void RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)
       (const int*, const double [][4], const int*, const int*, const int*,
        const dcomplex[4], const char*, double*, int*, long int);
  void RCLMOD(wrapper,wrapper_rescale_spin_colour_correlation)
       (const int*, const int*, const int*,const dcomplex[4], const char*,
        double*, long int);
  void RCLMOD(wrapper,wrapper_get_spin_colour_correlation_general)
       (const int*, const int[], const int*, const int*,const int*,
        const char*, double*, long int);
  void RCLMOD(wrapper,wrapper_compute_spin_correlation)
       (const int*, const double[][4], const int*, const int*,
        const dcomplex[4], const char*, double*, int*, long int);
  void RCLMOD(wrapper,wrapper_rescale_spin_correlation)
       (const int*, const int*, const dcomplex[4], const char*, double*, long int);
  void RCLMOD(wrapper,wrapper_get_spin_correlation_general)
       (const int*, const int[], const int*, const char*, double*, long int);
  void RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)
       (const int*, const double[][4], const int*, const int*,
        const char*, double[4][4], int*, long int);
  void RCLMOD(wrapper,wrapper_rescale_spin_correlation_matrix)
       (const int*, const int*, const char*, double[4][4], long int);
  void RCLMOD(wrapper,wrapper_get_spin_correlation_matrix_general)
       (const int*, const int[], const int*, const char*, double[4][4], long int);
  void RCLMOD(wrapper,get_legs)
       (const int*, int*, const char*, long int);
  void RCLMOD(wrapper,get_n_orders)
       (int*);
  void RCLMOD(wrapper,wrapper_get_momenta)
       (const int*, double [][4], const int*);
  void RCLMOD(wrapper,wrapper_get_recola_version)
       (char*,int*,long int);
  void RCLMOD(wrapper,wrapper_get_modelname)
       (char*,int*,long int);
  void RCLMOD(wrapper,wrapper_get_modelgauge)
       (char*,int*,long int);
  void RCLMOD(wrapper,wrapper_get_driver_timestamp)
       (char*,int*,long int);
  void RCLMOD(wrapper,wrapper_get_renoscheme)
       (const char[],char[],int*,long int);
  void RCLMOD(wrapper,wrapper_get_colour_configuration)
       (int*,int*,int[],const int*);
  void RCLMOD(wrapper,wrapper_get_helicity_configuration)
       (int*,int*,int[],const int*);
  void RCLMOD(wrapper,wrapper_switchoff_coupling3)
       (const char*,const char*,const char*,const int*,long int,long int,long int);
  void RCLMOD(wrapper,wrapper_switchoff_coupling4)
       (const char*,const char*,const char*,const char*,const int*,
        long int,long int,long int,long int);
  void RCLMOD(wrapper,wrapper_switchoff_coupling5)
       (const char*,const char*,const char*,const char*,const char*,const int*,
        long int,long int,long int,long int,long int);
  void RCLMOD(wrapper,wrapper_switchoff_coupling6)
       (const char*,const char*,const char*,const char*,const char*,const char*,const int*,
        long int,long int,long int,long int,long int,long int);


    // module extended_higgs_interface_rcl
  void RCLMOD(extended_higgs_interface,set_z2_thdm_yukawa_type)
       (const int*);
  void RCLMOD(extended_higgs_interface,set_pole_mass_hl)
       (const double*,const double*);
  void RCLMOD(extended_higgs_interface,set_pole_mass_hh)
       (const double*,const double*);
  void RCLMOD(extended_higgs_interface,set_pole_mass_hl_hh)
       (const double*,const double*,const double*,const double*);
  void RCLMOD(extended_higgs_interface,set_pole_mass_ha)
       (const double*,const double*);
  void RCLMOD(extended_higgs_interface,set_pole_mass_hc)
       (const double*,const double*);
  void RCLMOD(extended_higgs_interface,set_tb_cab)
       (const double*,const double*);
  void RCLMOD(extended_higgs_interface,use_mixing_alpha_msbar_scheme)
       (const char*,long int);
  void RCLMOD(extended_higgs_interface,use_mixing_alpha_onshell_scheme)
       (const char*,long int);
  void RCLMOD(extended_higgs_interface,use_mixing_beta_onshell_scheme)
       (const char*,long int);
  void RCLMOD(extended_higgs_interface,use_mixing_beta_msbar_scheme)
       (const char*,long int);
  void RCLMOD(extended_higgs_interface,set_l5)
       (const double*);
  void RCLMOD(extended_higgs_interface,set_msb)
       (const double*);
  void RCLMOD(extended_higgs_interface,use_msb_msbar_scheme)
       (const char*,long int);
  void RCLMOD(extended_higgs_interface,set_sa)
       (const double*);
  // deprecated
  void RCLMOD(extended_higgs_interface,set_tb)
       (const double*);
  void RCLMOD(extended_higgs_interface,set_l3)
       (const double*);
  // deprecated
  void RCLMOD(extended_higgs_interface,use_tb_msbar_scheme)
       (const char*,long int);
  void RCLMOD(extended_higgs_interface,use_l3_msbar_scheme)
       (const char*,long int);
  // deprecated
  void RCLMOD(extended_higgs_interface,use_tb_onshell_scheme)
       (const char*,long int);
  void RCLMOD(extended_higgs_interface,use_l3_onshell_scheme)
       (const char*,long int);

  // module statistics_rcl
  void RCLMOD(statistics,print_generation_statistics)
       ();
  void RCLMOD(statistics,print_ti_statistics)
       (const int*,const int*);
  void RCLMOD(statistics,print_tc_statistics)
       (const int*,const int*);

  // module reset_rcl
  void RCLMOD(reset,reset_recola)
       ();

#ifdef __cplusplus
}
#endif

namespace Recola{

/******************
*  get_legs_rcl  *
******************/

ivoid get_legs_rcl
      (const int npr, int *legs, const std::string meth)
{
  RCLMOD(wrapper,get_legs)(&npr,legs,meth.c_str(),meth.length());
}

/**********************
*  module input_rcl  *
**********************/

ivoid set_qcd_rescaling_rcl
      (const bool m)
{
  int boolint;
  if (m) {
    boolint = 1;
  } else {
    boolint = 0;
  }
  RCLMOD(input,set_qcd_rescaling)(&boolint);
}
ivoid set_fermionloop_optimization_rcl(const bool m)
{
  int boolint;
  if (m) {
    boolint = 1;
  } else {
    boolint = 0;
  }
  RCLMOD(input,set_fermionloop_optimization)(&boolint);
}
ivoid set_helicity_optimization_rcl(const bool m)
{
  int boolint;
  if (m) {
    boolint = 1;
  } else {
    boolint = 0;
  }
  RCLMOD(input,set_helicity_optimization)(&boolint);
}
ivoid set_colour_optimization_rcl(const int m)
{
  RCLMOD(input,set_colour_optimization)(&m);
}
ivoid set_compute_A12_rcl(const bool m)
{
  int boolint;
  if (m) {
    boolint = 1;
  } else {
    boolint = 0;
  }
  RCLMOD(input,set_compute_A12)(&boolint);
}
ivoid set_masscut_rcl(const double mc)
{
  RCLMOD(input,set_masscut)(&mc);
}
ivoid set_light_particle_rcl
      (const std::string pa)
{
  RCLMOD(input,set_light_particle)(pa.c_str(),pa.length());
}
ivoid unset_light_particle_rcl
      (const std::string pa)
{
  RCLMOD(input,unset_light_particle)(pa.c_str(),pa.length());
}
ivoid use_dim_reg_soft_rcl()
{
  RCLMOD(input,use_dim_reg_soft)();
}
ivoid use_mass_reg_soft_rcl
      (const double m)
{
  RCLMOD(input,use_mass_reg_soft)(&m);
}
ivoid use_recola_base_rcl
      (const int rb)
{
  RCLMOD(input,use_recola_base)(&rb);
}
ivoid set_mass_reg_soft_rcl
      (const double m)
{
  RCLMOD(input,set_mass_reg_soft)(&m);
}
ivoid set_delta_uv_rcl
      (const double d)
{
  RCLMOD(input,set_delta_uv)(&d);
}
ivoid get_delta_uv_rcl
      (double &d)
{
  RCLMOD(input,get_delta_uv)(&d);
}
ivoid set_delta_ir_rcl
      (const double d1, const double d2)
{
  RCLMOD(input,set_delta_ir)(&d1,&d2);
}
ivoid get_delta_ir_rcl
      (double &d1, double &d2)
{
  RCLMOD(input,get_delta_ir)(&d1,&d2);
}
ivoid set_mu_uv_rcl
      (const double m)
{
  RCLMOD(input,set_mu_uv)(&m);
}
ivoid get_mu_uv_rcl
      (double &m)
{
  RCLMOD(input,get_mu_uv)(&m);
}
ivoid set_mu_ir_rcl
      (const double m)
{
  RCLMOD(input,set_mu_ir)(&m);
}
ivoid get_mu_ir_rcl
      (double &m)
{
  RCLMOD(input,get_mu_ir)(&m);
}
ivoid set_mu_ms_rcl
      (const double m)
{
  RCLMOD(input,set_mu_ms)(&m);
}
ivoid get_mu_ms_rcl
      (double &m)
{
  RCLMOD(input,get_mu_ms)(&m);
}
ivoid get_renormalization_scale_rcl
      (double &mu)
{
  RCLMOD(input,get_renormalization_scale)(&mu);
}
ivoid get_flavour_scheme_rcl
      (int &nf)
{
  RCLMOD(input,get_flavour_scheme)(&nf);
}
ivoid set_complex_mass_scheme_rcl()
{
  RCLMOD(input,set_complex_mass_scheme)();
}
ivoid set_on_shell_scheme_rcl()
{
  RCLMOD(input,set_on_shell_scheme)();
}
ivoid set_momenta_correction_rcl
      (const bool mc)
{
  int boolint;
  if (mc) {
    boolint = 1;
  } else {
    boolint = 0;
  }
  RCLMOD(input,set_momenta_correction)(&boolint);
}
ivoid set_print_level_amplitude_rcl
      (const int n)
{
  RCLMOD(input,set_print_level_amplitude)(&n);
}
ivoid set_print_level_squared_amplitude_rcl
      (const int n)
{
  RCLMOD(input,set_print_level_squared_amplitude)(&n);
}
ivoid set_print_level_correlations_rcl
      (const int n)
{
  RCLMOD(input,set_print_level_correlations)(&n);
}
ivoid set_print_level_parameters_rcl
      (const int n)
{
  RCLMOD(input,set_print_level_parameters)(&n);
}
ivoid set_draw_level_branches_rcl
      (const int n)
{
  RCLMOD(input,set_draw_level_branches)(&n);
}
ivoid set_print_level_RAM_rcl
      (const int n)
{
  RCLMOD(input,set_print_level_ram)(&n);
}
ivoid scale_coupling3_rcl
      (const std::complex<double> fac,
       const std::string pa1,
       const std::string pa2,
       const std::string pa3)
{
   dcomplex dfac;
   dfac.dr = fac.real();
   dfac.di = fac.imag();
   RCLMOD(input,scale_coupling3)(&dfac,
                                 pa1.c_str(),
                                 pa2.c_str(),
                                 pa3.c_str(),
                                 pa1.length(),
                                 pa2.length(),
                                 pa3.length());
}
ivoid scale_coupling4_rcl
      (const std::complex<double> fac,
       const std::string pa1,
       const std::string pa2,
       const std::string pa3,
       const std::string pa4)
{
   dcomplex dfac;
   dfac.dr = fac.real();
   dfac.di = fac.imag();
   RCLMOD(input,scale_coupling4)(&dfac,
                                 pa1.c_str(),
                                 pa2.c_str(),
                                 pa3.c_str(),
                                 pa4.c_str(),
                                 pa1.length(),
                                 pa2.length(),
                                 pa3.length(),
                                 pa4.length());
}
ivoid switchoff_coupling3_rcl
      (const std::string pa1,
       const std::string pa2,
       const std::string pa3)
{
  const int xlp=-1;
  RCLMOD(wrapper,wrapper_switchoff_coupling3)(
                                    pa1.c_str(),
                                    pa2.c_str(),
                                    pa3.c_str(),
                                    &xlp,
                                    pa1.length(),
                                    pa2.length(),
                                    pa3.length());
}
ivoid switchoff_coupling4_rcl
      (const std::string pa1,
       const std::string pa2,
       const std::string pa3,
       const std::string pa4)
{
  const int xlp=-1;
  RCLMOD(wrapper,wrapper_switchoff_coupling4)(
                                    pa1.c_str(),
                                    pa2.c_str(),
                                    pa3.c_str(),
                                    pa4.c_str(),
                                    &xlp,
                                    pa1.length(),
                                    pa2.length(),
                                    pa3.length(),
                                    pa4.length());
}
ivoid switchoff_coupling5_rcl
      (const std::string pa1,
       const std::string pa2,
       const std::string pa3,
       const std::string pa4,
       const std::string pa5)
{
  const int xlp=-1;
  RCLMOD(wrapper,wrapper_switchoff_coupling5)(
                                    pa1.c_str(),
                                    pa2.c_str(),
                                    pa3.c_str(),
                                    pa4.c_str(),
                                    pa5.c_str(),
                                    &xlp,
                                    pa1.length(),
                                    pa2.length(),
                                    pa3.length(),
                                    pa4.length(),
                                    pa5.length());
}
ivoid switchoff_coupling6_rcl
      (const std::string pa1,
       const std::string pa2,
       const std::string pa3,
       const std::string pa4,
       const std::string pa5,
       const std::string pa6)
{
  const int xlp=-1;
  RCLMOD(wrapper,wrapper_switchoff_coupling6)(
                                    pa1.c_str(),
                                    pa2.c_str(),
                                    pa3.c_str(),
                                    pa4.c_str(),
                                    pa5.c_str(),
                                    pa6.c_str(),
                                    &xlp,
                                    pa1.length(),
                                    pa2.length(),
                                    pa3.length(),
                                    pa4.length(),
                                    pa5.length(),
                                    pa6.length());
}
ivoid switchon_resonant_selfenergies_rcl()
{
  RCLMOD(input,switchon_resonant_selfenergies)();
}
ivoid switchoff_resonant_selfenergies_rcl()
{
  RCLMOD(input,switchoff_resonant_selfenergies)();
}
ivoid set_dynamic_settings_rcl
      (const int n)
{
  RCLMOD(input,set_dynamic_settings)(&n);
}
ivoid set_ifail_rcl
      (const int i)
{
   RCLMOD(input,set_ifail)(&i);
}
ivoid get_ifail_rcl
      (int &i)
{
   RCLMOD(input,get_ifail)(&i);
}
ivoid set_iwarn_rcl
      (const int i)
{
   RCLMOD(input,set_iwarn)(&i);
}
ivoid get_iwarn_rcl
      (int &i)
{
   RCLMOD(input,get_iwarn)(&i);
}
ivoid set_collier_mode_rcl
      (const int i)
{
   RCLMOD(input,set_collier_mode)(&i);
}
ivoid set_collier_output_dir_rcl
      (const std::string x)
{
  RCLMOD(input,set_collier_output_dir)(x.c_str(),x.length());
}
ivoid set_cache_mode_rcl
      (const int i)
{
  RCLMOD(input,set_cache_mode)(&i);
}
ivoid set_global_cache_rcl
      (const int i)
{
  RCLMOD(input,set_global_cache)(&i);
}
ivoid set_compute_ir_poles_rcl
      (int i)
{
  RCLMOD(input,set_compute_ir_poles)(&i);
}
ivoid switch_global_cache_rcl
      (const int i)
{
  RCLMOD(input,switch_global_cache)(&i);
}
ivoid set_output_file_rcl
      (const std::string x)
{
  RCLMOD(input,set_output_file)(x.c_str(),x.length());
}
ivoid set_log_mem_rcl
      (const bool lm)
{
  int boolint;
  if (lm) {
    boolint = 1;
  } else {
    boolint = 0;
  }
  RCLMOD(input,set_log_mem)(&boolint);
}
ivoid set_crossing_symmetry_rcl
      (const bool lm)
{
  int boolint;
  if (lm) {
    boolint = 1;
  } else {
    boolint = 0;
  }
  RCLMOD(input,set_crossing_symmetry)(&boolint);
}
ivoid set_init_collier_rcl
      (const bool ic)
{
  int boolint;
  if (ic) {
    boolint = 1;
  } else {
    boolint = 0;
  }
  RCLMOD(input,set_init_collier)(&boolint);
}
ivoid set_parameter_rcl
      (const std::string pname,
       std::complex<double> pvalue)
{
  dcomplex pvaluec;
  pvaluec.di = pvalue.imag();
  pvaluec.dr = pvalue.real();
  RCLMOD(input,set_parameter)(pname.c_str(),
                              &pvaluec,
                              pname.length());
}
ivoid get_parameter_rcl
      (const std::string pname,
       std::complex<double>& pvalue)
{
  dcomplex pvaluec;
  RCLMOD(input,get_parameter)(pname.c_str(),
                              &pvaluec,
                              pname.length());
  pvalue = std::complex<double> (pvaluec.dr, pvaluec.di);
}
ivoid set_renoscheme_rcl
      (const std::string renoparam,
       const std::string renoscheme)
{
  RCLMOD(input,set_renoscheme)(renoparam.c_str(),
                               renoscheme.c_str(),
                               renoparam.length(),
                               renoscheme.length());
}
ivoid set_resonant_particle_rcl
      (const std::string pa)
{
  RCLMOD(input,set_resonant_particle)(pa.c_str(),pa.length());
}
ivoid set_quarkline_rcl
      (const int npr, const int q1, const int q2)
{
  RCLMOD(input,set_quarkline)(&npr,&q1,&q2);
}
ivoid reset_vertices_rcl
      ()
{
  RCLMOD(input,reset_vertices)();
}
ivoid reset_couplings_rcl
      ()
{
  RCLMOD(input,reset_couplings)();
}
ivoid reset_ctcouplings_rcl
      ()
{
  RCLMOD(input,reset_ctcouplings)();
}
ivoid print_collier_statistics_rcl
      ()
{
  RCLMOD(input,print_collier_statistics)();
}


/**********************************
*  module recola1_interface_rcl  *
**********************************/
ivoid set_pole_mass_w_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_w)(&m,&g);
}
ivoid set_onshell_mass_w_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_onshell_mass_w)(&m,&g);
}
ivoid set_pole_mass_z_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_z)(&m,&g);
}
ivoid set_onshell_mass_z_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_onshell_mass_z)(&m,&g);
}
ivoid set_pole_mass_h_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_h)(&m,&g);
}
ivoid set_pole_mass_top_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_top)(&m,&g);
}
ivoid set_pole_mass_bottom_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_bottom)(&m,&g);
}
ivoid set_pole_mass_charm_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_charm)(&m,&g);
}
ivoid set_pole_mass_strange_rcl
      (const double m)
{
  RCLMOD(recola1_interface,set_pole_mass_strange)(&m);
}
ivoid set_pole_mass_up_rcl
      (const double m)
{
  RCLMOD(recola1_interface,set_pole_mass_up)(&m);
}
ivoid set_pole_mass_down_rcl
      (const double m)
{
  RCLMOD(recola1_interface,set_pole_mass_down)(&m);
}
ivoid set_pole_mass_tau_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_tau)(&m,&g);
}
ivoid set_pole_mass_muon_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_muon)(&m,&g);
}
ivoid set_pole_mass_electron_rcl
      (const double m)
{
  RCLMOD(recola1_interface,set_pole_mass_electron)(&m);
}
ivoid set_light_top_rcl()
{
  RCLMOD(recola1_interface,set_light_top)();
}
ivoid unset_light_top_rcl()
{
  RCLMOD(recola1_interface,unset_light_top)();
}
ivoid set_light_bottom_rcl()
{
  RCLMOD(recola1_interface,set_light_bottom)();
}
ivoid unset_light_bottom_rcl()
{
  RCLMOD(recola1_interface,unset_light_bottom)();
}
ivoid set_light_charm_rcl()
{
  RCLMOD(recola1_interface,set_light_charm)();
}
ivoid unset_light_charm_rcl()
{
  RCLMOD(recola1_interface,unset_light_charm)();
}
ivoid set_light_strange_rcl()
{
  RCLMOD(recola1_interface,set_light_strange)();
}
ivoid unset_light_strange_rcl()
{
  RCLMOD(recola1_interface,unset_light_strange)();
}
ivoid set_light_up_rcl()
{
  RCLMOD(recola1_interface,set_light_up)();
}
ivoid unset_light_up_rcl()
{
  RCLMOD(recola1_interface,unset_light_up)();
}
ivoid set_light_down_rcl()
{
  RCLMOD(recola1_interface,set_light_down)();
}
ivoid unset_light_down_rcl()
{
  RCLMOD(recola1_interface,unset_light_down)();
}
ivoid set_light_tau_rcl()
{
  RCLMOD(recola1_interface,set_light_tau)();
}
ivoid unset_light_tau_rcl()
{
  RCLMOD(recola1_interface,unset_light_tau)();
}
ivoid set_light_muon_rcl()
{
  RCLMOD(recola1_interface,set_light_muon)();
}
ivoid unset_light_muon_rcl()
{
  RCLMOD(recola1_interface,unset_light_muon)();
}
ivoid set_light_electron_rcl()
{
  RCLMOD(recola1_interface,set_light_electron)();
}
ivoid unset_light_electron_rcl()
{
  RCLMOD(recola1_interface,unset_light_electron)();
}
ivoid set_light_fermions_rcl
      (const double m)
{
  RCLMOD(recola1_interface,set_light_fermions)(&m);
}
ivoid use_alpha0_scheme_rcl
      (const double a)
{
  RCLMOD(recola1_interface,use_alpha0_scheme)(&a);
}
ivoid use_alphaz_scheme_rcl
      (const double a)
{
  RCLMOD(recola1_interface,use_alphaz_scheme)(&a);
}
ivoid get_alpha_rcl
      (double &a)
{
  RCLMOD(recola1_interface,get_alpha)(&a);
}
ivoid set_alphas_rcl
      (const double a, const double s, const int nf)
{
  RCLMOD(recola1_interface,set_alphas)(&a,&s,&nf);
}
ivoid get_alphas_rcl
      (double &a)
{
  RCLMOD(recola1_interface,get_alphas)(&a);
}
ivoid set_alphas_masses_rcl
      (const double mc, const double mb, const double mt,
       const double wc, const double wb, const double wt)
{
  RCLMOD(recola1_interface,set_alphas_masses)(&mc,&mb,&mt,&wc,&wb,&wt);
}
ivoid compute_running_alphas_rcl
      (const double q, const int nf, const int lp)
{
  RCLMOD(recola1_interface,compute_running_alphas)(&q,&nf,&lp);
}
ivoid select_all_gs_powers_BornAmpl_rcl
      (const int npr)
{
  RCLMOD(recola1_interface,select_all_gs_powers_bornampl)(&npr);
}
ivoid unselect_all_gs_powers_BornAmpl_rcl
      (const int npr)
{
  RCLMOD(recola1_interface,unselect_all_gs_powers_bornampl)(&npr);
}
ivoid select_gs_power_BornAmpl_rcl
      (const int npr, const int gspower)
{
  RCLMOD(recola1_interface,select_gs_power_bornampl)(&npr,&gspower);
}
ivoid unselect_gs_power_BornAmpl_rcl
      (const int npr, const int gspower)
{
  RCLMOD(recola1_interface,unselect_gs_power_bornampl)(&npr,&gspower);
}
ivoid select_all_gs_powers_LoopAmpl_rcl
      (const int npr)
{
  RCLMOD(recola1_interface,select_all_gs_powers_loopampl)(&npr);
}
ivoid unselect_all_gs_powers_LoopAmpl_rcl
      (const int npr)
{
  RCLMOD(recola1_interface,unselect_all_gs_powers_loopampl)(&npr);
}
ivoid select_gs_power_LoopAmpl_rcl
      (const int npr, const int gspower)
{
  RCLMOD(recola1_interface,select_gs_power_loopampl)(&npr,&gspower);
}
ivoid unselect_gs_power_LoopAmpl_rcl
      (const int npr, const int gspower)
{
  RCLMOD(recola1_interface,unselect_gs_power_loopampl)(&npr,&gspower);
}
ivoid get_colour_correlation_rcl
      (const int npr,
       const int pow,
       const int i1, const int i2,
       const std::string order,
       double& A2cc)
{
  RCLMOD(recola1_interface,get_colour_correlation_r1)(&npr,
                                                      &pow,
                                                      &i1,
                                                      &i2,
                                                      order.c_str(),
                                                      &A2cc,
                                                      order.length());
}
ivoid get_colour_correlation_rcl
      (const int npr,
       const int pow,
       const int i1, const int i2,
       double& A2cc)
{
  const std::string order = "LO";
  RCLMOD(recola1_interface,get_colour_correlation_r1)(&npr,
                                                      &pow,
                                                      &i1,
                                                      &i2,
                                                      order.c_str(),
                                                      &A2cc,
                                                      order.length());
}
ivoid get_spin_colour_correlation_rcl
      (const int npr,
       const int pow,
       const int i1, const int i2,
       double &A2scc)
{
  const std::string order = "LO";
  RCLMOD(recola1_interface,get_spin_colour_correlation_r1)(&npr,
                                                           &pow,
                                                           &i1,
                                                           &i2,
                                                           order.c_str(),
                                                           &A2scc,
                                                           order.length());
}
ivoid get_spin_colour_correlation_rcl
      (const int npr,
       const int pow,
       const int i1, const int i2,
       const std::string order,
       double &A2scc)
{
  RCLMOD(recola1_interface,get_spin_colour_correlation_r1)(&npr,
                                                           &pow,
                                                           &i1,
                                                           &i2,
                                                           order.c_str(),
                                                           &A2scc,
                                                           order.length());
}
ivoid get_spin_correlation_rcl
      (const int npr,
       const int pow,
       double &A2sc)
{
  const std::string order = "LO";
  RCLMOD(recola1_interface,get_spin_correlation_r1)(&npr,
                                                    &pow,
                                                    order.c_str(),
                                                    &A2sc,
                                                    order.length());
}
ivoid get_spin_correlation_rcl
      (const int npr,
       const int pow,
       const std::string order,
       double &A2sc)
{
  RCLMOD(recola1_interface,get_spin_correlation_r1)(&npr,
                                                    &pow,
                                                    order.c_str(),
                                                    &A2sc,
                                                    order.length());
}
ivoid get_spin_correlation_matrix_rcl
      (const int npr,
       const int pow,
       const std::string order,
       double A2scm[4][4])
{
  RCLMOD(recola1_interface,get_spin_correlation_matrix_r1)(&npr,
                                                           &pow,
                                                           order.c_str(),
                                                           A2scm,
                                                           order.length());
}
ivoid get_spin_correlation_matrix_rcl
      (const int npr,
       const int pow,
       double A2scm[4][4])
{
  const std::string order = "LO";
  RCLMOD(recola1_interface,get_spin_correlation_matrix_r1)(&npr,
                                                           &pow,
                                                           order.c_str(),
                                                           A2scm,
                                                           order.length());
}

/***********************************
*  module process_definition_rcl  *
***********************************/

ivoid define_process_rcl
      (const int npr,
       const std::string process,
       const std::string order)
{
  RCLMOD(process_definition,define_process)(&npr,
                                            process.c_str(),
                                            order.c_str(),
                                            process.length(),
                                            order.length());
}
ivoid select_power_BornAmpl_rcl
      (const int npr,
       const std::string orderid,
       const int pow)
{
  RCLMOD(process_definition,select_power_bornampl)(&npr,
                                                   orderid.c_str(),
                                                   &pow,
                                                   orderid.length());
}
ivoid unselect_power_BornAmpl_rcl
      (const int npr,
       const std::string orderid,
       const int pow)
{
  RCLMOD(process_definition,unselect_power_bornampl)(&npr,
                                                     orderid.c_str(),
                                                     &pow,
                                                     orderid.length());
}
ivoid select_all_powers_BornAmpl_rcl
      (const int npr)
{
  RCLMOD(process_definition,select_all_powers_bornampl)(&npr);
}
ivoid unselect_all_powers_BornAmpl_rcl
      (const int npr)
{
  RCLMOD(process_definition,unselect_all_powers_bornampl)(&npr);
}
ivoid select_power_LoopAmpl_rcl
      (const int npr,
       const std::string orderid,
       const int pow)
{
  RCLMOD(process_definition,select_power_loopampl)(&npr,
                                                   orderid.c_str(),
                                                   &pow,
                                                   orderid.length());
}
ivoid unselect_power_LoopAmpl_rcl
      (const int npr,
       const std::string orderid,
       const int pow)
{
  RCLMOD(process_definition,unselect_power_loopampl)(&npr,
                                                     orderid.c_str(),
                                                     &pow,
                                                     orderid.length());
}
ivoid select_all_powers_LoopAmpl_rcl
      (const int npr)
{
  RCLMOD(process_definition,select_all_powers_loopampl)(&npr);
}
ivoid unselect_all_powers_LoopAmpl_rcl
      (const int npr)
{
  RCLMOD(process_definition,unselect_all_powers_loopampl)(&npr);
}
ivoid split_collier_cache_rcl
      (const int npr,
       const int n)
{
  RCLMOD(process_definition,split_collier_cache)(&npr,&n);
}

/***********************************
*  module process_generation_rcl  *
***********************************/

ivoid generate_processes_rcl()
{
  RCLMOD(process_generation,generate_processes)();
}
ivoid process_exists_rcl(const int npr, bool& exists)
{

  int boolint;
  RCLMOD(process_generation,process_exists)(&npr, &boolint);
  if (boolint) {
    exists = true;
  } else {
    exists = false;
  }
}

/************************************
*  module process_computation_rcl  *
************************************/

ivoid set_resonant_squared_momentum_rcl
      (const int npr,const int res, const double ps)
{
  RCLMOD(process_computation,set_resonant_squared_momentum)(&npr,&res,&ps);
}
ivoid rescale_process_rcl
      (const int npr, const std::string order, double A2[2])
{
  RCLMOD(process_computation,rescale_process)(&npr,
                                              order.c_str(),
                                              A2,
                                              order.length());
}
ivoid rescale_process_rcl
      (const int npr, const std::string order)
{
  double A2[2];
  RCLMOD(process_computation,rescale_process)(&npr,
                                              order.c_str(),
                                              A2,
                                              order.length());
}
ivoid rescale_colour_correlation_rcl
      (const int npr,
       const int i1, const int i2,
       const std::string order,
       double &A2cc)
{
  RCLMOD(wrapper,wrapper_rescale_colour_correlation)(&npr,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     order.length());
}
ivoid rescale_colour_correlation_rcl
      (const int npr,
       const int i1, const int i2,
       double &A2cc)
{
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_rescale_colour_correlation)(&npr,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     order.length());
}
ivoid rescale_colour_correlation_rcl
      (const int npr,
       const int i1, const int i2,
       const std::string order)
{
  double A2cc;
  RCLMOD(wrapper,wrapper_rescale_colour_correlation)(&npr,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     order.length());
}
ivoid rescale_colour_correlation_rcl
      (const int npr,
       const int i1, const int i2)
{
  double A2cc;
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_rescale_colour_correlation)(&npr,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     order.length());
}
ivoid rescale_all_colour_correlations_rcl
      (const int npr)
{
  RCLMOD(process_computation,rescale_all_colour_correlations)(&npr);
}
ivoid rescale_spin_colour_correlation_rcl
      (const int npr,
       const int i1, const int i2,
       const std::complex<double> v[4],
       const std::string order,
       double &A2scc)
{
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  RCLMOD(wrapper,wrapper_rescale_spin_colour_correlation)(&npr,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          order.length());
}
ivoid rescale_spin_colour_correlation_rcl
      (const int npr,
       const int i1, const int i2,
       const std::complex<double> v[4],
       double &A2scc)
{
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_rescale_spin_colour_correlation)(&npr,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          order.length());
}
ivoid rescale_spin_colour_correlation_rcl
      (const int npr,
       const int i1, const int i2,
       const std::complex<double> v[4],
       const std::string order)
{
  double A2scc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  RCLMOD(wrapper,wrapper_rescale_spin_colour_correlation)(&npr,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          order.length());
}
ivoid rescale_spin_colour_correlation_rcl
      (const int npr,
       const int i1, const int i2,
       const std::complex<double> v[4])
{
  double A2scc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_rescale_spin_colour_correlation)(&npr,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          order.length());
}
ivoid rescale_spin_correlation_rcl
      (const int npr,
       const int j,
       const std::complex<double> v[4],
       const std::string order,
       double &A2sc)
{
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  RCLMOD(wrapper,wrapper_rescale_spin_correlation)(&npr,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   order.length());
}
ivoid rescale_spin_correlation_rcl
      (const int npr,
       const int j,
       const std::complex<double> v[4],
       double &A2sc)
{
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_rescale_spin_correlation)(&npr,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   order.length());
}
ivoid rescale_spin_correlation_rcl
      (const int npr,
       const int j,
       const std::complex<double> v[4],
       const std::string order)
{
  double A2sc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  RCLMOD(wrapper,wrapper_rescale_spin_correlation)(&npr,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   order.length());
}
ivoid rescale_spin_correlation_rcl
      (const int npr,
       const int j,
       const std::complex<double> v[4])
{
  double A2sc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_rescale_spin_correlation)(&npr,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   order.length());
}
ivoid set_tis_required_accuracy_rcl
      (const double acc)
{
  RCLMOD(process_computation,set_tis_required_accuracy)(&acc);
}
ivoid get_tis_required_accuracy_rcl
      (double &acc)
{
  RCLMOD(process_computation,get_tis_required_accuracy)(&acc);
}
ivoid set_tis_critical_accuracy_rcl
      (const double acc)
{
  RCLMOD(process_computation,set_tis_critical_accuracy)(&acc);
}
ivoid get_tis_critical_accuracy_rcl
      (double &acc)
{
  RCLMOD(process_computation,get_tis_critical_accuracy)(&acc);
}
ivoid get_tis_accuracy_flag_rcl
      (int &flag)
{
  RCLMOD(process_computation,get_tis_accuracy_flag)(&flag);
}
ivoid get_n_colour_configurations_rcl
      (int npr,int &n)
{
  RCLMOD(process_computation,get_n_colour_configurations)(&npr,&n);
}
ivoid get_colour_configuration_rcl
      (int npr,int n,int col[])
{
  int legs;
  get_legs_rcl(npr, &legs, "get_colour_configuration_rcl");
  RCLMOD(wrapper,wrapper_get_colour_configuration)(&npr,&n,col,&legs);
}
ivoid get_n_helicity_configurations_rcl
      (int npr,int &n)
{
  RCLMOD(process_computation,get_n_helicity_configurations)(&npr,&n);
}
ivoid get_helicity_configuration_rcl
      (int npr,int n,int hel[])
{
  int legs;
  get_legs_rcl(npr, &legs, "get_helicity_configuration_rcl");
  RCLMOD(wrapper,wrapper_get_helicity_configuration)(&npr,&n,hel,&legs);
}


/********************
*  module wrapper  *
********************/
ivoid set_alphas_masses_rcl
       (const double mc, const double mb, const double mt)
{
  RCLMOD(wrapper,set_alphas_masses_nowidtharg)(&mc,&mb,&mt);
}
ivoid use_gfermi_scheme_rcl()
{
  RCLMOD(wrapper,use_gfermi_scheme_noarg)();
}
ivoid use_gfermi_scheme_and_set_alpha_rcl
      (const double a)
{
  RCLMOD(wrapper,use_gfermi_scheme_and_set_alpha)(&a);
}
ivoid use_gfermi_scheme_and_set_gfermi_rcl
      (const double g)
{
  RCLMOD(wrapper,use_gfermi_scheme_and_set_gfermi)(&g);
}
ivoid use_alpha0_scheme_rcl()
{
  RCLMOD(wrapper,use_alpha0_scheme_noarg)();
}
ivoid use_alphaz_scheme_rcl()
{
  RCLMOD(wrapper,use_alphaz_scheme_noarg)();
}
ivoid set_gs_power_rcl
      (const int npr,
       const int gsarray[][2],
       const int gslen)
{
   RCLMOD(wrapper,wrapper_set_gs_power)(&npr,gsarray,&gslen);
}
ivoid set_outgoing_momenta_rcl
     (const int npr, double pIn[][4], double p[][4])
{
  int legs;
  get_legs_rcl(npr, &legs, "set_outgoing_momenta_rcl");
  RCLMOD(wrapper,wrapper_set_outgoing_momenta)(&npr, pIn, p, &legs);
}
ivoid compute_process_rcl
      (const int npr,
       const double p[][4],
       const std::string order,
       double A2[2],
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_process_rcl");
  int boolint;
  RCLMOD(wrapper,wrapper_compute_process)(&npr,
                                          p,
                                          &legs,
                                          order.c_str(),
                                          A2,
                                          &boolint,
                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_process_rcl
      (const int npr,
       const double p[][4],
       const std::string order,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_process_rcl");
  double A2[2];
  int boolint;
  RCLMOD(wrapper,wrapper_compute_process)(&npr,
                                          p,
                                          &legs,
                                          order.c_str(),
                                          A2,
                                          &boolint,
                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_process_rcl
      (const int npr,
       const double p[][4],
       const std::string order,
       double A2[2])
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_process_rcl");
  int boolint;
  RCLMOD(wrapper,wrapper_compute_process)(&npr,
                                          p,
                                          &legs,
                                          order.c_str(),
                                          A2,
                                          &boolint,
                                          order.length());
}
ivoid compute_process_rcl
      (const int npr,
       const double p[][4],
       const std::string order)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_process_rcl");
  double A2[2];
  int boolint;
  RCLMOD(wrapper,wrapper_compute_process)(&npr,
                                          p,
                                          &legs,
                                          order.c_str(),
                                          A2,
                                          &boolint,
                                          order.length());
}
ivoid get_amplitude_rcl
      (const int npr,
       const int pow,
       const std::string order,
       const int colour[], const int hel[],
       std::complex<double>& A)
{
  int legs;
  get_legs_rcl(npr, &legs, "get_amplitude_rcl");
  dcomplex dA;
  dA.di = A.imag();
  dA.dr = A.real();
  RCLMOD(wrapper,wrapper_get_amplitude_r1)(&npr,
                                           &pow,
                                           order.c_str(),
                                           colour,
                                           hel,
                                           &legs,
                                           &dA,
                                           order.length());
  A = std::complex<double> (dA.dr, dA.di);
}
ivoid get_amplitude_rcl
      (const int npr,
       const int pow[],
       const std::string order,
       const int colour[], const int hel[],
       std::complex<double>& A)
{
  int legs;
  get_legs_rcl(npr, &legs, "get_amplitude_rcl");
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  dcomplex dA;
  dA.di = A.imag();
  dA.dr = A.real();
  RCLMOD(wrapper,wrapper_get_amplitude_general)(&npr,
                                                pow,
                                                &powlen,
                                                order.c_str(),
                                                colour,
                                                hel,
                                                &legs,
                                                &dA,
                                                order.length());
  A = std::complex<double> (dA.dr, dA.di);
}
ivoid get_squared_amplitude_rcl
      (const int npr,
       const int pow,
       const std::string order,
       double &A2)
{
  RCLMOD(wrapper,wrapper_get_squared_amplitude_r1)(&npr,
                                                   &pow,
                                                   order.c_str(),
                                                   &A2,
                                                   order.length());
}
ivoid get_squared_amplitude_rcl
      (const int npr,
       const int pow[],
       const std::string order,
       double &A2)
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_squared_amplitude_general)(&npr,
                                                        pow,
                                                        &powlen,
                                                        order.c_str(),
                                                        &A2,
                                                        order.length());
}
ivoid get_polarized_squared_amplitude_rcl
      (const int npr,
       const int pow,
       const std::string order,
       const int hel[],
       double &A2h)
{
  int legs;
  get_legs_rcl(npr, &legs, "get_polarized_squared_amplitude_rcl");
  RCLMOD(wrapper,wrapper_get_polarized_squared_amplitude_r1)(&npr,
                                                             &pow,
                                                             order.c_str(),
                                                             hel,
                                                             &legs,
                                                             &A2h,
                                                             order.length());
}
ivoid get_polarized_squared_amplitude_rcl
      (const int npr,
       const int pow[],
       const std::string order,
       const int hel[],
       double &A2h)
{
  int legs;
  get_legs_rcl(npr, &legs, "get_polarized_squared_amplitude_rcl");
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_polarized_squared_amplitude_general)(&npr,
                                                                  pow,
                                                                  &powlen,
                                                                  order.c_str(),
                                                                  hel,
                                                                  &legs,
                                                                  &A2h,
                                                                  order.length());
}
ivoid compute_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::string order,
       double &A2cc,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_colour_correlation_rcl");
  int boolint;
  RCLMOD(wrapper,wrapper_compute_colour_correlation)(&npr,
                                                     p,
                                                     &legs,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     &boolint,
                                                     order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       double &A2cc,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_colour_correlation_rcl");
  int boolint;

  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_colour_correlation)(&npr,
                                                     p,
                                                     &legs,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     &boolint,
                                                     order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::string order,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_colour_correlation_rcl");
  int boolint;
  double A2cc;
  RCLMOD(wrapper,wrapper_compute_colour_correlation)(&npr,
                                                     p,
                                                     &legs,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     &boolint,
                                                     order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_colour_correlation_rcl");
  int boolint;
  double A2cc;
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_colour_correlation)(&npr,
                                                     p,
                                                     &legs,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     &boolint,
                                                     order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::string order,
       double &A2cc)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_colour_correlation_rcl");
  int boolint;
  RCLMOD(wrapper,wrapper_compute_colour_correlation)(&npr,
                                                     p,
                                                     &legs,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     &boolint,
                                                     order.length());
}
ivoid compute_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       double &A2cc)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_colour_correlation_rcl");
  int boolint;
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_colour_correlation)(&npr,
                                                     p,
                                                     &legs,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     &boolint,
                                                     order.length());
}
ivoid compute_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::string order)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_colour_correlation_rcl");
  int boolint;
  double A2cc;
  RCLMOD(wrapper,wrapper_compute_colour_correlation)(&npr,
                                                     p,
                                                     &legs,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     &boolint,
                                                     order.length());
}
ivoid compute_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_colour_correlation_rcl");
  int boolint;
  double A2cc;
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_colour_correlation)(&npr,
                                                     p,
                                                     &legs,
                                                     &i1,
                                                     &i2,
                                                     order.c_str(),
                                                     &A2cc,
                                                     &boolint,
                                                     order.length());
}
ivoid compute_all_colour_correlations_rcl
      (const int npr,
       const double p[][4],
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_all_colour_correlations_rcl");
  int boolint;
  std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_all_colour_correlations)(&npr,
                                                          p,
                                                          &legs,
                                                          order.c_str(),
                                                          &boolint,
                                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_all_colour_correlations_rcl
      (const int npr,
       const double p[][4])
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_all_colour_correlations_rcl");
  int boolint;
  std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_all_colour_correlations)(&npr,
                                                          p,
                                                          &legs,
                                                          order.c_str(),
                                                          &boolint,
                                                          order.length());
}
ivoid compute_all_colour_correlations_rcl
      (const int npr,
       const double p[][4],
       const std::string order)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_all_colour_correlations_rcl");
  int boolint;
  RCLMOD(wrapper,wrapper_compute_all_colour_correlations)(&npr,
                                                          p,
                                                          &legs,
                                                          order.c_str(),
                                                          &boolint,
                                                          order.length());
}
ivoid compute_all_colour_correlations_rcl
      (const int npr,
       const double p[][4],
       const std::string order,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_all_colour_correlations_rcl");
  int boolint;
  RCLMOD(wrapper,wrapper_compute_all_colour_correlations)(&npr,
                                                          p,
                                                          &legs,
                                                          order.c_str(),
                                                          &boolint,
                                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid get_colour_correlation_rcl
      (const int npr,
       int pow[],
       const int i1, const int i2,
       const std::string order,
       double& A2cc)
{

  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_colour_correlation_general)(&npr,
                                                         pow,
                                                         &powlen,
                                                         &i1,
                                                         &i2,
                                                         order.c_str(),
                                                         &A2cc,
                                                         order.length());
}
ivoid get_colour_correlation_rcl
      (const int npr,
       int pow[],
       const int i1, const int i2,
       double& A2cc)
{

  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_get_colour_correlation_general)(&npr,
                                                         pow,
                                                         &powlen,
                                                         &i1,
                                                         &i2,
                                                         order.c_str(),
                                                         &A2cc,
                                                         order.length());
}
ivoid compute_spin_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::complex<double> v[4],
       const std::string order,
       double &A2scc,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_colour_correlation_rcl");
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)(&npr,
                                                          p,
                                                          &legs,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          &boolint,
                                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::complex<double> v[4],
       double &A2scc,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_colour_correlation_rcl");
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)(&npr,
                                                          p,
                                                          &legs,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          &boolint,
                                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::complex<double> v[4],
       const std::string order,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_colour_correlation_rcl");
  double A2scc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)(&npr,
                                                          p,
                                                          &legs,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          &boolint,
                                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::complex<double> v[4],
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_colour_correlation_rcl");
  double A2scc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)(&npr,
                                                          p,
                                                          &legs,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          &boolint,
                                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::complex<double> v[4],
       const std::string order,
       double &A2scc)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_colour_correlation_rcl");
  int boolint;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)(&npr,
                                                          p,
                                                          &legs,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          &boolint,
                                                          order.length());
}
ivoid compute_spin_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::complex<double> v[4],
       double &A2scc)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_colour_correlation_rcl");
  int boolint;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)(&npr,
                                                          p,
                                                          &legs,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          &boolint,
                                                          order.length());
}
ivoid compute_spin_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::complex<double> v[4],
       const std::string order)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_colour_correlation_rcl");
  int boolint;
  double A2scc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)(&npr,
                                                          p,
                                                          &legs,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          &boolint,
                                                          order.length());
}
ivoid compute_spin_colour_correlation_rcl
      (const int npr,
       const double p[][4],
       const int i1, const int i2,
       const std::complex<double> v[4])
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_colour_correlation_rcl");
  int boolint;
  double A2scc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)(&npr,
                                                          p,
                                                          &legs,
                                                          &i1,
                                                          &i2,
                                                          dfac,
                                                          order.c_str(),
                                                          &A2scc,
                                                          &boolint,
                                                          order.length());
}
ivoid get_spin_colour_correlation_rcl
      (const int npr,
       int pow[],
       const int i1, const int i2,
       const std::string order,
       double &A2scc)
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_spin_colour_correlation_general)(&npr,
                                                              pow,
                                                              &powlen,
                                                              &i1,
                                                              &i2,
                                                              order.c_str(),
                                                              &A2scc,
                                                              order.length());
}
ivoid get_spin_colour_correlation_rcl
      (const int npr,
       int pow[],
       const int i1, const int i2,
       double &A2scc)
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_get_spin_colour_correlation_general)(&npr,
                                                              pow,
                                                              &powlen,
                                                              &i1,
                                                              &i2,
                                                              order.c_str(),
                                                              &A2scc,
                                                              order.length());
}
ivoid compute_spin_correlation_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::complex<double> v[4],
       const std::string order,
       double &A2sc,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_rcl");
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  RCLMOD(wrapper,wrapper_compute_spin_correlation)(&npr,
                                                   p,
                                                   &legs,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   &boolint,
                                                   order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_correlation_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::complex<double> v[4],
       double &A2sc,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_rcl");
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_correlation)(&npr,
                                                   p,
                                                   &legs,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   &boolint,
                                                   order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_correlation_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::complex<double> v[4],
       const std::string order,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_rcl");
  double A2sc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  RCLMOD(wrapper,wrapper_compute_spin_correlation)(&npr,
                                                   p,
                                                   &legs,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   &boolint,
                                                   order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_correlation_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::complex<double> v[4],
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_rcl");
  double A2sc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_correlation)(&npr,
                                                   p,
                                                   &legs,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   &boolint,
                                                   order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_correlation_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::complex<double> v[4],
       const std::string order,
       double &A2sc)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_rcl");
  int boolint;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  RCLMOD(wrapper,wrapper_compute_spin_correlation)(&npr,
                                                   p,
                                                   &legs,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   &boolint,
                                                   order.length());
}
ivoid compute_spin_correlation_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::complex<double> v[4],
       double &A2sc)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_rcl");
  int boolint;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_correlation)(&npr,
                                                   p,
                                                   &legs,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   &boolint,
                                                   order.length());
}
ivoid compute_spin_correlation_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::complex<double> v[4],
       const std::string order)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_rcl");
  int boolint;
  double A2sc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  RCLMOD(wrapper,wrapper_compute_spin_correlation)(&npr,
                                                   p,
                                                   &legs,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   &boolint,
                                                   order.length());
}
ivoid compute_spin_correlation_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::complex<double> v[4])
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_rcl");
  int boolint;
  double A2sc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_correlation)(&npr,
                                                   p,
                                                   &legs,
                                                   &j,
                                                   dfac,
                                                   order.c_str(),
                                                   &A2sc,
                                                   &boolint,
                                                   order.length());
}
ivoid get_spin_correlation_rcl
      (const int npr,
       int pow[],
       const std::string order,
       double &A2sc)
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_spin_correlation_general)(&npr,
                                                       pow,
                                                       &powlen,
                                                       order.c_str(),
                                                       &A2sc,
                                                       order.length());
}
ivoid get_spin_correlation_rcl
      (const int npr,
       int pow[],
       double &A2sc)
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_get_spin_correlation_general)(&npr,
                                                       pow,
                                                       &powlen,
                                                       order.c_str(),
                                                       &A2sc,
                                                       order.length());
}
ivoid compute_spin_correlation_matrix_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::string order,
       double A2scm[4][4],
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_matrix_rcl");
  int boolint;
  RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)(&npr,
                                                          p,
                                                          &legs,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          &boolint,
                                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_correlation_matrix_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::string order,
       double A2scm[4][4])
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_matrix_rcl");
  int boolint;
  RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)(&npr,
                                                          p,
                                                          &legs,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          &boolint,
                                                          order.length());
}
ivoid compute_spin_correlation_matrix_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::string order,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_matrix_rcl");
  int boolint;
  double A2scm[4][4];
  RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)(&npr,
                                                          p,
                                                          &legs,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          &boolint,
                                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_correlation_matrix_rcl
      (const int npr,
       const double p[][4],
       const int j,
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_matrix_rcl");
  int boolint;
  double A2scm[4][4];
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)(&npr,
                                                          p,
                                                          &legs,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          &boolint,
                                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_correlation_matrix_rcl
      (const int npr,
       const double p[][4],
       const int j,
       const std::string order)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_matrix_rcl");
  int boolint;
  double A2scm[4][4];
  RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)(&npr,
                                                          p,
                                                          &legs,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          &boolint,
                                                          order.length());
}
ivoid compute_spin_correlation_matrix_rcl
      (const int npr,
       const double p[][4],
       const int j,
       double A2scm[4][4],
       bool& momenta_check)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_matrix_rcl");
  int boolint;
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)(&npr,
                                                          p,
                                                          &legs,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          &boolint,
                                                          order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
ivoid compute_spin_correlation_matrix_rcl
      (const int npr,
       const double p[][4],
       const int j,
       double A2scm[4][4])
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_matrix_rcl");
  int boolint;
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)(&npr,
                                                          p,
                                                          &legs,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          &boolint,
                                                          order.length());
}
ivoid compute_spin_correlation_matrix_rcl
      (const int npr,
       const double p[][4],
       const int j)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_matrix_rcl");
  int boolint;
  const std::string order = "LO";
  double A2scm[4][4];
  RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)(&npr,
                                                          p,
                                                          &legs,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          &boolint,
                                                          order.length());
}
ivoid rescale_spin_correlation_matrix_rcl
      (const int npr,
       const int j,
       const std::string order,
       double A2scm[4][4])
{
  RCLMOD(wrapper,wrapper_rescale_spin_correlation_matrix)(&npr,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          order.length());
}
ivoid rescale_spin_correlation_matrix_rcl
      (const int npr,
       const int j,
       double A2scm[4][4])
{
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_rescale_spin_correlation_matrix)(&npr,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          order.length());
}
ivoid rescale_spin_correlation_matrix_rcl
      (const int npr,
       const int j,
       const std::string order)
{
  double A2scm[4][4];
  RCLMOD(wrapper,wrapper_rescale_spin_correlation_matrix)(&npr,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          order.length());
}
ivoid rescale_spin_correlation_matrix_rcl
      (const int npr,
       const int j)
{
  double A2scm[4][4];
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_rescale_spin_correlation_matrix)(&npr,
                                                          &j,
                                                          order.c_str(),
                                                          A2scm,
                                                          order.length());
}
ivoid get_spin_correlation_matrix_rcl
      (const int npr,
       int pow[],
       const std::string order,
       double A2scm[4][4])
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_spin_correlation_matrix_general)(&npr,
                                                              pow,
                                                              &powlen,
                                                              order.c_str(),
                                                              A2scm,
                                                              order.length());
}
ivoid get_spin_correlation_matrix_rcl
      (const int npr,
       int pow[],
       double A2scm[4][4])
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  const std::string order = "LO";
  RCLMOD(wrapper,wrapper_get_spin_correlation_matrix_general)(&npr,
                                                              pow,
                                                              &powlen,
                                                              order.c_str(),
                                                              A2scm,
                                                              order.length());
}
// get_legs_rcl
// get_n_orders_rcl
ivoid get_momenta_rcl
      (const int npr, double p[][4])
{
  int legs;
  get_legs_rcl(npr, &legs, "get_momenta_rcl");
  RCLMOD(wrapper,wrapper_get_momenta)(&npr,p,&legs);
}
ivoid get_recola_version_rcl
      (std::string& recolaversion)
{
  char rv_cstr[10];
  int len;
  RCLMOD(wrapper,wrapper_get_recola_version)(rv_cstr,&len,10);
  recolaversion = std::string(rv_cstr,len);
}
ivoid get_modelname_rcl
      (std::string& modelname)
{
  char modelname_cstr[100];
  int len;
  RCLMOD(wrapper,wrapper_get_modelname)(modelname_cstr,&len,100);
  modelname = std::string(modelname_cstr,len);
}
ivoid get_modelgauge_rcl
      (std::string& modelgauge)
{
  char modelgauge_cstr[100];
  int len;
  RCLMOD(wrapper,wrapper_get_modelgauge)(modelgauge_cstr,&len,100);
  modelgauge = std::string(modelgauge_cstr,len);
}
ivoid get_driver_timestamp_rcl
      (std::string& driverts)
{
  char driverts_cstr[200];
  int len;
  RCLMOD(wrapper,wrapper_get_driver_timestamp)(driverts_cstr,&len,200);
  driverts = std::string(driverts_cstr,len);
}
ivoid get_renoscheme_rcl
      (const std::string paramname, std::string& renoscheme)
{

  char renoscheme_cstr[100];
  int len;
  RCLMOD(wrapper,wrapper_get_renoscheme)(paramname.c_str(),
                                               renoscheme_cstr,&len,100);
  renoscheme = std::string(renoscheme_cstr,len);
}

/*****************************************
*  module extended_higgs_interface_rcl  *
*****************************************/

ivoid set_Z2_thdm_yukawa_type_rcl
      (const int ytype)
{
  RCLMOD(extended_higgs_interface,set_z2_thdm_yukawa_type)(&ytype);
}
ivoid set_pole_mass_hl_rcl
      (const double ml,
       const double gl)
{
  RCLMOD(extended_higgs_interface,set_pole_mass_hl)(&ml,&gl);
}
ivoid set_pole_mass_hh_rcl
      (const double mh,
       const double gh)
{
  RCLMOD(extended_higgs_interface,set_pole_mass_hh)(&mh,&gh);
}
ivoid set_pole_mass_hl_hh_rcl
      (const double ml,
       const double gl,
       const double mh,
       const double gh)
{
  RCLMOD(extended_higgs_interface,set_pole_mass_hl_hh)(&ml,&gl,&mh,&gh);
}
ivoid set_pole_mass_ha_rcl
      (const double ma,
       const double ga)
{
  RCLMOD(extended_higgs_interface,set_pole_mass_ha)(&ma,&ga);
}
ivoid set_pole_mass_hc_rcl
      (const double hc,
       const double gc)
{
  RCLMOD(extended_higgs_interface,set_pole_mass_hc)(&hc,&gc);
}
ivoid set_tb_cab_rcl
      (const double tb,
       const double cab)
{
   RCLMOD(extended_higgs_interface,set_tb_cab)(&tb,&cab);
}
ivoid use_mixing_alpha_msbar_scheme_rcl
      (const std::string s)
{
   RCLMOD(extended_higgs_interface,use_mixing_alpha_msbar_scheme)(s.c_str(), s.length());
}
ivoid use_mixing_alpha_onshell_scheme_rcl
      (const std::string s)
{
   RCLMOD(extended_higgs_interface,use_mixing_alpha_onshell_scheme)(s.c_str(), s.length());
}
ivoid use_mixing_beta_onshell_scheme_rcl
      (const std::string s)
{
   RCLMOD(extended_higgs_interface,use_mixing_beta_onshell_scheme)(s.c_str(), s.length());
}
ivoid use_mixing_beta_msbar_scheme_rcl
      (const std::string s)
{
   RCLMOD(extended_higgs_interface,use_mixing_beta_msbar_scheme)(s.c_str(), s.length());
}
ivoid set_l5_rcl
      (const double msb)
{
   RCLMOD(extended_higgs_interface,set_l5)(&msb);
}
ivoid set_msb_rcl
      (const double msb)
{
   RCLMOD(extended_higgs_interface,set_msb)(&msb);
}
ivoid use_msb_msbar_scheme_rcl
      (const std::string s)
{
   RCLMOD(extended_higgs_interface,use_msb_msbar_scheme)(s.c_str(), s.length());
}
ivoid set_sa_rcl
      (const double sa)
{
  RCLMOD(extended_higgs_interface,set_sa)(&sa);
}
ivoid set_tb_rcl
      (const double tb)
{
  RCLMOD(extended_higgs_interface,set_tb)(&tb);
}
ivoid set_l3_rcl
      (const double l3)
{
  RCLMOD(extended_higgs_interface,set_l3)(&l3);
}
ivoid use_tb_msbar_scheme_rcl
      (const std::string s)
{
   RCLMOD(extended_higgs_interface,use_tb_msbar_scheme)(s.c_str(), s.length());
}
ivoid use_l3_msbar_scheme_rcl
      (const std::string s)
{
   RCLMOD(extended_higgs_interface,use_l3_msbar_scheme)(s.c_str(), s.length());
}
ivoid use_tb_onshell_scheme_rcl
      (const std::string s)
{
   RCLMOD(extended_higgs_interface,use_tb_onshell_scheme)(s.c_str(), s.length());
}
ivoid use_l3_onshell_scheme_rcl
      (const std::string s)
{
   RCLMOD(extended_higgs_interface,use_l3_onshell_scheme)(s.c_str(), s.length());
}

/***************************
*  module statistics_rcl  *
***************************/

ivoid print_generation_statistics_rcl
      ()
{
  RCLMOD(statistics,print_generation_statistics)();
}
ivoid print_TI_statistics_rcl
      (const int npr, const int npoints)
{
  RCLMOD(statistics,print_ti_statistics)(&npr, &npoints);
}
ivoid print_TC_statistics_rcl
      (const int npr, const int npoints)
{
  RCLMOD(statistics,print_tc_statistics)(&npr, &npoints);
}

/**********************
*  module reset_rcl  *
**********************/

ivoid reset_recola_rcl()
{
  RCLMOD(reset,reset_recola)();
}

}

#endif // RECOLA_HPP_
