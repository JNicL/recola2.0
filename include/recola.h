//****************************************************************************//
//                                                                            //
//   recola.h                                                                 //
//   is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2       //
//                                                                            //
//   Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and             //
//                           Sandro Uccirati                                  //
//                                                                            //
//   RECOLA2 is licenced under the GNU GPL version 3,                         //
//   see COPYING for details.                                                 //
//                                                                            //
//****************************************************************************//

// Macro to add name-mangling bits to fortran symbols.

// Define GNU_NAME_MANGLING(INTEL_NAME_MANGLING) if Recola has been build with
// gfortran(ifort)

#if defined(GNU_NAME_MANGLING)
// gfortran
//#pragma message ("Name mangling according to GNU." )
#define MODFUNCNAME(mod,fname) __ ## mod ## _MOD_ ## fname
#elif defined(INTEL_NAME_MANGLING)
// ifort
//#pragma message ("Name mangling according to INTEL." )
#define MODFUNCNAME(mod,fname) mod ## _mp_ ## fname ## _
#else
#warning "Name mangling not set. Trying GNU."
#define MODFUNCNAME(mod,fname) __ ## mod ## _MOD_ ## fname
#endif // if __GNUC__

#define RCLMOD(mod,fname) MODFUNCNAME(mod ## _rcl, fname ## _rcl)

#ifndef RECOLA_H_
#define RECOLA_H_

#define ivoid void
typedef struct cconstruct{double dr,di;} dcomplex;

// module input_rcl
void RCLMOD(input,set_qcd_rescaling)
       (const int*);
void RCLMOD(input,set_fermionloop_optimization)
     (const int*);
void RCLMOD(input,set_helicity_optimization)
     (const int*);
void RCLMOD(input,set_colour_optimization)
     (const int*);
void RCLMOD(input,set_compute_a12)
     (const int*);
void RCLMOD(input,set_masscut)
     (const double*);
void RCLMOD(input,set_light_particle)
     (const char*,long int);
void RCLMOD(input,unset_light_particle)
     (const char*,long int);
void RCLMOD(input,use_dim_reg_soft)
     ();
void RCLMOD(input,use_mass_reg_soft)
     (const double*);
void RCLMOD(input,use_recola_base)
     (const int*);
void RCLMOD(input,set_mass_reg_soft)
     (const double*);
void RCLMOD(input,set_delta_uv)
     (const double*);
void RCLMOD(input,get_delta_uv)
     (const double*);
void RCLMOD(input,set_delta_ir)
     (const double*, const double*);
void RCLMOD(input,get_delta_ir)
     (const double*, const double*);
void RCLMOD(input,set_mu_uv)
     (const double*);
void RCLMOD(input,get_mu_uv)
     (const double*);
void RCLMOD(input,set_mu_ir)
     (const double*);
void RCLMOD(input,get_mu_ir)
     (const double*);
void RCLMOD(input,set_mu_ms)
     (const double*);
void RCLMOD(input,get_mu_ms)
     (const double*);
void RCLMOD(input,get_renormalization_scale)
     (double*);
void RCLMOD(input,get_flavour_scheme)
     (int*);
void RCLMOD(input,set_complex_mass_scheme)
     ();
void RCLMOD(input,set_on_shell_scheme)
     ();
void RCLMOD(input,set_momenta_correction)
     (const int*);
void RCLMOD(input,set_print_level_amplitude)
     (const int*);
void RCLMOD(input,set_print_level_squared_amplitude)
     (const int*);
void RCLMOD(input,set_print_level_correlations)
     (const int*);
void RCLMOD(input,set_print_level_parameters)
     (const int*);
void RCLMOD(input,set_draw_level_branches)
     (const int*);
void RCLMOD(input,set_print_level_ram)
     (const int*);
void RCLMOD(input,scale_coupling3)
     (const dcomplex*,const char*,const char*, const char*,
      long int,long int,long int);
void RCLMOD(input,scale_coupling4)
     (const dcomplex*, const char*, const char*, const char*,const char*,
      long int,long int,long int,long int);
//void RCLMOD(input,switchoff_coupling3)
//     (const char*,const char*,const char*,const int*,long int,long int,long int);
//void RCLMOD(input,switchoff_coupling4)
//     (const char*,const char*,const char*,const char*,const int*,
//      long int,long int,long int,long int);
//void RCLMOD(input,switchoff_coupling5)
//     (const char*,const char*,const char*,const char*,const char*,const int*,
//      long int,long int,long int,long int);
void RCLMOD(input,switchon_resonant_selfenergies)
     ();
void RCLMOD(input,switchoff_resonant_selfenergies)
     ();
void RCLMOD(input,set_dynamic_settings)
     (const int*);
void RCLMOD(input,set_ifail)
     (const int*);
void RCLMOD(input,get_ifail)
     (int*);
void RCLMOD(input,set_iwarn)
     (const int*);
void RCLMOD(input,get_iwarn)
     (int*);
void RCLMOD(input,set_collier_mode)
     (const int*);
void RCLMOD(input,set_collier_output_dir)
     (const char*,long int);
void RCLMOD(input,set_cache_mode)
     (const int*);
void RCLMOD(input,set_global_cache)
     (const int*);
void RCLMOD(input,switch_global_cache)
     (const int*);
void RCLMOD(input,set_compute_ir_poles)
     (const int*);
void RCLMOD(input,set_output_file)
     (const char*,long int);
void RCLMOD(input,set_log_mem)
     (const int*);
void RCLMOD(input,set_crossing_symmetry)
     (const int*);
void RCLMOD(input,set_init_collier)
     (const int*);
void RCLMOD(input,set_parameter)
     (const char*, dcomplex*, long int);
void RCLMOD(input,get_parameter)
     (const char*, dcomplex*, long int);
void RCLMOD(input,set_renoscheme)
     (const char*, const char*, long int, long int);
// get_renoscheme_rcl -> wrapper
void RCLMOD(input,set_resonant_particle)
     (const char*,long int);
void RCLMOD(input,set_quarkline)
     (const int*,const int*,const int*);
void RCLMOD(input,reset_vertices)
     ();
void RCLMOD(input,reset_couplings)
     ();
void RCLMOD(input,reset_ctcouplings)
     ();
void RCLMOD(input,print_collier_statistics)
     ();

// additional stuff, only used in combination with rept1l
void RCLMOD(input,set_compute_selfenergy)
     (int*);
void RCLMOD(input,set_compute_selfenergy_offshell)
     (int*);
void RCLMOD(input,set_compute_tadpole)
     (int*);
void RCLMOD(input,set_lp)
     (int*, int*);
void RCLMOD(input,set_form)
     (int*);
void RCLMOD(input,set_all_couplings_active)
     (int*);
void RCLMOD(input,set_all_tree_couplings_active)
     (int*);
void RCLMOD(input,set_all_loop_couplings_active)
     (int*);
void RCLMOD(input,set_all_ct_couplings_active)
     (int*);
void RCLMOD(input,set_all_r2_couplings_active)
     (int*);
void RCLMOD(input,set_complex_mass_form)
     (int*);
void RCLMOD(input,set_vertex_functions)
     (int*);
void RCLMOD(input,set_particle_ordering)
     (int*);
void RCLMOD(input,set_masscut_form)
     (int*);
void RCLMOD(input,set_cutparticle)
     (const char*, long int);
void RCLMOD(input,unset_cutparticle)
     ();
void RCLMOD(input,set_print_recola_logo)
     (int*);

// module recola1_interface_rcl
void RCLMOD(recola1_interface,set_pole_mass_w)
     (const double*,const double*);
void RCLMOD(recola1_interface,set_onshell_mass_w)
     (const double*,const double*);
void RCLMOD(recola1_interface,set_pole_mass_z)
     (const double*,const double*);
void RCLMOD(recola1_interface,set_onshell_mass_z)
     (const double*,const double*);
void RCLMOD(recola1_interface,set_pole_mass_h)
     (const double*,const double*);
void RCLMOD(recola1_interface,set_pole_mass_top)
     (const double*,const double*);
void RCLMOD(recola1_interface,set_pole_mass_bottom)
     (const double*,const double*);
void RCLMOD(recola1_interface,set_pole_mass_charm)
     (const double*,const double*);
void RCLMOD(recola1_interface,set_pole_mass_strange)
     (const double*);
void RCLMOD(recola1_interface,set_pole_mass_up)
     (const double*);
void RCLMOD(recola1_interface,set_pole_mass_down)
     (const double*);
void RCLMOD(recola1_interface,set_pole_mass_tau)
     (const double*,const double*);
void RCLMOD(recola1_interface,set_pole_mass_muon)
     (const double*,const double*);
void RCLMOD(recola1_interface,set_pole_mass_electron)
     (const double*);
void RCLMOD(recola1_interface,set_light_top)
     ();
void RCLMOD(recola1_interface,unset_light_top)
     ();
void RCLMOD(recola1_interface,set_light_bottom)
     ();
void RCLMOD(recola1_interface,unset_light_bottom)
     ();
void RCLMOD(recola1_interface,set_light_charm)
     ();
void RCLMOD(recola1_interface,unset_light_charm)
     ();
void RCLMOD(recola1_interface,set_light_strange)
     ();
void RCLMOD(recola1_interface,unset_light_strange)
     ();
void RCLMOD(recola1_interface,set_light_up)
     ();
void RCLMOD(recola1_interface,unset_light_up)
     ();
void RCLMOD(recola1_interface,set_light_down)
     ();
void RCLMOD(recola1_interface,unset_light_down)
     ();
void RCLMOD(recola1_interface,set_light_tau)
     ();
void RCLMOD(recola1_interface,unset_light_tau)
     ();
void RCLMOD(recola1_interface,set_light_muon)
     ();
void RCLMOD(recola1_interface,unset_light_muon)
     ();
void RCLMOD(recola1_interface,set_light_electron)
     ();
void RCLMOD(recola1_interface,unset_light_electron)
     ();
void RCLMOD(recola1_interface,set_light_fermions)
     (const double*);
// use_gfermi_scheme_rcl -> wrapper
void RCLMOD(recola1_interface,use_alpha0_scheme)
     (const double*);
void RCLMOD(recola1_interface,use_alphaz_scheme)
     (const double*);
void RCLMOD(recola1_interface,get_alpha)
     (double*);
void RCLMOD(recola1_interface,set_alphas)
     (const double*,const double*,const int*);
void RCLMOD(recola1_interface,get_alphas)
     (double*);
void RCLMOD(recola1_interface,set_alphas_masses)
     (const double*,const double*,const double*,
      const double*,const double*,const double*);
void RCLMOD(recola1_interface,compute_running_alphas)
     (const double*, const int*, const int*);
void RCLMOD(recola1_interface,select_all_gs_powers_bornampl)
     (const int*);
void RCLMOD(recola1_interface,unselect_all_gs_powers_bornampl)
     (const int*);
void RCLMOD(recola1_interface,select_gs_power_bornampl)
     (const int*, const int*);
void RCLMOD(recola1_interface,unselect_gs_power_bornampl)
     (const int*, const int*);
void RCLMOD(recola1_interface,select_all_gs_powers_loopampl)
     (const int*);
void RCLMOD(recola1_interface,unselect_all_gs_powers_loopampl)
     (const int*);
void RCLMOD(recola1_interface,select_gs_power_loopampl)
     (const int*, const int*);
void RCLMOD(recola1_interface,unselect_gs_power_loopampl)
     (const int*, const int*);
void RCLMOD(recola1_interface,get_colour_correlation_r1)
     (const int*, const int*, const int*, const int*,
      const char*, double*, long int);
void RCLMOD(recola1_interface,get_spin_colour_correlation_r1)
     (const int*, const int*, const int*, const int*, const char*,
      double*, long int);
void RCLMOD(recola1_interface,get_spin_correlation_r1)
     (const int*, const int*, const char*, double*, long int);
void RCLMOD(recola1_interface,get_spin_correlation_matrix_r1)
     (const int*, const int*, const char*, double[4][4], long int);
// set_gs_power_rcl -> wrapper
// get_amplitude_r1_rcl -> wrapper
/* get_*_amplitude_r1_rcl ?:  <24-07-17, Jean-Nicolas Lang> */
/* get_*_correlation_r1_rcl ?:  <24-07-17, Jean-Nicolas Lang> */

// module process_definition_rcl
void RCLMOD(process_definition,define_process)
     (const int*,const char*,const char*,long int,long int);
void RCLMOD(process_definition,select_power_bornampl)
     (const int*,const char*, const int*,long int);
void RCLMOD(process_definition,unselect_power_bornampl)
     (const int*,const char*,const int*,long int);
void RCLMOD(process_definition,select_all_powers_bornampl)
     (const int*);
void RCLMOD(process_definition,unselect_all_powers_bornampl)
     (const int*);
void RCLMOD(process_definition,select_power_loopampl)
     (const int*,const char*,const int*,long int);
void RCLMOD(process_definition,unselect_power_loopampl)
     (const int*,const char*,const int*,long int);
void RCLMOD(process_definition,select_all_powers_loopampl)
     (const int*);
void RCLMOD(process_definition,unselect_all_powers_loopampl)
     (const int*);
void RCLMOD(process_definition,split_collier_cache)
     (const int*,const int*);

// module process_generation_rcl
void RCLMOD(process_generation,generate_processes)
     ();
void RCLMOD(process_generation,process_exists)
     (const int*, int *);

// module process_computation_rcl
void RCLMOD(process_computation,set_resonant_squared_momentum)
     (const int*, const int*, const double*);
void RCLMOD(process_computation,rescale_process)
     (const int*,const char*,double*,long int);
void RCLMOD(process_computation,rescale_all_colour_correlations)
     (const int*);
void RCLMOD(process_computation,set_tis_required_accuracy)
     (const double*);
void RCLMOD(process_computation,get_tis_required_accuracy)
     (double*);
void RCLMOD(process_computation,set_tis_critical_accuracy)
     (const double*);
void RCLMOD(process_computation,get_tis_critical_accuracy)
     (double*);
void RCLMOD(process_computation,get_tis_accuracy_flag)
     (int*);
void RCLMOD(process_computation,get_n_colour_configurations)
     (int*,int*);
void RCLMOD(process_computation,get_n_helicity_configurations)
     (int*,int*);


// module wrapper_rcl
void RCLMOD(wrapper,set_alphas_masses_nowidtharg)
     (const double*,const double*,const double*);
void RCLMOD(wrapper,use_gfermi_scheme_noarg)
     ();
void RCLMOD(wrapper,use_gfermi_scheme_and_set_alpha)
     (const double*);
void RCLMOD(wrapper,use_gfermi_scheme_and_set_gfermi)
     (const double*);
void RCLMOD(wrapper,use_alpha0_scheme_noarg)
     ();
void RCLMOD(wrapper,use_alphaz_scheme_noarg)
     ();
void RCLMOD(wrapper,wrapper_set_gs_power)
     (const int*,int[][2],const int*);
void RCLMOD(wrapper,wrapper_set_outgoing_momenta)
     (const int*, double[][4], double[][4], const int*);
void RCLMOD(wrapper,wrapper_compute_process)
     (const int*,double[][4],const int*,
      const char*,double*,int*,long int);
void RCLMOD(wrapper,wrapper_get_amplitude_r1)
     (const int*, const int*, const char*, const int[], const int[],
      const int*, dcomplex*, long int);
void RCLMOD(wrapper,wrapper_get_amplitude_general)
     (const int*, const int[], const int*, const char*, const int[],
      const int[], const int*, dcomplex*, long int);
void RCLMOD(wrapper,wrapper_get_squared_amplitude_r1)
     (const int *, const int*,
      const char*, double*, long int);
void RCLMOD(wrapper,wrapper_get_squared_amplitude_general)
     (const int *, const int[], const int*,
      const char*, double*, long int);
void RCLMOD(wrapper,wrapper_get_polarized_squared_amplitude_r1)
     (const int*, const int*, const char*, const int[],
      const int*, double*, long int);
void RCLMOD(wrapper,wrapper_get_polarized_squared_amplitude_general)
     (const int*, const int[], const int*, const char*, const int[],
      const int*, double*, long int);
void RCLMOD(wrapper,wrapper_compute_colour_correlation)
     (const int*, double[][4], const int*, const int*, const int*,
      const char*, double*, int*, long int);
void RCLMOD(wrapper,wrapper_compute_colour_correlation_int)
     (const int*, double[][4], const int*, const int*, const int*,
      double*, int*);
void RCLMOD(wrapper,wrapper_compute_all_colour_correlations)
     (const int*, double[][4], const int*, const char*, int*, long int);
void RCLMOD(wrapper,wrapper_rescale_colour_correlation)
     (const int*,const int*,const int*, const char*,double*,long int);
void RCLMOD(wrapper,wrapper_get_colour_correlation_general)
     (const int*, const int[], const int*, const int*,const int*,
      const char*, double*, long int);
void RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)
     (const int*, double [][4], const int*, const int*, const int*,
      const dcomplex[4], const char*, double*, int*, long int);
void RCLMOD(wrapper,wrapper_rescale_spin_colour_correlation)
     (const int*, const int*, const int*,const dcomplex[4], const char*,
      double*, long int);
void RCLMOD(wrapper,wrapper_get_spin_colour_correlation_general)
     (const int*, const int[], const int*, const int*,const int*,
      const char*, double*, long int);
void RCLMOD(wrapper,wrapper_compute_spin_correlation)
     (const int*, double[][4], const int*, const int*,
      const dcomplex[4], const char*, double*, int*, long int);
void RCLMOD(wrapper,wrapper_rescale_spin_correlation)
     (const int*, const int*, const dcomplex[4], const char*, double*, long int);
void RCLMOD(wrapper,wrapper_get_spin_correlation_general)
     (const int*, const int[], const int*, const char*, double*, long int);
void RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)
     (const int*, double[][4], const int*, const int*,
      const char*, double[4][4], int*, long int);
void RCLMOD(wrapper,wrapper_rescale_spin_correlation_matrix)
     (const int*, const int*, const char*, double[4][4], long int);
void RCLMOD(wrapper,wrapper_get_spin_correlation_matrix_general)
     (const int*, const int[], const int*, const char*, double[4][4], long int);
void RCLMOD(wrapper,get_legs)
     (const int*, int*, const char*, long int);
void RCLMOD(wrapper,get_n_orders)
     (int*);
void RCLMOD(wrapper,wrapper_get_momenta)
     (const int*, double [][4], const int*);
void RCLMOD(wrapper,wrapper_get_recola_version)
     (char*,int*,long int);
void RCLMOD(wrapper,wrapper_get_modelname)
     (char*,int*,long int);
void RCLMOD(wrapper,wrapper_get_modelgauge)
     (char*,int*,long int);
void RCLMOD(wrapper,wrapper_get_driver_timestamp)
     (char*,int*,long int);
void RCLMOD(wrapper,wrapper_get_renoscheme)
     (const char[],char[], int*,long int);
void RCLMOD(wrapper,wrapper_get_colour_configuration)
     (int*,int*,int[],const int*);
void RCLMOD(wrapper,wrapper_get_helicity_configuration)
     (int*,int*,int[],const int*);
void RCLMOD(wrapper,wrapper_switchoff_coupling3)
     (const char*,const char*,const char*,const int*,long int,long int,long int);
void RCLMOD(wrapper,wrapper_switchoff_coupling4)
     (const char*,const char*,const char*,const char*,const int*,
      long int,long int,long int,long int);
void RCLMOD(wrapper,wrapper_switchoff_coupling5)
     (const char*,const char*,const char*,const char*,const char*,const int*,
      long int,long int,long int,long int,long int);
void RCLMOD(wrapper,wrapper_switchoff_coupling6)
     (const char*,const char*,const char*,const char*,const char*,const char*,const int*,
      long int,long int,long int,long int,long int,long int);

  // module extended_higgs_interface_rcl
void RCLMOD(extended_higgs_interface,set_z2_thdm_yukawa_type)
     (const int*);
void RCLMOD(extended_higgs_interface,set_pole_mass_hl)
     (const double*,const double*);
void RCLMOD(extended_higgs_interface,set_pole_mass_hh)
     (const double*,const double*);
void RCLMOD(extended_higgs_interface,set_pole_mass_hl_hh)
     (const double*,const double*,const double*,const double*);
void RCLMOD(extended_higgs_interface,set_pole_mass_ha)
     (const double*,const double*);
void RCLMOD(extended_higgs_interface,set_pole_mass_hc)
     (const double*,const double*);
void RCLMOD(extended_higgs_interface,set_tb_cab)
     (const double*,const double*);
void RCLMOD(extended_higgs_interface,use_mixing_alpha_msbar_scheme)
     (const char*,long int);
void RCLMOD(extended_higgs_interface,use_mixing_alpha_onshell_scheme)
     (const char*,long int);
void RCLMOD(extended_higgs_interface,use_mixing_beta_onshell_scheme)
     (const char*,long int);
void RCLMOD(extended_higgs_interface,use_mixing_beta_msbar_scheme)
     (const char*,long int);
void RCLMOD(extended_higgs_interface,set_l5)
     (const double*);
void RCLMOD(extended_higgs_interface,set_msb)
     (const double*);
void RCLMOD(extended_higgs_interface,use_msb_msbar_scheme)
     (const char*,long int);
void RCLMOD(extended_higgs_interface,set_sa)
     (const double*);
//deprecated
void RCLMOD(extended_higgs_interface,set_tb)
     (const double*);
void RCLMOD(extended_higgs_interface,set_l3)
     (const double*);
//deprecated
void RCLMOD(extended_higgs_interface,use_tb_msbar_scheme)
     (const char*,long int);
void RCLMOD(extended_higgs_interface,use_l3_msbar_scheme)
     (const char*,long int);
//deprecated
void RCLMOD(extended_higgs_interface,use_tb_onshell_scheme)
     (const char*,long int);
void RCLMOD(extended_higgs_interface,use_l3_onshell_scheme)
     (const char*,long int);

// module statistics_rcl
void RCLMOD(statistics,print_generation_statistics)
     ();
void RCLMOD(statistics,print_ti_statistics)
     (const int*,const int*);
void RCLMOD(statistics,print_tc_statistics)
     (const int*,const int*);

// module reset_rcl
void RCLMOD(reset,reset_recola)
     ();

/******************
*  get_legs_rcl  *
******************/

ivoid get_legs_rcl
      (const int npr, int *legs, const char* meth)
{
  RCLMOD(wrapper,get_legs)(&npr,legs,meth,strlen(meth));
}

/**********************
*  module input_rcl  *
**********************/

ivoid set_qcd_rescaling_rcl
      (const int value)
{
  RCLMOD(input,set_qcd_rescaling)(&value);
}
ivoid set_fermionloop_optimization_rcl
      (const int value)
{
  RCLMOD(input,set_fermionloop_optimization)(&value);
}
ivoid set_helicity_optimization_rcl
      (const int value)
{
  RCLMOD(input,set_helicity_optimization)(&value);
}
ivoid set_colour_optimization_rcl
      (const int value)
{
  RCLMOD(input,set_colour_optimization)(&value);
}
ivoid set_compute_A12_rcl
      (const int value)
{
  RCLMOD(input,set_compute_a12)(&value);
}
ivoid set_masscut_rcl
      (double value)
{
  RCLMOD(input,set_masscut)(&value);
}
ivoid set_light_particle_rcl
      (const char* pa)
{
  RCLMOD(input,set_light_particle)(pa,strlen(pa));
}
ivoid unset_light_particle_rcl
      (const char* pa)
{
  RCLMOD(input,unset_light_particle)(pa,strlen(pa));
}
ivoid use_dim_reg_soft_rcl()
{
  RCLMOD(input,use_dim_reg_soft)();
}
ivoid use_mass_reg_soft_rcl
      (const double m)
{
  RCLMOD(input,use_mass_reg_soft)(&m);
}
ivoid use_recola_base_rcl
      (const int rb)
{
  RCLMOD(input,use_recola_base)(&rb);
}
ivoid set_mass_reg_soft_rcl
      (const double m)
{
  RCLMOD(input,set_mass_reg_soft)(&m);
}
ivoid set_delta_uv_rcl
      (const double d)
{
  RCLMOD(input,set_delta_uv)(&d);
}
ivoid get_delta_uv_rcl
      (double* d)
{
  RCLMOD(input,get_delta_uv)(d);
}
ivoid set_delta_ir_rcl
      (const double d, const double d2)
{
  RCLMOD(input,set_delta_ir)(&d, &d2);
}
ivoid get_delta_ir_rcl
      (double* d, double* d2)
{
  RCLMOD(input,get_delta_ir)(d, d2);
}
ivoid set_mu_uv_rcl
      (const double mu_uv)
{
  RCLMOD(input,set_mu_uv)(&mu_uv);
}
ivoid get_mu_uv_rcl
      (double* mu_uv)
{
  RCLMOD(input,get_mu_uv)(mu_uv);
}
ivoid set_mu_ir_rcl
      (const double mu_ir)
{
  RCLMOD(input,set_mu_ir)(&mu_ir);
}
ivoid get_mu_ir_rcl
      (double* mu_ir)
{
  RCLMOD(input,get_mu_ir)(mu_ir);
}
ivoid set_mu_ms_rcl
      (const double mu_ms)
{
  RCLMOD(input,set_mu_ms)(&mu_ms);
}
ivoid get_mu_ms_rcl
      (double* mu_ms)
{
  RCLMOD(input,get_mu_ms)(mu_ms);
}
ivoid get_renormalization_scale_rcl
      (double* mu)
{
  RCLMOD(input,get_renormalization_scale)(mu);
}
ivoid get_flavour_scheme_rcl
      (int *nf)
{
  RCLMOD(input,get_flavour_scheme)(nf);
}
ivoid set_complex_mass_scheme_rcl()
{
  RCLMOD(input,set_complex_mass_scheme)();
}
ivoid set_on_shell_scheme_rcl()
{
  RCLMOD(input,set_on_shell_scheme)();
}
ivoid set_momenta_correction_rcl
      (const int value)
{
  RCLMOD(input,set_momenta_correction)(&value);
}
ivoid set_print_level_amplitude_rcl
     (const int n)
{
  RCLMOD(input,set_print_level_amplitude)(&n);
}
ivoid set_print_level_squared_amplitude_rcl
     (const int n)
{
  RCLMOD(input,set_print_level_squared_amplitude)(&n);
}
ivoid set_print_level_correlations_rcl
      (const int n)
{
  RCLMOD(input,set_print_level_correlations)(&n);
}
ivoid set_print_level_parameters_rcl
            (const int n)
{
  RCLMOD(input,set_print_level_parameters)(&n);
}
ivoid set_draw_level_branches_rcl
      (const int n)
{
  RCLMOD(input,set_draw_level_branches)(&n);
}
ivoid set_print_level_RAM_rcl
      (const int n)
{
  RCLMOD(input,set_print_level_ram)(&n);
}
ivoid scale_coupling3_rcl
      (const dcomplex fac,
       const char* pa1,
       const char* pa2,
       const char* pa3)
{
   RCLMOD(input,scale_coupling3)(&fac,
                                 pa1,
                                 pa2,
                                 pa3,
                                 strlen(pa1),
                                 strlen(pa2),
                                 strlen(pa3));
}
ivoid scale_coupling4_rcl
      (const dcomplex fac,
       const char* pa1,
       const char* pa2,
       const char* pa3,
       const char* pa4)
{
   RCLMOD(input,scale_coupling4)(&fac,
                                 pa1,
                                 pa2,
                                 pa3,
                                 pa4,
                                 strlen(pa1),
                                 strlen(pa2),
                                 strlen(pa3),
                                 strlen(pa4));
}
ivoid switchoff_coupling3_rcl
      (const char* pa1,
       const char* pa2,
       const char* pa3,
       const int xlp)
{
  RCLMOD(wrapper,wrapper_switchoff_coupling3)(pa1,
                                    pa2,
                                    pa3,
                                    &xlp,
                                    strlen(pa1),
                                    strlen(pa2),
                                    strlen(pa3));
}
ivoid switchoff_coupling4_rcl
      (const char* pa1,
       const char* pa2,
       const char* pa3,
       const char* pa4,
       const int xlp)
{
  RCLMOD(wrapper,wrapper_switchoff_coupling4)(pa1,
                                    pa2,
                                    pa3,
                                    pa4,
                                    &xlp,
                                    strlen(pa1),
                                    strlen(pa2),
                                    strlen(pa3),
                                    strlen(pa4));
}
ivoid switchoff_coupling5_rcl
      (const char* pa1,
       const char* pa2,
       const char* pa3,
       const char* pa4,
       const char* pa5,
       const int xlp)
{
  RCLMOD(wrapper,wrapper_switchoff_coupling5)(pa1,
                                    pa2,
                                    pa3,
                                    pa4,
                                    pa5,
                                    &xlp,
                                    strlen(pa1),
                                    strlen(pa2),
                                    strlen(pa3),
                                    strlen(pa4),
                                    strlen(pa5));
}
ivoid switchoff_coupling6_rcl
      (const char* pa1,
       const char* pa2,
       const char* pa3,
       const char* pa4,
       const char* pa5,
       const char* pa6,
       const int xlp)
{
  RCLMOD(wrapper,wrapper_switchoff_coupling6)(pa1,
                                    pa2,
                                    pa3,
                                    pa4,
                                    pa5,
                                    pa6,
                                    &xlp,
                                    strlen(pa1),
                                    strlen(pa2),
                                    strlen(pa3),
                                    strlen(pa4),
                                    strlen(pa5),
                                    strlen(pa6));
}
ivoid switchon_resonant_selfenergies_rcl()
{
  RCLMOD(input,switchon_resonant_selfenergies)();
}
ivoid switchoff_resonant_selfenergies_rcl()
{
  RCLMOD(input,switchoff_resonant_selfenergies)();
}
ivoid set_dynamic_settings_rcl
      (const int n)
{
  RCLMOD(input,set_dynamic_settings)(&n);
}
ivoid set_ifail_rcl
      (const int i)
{
   RCLMOD(input,set_ifail)(&i);
}
ivoid get_ifail_rcl
      (int *i)
{
   RCLMOD(input,get_ifail)(i);
}
ivoid set_iwarn_rcl
      (const int i)
{
   RCLMOD(input,set_iwarn)(&i);
}
ivoid get_iwarn_rcl
      (int *i)
{
   RCLMOD(input,get_iwarn)(i);
}
ivoid set_collier_mode_rcl
      (const int i)
{
   RCLMOD(input,set_collier_mode)(&i);
}
ivoid set_collier_output_dir_rcl
      (const char* x)
{
  RCLMOD(input,set_collier_output_dir)(x,strlen(x));
}
ivoid set_cache_mode_rcl
      (int i)
{
  RCLMOD(input,set_cache_mode)(&i);
}
ivoid set_global_cache_rcl
      (int i)
{
  RCLMOD(input,set_global_cache)(&i);
}
ivoid switch_global_cache_rcl
      (int i)
{
  RCLMOD(input,switch_global_cache)(&i);
}
ivoid set_compute_ir_poles_rcl
      (int i)
{
  RCLMOD(input,set_compute_ir_poles)(&i);
}
ivoid set_output_file_rcl
      (const char* x)
{
  RCLMOD(input,set_output_file)(x,strlen(x));
}
ivoid set_log_mem_rcl
      (const int value)
{
  RCLMOD(input,set_log_mem)(&value);
}
ivoid set_crossing_symmetry_rcl
      (const int value)
{
  RCLMOD(input,set_crossing_symmetry)(&value);
}
ivoid set_init_collier_rcl
      (int value)
{
  RCLMOD(input,set_init_collier)(&value);
}
ivoid set_parameter_rcl
      (const char* pname,
       dcomplex pvalue)
{
  RCLMOD(input,set_parameter)(pname,
                              &pvalue,
                              strlen(pname));
}
ivoid get_parameter_rcl
      (const char* pname,
       dcomplex* pvalue)
{
  RCLMOD(input,get_parameter)(pname,
                              pvalue,
                              strlen(pname));
}
ivoid set_renoscheme_rcl
      (const char* renovar, const char* renoscheme){
  RCLMOD(input,set_renoscheme)(renovar,
                               renoscheme,
                               strlen(renovar),
                               strlen(renoscheme));
}
ivoid set_resonant_particle_rcl
      (const char* p)
{
  RCLMOD(input,set_resonant_particle)(p, strlen(p));
}
ivoid set_quarkline_rcl
            (int npr, int q1, int q2)
{
  RCLMOD(input,set_quarkline)(&npr,&q1,&q2);
}
ivoid reset_vertices_rcl()
{
  RCLMOD(input,reset_vertices)();
}
ivoid reset_couplings_rcl()
{
  RCLMOD(input,reset_couplings)();
}
ivoid reset_ctcouplings_rcl()
{
  RCLMOD(input,reset_ctcouplings)();
}
ivoid print_collier_statistics_rcl()
{
  RCLMOD(input,print_collier_statistics)();
}


/**********************************************
*  module input_rcl, additionals for rept1l  *
**********************************************/
ivoid set_compute_selfenergy_rcl
      (int value)
{
  RCLMOD(input,set_compute_selfenergy)(&value);
}
ivoid set_compute_selfenergy_offshell_rcl
      (int value)
{
  RCLMOD(input,set_compute_selfenergy_offshell)(&value);
}
ivoid set_compute_tadpole_rcl
      (int value)
{
  RCLMOD(input,set_compute_tadpole)(&value);
}
ivoid set_lp_rcl
      (int lp_order, int value)
{
  RCLMOD(input,set_lp)(&lp_order, &value);
}
ivoid set_form_rcl
      (int value)
{
  RCLMOD(input,set_form)(&value);
}
ivoid set_all_couplings_active_rcl
      (int value)
{
  RCLMOD(input,set_all_couplings_active)(&value);
}
ivoid set_all_tree_couplings_active_rcl
      (int value)
{
  RCLMOD(input,set_all_tree_couplings_active)(&value);
}
ivoid set_all_loop_couplings_active_rcl
      (int value)
{
  RCLMOD(input,set_all_loop_couplings_active)(&value);
}
ivoid set_all_ct_couplings_active_rcl
      (int value)
{
  RCLMOD(input,set_all_ct_couplings_active)(&value);
}
ivoid set_all_r2_couplings_active_rcl
      (int value)
{
  RCLMOD(input,set_all_r2_couplings_active)(&value);
}
ivoid set_complex_mass_form_rcl
      (int value)
{
  RCLMOD(input,set_complex_mass_form)(&value);
}
ivoid set_vertex_functions_rcl
      (int value)
{
  RCLMOD(input,set_vertex_functions)(&value);
}
ivoid set_particle_ordering_rcl
      (int value)
{
  RCLMOD(input,set_particle_ordering)(&value);
}
ivoid set_masscut_form_rcl
      (int value)
{
  RCLMOD(input,set_masscut_form)(&value);
}
ivoid set_cutparticle_rcl
      (const char* pname)
{
  RCLMOD(input,set_cutparticle)(pname, strlen(pname));
}
ivoid unset_cutparticle_rcl()
{
  RCLMOD(input,unset_cutparticle)();
}
ivoid set_print_recola_logo_rcl
      (int value)
{
  RCLMOD(input,set_print_recola_logo)(&value);
}

/**********************************
*  module recola1_interface_rcl  *
**********************************/

ivoid set_pole_mass_w_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_w)(&m,&g);
}
ivoid set_onshell_mass_w_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_onshell_mass_w)(&m,&g);
}
ivoid set_pole_mass_z_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_z)(&m,&g);
}
ivoid set_onshell_mass_z_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_onshell_mass_z)(&m,&g);
}
ivoid set_pole_mass_h_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_h)(&m,&g);
}
ivoid set_pole_mass_top_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_top)(&m,&g);
}
ivoid set_pole_mass_bottom_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_bottom)(&m,&g);
}
ivoid set_pole_mass_charm_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_charm)(&m,&g);
}
ivoid set_pole_mass_strange_rcl
      (const double m)
{
  RCLMOD(recola1_interface,set_pole_mass_strange)(&m);
}
ivoid set_pole_mass_up_rcl
      (const double m)
{
  RCLMOD(recola1_interface,set_pole_mass_up)(&m);
}
ivoid set_pole_mass_down_rcl
      (const double m)
{
  RCLMOD(recola1_interface,set_pole_mass_down)(&m);
}
ivoid set_pole_mass_tau_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_tau)(&m,&g);
}
ivoid set_pole_mass_muon_rcl
      (const double m, const double g)
{
  RCLMOD(recola1_interface,set_pole_mass_muon)(&m,&g);
}
ivoid set_pole_mass_electron_rcl
      (const double m)
{
  RCLMOD(recola1_interface,set_pole_mass_electron)(&m);
}
ivoid set_light_top_rcl()
{
  RCLMOD(recola1_interface,set_light_top)();
}
ivoid unset_light_top_rcl()
{
  RCLMOD(recola1_interface,unset_light_top)();
}
ivoid set_light_bottom_rcl()
{
  RCLMOD(recola1_interface,set_light_bottom)();
}
ivoid unset_light_bottom_rcl()
{
  RCLMOD(recola1_interface,unset_light_bottom)();
}
ivoid set_light_charm_rcl()
{
  RCLMOD(recola1_interface,set_light_charm)();
}
ivoid unset_light_charm_rcl()
{
  RCLMOD(recola1_interface,unset_light_charm)();
}
ivoid set_light_strange_rcl()
{
  RCLMOD(recola1_interface,set_light_strange)();
}
ivoid unset_light_strange_rcl()
{
  RCLMOD(recola1_interface,unset_light_strange)();
}
ivoid set_light_up_rcl()
{
  RCLMOD(recola1_interface,set_light_up)();
}
ivoid unset_light_up_rcl()
{
  RCLMOD(recola1_interface,unset_light_up)();
}
ivoid set_light_down_rcl()
{
  RCLMOD(recola1_interface,set_light_down)();
}
ivoid unset_light_down_rcl()
{
  RCLMOD(recola1_interface,unset_light_down)();
}
ivoid set_light_tau_rcl()
{
  RCLMOD(recola1_interface,set_light_tau)();
}
ivoid unset_light_tau_rcl()
{
  RCLMOD(recola1_interface,unset_light_tau)();
}
ivoid set_light_muon_rcl()
{
  RCLMOD(recola1_interface,set_light_muon)();
}
ivoid unset_light_muon_rcl()
{
  RCLMOD(recola1_interface,unset_light_muon)();
}
ivoid set_light_electron_rcl()
{
  RCLMOD(recola1_interface,set_light_electron)();
}
ivoid unset_light_electron_rcl()
{
  RCLMOD(recola1_interface,unset_light_electron)();
}
ivoid set_light_fermions_rcl
      (const double m)
{
  RCLMOD(recola1_interface,set_light_fermions)(&m);
}
ivoid use_alpha0_scheme_and_set_alpha_rcl
      (const double a)
{
  RCLMOD(recola1_interface,use_alpha0_scheme)(&a);
}
ivoid use_alphaz_scheme_and_set_alpha_rcl
      (const double a)
{
  RCLMOD(recola1_interface,use_alphaz_scheme)(&a);
}
ivoid get_alpha_rcl
      (double* a)
{
  RCLMOD(recola1_interface,get_alpha)(a);
}
ivoid set_alphas_rcl
      (double* aS, double* scaleQCD, int* nflavour)
{
  RCLMOD(recola1_interface,set_alphas)(aS, scaleQCD, nflavour);
}
ivoid get_alphas_rcl
      (double* a)
{
  RCLMOD(recola1_interface,get_alphas)(a);
}
ivoid set_alphas_masses_rcl
      (const double mc, const double mb, const double mt,
       const double wc, const double wb, const double wt)
{
  RCLMOD(recola1_interface,set_alphas_masses)(&mc,&mb,&mt,&wc,&wb,&wt);
}
ivoid compute_running_alphas_rcl
      (const double q, const int nf, const int lp)
{
  RCLMOD(recola1_interface,compute_running_alphas)(&q,&nf,&lp);
}
ivoid select_all_gs_powers_BornAmpl_rcl
      (const int npr)
{
  RCLMOD(recola1_interface,select_all_gs_powers_bornampl)(&npr);
}
ivoid unselect_all_gs_powers_BornAmpl_rcl
      (const int npr)
{
  RCLMOD(recola1_interface,unselect_all_gs_powers_bornampl)(&npr);
}
ivoid select_gs_power_BornAmpl_rcl
      (const int npr, const int gspower)
{
  RCLMOD(recola1_interface,select_gs_power_bornampl)(&npr,&gspower);
}
ivoid unselect_gs_power_BornAmpl_rcl
      (const int npr, const int gspower)
{
  RCLMOD(recola1_interface,unselect_gs_power_bornampl)(&npr,&gspower);
}
ivoid select_all_gs_powers_LoopAmpl_rcl
      (const int npr)
{
  RCLMOD(recola1_interface,select_all_gs_powers_loopampl)(&npr);
}
ivoid unselect_all_gs_powers_LoopAmpl_rcl
      (const int npr)
{
  RCLMOD(recola1_interface,unselect_all_gs_powers_loopampl)(&npr);
}
ivoid select_gs_power_LoopAmpl_rcl
      (const int npr, const int gspower)
{
  RCLMOD(recola1_interface,select_gs_power_loopampl)(&npr,&gspower);
}
ivoid unselect_gs_power_LoopAmpl_rcl
      (const int npr, const int gspower)
{
  RCLMOD(recola1_interface,unselect_gs_power_loopampl)(&npr,&gspower);
}
ivoid get_colour_correlation_r1_rcl
      (const int npr,
       const int pow,
       const int i1, const int i2,
       const char* order,
       double* A2cc)
{
  RCLMOD(recola1_interface,get_colour_correlation_r1)(&npr,
                                                      &pow,
                                                      &i1,
                                                      &i2,
                                                      order,
                                                      A2cc,
                                                      strlen(order));
}
ivoid get_spin_colour_correlation_r1_rcl
      (const int npr,
       const int pow,
       const int i1, const int i2,
       const char* order,
       double* A2scc)
{
  RCLMOD(recola1_interface,get_spin_colour_correlation_r1)(&npr,
                                                           &pow,
                                                           &i1,
                                                           &i2,
                                                           order,
                                                           A2scc,
                                                           strlen(order));
}
ivoid get_spin_correlation_r1_rcl
      (const int npr,
       const int pow,
       const char* order,
       double* A2sc)
{
  RCLMOD(recola1_interface,get_spin_correlation_r1)(&npr,
                                                    &pow,
                                                    order,
                                                    A2sc,
                                                    strlen(order));
}
ivoid get_spin_correlation_matrix_r1_rcl
      (const int npr,
       const int pow,
       const char* order,
       double A2scm[4][4])
{
  RCLMOD(recola1_interface,get_spin_correlation_matrix_r1)(&npr,
                                                           &pow,
                                                           order,
                                                           A2scm,
                                                           strlen(order));
}


/*******************************
*  module process definition  *
*******************************/

ivoid define_process_rcl
      (const int n,
       const char* proc,
       const char* order)
{
  RCLMOD(process_definition,define_process)(&n,
                                            proc,
                                            order,
                                            strlen(proc),
                                            strlen(order));
}
ivoid select_power_bornampl_rcl
      (int n, const char* order, int power)
{
  RCLMOD(process_definition,select_power_bornampl)(&n, order, &power, strlen(order));
}
ivoid unselect_power_bornampl_rcl
      (int n, const char* order, int power)
{
  RCLMOD(process_definition,unselect_power_bornampl)(&n, order, &power, strlen(order));
}
ivoid select_all_powers_bornampl_rcl
      (const int n)
{
  RCLMOD(process_definition,select_all_powers_bornampl)(&n);
}
ivoid unselect_all_powers_bornampl_rcl
      (const int n)
{
  RCLMOD(process_definition,unselect_all_powers_bornampl)(&n);
}
ivoid select_power_loopampl_rcl
      (int n, const char* order, int power)
{
  RCLMOD(process_definition,select_power_loopampl)(&n, order, &power, strlen(order));
}
ivoid unselect_power_loopampl_rcl
      (int n, const char* order, int power)
{
  RCLMOD(process_definition,unselect_power_loopampl)(&n, order, &power, strlen(order));
}
ivoid select_all_powers_loopampl_rcl
      (int n)
{
  RCLMOD(process_definition,select_all_powers_loopampl)(&n);
}
ivoid unselect_all_powers_loopampl_rcl
      (int n)
{
  RCLMOD(process_definition,unselect_all_powers_loopampl)(&n);
}
ivoid split_collier_cache_rcl
      (const int npr,
       const int n)
{
  RCLMOD(process_definition,split_collier_cache)(&npr,&n);
}

/*******************************
*  module process generation  *
*******************************/

ivoid generate_processes_rcl()
{
  RCLMOD(process_generation,generate_processes)();
}
ivoid process_exists_rcl(const int npr, int* exists)
{
  RCLMOD(process_generation,process_exists)(&npr, exists);
}

/************************************
*  module process_computation_rcl  *
************************************/

ivoid set_resonant_squared_momentum_rcl
      (const int npr,
       const int res,
       const double ps)
{
  RCLMOD(process_computation,set_resonant_squared_momentum)(&npr,
                                                            &res,
                                                            &ps);
}
ivoid rescale_process_rcl
      (int npr,
       const char* order,
       double A2[2])
{
  RCLMOD(process_computation,rescale_process)(&npr,
                                              order,
                                              A2,
                                              strlen(order));
}
ivoid rescale_colour_correlation_rcl
      (const int npr,
       const int i1, const int i2,
       const char* order,
       double* A2cc)
{
  RCLMOD(wrapper,wrapper_rescale_colour_correlation)(&npr,
                                                     &i1,
                                                     &i2,
                                                     order,
                                                     A2cc,
                                                     strlen(order));
}
ivoid rescale_all_colour_correlations_rcl
      (const int npr)
{
  RCLMOD(process_computation,rescale_all_colour_correlations)(&npr);
}
ivoid rescale_spin_colour_correlation_rcl
      (const int npr,
       const int i1, const int i2,
       const dcomplex v[4],
       const char* order,
       double* A2scc)
{
  RCLMOD(wrapper,wrapper_rescale_spin_colour_correlation)(&npr,
                                                          &i1,
                                                          &i2,
                                                          v,
                                                          order,
                                                          A2scc,
                                                          strlen(order));
}
ivoid rescale_spin_correlation_rcl
      (const int npr,
       const int j,
       const dcomplex v[4],
       const char* order,
       double* A2sc)
{
  RCLMOD(wrapper,wrapper_rescale_spin_correlation)(&npr,
                                                   &j,
                                                   v,
                                                   order,
                                                   A2sc,
                                                   strlen(order));
}
ivoid rescale_spin_correlation_matrix_rcl
      (const int npr,
       const int j,
       const char* order,
       double A2scm[4][4])
{
  RCLMOD(wrapper,wrapper_rescale_spin_correlation_matrix)(&npr,
                                                          &j,
                                                          order,
                                                          A2scm,
                                                          strlen(order));
}
ivoid set_tis_required_accuracy_rcl
      (const double acc)
{
  RCLMOD(process_computation,set_tis_required_accuracy)(&acc);
}
ivoid get_tis_required_accuracy_rcl
      (double* acc)
{
  RCLMOD(process_computation,get_tis_required_accuracy)(acc);
}
ivoid set_tis_critical_accuracy_rcl
      (const double acc)
{
  RCLMOD(process_computation,set_tis_critical_accuracy)(&acc);
}
ivoid get_tis_critical_accuracy_rcl
      (double* acc)
{
  RCLMOD(process_computation,get_tis_critical_accuracy)(acc);
}
ivoid get_tis_accuracy_flag_rcl
      (int* flag)
{
  RCLMOD(process_computation,get_tis_accuracy_flag)(flag);
}
ivoid get_n_colour_configurations_rcl
      (int npr,int *n)
{
  RCLMOD(process_computation,get_n_colour_configurations)(&npr,n);
}
ivoid get_colour_configuration_rcl
      (int npr,int n,int col[])
{
  int legs;
  get_legs_rcl(npr, &legs, "get_colour_configuration_rcl");
  RCLMOD(wrapper,wrapper_get_colour_configuration)(&npr,&n,col,&legs);
}
ivoid get_n_helicity_configurations_rcl
      (int npr,int *n)
{
  RCLMOD(process_computation,get_n_helicity_configurations)(&npr,n);
}
ivoid get_helicity_configuration_rcl
      (int npr,int n,int hel[])
{
  int legs;
  get_legs_rcl(npr, &legs, "get_helicity_configuration_rcl");
  RCLMOD(wrapper,wrapper_get_helicity_configuration)(&npr,&n,hel,&legs);
}

/************************
*  module wrapper_rcl  *
************************/

ivoid set_alphas_masses_nowidtharg_rcl
       (const double mc, const double mb, const double mt)
{
  RCLMOD(wrapper,set_alphas_masses_nowidtharg)(&mc,&mb,&mt);
}
ivoid use_gfermi_scheme_rcl()
{
  RCLMOD(wrapper,use_gfermi_scheme_noarg)();
}
ivoid use_gfermi_scheme_and_set_alpha_rcl
      (const double a)
{
  RCLMOD(wrapper,use_gfermi_scheme_and_set_alpha)(&a);
}
ivoid use_gfermi_scheme_and_set_gfermi_rcl
      (const double g)
{
  RCLMOD(wrapper,use_gfermi_scheme_and_set_gfermi)(&g);
}
ivoid use_alpha0_scheme_rcl()
{
  RCLMOD(wrapper,use_alpha0_scheme_noarg)();
}
ivoid use_alphaz_scheme_rcl()
{
  RCLMOD(wrapper,use_alphaz_scheme_noarg)();
}
ivoid set_gs_power_rcl
      (const int npr,
       int gsarray[][2],
       const int gslen)
{
   RCLMOD(wrapper,wrapper_set_gs_power)(&npr,gsarray,&gslen);
}
ivoid set_outgoing_momenta_rcl
     (const int npr, double pIn[][4], double p[][4])
{

  int legs;
  get_legs_rcl(npr, &legs, "set_outgoing_momenta_rcl");
  RCLMOD(wrapper,wrapper_set_outgoing_momenta)(&npr, pIn, p, &legs);
}
ivoid compute_process_rcl
            (const int npr,
             double p[][4],
             const char* order,
             double A2[2],
             int* momcheck)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_process_rcl");
  RCLMOD(wrapper,wrapper_compute_process)(&npr,
                                          p,
                                          &legs,
                                          order,
                                          A2,
                                          momcheck,
                                          strlen(order));
}
ivoid get_amplitude_r1_rcl(const int npr,
                           const int gspow,
                           const char* order,
                           int colour[],
                           int hel[],
                           dcomplex* A)
{
  int legs;
  get_legs_rcl(npr, &legs, "get_amplitude_r1_rcl");
  RCLMOD(wrapper,wrapper_get_amplitude_r1)(&npr,
                                           &gspow,
                                           order,
                                           colour,
                                           hel,
                                           &legs,
                                           A,
                                           strlen(order));
}
ivoid get_amplitude_general_rcl(const int npr,
                                int *pow,
                                const char* order,
                                int colour[],
                                int hel[],
                                dcomplex* A)
{

  int legs;
  get_legs_rcl(npr, &legs, "get_amplitude_general_rcl");
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_amplitude_general)(&npr,
                                                pow,
                                                &powlen,
                                                order,
                                                colour,
                                                hel,
                                                &legs,
                                                A,
                                                strlen(order));
}
ivoid get_squared_amplitude_r1_rcl
      (const int npr,
       const int pow,
       const char* order,
       double* A2)
{
  RCLMOD(wrapper,wrapper_get_squared_amplitude_r1)(&npr,
                                                   &pow,
                                                   order,
                                                   A2,
                                                   strlen(order));
}
ivoid get_squared_amplitude_general_rcl
            (const int npr,
             const int pow[],
             const char* order,
             double* A2)
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_squared_amplitude_general)(&npr,
                                                        pow,
                                                        &powlen,
                                                        order,
                                                        A2,
                                                        strlen(order));
}
ivoid get_polarized_squared_amplitude_r1_rcl
      (const int npr,
       const int pow,
       const char* order,
       const int hel[],
       double* A2h)
{
  int legs;
  get_legs_rcl(npr, &legs, "get_polarized_squared_amplitude_r1_rcl");
  RCLMOD(wrapper,wrapper_get_polarized_squared_amplitude_r1)(&npr,
                                                             &pow,
                                                             order,
                                                             hel,
                                                             &legs,
                                                             A2h,
                                                             strlen(order));
}
ivoid get_polarized_squared_amplitude_general_rcl
      (const int npr,
       const int pow[],
       const char* order,
       const int hel[],
       double* A2h)
{
  int legs;
  get_legs_rcl(npr, &legs, "get_polarized_squared_amplitude_general_rcl");
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_polarized_squared_amplitude_general)(&npr,
                                                                  pow,
                                                                  &powlen,
                                                                  order,
                                                                  hel,
                                                                  &legs,
                                                                  A2h,
                                                                  strlen(order));
}
ivoid compute_colour_correlation_rcl
      (const int npr,
       double p[][4],
       const int i1,
       const int i2,
       const char* order,
       double *A2cc,
       int* momcheck)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_colour_correlation_rcl");
  RCLMOD(wrapper,wrapper_compute_colour_correlation)(&npr,
                                                     p,
                                                     &legs,
                                                     &i1,
                                                     &i2,
                                                     order,
                                                     A2cc,
                                                     momcheck,
                                                     strlen(order)
                                                     );
}
ivoid compute_all_colour_correlations_rcl
      (const int npr,
       double p[][4],
       const char* order,
       int* momcheck)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_all_colour_correlations_rcl");
  RCLMOD(wrapper,wrapper_compute_all_colour_correlations)(&npr,
                                                          p,
                                                          &legs,
                                                          order,
                                                          momcheck,
                                                          strlen(order));
}
ivoid get_colour_correlation_general_rcl
      (const int npr,
       const int pow[],
       const int i1, const int i2,
       const char* order,
       double* A2cc)
{

  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_colour_correlation_general)(&npr,
                                                         pow,
                                                         &powlen,
                                                         &i1,
                                                         &i2,
                                                         order,
                                                         A2cc,
                                                         strlen(order));
}
ivoid compute_spin_colour_correlation_rcl
      (const int npr,
       double p[][4],
       const int i1, const int i2,
       const dcomplex v[4],
       const char* order,
       double* A2scc,
       int* momcheck)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_colour_correlation_rcl");
  RCLMOD(wrapper,wrapper_compute_spin_colour_correlation)(&npr,
                                                          p,
                                                          &legs,
                                                          &i1,
                                                          &i2,
                                                          v,
                                                          order,
                                                          A2scc,
                                                          momcheck,
                                                          strlen(order));
}
ivoid get_spin_colour_correlation_general_rcl
      (const int npr,
       int pow[],
       const int i1, const int i2,
       const char* order,
       double* A2scc)
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_spin_colour_correlation_general)(&npr,
                                                              pow,
                                                              &powlen,
                                                              &i1,
                                                              &i2,
                                                              order,
                                                              A2scc,
                                                              strlen(order));
}
ivoid compute_spin_correlation_rcl
      (const int npr,
       double p[][4],
       const int j,
       const dcomplex v[4],
       const char* order,
       double *A2sc,
       int* momcheck)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_rcl");
  RCLMOD(wrapper,wrapper_compute_spin_correlation)(&npr,
                                                   p,
                                                   &legs,
                                                   &j,
                                                   v,
                                                   order,
                                                   A2sc,
                                                   momcheck,
                                                   strlen(order));
}
ivoid get_spin_correlation_general_rcl
      (const int npr,
       int pow[],
       const char* order,
       double* A2sc)
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_spin_correlation_general)(&npr,
                                                       pow,
                                                       &powlen,
                                                       order,
                                                       A2sc,
                                                       strlen(order));
}
ivoid compute_spin_correlation_matrix_rcl
      (const int npr,
       double p[][4],
       const int j,
       const char* order,
       double A2scm[4][4],
       int* momcheck)
{
  int legs;
  get_legs_rcl(npr, &legs, "compute_spin_correlation_matrix_rcl");
  RCLMOD(wrapper,wrapper_compute_spin_correlation_matrix)(&npr,
                                                          p,
                                                          &legs,
                                                          &j,
                                                          order,
                                                          A2scm,
                                                          momcheck,
                                                          strlen(order));
}
ivoid get_spin_correlation_matrix_general_rcl
      (const int npr,
       int pow[],
       const char* order,
       double A2scm[4][4])
{
  int powlen;
  RCLMOD(wrapper,get_n_orders)(&powlen);
  RCLMOD(wrapper,wrapper_get_spin_correlation_matrix_general)(&npr,
                                                              pow,
                                                              &powlen,
                                                              order,
                                                              A2scm,
                                                              strlen(order));
}
ivoid get_n_orders_rcl
      (int *n_orders)
{
  RCLMOD(wrapper,get_n_orders)(n_orders);
}
ivoid get_momenta_rcl
      (const int npr,
       double p[][4])
{
  int legs;
  get_legs_rcl(npr, &legs, "get_momenta_rcl");
  RCLMOD(wrapper,wrapper_get_momenta)(&npr,p,&legs);
}
ivoid get_recola_version_rcl
      (char recolaversion[][10])
{
  int  len;
  RCLMOD(wrapper,wrapper_get_recola_version)(*recolaversion,&len,10);
}
ivoid get_modelname_rcl
      (char modelname[][100])
{
  int  len;
  RCLMOD(wrapper,wrapper_get_modelname)(*modelname,&len,100);
}
ivoid get_modelgauge_rcl
      (char modelgauge[][100])
{
  int  len;
  RCLMOD(wrapper,wrapper_get_modelgauge)(*modelgauge,&len,100);
}
ivoid get_driver_timestamp_rcl
      (char driverts[][200])
{
  int  len;
  RCLMOD(wrapper,wrapper_get_driver_timestamp)(*driverts, &len,200);
}
ivoid get_renoscheme_rcl
      (const char paramname[], char renoscheme[][100])
{
  int  len;
  RCLMOD(wrapper,wrapper_get_renoscheme)(paramname,
                                         *renoscheme,
                                         &len,
                                         strlen(paramname));
}

/*****************************************
*  module extended_higgs_interface_rcl  *
*****************************************/

ivoid set_Z2_thdm_yukawa_type_rcl
      (const int ytype)
{
  RCLMOD(extended_higgs_interface,set_z2_thdm_yukawa_type)(&ytype);
}
ivoid set_pole_mass_hl_rcl
      (const double ml,
       const double gl)
{
  RCLMOD(extended_higgs_interface,set_pole_mass_hl)(&ml,&gl);
}
ivoid set_pole_mass_hh_rcl
      (const double mh,
       const double gh)
{
  RCLMOD(extended_higgs_interface,set_pole_mass_hh)(&mh,&gh);
}
ivoid set_pole_mass_hl_hh_rcl
      (const double ml,
       const double gl,
       const double mh,
       const double gh)
{
  RCLMOD(extended_higgs_interface,set_pole_mass_hl_hh)(&ml,&gl,&mh,&gh);
}
ivoid set_pole_mass_ha_rcl
      (const double ma,
       const double ga)
{
  RCLMOD(extended_higgs_interface,set_pole_mass_ha)(&ma,&ga);
}
ivoid set_pole_mass_hc_rcl
      (const double hc,
       const double gc)
{
  RCLMOD(extended_higgs_interface,set_pole_mass_hc)(&hc,&gc);
}
ivoid set_tb_cab_rcl
      (const double tb,
       const double cab)
{
   RCLMOD(extended_higgs_interface,set_tb_cab)(&tb,&cab);
}
ivoid use_mixing_alpha_msbar_scheme_rcl
      (const char* s)
{
   RCLMOD(extended_higgs_interface,use_mixing_alpha_msbar_scheme)(s, strlen(s));
}
ivoid use_mixing_alpha_onshell_scheme_rcl
      (const char* s)
{
   RCLMOD(extended_higgs_interface,use_mixing_alpha_onshell_scheme)(s, strlen(s));
}
ivoid use_mixing_beta_onshell_scheme_rcl
      (const char* s)
{
   RCLMOD(extended_higgs_interface,use_mixing_beta_onshell_scheme)(s, strlen(s));
}
ivoid use_mixing_beta_msbar_scheme_rcl
      (const char* s)
{
   RCLMOD(extended_higgs_interface,use_mixing_beta_msbar_scheme)(s, strlen(s));
}
ivoid set_l5_rcl
      (const double l5)
{
   RCLMOD(extended_higgs_interface,set_l5)(&l5);
}
ivoid set_msb_rcl
      (const double msb)
{
   RCLMOD(extended_higgs_interface,set_msb)(&msb);
}
ivoid use_msb_msbar_scheme_rcl
      (const char* s)
{
   RCLMOD(extended_higgs_interface,use_msb_msbar_scheme)(s, strlen(s));
}
ivoid set_sa_rcl
      (const double sa)
{
  RCLMOD(extended_higgs_interface,set_sa)(&sa);
}
ivoid set_tb_rcl
      (const double tb)
{
  RCLMOD(extended_higgs_interface,set_tb)(&tb);
}
ivoid set_l3_rcl
      (const double l3)
{
  RCLMOD(extended_higgs_interface,set_l3)(&l3);
}
ivoid use_tb_msbar_scheme_rcl
      (const char* s)
{
   RCLMOD(extended_higgs_interface,use_tb_msbar_scheme)(s, strlen(s));
}
ivoid use_l3_msbar_scheme_rcl
      (const char* s)
{
   RCLMOD(extended_higgs_interface,use_l3_msbar_scheme)(s, strlen(s));
}
ivoid use_tb_onshell_scheme_rcl
      (const char* s)
{
   RCLMOD(extended_higgs_interface,use_tb_onshell_scheme)(s, strlen(s));
}
ivoid use_l3_onshell_scheme_rcl
      (const char* s)
{
   RCLMOD(extended_higgs_interface,use_l3_onshell_scheme)(s, strlen(s));
}

/***************************
*  module statistics_rcl  *
***************************/

ivoid print_generation_statistics_rcl
      ()
{
  RCLMOD(statistics,print_generation_statistics)();
}
ivoid print_TI_statistics_rcl
      (const int npr, const int npoints)
{
  RCLMOD(statistics,print_ti_statistics)(&npr, &npoints);
}
ivoid print_TC_statistics_rcl
      (const int npr, const int npoints)
{
  RCLMOD(statistics,print_tc_statistics)(&npr, &npoints);
}

/**********************
*  module reset_rcl  *
**********************/

ivoid reset_recola_rcl()
{
  RCLMOD(reset,reset_recola)();
}

#endif // RECOLA_H_
