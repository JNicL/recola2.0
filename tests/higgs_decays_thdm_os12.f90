!******************************************************************************!
!                                                                              !
!    higgs_decays_thdm_os12.f90                                                !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016,2017 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

program higgs_decays_thdm_os12

  use recola
  implicit none

  integer, parameter  :: dp = kind (23d0)
  real(dp), parameter :: pi = 3.141592653589793238462643d0
  real (dp)           :: p(0:3,1:5),A2(1:2),BORN,NLOEW,NLOQCD
  real(dp)            :: mreg,diffborn,diffnlo


  call set_output_file_rcl('*')
  call define_process_rcl(1,'Hl -> nu_mu mu+ e- nu_e~','NLO')
  call define_process_rcl(2,'Hh -> nu_mu mu+ e- nu_e~','NLO')

  call set_print_level_parameters_rcl(2)


  call use_gfermi_scheme_rcl(g=0.11663787d-04)
  call set_parameter_rcl("MW",cmplx(80.385d0,kind=dp))
  call set_parameter_rcl("MZ",cmplx(91.1876d0,kind=dp))
  call use_gfermi_scheme_rcl(g=0.116637d-04)
  
  call set_onshell_mass_w_rcl(80.385d0,0d0)
  call set_onshell_mass_z_rcl(91.1876d0,0d0)
  call set_pole_mass_hl_hh_rcl(125d0,0d0,600d0,0d0)
  call set_pole_mass_ha_rcl(690d0,0d0)
  call set_pole_mass_hc_rcl(690d0,0d0)
  call set_pole_mass_top_rcl(172.5d0,0d0)

  mreg = 1d-3
  call set_pole_mass_electron_rcl(mreg)
  call set_pole_mass_muon_rcl    (mreg,0d0)
  call set_pole_mass_tau_rcl     (mreg,0d0)
  call set_pole_mass_up_rcl      (mreg)
  call set_pole_mass_down_rcl    (mreg)
  call set_pole_mass_charm_rcl   (mreg,0d0)
  call set_pole_mass_strange_rcl (mreg)
  call set_pole_mass_bottom_rcl  (mreg,0d0)

  call set_mu_uv_rcl(125d0)
  call set_mu_ir_rcl(1d0)
  call set_delta_ir_rcl(0d0, pi**2/6d0)
  call set_masscut_rcl(6d0)

  call set_renoscheme_rcl('da_QED2', 'OS2')
  call set_renoscheme_rcl('db_QED2', 'OS12')
  call set_mu_ms_rcl(361d0)
  call set_tb_cab_rcl(4.5d0, 0.1d0)
  call set_l5_rcl(-1.9d0)



  call generate_processes_rcl

  p(:,1)=[125d0,0d0,0d0,0d0]
  p(:,2)=[ 0.57695854123977D+01,-0.24196655385644D+00,-0.54033967314394D+01,-0.20082011304738D+01]
  p(:,3)=[ 0.78957242310389D+01,-0.18748632795730D+01, 0.67319589540452D-01,-0.76696034374919D+01]
  p(:,4)=[ 0.58118202734874D+02, 0.30687496740888D+02, 0.49349161929827D+02, 0.81440145724436D+00]
  p(:,5)=[ 0.53216487621690D+02,-0.28570666907459D+02,-0.44013084787928D+02, 0.88634031107214D+01]

  BORN = 0.38365235216875D-06
  NLOEW = 0.85604771747089D-07

  call compute_process_rcl(1,p,'NLO', A2)

  diffborn = abs(A2(1)-BORN)/abs(BORN+A2(1))*2
  diffnlo = abs(A2(2)-NLOEW)/abs(NLOEW+A2(2))*2

  write(*,*) 'Hl -> nu_mu mu+ e- nu_e~'
  write(*,*) 'OS T2 (alpha) OS T1T2 (beta) renormalization'
  write(*,'(1x,a,e21.14)') "Recola2@LO:  ", A2(1)
  write(*,'(1x,a,e21.14)') "Prophecy@LO: ", BORN
  write(*,'(1x,a,i2)') "Digits:      ", -int(log(diffborn)/log(10d0))
  write(*,'(1x,a,e21.14)') "Recola2@NLO: ", A2(2)
  write(*,'(1x,a,e21.14)') "Prophecy@NLO:", NLOEW
  write(*,'(1x,a,i2)') "Digits:      ", -int(log(diffnlo)/log(10d0))
  if (diffborn .gt. 1d-12 .or. diffnlo .gt. 1d-8) then
    stop 9
  end if


  p(:,1)=[600d0,0d0,0d0,0d0]
  p(:,2)=[ 0.27694009979509D+02,-0.11614394585109D+01,-0.25936304310909D+02,-0.96393654262744D+01]
  p(:,3)=[ 0.37899476308987D+02,-0.89993437419502D+01, 0.32313402979416D+00,-0.36814096499961D+02]
  p(:,4)=[ 0.27896737312739D+03, 0.14729998435626D+03, 0.23687597726317D+03, 0.39091269947729D+01]
  p(:,5)=[ 0.25543914058411D+03,-0.13713920115580D+03,-0.21126280698205D+03, 0.42544334931463D+02]


  BORN = 0.14459616072455D-08
  NLOEW = 0.48315349976673D-09

  call compute_process_rcl(2,p,'NLO',A2)
  diffborn = abs(A2(1)-BORN)/abs(BORN+A2(1))*2
  diffnlo = abs(A2(2)-NLOEW)/abs(NLOEW+A2(2))*2

  write(*,*)
  write(*,*) 'Hh -> nu_mu mu+ e- nu_e~'
  write(*,*) 'OS T2 (alpha) OS T1T2 (beta) renormalization'
  write(*,'(1x,a,e21.14)') "Recola2@LO:  ", A2(1)
  write(*,'(1x,a,e21.14)') "Prophecy@LO: ", BORN
  write(*,'(1x,a,i2)') "Digits:      ", -int(log(diffborn)/log(10d0))
  write(*,'(1x,a,e21.14)') "Recola2@NLO: ", A2(2)
  write(*,'(1x,a,e21.14)') "Prophecy@NLO:", NLOEW
  write(*,'(1x,a,i2)') "Digits:      ", -int(log(diffnlo)/log(10d0))
  if (diffborn .gt. 1d-11 .or. diffnlo .gt. 1d-8) then
    stop 9
  end if

!------------------------------------------------------------------------------!

end program higgs_decays_thdm_os12

!------------------------------------------------------------------------------!
