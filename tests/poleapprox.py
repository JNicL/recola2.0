#!/usr/bin/env python
from __future__ import print_function

import sys
sys.path.append('..')
from pyrecola import *
from math import pi

from test_tools import check_value


set_pole_mass_z_rcl (91.153480619183,2.4942663787728)
set_pole_mass_w_rcl (80.357973609878,2.0842989982782)
set_pole_mass_top_rcl (173.34,1.36918)
set_pole_mass_h_rcl (125.900,0)
set_pole_mass_bottom_rcl (0,0)
set_light_fermions_rcl(1E-3)
use_gfermi_scheme_rcl(a=0.75553105223690E-02)

set_delta_ir_rcl(0,0)

set_mu_uv_rcl (173.34)
set_mu_ir_rcl (173.34)
set_alphas_rcl (0.10758313068882,173.34,5)


set_print_level_squared_amplitude_rcl (1)


define_process_rcl(1,'g g -> t(e+ nu_e b) t~(mu- nu_mu~ b~) H','NLO')

set_resonant_particle_rcl('t')
set_resonant_particle_rcl('t~')

unselect_all_gs_powers_BornAmpl_rcl(1)
select_gs_power_BornAmpl_rcl(1,2)
unselect_all_gs_powers_LoopAmpl_rcl(1)
select_gs_power_LoopAmpl_rcl(1,2)

generate_processes_rcl()


p1 = [1658.6042064580165,0.0000000000000000,0.0000000000000000,1658.6042064580165]
p2 = [93.167391792675502,0.0000000000000000,0.0000000000000000,-93.167391792675502]
p3 = [290.23203995788782,0.21869328541838526,87.165794239673843,276.83336776106086]
p4 = [250.51659532810453,51.628356024538881,122.57366359199837,212.29407524145455]
p5 = [82.153984919510563,-33.946760734409288,60.085139806118882,44.572083732703945]
p6 = [96.964294188537764,-29.489914748098268,-2.2283141261186357,92.344214176019378]
p7 = [281.86645248355819,-62.627197237084872,-139.32428266727388,236.88662997548596]
p8 = [431.94277986559786,1.4436286863595815,-93.060576824025731,421.79640830075340]
p9 = [318.09545150749534,72.773194723275566,-35.211424020372817,280.71003547786290]

p = [p1,p2,p3,p4,p5,p6,p7,p8,p9]
compute_process_rcl(1,p,'NLO')
m = get_squared_amplitude_rcl(1,'LO',als=2)
thres = 13
check_value(6.5897430522232609E-010, m, thres)
thres = 10
m = get_squared_amplitude_rcl(1,'NLO',als=2)
check_value(1.1729101810262603E-011, m, thres)

reset_recola_rcl()
