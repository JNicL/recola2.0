#!/usr/bin/env python
from __future__ import print_function
import sys
sys.path.append('..')

# Import the Recola library (pyrecola) as follows:
import pyrecola
from test_tools import check_value

pyrecola.set_output_file_rcl('*')

pyrecola.set_pole_mass_z_rcl (91.1534806191828,2.49426637877282)
pyrecola.set_pole_mass_w_rcl (80.3579736098775,2.08429899827822)
pyrecola.set_pole_mass_top_rcl (173.,0.)
pyrecola.set_pole_mass_h_rcl (125.0,0.)
pyrecola.set_pole_mass_bottom_rcl (0.,0.)
pyrecola.set_light_fermions_rcl(1E-3)
pyrecola.use_gfermi_scheme_rcl(a=0.00755525416742918)
from math import pi
pyrecola.set_delta_ir_rcl(0.,pi**2/6.)
pyrecola.set_mu_uv_rcl (91.1534806191828)
pyrecola.set_mu_ir_rcl (91.1534806191828)
pyrecola.set_print_level_squared_amplitude_rcl (2)
pyrecola.set_dynamic_settings_rcl(1)
pyrecola.set_compute_ir_poles_rcl(1)
pyrecola.set_alphas_rcl(1.1799999999999999E-01, 100., 5)

pyrecola.define_process_rcl(1,'u u~ -> e+ e- mu+ mu-','NLO')

pyrecola.unselect_all_gs_powers_BornAmpl_rcl(1)
pyrecola.select_gs_power_BornAmpl_rcl(1,0)
pyrecola.unselect_all_gs_powers_LoopAmpl_rcl(1)
pyrecola.select_gs_power_LoopAmpl_rcl(1,0)

pyrecola.generate_processes_rcl()

def psptest(n):
  """ Reference values from 1803.07977 (comparison against OpenLoops).  """
  thres = 8
  if n == 1:
    p1=[500.00000000000000, 0.0000000000000000, 0.0000000000000000, 500.0000000000000]
    p2=[500.00000000000000, 0.0000000000000000, 0.0000000000000000,-500.0000000000000]
    p3=[88.551333054503004,-22.100690287690000, 40.080353191685298,-75.80543095693660]
    p4=[328.32941922709801,-103.84961188345601,-301.93375538954001, 76.49492138716590]
    p5=[152.35810946743101,-105.88095966659201,-97.709638326975707, 49.54838522679280]
    p6=[430.76113825096797, 231.83126183773800, 359.56304052483000,-50.23787565702210]
    expected_fin = 6.60088670210145E-16
    expected_ep1 = 2.63915540078563E-17
    expected_ep2 = -3.09566543905505E-17

  elif n == 2:
    p1=[5.00000000000000E+02, 0.00000000000000E+00,  0.00000000000000E+00,  5.00000000000000E+02]
    p2=[5.00000000000000E+02, 0.00000000000000E+00,  0.00000000000000E+00, -5.00000000000000E+02]
    p3=[1.17747708137171E+02, -6.07059218463615E+01, 7.12310458623266E+01,  7.14524452324688E+01]
    p4=[3.50954173077969E+02, -3.17881667026108E+01, 8.39394286931734E+01,  3.39282354933456E+02]
    p5=[3.49332228573790E+02, 1.84009303995763E+02, -5.15277979392370E+01, -2.92435408257719E+02]
    p6=[1.81965890211070E+02, -9.15152154467912E+01,-1.03642676616263E+02, -1.18299391908206E+02]
    expected_fin = -3.10783717792216E-13
    expected_ep1 = 2.47558966660688E-14
    expected_ep2 = -1.61109736655762E-14

  elif n == 3:
    p1=[5.00000000000000E+02, 0.00000000000000E+00, 0.00000000000000E+00, 5.00000000000000E+02]
    p2=[5.00000000000000E+02, 0.00000000000000E+00, 0.00000000000000E+00,-5.00000000000000E+02]
    p3=[2.71200874120175E+02,-3.73192604194911E+01, 3.27628636758588E+01,-2.66615419075953E+02]
    p4=[2.28710119031214E+02, 3.01476738097433E+01,-2.76873495467817E+01,-2.25017437071458E+02]
    p5=[3.68223563778399E+02,-3.45262681797353E+01,-6.56193561878981E+00, 3.66542590606031E+02]
    p6=[1.31865443070211E+02, 4.16978547894831E+01, 1.48642148971272E+00, 1.25090265541380E+02]
    expected_fin = -4.77231273844357E-6
    expected_ep1 = -9.28399415556438E-7
    expected_ep2 = -7.16650990014111E-7

  p = [p1, p2, p3, p4, p5, p6]

  pyrecola.compute_process_rcl(1,p,'NLO')
  fin = pyrecola.get_squared_amplitude_rcl(1,'NLO',als=0)
  ep1 = pyrecola.get_squared_amplitude_rcl(1,'NLO-IR1',als=0)
  ep2 = pyrecola.get_squared_amplitude_rcl(1,'NLO-IR2',als=0)

  check_value(fin, expected_fin, thres)
  check_value(ep1, expected_ep1, thres)
  check_value(ep2, expected_ep2, thres)

psptest(1)
psptest(2)
psptest(3)

pyrecola.reset_recola_rcl()
