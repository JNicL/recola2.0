from __future__ import print_function
import sys
from math import log10


def check_value(ref, cmp, thres, verbose=False):
  """ Compare the doule precision value of ref with cmp.
  :thres: number of accurate digits required for the test to pass.
  """
  if abs(ref) != 0.:
    acc = abs(ref - cmp) / abs(ref)
  elif abs(cmp) != 0.:
    acc = abs(ref - cmp) / abs(cmp)
  else:
    # both values 0
    acc = 0.

  if acc != 0.:
    acc = -int(log10(acc))
  else:
    acc = 17

  if acc < thres:
    print("Value " + str(cmp) + " does not match " + str(ref) + ".")
    print("acc, thres:", acc, thres)
    sys.exit(1)
  elif verbose:
    print("Success in comparing " + str(cmp) + " with " + str(ref) + ".")
    print("acc, thres:", acc, thres)

