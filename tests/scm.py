#!/usr/bin/env python
import sys
sys.path.append('..')
from pyrecola import *
from test_tools import check_value
from math import pi
set_colour_optimization_rcl(0)
set_delta_ir_rcl(0,pi**2/6)
do_rescale = True
define_process_rcl(1, 'u u~ -> g Z [+]', 'LO')
set_pole_mass_w_rcl(80.357971191406250,0.)
set_pole_mass_z_rcl(91.153480529785156,0.)
set_pole_mass_top_rcl(172.,0.)
set_alphas_rcl(0.12580868569239670,91.153480529785156,5)
set_mu_uv_rcl(91.153480529785156)
set_mu_ir_rcl(91.153480529785156)
use_gfermi_scheme_rcl(a=7.5552553808086852E-003)
if do_rescale:
  set_alphas_rcl(0.1, 91.153480529785156, 5)
else:
  set_alphas_rcl(0.2, 91.153480529785156/2, 5)

set_dynamic_settings_rcl(1)

generate_processes_rcl()
p1=[500.00000000000000, 0.0000000000000000, 0.0000000000000000, 500.00000000000000]
p2=[500.00000000000000, 0.0000000000000000, 0.0000000000000000,-500.00000000000000]
p3=[495.84552149365317, 301.75334487222250, 163.04327753574904, 358.08461247886515]
p4=[504.15447850634706,-301.75334487222250,-163.04327753574901,-358.08461247886515]
psp = [p1,p2,p3,p4]

r = compute_process_rcl(1,psp,'LO')
if do_rescale:
  set_alphas_rcl(0.2, 91.153480529785156/2, 5)
  rescale_process_rcl(1,'LO')
r = get_squared_amplitude_rcl(1,'LO',als=1)
r = rescale_spin_correlation_matrix_rcl(1,3,order='LO')
mm = sum(r[i][i] for i in range(4))
check_value(0.47897074703027687, mm, 15)
r = get_spin_correlation_matrix_rcl(1,order='LO',als=1)
mm = sum(r[i][i] for i in range(4))
check_value(0.47897074703027687, mm, 15)

reset_recola_rcl()
