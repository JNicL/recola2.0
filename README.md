# RECOLA2 README #

* Version 2.2.1'

* * *

**Recola2** is a *Fortran95* computer program for the *automated* generation and *numerical* evaluation of amplitudes in *general* quantum field theories at *one-loop* order. It has been built on top of [Recola][1] and comes with the same features and additional ones.

## Installation

### External dependencies

+ [Collier][2] tensor integral library
+ [Recola2 model files][1]
+ [CMake][3] build system

* * *

### Summary of set up

This *Recola2* package is distributed as a standalone version and requires the user to resolve dependencies to *Collier* and the model files by hand.  In the following it is assumed that the *Collier* library is built as shared libraries and ready to get linked to *Recola2*. The compilation of *Recola2* consists of the following steps:

1. Configuration of a model file with `cmake`
2. Compilation of a model file with `make`
3. Configuration of *Recola2* with `cmake`
4. Compilation of *Recola2* with `make`

* * *

### **QUICK** configuration and installation of model files

Obtain a model file from the website <http://recola.hepforge.org/> and extract the tarball via the shell command

    tar -zxvf <model>-X.Y.Z.tar.gz

where \<model\> can be SM, SM_BFM, HS, HS_BFM, THDM, THDM_BFM and X.Y.Z is the current **Recola2** version. Then, switch to the build directory and run

    cd modelfile-X.Y.Z/build
    cmake [options] .. -Dcollier_path=<path_to_collier>
    make [options]

where \<path_to_collier\> points to the directory including the compiled *Collier* library. It is highly advised to run the compilation of model files parallelized

    make -j <THREADS>

with \<THREADS\> being the number of parallel compilation units.

* * *

### **QUICK** configuration and installation of **Recola2**

Obtain a copy of *Recola2* from <http://recola.hepforge.org/> and extract it via the shell command

    tar -zxvf recola2-X.Y.Z.tar.gz

where X.Y.Z is the current *Recola2* version. Then, switch to the build directory and run

    cd recola2-X.Y.Z/build
    cmake [options] .. -Dcollier_path=<path_to_collier> -Dmodelfile_path=<path_to_modelfile>
    make [options]

where \<path_to_collier\> and \<path_to_modelfile\> point to the directories containing the compiled *Collier* and model file library, respectively.

* * *

### Running demo files

The [Recola2][1] demo files are located in:
  
    recola2-X.Y.Z/demos

The Fortran and C++ demo files can be run by invoking the run script

    ./run <demofile>

where <demofile> can be demo{i}_rcl, cdemo{i}_rcl (i=0,..5). The python demo files can be run directly

    python <demofile>

with <demofile> taking the values pydemo{i}_rcl.py (i=0,..5).

* * *

### Summary of CMake and Makefile options 

|      CMake option      | Value         | Short description  |
| :--------------------: | :------------:|:------------------ |
| collier path           | Path          | Absolute or relative path to the *Collier* library. |
| modelfile path         | Path          | Absolute or relative path to the *Recola2* model file. Only available in *Recola2* CMakeLists.txt. |
| static                 | On/Off        | Compile the library as a shared or static library. |
| with_python3           | On/Off        | Choose Python 3.x over Python 2.7 to compile *pyrecola*. Only available in *Recola2* CMakeLists.txt. |
| with_smtests           | On/Off        | Run tests against *Pole* and *OpenLoops*. Only available in *Recola2* CMakeLists.txt. |
| CMAKE_BUILD_TYPE       | Debug/Release | Set the compiler flags. By default Release flags (optimized) are used. |
| CMAKE_Fortran_COMPILER | Path/Name     | Set the Fortran compiler either via executable name or the absolute path to executable. |
| CMAKE_INSTALL_PREFIX   | Path          | Set the installation prefix. |

|  Makefile option | Value      | Short description  |
| :--------------: | :---------:|:------------------ |
| -j               | Integer    | Number of threads for compilation. |
| VERBOSE          | True/False | Enable *verbose* compilation. In this mode all compilation flags are visible to the user. |

* * *

### Running testsuite 

The automated tests in the SM can be enabled via

    cmake [options] .. -Dwith_smtests=On -Dcollier_path=<path_to_collier> -Dmodelfile_path=<path_to_modelfile>

where \<path_to_modelfile\> should point to a SM model file. Then, after compilation, run the tests with

    make check

* * *

## General instruction when using Recola2 in a Fortran/C++/Python program

In order to use Recola2 its modules have to be loaded:

+ Fortran: `use recola`

+ C++: `#include "recola.hpp"`

+ Python: `import pyrecola`

in the preamble of the respective code, and the library `librecola.so`(or `librecola.a`, `librecola.dynlib`, `pyrecola.so`) has to be supplied to the linker. This gives access to the public functions and subroutines of the Recola2 library. The names of all these routines end with the suffix `_rcl`. 

### Contact ###

  - Jean-Nicolas Lang <jlang@physik.uzh.ch>
  - Sandro Uccirati <uccirati@to.infn.it>
  - Ansgar Denner <denner@physik.uni-wuerzburg.de>

* * *

[1]: http://recola.hepforge.org/          "recola@hepforge"
[2]: http://collier.hepforge.org/         "collier@hepforge"
[3]: https://cmake.org/                   "CMake"
