!******************************************************************************!
!                                                                              !
!    demo0_rcl.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016,2017 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!
! PARTICLES                                                                    !
! Scalars in the SM:    'H', 'G0', 'G+', 'G-'                                  !
! Scalars in the HSESM: 'Hl', 'Hh', 'G0', 'G+', 'G-'                           !
! Scalars in the THDM:  'Hl', 'Hh', 'Ha', 'H+', 'H-', 'G0', 'G+', 'G-'         !
! Vector bosons:        'g', 'A', 'Z', 'W+', 'W-'                              !
! leptons:              'nu_e', 'nu_e~', 'e-', 'e+',                           !
!                       'nu_mu', 'nu_mu~', 'mu-', 'mu+',                       !
!                       'nu_tau', 'nu_tau~', 'tau-', 'tau+'                    !
! quarks:               'u', 'u~', 'd', 'd~',                                  !
!                       'c', 'c~', 's', 's~',                                  !
!                       't', 't~', 'b', 'b~'                                   !
!******************************************************************************!

program main_rcl

! Each program, which uses RECOLA must have:
  use recola

  implicit none

! Variables for this demo file
  integer, parameter :: dp = kind (23d0) ! double precision
  real (dp)          :: p(0:3,1:6)
  character(len=100) :: modelname

!------------------------------------------------------------------------------!
! Step 1                                                                       !
!------------------------------------------------------------------------------!
! Set input values for the computation.                                        !
! General methods for arbitrary models are defined in "input_rcl.f90",         !
! whereas model specific ones are defined in "recola1_interface_rcl.90".       !
! Methods dedicated to the THDM/HSESM are listed in                            !
! "extended_higgs_interface_rcl.f90".                                          !
! Since all variables have default values, this step is optional.              !
!------------------------------------------------------------------------------!

! The standard output is selected
  call set_output_file_rcl('*')

! Let's print the squared amplitude
  call set_print_level_squared_amplitude_rcl(1)

!------------------------------------------------------------------------------!
! Step 2                                                                       !
!------------------------------------------------------------------------------!
! In step 2 processes to be computed are defined. Optionally, specific powers  !
! in fundamental couplings, or polarizations can be enforced using methods     !
! defined in the modules "process_definition_rcl" and "recola1_interface_rcl". !
! The processes are defined by calling "define_process_rcl" which requires a   !
! unique process number, a process string and the order of computation. Note   !
! that by default all powers in fundamental couplings are selected.            !
!------------------------------------------------------------------------------!

! We define a process at NLO:
  call define_process_rcl(1,'u u~ -> g g tau+ tau-','NLO')

!------------------------------------------------------------------------------!
! Step 3                                                                       !
!------------------------------------------------------------------------------!
! The skeleton of the recursive procedure is built for all defined             !
! processes, by calling the subroutine "generate_processes_rcl".               !
!------------------------------------------------------------------------------!

  call generate_processes_rcl

!------------------------------------------------------------------------------!
! Step 4                                                                       !
!------------------------------------------------------------------------------!
! The fourth step is the actual computation of processes.                      !
! Each process defined at step 2 can be computed at this stage for given       !
! phase-space points provided by the user.                                     !
! The computation of (squared) amplitudes is carried out by calling the        !
! subroutine "compute_process_rcl".                                            !
! In the module "process_computation_rcl" other useful                         !
! methods are defined, which allow to get the value for specific               !
! contributions to amplitudes, squared amplitudes, and                         !
! Born colour- and/or spin-correlated squared amplitudes.                      !
!------------------------------------------------------------------------------!

! Momenta of the phase-space point.
  p(:,1) = [4000.0000000000d0,    0.0000000000d0,    0.0000000000d0, 4000.0000000000d0]
  p(:,2) = [4000.0000000000d0,    0.0000000000d0,    0.0000000000d0,-4000.0000000000d0]
  p(:,3) = [2387.4445571379d0,-2131.7219821216d0,  677.6712380335d0, -834.5145879427d0]
  p(:,4) = [2084.0108209587d0, 1206.0274745508d0, 1266.0449626178d0,-1133.8999008430d0]
  p(:,5) = [1954.1326742459d0, -173.3442838631d0, -836.2617619034d0, 1757.6269608155d0]
  p(:,6) = [1574.4119476575d0, 1099.0387914340d0,-1107.4544387478d0,  210.7875279701d0]

  call compute_process_rcl(1,p,'NLO')

!------------------------------------------------------------------------------!
! Step 5                                                                       !
!------------------------------------------------------------------------------!
! Calling reset_recola_rcl (module reset_rcl), deallocates all processes       !
! generated in the previous steps and allows for the next call of Recola.      !
!------------------------------------------------------------------------------!

  call reset_recola_rcl

!------------------------------------------------------------------------------!

end program main_rcl

!------------------------------------------------------------------------------!
