!******************************************************************************!
!                                                                              !
!    demo2_rcl.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016,2017 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!
! PARTICLES                                                                    !
! Scalars in the SM:    'H', 'G0', 'G+', 'G-'                                  !
! Scalars in the HSESM: 'Hl', 'Hh', 'G0', 'G+', 'G-'                           !
! Scalars in the THDM:  'Hl', 'Hh', 'Ha', 'H+', 'H-', 'G0', 'G+', 'G-'         !
! Vector bosons:        'g', 'A', 'Z', 'W+', 'W-'                              !
! leptons:              'nu_e', 'nu_e~', 'e-', 'e+',                           !
!                       'nu_mu', 'nu_mu~', 'mu-', 'mu+',                       !
!                       'nu_tau', 'nu_tau~', 'tau-', 'tau+'                    !
! quarks:               'u', 'u~', 'd', 'd~',                                  !
!                       'c', 'c~', 's', 's~',                                  !
!                       't', 't~', 'b', 'b~'                                   !
!******************************************************************************!

program main_rcl

! Each program, which uses RECOLA must have:
  use recola

  implicit none

! Variables for this demo file
  integer,  parameter :: dp = kind (23d0) ! double precision
  real(dp)            :: pOff(0:3,1:6),pOn(0:3,1:6),Minv2,Minv

!------------------------------------------------------------------------------!
! Step 1                                                                       !
!------------------------------------------------------------------------------!
! Set input values for the computation.                                        !
! General methods for arbitrary models are defined in "input_rcl.f90",         !
! whereas model specific ones are defined in "recola1_interface_rcl.90".       !
! Methods dedicated to the THDM/HSESM are listed in                            !
! "extended_higgs_interface_rcl.f90".                                          !
! Since all variables have default values, this step is optional.              !
!------------------------------------------------------------------------------!

! Let's print all input/derived parameters
  call set_print_level_parameters_rcl(1)
! Let's print the squared amplitude
  call set_print_level_squared_amplitude_rcl(2)

!------------------------------------------------------------------------------!
! Step 2                                                                       !
!------------------------------------------------------------------------------!
! In step 2 processes to be computed are defined. Optionally, specific powers  !
! in fundamental couplings, or polarizations can be enforced using methods     !
! defined in the modules "process_definition_rcl" and "recola1_interface_rcl". !
! The processes are defined by calling "define_process_rcl" which requires a   !
! unique process number, a process string and the order of computation. Note   !
! that by default all powers in fundamental couplings are selected.            !
!------------------------------------------------------------------------------!

! Process 1 is defined:
! Scattering of 2 up quarks producing 2 up quarks and a Z boson. 
! The Z boson decays in a pair of left-handed positron-electron.
  call define_process_rcl(1,'u u -> u u Z(e+[+] e-[-])','NLO')

! We set the Z boson resonant, which puts its width to zero except in the
! denominator of the resonant propagator. 
  call set_pole_mass_z_rcl(91.153480619179192d0,2.4942663787728243d0);
  call set_resonant_particle_rcl('Z')

! The W boson should be set resonant too, or its width should be set to zero.
  call set_resonant_particle_rcl('W+')
! If this is not done, gauge dependencies in the CMS approximated mass
! counterterm get pronounced which can be checked by comparing against the BFM

!------------------------------------------------------------------------------!
! Step 3                                                                       !
!------------------------------------------------------------------------------!
! The skeleton of the recursive procedure is built for all defined             !
! processes, by calling the subroutine "generate_processes_rcl".               !
!------------------------------------------------------------------------------!

  call generate_processes_rcl

!------------------------------------------------------------------------------!
! Step 4                                                                       !
!------------------------------------------------------------------------------!
! The fourth step is the actual computation of processes.                      !
! Each process defined at step 2 can be computed at this stage for given       !
! phase-space points provided by the user.                                     !
! The computation of (squared) amplitudes is carried out by calling the        !
! subroutine "compute_process_rcl".                                            !
! In the module "process_computation_rcl" other useful                         !
! methods are defined, which allow to get the value for specific               !
! contributions to amplitudes, squared amplitudes, and                         !
! Born colour- and/or spin-correlated squared amplitudes.                      !
!------------------------------------------------------------------------------!

! In order to compute resonant contributions in pole approximation, 
! usually one starts with momenta where the resonant particle is 
! off-shell. 

! In this example we start with the following phase-space point
  pOff(:,1) = [ 274.630377518d0,    0.000000000d0,    0.000000000d0,  274.630377518d0]
  pOff(:,2) = [5048.357339139d0,    0.000000000d0,    0.000000000d0,-5048.357339139d0]
  pOff(:,3) = [ 840.453572436d0, -563.109454008d0,  393.074828591d0, -484.522578788d0]
  pOff(:,4) = [3147.898045830d0,  -59.702953318d0, -120.809411219d0,-3145.012360940d0]
  pOff(:,5) = [1219.012902019d0,  592.691410200d0, -258.284406057d0,-1033.440135252d0]
  pOff(:,6) = [ 115.623196373d0,   30.120997125d0,  -13.981011315d0, -110.751886639d0]

  Minv2 = (pOff(0,5)+pOff(0,6))**2 - sum((pOff(1:3,5)+pOff(1:3,6))**2)
  Minv = sqrt(Minv2)

! Particles 5 and 6 are the e+ and e- respectively. The e+ and e- 
! system has invariant mass Minv = 100.27153111734418d0 (the resonant 
! Z boson is off-shell).

! In order to get the pole approximation, a shift of the momenta of 
! the outgoing particles is needed to get a new "on-shell" invariant 
! mass Minv = Mz = 91.153480619179192d0. There is not a unique recipe 
! for the shift; requiring that (together with the mass conditions for 
! the outgoing particles and the four-momentum conservation)
! pOn(:,3) = b*pOff(:,3)
! pOn(:,4) = b*pOff(:,4)
! pOn(:,5) = a*pOff(:,5)
! and getting the value of "b" closer to 1, one gets

  pOn(:,1) = [ 274.630377518d0,    0.000000000d0,    0.000000000d0,  274.630377518d0]
  pOn(:,2) = [5048.357339139d0,    0.000000000d0,    0.000000000d0,-5048.357339139d0]
  pOn(:,3) = [ 840.903134346d0, -563.410663462d0,  393.285085858d0, -484.781751815d0]
  pOn(:,4) = [3149.581868833d0,  -59.734888661d0, -120.874032647d0,-3146.694640379d0]
  pOn(:,5) = [1206.411477651d0,  586.564521824d0, -255.614416754d0,-1022.757050864d0]
  pOn(:,6) = [ 126.091235828d0,   36.581030299d0,  -16.796636457d0, -119.493518563d0]

! The resonant amplitude is then computed with the momenta pOn, 
! where the squared momentum of the resonant particle is set to 
! Minv2
  call set_resonant_squared_momentum_rcl(1,1,Minv2)

  call compute_process_rcl(1,pOn,'NLO')

!------------------------------------------------------------------------------!
! Step 5                                                                       !
!------------------------------------------------------------------------------!
! Calling reset_recola_rcl (module reset_rcl), deallocates all processes       !
! generated in the previous steps and allows for the next call of Recola.      !
!------------------------------------------------------------------------------!

  call reset_recola_rcl

!------------------------------------------------------------------------------!
  
end program main_rcl

!------------------------------------------------------------------------------!
