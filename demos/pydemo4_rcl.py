#==============================================================================#
#                                                                              #
#    pydemo4_rcl.py                                                            #
#    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        #
#                                                                              #
#    Copyright (C) 2016,2017 Ansgar Denner, Jean-Nicolas Lang and              #
#                            Sandro Uccirati                                   #
#                                                                              #
#    RECOLA2 is licenced under the GNU GPL version 3,                          #
#    see COPYING for details.                                                  #
#                                                                              #
#==============================================================================#
# PARTICLES                                                                    #
# Scalars in the SM:    'H', 'G0', 'G+', 'G-'                                  #
# Scalars in the HSESM: 'Hl', 'Hh', 'G0', 'G+', 'G-'                           #
# Scalars in the THDM:  'Hl', 'Hh', 'Ha', 'H+', 'H-', 'G0', 'G+', 'G-'         #
# Vector bosons:        'g', 'A', 'Z', 'W+', 'W-'                              #
# leptons:              'nu_e', 'nu_e~', 'e-', 'e+',                           #
#                       'nu_mu', 'nu_mu~', 'mu-', 'mu+',                       #
#                       'nu_tau', 'nu_tau~', 'tau-', 'tau+'                    #
# quarks:               'u', 'u~', 'd', 'd~',                                  #
#                       'c', 'c~', 's', 's~',                                  #
#                       't', 't~', 'b', 'b~'                                   #
#==============================================================================#
import sys
sys.path.append('..')

# Import the Recola library (pyrecola) as follows:
from pyrecola import *

#------------------------------------------------------------------------------#
# Step 1                                                                       #
#------------------------------------------------------------------------------#
# Set input values for the computation.                                        #
# General methods for arbitrary models are defined in "input_rcl.f90",         #
# whereas model specific ones are defined in "recola1_interface_rcl.90".       #
# Methods dedicated to the THDM/HSESM are listed in                            #
# "extended_higgs_interface_rcl.f90".                                          #
# Since all variables have default values, this step is optional.              #
#------------------------------------------------------------------------------#

# Retrieve active model
model = get_modelname_rcl()

# Print everything on the screen
set_output_file_rcl('*')

# Let's print the input parameters + counterterms
set_print_level_parameters_rcl(2)

# We set the muUV scale. Change this scale to check the UV finiteness.
set_mu_uv_rcl(100.)

# We set the muMS scale. Change this scale to probe the scale uncertainty.
set_mu_ms_rcl(100.)

# We set the muIR scale. Change this scale to probe the IR scale dependence.
set_mu_ir_rcl(100.)

# We print the results for all squared amplitudes
set_print_level_squared_amplitude_rcl(1)

# In this demo file we consider all kinds of on-shell decays.
# For this reason, we set the pole masses of the W/Z and Higgs bosons to
# real-valued ones.
set_pole_mass_w_rcl(80.357974, 0.)
set_pole_mass_z_rcl(91.153481, 0.)

if model == 'THDM':
  set_pole_mass_hl_hh_rcl(125., 0., 400., 0.)
  set_pole_mass_ha_rcl(300., 0.)
  set_pole_mass_hc_rcl(333., 0.)
  # We choose Yukawa Type II
  set_Z2_thdm_yukawa_type_rcl(2)
elif model == 'HS':
  set_pole_mass_hl_hh_rcl(125., 0., 400., 0.)
elif model == 'SM':
  set_pole_mass_h_rcl(125., 0.)

# Depending on the active model file, we define values for (mixing)
# angles and fix their renormalization scheme
if model == 'THDM':
  # Set mixing angles tb=tan(beta), cab = cos(alpha-beta)
  from math import sqrt
  set_tb_cab_rcl(2.2, sqrt(1. - 0.979796 * 0.979796))

  # Set soft breaking scale
  set_msb_rcl(320.)

  # all the msbar schemes for alpha (mixing angle) in the THDM
  #call use_mixing_alpha_msbar_scheme_rcl('FJTS')
  use_mixing_alpha_msbar_scheme_rcl('l3')
  #call use_mixing_alpha_msbar_scheme_rcl('l345')
  #call use_mixing_alpha_msbar_scheme_rcl('MDTS'):
  #call use_mixing_alpha_msbar_scheme_rcl('MTS')

  # all on-shell schemes for alpha (mixing angle) in the THDM
  #use_mixing_alpha_onshell_scheme_rcl('os')
  #use_mixing_alpha_onshell_scheme_rcl('ps')

  # all the msbar beta renormalization schemes in the THDM
  #use_mixing_beta_msbar_scheme_rcl('FJTS')
  #use_mixing_beta_msbar_scheme_rcl('MDTS')

  # all the on-shell beta renormalization schemes in the THDM
  use_mixing_beta_onshell_scheme_rcl('ps1')
  #use_mixing_beta_onshell_scheme_rcl('ps2')
  #use_mixing_beta_onshell_scheme_rcl('os1')
  #use_mixing_beta_onshell_scheme_rcl('os2')

  # all the msbar MSB renormalization schemes in the THDM
  use_msb_msbar_scheme_rcl('MSB')
  #use_msb_msbar_scheme_rcl('m12')

elif model == 'HS':
  # Set mixing angle alpha via sin(alpha)
  set_sa_rcl(-0.979796)

  # Set tb=tan(beta)
  set_tb_rcl(2.2)

  # all msbar alpha (mixing angle) renormalization schemes in the HSESM
  #use_mixing_alpha_msbar_scheme_rcl('FJTS')
  use_mixing_alpha_msbar_scheme_rcl('l3')

  # all on-shell alpha (mixing angle) renormalization schemes in the HSESM
  #use_mixing_alpha_onshell_scheme_rcl('ps')
  #use_mixing_alpha_onshell_scheme_rcl('os')
  #use_mixing_alpha_msbar_scheme_rcl('MDTS')
  #use_mixing_alpha_msbar_scheme_rcl('MTS')

  # all the msbar tb renormalization schemes in the HSESM
  #use_tb_msbar_scheme_rcl('FJTS')
  use_tb_msbar_scheme_rcl('MDTS')


#------------------------------------------------------------------------------#
# Step 2                                                                       #
#------------------------------------------------------------------------------#
# In step 2 processes to be computed are defined. Optionally, specific powers  #
# in fundamental couplings, or polarizations can be enforced using methods     #
# defined in the modules "process_definition_rcl" and "recola1_interface_rcl". #
# The processes are defined by calling "define_process_rcl" which requires a   #
# unique process number, a process string and the order of computation. Note   #
# that by default all powers in fundamental couplings are selected.            #
#------------------------------------------------------------------------------#

# We define various light and heavy Higgs decays, without specifying the
# orders. Some of them are loop-induced.
if model == 'THDM' or model == 'HS':
  define_process_rcl(1, "Hh -> Hl Hl", "NLO")
  define_process_rcl(2, "Hh -> Z Z", "NLO")
  define_process_rcl(3, "Hl -> g g", "NLO")
  define_process_rcl(4, "Hl -> A A", "NLO")
  define_process_rcl(5, "Hl -> Z A", "NLO")
  define_process_rcl(6, "Hh -> g g", "NLO")
  define_process_rcl(7, "Hh -> A A", "NLO")
  define_process_rcl(8, "Hh -> Z A", "NLO")
elif model == 'SM':
  define_process_rcl(3, "H -> g g", "NLO")
  define_process_rcl(4, "H -> A A", "NLO")
  define_process_rcl(5, "H -> Z A", "NLO")

#------------------------------------------------------------------------------#
# Step 3                                                                       #
#------------------------------------------------------------------------------#
# The skeleton of the recursive procedure is built for all defined             #
# processes, by calling the subroutine "generate_processes_rcl".               #
#------------------------------------------------------------------------------#

generate_processes_rcl()

#------------------------------------------------------------------------------#
# Step 4                                                                       #
#------------------------------------------------------------------------------#
# The fourth step is the actual computation of processes.                      #
# Each process defined at step 2 can be computed at this stage for given       #
# phase-space points provided by the user.                                     #
# The computation of (squared) amplitudes is carried out by calling the        #
# subroutine "compute_process_rcl".                                            #
# In the module "process_computation_rcl" other useful                         #
# methods are defined, which allow to get the value for specific               #
# contributions to amplitudes, squared amplitudes, and                         #
# Born colour- and/or spin-correlated squared amplitudes.                      #
#------------------------------------------------------------------------------#

if model == 'THDM' or model == 'HS':
  # Hh-> Hl Hl
  p1 = [400., 0.0, 0.0, 0.0]
  p2 = [200., 0.0, 0.0, 156.124949960]
  p3 = [200., 0.0, 0.0, -156.124949960]
  p = [p1, p2, p3]
  compute_process_rcl(1, p, 'NLO')

# Hh -> Z Z
  p1 = [400., 0.0, 0.0, 0.0]
  p2 = [200., 0.0, 0.0, 178.019782332]
  p3 = [200., 0.0, 0.0, -178.0197823320]
  p = [p1, p2, p3]
  compute_process_rcl(2, p, "NLO")

# Hl -> gg, AA
  p1 = [125., 0.0, 0.0, 0.0]
  p2 = [62.5, 0.0, 0.0, 62.5]
  p3 = [62.5, 0.0, 0.0, -62.5]
  p = [p1, p2, p3]
  compute_process_rcl(3, p, "NLO")
  compute_process_rcl(4, p, "NLO")

# Hl -> ZA
  p1 = [125., 0.0, 0.0, 0.0]
  p2 = [95.735828394, 0.0, 0.0, 29.264171606]
  p3 = [29.264171606, 0.0, 0.0, -29.264171606]
  p = [p1, p2, p3]
  compute_process_rcl(5, p, "NLO")

# Hh -> gg, AA
  p1 = [400., 0.0, 0.0, 0.0]
  p2 = [200., 0.0, 0.0, 200.]
  p3 = [200., 0.0, 0.0, -200.]
  p = [p1, p2, p3]
  compute_process_rcl(6, p, "NLO")
  compute_process_rcl(7, p, "NLO")

# Hh -> ZA
  p1 = [400., 0.0, 0.0, 0.0]
  p2 = [210.386196373, 0.0, 0.0, 189.613803627]
  p3 = [189.613803627, 0.0, 0.0, -189.613803627]
  p = [p1, p2, p3]
  compute_process_rcl(8, p, "NLO")

elif model == 'SM':
  # H -> gg, AA
  p1 = [125., 0.0, 0.0, 0.0]
  p2 = [62.5, 0.0, 0.0, 62.5]
  p3 = [62.5, 0.0, 0.0, -62.5]
  p = [p1, p2, p3]
  compute_process_rcl(3, p, "NLO")
  compute_process_rcl(4, p, "NLO")

# H -> ZA
  p1 = [125., 0.0, 0.0, 0.0]
  p2 = [95.735828394, 0.0, 0.0, 29.264171606]
  p3 = [29.264171606, 0.0, 0.0, -29.264171606]
  p = [p1, p2, p3]
  compute_process_rcl(5, p, "NLO")

#------------------------------------------------------------------------------#
# Step 5                                                                       #
#------------------------------------------------------------------------------#
# Calling reset_recola_rcl (module reset_rcl), deallocates all processes       #
# generated in the previous steps and allows for the next call of Recola.      #
#------------------------------------------------------------------------------#

reset_recola_rcl()
