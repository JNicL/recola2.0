!******************************************************************************!
!                                                                              !
!    demo5_rcl.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016,2017 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!
! PARTICLES                                                                    !
! Scalars in the SM:    'H', 'G0', 'G+', 'G-'                                  !
! Scalars in the HSESM: 'Hl', 'Hh', 'G0', 'G+', 'G-'                           !
! Scalars in the THDM:  'Hl', 'Hh', 'Ha', 'H+', 'H-', 'G0', 'G+', 'G-'         !
! Vector bosons:        'g', 'A', 'Z', 'W+', 'W-'                              !
! leptons:              'nu_e', 'nu_e~', 'e-', 'e+',                           !
!                       'nu_mu', 'nu_mu~', 'mu-', 'mu+',                       !
!                       'nu_tau', 'nu_tau~', 'tau-', 'tau+'                    !
! quarks:               'u', 'u~', 'd', 'd~',                                  !
!                       'c', 'c~', 's', 's~',                                  !
!                       't', 't~', 'b', 'b~'                                   !
!******************************************************************************!

program main_rcl

! Each program, which uses RECOLA must have:
  use recola

  implicit none

! Variables for this demo file
  integer, parameter :: dp = kind (23d0)
  real(dp)           :: p(0:3,5),A2_EW,A2_QCD_2,A2_QCD_4
  character(len=100) :: modelname

!------------------------------------------------------------------------------!
! Step 1                                                                       !
!------------------------------------------------------------------------------!
! Set input values for the computation.                                        !
! General methods for arbitrary models are defined in "input_rcl.f90",         !
! whereas model specific ones are defined in "recola1_interface_rcl.90".       !
! Methods dedicated to the THDM/HSESM are listed in                            !
! "extended_higgs_interface_rcl.f90".                                          !
! Since all variables have default values, this step is optional.              !
!------------------------------------------------------------------------------!

! Retrieve active model
  call get_modelname_rcl(modelname)
! The standard output is selected
  call set_output_file_rcl('*')

!------------------------------------------------------------------------------!
! Step 2                                                                       !
!------------------------------------------------------------------------------!
! In step 2 processes to be computed are defined. Optionally, specific powers  !
! in fundamental couplings, or polarizations can be enforced using methods     !
! defined in the modules "process_definition_rcl" and "recola1_interface_rcl". !
! The processes are defined by calling "define_process_rcl" which requires a   !
! unique process number, a process string and the order of computation. Note   !
! that by default all powers in fundamental couplings are selected.            !
!------------------------------------------------------------------------------!

! we define a partonic channel for VBF for different types of corrections
! using the new methods for the selection of orders

  if (modelname .eq. 'THDM' .or. modelname .eq. 'HS') then
    call define_process_rcl(1, 'u u~ -> u u~ Hl', 'NLO')
    call define_process_rcl(2, 'u u~ -> u u~ Hl', 'NLO')
    call define_process_rcl(3, 'u u~ -> u u~ Hl', 'NLO')
  else if (modelname .eq. 'SM') then
    call define_process_rcl(1, 'u u~ -> u u~ H', 'NLO')
    call define_process_rcl(2, 'u u~ -> u u~ H', 'NLO')
    call define_process_rcl(3, 'u u~ -> u u~ H', 'NLO')
  end if
  call unselect_all_powers_BornAmpl_rcl(1)
  call unselect_all_powers_LoopAmpl_rcl(1)

! the Born is of the order e^3. Since the selection applies on amplitude
! level, we select g_s^0 e^3 (=QCD^0 QED^3)
  call select_power_BornAmpl_rcl(1, 'QCD', 0)
  call select_power_BornAmpl_rcl(1, 'QED', 3)

! Next we select the EW virtual corrections for the EW born
  call select_power_LoopAmpl_rcl(1, 'QCD', 0)
  call select_power_LoopAmpl_rcl(1, 'QED', 5)
! These selections correspond on squared amplitude level to: QED^6, QED^8


! Next, we select the QCD virtual corrections to the EW born using the original
! Recola1 methods for the selection of orders
  call unselect_all_gs_powers_BornAmpl_rcl(2)
! equivalent to: unselect_all_powers_BornAmpl_rcl(2)
  call unselect_all_gs_powers_LoopAmpl_rcl(2)
! equivalent to: unselect_all_powers_LoopAmpl_rcl(2)
  call select_gs_power_BornAmpl_rcl(2, 0)
  call select_gs_power_LoopAmpl_rcl(2, 2)
! These selections correspond on squared amplitude level to: QED^6, QED^6 QCD^2


! We select the purely QCD virtual corrections as follows
  call unselect_all_gs_powers_BornAmpl_rcl(3)
  call unselect_all_gs_powers_LoopAmpl_rcl(3)
  call select_gs_power_BornAmpl_rcl(3, 0)
  call select_gs_power_LoopAmpl_rcl(3, 4)
! These selections correspond on squared amplitude level to: QED^6, QED^4 QCD^4


! At last, we compute the full process for comparison, without any specific
! order selections.
  if (modelname .eq. 'THDM' .or. modelname .eq. 'HS') then
    call define_process_rcl(4, 'u u~ -> u u~ Hl', 'NLO')
  else if (modelname .eq. 'SM') then
    call define_process_rcl(4, 'u u~ -> u u~ H', 'NLO')
  end if

!------------------------------------------------------------------------------!
! Step 3                                                                       !
!------------------------------------------------------------------------------!
! The skeleton of the recursive procedure is built for all defined             !
! processes, by calling the subroutine "generate_processes_rcl".               !
!------------------------------------------------------------------------------!

  call generate_processes_rcl

!------------------------------------------------------------------------------!
! Step 4                                                                       !
!------------------------------------------------------------------------------!
! The fourth step is the actual computation of processes.                      !
! Each process defined at step 2 can be computed at this stage for given       !
! phase-space points provided by the user.                                     !
! The computation of (squared) amplitudes is carried out by calling the        !
! subroutine "compute_process_rcl".                                            !
! In the module "process_computation_rcl" other useful                         !
! methods are defined, which allow to get the value for specific               !
! contributions to amplitudes, squared amplitudes, and                         !
! Born colour- and/or spin-correlated squared amplitudes.                      !
!------------------------------------------------------------------------------!

  p(:,1) = [6500.000000000d0,    0.000000000d0,    0.000000000d0, 6500.000000000d0]
  p(:,2) = [6500.000000000d0,    0.000000000d0,    0.000000000d0,-6500.000000000d0]
  p(:,3) = [4043.061492103d0,-3750.493447903d0,  498.083700590d0,-1425.502631835d0]
  p(:,4) = [4379.284713032d0, 3091.569143128d0, 2096.964798066d0,-2285.404442702d0]
  p(:,5) = [4577.653794866d0,  658.924304774d0,-2595.048498656d0, 3710.907074537d0]

  write(*,*) 
  write(*,*) "Computing virtual corrections to u u~ -> u u~ Hl(H):"
  call compute_process_rcl(1, p, 'NLO')
  call get_squared_amplitude_rcl(1, 0, 'NLO', A2_EW)
  call compute_process_rcl(2, p, 'NLO')
  call get_squared_amplitude_rcl(2, 1, 'NLO', A2_QCD_2)
  call compute_process_rcl(3, p, 'NLO')
  call get_squared_amplitude_rcl(3, 2, 'NLO', A2_QCD_4)

  write(*,'(2x,a58,2x,es23.16)') &
    "Extracting EW (QCD^0) virtual amplitude^2 from process 1:", A2_EW
  write(*,'(2x,a58,2x,es23.16)') &
    "Extracting QCD^2 virtual amplitude^2 from process 2:     ", A2_QCD_2
  write(*,'(2x,a58,2x,es23.16)') &
    "Extracting QCD^4 virtual amplitude^2 from process 3:     ", A2_QCD_4

  write(*,*) 

  write(*,*) "Comparison against process 4 without order selections:"
  call compute_process_rcl(4, p, 'NLO')
  call get_squared_amplitude_rcl(4, 0, 'NLO', A2_EW)
  !equiv: call get_squared_amplitude_rcl(4, [0, 8], 'NLO', A2_EW)
  call get_squared_amplitude_rcl(4, 1, 'NLO', A2_QCD_2)
  !equiv: call get_squared_amplitude_rcl(4, [2, 6], 'NLO', A2_QCD_2)
  call get_squared_amplitude_rcl(4, 2, 'NLO', A2_QCD_4)
  !equiv: call get_squared_amplitude_rcl(4, [4, 4], 'NLO', A2_QCD_4)

  write(*,'(2x,a58,2x,es23.16)') &
    "Extracting EW (QCD^0) virtual amplitude^2 from process 4:", A2_EW
  write(*,'(2x,a58,2x,es23.16)') &
    "Extracting QCD^2 virtual amplitude^2 from process 4:     ", A2_QCD_2
  write(*,'(2x,a58,2x,es23.16)') &
    "Extracting QCD^4 virtual amplitude^2 from process 4:     ", A2_QCD_4

!------------------------------------------------------------------------------!
! Step 5                                                                       !
!------------------------------------------------------------------------------!
! Calling reset_recola_rcl (module reset_rcl), deallocates all processes       !
! generated in the previous steps and allows for the next call of Recola.      !
!------------------------------------------------------------------------------!

  call reset_recola_rcl

!------------------------------------------------------------------------------!

end program main_rcl

!------------------------------------------------------------------------------!
