!******************************************************************************!
!                                                                              !
!    demo4_rcl.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016,2017 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!
! PARTICLES                                                                    !
! Scalars in the SM:    'H', 'G0', 'G+', 'G-'                                  !
! Scalars in the HSESM: 'Hl', 'Hh', 'G0', 'G+', 'G-'                           !
! Scalars in the THDM:  'Hl', 'Hh', 'Ha', 'H+', 'H-', 'G0', 'G+', 'G-'         !
! Vector bosons:        'g', 'A', 'Z', 'W+', 'W-'                              !
! leptons:              'nu_e', 'nu_e~', 'e-', 'e+',                           !
!                       'nu_mu', 'nu_mu~', 'mu-', 'mu+',                       !
!                       'nu_tau', 'nu_tau~', 'tau-', 'tau+'                    !
! quarks:               'u', 'u~', 'd', 'd~',                                  !
!                       'c', 'c~', 's', 's~',                                  !
!                       't', 't~', 'b', 'b~'                                   !
!******************************************************************************!

program main_rcl

! Each program, which uses RECOLA must have:
  use recola

  implicit none

! Variables for this demo file
  integer, parameter :: dp = kind (23d0)
  real(dp)           :: p(0:3,3)
  character(len=100) :: model

!------------------------------------------------------------------------------!
! Step 1                                                                       !
!------------------------------------------------------------------------------!
! Set input values for the computation.                                        !
! General methods for arbitrary models are defined in "input_rcl.f90",         !
! whereas model specific ones are defined in "recola1_interface_rcl.90".       !
! Methods dedicated to the THDM/HSESM are listed in                            !
! "extended_higgs_interface_rcl.f90".                                          !
! Since all variables have default values, this step is optional.              !
!------------------------------------------------------------------------------!

! Retrieve active model
  call get_modelname_rcl(model)

! Print everything on the screen
  call set_output_file_rcl('*')

! Let's print the input parameters + derived parameters
  call set_print_level_parameters_rcl(2)

! We set the muUV scale. Change this scale to check the UV finiteness.
  call set_mu_uv_rcl(100d0)

! We set the muMS scale. Change this scale to probe the scale uncertainty.
  call set_mu_ms_rcl(100d0)

! We set the muIR scale. Change this scale to probe the IR scale dependence.
  call set_mu_ir_rcl(100d0)

! We print the results for all squared amplitudes
  call set_print_level_squared_amplitude_rcl(1)

! In this demo file we consider all kinds of 3-particle on-shell Higgs decays.
! For this reason, we set the pole masses of the W/Z and Higgs bosons to
! real-valued ones.
  call set_pole_mass_w_rcl(80.357974d0,0d0)
  call set_pole_mass_z_rcl(91.153481d0,0d0)
  if (model .eq. 'THDM') then
    call set_pole_mass_hl_hh_rcl(125d0,0d0,400d0,0d0)
    call set_pole_mass_ha_rcl(300d0, 0d0)
    call set_pole_mass_hc_rcl(333d0, 0d0)
    ! We choose Yukawa Type II
    call set_Z2_thdm_yukawa_type_rcl(2)
  else if (model .eq. 'HS') then
    call set_pole_mass_hl_hh_rcl(125d0,0d0,400d0,0d0)
  else if (model .eq. 'SM') then
    call set_pole_mass_h_rcl(125d0,0d0)
  end if

! Depending on the active model file, we define values for (mixing)
! angles and fix their renormalization scheme
  if (model .eq. 'THDM') then
    ! Set mixing angles tb=tan(beta), cab = cos(alpha-beta)
    call set_tb_cab_rcl(2.2d0,sqrt(1-0.979796d0**2d0))

    ! Set soft breaking scale
    call set_msb_rcl(320d0)

    ! all msbar schemes for alpha (mixing angle) in the THDM
    !call use_mixing_alpha_msbar_scheme_rcl('FJTS')
    call use_mixing_alpha_msbar_scheme_rcl('l3')
    !call use_mixing_alpha_msbar_scheme_rcl('l345')
    !call use_mixing_alpha_msbar_scheme_rcl('MDTS')
    !call use_mixing_alpha_msbar_scheme_rcl('MTS')

    ! all on-shell schemes for alpha (mixing angle) in the THDM
    !call use_mixing_alpha_onshell_scheme_rcl('os')
    !call use_mixing_alpha_onshell_scheme_rcl('ps')

    ! all the msbar beta renormalization schemes in the THDM
    !call use_mixing_beta_msbar_scheme_rcl('FJTS')
    !call use_mixing_beta_msbar_scheme_rcl('MDTS')

    ! all the on-shell beta renormalization schemes in the THDM
    call use_mixing_beta_onshell_scheme_rcl('ps1')
    !call use_mixing_beta_onshell_scheme_rcl('ps2')
    !call use_mixing_beta_onshell_scheme_rcl('os1')
    !call use_mixing_beta_onshell_scheme_rcl('os2')

    ! all the MSB renormalization schemes in the THDM
    call use_msb_msbar_scheme_rcl('MSB')
    !call use_msb_msbar_scheme_rcl('m12')

  else if (model .eq. 'HS') then
    ! Set mixing angle alpha via sin(alpha)
    call set_sa_rcl(-0.979796d0)

    ! Set tb=tan(beta)
    call set_tb_rcl(2.2d0)

    ! all msbar alpha (mixing angle) renormalization schemes in the HSESM
    !call use_mixing_alpha_msbar_scheme_rcl('FJTS')
    call use_mixing_alpha_msbar_scheme_rcl('l3')

    ! all on-shell alpha (mixing angle) renormalization schemes in the HSESM
    !call use_mixing_alpha_onshell_scheme_rcl('ps')
    !call use_mixing_alpha_onshell_scheme_rcl('os')
    !call use_mixing_alpha_msbar_scheme_rcl('MDTS')
    !call use_mixing_alpha_msbar_scheme_rcl('MTS')

    ! all the msbar tb renormalization schemes in the HSESM
    !call use_tb_msbar_scheme_rcl('FJTS')
    call use_tb_msbar_scheme_rcl('MDTS')
  end if

!------------------------------------------------------------------------------!
! Step 2                                                                       !
!------------------------------------------------------------------------------!
! In step 2 processes to be computed are defined. Optionally, specific powers  !
! in fundamental couplings, or polarizations can be enforced using methods     !
! defined in the modules "process_definition_rcl" and "recola1_interface_rcl". !
! The processes are defined by calling "define_process_rcl" which requires a   !
! unique process number, a process string and the order of computation. Note   !
! that by default all powers in fundamental couplings are selected.            !
!------------------------------------------------------------------------------!

! We define various light and heavy Higgs decays
  if (model .eq. 'HS' .or. model .eq. 'THDM') then
    call define_process_rcl(1,'Hh -> Hl Hl','NLO')
    call define_process_rcl(2,'Hh -> Z Z','NLO')
    call define_process_rcl(3,'Hl -> g g','NLO')
    call define_process_rcl(4,'Hl -> A A','NLO')
    call define_process_rcl(5,'Hl -> Z A','NLO')
    call define_process_rcl(6,'Hh -> g g','NLO')
    call define_process_rcl(7,'Hh -> A A','NLO')
    call define_process_rcl(8,'Hh -> Z A','NLO')
  else if (model .eq. 'SM') then
    call define_process_rcl(3,'H -> g g','NLO')
    call define_process_rcl(4,'H -> A A','NLO')
    call define_process_rcl(5,'H -> Z A','NLO')
  end if

!------------------------------------------------------------------------------!
! Step 3                                                                       !
!------------------------------------------------------------------------------!
! The skeleton of the recursive procedure is built for all defined             !
! processes, by calling the subroutine "generate_processes_rcl".               !
!------------------------------------------------------------------------------!

  call generate_processes_rcl

!------------------------------------------------------------------------------!
! Step 4                                                                       !
!------------------------------------------------------------------------------!
! The fourth step is the actual computation of processes.                      !
! Each process defined at step 2 can be computed at this stage for given       !
! phase-space points provided by the user.                                     !
! The computation of (squared) amplitudes is carried out by calling the        !
! subroutine "compute_process_rcl".                                            !
! In the module "process_computation_rcl" other useful                         !
! methods are defined, which allow to get the value for specific               !
! contributions to amplitudes, squared amplitudes, and                         !
! Born colour- and/or spin-correlated squared amplitudes.                      !
!------------------------------------------------------------------------------!

  if (model .eq. 'HS' .or. model .eq. 'THDM') then
    ! Hh-> Hl Hl
    p(:,1) = [400.000000000d0, 0.000000000d0, 0.000000000d0,   0.000000000d0]
    p(:,2) = [200.000000000d0, 0.000000000d0, 0.000000000d0, 156.124949960d0]
    p(:,3) = [200.000000000d0, 0.000000000d0, 0.000000000d0,-156.124949960d0]
    call compute_process_rcl(1,p,'NLO')

    ! Hh -> Z Z
    p(:,1) = [400.000000000d0, 0.000000000d0, 0.000000000d0,   0.000000000d0]
    p(:,2) = [200.000000000d0, 0.000000000d0, 0.000000000d0, 178.019782332d0]
    p(:,3) = [200.000000000d0, 0.000000000d0, 0.000000000d0,-178.019782332d0]
    call compute_process_rcl(2,p,'NLO')

    ! Hl -> gg, AA
    p(:,1) = [125.000000000d0, 0.000000000d0, 0.000000000d0,  0.000000000d0]
    p(:,2) = [ 62.500000000d0, 0.000000000d0, 0.000000000d0, 62.500000000d0]
    p(:,3) = [ 62.500000000d0, 0.000000000d0, 0.000000000d0,-62.500000000d0]
    call compute_process_rcl(3,p,'NLO')
    call compute_process_rcl(4,p,'NLO')

    ! Hl -> ZA
    p(:,1) = [125.000000000d0, 0.000000000d0, 0.000000000d0,  0.000000000d0]
    p(:,2) = [ 95.735828394d0,-0.000000000d0,-0.000000000d0, 29.264171606d0]
    p(:,3) = [ 29.264171606d0, 0.000000000d0, 0.000000000d0,-29.264171606d0]
    call compute_process_rcl(5,p,'NLO')

    ! Hh -> gg, AA
    p(:,1) = [400.000000000d0, 0.000000000d0, 0.000000000d0,   0.000000000d0]
    p(:,2) = [200.000000000d0, 0.000000000d0, 0.000000000d0, 200.000000000d0]
    p(:,3) = [200.000000000d0, 0.000000000d0, 0.000000000d0,-200.000000000d0]
    call compute_process_rcl(6,p,'NLO')
    call compute_process_rcl(7,p,'NLO')

    ! Hh -> ZA
    p(:,1) = [400.000000000d0, 0.000000000d0, 0.000000000d0,   0.000000000d0]
    p(:,2) = [210.386196373d0,-0.000000000d0,-0.000000000d0, 189.613803627d0]
    p(:,3) = [189.613803627d0, 0.000000000d0, 0.000000000d0,-189.613803627d0]
    call compute_process_rcl(8,p,'NLO')
  else if (model .eq. 'SM') then
    ! H -> gg, AA
    p(:,1) = [125.000000000d0, 0.000000000d0, 0.000000000d0,  0.000000000d0]
    p(:,2) = [ 62.500000000d0, 0.000000000d0, 0.000000000d0, 62.500000000d0]
    p(:,3) = [ 62.500000000d0, 0.000000000d0, 0.000000000d0,-62.500000000d0]
    call compute_process_rcl(3,p,'NLO')
    call compute_process_rcl(4,p,'NLO')

    ! H -> ZA
    p(:,1) = [125.000000000d0, 0.000000000d0, 0.000000000d0,  0.000000000d0]
    p(:,2) = [ 95.735828394d0,-0.000000000d0,-0.000000000d0, 29.264171606d0]
    p(:,3) = [ 29.264171606d0, 0.000000000d0, 0.000000000d0,-29.264171606d0]
    call compute_process_rcl(5,p,'NLO')
  end if

!------------------------------------------------------------------------------!
! Step 5                                                                       !
!------------------------------------------------------------------------------!
! Calling reset_recola_rcl (module reset_rcl), deallocates all processes       !
! generated in the previous steps and allows for the next call of Recola.      !
!------------------------------------------------------------------------------!

  call reset_recola_rcl

!------------------------------------------------------------------------------!

end program main_rcl

!------------------------------------------------------------------------------!
