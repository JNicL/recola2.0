!******************************************************************************!
!                                                                              !
!    demo1_rcl.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016,2017 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!
! PARTICLES                                                                    !
! Scalars in the SM:    'H', 'G0', 'G+', 'G-'                                  !
! Scalars in the HSESM: 'Hl', 'Hh', 'G0', 'G+', 'G-'                           !
! Scalars in the THDM:  'Hl', 'Hh', 'Ha', 'H+', 'H-', 'G0', 'G+', 'G-'         !
! Vector bosons:        'g', 'A', 'Z', 'W+', 'W-'                              !
! leptons:              'nu_e', 'nu_e~', 'e-', 'e+',                           !
!                       'nu_mu', 'nu_mu~', 'mu-', 'mu+',                       !
!                       'nu_tau', 'nu_tau~', 'tau-', 'tau+'                    !
! quarks:               'u', 'u~', 'd', 'd~',                                  !
!                       'c', 'c~', 's', 's~',                                  !
!                       't', 't~', 'b', 'b~'                                   !
!******************************************************************************!

program main_rcl

! Each program, which uses RECOLA must have:
  use recola

  implicit none

! Variables for this demo file
  integer,  parameter :: dp = kind (23d0) ! double precision
  real(dp), parameter :: pi = 3.141592653589793238462643d0
  real(dp)            :: p(0:3,1:6),k(0:3,1:7)
  complex(dp)         :: v(0:3)

!------------------------------------------------------------------------------!
! Step 1                                                                       !
!------------------------------------------------------------------------------!
! Set input values for the computation.                                        !
! General methods for arbitrary models are defined in "input_rcl.f90",         !
! whereas model specific ones are defined in "recola1_interface_rcl.90".       !
! Methods dedicated to the THDM/HSESM are listed in                            !
! "extended_higgs_interface_rcl.f90".                                          !
! Since all variables have default values, this step is optional.              !
!------------------------------------------------------------------------------!

! Let's print all input/derived parameters
  call set_print_level_parameters_rcl(1)
! We enable QCD running which can be used for theories with SM-like quark
! content.
  call set_qcd_rescaling_rcl(.true.)

! Set a massive bottom quark
  call set_pole_mass_bottom_rcl(4.5d0,0d0)
! The bottom quark is set as a heavy particle (full mass dependence 
! is kept)
  call unset_light_bottom_rcl
! Particles with mass below 1 GeV are treated as massless
  call set_light_fermions_rcl(1d0)

! The double IR-pole DeltaIR2 is set to Zeta(2) = pi^2/6. 
! In this way the result of the amplitude corresponds to the finite 
! part of the amplitude in the conventions of the Binoth-Les Houches 
! Accord (arXiv:1001.1307 [hep-ph])
  call set_delta_ir_rcl(0d0,pi**2/6d0)

! UV- and IR-scales are set to 100 GeV
  call set_mu_uv_rcl(100d0)
  call set_mu_ir_rcl(100d0)

! QCD renormalization scale is set to 100 GeV, the variable flavour 
! scheme is selected and the corresponding value for alpha_s is set 
! to 0.125808685692
  call set_alphas_rcl(0.125808685692d0,100d0,-1)

! The alphaZ scheme is selected for EW renormalization
  call use_alphaZ_scheme_rcl(0.0078125d0)

! The print levels are changed
  call set_print_level_amplitude_rcl(2)
  call set_print_level_squared_amplitude_rcl(3)

! The .tex file will be generated
  call set_draw_level_branches_rcl(2)

!------------------------------------------------------------------------------!
! Step 2                                                                       !
!------------------------------------------------------------------------------!
! In step 2 processes to be computed are defined. Optionally, specific powers  !
! in fundamental couplings, or polarizations can be enforced using methods     !
! defined in the modules "process_definition_rcl" and "recola1_interface_rcl". !
! The processes are defined by calling "define_process_rcl" which requires a   !
! unique process number, a process string and the order of computation. Note   !
! that by default all powers in fundamental couplings are selected.            !
!------------------------------------------------------------------------------!

! Two processes are defined:
! 1) Scattering at NLO of a pair of down-antidown producing a pair of 
!    top-antitop and a pair of bottom-antibottom. The bottom and the 
!    anti-bottom are set as left-handed. 
! 2) Scattering at LO of a pair of down-antidown producing a pair of 
!    top-antitop and a pair of bottom-antibottom and an extra gluon. 
!    The bottom and the anti-bottom are set as left-handed. 
  call define_process_rcl(1,'d d~ -> t t~ b[-] b~[+]','NLO')
  call define_process_rcl(2,'d d~ -> t t~ b[-] b~[+] g','LO')

! Selection of QCD contributions for process 1:

! All powers of gs are unselected for Born amplitudes, except gs^4.
  call unselect_all_gs_powers_BornAmpl_rcl(1)
  call select_gs_power_BornAmpl_rcl(1,4)
  ! These selection are equivalent to the more general calls:
  !call unselect_all_powers_BornAmpl_rcl(1)
  !call select_power_BornAmpl_rcl(1,'QCD',4)
  !call select_power_BornAmpl_rcl(1,'QED',0)

! All powers of gs are unselected for Loop amplitudes, except for gs^6.
  call unselect_all_gs_powers_LoopAmpl_rcl(1)
  call select_gs_power_LoopAmpl_rcl(1,6)

! Selection of QCD contributions for process 2:
! All powers of gs are unselected for Born amplitudes, except for gs^5.
  call unselect_all_gs_powers_BornAmpl_rcl(2)
  call select_gs_power_BornAmpl_rcl(2,5)

!------------------------------------------------------------------------------!
! Step 3                                                                       !
!------------------------------------------------------------------------------!
! The skeleton of the recursive procedure is built for all defined             !
! processes, by calling the subroutine "generate_processes_rcl".               !
!------------------------------------------------------------------------------!

  call generate_processes_rcl

!------------------------------------------------------------------------------!
! Step 4                                                                       !
!------------------------------------------------------------------------------!
! The fourth step is the actual computation of processes.                      !
! Each process defined at step 2 can be computed at this stage for given       !
! phase-space points provided by the user.                                     !
! The computation of (squared) amplitudes is carried out by calling the        !
! subroutine "compute_process_rcl".                                            !
! In the module "process_computation_rcl" other useful                         !
! methods are defined, which allow to get the value for specific               !
! contributions to amplitudes, squared amplitudes, and                         !
! Born colour- and/or spin-correlated squared amplitudes.                      !
!------------------------------------------------------------------------------!

! Momenta of the phase-space point for process 1
  p(:,1) = [250.000000000d0,   0.000000000d0,   0.000000000d0, 250.000000000d0]
  p(:,2) = [250.000000000d0,   0.000000000d0,   0.000000000d0,-250.000000000d0]
  p(:,3) = [191.521737019d0, -72.989672519d0,  23.203308008d0, -28.573588393d0]
  p(:,4) = [187.323053415d0,  41.294104557d0,  43.349089605d0, -38.824472950d0]
  p(:,5) = [ 67.060209357d0,  -5.935268585d0, -28.633411230d0,  60.180744654d0]
  p(:,6) = [ 54.095000210d0,  37.630836547d0, -37.918986384d0,   7.217316689d0]

! Running of alpha_s for this phase-space point.
! The running value is computed by RECOLA at the CM energy 
! (p(0,1)+p(0,2) = 500 GeV), in the variable flavour scheme, with 
! one-loop running.
  call compute_running_alphas_rcl(p(0,1)+p(0,2),-1,1)

! Compute process 1
  call compute_process_rcl(1,p,'NLO')

! Momenta of the phase-space point for process 2
  k(:,1) = [250.000000000d0,   0.000000000d0,   0.000000000d0, 250.000000000d0]
  k(:,2) = [250.000000000d0,   0.000000000d0,   0.000000000d0,-250.000000000d0]
  k(:,3) = [187.692956406d0, -59.993359307d0,  28.357226683d0, -28.758832003d0]
  k(:,4) = [186.301598855d0,  38.377203074d0,  43.056475804d0, -37.193761727d0]
  k(:,5) = [ 52.741902453d0,  -3.942491762d0, -17.679399804d0,  49.329036639d0]
  k(:,6) = [ 44.020044292d0,  33.185994848d0, -28.199232244d0,   4.583377168d0]
  k(:,7) = [ 29.243497994d0,  -7.627346853d0, -25.535070439d0,  12.040179922d0]

! Running of alpha_s for this phase-space point.
! The running value is computed by RECOLA at the CM energy 
! (k(0,1)+k(0,2) = 500 GeV), in the variable flavour scheme, with 
! one-loop running.
  call compute_running_alphas_rcl(k(0,1)+k(0,2),-1,1)

! Compute process 2
  call compute_process_rcl(2,k,'LO')

!------------------------------------------------------------------------------!
! Step 5                                                                       !
!------------------------------------------------------------------------------!
! Calling reset_recola_rcl (module reset_rcl), deallocates all processes       !
! generated in the previous steps and allows for the next call of Recola.      !
!------------------------------------------------------------------------------!

  call reset_recola_rcl

!------------------------------------------------------------------------------!

end program main_rcl

!------------------------------------------------------------------------------!
