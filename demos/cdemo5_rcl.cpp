//****************************************************************************//
//                                                                            //
//    cdemo5_rcl.cpp                                                          //
//    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2      //
//                                                                            //
//    Copyright (C) 2016,2017 Ansgar Denner, Jean-Nicolas Lang and            //
//                            Sandro Uccirati                                 //
//                                                                            //
//    RECOLA2 is licenced under the GNU GPL version 3,                        //
//    see COPYING for details.                                                //
//                                                                            //
//****************************************************************************//
// PARTICLES                                                                  //
// Scalars in the SM:    'H', 'G0', 'G+', 'G-'                                //
// Scalars in the HSESM: 'Hl', 'Hh', 'G0', 'G+', 'G-'                         //
// Scalars in the THDM:  'Hl', 'Hh', 'Ha', 'H+', 'H-', 'G0', 'G+', 'G-'       //
// Vector bosons:        'g', 'A', 'Z', 'W+', 'W-'                            //
// leptons:              'nu_e', 'nu_e~', 'e-', 'e+',                         //
//                       'nu_mu', 'nu_mu~', 'mu-', 'mu+',                     //
//                       'nu_tau', 'nu_tau~', 'tau-', 'tau+'                  //
// quarks:               'u', 'u~', 'd', 'd~',                                //
//                       'c', 'c~', 's', 's~',                                //
//                       't', 't~', 'b', 'b~'                                 //
//****************************************************************************//

#include <iostream>
#include <stdlib.h>

// Include the Recola C++ header:
#include "recola.hpp"

using namespace Recola;
using namespace std;

int main(int argc, char *argv[])
{

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Step 1                                                                     //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Set input values for the computation.                                      //
// General methods for arbitrary models are defined in "input_rcl.f90",       //
// whereas model specific ones are defined in "recola1_interface_rcl.90".     //
// Methods dedicated to the THDM/HSESM are listed in                          //
// "extended_higgs_interface_rcl.f90".                                        //
// Since all variables have default values, this step is optional.            //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

// Retrieve active model
  string modelname;
  get_modelname_rcl(modelname);

// The standard output is selected
  set_output_file_rcl("*");

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Step 2                                                                     //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// In step 2 processes to be computed are defined. Optionally, specific powers//
// in fundamental couplings, or polarizations can be enforced using methods   //
// defined in the modules "process_definition_rcl" and "recola1_interface_rcl"//
// The processes are defined by calling "define_process_rcl"                  //
// which requires a unique process number, a process string and the order of  //
// computation. Note that by default all powers in fundamental couplings are  //
// selected by default, and any specific selection is optional.               //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

// we define a partonic channel for VBF for different types of corrections
// using the new methods for the selection of orders

  if (modelname == "THDM" || modelname == "HS")
  {
    define_process_rcl(1, "u u~ -> u u~ Hl", "NLO");
    define_process_rcl(2, "u u~ -> u u~ Hl", "NLO");
    define_process_rcl(3, "u u~ -> u u~ Hl", "NLO");
  }
  else if (modelname == "SM")
  {
    define_process_rcl(1, "u u~ -> u u~ H", "NLO");
    define_process_rcl(2, "u u~ -> u u~ H", "NLO");
    define_process_rcl(3, "u u~ -> u u~ H", "NLO");
  }
  unselect_all_powers_BornAmpl_rcl(1);
  unselect_all_powers_LoopAmpl_rcl(1);

// the Born is of the order e^3. Since the selection applies on amplitude
// level, we select g_s^0 e^3 (=QCD^0 QED^3)
  select_power_BornAmpl_rcl(1, "QCD", 0);
  select_power_BornAmpl_rcl(1, "QED", 3);

// Next we select the EW virtual corrections for the EW born
  select_power_LoopAmpl_rcl(1, "QCD", 0);
  select_power_LoopAmpl_rcl(1, "QED", 5);
// These selections correspond on squared amplitude level to: QED^6, QED^8


// Next, we select the QCD virtual corrections to the EW born using the original
// Recola1 methods for the selection of orders
  unselect_all_gs_powers_BornAmpl_rcl(2);
// equivalent to: unselect_all_powers_BornAmpl_rcl(2)
  unselect_all_gs_powers_LoopAmpl_rcl(2);
// equivalent to: unselect_all_powers_LoopAmpl_rcl(2)
  select_gs_power_BornAmpl_rcl(2, 0);
  select_gs_power_LoopAmpl_rcl(2, 2);
// These selections correspond on squared amplitude level to: QED^6, QED^6 QCD^2

// We select the purely QCD virtual corrections as follows
  unselect_all_gs_powers_BornAmpl_rcl(3);
  unselect_all_gs_powers_LoopAmpl_rcl(3);
  select_gs_power_BornAmpl_rcl(3, 0);
  select_gs_power_LoopAmpl_rcl(3, 4);
// These selections correspond on squared amplitude level to: QED^6, QED^4 QCD^4

// At last, we compute the full process for comparison, without any specific
// order selections.
  if (modelname == "THDM" || modelname == "HS")
  {
    define_process_rcl(4, "u u~ -> u u~ Hl", "NLO");
  }
  else if (modelname == "SM")
  {
    define_process_rcl(4, "u u~ -> u u~ H", "NLO");
  }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Step 3                                                                     //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// The skeleton of the recursive procedure is built for all defined           //
// processes, by calling the subroutine "generate_processes_rcl".             //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

  generate_processes_rcl();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Step 4                                                                     //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// The fourth step is the actual computation of processes.                    //
// Each process defined at step 2 can be computed at this stage for given     //
// phase-space points provided by the user.                                   //
// The computation of (squared) amplitudes is carried out by calling the      //
// subroutine "compute_process_rcl".                                          //
// In the module "process_computation_rcl" other useful                       //
// methods are defined, which allow to get the value for specific             //
// contributions to amplitudes, squared amplitudes, and                       //
// Born colour- and/or spin-correlated squared amplitudes.                    //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

  double p[5][4] = {{6500.000000000,    0.000000000,    0.000000000, 6500.000000000},
                    {6500.000000000,    0.000000000,    0.000000000,-6500.000000000},
                    {4043.061492103,-3750.493447903,  498.083700590,-1425.502631835},
                    {4379.284713032, 3091.569143128, 2096.964798066,-2285.404442702},
                    {4577.653794866,  658.924304774,-2595.048498656, 3710.907074537}};

  compute_process_rcl(1, p, "NLO");
  double A2_EW;
  get_squared_amplitude_rcl(1, 0, "NLO", A2_EW);
  compute_process_rcl(2, p, "NLO");
  double A2_QCD2;
  get_squared_amplitude_rcl(2, 1, "NLO", A2_QCD2);
  compute_process_rcl(3, p, "NLO");
  double A2_QCD4;
  get_squared_amplitude_rcl(3, 2, "NLO", A2_QCD4);

  cout.precision(17);
  cout << endl;
  cout << "Computing virtual corrections to u u~ -> u u~ Hl:" << endl;
  cout << "  Extracting EW (QCD^0) virtual amplitude^2 from process 1: " << A2_EW << endl;
  cout << "  Extracting QCD^2 virtual amplitude^2 from process 2:      " << A2_QCD2 << endl;
  cout << "  Extracting QCD^4 virtual amplitude^2 from process 3:      " << A2_QCD4 << endl;


  compute_process_rcl(4, p, "NLO");

  double A2_EW_cmp;
  get_squared_amplitude_rcl(4, 0, "NLO", A2_EW_cmp);
  double A2_QCD2_cmp;
  get_squared_amplitude_rcl(4, 1, "NLO", A2_QCD2_cmp);
  double A2_QCD4_cmp;
  get_squared_amplitude_rcl(4, 2, "NLO", A2_QCD4_cmp);

  cout << endl;
  cout << "Comparison against process 4 without order selections:" << endl;
  cout << "  Extracting EW (QCD^0) virtual amplitude^2 from process 4: " << A2_EW_cmp << endl;
  cout << "  Extracting QCD^2 virtual amplitude^2 from process 4:      " << A2_QCD2_cmp << endl;
  cout << "  Extracting QCD^4 virtual amplitude^2 from process 4:      " << A2_QCD4_cmp << endl;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Step 5                                                                     //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Calling reset_recola_rcl (module reset_rcl), deallocates all processes     //
// generated in the previous steps and allows for the next call of Recola.    //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

  reset_recola_rcl();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

  return 0;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
