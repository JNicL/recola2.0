//****************************************************************************//
//                                                                            //
//    cdemo1_rcl.cpp                                                          //
//    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2      //
//                                                                            //
//    Copyright (C) 2016,2017 Ansgar Denner, Jean-Nicolas Lang and            //
//                            Sandro Uccirati                                 //
//                                                                            //
//    RECOLA2 is licenced under the GNU GPL version 3,                        //
//    see COPYING for details.                                                //
//                                                                            //
//****************************************************************************//
// PARTICLES                                                                  //
// Scalars in the SM:    'H', 'G0', 'G+', 'G-'                                //
// Scalars in the HSESM: 'Hl', 'Hh', 'G0', 'G+', 'G-'                         //
// Scalars in the THDM:  'Hl', 'Hh', 'Ha', 'H+', 'H-', 'G0', 'G+', 'G-'       //
// Vector bosons:        'g', 'A', 'Z', 'W+', 'W-'                            //
// leptons:              'nu_e', 'nu_e~', 'e-', 'e+',                         //
//                       'nu_mu', 'nu_mu~', 'mu-', 'mu+',                     //
//                       'nu_tau', 'nu_tau~', 'tau-', 'tau+'                  //
// quarks:               'u', 'u~', 'd', 'd~',                                //
//                       'c', 'c~', 's', 's~',                                //
//                       't', 't~', 'b', 'b~'                                 //
//****************************************************************************//

// Include the Recola C++ header:
#include "recola.hpp"

int main(int argc, char *argv[])
{
  double pi= 3.141592653589793238462643;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Step 1                                                                     //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Set input values for the computation.                                      //
// General methods for arbitrary models are defined in "input_rcl.f90",       //
// whereas model specific ones are defined in "recola1_interface_rcl.90".     //
// Methods dedicated to the THDM/HSESM are listed in                          //
// "extended_higgs_interface_rcl.f90".                                        //
// Since all variables have default values, this step is optional.            //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

// We enable SM QCD running feature
  Recola::set_qcd_rescaling_rcl(true);

// Set a massive bottom quark
  Recola::set_pole_mass_bottom_rcl(4.5,0.);
// The bottom quark is set as a heavy particle (full mass dependence 
// is kept)
  Recola::unset_light_bottom_rcl ();
// Particles with mass below 1 GeV are treated as massless
  Recola::set_light_fermions_rcl (1.);

// The double IR-pole DeltaIR2 is set to Zeta(2) = pi^2/6. 
// In this way the result of the amplitude corresponds to the finite 
// part of the amplitude in the conventions of the Binoth-Les Houches 
// Accord (arXiv:1001.1307 [hep-ph])
  Recola::set_delta_ir_rcl (0,pi*pi/6.);

// UV- and IR-scales are set to 100 GeV
  Recola::set_mu_uv_rcl (100.);
  Recola::set_mu_ir_rcl (100.);

// QCD renormalization scale is set to 100 GeV, the variable flavour 
// scheme is selected and the corresponding value for alpha_s is set 
// to 0.125808685692
  Recola::set_alphas_rcl (0.125808685692,100.,-1);

// The alphaZ scheme is selected for EW renormalization
  Recola::use_alphaz_scheme_rcl (0.0078125);

// The print levels are changed
  Recola::set_print_level_amplitude_rcl (2);
  Recola::set_print_level_squared_amplitude_rcl (3);

// The .tex file will be generated
  Recola::set_draw_level_branches_rcl (2);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Step 2                                                                     //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// In step 2 processes to be computed are defined. Optionally, specific powers//
// in fundamental couplings, or polarizations can be enforced using methods   //
// defined in the modules "process_definition_rcl" and "recola1_interface_rcl"//
// The processes are defined by calling "define_process_rcl" which requires a //
// unique process number, a process string and the order of computation. Note //
// that by default all powers in fundamental couplings are selected.          //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

// Two processes are defined:
// 1) Scattering at NLO of a pair of down-antidown producing a pair of 
//    top-antitop and a pair of bottom-antibottom. The bottom and the 
//    anti-bottom are set as left-handed. 
// 2) Scattering at LO of a pair of down-antidown producing a pair of 
//    top-antitop and a pair of bottom-antibottom and an extra gluon. 
//    The bottom and the anti-bottom are set as left-handed. 
  Recola::define_process_rcl(1,"d d~ -> t t~ b[-] b~[+]","NLO");
  Recola::define_process_rcl(2,"d d~ -> t t~ b[-] b~[+] g","LO");

// Selection of QCD contributions for process 1:
// All powers of gs are unselected for Born amplitude, except for gs^4.
  Recola::unselect_all_gs_powers_BornAmpl_rcl(1);
  Recola::select_gs_power_BornAmpl_rcl(1,4);
// These selection are equivalent to the more general calls:
//Recola::unselect_all_powers_BornAmpl_rcl(1);
//Recola::select_power_BornAmpl_rcl(1,"QCD",4);
//Recola::select_power_BornAmpl_rcl(1,"QED",0);

// All powers of gs are unselected for Loop amplitude, except for gs^6.
  Recola::unselect_all_gs_powers_LoopAmpl_rcl(1);
  Recola::select_gs_power_LoopAmpl_rcl(1,6);

// Selection of QCD contributions for process 2:
// All powers of gs are unselected for Born amplitude, except for gs^5.
  Recola::unselect_all_gs_powers_BornAmpl_rcl(2);
  Recola::select_gs_power_BornAmpl_rcl(2,5);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Step 3                                                                     //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// The skeleton of the recursive procedure is built for all defined           //
// processes, by calling the subroutine "generate_processes_rcl".             //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

  Recola::generate_processes_rcl();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Step 4                                                                     //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// The fourth step is the actual computation of processes.                    //
// Each process defined at step 2 can be computed at this stage for given     //
// phase-space points provided by the user.                                   //
// The computation of (squared) amplitudes is carried out by calling the      //
// subroutine "compute_process_rcl".                                          //
// In the module "process_computation_rcl" other useful                       //
// methods are defined, which allow to get the value for specific             //
// contributions to amplitudes, squared amplitudes, and                       //
// Born colour- and/or spin-correlated squared amplitudes.                    //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

// Momenta of the phase-space point for process 1
  double p[6][4] =
  {{           250.,              0.,              0.,            250.},
   {           250.,              0.,              0.,           -250.},
   {  191.521737019,   -72.989672519,    23.203308008,   -28.573588393},
   {  187.323053415,    41.294104557,    43.349089605,   -38.824472950},
   {  67.060209357,     -5.935268585,   -28.633411230,    60.180744654},
   {  54.095000210,     37.630836547,   -37.918986384,     7.217316689}};

// Running of alpha_s for this phase-space point.
// The running value is computed by RECOLA at the CM energy 
// (p(0,0)+p(1,0) = 500 GeV), in the variable flavour scheme, with 
// one-loop running.
  Recola::compute_running_alphas_rcl (p[0][0]+p[1][0],-1,1);

// Compute process 1
  Recola::compute_process_rcl(1,p,"NLO");

// Momenta of the phase-space point for process 2
  double k[7][4] =
          {{250.000000000,   0.000000000,   0.000000000, 250.000000000},
           {250.000000000,   0.000000000,   0.000000000,-250.000000000},
           {187.692956406, -59.993359307,  28.357226683, -28.758832003},
           {186.301598855,  38.377203074,  43.056475804, -37.193761727},
           { 52.741902453,  -3.942491762, -17.679399804,  49.329036639},
           { 44.020044292,  33.185994848, -28.199232244,   4.583377168},
           { 29.243497994,  -7.627346853, -25.535070439,  12.040179922}};

// Running of alpha_s for this phase-space point.
// The running value is computed by RECOLA at the CM energy 
// (k(0,1)+k(0,2) = 500 GeV), in the variable flavour scheme, with 
// one-loop running.
  Recola::compute_running_alphas_rcl (k[0][0]+k[1][0],-1,1);

// Compute process 2
  Recola::compute_process_rcl(2,k,"LO");

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Step 5                                                                     //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Calling reset_recola_rcl (module reset_rcl), deallocates all processes     //
// generated in the previous steps and allows for the next call of Recola.    //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

  Recola::reset_recola_rcl();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

  return 0;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
