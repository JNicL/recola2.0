#include "recola.hpp"
#include <iostream> 
using namespace std;
using namespace Recola;

int main(int argc, char *argv[])
{
    
  double pi = 3.141592653589793238462643;

  double one4pi = 0.079577471545947667884441881686257;
  double mtop =173.3;
  double mu = 8000;

  set_mu_ir_rcl(mu);
  set_mu_uv_rcl(mu);
  set_alphas_rcl(one4pi,mu,5);
  double normq = 2.*4./3.;
  double normg = 2.*3.;
  
  set_output_file_rcl("*");
  set_delta_ir_rcl(0.,pi*pi/12.);
  set_print_level_squared_amplitude_rcl(0);
  set_print_level_correlations_rcl (0);
  set_pole_mass_top_rcl(mtop,0.);


  define_process_rcl(1,"g g -> t t~","NLO");
  unselect_all_gs_powers_BornAmpl_rcl(1);
  select_gs_power_BornAmpl_rcl(1,2);
  unselect_all_gs_powers_LoopAmpl_rcl(1);
  select_gs_power_LoopAmpl_rcl(1,4);

  generate_processes_rcl();

  double pin[2][4] =
     {{4000., 0., 0., 4000.},
      {4000., 0., 0., -4000.}};
  double p[4][4];
  set_outgoing_momenta_rcl(1, pin, p);

  //! LO color conservation
  compute_process_rcl(1,p,"LO");


  double m;
  get_squared_amplitude_rcl(1,2,"LO",m);
  
  double cc12,cc13,cc14,cc34;
  compute_all_colour_correlations_rcl(1,p);
  get_colour_correlation_rcl(1,2,1,2,cc12);
  get_colour_correlation_rcl(1,2,1,3,cc13);
  get_colour_correlation_rcl(1,2,1,4,cc14);
  get_colour_correlation_rcl(1,2,3,4,cc34);
  cout << "HERE" << endl;

  cout.precision(17);
  cout << endl;

  cout << "Tree -level" << endl;
  cout << "2Re<A0|A0>*CA =  2Re<A0|T1.T1|A0> :" << m*normg << endl;
  cout << "  2Re<A0|(-T1.T2-T1.T3-T1.T4)|A0> :" << (-cc12-cc13-cc14)*normg << endl;
  cout << endl;
  cout << "Color conservation:" << endl;
  cout << " 2Re<A0|(T1.T1+T1.T2+T1.T3+T1.T4)|A0> :" << (m+cc12+cc13+cc14)*normg  << endl;
  cout << endl;
  cout << " T3.T4 = (C1+C2-C3-C4)/2 + T1.T2  (Eq A.7 in hep-ph/9605323)" << endl;
  cout << "  2Re<A0|(T3.T4)|A0>                   :" << (cc34)*normq << endl;
  cout << "  2Re<A0|((C1+C2-C3-C4)/2 + T1.T2)|A0> :" << ((normg-normq)*m + cc12*normg) << endl;
  cout << endl;
  cout << endl;
  
  compute_process_rcl(1,p,"NLO");
  get_squared_amplitude_rcl(1,3,"NLO",m);
  
  //compute_all_colour_correlations_int_rcl(1,p);
  rescale_colour_correlation_rcl(1,1,2,"NLO",cc12);
  //get_colour_correlation_int_rcl(1,3,1,2,cc12);
  rescale_colour_correlation_rcl(1,1,3,"NLO",cc13);
  //get_colour_correlation_int_rcl(1,3,1,3,cc13);
  rescale_colour_correlation_rcl(1,1,4,"NLO",cc14);
  //get_colour_correlation_int_rcl(1,3,1,4,cc14);
  rescale_colour_correlation_rcl(1,3,4,"NLO",cc34);
  get_colour_correlation_rcl(1,3,3,4,"NLO",cc34);

  cout << endl;
  cout << endl;
  cout << "2Re<A0|A1>*CA =  2Re<A0|T1.T1|A1> :" << m << endl;
  cout << "  -2Re<A0|(T1.T2-T1.T3-T1.T4)|A1> :" << (-cc12-cc13-cc14) << endl;
  cout << endl;
  cout << "Color conservation:" << endl;
  cout << " 2Re<A0|(T1.T1+T1.T2+T1.T3+T1.T4)|A1> :" << (m+cc12+cc13+cc14) << endl;
  cout << endl;
  cout << " T3.T4 = (C1+C2-C3-C4)/2 + T1.T2  (Eq A.7 in hep-ph/9605323)" << endl;
  cout << "  2Re<A0|(T3.T4)|A0>                   :" << (cc34)*normq << endl;
  cout << "  2Re<A0|((C1+C2-C3-C4)/2 + T1.T2)|A0> :" << ((normg-normq)*m + cc12*normg) << endl;
  
  reset_recola_rcl();

  return 0;
}
