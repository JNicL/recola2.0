!******************************************************************************!
!                                                                              !
!    tree_vertices_rcl.f90                                                     !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

  module tree_vertices_rcl

!------------------------------------------------------------------------------!

  use globals_rcl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine tree2 ( ps,p,pl,m,den,cop,ty,wp1,wp2 )

    integer,      intent (in)  :: ty
    complex (dp), intent (in)  :: ps,m,den,cop(:),p(0:3),pl(1:4), &
                                  wp1(0:3)
    complex (dp), intent (out) :: wp2(0:3)

    complex (dp) :: cp(4),spr,fac1,fac2,fac3,fac4

    ! A 2-point vertex are never the last vertex
    cp = - cop/den

    select case (ty)

    case (1001) ! s -> s
    ! currents: wp1 -> wp2
    ! coupling = i*( cop(1)*ps - cop(2) ), propagator = i/(ps-m2)

      wp2(0) = (cp(1)*ps - cp(2))*wp1(0)
      wp2(1:3) = cnul

    case (1002) ! v -> s

      wp2 = cnul

    case (1011) ! s -> v

      wp2 = cnul

    case (1012) ! v -> v
    ! currents: wp1^mu -> wp2^nu
    ! coupling = - i*( ( cop(1)*p^2 - cop(2) )*g_{mu,ro} + cop(3)*p_mu*p_ro )
    ! propagator = - i*g^{ro,nu}/(ps-m2)

      spr = p(0)*wp1(0) - sum(p(1:3)*wp1(1:3))

      wp2 = (cp(1)*ps - cp(2))*wp1 + cp(3)*spr*p

    case (1021) ! f -> f, massive
    ! currents: wp1(i) -> wp2(j)
    ! coupling = i*(
    !            + cop(1)*pslash*omega_-(k,i)
    !            + cop(2)*pslash*omega_+(k,i)
    !            + cop(3)*omega_-(k,i)
    !            + cop(4)*omega_+(k,i)
    !            )
    ! propagator = i*(pslash+m)(j,k)/(ps-m2); pslash = p^mu*gamma_mu

      fac1 = + cp(1)*m + cp(3)
      fac2 = + cp(2)*m + cp(4)
      fac3 = + cp(3)*m + cp(1)*ps
      fac4 = + cp(4)*m + cp(2)*ps

      wp2(0:1) = fac4*wp1(0:1)
      wp2(2:3) = fac3*wp1(2:3)

      wp2(0) = wp2(0) + fac1*( - pl(1)*wp1(2) - pl(4)*wp1(3) )
      wp2(1) = wp2(1) + fac1*( - pl(3)*wp1(2) - pl(2)*wp1(3) )
      wp2(2) = wp2(2) + fac2*( - pl(2)*wp1(0) + pl(4)*wp1(1) )
      wp2(3) = wp2(3) + fac2*( + pl(3)*wp1(0) - pl(1)*wp1(1) )

    case (1022) ! f -> f, massless (m2=0, c3=c4=0)
    ! currents: wp1(i) -> wp2(j)
    ! coupling = i*(
    !            + cop(1)*pslash*omega_-(k,i)
    !            + cop(2)*pslash*omega_+(k,i)
    !            )
    ! propagator = i*pslash(j,k)/ps; pslash = p^mu*gamma_mu

      fac3 = + cp(1)*ps
      fac4 = + cp(2)*ps

      wp2(0:1) = fac4*wp1(0:1)
      wp2(2:3) = fac3*wp1(2:3)

    case (1023) ! f_- -> f_-, massless (m2=0, c3=c4=0)
    ! currents: wp1(i) -> wp2(j)
    ! coupling = i*cop(1)*pslash*omega_-(k,i)
    ! propagator = i*pslash(j,k)/ps; pslash = p^mu*gamma_mu

      fac3 = + cp(1)*ps

      wp2(0:1) = cnul
      wp2(2:3) = fac3*wp1(2:3)

    case (1024) ! f_+ -> f_+, massless (m2=0, c3=c4=0)
    ! currents: wp1(i) -> wp2(j)
    ! coupling = i*cop(2)*pslash*omega_+(k,i)
    ! propagator = i*pslash(j,k)/ps; pslash = p^mu*gamma_mu

      fac4 = + cp(2)*ps

      wp2(0:1) = fac4*wp1(0:1)
      wp2(2:3) = cnul

    case (1041) ! f~ -> f~, massive
    ! currents: wp1(i) -> wp2(j)
    ! coupling = i*(
    !            - cop(1)*pslash*omega_-(i,k)
    !            - cop(2)*pslash*omega_+(i,k)
    !            + cop(3)*omega_-(i,k)
    !            + cop(4)*omega_+(i,k)
    !            )
    ! propagator = i*(-pslash+m)(k,j)/(ps-m2); pslash = p^mu*gamma_mu

      fac1 = - cp(1)*m - cp(4)
      fac2 = - cp(2)*m - cp(3)
      fac3 = + cp(3)*m + cp(2)*ps
      fac4 = + cp(4)*m + cp(1)*ps

      wp2(0:1) = fac4*wp1(0:1)
      wp2(2:3) = fac3*wp1(2:3)

      wp2(0) = wp2(0) + fac2*( - pl(2)*wp1(2) + pl(3)*wp1(3) )
      wp2(1) = wp2(1) + fac2*( + pl(4)*wp1(2) - pl(1)*wp1(3) )
      wp2(2) = wp2(2) + fac1*( - pl(1)*wp1(0) - pl(3)*wp1(1) )
      wp2(3) = wp2(3) + fac1*( - pl(4)*wp1(0) - pl(2)*wp1(1) )

    case (1042) ! f~ -> f~, massless (m2=0, c3=c4=0)
    ! currents: wp1(i) -> wp2(j)
    ! coupling = i*(
    !            - cop(1)*pslash*omega_-(i,k)
    !            - cop(2)*pslash*omega_+(i,k)
    !            )
    ! propagator = -i*pslash(k,j)/ps; pslash = p^mu*gamma_mu

      fac3 = + cp(2)*ps
      fac4 = + cp(1)*ps

      wp2(0:1) = fac4*wp1(0:1)
      wp2(2:3) = fac3*wp1(2:3)

    case (1043) ! f~_- -> f~_-, massless (m2=0, c3=c4=0)
    ! currents: wp1(i) -> wp2(j)
    ! coupling = - i*cop(1)*pslash*omega_-(i,k)
    ! propagator = -i*pslash(k,j)/ps; pslash = p^mu*gamma_mu

      fac3 = + cp(2)*ps

      wp2(0:1) = cnul
      wp2(2:3) = fac3*wp1(2:3)

    case (1044) ! f~_+ -> f~_+, massless (m2=0, c3=c4=0)
    ! currents: wp1(i) -> wp2(j)
    ! coupling = - i*cop(1)*pslash*omega_-(i,k)
    ! propagator = -i*pslash(k,j)/ps; pslash = p^mu*gamma_mu

      fac4 = + cp(1)*ps

      wp2(0:1) = fac4*wp1(0:1)
      wp2(2:3) = cnul

    case default

      call error_rcl('Wrong 2-leg interaction', where='tree2')

    end select

  end subroutine tree2

!------------------------------------------------------------------------------!

  subroutine tree3(last,p1,p2,pl1,pl2,m,den,cop,ty,wp1,wp2,wp3)

    logical,      intent (in)  :: last
    integer,      intent (in)  :: ty
    complex (dp), intent (in)  :: m,den,cop(:),p1(0:3),p2(0:3), &
                                  pl1(1:4),pl2(1:4),wp1(0:3),wp2(0:3)
    complex (dp), intent (out) :: wp3(0:3)

    complex (dp) :: cp(size(cop)),cc1,cc2,cc,wpr,spr,spr1,spr2,              &
                    p13(0:3),p23(0:3),p12(0:3),pl(4),                &
                    prd1(0:3),prd2(0:3),                             &
                    wab02,wab13,wab12,wab03,wab20,wab31,wab21,wab30, &
                    auxa,auxb,auxc,auxd,wap(4)

    ! set overall factors for currents
    if (last) then
      cp = cima*cop
    else
      cp = - cop/den
    endif

    ! select interaction type and build currents

    select case (ty)

    case (1) ! s + s -> s
    ! currents: wp1, wp2 -> wp3
    ! coupling = i*cop(1), propagator = i/(ps-m2)

      wp3(0) = cp(1) * wp1(0) * wp2(0)
      wp3(1:3) = cnul

    case (2) ! v + v -> s
    ! currents: wp1^mu, wp2^nu -> wp3
    ! coupling = i*cop(1)*g_{mu,nu}, propagator = i/(ps-m2)

      wpr = wp1(0)*wp2(0) - sum(wp1(1:3)*wp2(1:3))
      wp3(0) = cp(1) * wpr
      wp3(1:3) = cnul

    case (3) ! s + v -> s
    ! currents: wp1, wp2^mu -> wp3
    ! coupling = i*cop(1)*(p1-p3)_mu, propagator = i/(ps-m2)

      p13 = 2*p1 + p2 ! p13 = p1 - p3
      spr = p13(0)*wp2(0) - sum(p13(1:3)*wp2(1:3))
      wp3(0) = cp(1) * wp1(0) * spr
      wp3(1:3) = cnul

    case (4) ! v + s -> s
    ! currents: wp1^mu, wp2 -> wp3
    ! coupling = i*cop(1)*(p3-p2)_mu, propagator = i/(ps-m2)

      p23 = - 2*p2 - p1 ! p23 = p3 - p2
      spr = p23(0)*wp1(0) - sum(p23(1:3)*wp1(1:3))
      wp3(0) = cp(1) * wp2(0) * spr
      wp3(1:3) = cnul

    case (5,6) ! f + f~ -> s,  f~ + f -> s
    ! currents: wp1(i), wp2(j) -> wp3
    ! coupling = i*( cop(1)*omega_+(i,j) + cop(2)*omega_-(i,j) )
    ! propagator = i/(ps-m2)

      spr1 = wp1(2)*wp2(2) + wp1(3)*wp2(3)
      spr2 = wp1(0)*wp2(0) + wp1(1)*wp2(1)
      wp3(0) = cp(2)*spr1 + cp(1)*spr2
      wp3(1:3) = cnul

    case (7:8) ! f_- + f~_- -> s,  f~_- + f_- -> s
    ! currents: wp1(i), wp2(j) -> wp3 (wp1(0:1)=wp2(0:1)=0)

      spr1 = wp1(2)*wp2(2) + wp1(3)*wp2(3)
      wp3(0) = cp(1) * spr1
      wp3(1:3) = cnul

    case (9:10) ! f_+ + f~_+ -> s,  f~_+ + f_+ -> s
    ! currents: wp1(i), wp2(j) -> wp3 (wp1(2:3)=wp2(2:3)=0)

      spr2 = wp1(0)*wp2(0) + wp1(1)*wp2(1)
      wp3(0) = cp(1) * spr2
      wp3(1:3) = cnul

    case (11) ! s + s -> v
    ! currents: wp1, wp2 -> wp3^mu
    ! coupling = i*cop(1)*(p2-p1)^nu, propagator = - i*g_nu^mu/(ps-m2)

      p12 = p1 - p2
      cc1 = wp1(0) * wp2(0) * cp(1)
      wp3 = cc1 * p12
      if (last) wp3(0) = - wp3(0)

    case (12) ! v + v -> v
    ! currents: wp1^mu, wp2^nu -> wp3^ro
    ! coupling = - i*(-cop(1))*(
    !              + g_{mu,nu}*(p2-p1)_si
    !              + g_{nu,si}*(p3-p2)_mu
    !              + g_{si,mu}*(p1-p3)_nu
    !              )
    ! propagator = - i*g^{si,ro}/(ps-m2)

      p12 = p1 - p2
      p23 = 2*p2 + p1 ! p23 = p2 - p3
      p13 = 2*p1 + p2 ! p13 = p1 - p3
      spr1 = p23(0)*wp1(0) - sum(p23(1:3)*wp1(1:3))
      spr2 = p13(0)*wp2(0) - sum(p13(1:3)*wp2(1:3))
      wpr  = wp2(0)*wp1(0) - sum(wp2(1:3)*wp1(1:3))
      cc1 = - cp(1) * spr2
      cc2 =   cp(1) * spr1
      cc  =   cp(1) * wpr
      wp3 = cc1*wp1 + cc2*wp2 + cc*p12
      if (last) wp3(0) = - wp3(0)

    case (13) ! s + v -> v
    ! currents: wp1, wp2^mu -> wp3^nu
    ! coupling = i*cop(1)*g_{mu,ro}, propagator = - i*g^{ro,nu}/(ps-m2)

      cc2 = - cp(1) * wp1(0)
      wp3 = cc2 * wp2
      if (last) wp3(0) = - wp3(0)

    case (14) ! v + s -> v
    ! currents: wp1^mu, wp2 -> wp3^nu
    ! coupling = i*cop(1)*g_{mu,ro}, propagator = - i*g^{ro,nu}/(ps-m2)

      cc1 = - cp(1)*wp2(0)
      wp3 = cc1 * wp1
      if (last) wp3(0) = - wp3(0)

    case (15) ! f + f~ -> v
    ! currents: wp1(i), wp2(j) -> wp3^mu
    ! coupling = i*gamma^ro(j,k)*( cop(1)*omega_+(k,i) + cop(2)*omega_-(k,i) )
    ! propagator = - i*g_ro^mu/(ps-m2)

      wab02 = wp2(0)*wp1(2)
      wab13 = wp2(1)*wp1(3)
      wab12 = wp2(1)*wp1(2)
      wab03 = wp2(0)*wp1(3)
      wab20 = wp2(2)*wp1(0)
      wab31 = wp2(3)*wp1(1)
      wab21 = wp2(2)*wp1(1)
      wab30 = wp2(3)*wp1(0)
      prd1(0) =     wab02 + wab13
      prd1(1) =   - wab12 - wab03
      prd1(2) = ( - wab12 + wab03 )*cima
      prd1(3) =   - wab02 + wab13
      prd2(0) =     wab20 + wab31
      prd2(1) =     wab30 + wab21
      prd2(2) = (   wab30 - wab21 )*cima
      prd2(3) =     wab20 - wab31

      wp3 = cp(2)*prd1 + cp(1)*prd2
      if (last) wp3(0) = - wp3(0)

    case (16) ! f~ + f -> v
    ! currents: wp1(i), wp2(j) -> wp3^mu
    ! coupling = i*gamma^ro(i,k)*( cop(1)*omega_+(k,j) + cop(2)*omega_-(k,j) )
    ! propagator = - i*g_ro^mu/(ps-m2)

      wab02 = wp1(0)*wp2(2)
      wab13 = wp1(1)*wp2(3)
      wab12 = wp1(1)*wp2(2)
      wab03 = wp1(0)*wp2(3)
      wab20 = wp1(2)*wp2(0)
      wab31 = wp1(3)*wp2(1)
      wab21 = wp1(2)*wp2(1)
      wab30 = wp1(3)*wp2(0)
      prd1(0) =     wab02 + wab13
      prd1(1) =   - wab12 - wab03
      prd1(2) = ( - wab12 + wab03 )*cima
      prd1(3) =   - wab02 + wab13
      prd2(0) =     wab20 + wab31
      prd2(1) =     wab30 + wab21
      prd2(2) = (   wab30 - wab21 )*cima
      prd2(3) =     wab20 - wab31

      wp3 = cp(2)*prd1 + cp(1)*prd2
      if (last) wp3(0) = - wp3(0)

    case (17) ! f_- + f~_+ -> v
    ! currents: wp1(i), wp2(j) -> wp3^mu (wp1(0:1)=wp2(2:3)=0)
    ! coupling = i*gamma^ro(j,k)*( cop(1)*omega_-(k,i) + cop(2)*omega_+(k,i) )
    ! propagator = - i*g_ro^mu/(ps-m2)

      wab02 = wp2(0)*wp1(2)
      wab13 = wp2(1)*wp1(3)
      wab12 = wp2(1)*wp1(2)
      wab03 = wp2(0)*wp1(3)
      prd1(0) =     wab02 + wab13
      prd1(1) =   - wab12 - wab03
      prd1(2) = ( - wab12 + wab03 )*cima
      prd1(3) =   - wab02 + wab13

      wp3 = cp(1) * prd1
      if (last) wp3(0) = - wp3(0)

    case (18) ! f~_+ + f_- -> v
    ! currents: wp1(i), wp2(j) -> wp3^mu (wp1(2:3)=wp2(0:1)=0)
    ! coupling = i*gamma^ro(i,k)*( cop(1)*omega_-(k,j) + cop(2)*omega_+(k,j) )
    ! propagator = - i*g_ro^mu/(ps-m2)

      wab02 = wp1(0)*wp2(2)
      wab13 = wp1(1)*wp2(3)
      wab12 = wp1(1)*wp2(2)
      wab03 = wp1(0)*wp2(3)
      prd1(0) =     wab02 + wab13
      prd1(1) =   - wab12 - wab03
      prd1(2) = ( - wab12 + wab03 )*cima
      prd1(3) =   - wab02 + wab13

      wp3 = cp(1) * prd1
      if (last) wp3(0) = - wp3(0)

    case (19) ! f_+ + f~_- -> v
    ! currents: wp1(i), wp2(j) -> wp3^mu (wp1(2:3)=wp2(0:1)=0)
    ! coupling = i*gamma^ro(j,k)*( cop(1)*omega_-(k,i) + cop(2)*omega_+(k,i) )
    ! propagator = - i*g_ro^mu/(ps-m2)

      wab20 = wp2(2)*wp1(0)
      wab31 = wp2(3)*wp1(1)
      wab21 = wp2(2)*wp1(1)
      wab30 = wp2(3)*wp1(0)
      prd2(0) =   wab20 + wab31
      prd2(1) =   wab30 + wab21
      prd2(2) = ( wab30 - wab21 )*cima
      prd2(3) =   wab20 - wab31

      wp3 = cp(1) * prd2
      if (last) wp3(0) = - wp3(0)

    case (20) ! f~_- f_+ -> v
    ! currents: wp1(i), wp2(j) -> wp3^mu (wp1(0:1)=wp2(2:3)=0)
    ! coupling = i*gamma^ro(i,k)*( cop(1)*omega_-(k,j) + cop(2)*omega_+(k,j) )
    ! propagator = - i*g_ro^mu/(ps-m2)

      wab20 = wp1(2)*wp2(0)
      wab31 = wp1(3)*wp2(1)
      wab21 = wp1(2)*wp2(1)
      wab30 = wp1(3)*wp2(0)
      prd2(0) =   wab20 + wab31
      prd2(1) =   wab30 + wab21
      prd2(2) = ( wab30 - wab21 )*cima
      prd2(3) =   wab20 - wab31

      wp3 = cp(1) * prd2
      if (last) wp3(0) = - wp3(0)

    case (21) ! f + s -> f
    ! currents: wp1(i), wp2 -> wp3(j)
    ! coupling = i*( cop(1)*omega_+(k,i) + cop(2)*omega_-(k,i) )
    ! propagator = i*(pslash+m)(j,k)/(ps-m2); pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxa = cp(1)*wp1(0)*wp2(0)
      auxb = cp(1)*wp1(1)*wp2(0)
      auxc = cp(2)*wp1(2)*wp2(0)
      auxd = cp(2)*wp1(3)*wp2(0)
      select case (last)
      case (.false.)
        wp3(0)= - auxc*pl(1) - auxd*pl(4) + auxa*m
        wp3(1)= - auxc*pl(3) - auxd*pl(2) + auxb*m
        wp3(2)= - auxa*pl(2) + auxb*pl(4) + auxc*m
        wp3(3)=   auxa*pl(3) - auxb*pl(1) + auxd*m
      case (.true.)
        wp3(0)= auxa
        wp3(1)= auxb
        wp3(2)= auxc
        wp3(3)= auxd
      end select

    case (23) ! s + f -> f
    ! currents: wp1, wp2(i) -> wp3(j)
    ! coupling = i*( cop(1)*omega_+(k,i) + cop(2)*omega_-(k,i) )
    ! propagator = i*(pslash+m)(j,k)/(ps-m2); pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxa = cp(1)*wp2(0)*wp1(0)
      auxb = cp(1)*wp2(1)*wp1(0)
      auxc = cp(2)*wp2(2)*wp1(0)
      auxd = cp(2)*wp2(3)*wp1(0)
      select case (last)
      case (.false.)
        wp3(0)= - auxc*pl(1) - auxd*pl(4) + auxa*m
        wp3(1)= - auxc*pl(3) - auxd*pl(2) + auxb*m
        wp3(2)= - auxa*pl(2) + auxb*pl(4) + auxc*m
        wp3(3)=   auxa*pl(3) - auxb*pl(1) + auxd*m
      case (.true.)
        wp3(0)= auxa
        wp3(1)= auxb
        wp3(2)= auxc
        wp3(3)= auxd
      end select

    case (25) ! f_- + s -> f_+/-
    ! currents: wp1(i), wp2 -> wp3(j) (wp1(0:1)=0)
    ! coupling = i*( cop(1)*omega_-(k,i) )
    ! propagator = i*(pslash+m)(j,k)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxc = cp(1)*wp1(2)*wp2(0)
      auxd = cp(1)*wp1(3)*wp2(0)
      select case (last)
      case (.false.)
        wp3(0) = - auxc*pl(1) - auxd*pl(4)
        wp3(1) = - auxc*pl(3) - auxd*pl(2)
        wp3(2)=                           + auxc*m
        wp3(3)=                           + auxd*m
      case (.true.)
        wp3(0:1) = cnul
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (26) ! s + f_- -> f_+/-
    ! currents: wp1, wp2(i) -> wp3(j) (wp2(0:1)=0)
    ! coupling = i*( cop(1)*omega_-(k,i) )
    ! propagator = i*(pslash+m)(j,k)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxc = cp(1)*wp2(2)*wp1(0)
      auxd = cp(1)*wp2(3)*wp1(0)
      select case (last)
      case (.false.)
        wp3(0) = - auxc*pl(1) - auxd*pl(4)
        wp3(1) = - auxc*pl(3) - auxd*pl(2)
        wp3(2) =                           + auxc*m
        wp3(3) =                           + auxd*m
      case (.true.)
        wp3(0:1) = cnul
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (27) ! f_+ + s -> f_-/+
    ! currents: wp1(i), wp2 -> wp3(j) (wp1(2:3)=0)
    ! coupling = i*( cop(1)*omega_+(k,i) )
    ! propagator = i*(pslash+m)(j,k)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxa = cp(1)*wp1(0)*wp2(0)
      auxb = cp(1)*wp1(1)*wp2(0)
      select case (last)
      case (.false.)
        wp3(0) =                           + auxa*m
        wp3(1) =                           + auxb*m
        wp3(2) = - auxa*pl(2) + auxb*pl(4)
        wp3(3) =   auxa*pl(3) - auxb*pl(1)
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2:3) = cnul
      end select

    case (28) ! s + f_+ -> f_-/+
    ! currents: wp1, wp2(i) -> wp3(j) (wp2(2:3)=0)
    ! coupling = i*( cop(1)*omega_+(k,i) )
    ! propagator = i*(pslash+m)(j,k)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxa = cp(1)*wp2(0)*wp1(0)
      auxb = cp(1)*wp2(1)*wp1(0)
      select case (last)
      case (.false.)
        wp3(0) =                           + auxa*m
        wp3(1) =                           + auxb*m
        wp3(2) = - auxa*pl(2) + auxb*pl(4)
        wp3(3) =   auxa*pl(3) - auxb*pl(1)
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2:3) = cnul
      end select

    case (31) ! f + v -> f
    ! currents: wp1(i), wp2^mu -> wp3(j)
    ! coupling = i*gamma_mu(l,k)*( cop(1)*omega_+(k,i) + cop(2)*omega_-(k,i) )
    ! propagator = i*(pslash+m)(j,l)/(ps-m2); pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp2(0) + wp2(3)
      wap(2) = wp2(0) - wp2(3)
      wap(3) = wp2(1) + cima*wp2(2)
      wap(4) = wp2(1) - cima*wp2(2)
      auxa = cp(2)*( - wp1(2)*wap(1) - wp1(3)*wap(4) )
      auxb = cp(2)*( - wp1(2)*wap(3) - wp1(3)*wap(2) )
      auxc = cp(1)*( - wp1(0)*wap(2) + wp1(1)*wap(4) )
      auxd = cp(1)*(   wp1(0)*wap(3) - wp1(1)*wap(1) )
      select case (last)
      case (.false.)
        wp3(0) = - auxc*pl(1) - auxd*pl(4) + auxa*m
        wp3(1) = - auxc*pl(3) - auxd*pl(2) + auxb*m
        wp3(2) = - auxa*pl(2) + auxb*pl(4) + auxc*m
        wp3(3) =   auxa*pl(3) - auxb*pl(1) + auxd*m
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (33) ! v + f -> f
    ! currents: wp1^mu, wp2(i) -> wp3(j)
    ! coupling = i*gamma_mu(l,k)*( cop(1)*omega_+(k,i) + cop(2)*omega_-(k,i) )
    ! propagator = i*(pslash+m)(j,l)/(ps-m2); pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp1(0) + wp1(3)
      wap(2) = wp1(0) - wp1(3)
      wap(3) = wp1(1) + cima*wp1(2)
      wap(4) = wp1(1) - cima*wp1(2)
      auxa = cp(2)*( - wp2(2)*wap(1) - wp2(3)*wap(4) )
      auxb = cp(2)*( - wp2(2)*wap(3) - wp2(3)*wap(2) )
      auxc = cp(1)*( - wp2(0)*wap(2) + wp2(1)*wap(4) )
      auxd = cp(1)*(   wp2(0)*wap(3) - wp2(1)*wap(1) )
      select case (last)
      case (.false.)
        wp3(0) = - auxc*pl(1) - auxd*pl(4) + auxa*m
        wp3(1) = - auxc*pl(3) - auxd*pl(2) + auxb*m
        wp3(2) = - auxa*pl(2) + auxb*pl(4) + auxc*m
        wp3(3) =   auxa*pl(3) - auxb*pl(1) + auxd*m
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (35) ! f_- + v -> f_-/+
    ! currents: wp1(i), wp2^mu -> wp3(j) (wp1(0:1)=0)
    ! coupling = i*gamma_mu(l,k)*( cop(1)*omega_-(k,i) )
    ! propagator = i*(pslash+m)(j,l)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp2(0) + wp2(3)
      wap(2) = wp2(0) - wp2(3)
      wap(3) = wp2(1) + cima*wp2(2)
      wap(4) = wp2(1) - cima*wp2(2)
      auxa = cp(1)*( - wp1(2)*wap(1) - wp1(3)*wap(4) )
      auxb = cp(1)*( - wp1(2)*wap(3) - wp1(3)*wap(2) )

      select case (last)
      case (.false.)
        wp3(0) =                           + auxa*m
        wp3(1) =                           + auxb*m
        wp3(2) = - auxa*pl(2) + auxb*pl(4)
        wp3(3) =   auxa*pl(3) - auxb*pl(1)

      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2:3) = cnul
      end select

    case (36) ! v + f_- -> f_-/+
    ! currents: wp1^mu, wp2(i) -> wp3(j) (wp2(0:1)=0)
    ! coupling = i*gamma_mu(l,k)*( cop(1)*omega_-(k,i) + cop(2)*omega_+(k,i) )
    ! propagator = i*(pslash+m)(j,l)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp1(0) + wp1(3)
      wap(2) = wp1(0) - wp1(3)
      wap(3) = wp1(1) + cima*wp1(2)
      wap(4) = wp1(1) - cima*wp1(2)
      auxa = cp(1)*( - wp2(2)*wap(1) - wp2(3)*wap(4) )
      auxb = cp(1)*( - wp2(2)*wap(3) - wp2(3)*wap(2) )
      select case (last)
      case (.false.)
        wp3(0) =                           + auxa*m
        wp3(1) =                           + auxb*m
        wp3(2) = - auxa*pl(2) + auxb*pl(4)
        wp3(3) =   auxa*pl(3) - auxb*pl(1)
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2:3) = cnul
      end select

    case (37) ! f_+ + v -> f_+/-
    ! currents: wp1(i), wp2^mu -> wp3(j) (wp1(2:3)=0)
    ! coupling = i*gamma_mu(l,k)*( cop(1)*omega_+(k,i) )
    ! propagator = i*(pslash+m)(j,l)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp2(0) + wp2(3)
      wap(2) = wp2(0) - wp2(3)
      wap(3) = wp2(1) + cima*wp2(2)
      wap(4) = wp2(1) - cima*wp2(2)
      auxc = cp(1)*( - wp1(0)*wap(2) + wp1(1)*wap(4) )
      auxd = cp(1)*(   wp1(0)*wap(3) - wp1(1)*wap(1) )
      select case (last)
      case (.false.)
        wp3(0) = - auxc*pl(1) - auxd*pl(4)
        wp3(1) = - auxc*pl(3) - auxd*pl(2)
        wp3(2) =                           + auxc*m
        wp3(3) =                           + auxd*m
      case (.true.)
        wp3(0:1) = cnul
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (38) ! v + f_+ -> f_+/-
    ! currents: wp1^mu, wp2(i) -> wp3(j) (wp2(2:3)=0)
    ! coupling = i*gamma_mu(l,k)*( cop(1)*omega_+(k,i) )
    ! propagator = i*(pslash+m)(j,l)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp1(0) + wp1(3)
      wap(2) = wp1(0) - wp1(3)
      wap(3) = wp1(1) + cima*wp1(2)
      wap(4) = wp1(1) - cima*wp1(2)
      auxc = cp(1)*( - wp2(0)*wap(2) + wp2(1)*wap(4) )
      auxd = cp(1)*(   wp2(0)*wap(3) - wp2(1)*wap(1) )
      select case (last)
      case (.false.)
        wp3(0) = - auxc*pl(1) - auxd*pl(4)
        wp3(1) = - auxc*pl(3) - auxd*pl(2)
        wp3(2) =                           + auxc*m
        wp3(3) =                           + auxd*m
      case (.true.)
        wp3(0:1) = cnul
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (41) ! f~ + s -> f~
    ! currents: wp1(i), wp2 -> wp3(j)
    ! coupling = i*( cop(1)*omega_+(i,k) + cop(2)*omega_-(i,k) )
    ! propagator = i*(-pslash+m)(k,j)/(ps-m2); pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxa = cp(1)*wp1(0)*wp2(0)
      auxb = cp(1)*wp1(1)*wp2(0)
      auxc = cp(2)*wp1(2)*wp2(0)
      auxd = cp(2)*wp1(3)*wp2(0)
      select case (last)
      case (.false.)
        wp3(0) =   auxc*pl(2) - auxd*pl(3) + auxa*m
        wp3(1) = - auxc*pl(4) + auxd*pl(1) + auxb*m
        wp3(2) =   auxa*pl(1) + auxb*pl(3) + auxc*m
        wp3(3) =   auxa*pl(4) + auxb*pl(2) + auxd*m
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (43) ! s + f~ -> f~
    ! currents: wp1, wp2(i) -> wp3(j)
    ! coupling = i*( cop(1)*omega_+(i,k) + cop(2)*omega_-(i,k) )
    ! propagator = i*(-pslash+m)(k,j)/(ps-m2); pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxa = cp(1)*wp2(0)*wp1(0)
      auxb = cp(1)*wp2(1)*wp1(0)
      auxc = cp(2)*wp2(2)*wp1(0)
      auxd = cp(2)*wp2(3)*wp1(0)
      select case (last)
      case (.false.)
        wp3(0) =   auxc*pl(2) - auxd*pl(3) + auxa*m
        wp3(1) = - auxc*pl(4) + auxd*pl(1) + auxb*m
        wp3(2) =   auxa*pl(1) + auxb*pl(3) + auxc*m
        wp3(3) =   auxa*pl(4) + auxb*pl(2) + auxd*m
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (45) ! f~_- + s -> f~_+/-
    ! currents: wp1(i), wp2 -> wp3(j) (wp1(0:1)=0)
    ! coupling = i*( cop(1)*omega_-(i,k) )
    ! propagator = i*(-pslash+m)(k,j)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxc = cp(1)*wp1(2)*wp2(0)
      auxd = cp(1)*wp1(3)*wp2(0)
      select case (last)
      case (.false.)
        wp3(0) =   auxc*pl(2) - auxd*pl(3)
        wp3(1) = - auxc*pl(4) + auxd*pl(1)
        wp3(2) =                           + auxc*m
        wp3(3) =                           + auxd*m
      case (.true.)
        wp3(0:1) = cnul
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (46) ! s + f~_- -> f~_+/-
    ! currents: wp1, wp2(i) -> wp3(j) (wp2(0:1)=0)
    ! coupling = i*( cop(1)*omega_-(i,k) )
    ! propagator = i*(-pslash+m)(k,j)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxc = cp(1)*wp2(2)*wp1(0)
      auxd = cp(1)*wp2(3)*wp1(0)
      select case (last)
      case (.false.)
        wp3(0) =   auxc*pl(2) - auxd*pl(3)
        wp3(1) = - auxc*pl(4) + auxd*pl(1)
        wp3(2) =                           + auxc*m
        wp3(3) =                           + auxd*m
      case (.true.)
        wp3(0:1) = cnul
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (47) ! f~_+ + s -> f~_-/+
    ! currents: wp1(i), wp2 -> wp3(j) (wp1(2:3)=0)
    ! coupling = i*( cop(1)*omega_+(i,k) )
    ! propagator = i*(-pslash+m)(k,j)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxa = cp(1)*wp1(0)*wp2(0)
      auxb = cp(1)*wp1(1)*wp2(0)
      select case (last)
      case (.false.)
        wp3(0) =                           + auxa*m
        wp3(1) =                           + auxb*m
        wp3(2) =   auxa*pl(1) + auxb*pl(3)
        wp3(3) =   auxa*pl(4) + auxb*pl(2)
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2:3) = cnul
      end select

    case (48) ! s + f~_+ -> f~_-/+
    ! currents: wp1, wp2(i) -> wp3(j) (wp2(2:3)=0)
    ! coupling = i*( cop(1)*omega_+(i,k) )
    ! propagator = i*(-pslash+m)(k,j)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      auxa = cp(1)*wp2(0)*wp1(0)
      auxb = cp(1)*wp2(1)*wp1(0)
      select case (last)
      case (.false.)
        wp3(0) =                           + auxa*m
        wp3(1) =                           + auxb*m
        wp3(2) =   auxa*pl(1) + auxb*pl(3)
        wp3(3) =   auxa*pl(4) + auxb*pl(2)
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2:3) = cnul
      end select

    case (51) ! f~ + v -> f~
    ! currents: wp1(i), wp2^mu -> wp3(j)
    ! coupling = i*gamma_mu(i,k)*( cop(1)*omega_+(k,l) + cop(2)*omega_-(k,l) )
    ! propagator = i*(-pslash+m)(l,j)/(ps-m2); pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp2(0) + wp2(3)
      wap(2) = wp2(0) - wp2(3)
      wap(3) = wp2(1) + cima*wp2(2)
      wap(4) = wp2(1) - cima*wp2(2)
      auxa = cp(1)*( - wp1(2)*wap(2) + wp1(3)*wap(3) )
      auxb = cp(1)*(   wp1(2)*wap(4) - wp1(3)*wap(1) )
      auxc = cp(2)*( - wp1(0)*wap(1) - wp1(1)*wap(3) )
      auxd = cp(2)*( - wp1(0)*wap(4) - wp1(1)*wap(2) )
      select case (last)
      case (.false.)
        wp3(0) =   auxc*pl(2) - auxd*pl(3) + auxa*m
        wp3(1) = - auxc*pl(4) + auxd*pl(1) + auxb*m
        wp3(2) =   auxa*pl(1) + auxb*pl(3) + auxc*m
        wp3(3) =   auxa*pl(4) + auxb*pl(2) + auxd*m
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (53) ! v + f~ -> f~
    ! currents: wp1^mu, wp2(i) -> wp3(j)
    ! coupling = i*gamma_mu(i,k)*( cop(1)*omega_+(k,l) + cop(2)*omega_-(k,l) )
    ! propagator = i*(-pslash+m)(l,j)/(ps-m2); pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp1(0) + wp1(3)
      wap(2) = wp1(0) - wp1(3)
      wap(3) = wp1(1) + cima*wp1(2)
      wap(4) = wp1(1) - cima*wp1(2)
      auxa = cp(1)*( - wp2(2)*wap(2) + wp2(3)*wap(3) )
      auxb = cp(1)*(   wp2(2)*wap(4) - wp2(3)*wap(1) )
      auxc = cp(2)*( - wp2(0)*wap(1) - wp2(1)*wap(3) )
      auxd = cp(2)*( - wp2(0)*wap(4) - wp2(1)*wap(2) )
      select case (last)
      case (.false.)
        wp3(0) =   auxc*pl(2) - auxd*pl(3) + auxa*m
        wp3(1) = - auxc*pl(4) + auxd*pl(1) + auxb*m
        wp3(2) =   auxa*pl(1) + auxb*pl(3) + auxc*m
        wp3(3) =   auxa*pl(4) + auxb*pl(2) + auxd*m
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (55) ! f~_- + v -> f~_-/+
    ! currents: wp1(i), wp2^mu -> wp3(j) (wp1(0:1)=0)
    ! coupling = i*gamma_mu(i,k)*( cop(1)*omega_+(k,l) )
    ! propagator = i*(-pslash+m)(l,j)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp2(0) + wp2(3)
      wap(2) = wp2(0) - wp2(3)
      wap(3) = wp2(1) + cima*wp2(2)
      wap(4) = wp2(1) - cima*wp2(2)
      auxa = cp(1)*( - wp1(2)*wap(2) + wp1(3)*wap(3) )
      auxb = cp(1)*(   wp1(2)*wap(4) - wp1(3)*wap(1) )
      select case (last)
      case (.false.)
        wp3(0) =                           + auxa*m
        wp3(1) =                           + auxb*m
        wp3(2) =   auxa*pl(1) + auxb*pl(3)
        wp3(3) =   auxa*pl(4) + auxb*pl(2)
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2:3) = cnul
      end select

    case (56) ! v + f~_- -> f~_-/+
    ! currents: wp1^mu, wp2(i) -> wp3(j) (wp2(0:1)=0)
    ! coupling = i*gamma_mu(i,k)*( cop(1)*omega_+(k,l) )
    ! propagator = i*(-pslash+m)(l,j)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp1(0) + wp1(3)
      wap(2) = wp1(0) - wp1(3)
      wap(3) = wp1(1) + cima*wp1(2)
      wap(4) = wp1(1) - cima*wp1(2)
      auxa = cp(1)*( - wp2(2)*wap(2) + wp2(3)*wap(3) )
      auxb = cp(1)*(   wp2(2)*wap(4) - wp2(3)*wap(1) )
      select case (last)
      case (.false.)
        wp3(0) =                           + auxa*m
        wp3(1) =                           + auxb*m
        wp3(2) =   auxa*pl(1) + auxb*pl(3)
        wp3(3) =   auxa*pl(4) + auxb*pl(2)
      case (.true.)
        wp3(0) = auxa
        wp3(1) = auxb
        wp3(2:3) = cnul
      end select

    case (57) ! f~_+ + v -> f~_+/-
    ! currents: wp1(i), wp2^mu -> wp3(j) (wp1(2:3)=0)
    ! coupling = i*gamma_mu(i,k)*( cop(1)*omega_-(k,l) + cop(2)*omega_+(k,l) )
    ! propagator = -i*(-pslash+m)(l,j)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp2(0) + wp2(3)
      wap(2) = wp2(0) - wp2(3)
      wap(3) = wp2(1) + cima*wp2(2)
      wap(4) = wp2(1) - cima*wp2(2)
      auxc = cp(1)*( - wp1(0)*wap(1) - wp1(1)*wap(3) )
      auxd = cp(1)*( - wp1(0)*wap(4) - wp1(1)*wap(2) )
      select case (last)
      case (.false.)
        wp3(0) =   auxc*pl(2) - auxd*pl(3)
        wp3(1) = - auxc*pl(4) + auxd*pl(1)
        wp3(2) =                           + auxc*m
        wp3(3) =                           + auxd*m
      case (.true.)
        wp3(0:1) = cnul
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (58) ! v + f~_+ -> f~_+/-
    ! currents: wp1^mu, wp2(i) -> wp3(j) (wp2(2:3)=0)
    ! coupling = i*gamma_mu(i,k)*( cop(1)*omega_-(k,l) + cop(2)*omega_+(k,l) )
    ! propagator = -i*(-pslash+m)(l,j)/ps; pslash = (p1+p2)^mu*gamma_mu

      pl = pl1 + pl2

      wap(1) = wp1(0) + wp1(3)
      wap(2) = wp1(0) - wp1(3)
      wap(3) = wp1(1) + cima*wp1(2)
      wap(4) = wp1(1) - cima*wp1(2)
      auxc = cp(1)*( - wp2(0)*wap(1) - wp2(1)*wap(3) )
      auxd = cp(1)*( - wp2(0)*wap(4) - wp2(1)*wap(2) )
      select case (last)
      case (.false.)
        wp3(0) =   auxc*pl(2) - auxd*pl(3)
        wp3(1) = - auxc*pl(4) + auxd*pl(1)
        wp3(2) =                           + auxc*m
        wp3(3) =                           + auxd*m
      case (.true.)
        wp3(0:1) = cnul
        wp3(2) = auxc
        wp3(3) = auxd
      end select

    case (VVS_heft)
      wp3(0) = cp(1)*(-P1(0)*wp2(0) + P1(1)*wp2(1) + P1(2)*wp2(2) + P1(3)*wp2(3))*(- &
             P2(0)*wp1(0) + P2(1)*wp1(1) + P2(2)*wp1(2) + P2(3)*wp1(3)) + cp(2)*(- &
             P1(0)*P2(0) + P1(1)*P2(1) + P1(2)*P2(2) + P1(3)*P2(3))*(-wp1(0)*wp2(&
             0) + wp1(1)*wp2(1) + wp1(2)*wp2(2) + wp1(3)*wp2(3))
      wp3(1:3) = cnul


    case (VSV_heft)
      wp3(:) = -wp2(0)*(-P1(0)**2*cp(2)*wp1(:) + P1(:)*cp(1)*(-wp1(0)*(P1(0) + P2(0)) + wp1&
             (1)*(P1(1) + P2(1)) + wp1(2)*(P1(2) + P2(2)) + wp1(3)*(P1(3) + P2(3 &
             ))) + cp(2)*wp1(:)*(P1(1)**2 + P1(2)**2 + P1(3)**2) + cp(2)*wp1(:)*(-P1(0)*P2(0 &
             ) + P1(1)*P2(1) + P1(2)*P2(2) + P1(3)*P2(3)))
    case (SVV_heft)
      wp3 = wp1(0)*(P2(0)**2*cp(2)*wp2(:) + P2(:)*cp(1)*(wp2(0)*(P1(0) + P2(0    &
             )) - wp2(1)*(P1(1) + P2(1)) - wp2(2)*(P1(2) + P2(2)) - wp2(3)*(P1(3  &
             ) + P2(3))) - cp(2)*wp2(:)*(P2(1)**2 + P2(2)**2 + P2(3)**2) + cp(2 &
             )*wp2(:)*(P1(0)*P2(0) - P1(1)*P2(1) - P1(2)*P2(2) - P1(3)*P2(3)))

    case (VVV_bfm)
      ! TODO:  <27-05-19, J.-N. Lang> !
      ! CHECK
      ! c0*(Metric(1,2)*P(3,1) - Metric(1,2)*P(3,2) + Metric(1,2)*P(3,3)
      !   - Metric(1,3)*P(2,1) + Metric(1,3)*P(2,3)
      !   - Metric(2,3)*P(1,1) + Metric(2,3)*P(1,2) - Metric(2,3)*P(1,3))
      p23 = 2*p2 + p1 ! p23 = p2 - p3
      spr1 = p23(0)*wp2(0) - sum(p23(1:3)*wp2(1:3))
      spr2 = p2(0)*wp1(0) - sum(p2(1:3)*wp2(1:3))
      wpr  = wp2(0)*wp1(0) - sum(wp2(1:3)*wp1(1:3))
      wp3 = cp(1)*(-wp1(:)*spr1 + 2*wp2(:)*spr2 + 2*P2(:)*wpr)

      if (last) wp3(0) = - wp3(0)

    case default

      call error_rcl('Wrong 3-leg interaction: ' // to_str(ty), where='tree3')

    end select


  end subroutine tree3

!------------------------------------------------------------------------------!

  subroutine tree4(last,p1,p2,p3,pl1,pl2,pl3,m,den,cop,ty,wp1,wp2,wp3,wp4)

    logical,     intent (in)  :: last
    integer,     intent (in)  :: ty
    complex(dp), intent (in)  :: m,den,cop(:)
    complex (dp), intent (in) :: p1(0:3),p2(0:3),p3(0:3), &
                                 pl1(1:4),pl2(1:4),pl3(1:4)
    complex(dp), intent (inout) :: wp1(0:3),wp2(0:3),wp3(0:3)
    complex(dp), intent (out)   :: wp4(0:3)

    complex(dp) :: cp(size(cop)),cc1,cc2,cc3,wpr,wpr1,wpr2,wpr3

    complex(dp) :: x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,       &
                   x11,x12,x13,x14
    complex(dp), dimension(0:3) :: x0vec,x1vec,x2vec,x3vec,x4vec, &
                                   x5vec,x6vec,x7vec

    ! overall factor
    if (last) then
      cp = cima*cop
    else
      cp = - cop/den
    endif

    ! select interaction type and contract currents

    select case (ty)

    case (-1) ! s + s + s -> s
    ! currents: wp1, wp2, wp3 -> wp4
    ! coupling = i*cop(1), propagator = i/(ps-m2)

      wp4(0) = cp(1) * wp1(0) * wp2(0) * wp3(0)
      wp4(1:3) = cnul

    case (-2) ! s + v + v -> s
    ! currents: wp1, wp2^mu, wp3^nu -> wp4
    ! coupling = i*cop(1)*g_{mu,nu}, propagator = i/(ps-m2)

      wpr = wp2(0)*wp3(0) - sum(wp2(1:3)*wp3(1:3))
      wp4(0) = cp(1) * wpr * wp1(0)
      wp4(1:3) = cnul

    case (-3) ! v + s + v -> s
    ! currents: wp1^mu, wp2, wp3^nu -> wp4
    ! coupling = i*cop(1)*g_{mu,nu}, propagator = i/(ps-m2)

      wpr = wp1(0)*wp3(0) - sum(wp1(1:3)*wp3(1:3))
      wp4(0) = cp(1) * wpr * wp2(0)
      wp4(1:3) = cnul

    case (-4) ! v + v + s -> s
    ! currents: wp1^mu, wp2^nu, wp3 -> wp4
    ! coupling = i*cop(1)*g_{mu,nu}, propagator = i/(ps-m2)

      wpr = wp2(0)*wp1(0) - sum(wp2(1:3)*wp1(1:3))
      wp4(0) = cp(1) * wpr * wp3(0)
      wp4(1:3) = cnul

    case (-11) ! v + v + v -> v
    ! currents: wp1^mu, wp2^nu, wp3^ro -> wp4^si
    ! coupling = i*(
    !            + cop(1)*g_{mu,al}*g_{nu,ro}
    !            + cop(2)*g_{mu,ro}*g_{nu,al}
    !            + cop(3)*g_{mu,nu}*g_{ro,al}
    !            )
    ! propagator = - i*g^{al,si}/(ps-m2)

      wpr1 = wp2(0)*wp3(0) - sum(wp2(1:3)*wp3(1:3))
      wpr2 = wp1(0)*wp3(0) - sum(wp1(1:3)*wp3(1:3))
      wpr3 = wp2(0)*wp1(0) - sum(wp2(1:3)*wp1(1:3))
      cc1 = - cp(1)*wpr1
      cc2 = - cp(2)*wpr2
      cc3 = - cp(3)*wpr3
      wp4 = cc1*wp1 + cc2*wp2 + cc3*wp3
      if (last) wp4(0) = - wp4(0)


    case (-12) ! s + s + v -> v
    ! currents: wp1, wp2, wp3^mu -> wp4^nu
    ! coupling = i*cop(1)*g_{mu,al}, propagator = i*g^{al,nu}/(ps-m2)

      cc3 = - cp(1) * wp1(0) * wp2(0)
      wp4 = cc3 * wp3
      if (last) wp4(0) = - wp4(0)

    case (-13) ! v + s + s -> v
    ! currents: wp1^mu, wp2, wp3 -> wp4^nu
    ! coupling = i*cop(1)*g_{mu,al}, propagator = i*g^{al,nu}/(ps-m2)

      cc1 = - cp(1) * wp2(0) * wp3(0)
      wp4 = cc1 * wp1
      if (last) wp4(0) = - wp4(0)

    case (-14) ! s + v + s -> v
    ! currents: wp1, wp2^mu, wp3 -> wp4^nu
    ! coupling = i*cop(1)*g_{mu,al}, propagator = i*g^{al,nu}/(ps-m2)

      cc2 = - cp(1) * wp1(0) * wp3(0)
      wp4 = cc2 * wp2
      if (last) wp4(0) = - wp4(0)

    ! HEFT
    case (VVSS_heft)
      call tree3(last,p1,p2,pl1,pl2,m,den,cop,VVS_heft,wp1,wp2,wp4)
      wp4(0) = wp4(0) * wp3(0)

    case (VSSV_heft)
      call tree3(last,p1,p2+p3,pl1,pl2,m,den,cop,VSV_heft,wp1,wp2,wp4)
      wp4 = wp4 * wp3(0)

    case (SVVS_heft)
      x0 = cp(2)*(-wp2(0)*wp3(0) + wp2(1)*wp3(1) + wp2(2)*wp3(2) + wp2(3)*wp3(3))
      x1 = cp(1)*(-P2(0)*wp3(0) + P2(1)*wp3(1) + P2(2)*wp3(2) + P2(3)*wp3(3))
      wp4(0) = wp1(0)*(-P2(0)*P3(0)*x0 + P2(1)*P3(1)*x0 + P2(2)*P3(2)*x0 + P2&
                (3)*P3(3)*x0 - P3(0)*wp2(0)*x1 + P3(1)*wp2(1)*x1 + P3(2)*wp2(2)*&
                x1 + P3(3)*wp2(3)*x1)
      wp4(1:3) = cnul

    case (SVSV_heft)
      call tree3(last,p1+p3,p2,pl1,pl2,m,den,cop,SVV_heft,wp1,wp2,wp4)
      wp4 = wp4 * wp3(0)

    case (VVVS_heft)
      x0 = -wp1(0)*wp2(0) + wp1(1)*wp2(1) + wp1(2)*wp2(2) + wp1(3)*wp2(3)
      x1 = wp3(0)*x0
      x2 = -wp1(0)*wp3(0) + wp1(1)*wp3(1) + wp1(2)*wp3(2) + wp1(3)*wp3(3)
      x3 = wp2(1)*x2
      x4 = wp2(2)*x2
      x5 = wp2(3)*x2
      x6 = -wp2(0)*wp3(0) + wp2(1)*wp3(1) + wp2(2)*wp3(2) + wp2(3)*wp3(3)
      x7 = wp1(0)*x6
      x8 = wp3(1)*x0
      x9 = wp3(2)*x0
      x10 = wp3(3)*x0
      x11 = wp2(0)*x2
      x12 = wp1(1)*x6
      x13 = wp1(2)*x6
      x14 = wp1(3)*x6
      wp4(0) = cp(1)*(P1(0)*x1 - P1(0)*x11 + P1(1)*x3 - P1(1)*x8 + P1(2)*x4 &
                 - P1(2)*x9 - P1(3)*x10 + P1(3)*x5 - P2(0)*x1 + P2(0)*x7 - P2&
                (1)*x12 + P2(1)*x8 - P2(2)*x13 + P2(2)*x9 + P2(3)*x10 - P2(3 &
                )*x14 + P3(0)*x11 - P3(0)*x7 + P3(1)*x12 - P3(1)*x3 + P3(2)* &
                x13 - P3(2)*x4 + P3(3)*x14 - P3(3)*x5)
      wp4(1:3) = cnul

    case (VVSV_heft)
      call tree3(last,p1+p3/3d0,p2+p3/3d0,pl1,pl2,m,den,cop,12,wp1,wp2,wp4)
      wp4 = wp4 * wp3(0)

    case (SVVV_heft)
      x0 = -wp2(0)*wp3(0) + wp2(1)*wp3(1) + wp2(2)*wp3(2) + wp2(3)*wp3(3)
      x0vec = wp2(0)*wp3(:)
      x1vec = wp2(:)*wp3(1)
      x2vec = wp2(:)*wp3(2)
      x3vec = wp2(:)*wp3(3)
      x4vec = wp2(:)*wp3(0)
      x5vec = wp2(1)*wp3(:)
      x6vec = wp2(2)*wp3(:)
      x7vec = wp2(3)*wp3(:)
      wp4 = cp(1)*wp1(0)*(P1(0)*x0vec - P1(0)*x4vec + P1(1)*x1vec - P1(1)*   &
             x5vec + P1(2)*x2vec - P1(2)*x6vec + P1(3)*x3vec - P1(3)*x7vec + &
             P2(0)*x0vec - 2*P2(0)*x4vec + 2*P2(1)*x1vec - P2(1)*x5vec + 2*P2&
             (2)*x2vec - P2(2)*x6vec + 2*P2(3)*x3vec - P2(3)*x7vec - P2(:)*x0&
              + 2*P3(0)*x0vec - P3(0)*x4vec + P3(1)*x1vec - 2*P3(1)*x5vec +  &
             P3(2)*x2vec - 2*P3(2)*x6vec + P3(3)*x3vec - 2*P3(3)*x7vec + P3  &
             (:)*x0)

    case default

      call error_rcl('Wrong 4-leg interaction', where='tree4')

    end select


  end subroutine tree4

!------------------------------------------------------------------------------!

  subroutine tree5(last,p1,p2,p3,p4,pl1,pl2,pl3,pl4, &
                   m,den,cop,ty,wp1,wp2,wp3,wp4,wp5)
    logical,      intent (in) :: last
    integer,      intent (in) :: ty
    complex(dp),  intent (in) :: m,den,cop(:)
    complex (dp), intent (in) :: p1(0:3),p2(0:3),p3(0:3),p4(0:3), &
                                 pl1(1:4),pl2(1:4),pl3(1:4),pl4(1:4)
    complex(dp), intent (inout) :: wp1(0:3),wp2(0:3),wp3(0:3),wp4(0:3)
    complex(dp), intent (out)   :: wp5(0:3)

    complex(dp) :: cp(size(cop))

    complex(dp) :: x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,       &
                   x11,x12,x13,x14

    ! overall factor
    if (last) then
      cp = cima*cop(1)
    else
      cp = - cop(1)/den
    endif

    ! select interaction type and contract currents

    select case (ty)

    case (VVVSS_heft)
      call tree4(last,p1,p2,p3,pl1,pl2,pl3,m,den,cop,VVVS_heft,wp1,wp2,wp3,wp5)
      wp5(0) = wp5(0) * wp4(0)

    case (VVSSV_heft)
      call tree3(last,p1+(p3+p4)/3d0,p2+(p3+p4)/3d0,pl1,pl2,m,den,cop, &
                 12,wp1,wp2,wp5)
      wp5 = wp5 * wp4(0) * wp3(0)

    case (SVVVS_heft)
      x0 = -wp2(0)*wp3(0) + wp2(1)*wp3(1) + wp2(2)*wp3(2) + wp2(3)*wp3(3)
      x1 = wp4(0)*x0
      x2 = -wp2(0)*wp4(0) + wp2(1)*wp4(1) + wp2(2)*wp4(2) + wp2(3)*wp4(3)
      x3 = wp3(1)*x2
      x4 = wp3(2)*x2
      x5 = wp3(3)*x2
      x6 = -wp3(0)*wp4(0) + wp3(1)*wp4(1) + wp3(2)*wp4(2) + wp3(3)*wp4(3)
      x7 = wp2(0)*x6
      x8 = wp4(1)*x0
      x9 = wp4(2)*x0
      x10 = wp4(3)*x0
      x11 = wp3(0)*x2
      x12 = wp2(1)*x6
      x13 = wp2(2)*x6
      x14 = wp2(3)*x6
      wp5(0) = cp(1)*wp1(0)*(P2(0)*x1 - P2(0)*x11 + P2(1)*x3 - P2(1)*x8 + P2(&
                2)*x4 - P2(2)*x9 - P2(3)*x10 + P2(3)*x5 - P3(0)*x1 + P3(0)*x7&
                 - P3(1)*x12 + P3(1)*x8 - P3(2)*x13 + P3(2)*x9 + P3(3)*x10 - &
                P3(3)*x14 + P4(0)*x11 - P4(0)*x7 + P4(1)*x12 - P4(1)*x3 + P4(&
                2)*x13 - P4(2)*x4 + P4(3)*x14 - P4(3)*x5)
      wp5(1:3) = cnul

    case (SVVSV_heft)
      call tree3(last,p2+(p1+p4)/3d0,p3+(p1+p4)/3d0,pl2,pl3,m,den,cop, &
                 12,wp2,wp3,wp5)
      wp5 = wp5 * wp4(0)

    case (VVVVS_heft)
      x0 = cp(1)*(-wp2(0)*wp3(0) + wp2(1)*wp3(1) + wp2(2)*wp3(2) + wp2(3)*wp3(3))
      x1 = cp(2)*(-wp2(0)*wp4(0) + wp2(1)*wp4(1) + wp2(2)*wp4(2) + wp2(3)*wp4(3))
      x2 = cp(3)*(-wp3(0)*wp4(0) + wp3(1)*wp4(1) + wp3(2)*wp4(2) + wp3(3)*wp4(3))
      wp5(0) = -wp1(0)*wp2(0)*x2 - wp1(0)*wp3(0)*x1 - wp1(0)*wp4(0)*x0 + wp1(1)*wp2(&
                1)*x2 + wp1(1)*wp3(1)*x1 + wp1(1)*wp4(1)*x0 + wp1(2)*wp2(2)*x2 + wp1&
                (2)*wp3(2)*x1 + wp1(2)*wp4(2)*x0 + wp1(3)*wp2(3)*x2 + wp1(3)*wp3(3)*&
                x1 + wp1(3)*wp4(3)*x0
      wp5(1:3) = cnul

    case (VVVSV_heft)
      call tree4(last,p1,p2,p3,pl1,pl2,pl3,m,den,cop,-11,wp1,wp2,wp3,wp5)
      wp5 = wp5 * wp4(0)

    case (SVVVV_heft)
      wp5 = wp1(0)*(cp(1)*wp2(:)*(-wp3(0)*wp4(0) + wp3(1)*wp4(1) + wp3(2)*wp4(2) +  &
             wp3(3)*wp4(3)) - wp2(0)*wp3(0)*cp(3)*wp4(:) - wp2(0)*wp4(0)*cp(2)*wp3(:) + wp2(1)*wp3(&
             1)*cp(3)*wp4(:) + wp2(1)*wp4(1)*cp(2)*wp3(:) + wp2(2)*wp3(2)*cp(3)*wp4(:) + wp2(2)*wp4(2)* &
             cp(2)*wp3(:) + wp2(3)*wp3(3)*cp(3)*wp4(:) + wp2(3)*wp4(3)*cp(2)*wp3(:))

    case default

      call error_rcl('Wrong 5-leg interaction', where='tree5')

    end select


  end subroutine tree5

!------------------------------------------------------------------------------!

  subroutine tree6(last,p1,p2,p3,p4,p5,pl1,pl2,pl3,pl4,pl5, &
                   m,den,cop,ty,wp1,wp2,wp3,wp4,wp5,wp6)
    logical,      intent (in) :: last
    integer,      intent (in) :: ty
    complex(dp),  intent (in) :: m,den,cop(:)
    complex (dp), intent (in) :: p1(0:3),p2(0:3),p3(0:3),p4(0:3),p5(0:3), &
                                 pl1(1:4),pl2(1:4),pl3(1:4),pl4(1:4),pl5(1:4)
    complex (dp), intent (inout) :: wp1(0:3),wp2(0:3),wp3(0:3),wp4(0:3),wp5(0:3)
    complex(dp),  intent (out)   :: wp6(0:3)

    complex(dp) :: cp(size(cop))

    complex(dp) :: x0,x1,x2

    ! overall factor
    if (last) then
      cp = cima*cop(1)
    else
      cp = - cop(1)/den
    endif

    ! select interaction type and contract currents

    select case (ty)

    case (VVVVSS_heft)
      call tree5(last,p1,p2,p3,p4,pl1,pl2,pl3,pl4,m,den,cop, &
                 VVVVS_heft,wp1,wp2,wp3,wp4,wp6)
      wp6(0) = wp6(0) * wp5(0)

    case (VVVSSV_heft)
      call tree5(last,p1,p2,p3,p4,pl1,pl2,pl3,pl4,m,den,cop, &
                 VVVSV_heft,wp1,wp2,wp3,wp4,wp6)
      wp6 = wp6 * wp5(0)

    case (SVVVVS_heft)
      x0 = cp(1)*(-wp3(0)*wp4(0) + wp3(1)*wp4(1) + wp3(2)*wp4(2) + wp3(3)*wp4(3))
      x1 = cp(2)*(-wp3(0)*wp5(0) + wp3(1)*wp5(1) + wp3(2)*wp5(2) + wp3(3)*wp5(3))
      x2 = cp(3)*(-wp4(0)*wp5(0) + wp4(1)*wp5(1) + wp4(2)*wp5(2) + wp4(3)*wp5(3))
      wp6(0) = wp1(0)*(-wp2(0)*wp3(0)*x2 - wp2(0)*wp4(0)*x1 - wp2(0)*wp5(0)*x0 + wp2&
                (1)*wp3(1)*x2 + wp2(1)*wp4(1)*x1 + wp2(1)*wp5(1)*x0 + wp2(2)*wp3(2)*&
                x2 + wp2(2)*wp4(2)*x1 + wp2(2)*wp5(2)*x0 + wp2(3)*wp3(3)*x2 + wp2(3 &
                )*wp4(3)*x1 + wp2(3)*wp5(3)*x0)
      wp6(1:3) = cnul

    case (SVVVSV_heft)
      call tree5(last,p1,p2,p3,p4,pl1,pl2,pl3,pl4,m,den,cop, &
                 SVVVV_heft,wp1,wp2,wp3,wp4,wp6)
      wp6 = wp6 * wp5(0)

    case default

      call error_rcl('Wrong 6-leg interaction', where='tree6')

    end select


  end subroutine tree6

!------------------------------------------------------------------------------!

 end module tree_vertices_rcl

!------------------------------------------------------------------------------!
