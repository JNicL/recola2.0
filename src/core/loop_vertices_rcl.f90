!******************************************************************************!
!                                                                              !
!    loop_vertices_rcl.f90                                                     !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

  module loop_vertices_rcl

!------------------------------------------------------------------------------!

  use globals_rcl
  use tables_rcl, only: gg,incRI,firstRI

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine loop3(riMaxIn,riMaxOut,p1,p2,pl1,pl2, &
                   m,cop,ty,wpl,wpt,wp)

    integer,     intent (in)    :: riMaxIn,riMaxOut,ty
    complex(dp), intent (in)    :: m,cop(:),                      &
                                   p1(0:3),p2(0:3),pl1(4),pl2(4), &
                                   wpl(0:3,0:riMaxIn),wpt(0:3)
    complex(dp), intent (out)   :: wp(0:3,0:riMaxOut)

    integer                         :: j,riOut,mu,nu,top,coefq
    complex(dp)                     :: cc1,cc2,cc3,cc4,cc5,spr,sprt, &
                                       auxa,auxb,auxc,auxd,          &
                                       wab02,wab13,wab12,wab03,      &
                                       wab20,wab31,wab21,wab30
    complex(dp), dimension(0:3)     :: p13,p23,p12,prd1,prd2,  &
                                       pp,vv1,vv2,vv3,vv4,vv5, &
                                       spg,wp1,wp2
    complex(dp), dimension(4)       :: pl,wap,wap1,wap2
    complex(dp), dimension(0:3,0:3) :: spgg,gg1

    ! select interaction type and build currents

    select case (ty)

    case (1) ! s + s -> s

      !  |
      !  | loop line in:
      !  V s = s0 + s1(mu1)*q(mu1) + s2(mu1,mu2)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) = q + p(el)
      !  |
      !  0---<-- tree line in:
      !  |       ss
      !  |       momentum (incoming) = p(et)
      !  |
      !  |
      !  V loop line out:
      !  | sss
      !  | momentum (incoming) = - q - p(e3) = - q - p(el) - p(et)
      !  |
      !
      !  coupling = - cop(1)

      cc1 = - cop(1) * wpt(0)
      wp(0,:) = cc1 * wpl(0,:)
      wp(1:3,:) = cnul

    case (2) ! v + v -> s

      !  |
      !  | loop line in:
      !  V v(nu) = v0(nu) + v1(mu1,nu)*q(mu1) + v2(mu1,mu2,nu)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) = q + p(el)
      !  |
      !  0---<-- tree line in:
      !  |       vv(ro)
      !  |       momentum (incoming) = p(et)
      !  |
      !  |
      !  V loop line out:
      !  | s
      !  | momentum (incoming) = - q - p(e3) = - q - p(el) - p(et)
      !  |
      !
      !  coupling = - cop(1)

      wp2 = - cop(1) * wpt
      do j = 0,riMaxOut
        wp(0,j) = wpl(0,j)*wp2(0) - sum(wpl(1:3,j)*wp2(1:3))
      enddo
      wp(1:3,:) = cnul

    case (3) ! s + v -> s

      !  |
      !  | loop line in:
      !  V s = s0 + s1(mu1)*q(mu1) + s2(mu1,mu2)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) = q + p(el)
      !  |
      !  0---<-- tree line in:
      !  |       v(nu)
      !  |       momentum (incoming) = p(et)
      !  |
      !  |
      !  V loop line out:
      !  | ss
      !  | momentum (incoming) = - q - p(e3) = - q - p(e1) - p(e2)
      !  |
      !
      !  coupling = - cop(1) * ( 2*q(nu) + pp(nu) )

      pp = + 2*p1 + p2

      spr = pp(0)*wpt(0) - sum(pp(1:3)*wpt(1:3))

      cc1 = -   cop(1) * spr
      cc2 = - 2*cop(1)
      wp2 = cc2 * wpt

      wp(0,0:riMaxIn) = cc1 * wpl(0,0:riMaxIn)
      do j = riMaxIn,0,-1
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                wp(0,riOut) = wpl(0,j)*wp2(mu)
          else; wp(0,riOut) = wp(0,riOut) + wpl(0,j)*wp2(mu)
          endif
        enddo
      enddo
      wp(1:3,:) = cnul

    case (4) ! v + s -> s

      !  |
      !  | loop line in:
      !  V v(nu) = v0(nu) + v1(mu1,nu)*q(mu1) + v2(mu1,mu2,nu)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) = q + p(el)
      !  |
      !  0---<-- tree line in:
      !  |       s
      !  |       momentum (incoming) = p(et)
      !  |
      !  |
      !  V loop line out:
      !  | ss
      !  | momentum (incoming) = - q - p(e3) = - q - p(e1) - p(e2)
      !  |
      !
      !  coupling = - cop(1) * ( - q(nu) + pp(nu) )

      pp = - 2*p2 - p1

      cc1 = - cop(1) * wpt(0)
      cc2 = - cc1

      do j = riMaxIn,0,-1
        spr = pp(0)*wpl(0,j) - sum(pp(1:3)*wpl(1:3,j))
        wp(0,j) = cc1 * spr
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                wp(0,riOut) = cc2 * wpl(mu,j)
          else; wp(0,riOut) = wp(0,riOut) + cc2 * wpl(mu,j)
          endif
        enddo
      enddo

    case (5,6) ! f + {f} -> s, {f} + f -> s

      !  |
      !  | loop line in:
      !  V f(d1) = v0(d1) + v1(mu1,d1)*q(mu1) + v2(mu1,mu2,d1)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) = q + p(el)
      !  |
      !  0---<-- tree line in:
      !  |       {f}(d2)
      !  |       momentum (incoming) = p(et)
      !  |
      !  |
      !  V loop line out:
      !  | s
      !  | momentum (incoming) = - q - p(e3) = - q - p(e1) - p(e2)
      !  |

      wp2(0:1) = - cop(1) * wpt(0:1)
      wp2(2:3) = - cop(2) * wpt(2:3)
      do j = 0,riMaxOut
        wp(0,j) = sum(wp2(0:3)*wpl(0:3,j))
      enddo
      wp(1:3,:) = cnul

    case (7,8) ! f_- + {f}_- -> s, {f}_- + f_- -> s

      wp2(2:3) = - cop(1) * wpt(2:3)
      wp(0,:) = wp2(2)*wpl(2,:) + wp2(3)*wpl(3,:)
      wp(1:3,:) = cnul

    case (9,10) ! f_+ + {f}_+ -> s, {f}_+ + f_+ -> s

      wp2(0:1) = - cop(1) * wpt(0:1)
      wp(0,:) = wp2(0)*wpl(0,:) + wp2(1)*wpl(1,:)
      wp(1:3,:) = cnul

    case (11) ! s + s -> v

      wp(:,riMaxIn+1:riMaxOut) = cnul

      p12(:) = p1 - p2

      !  |
      !  | loop line in:
      !  V s = s0 + s1(mu1)*q(mu1) + s2(mu1,mu2)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) = q + p(el)
      !  |
      !  0---<-- tree line in:
      !  |       ss
      !  |       momentum (incoming) = p(et)
      !  |
      !  |
      !  V loop line out:
      !  | v(nu)
      !  | momentum (incoming) = - q - p(e3) = - q - p(e1) - p(e2)
      !  |
      !
      !  coupling =
      !    - cop(1) * ( q(nu) + p12(nu) ) =
      !    - cop(1) * ( q(nu) + ( p(e1,nu) - p(e2,nu) ) )
      !    - cop(1) * ( q(nu) + ( p(el,nu) - p(et,nu) ) )

      cc1 = - cop(1) * wpt(0)
      do j = riMaxIn,0,-1
        cc2 = cc1 * wpl(0,j)
        wp(:,j) = cc2 * p12(:)
        do mu= 0, 3
          riOut = incRI(mu,j)
          wp(mu,riOut) = wp(mu,riOut) + cc2 * gg(mu,mu)
        enddo
      enddo

    case (12) ! v + v -> v

      p12(:)=   p1 - p2
      p13(:)= 2*p1 + p2
      p23(:)= 2*p2 + p1

      !  |
      !  | loop line in:
      !  V v(mu) = v0(mu) + v1(mu1,mu)*q(mu1) + v2(mu1,mu2,mu)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) k1 = q + p(e1)
      !  |
      !  0---<-- tree line in:
      !  |       vv(nu)
      !  |       momentum (incoming) k2 = p(e2)
      !  |
      !  |
      !  V loop line out:
      !  | vvv(ro)
      !  | momentum (incoming) k3 = - q - p(e3)
      !  |
      !
      !  coupling =
      !    - (- cop(1)) * (
      !    + g(mu,nu)*( k1(ro) - k2(ro) )
      !    + g(nu,ro)*( k2(mu) - k3(mu) )
      !    + g(mu,ro)*( k3(nu) - k1(nu) )
      !    ) =
      !    - (- cop(1)) * (
      !    + g(mu,nu)*( +   q(ro) + p1(ro) - p2(ro) )
      !    + g(nu,ro)*( +   q(mu) + p2(mu) + p3(mu) )
      !    + g(mu,ro)*( - 2*q(nu) - p1(nu) - p3(nu) )
      !    )

      sprt = p13(0)*wpt(0) - sum(p13(1:3)*wpt(1:3))
      cc1  = cop(1) * sprt
      vv1  = - cop(1) * p23
      vv2  = - cop(1) * p12
      do mu = 0,3
        gg1(:,mu) = vv1 * wpt(mu) + wpt * vv2(mu)
      enddo

      cc3 = 2 * cop(1)
      vv3 = cc3 * wpt
      cc4 = - cop(1)
      vv4 = cc4 * wpt

      do j = riMaxIn,0,-1

        do mu= 0,3
          vv5(mu) = gg1(0,mu)*wpl(0,j) - sum(gg1(1:3,mu)*wpl(1:3,j))
        enddo

        spr = wpt(0)*wpl(0,j) - sum(wpt(1:3)*wpl(1:3,j))
        cc5 = cc4 * spr

        wp(:,j) = cc1*wpl(:,j) + vv5(:)
        do mu= 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
            wp(:,riOut) = + vv3(mu)*wpl(:,j) + vv4(:)*wpl(mu,j) &
                          + cc5*gg(:,mu)
          else
            wp(:,riOut) = wp(:,riOut)                           &
                          + vv3(mu)*wpl(:,j) + vv4(:)*wpl(mu,j) &
                          + cc5*gg(:,mu)
          endif
        enddo

      enddo

    case (13)  ! s + v -> v, v + s -> v

      !  |
      !  | loop line in:
      !  V s = s0 + s1(mu1)*q(mu1) + s2(mu1,mu2)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) = q + p(el)
      !  |
      !  0---<-- tree line in:
      !  |       v(nu)
      !  |       momentum (incoming) = p(et)
      !  |
      !  |
      !  V loop line out:
      !  | v(ro)
      !  | momentum (incoming) = - q - p(e3) = - q - p(e1) - p(e2)
      !  |
      !
      !  coupling = - cop(1) * g(nu,ro)

      vv1 = cop(1) * wpt
      do j = 0,riMaxIn
        wp(:,j) = vv1 * wpl(0,j)
      enddo

    case (14)  ! s + v -> v, v + s -> v

      !  |
      !  | loop line in:
      !  V v(nu) = v0(nu) + v1(mu1,nu)*q(mu1) + v2(mu1,mu2,nu)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) = q + p(el)
      !  |
      !  0---<-- tree line in:
      !  |       s
      !  |       momentum (incoming) = p(et)
      !  |
      !  |
      !  V loop line out:
      !  | v(ro)
      !  | momentum (incoming) = - q - p(e3) = - q - p(e1) - p(e2)
      !  |
      !
      !  coupling = - cop(1) * g(nu,ro)

      cc1 = cop(1) * wpt(0)
      wp(:,:) = cc1 * wpl(:,:)

    case (15) ! f + {f} -> v

      !  |
      !  | loop line in:
      !  V f(d1) = v0(d1) + v1(mu1,d1)*q(mu1) + v2(mu1,mu2,d1)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) = q + p(el)
      !  |
      !  0---<-- tree line in:
      !  |       {f}(d2)
      !  |       momentum (incoming) = p(et)
      !  |
      !  |
      !  V loop line out:
      !  | v(ro)
      !  | momentum (incoming) = - q - p(e3) = - q - p(e1) - p(e2)
      !  |

      wp2(0:1) = - cop(2) * wpt(0:1)
      wp2(2:3) = - cop(1) * wpt(2:3)
      do j = 0,riMaxOut
        wab02 = wp2(0)*wpl(2,j)
        wab13 = wp2(1)*wpl(3,j)
        wab12 = wp2(1)*wpl(2,j)
        wab03 = wp2(0)*wpl(3,j)
        wab20 = wp2(2)*wpl(0,j)
        wab31 = wp2(3)*wpl(1,j)
        wab21 = wp2(2)*wpl(1,j)
        wab30 = wp2(3)*wpl(0,j)
        prd1(0) =     wab02 + wab13
        prd1(1) =   - wab12 - wab03
        prd1(2) = ( - wab12 + wab03 )*cima
        prd1(3) =   - wab02 + wab13
        prd2(0) =     wab20 + wab31
        prd2(1) =     wab30 + wab21
        prd2(2) = (   wab30 - wab21 )*cima
        prd2(3) =     wab20 - wab31
        wp(:,j) = prd1 + prd2
      enddo

    case (16) ! {f} + f -> v

      wp2(0:1) = - cop(1) * wpt(0:1)
      wp2(2:3) = - cop(2) * wpt(2:3)
      do j = 0,riMaxOut
        wab02 = wpl(0,j)*wp2(2)
        wab13 = wpl(1,j)*wp2(3)
        wab12 = wpl(1,j)*wp2(2)
        wab03 = wpl(0,j)*wp2(3)
        wab20 = wpl(2,j)*wp2(0)
        wab31 = wpl(3,j)*wp2(1)
        wab21 = wpl(2,j)*wp2(1)
        wab30 = wpl(3,j)*wp2(0)
        prd1(0) =     wab02 + wab13
        prd1(1) =   - wab12 - wab03
        prd1(2) = ( - wab12 + wab03 )*cima
        prd1(3) =   - wab02 + wab13
        prd2(0) =     wab20 + wab31
        prd2(1) =     wab30 + wab21
        prd2(2) = (   wab30 - wab21 )*cima
        prd2(3) =     wab20 - wab31
        wp(:,j) = prd1 + prd2
      enddo

    case (17) ! f_- + {f}_+ -> v

      wp2(0:1) = - cop(1) * wpt(0:1)
      do j = 0,riMaxOut
        wab02 = wp2(0)*wpl(2,j)
        wab13 = wp2(1)*wpl(3,j)
        wab12 = wp2(1)*wpl(2,j)
        wab03 = wp2(0)*wpl(3,j)
        prd1(0) =     wab02 + wab13
        prd1(1) =   - wab12 - wab03
        prd1(2) = ( - wab12 + wab03 )*cima
        prd1(3) =   - wab02 + wab13
        wp(:,j) = prd1
      enddo

    case (18) ! {f}_+ + f_- -> v

      wp2(2:3) = - cop(1) * wpt(2:3)
      do j = 0,riMaxOut
        wab02 = wpl(0,j)*wp2(2)
        wab13 = wpl(1,j)*wp2(3)
        wab12 = wpl(1,j)*wp2(2)
        wab03 = wpl(0,j)*wp2(3)
        prd1(0) =     wab02 + wab13
        prd1(1) =   - wab12 - wab03
        prd1(2) = ( - wab12 + wab03 )*cima
        prd1(3) =   - wab02 + wab13
        wp(:,j) = prd1
      enddo

    case (19) ! f_+ + {f}_- -> v

      vv1(2:3) = - cop(1) * wpt(2:3)
      do j = 0,riMaxOut
        wab20 = vv1(2)*wpl(0,j)
        wab31 = vv1(3)*wpl(1,j)
        wab21 = vv1(2)*wpl(1,j)
        wab30 = vv1(3)*wpl(0,j)
        prd2(0) =   wab20 + wab31
        prd2(1) =   wab30 + wab21
        prd2(2) = ( wab30 - wab21 )*cima
        prd2(3) =   wab20 - wab31
        wp(:,j) = prd2
      enddo

    case (20) ! {f}_- + f_+ -> v

      vv1(0:1) = - cop(1) * wpt(0:1)
      do j = 0,riMaxOut
        wab20 = wpl(2,j)*vv1(0)
        wab31 = wpl(3,j)*vv1(1)
        wab21 = wpl(2,j)*vv1(1)
        wab30 = wpl(3,j)*vv1(0)
        prd2(0) =   wab20 + wab31
        prd2(1) =   wab30 + wab21
        prd2(2) = ( wab30 - wab21 )*cima
        prd2(3) =   wab20 - wab31
        wp(:,j) = prd2
      enddo

    case (21) ! f + s -> f

      ! spg(i) = ( (slash{pl}+m)*psi*wp2 )_i
      ! spgg(i,mu) = ( gamma(mu)*psi*wp2 )_i
      ! psi = wp1
      ! auxa = (omega_-*psi)_0 = (psi)_0
      ! auxb = (omega_-*psi)_1 = (psi)_1
      ! auxc = (omega_+*psi)_2 = (psi)_2
      ! auxd = (omega_+*psi)_3 = (psi)_3

      pl = pl1 + pl2

      cc1 = - cop(2) * wpt(0)
      cc2 = - cop(1) * wpt(0)
      do j = riMaxIn,0,-1
        auxa = wpl(0,j)*cc2
        auxb = wpl(1,j)*cc2
        auxc = wpl(2,j)*cc1
        auxd = wpl(3,j)*cc1
        spgg(0,0) = - auxc
        spgg(1,0) = - auxd
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(0,2) = - auxd*cima
        spgg(1,2) = + auxc*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spgg(2,0) = - auxa
        spgg(3,0) = - auxb
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(2,2) = + auxb*cima
        spgg(3,2) = - auxa*cima
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0) = - auxc*pl(1) - auxd*pl(4) + auxa*m
        spg(1) = - auxc*pl(3) - auxd*pl(2) + auxb*m
        spg(2) = - auxa*pl(2) + auxb*pl(4) + auxc*m
        spg(3) = + auxa*pl(3) - auxb*pl(1) + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:3,riOut) = spgg(0:3,mu)
          else;  wp(0:3,riOut) = wp(0:3,riOut) + spgg(0:3,mu)
          endif
        enddo
      enddo

    case (23) ! s + f -> f

      ! spg(i) = ( (slash{pl}+m)*psi*wp1 )_i
      ! spgg(mu,i) = ( gamma(mu)*psi*wp1 )_i
      ! psi = wp2
      ! auxa = (omega_-*psi)_0 = (psi)_0
      ! auxb = (omega_-*psi)_1 = (psi)_1
      ! auxc = (omega_+*psi)_2 = (psi)_2
      ! auxd = (omega_+*psi)_3 = (psi)_3

      pl = pl1 + pl2

      wp2(0:1) = - cop(1) * wpt(0:1)
      wp2(2:3) = - cop(2) * wpt(2:3)
      do j = riMaxIn,0,-1
        auxa = wp2(0)*wpl(0,j)
        auxb = wp2(1)*wpl(0,j)
        auxc = wp2(2)*wpl(0,j)
        auxd = wp2(3)*wpl(0,j)
        spgg(0,0) = - auxc
        spgg(1,0) = - auxd
        spgg(2,0) = - auxa
        spgg(3,0) = - auxb
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(0,2) = - auxd*cima
        spgg(1,2) = + auxc*cima
        spgg(2,2) = + auxb*cima
        spgg(3,2) = - auxa*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0)= - auxc*pl(1) - auxd*pl(4) + auxa*m
        spg(1)= - auxc*pl(3) - auxd*pl(2) + auxb*m
        spg(2)= - auxa*pl(2) + auxb*pl(4) + auxc*m
        spg(3)= + auxa*pl(3) - auxb*pl(1) + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:3,riOut) = spgg(0:3,mu)
          else;  wp(0:3,riOut) = wp(0:3,riOut) + spgg(0:3,mu)
          endif
        enddo
      enddo

    case (25) ! f_- + s -> f_+

      pl = pl1 + pl2

      cc1 = - cop(1) * wpt(0)
      do j = riMaxIn,0,-1
        auxc = wpl(2,j)*cc1
        auxd = wpl(3,j)*cc1
        spgg(0,0) = - auxc
        spgg(1,0) = - auxd
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(0,2) = - auxd*cima
        spgg(1,2) = + auxc*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spg(0) = - auxc*pl(1) - auxd*pl(4)
        spg(1) = - auxc*pl(3) - auxd*pl(2)
        spg(2) =                           + auxc*m
        spg(3) =                           + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:1,riOut) = spgg(0:1,mu)
                 wp(2:3,riOut) = 0d0
          else;  wp(0:1,riOut) = wp(0:1,riOut) + spgg(0:1,mu)
          endif
        enddo
      enddo

    case (26) ! s + f_- -> f_+

      pl = pl1 + pl2

      wp2(2:3) = - cop(1) * wpt(2:3)
      do j = riMaxIn,0,-1
        auxc = wp2(2)*wpl(0,j)
        auxd = wp2(3)*wpl(0,j)
        spgg(0,0) = - auxc
        spgg(1,0) = - auxd
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(0,2) = - auxd*cima
        spgg(1,2) = + auxc*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spg(0)= - auxc*pl(1) - auxd*pl(4)
        spg(1)= - auxc*pl(3) - auxd*pl(2)
        spg(2)=                           + auxc*m
        spg(3)=                           + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:1,riOut) = spgg(0:1,mu)
                 wp(2:3,riOut) = 0d0
          else;  wp(0:1,riOut) = wp(0:1,riOut) + spgg(0:1,mu)
          endif
        enddo
      enddo

    case (27) ! f_+ + s -> f_-

      pl = pl1 + pl2

      cc2 = - cop(1) * wpt(0)
      do j = riMaxIn,0,-1
        auxa = wpl(0,j)*cc2
        auxb = wpl(1,j)*cc2
        spgg(2,0) = - auxa
        spgg(3,0) = - auxb
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(2,2) = + auxb*cima
        spgg(3,2) = - auxa*cima
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0) =                           + auxa*m
        spg(1) =                           + auxb*m
        spg(2) = - auxa*pl(2) + auxb*pl(4)
        spg(3) = + auxa*pl(3) - auxb*pl(1)
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(2:3,riOut) = spgg(2:3,mu)
                 wp(0:1,riOut) = 0d0
          else;  wp(2:3,riOut) = wp(2:3,riOut) + spgg(2:3,mu)
          endif
        enddo
      enddo

    case (28) ! s + f_+ -> f_-

      pl = pl1 + pl2

      wp2(0:1) = - cop(1) * wpt(0:1)
      do j = riMaxIn,0,-1
        auxa = wp2(0)*wpl(0,j)
        auxb = wp2(1)*wpl(0,j)
        spgg(2,0) = - auxa
        spgg(3,0) = - auxb
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(2,2) = + auxb*cima
        spgg(3,2) = - auxa*cima
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0) =                           + auxa*m
        spg(1) =                           + auxb*m
        spg(2) = - auxa*pl(2) + auxb*pl(4)
        spg(3) = + auxa*pl(3) - auxb*pl(1)
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(2:3,riOut) = spgg(2:3,mu)
                 wp(0:1,riOut) = 0d0
          else;  wp(2:3,riOut) = wp(2:3,riOut) + spgg(2:3,mu)
          endif
        enddo
      enddo

    case (31) ! f + v -> f

      ! spg(i) = ( (slash{pl}+m)*slash{eps}*psi )_i
      ! spgg(i,mu) = ( gamma(mu)*slash{eps}*psi )_i
      ! eps = wp2
      ! psi = wp1
      ! auxa= (slash{eps}*omega_-*psi)_0
      ! auxb= (slash{eps}*omega_-*psi)_1
      ! auxc= (slash{eps}*omega_+*psi)_2
      ! auxd= (slash{eps}*omega_+*psi)_3

      pl = pl1 + pl2

      wap(1) = wpt(0) + wpt(3)
      wap(2) = wpt(0) - wpt(3)
      wap(3) = wpt(1) + cima*wpt(2)
      wap(4) = wpt(1) - cima*wpt(2)
      wap1 = - cop(2) * wap
      wap2 = - cop(1) * wap
      do j = riMaxIn,0,-1
        wp1(0:1) = wpl(0:1,j)
        wp1(2:3) = wpl(2:3,j)
        auxa  = - wp1(2)*wap1(1) - wp1(3)*wap1(4)
        auxb  = - wp1(2)*wap1(3) - wp1(3)*wap1(2)
        auxc  = - wp1(0)*wap2(2) + wp1(1)*wap2(4)
        auxd  = + wp1(0)*wap2(3) - wp1(1)*wap2(1)
        spgg(0,0) = - auxc
        spgg(1,0) = - auxd
        spgg(2,0) = - auxa
        spgg(3,0) = - auxb
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(0,2) = - auxd*cima
        spgg(1,2) = + auxc*cima
        spgg(2,2) = + auxb*cima
        spgg(3,2) = - auxa*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0)= - auxc*pl(1) - auxd*pl(4) + auxa*m
        spg(1)= - auxc*pl(3) - auxd*pl(2) + auxb*m
        spg(2)= - auxa*pl(2) + auxb*pl(4) + auxc*m
        spg(3)= + auxa*pl(3) - auxb*pl(1) + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:3,riOut) = spgg(0:3,mu)
          else;  wp(0:3,riOut) = wp(0:3,riOut) + spgg(0:3,mu)
          endif
        enddo
      enddo

    case (33) ! v + f -> f

      ! spg(i) = ( (slash{pl}+m)*slash{eps}*psi )_i
      ! spgg(i,mu) = ( gamma(mu)*slash{eps}*psi )_i
      ! eps = wp1
      ! psi = wp2
      ! auxa= (slash{eps}*omega_-*psi)_0
      ! auxb= (slash{eps}*omega_-*psi)_1
      ! auxc= (slash{eps}*omega_+*psi)_2
      ! auxd= (slash{eps}*omega_+*psi)_3

      pl = pl1 + pl2

      wp2(0:1) = - cop(1) * wpt(0:1)
      wp2(2:3) = - cop(2) * wpt(2:3)
      do j = riMaxIn,0,-1
        wp1 = wpl(:,j)
        wap(1) = wp1(0) + wp1(3)
        wap(2) = wp1(0) - wp1(3)
        wap(3) = wp1(1) + cima*wp1(2)
        wap(4) = wp1(1) - cima*wp1(2)
        auxa  = - wp2(2)*wap(1) - wp2(3)*wap(4)
        auxb  = - wp2(2)*wap(3) - wp2(3)*wap(2)
        auxc  = - wp2(0)*wap(2) + wp2(1)*wap(4)
        auxd  = + wp2(0)*wap(3) - wp2(1)*wap(1)
        spgg(0,0) = - auxc
        spgg(1,0) = - auxd
        spgg(2,0) = - auxa
        spgg(3,0) = - auxb
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(0,2) = - auxd*cima
        spgg(1,2) = + auxc*cima
        spgg(2,2) = + auxb*cima
        spgg(3,2) = - auxa*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0)= - auxc*pl(1) - auxd*pl(4) + auxa*m
        spg(1)= - auxc*pl(3) - auxd*pl(2) + auxb*m
        spg(2)= - auxa*pl(2) + auxb*pl(4) + auxc*m
        spg(3)= + auxa*pl(3) - auxb*pl(1) + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:3,riOut) = spgg(0:3,mu)
          else;  wp(0:3,riOut) = wp(0:3,riOut) + spgg(0:3,mu)
          endif
        enddo
      enddo

    case (35) ! f_- + v -> f_-

      pl = pl1 + pl2
      wap(1) = wpt(0) + wpt(3)
      wap(2) = wpt(0) - wpt(3)
      wap(3) = wpt(1) + cima*wpt(2)
      wap(4) = wpt(1) - cima*wpt(2)
      wap = - cop(1) * wap
      do j = riMaxIn,0,-1
        wp1(2:3) = wpl(2:3,j)
        auxa  = - wp1(2)*wap(1) - wp1(3)*wap(4)
        auxb  = - wp1(2)*wap(3) - wp1(3)*wap(2)
        spgg(2,0) = - auxa
        spgg(3,0) = - auxb
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(2,2) = + auxb*cima
        spgg(3,2) = - auxa*cima
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0)=                           + auxa*m
        spg(1)=                           + auxb*m
        spg(2)= - auxa*pl(2) + auxb*pl(4)
        spg(3)= + auxa*pl(3) - auxb*pl(1)
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(2:3,riOut) = spgg(2:3,mu)
                 wp(0:1,riOut) = 0d0
          else;  wp(2:3,riOut) = wp(2:3,riOut) + spgg(2:3,mu)
          endif
        enddo
      enddo

    case (36) ! v + f_- -> f_-

      pl = pl1 + pl2

      wp2(2:3) = - cop(1) * wpt(2:3)
      do j = riMaxIn,0,-1
        wp1      = wpl(:,j)
        wap(1) = wp1(0) + wp1(3)
        wap(2) = wp1(0) - wp1(3)
        wap(3) = wp1(1) + cima*wp1(2)
        wap(4) = wp1(1) - cima*wp1(2)
        auxa  = - wp2(2)*wap(1) - wp2(3)*wap(4)
        auxb  = - wp2(2)*wap(3) - wp2(3)*wap(2)
        spgg(2,0) = - auxa
        spgg(3,0) = - auxb
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(2,2) = + auxb*cima
        spgg(3,2) = - auxa*cima
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0)=                          + auxa*m
        spg(1)=                          + auxb*m
        spg(2)= - auxa*pl(2) + auxb*pl(4)
        spg(3)= + auxa*pl(3) - auxb*pl(1)
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(2:3,riOut) = spgg(2:3,mu)
                 wp(0:1,riOut) = 0d0
          else;  wp(2:3,riOut) = wp(2:3,riOut) + spgg(2:3,mu)
          endif
        enddo
      enddo

    case (37) ! f_+ + v -> f_+

      pl = pl1 + pl2

      wap(1) = wpt(0) + wpt(3)
      wap(2) = wpt(0) - wpt(3)
      wap(3) = wpt(1) + cima*wpt(2)
      wap(4) = wpt(1) - cima*wpt(2)
      wap = - cop(1) * wap
      do j = riMaxIn,0,-1
        wp1(0:1) = wpl(0:1,j)
        auxc  = - wp1(0)*wap(2) + wp1(1)*wap(4)
        auxd  = + wp1(0)*wap(3) - wp1(1)*wap(1)
        spgg(0,0) = - auxc
        spgg(1,0) = - auxd
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(0,2) = - auxd*cima
        spgg(1,2) = + auxc*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spg(0) = - auxc*pl(1) - auxd*pl(4)
        spg(1) = - auxc*pl(3) - auxd*pl(2)
        spg(2) =                           + auxc*m
        spg(3) =                           + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:1,riOut) = spgg(0:1,mu)
                 wp(2:3,riOut) = 0d0
          else;  wp(0:1,riOut) = wp(0:1,riOut) + spgg(0:1,mu)
          endif
        enddo
      enddo

    case (38) ! v + f_+ -> f_+

      pl = pl1 + pl2

      wp2(0:1) = - cop(1) * wpt(0:1)
      do j = riMaxIn,0,-1
        wp1      = wpl(:,j)
        wap(1) = wp1(0) + wp1(3)
        wap(2) = wp1(0) - wp1(3)
        wap(3) = wp1(1) + cima*wp1(2)
        wap(4) = wp1(1) - cima*wp1(2)
        auxc  = - wp2(0)*wap(2) + wp2(1)*wap(4)
        auxd  = + wp2(0)*wap(3) - wp2(1)*wap(1)
        spgg(0,0) = - auxc
        spgg(1,0) = - auxd
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(0,2) = - auxd*cima
        spgg(1,2) = + auxc*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spg(0) = - auxc*pl(1) - auxd*pl(4)
        spg(1) = - auxc*pl(3) - auxd*pl(2)
        spg(2) =                           + auxc*m
        spg(3) =                           + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:1,riOut) = spgg(0:1,mu)
                 wp(2:3,riOut) = 0d0
          else;  wp(0:1,riOut) = wp(0:1,riOut) + spgg(0:1,mu)
          endif
        enddo
      enddo

    case (41) ! {f} + s -> {f}

      ! spg(i) = ( psibar*(-slash{pl}+m)*wp2 )_i
      ! spgg(mu,i) = ( psibar*gamma(mu)*wp2 )_i
      ! psi = wp1
      ! auxa = (psibar*omega_+)_0 = (psibar)_0
      ! auxb = (psibar*omega_+)_1 = (psibar)_1
      ! auxc = (psibar*omega_-)_2 = (psibar)_2
      ! auxd = (psibar*omega_-)_3 = (psibar)_3

      pl = pl1 + pl2

      cc1 = - cop(2) * wpt(0)
      cc2 = - cop(1) * wpt(0)
      do j = riMaxIn,0,-1
        auxa = wpl(0,j)*cc2
        auxb = wpl(1,j)*cc2
        auxc = wpl(2,j)*cc1
        auxd = wpl(3,j)*cc1
        spgg(0,0) = + auxc
        spgg(1,0) = + auxd
        spgg(2,0) = + auxa
        spgg(3,0) = + auxb
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(0,2) = + auxd*cima
        spgg(1,2) = - auxc*cima
        spgg(2,2) = - auxb*cima
        spgg(3,2) = + auxa*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0)= + auxc*pl(2) - auxd*pl(3) + auxa*m
        spg(1)= - auxc*pl(4) + auxd*pl(1) + auxb*m
        spg(2)= + auxa*pl(1) + auxb*pl(3) + auxc*m
        spg(3)= + auxa*pl(4) + auxb*pl(2) + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:3,riOut) = spgg(0:3,mu)
          else;  wp(0:3,riOut) = wp(0:3,riOut) + spgg(0:3,mu)
          endif
        enddo
      enddo

    case (43) ! s + {f} -> {f}

      ! spg(i) = ( psibar*(-slash{pl}+m)*wp1 )_i
      ! spgg(mu,i) = ( psibar*gamma(mu)*wp1 )_i
      ! psi = wp2
      ! auxa = (psibar*omega_+)_0 = (psibar)_0
      ! auxb = (psibar*omega_+)_1 = (psibar)_1
      ! auxc = (psibar*omega_-)_2 = (psibar)_2
      ! auxd = (psibar*omega_-)_3 = (psibar)_3

      pl = pl1 + pl2

      wp2(0:1) = - cop(1) * wpt(0:1)
      wp2(2:3) = - cop(2) * wpt(2:3)
      do j = riMaxIn,0,-1
        auxa = wp2(0)*wpl(0,j)
        auxb = wp2(1)*wpl(0,j)
        auxc = wp2(2)*wpl(0,j)
        auxd = wp2(3)*wpl(0,j)
        spgg(0,0) = + auxc
        spgg(1,0) = + auxd
        spgg(2,0) = + auxa
        spgg(3,0) = + auxb
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(0,2) = + auxd*cima
        spgg(1,2) = - auxc*cima
        spgg(2,2) = - auxb*cima
        spgg(3,2) = + auxa*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0)= + auxc*pl(2) - auxd*pl(3) + auxa*m
        spg(1)= - auxc*pl(4) + auxd*pl(1) + auxb*m
        spg(2)= + auxa*pl(1) + auxb*pl(3) + auxc*m
        spg(3)= + auxa*pl(4) + auxb*pl(2) + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:3,riOut) = spgg(0:3,mu)
          else;  wp(0:3,riOut) = wp(0:3,riOut) + spgg(0:3,mu)
          endif
        enddo
      enddo

    case (45) ! {f}_- + s -> {f}_+

      pl = pl1 + pl2

      cc1 = - cop(1) * wpt(0)
      do j = riMaxIn,0,-1
        auxc = wpl(2,j)*cc1
        auxd = wpl(3,j)*cc1
        spgg(0,0) = + auxc
        spgg(1,0) = + auxd
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(0,2) = + auxd*cima
        spgg(1,2) = - auxc*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spg(0) = + auxc*pl(2) - auxd*pl(3)
        spg(1) = - auxc*pl(4) + auxd*pl(1)
        spg(2) =                           + auxc*m
        spg(3) =                           + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:1,riOut) = spgg(0:1,mu)
                 wp(2:3,riOut) = 0d0
          else;  wp(0:1,riOut) = wp(0:1,riOut) + spgg(0:1,mu)
          endif
        enddo
      enddo

    case (46) ! s + {f}_- -> {f}_+

      pl = pl1 + pl2

      wp2(2:3) = - cop(1) * wpt(2:3)
      do j = riMaxIn,0,-1
        auxc = wp2(2)*wpl(0,j)
        auxd = wp2(3)*wpl(0,j)
        spgg(0,0) = + auxc
        spgg(1,0) = + auxd
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(0,2) = + auxd*cima
        spgg(1,2) = - auxc*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spg(0) = + auxc*pl(2) - auxd*pl(3)
        spg(1) = - auxc*pl(4) + auxd*pl(1)
        spg(2) =                           + auxc*m
        spg(3) =                           + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:1,riOut) = spgg(0:1,mu)
                 wp(2:3,riOut) = 0d0
          else;  wp(0:1,riOut) = wp(0:1,riOut) + spgg(0:1,mu)
          endif
        enddo
      enddo

    case (47) ! {f}_+ + s -> {f}_-

      pl = pl1 + pl2

      cc2 = - cop(1) * wpt(0)
      do j = riMaxIn,0,-1
        auxa = wpl(0,j)*cc2
        auxb = wpl(1,j)*cc2
        spgg(2,0) = + auxa
        spgg(3,0) = + auxb
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(2,2) = - auxb*cima
        spgg(3,2) = + auxa*cima
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0) =                           + auxa*m
        spg(1) =                           + auxb*m
        spg(2) = + auxa*pl(1) + auxb*pl(3)
        spg(3) = + auxa*pl(4) + auxb*pl(2)
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(2:3,riOut) = spgg(2:3,mu)
                 wp(0:1,riOut) = 0d0
          else;  wp(2:3,riOut) = wp(2:3,riOut) + spgg(2:3,mu)
          endif
        enddo
      enddo

    case (48) ! s + {f}_+ -> {f}_-

      pl = pl1 + pl2

      wp2(0:1) = - cop(1) * wpt(0:1)
      do j = riMaxIn,0,-1
        auxa = wp2(0)*wpl(0,j)
        auxb = wp2(1)*wpl(0,j)
        spgg(2,0) = + auxa
        spgg(3,0) = + auxb
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(2,2) = - auxb*cima
        spgg(3,2) = + auxa*cima
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0) =                           + auxa*m
        spg(1) =                           + auxb*m
        spg(2) = + auxa*pl(1) + auxb*pl(3)
        spg(3) = + auxa*pl(4) + auxb*pl(2)
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(2:3,riOut) = spgg(2:3,mu)
                 wp(0:1,riOut) = 0d0
          else;  wp(2:3,riOut) = wp(2:3,riOut) + spgg(2:3,mu)
          endif
        enddo
      enddo

    case (51) ! {f} + v -> {f}

      ! spg(i) = ( psibar*slash{eps}*(-slash{pl}+m) )_i
      ! spgg(i,mu) = ( psibar*slash{eps}*(-gamma(mu)) )_i
      ! eps = wp2
      ! bar{psi} = wp1
      ! auxa= (psibar*slash{eps}*omega_+)_0
      ! auxb= (psibar*slash{eps}*omega_+)_1
      ! auxc= (psibar*slash{eps}*omega_-)_2
      ! auxd= (psibar*slash{eps}*omega_-)_3

      pl = pl1 + pl2

      wap(1) = wpt(0) + wpt(3)
      wap(2) = wpt(0) - wpt(3)
      wap(3) = wpt(1) + cima*wpt(2)
      wap(4) = wpt(1) - cima*wpt(2)
      wap1 = - cop(2) * wap
      wap2 = - cop(1) * wap
      do j = riMaxIn,0,-1
        wp1 = wpl(:,j)
        auxa  = - wp1(2)*wap2(2) + wp1(3)*wap2(3)
        auxb  = + wp1(2)*wap2(4) - wp1(3)*wap2(1)
        auxc  = - wp1(0)*wap1(1) - wp1(1)*wap1(3)
        auxd  = - wp1(0)*wap1(4) - wp1(1)*wap1(2)
        spgg(0,0) = + auxc
        spgg(1,0) = + auxd
        spgg(2,0) = + auxa
        spgg(3,0) = + auxb
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(0,2) = + auxd*cima
        spgg(1,2) = - auxc*cima
        spgg(2,2) = - auxb*cima
        spgg(3,2) = + auxa*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0)= + auxc*pl(2) - auxd*pl(3) + auxa*m
        spg(1)= - auxc*pl(4) + auxd*pl(1) + auxb*m
        spg(2)= + auxa*pl(1) + auxb*pl(3) + auxc*m
        spg(3)= + auxa*pl(4) + auxb*pl(2) + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:3,riOut) = spgg(0:3,mu)
          else;  wp(0:3,riOut) = wp(0:3,riOut) + spgg(0:3,mu)
          endif
        enddo
      enddo

    case (53) ! v + {f} -> {f}

      ! spg(i) = ( psibar*slash{eps}*(-slash{pl}+m) )_i
      ! spgg(i,mu) = ( psibar*slash{eps}*(-gamma(mu)) )_i
      ! eps = wp1
      ! bar{psi} = wp2
      ! auxa= (psibar*slash{eps}*omega_+)_0
      ! auxb= (psibar*slash{eps}*omega_+)_1
      ! auxc= (psibar*slash{eps}*omega_-)_2
      ! auxd= (psibar*slash{eps}*omega_-)_3

      pl = pl1 + pl2

      wp2(0:1) = - cop(2) * wpt(0:1)
      wp2(2:3) = - cop(1) * wpt(2:3)
      do j = riMaxIn,0,-1
        wp1 = wpl(:,j)
        wap(1) = wp1(0) + wp1(3)
        wap(2) = wp1(0) - wp1(3)
        wap(3) = wp1(1) + cima*wp1(2)
        wap(4) = wp1(1) - cima*wp1(2)
        auxa  = - wp2(2)*wap(2) + wp2(3)*wap(3)
        auxb  = + wp2(2)*wap(4) - wp2(3)*wap(1)
        auxc  = - wp2(0)*wap(1) - wp2(1)*wap(3)
        auxd  = - wp2(0)*wap(4) - wp2(1)*wap(2)
        spgg(0,0) = + auxc
        spgg(1,0) = + auxd
        spgg(2,0) = + auxa
        spgg(3,0) = + auxb
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(0,2) = + auxd*cima
        spgg(1,2) = - auxc*cima
        spgg(2,2) = - auxb*cima
        spgg(3,2) = + auxa*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0)= + auxc*pl(2) - auxd*pl(3) + auxa*m
        spg(1)= - auxc*pl(4) + auxd*pl(1) + auxb*m
        spg(2)= + auxa*pl(1) + auxb*pl(3) + auxc*m
        spg(3)= + auxa*pl(4) + auxb*pl(2) + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:3,riOut) = spgg(0:3,mu)
          else;  wp(0:3,riOut) = wp(0:3,riOut) + spgg(0:3,mu)
          endif
        enddo
      enddo

    case (55) ! {f}_- + v -> {f}_-

      pl = pl1 + pl2

      wap(1) = wpt(0) + wpt(3)
      wap(2) = wpt(0) - wpt(3)
      wap(3) = wpt(1) + cima*wpt(2)
      wap(4) = wpt(1) - cima*wpt(2)
      wap = - cop(1) * wap
      do j = riMaxIn,0,-1
        wp1(2:3) = wpl(2:3,j)
        auxa  = - wp1(2)*wap(2) + wp1(3)*wap(3)
        auxb  = + wp1(2)*wap(4) - wp1(3)*wap(1)
        spgg(2,0) = + auxa
        spgg(3,0) = + auxb
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(2,2) = - auxb*cima
        spgg(3,2) = + auxa*cima
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0)=                           + auxa*m
        spg(1)=                           + auxb*m
        spg(2)= + auxa*pl(1) + auxb*pl(3)
        spg(3)= + auxa*pl(4) + auxb*pl(2)
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(2:3,riOut) = spgg(2:3,mu)
                 wp(0:1,riOut) = 0d0
          else;  wp(2:3,riOut) = wp(2:3,riOut) + spgg(2:3,mu)
          endif
        enddo
      enddo

    case (56) ! v + {f}_- -> {f}_-

      pl = pl1 + pl2

      wp2(2:3) = - cop(1) * wpt(2:3)
      do j = riMaxIn,0,-1
        wp1      = wpl(:,j)
        wap(1) = wp1(0) + wp1(3)
        wap(2) = wp1(0) - wp1(3)
        wap(3) = wp1(1) + cima*wp1(2)
        wap(4) = wp1(1) - cima*wp1(2)
        auxa  = - wp2(2)*wap(2) + wp2(3)*wap(3)
        auxb  = + wp2(2)*wap(4) - wp2(3)*wap(1)
        spgg(2,0) = + auxa
        spgg(3,0) = + auxb
        spgg(2,1) = - auxb
        spgg(3,1) = - auxa
        spgg(2,2) = - auxb*cima
        spgg(3,2) = + auxa*cima
        spgg(2,3) = - auxa
        spgg(3,3) = + auxb
        spg(0) =                           + auxa*m
        spg(1) =                           + auxb*m
        spg(2) = + auxa*pl(1) + auxb*pl(3)
        spg(3) = + auxa*pl(4) + auxb*pl(2)
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(2:3,riOut) = spgg(2:3,mu)
                 wp(0:1,riOut) = 0d0
          else;  wp(2:3,riOut) = wp(2:3,riOut) + spgg(2:3,mu)
          endif
        enddo
      enddo

    case (57) ! {f}_+ + v -> {f}_+

      pl = pl1 + pl2

      wap(1) = wpt(0) + wpt(3)
      wap(2) = wpt(0) - wpt(3)
      wap(3) = wpt(1) + cima*wpt(2)
      wap(4) = wpt(1) - cima*wpt(2)
      wap = - cop(1) * wap
      do j = riMaxIn,0,-1
        wp1(0:1) = wpl(0:1,j)
        auxc  = - wp1(0)*wap(1) - wp1(1)*wap(3)
        auxd  = - wp1(0)*wap(4) - wp1(1)*wap(2)
        spgg(0,0) = + auxc
        spgg(1,0) = + auxd
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(0,2) = + auxd*cima
        spgg(1,2) = - auxc*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spg(0)= + auxc*pl(2) - auxd*pl(3)
        spg(1)= - auxc*pl(4) + auxd*pl(1)
        spg(2)=                           + auxc*m
        spg(3)=                           + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:1,riOut) = spgg(0:1,mu)
                 wp(2:3,riOut) = 0d0
          else;  wp(0:1,riOut) = wp(0:1,riOut) + spgg(0:1,mu)
          endif
        enddo
      enddo

    case (58) ! v + {f}_+ -> {f}_+

      pl = pl1 + pl2

      wp2(0:1) = - cop(1) * wpt(0:1)
      do j = riMaxIn,0,-1
        wp1      = wpl(:,j)
        wap(1) = wp1(0) + wp1(3)
        wap(2) = wp1(0) - wp1(3)
        wap(3) = wp1(1) + cima*wp1(2)
        wap(4) = wp1(1) - cima*wp1(2)
        auxc  = - wp2(0)*wap(1) - wp2(1)*wap(3)
        auxd  = - wp2(0)*wap(4) - wp2(1)*wap(2)
        spgg(0,0) = + auxc
        spgg(1,0) = + auxd
        spgg(0,1) = + auxd
        spgg(1,1) = + auxc
        spgg(0,2) = + auxd*cima
        spgg(1,2) = - auxc*cima
        spgg(0,3) = + auxc
        spgg(1,3) = - auxd
        spg(0)= + auxc*pl(2) - auxd*pl(3)
        spg(1)= - auxc*pl(4) + auxd*pl(1)
        spg(2)=                           + auxc*m
        spg(3)=                           + auxd*m
        wp(0:3,j) = spg(0:3)
        do mu = 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                 wp(0:1,riOut) = spgg(0:1,mu)
                 wp(2:3,riOut) = 0d0
          else;  wp(0:1,riOut) = wp(0:1,riOut) + spgg(0:1,mu)
          endif
        enddo
      enddo

    case (61,71) ! x + s -> x, s + x -> x, {x} + s -> {x}, s + {x} -> {x}

      cc1 = - cop(1) * wpt(0)
      wp(0,:) = cc1 * wpl(0,:)
      wp(1:3,:) = cnul

    case (62,63,72,73) ! x + v -> x, v + x -> x, {x} + v -> {x}, v + {x} -> {x}

      select case (ty)
      case (62)
        coefq = - 1
        pp = - p1 - p2
        top = 1
      case (63)
        coefq = - 1
        pp = - p1 - p2
        top = 2
      case (72)
        coefq = + 1
        pp = + p1
        top = 1
      case (73)
        coefq =   0
        pp = + p2
        top = 2
      end select

      select case (top)
      case (1)
        !
        !  |
        !  | loop line in:
        !  V s = s0 + s1(mu1)*q(mu1) + s2(mu1,mu2)*q(mu1)*q(mu2) + ...
        !  | momentum (incoming) = q + p(el)
        !  |
        !  0---<-- tree line in:
        !  |       v(nu)
        !  |       momentum (incoming) = p(et)
        !  |
        !  |
        !  V loop line out:
        !  | ss
        !  | momentum (incoming) = - q - p(e3)
        !  |
        !
        !  coupling = - cop(1) * ( coefq*q(nu) + pp(nu) )
        !
        spr = pp(0)*wpt(0) - sum(pp(1:3)*wpt(1:3))
        cc1 = - cop(1) * spr
        cc2 = - cop(1) * coefq
        vv2 = cc2 * wpt
        do j = riMaxIn,0,-1
          wp(0,j) = cc1 * wpl(0,j)
          if ( coefq .ne. 0 ) then
            do mu = 0,3
              riOut = incRI(mu,j)
              if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                     wp(0,riOut) = wpl(0,j)*vv2(mu)
              else;  wp(0,riOut) = wp(0,riOut) + wpl(0,j)*vv2(mu)
              endif
            enddo
          endif
        enddo
      case (2)
        !
        !  |
        !  | loop line in:
        !  V v(nu) = v0(nu) + v1(mu1,nu)*q(mu1) + v2(mu1,mu2,nu)*q(mu1)*q(mu2) + ...
        !  | momentum (incoming) = q + p(el)
        !  |
        !  0---<-- tree line in:
        !  |       s
        !  |       momentum (incoming) = p(et)
        !  |
        !  |
        !  V loop line out:
        !  | ss
        !  | momentum (incoming) = - q - p(e3)
        !  |
        !
        !  coupling = sign * - cop(1) * ( coefq*q(nu) + pp(nu) )
        !
        cc1 = - cop(1) * wpt(0)
        vv1 = cc1 * pp
        cc2 = cc1 * coefq
        do j = riMaxIn,0,-1
          wp(0,j) = vv1(0)*wpl(0,j) - sum(vv1(1:3)*wpl(1:3,j))
          if ( coefq .ne. 0 ) then
            do mu = 0,3
              riOut = incRI(mu,j)
              if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
                     wp(0,riOut) = cc2 * wpl(mu,j)
              else;  wp(0,riOut) = wp(0,riOut) + cc2 * wpl(mu,j)
              endif
            enddo
          endif
        enddo
      end select

    ! additional rules for Ghost & BFM vertices
    case (160) ! vQ + v -> vQ

      p12(:)= -2*p2
      p13(:)= 2*p1 + p2
      p23(:)= 2*p2

      !  |
      !  | loop line in:
      !  V v(mu) = v0(mu) + v1(mu1,mu)*q(mu1) + v2(mu1,mu2,mu)*q(mu1)*q(mu2) + ...
      !  | momentum (incoming) k1 = q + p(e1)
      !  |
      !  0---<-- tree line in:
      !  |       vv(nu)
      !  |       momentum (incoming) k2 = p(e2)
      !  |
      !  |
      !  V loop line out:
      !  | vvv(ro)
      !  | momentum (incoming) k3 = - q - p(e3)
      !  |
      !
      !  coupling =
      !    - (- cop(1)) * (
      !    + g(mu,nu)*( k1(ro) - k2(ro) + k3(mu) )
      !    + g(nu,ro)*( k2(mu) - k3(mu) - k1(mu) )
      !    + g(mu,ro)*( k3(nu) - k1(nu) )
      !    ) =
      !    - (- cop(1)) * (
      !    + g(mu,nu)*(                  - 2*p2(ro) )
      !    + g(nu,ro)*(                  + 2*p2(mu) )
      !    + g(mu,ro)*( - 2*q(nu) - p1(nu) - p3(nu) )
      !    )

      sprt = p13(0)*wpt(0) - sum(p13(1:3)*wpt(1:3))
      cc1  = cop(1) * sprt
      vv1  = - cop(1) * p23
      vv2  = - cop(1) * p12
      do mu = 0,3
        gg1(:,mu) = vv1 * wpt(mu) + wpt * vv2(mu)
      enddo

      cc3 = 2 * cop(1)
      vv3 = cc3 * wpt

      do j = riMaxIn,0,-1

        do mu= 0,3
          vv5(mu) = gg1(0,mu)*wpl(0,j) - sum(gg1(1:3,mu)*wpl(1:3,j))
        enddo

        spr = wpt(0)*wpl(0,j) - sum(wpt(1:3)*wpl(1:3,j))

        wp(:,j) = cc1*wpl(:,j) + vv5(:)
        do mu= 0,3
          riOut = incRI(mu,j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
            wp(:,riOut) = + vv3(mu)*wpl(:,j)
          else
            wp(:,riOut) = wp(:,riOut) + vv3(mu)*wpl(:,j)
          endif
        enddo

      enddo

    case (161) ! s + s -> v
      ! -P(3,2)*c0
      do j = riMaxIn, 0, -1
        wp(:,j) = -p2(:)*cop(1)*wpt(0)*wpl(0,j)
      end do

    case (162) ! v + s -> s
      ! -P(1,2)*c0
      wp(0,:) = -cop(1)*wpt(0)*(- p2(0)*wpl(0,:) + p2(1)*wpl(1,:) &
                              + p2(2)*wpl(2,:) + p2(3)*wpl(3,:))
      wp(1:3,:) = cnul

    case(165)! s + v -> s
      ! -P(2,3)*c0
      pp = + p1 + p2
      spr = wpt(0)*pp(0) - sum(wpt(1:3)*pp(1:3))
      do j = riMaxIn, 0, -1
        cc1 = -cop(1)*wpl(0,j)
        wp(0,j) = cc1*spr
        do mu = 0, 3
          riOut = incRI(mu, j)
          if ((riOut .gt. riMaxIn) .and. firstRI(mu,j)) then
            wp(0,riOut) = + wpt(mu)*cc1
          else
            wp(0,riOut) = wp(0,riOut) + wpt(mu)*cc1
          end if
        end do
      end do
      wp(1:3,:) = cnul

    case(166)
    ! -P(2,1)*c0
      spr = -wpt(0)*p1(0) + sum(wpt(1:3)*p1(1:3))
      do j = riMaxIn, 0, -1
        cc1 = -cop(1)*wpl(0,j)
        wp(0,j) = cc1*spr
        do mu = 0, 3
          riOut = incRI(mu, j)
          if ((riOut .gt. riMaxIn) .and. firstRI(mu,j)) then
            wp(0,riOut) = - wpt(mu)*cc1
          else
            wp(0,riOut) = wp(0,riOut) - wpt(mu)*cc1
          end if
        end do
      end do
      wp(1:3,:) = cnul

    case(163)
      ! -P(1,3)*c0
      cc1 = -cop(1)*wpt(0)
      pp = + p1 + p2
      do j = riMaxIn, 0, -1
        wp(0,j) = cc1*(pp(0)*wpl(0,j) - pp(1)*wpl(1,j) - pp(2)*wpl(2,j) &
                       - pp(3)*wpl(3,j))
        do mu = 0, 3
          riOut = incRI(mu, j)
          wp(0,riOut) = wp(0,riOut) + wpl(mu,j)*cc1
          if ((riOut .gt. riMaxIn) .and. firstRI(mu,j)) then
            wp(0,riOut) = + wpl(mu,j)*cc1
          else
            wp(0,riOut) = wp(0,riOut) + wpl(mu,j)*cc1
          endif
        end do
      end do
      wp(1:3,:) = cnul

    case (164) ! s + s -> v
      ! -P(3,1)*c0
      do j = riMaxIn, 0, -1
        cc1 = -cop(1)*wpt(0)*wpl(0,j)
        wp(:,j) = p1(:)*cc1
        do mu = 0, 3
          riOut = incRI(mu, j)
          if ((riOut .gt. riMaxIn) .and. firstRI(mu,j)) then
            wp(:,riOut) = + gg(:,mu)*cc1
          else
            wp(:,riOut) = wp(:,riOut) + gg(:,mu)*cc1
          end if
        end do
      end do


    case (VVS_heft)
      do j = riMaxIn, 0, -1
        cc1 = cop(1)*(-P2(0)*wpl(0,j) + P2(1)*wpl(1,j) + P2(2)*wpl(2,j) + P2(3)*wpl(3,j))
        cc2 = cop(2)*(-wpt(0)*wpl(0,j) + wpt(1)*wpl(1,j) + wpt(2)*wpl(2,j) + wpt(3)*wpl(3,j))
        wp(0,j) = (cc1*(-P1(0)*wpt(0) + P1(1)*wpt(1) + P1(2)*wpt(2) + P1(3)*wpt(3)) &
                 + cc2*(-P1(0)*P2(0) + P1(1)*P2(1) + P1(2)*P2(2) + P1(3)*P2(3)))
        do mu = 0, 3
          riOut = incRI(mu, j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
            wp(0,riOut) = - (P2(mu)*cc2 + wpt(mu)*cc1)
          else
            wp(0,riOut) = wp(0,riOut) - (P2(mu)*cc2 + wpt(mu)*cc1)
          end if
        end do
      end do
      wp(1:3,:) = cnul

    case (VSV_heft)
!       P(1,3)*P(3,1)*c0
!       Metric(1,3)*P(-1,1)*P(-1,3)*c1

        pp = + p1 + p2
        do j = riMaxIn, 0, -1
          cc1 = wpl(0,j)*pp(0)-wpl(1,j)*pp(1)-wpl(2,j)*pp(2)-wpl(3,j)*pp(3)
          wp(:,j) = - wpt(0)*( &
                    + cop(1)*p1(:)*cc1 &
                    + cop(2)*wpl(:,j)*(p1(0)*pp(0)-p1(1)*pp(1)-p1(2)*pp(2)-p1(3)*pp(3)))
          do mu = 0, 3
            riOut = incRI(mu, j)
            wp(:,riOut) = wp(:,riOut) - wpt(0)*( &
                        + cop(1)*(p1(:)*wpl(mu,j) + gg(:,mu)*(cc1)) &
                        + cop(2)*wpl(:,j)*(2*p1(mu) + p2(mu)))

            riOut = incRI(mu, incRI(mu, j))
            wp(:,riOut) = wp(:,riOut)  &
                          - wpt(0)*(cop(1)*(gg(:,mu))*wpl(mu,j) &
                          + cop(2)*gg(mu,mu)*wpl(:,j))
            do nu = 0, mu-1
              riOut = incRI(nu, incRI(mu, j))
              wp(:,riOut) = wp(:,riOut)  &
                            - wpt(0)*(cop(1)*(gg(:,nu)*wpl(mu,j)+gg(:,mu)*wpl(nu,j)))
            end do
          end do
        end do

    case (SVV_heft)
      do j = riMaxIn, 0, -1
        wp(:,j) = -wpl(0,j)*(P2(0)**2*cop(2)*wpt(:) + P2(:)*cop(1)*(wpt(0)*(P1&
                  (0) + P2(0)) - wpt(1)*(P1(1) + P2(1)) - wpt(2)*(P1(2) + P2(2 &
                  )) - wpt(3)*(P1(3) + P2(3))) - cop(2)*wpt(:)*(P2(1)**2 + P2(2 &
                  )**2 + P2(3)**2) + cop(2)*wpt(:)*(P1(0)*P2(0) - P1(1)*P2(1   &
                  ) - P1(2)*P2(2) - P1(3)*P2(3)))
        do mu = 0, 3
          riOut = incRI(mu, j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
            wp(:,riOut) = - wpl(0,j)*(P2(:)*cop(1)*wpt(mu) + P2(mu)*cop(2)*wpt(:))
          else
            wp(:,riOut) = wp(:,riOut) &
                          - wpl(0,j)*(P2(:)*cop(1)*wpt(mu) + P2(mu)*cop(2)*wpt(:))
          end if
        end do
      end do

    case default

      write(*,*) "ty:", ty
      call error_rcl('Wrong 3-leg interaction', where='loop3')

    end select

  end subroutine loop3

!------------------------------------------------------------------------------!

  subroutine loop4(riMaxIn,riMaxOut,p1,p2,p3,pl1,pl2,pl3, &
                   m,c,ty,wpl,wpt1,wpt2,wp)

    integer,     intent (in)    :: riMaxIn,riMaxOut,ty
    complex(dp), intent (in)    :: m,c(:),p1(0:3),p2(0:3),p3(0:3), &
                                   pl1(4),pl2(4),pl3(4),           &
                                   wpl(0:3,0:riMaxIn),             &
                                   wpt1(0:3),wpt2(0:3)
    complex(dp), intent (out)   :: wp(0:3,0:riMaxOut)

    integer     :: j,mu,riOut
    complex(dp) :: c1,cc1,cc2,wpr,wpr1,wpr2,wpr3,wp2(0:3),wp3(0:3)
    complex(dp) :: x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,       &
                   x11,x12,x13,x14,x15,x16,x17,x18,x19,x20, &
                   x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32

    c1 = c(1)

    ! select interaction type and contract currents

    select case (ty)

    case (-1) ! s + s + s -> s

      cc1 = - c1 * wpt1(0) * wpt2(0)
      wp(0,:) = cc1 * wpl(0,:)
      wp(1:3,:) = cnul

    case (-2) ! s + v + v -> s

      wpr = wpt1(0)*wpt2(0) - sum(wpt1(1:3)*wpt2(1:3))
      cc1 = - c1 * wpr
      wp(0,  :) = cc1 * wpl(0,:)
      wp(1:3,:) = cnul

    case (-3) ! v + s + v -> s

      cc1 = - c1 * wpt1(0)
      do j = 0,riMaxOut
        wpr = wpt2(0)*wpl(0,j) - sum(wpt2(1:3)*wpl(1:3,j))
        wp(0,j) = cc1 * wpr
      enddo
      wp(1:3,:) = cnul

    case (-4) ! v + v + s -> s

      cc1 = - c1 * wpt2(0)
      do j = 0,riMaxOut
        wpr = wpt1(0)*wpl(0,j) - sum(wpt1(1:3)*wpl(1:3,j))
        wp(0,j) = cc1 * wpr
      enddo
      wp(1:3,:) = cnul

    case (-11) ! v + v + v -> v

      wpr1 = wpt1(0)*wpt2(0) - sum(wpt1(1:3)*wpt2(1:3)) ! wp2*wp3
      cc1 = c(1) * wpr1
      wp2 = c(2) * wpt2
      wp3 = c(3) * wpt1
      do j = 0,riMaxOut
        wpr2 = wp2(0)*wpl(0,j) - sum(wp2(1:3)*wpl(1:3,j)) ! wp1*wp3*cc(2)
        wpr3 = wp3(0)*wpl(0,j) - sum(wp3(1:3)*wpl(1:3,j)) ! wp1*wp2*cc(3)
        wp(:,j) = cc1*wpl(:,j) + wpr2*wpt1 + wpr3*wpt2
      enddo

    case (-12) ! s + s + v -> v

      cc1 = c1 * wpt1(0)
      do j = 0,riMaxOut
        cc2 = cc1 * wpl(0,j)
        wp(:,j) = cc2 * wpt2(:)
      enddo

    case (-13) ! v + s + s -> v

      cc1 = c1 * wpt1(0) * wpt2(0)
      wp(:,:) = cc1 * wpl(:,:)

    case (-14) ! s + v + s -> v

      cc1 = c1 * wpt2(0)
      do j = 0,riMaxOut
        cc2 = cc1 * wpl(0,j)
        wp(:,j) = cc2 * wpt1(:)
      enddo

    ! BFM additional
    case (-111) ! v + v + v -> v

      wpr1 = wpt1(0)*wpt2(0) - sum(wpt1(1:3)*wpt2(1:3)) ! wp2*wp3
      cc1 = c(1) * wpr1
      do j = 0,riMaxOut
        wp(:,j) = cc1*wpl(:,j)
      enddo

    ! HEFT
    case (VVSS_heft)
      call loop3(riMaxIn,riMaxOut,p1,p2,pl1,pl2, &
                 m,c,VVS_heft,wpl,wpt1,wp)
      wp = wp * wpt2(0)

    case (VSSV_heft)
      call loop3(riMaxIn,riMaxOut,p1,p2+p3,pl1,pl2, &
                 m,c,VSV_heft,wpl,wpt1,wp)
      wp = wp * wpt2(0)

    case (SVVS_heft)
      wp(0,:) = wpl(0,:)*(c(1)*(P2(0)*wpt2(0) - P2(1)*wpt2(1) - P2(2)*wpt2(2           &
                ) - P2(3)*wpt2(3))*(P3(0)*wpt1(0) - P3(1)*wpt1(1) - P3(2)*wpt1(2) -     &
                P3(3)*wpt1(3)) + c(2)*(P2(0)*P3(0) - P2(1)*P3(1) - P2(2)*P3(2           &
                ) - P2(3)*P3(3))*(wpt1(0)*wpt2(0) - wpt1(1)*wpt2(1) - wpt1(2)*wpt2(2) - &
                wpt1(3)*wpt2(3)))
      wp(1:3,:) = cnul

    case (SVSV_heft)
      call loop3(riMaxIn,riMaxOut,p1+p3,p2,pl1,pl2, &
                 m,c,SVV_heft,wpl,wpt1,wp)
      wp = wp * wpt2(0)

    case (VVVS_heft)
      x0 = wpt1(1)*wpt2(1)
      x1 = wpt1(2)*wpt2(2)
      x2 = wpt1(0)*wpt2(0)
      x3 = x0 + x1 - x2
      x4 = P1(0)*wpt1(0)
      x5 = P3(1)*wpt1(1)
      x6 = P3(2)*wpt1(2)
      x7 = P1(1)*wpt1(1)
      x8 = P1(2)*wpt1(2)
      x9 = P3(0)*wpt1(0)
      x10 = P1(0)*wpt2(0)
      x11 = P2(1)*wpt2(1)
      x12 = P2(2)*wpt2(2)
      x13 = P1(1)*wpt2(1)
      x14 = P1(2)*wpt2(2)
      x15 = P2(0)*wpt2(0)
      x21 = P1(3)*wpt2(3)
      x22 = P1(3)*wpt1(3)
      x25 = wpt1(3)*wpt2(3)
      x27 = P2(3)*wpt2(3)
      x31 = P3(3)*wpt1(3)
      do j = riMaxIn, 0, -1
        x16 = wpt2(1)*wpl(1,j)
        x17 = wpt2(2)*wpl(2,j)
        x18 = wpt2(0)*wpl(0,j)
        x19 = wpt1(2)*wpl(2,j)
        x20 = wpt1(1)*wpl(1,j)
        x23 = wpt1(0)*wpl(0,j)
        x24 = P2(1)*wpl(1,j)
        x26 = P2(2)*wpl(2,j)
        x28 = P3(0)*wpl(0,j)
        x29 = P3(1)*wpl(1,j)
        x30 = P3(2)*wpl(2,j)
        x32 = P2(0)*wpl(0,j)
        wp(0,j) = -c(1)*(P2(3)*wpl(3,j)*x3 - P3(3)*wpl(3,j)*x3 + wpt1(3)*wpl(3,j)*(-&
                  x10 - x11 - x12 + x13 + x14 + x15) + wpt2(3)*wpl(3,j)*x4 + wpt2(&
                  3)*wpl(3,j)*x5 + wpt2(3)*wpl(3,j)*x6 - wpt2(3)*wpl(3,j)*x7 - wpt2(3 &
                  )*wpl(3,j)*x8 + x0*(x26 + x28 - x30 - x32) + x1*(x24 + x28  &
                   - x29 - x32) - x10*(x19 + x20) - x11*x19 + x11*x23 - x12* &
                  x20 + x12*x23 + x13*x19 - x13*x23 + x14*x20 - x14*x23 + x15&
                  *x19 + x15*x20 + x16*x4 + x16*x6 - x16*x8 + x17*x4 + x17*x5&
                   - x17*x7 - x18*x5 - x18*x6 + x18*x7 + x18*x8 + x19*x21 +  &
                  x2*(-x24 - x26 + x29 + x30) + x20*x21 - x21*x23 + x25*(x24 &
                   + x26 + x28 - x29 - x30 - x32) - x27*(x19 + x20 - x23) -  &
                  x9*(wpt2(3)*wpl(3,j) + x16 + x17) - (x22 - x31)*(x16 + x17 -  &
                  x18))
        do mu = 0, 3
          riOut = incRI(mu, j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
            wp(0,riOut) = - c(1)*(wpt1(mu)*(wpt2(3)*wpl(3,j) + x16 + x17 &
                           - x18) - wpt2(mu)*(wpt1(3)*wpl(3,j) + x19 + x20 - x23))
          else
            wp(0,riOut) = wp(0,riOut) - c(1)*(wpt1(mu)*(wpt2(3)*wpl(3,j) + x16 + x17 &
                           - x18) - wpt2(mu)*(wpt1(3)*wpl(3,j) + x19 + x20 - x23))
          end if
        end do
      end do
      wp(1:3,:) = cnul

    case (VVSV_heft) ! v + v + s -> v
      call loop3(riMaxIn,riMaxOut,p1+p3/3d0,p2+p3/3d0,pl1,pl2, &
                 m,c,12,wpl,wpt1,wp)
      wp = wp * wpt2(0)

    case (SVVV_heft) ! s + v + v -> v
      x0 = -wpt1(0)*wpt2(0) + wpt1(1)*wpt2(1) + wpt1(2)*wpt2(2) + wpt1(3)*wpt2(3)
      do j = riMaxIn, 0, -1
        x1 = c(1)*wpl(0,j)
        wp(:,j) = x1*(P2(:)*x0 - P3(:)*x0 - wpt1(:)*(-wpt2(0)*(P1(0) + 2*P2(0   &
                  ) + P3(0)) + wpt2(1)*(P1(1) + 2*P2(1) + P3(1)) + wpt2(2)*(P1(2 &
                  ) + 2*P2(2) + P3(2)) + wpt2(3)*(P1(3) + 2*P2(3) + P3(3))) +  &
                  wpt2(:)*(-wpt1(0)*(P1(0) + P2(0) + 2*P3(0)) + wpt1(1)*(P1(1) + P2&
                  (1) + 2*P3(1)) + wpt1(2)*(P1(2) + P2(2) + 2*P3(2)) + wpt1(3)*( &
                  P1(3) + P2(3) + 2*P3(3))))
        do mu = 0, 3
          riOut = incRI(mu, j)
          if ((riOut.gt.riMaxIn).and.firstRI(mu,j)) then
            wp(:,riOut) = - x1*(-wpt1(:)*wpt2(mu) + wpt1(mu)*wpt2(:))
          else
            wp(:,riOut) = wp(:,riOut) - x1*(-wpt1(:)*wpt2(mu) + wpt1(mu)*wpt2(:))
          end if
        end do
      end do

    case default

      call error_rcl('Wrong 4-leg interaction.', where='loop4')

    end select

  end subroutine loop4

!------------------------------------------------------------------------------!

  subroutine loop5(riMaxIn,riMaxOut,p1,p2,p3,p4,pl1,pl2,pl3,pl4, &
                   m,c,ty,wpl,wpt1,wpt2,wpt3,wp)

    integer,     intent (in)    :: riMaxIn,riMaxOut,ty
    complex(dp), intent (in)    :: m,c(:),p1(0:3),p2(0:3),p3(0:3),p4(0:4), &
                                   pl1(4),pl2(4),pl3(4),pl4(4),            &
                                   wpl(0:3,0:riMaxIn)
    complex(dp), intent (inout) :: wpt1(0:3),wpt2(0:3),wpt3(0:3)
    complex(dp), intent (out)   :: wp(0:3,0:riMaxOut)

    integer :: j
    complex(dp) :: x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,       &
                   x11,x12,x13,x14,x15,x16,x17,x18,x19,x20, &
                   x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33

    select case (ty)

    case (VVVSS_heft)
      call loop4(riMaxIn,riMaxOut,p1,p2,p3,pl1,pl2,pl3, &
                 m,c,VVVS_heft,wpl,wpt1,wpt2,wp)
      wp(0,:) = wp(0,:) * wpt3(0)

    case (VVSSV_heft)
      call loop3(riMaxIn,riMaxOut,p1(0:3)+(p3(0:3)+p4(0:3))/3d0, &
                 p2(0:3)+(p3(0:3)+p4(0:3))/3d0,pl1,pl2, &
                 m,c,12,wpl,wpt1,wp)
      wp = wp * wpt2(0) * wpt3(0)

    ! TODO:  <31-07-18, J.-N. Lang> !
    ! take over tree parts
    case (SVVVS_heft)
      x0 = wpt1(1)*wpt2(1)
      x1 = wpt1(2)*wpt2(2)
      x2 = wpt1(0)*wpt2(0)
      x3 = x1 - x2
      x4 = x0 + x3
      x5 = P2(0)*wpt2(0)
      x6 = P4(1)*wpt2(1)
      x7 = P4(2)*wpt2(2)
      x8 = P2(1)*wpt2(1)
      x9 = P2(2)*wpt2(2)
      x10 = P4(0)*wpt2(0)
      x11 = P3(0)*wpt1(0)
      x12 = P4(1)*wpt1(1)
      x13 = P4(2)*wpt1(2)
      x14 = P3(1)*wpt1(1)
      x15 = P3(2)*wpt1(2)
      x16 = P4(0)*wpt1(0)
      x17 = wpt1(3)*wpt2(3)
      x18 = wpt1(1)*wpt3(1)
      x19 = wpt1(2)*wpt3(2)
      x20 = wpt1(0)*wpt3(0)
      x21 = P2(1)*wpt3(1)
      x22 = wpt1(1)*wpt2(1)*wpt3(2)
      x23 = wpt1(3)*wpt2(3)*wpt3(2)
      x24 = P2(3)*wpt2(3)
      x25 = P3(0)*wpt3(0)
      x26 = wpt2(2)*wpt3(2)
      x27 = wpt1(0)*wpt2(0)*wpt3(2)
      x28 = wpt2(1)*wpt3(1)
      x29 = P3(3)*wpt1(3)
      x30 = wpt2(0)*wpt3(0)
      x31 = P4(3)*wpt2(3)
      x32 = P4(3)*wpt1(3)
      x33 = P2(0)*wpt3(0)
      wp(0,:) = -c(1)*wpl(0,:)*(-P3(1)*wpt3(1)*(x17 + x3) + wpt1(3)*wpt3(3)*(- &
                x10 + x5 + x6 + x7 - x8 - x9) + wpt2(3)*wpt3(3)*(-x11 - x12 - x13&
                 + x14 + x15 + x16) + wpt3(3)*x4*(P2(3) - P3(3)) + x0*(x25 -   &
                x33) + x1*(x21 + x25 - x33) - x10*(x18 + x19) - x11*x26 - x11&
                *x28 - x12*x26 + x12*x30 - x13*x28 + x13*x30 + x14*x26 - x14*&
                x30 + x15*x28 - x15*x30 + x16*x26 + x16*x28 + x17*(x21 + x25 &
                 - x33) - x18*x24 + x18*x31 + x18*x5 + x18*x7 - x18*x9 - x19*&
                x24 + x19*x31 + x19*x5 + x19*x6 - x19*x8 - x2*x21 + x20*x24  &
                 - x20*x31 - x20*x6 - x20*x7 + x20*x8 + x20*x9 + x26*x29 +   &
                x28*x29 - x29*x30 - x32*(x26 + x28 - x30) + (P2(2) - P3(2))*(&
                x22 + x23 - x27))
      wp(1:3,:) = cnul

    case (SVVSV_heft)
      call loop4(riMaxIn,riMaxOut,p1,p2(0:3)+p4(0:3)/3d0, &
                 p3(0:3)+p4(0:3)/3d0,pl1,pl2,pl3,         &
                 m,c,SVVV_heft,wpl,wpt1,wpt2,wp)
      wp = wp * wpt3(0)

    case (VVVVS_heft)
      wp(0,:) = (c(1)*(-wpt1(0)*wpt2(0) + wpt1(1)*wpt2(1) + wpt1(2)*wpt2(2) + wpt1(3&
            )*wpt2(3))*(-wpt3(0)*wpl(0,:) + wpt3(1)*wpl(1,:) + wpt3(2)*wpl(2,:) + wpt3&
            (3)*wpl(3,:)) + c(2)*(-wpt1(0)*wpt3(0) + wpt1(1)*wpt3(1) + wpt1(2)*wpt3(2&
            ) + wpt1(3)*wpt3(3))*(-wpt2(0)*wpl(0,:) + wpt2(1)*wpl(1,:) + wpt2(2)*wpl(2&
            ,:) + wpt2(3)*wpl(3,:)) + c(3)*(-wpt1(0)*wpl(0,:) + wpt1(1)*wpl(1    &
            ,:) + wpt1(2)*wpl(2,:) + wpt1(3)*wpl(3,:))*(-wpt2(0)*wpt3(0) + wpt2(1)*wpt3&
            (1) + wpt2(2)*wpt3(2) + wpt2(3)*wpt3(3)))
      wp(1:3,:) = cnul

    case (VVVSV_heft)
      call loop4(riMaxIn,riMaxOut,p1,p2,p3,pl1,pl2,pl3, &
                 m,c,-11,wpl,wpt1,wpt2,wp)
      wp = wp * wpt3(0)

    case (SVVVV_heft)
      do j = riMaxIn, 0, -1
        wp(:,j) = -wpl(0,j)*(c(1)*wpt1(:) *                                        &
          (-wpt2(0)*wpt3(0) + wpt2(1)*wpt3(1) + wpt2(2)*wpt3(2) + wpt2(3)*wpt3(3)) &
                            + c(2)*wpt2(:) *                                       &
          (-wpt1(0)*wpt3(0) + wpt1(1)*wpt3(1) + wpt1(2)*wpt3(2) + wpt1(3)*wpt3(3)) &
                            + c(3)*wpt3(:) *                                       &
          (-wpt1(0)*wpt2(0) + wpt1(1)*wpt2(1) + wpt1(2)*wpt2(2) + wpt1(3)*wpt2(3)))
      end do

    case default

      call error_rcl('Wrong 5-leg interaction.', where='loop5')

    end select

  end subroutine loop5

!------------------------------------------------------------------------------!

  subroutine loop6(riMaxIn,riMaxOut,p1,p2,p3,p4,p5, &
                   pl1,pl2,pl3,pl4,pl5,             &
                   m,c,ty,wpl,wpt1,wpt2,wpt3,wpt4,wp)

    integer,     intent (in) :: riMaxIn,riMaxOut,ty
    complex(dp), intent (in) :: m,c(:),p1(0:3),p2(0:3),p3(0:3),p4(0:4),p5(0:4), &
                                pl1(4),pl2(4),pl3(4),pl4(4),pl5(4),             &
                                wpl(0:3,0:riMaxIn)
    complex(dp), intent (inout) :: wpt1(0:3),wpt2(0:3),wpt3(0:3),wpt4(0:3)
    complex(dp), intent (out)   :: wp(0:3,0:riMaxOut)

    select case (ty)

    case (VVVVSS_heft)
      call loop5(riMaxIn,riMaxOut,p1,p2,p3,p4,pl1,pl2,pl3,pl4, &
                 m,c,VVVVS_heft,wpl,wpt1,wpt2,wpt3,wp)
      wp = wp * wpt4(0)

    case (VVVSSV_heft)
      call loop5(riMaxIn,riMaxOut,p1,p2,p3,p4,pl1,pl2,pl3,pl4, &
                 m,c,VVVSV_heft,wpl,wpt1,wpt2,wpt3,wp)
      wp = wp * wpt4(0)

    case (SVVVVS_heft)
      wp(0,:) = wpl(0,:)*(c(1)*(-wpt1(0)*wpt4(0) + wpt1(1)*wpt4(1) + wpt1(2)*wpt4(2&
                ) + wpt1(3)*wpt4(3))*(-wpt2(0)*wpt3(0) + wpt2(1)*wpt3(1) + wpt2(2)*wpt3(2) + &
                wpt2(3)*wpt3(3)) + c(2)*(-wpt1(0)*wpt3(0) + wpt1(1)*wpt3(1) + wpt1(2)*wpt3(2&
                ) + wpt1(3)*wpt3(3))*(-wpt2(0)*wpt4(0) + wpt2(1)*wpt4(1) + wpt2(2)*wpt4(2) + &
                wpt2(3)*wpt4(3)) + c(3)*(-wpt1(0)*wpt2(0) + wpt1(1)*wpt2(1) + wpt1(2)*wpt2(2&
                ) + wpt1(3)*wpt2(3))*(-wpt3(0)*wpt4(0) + wpt3(1)*wpt4(1) + wpt3(2)*wpt4(2) + &
                wpt3(3)*wpt4(3)))
      wp(1:3,:) = cnul

    case (SVVVSV_heft)
      call loop5(riMaxIn,riMaxOut,p1,p2,p3,p4,pl1,pl2,pl3,pl4, &
                 m,c,SVVVV_heft,wpl,wpt1,wpt2,wpt3,wp)
      wp = wp * wpt4(0)

    case default

      call error_rcl('Wrong 6-leg interaction.', where='loop6')

    end select

  end subroutine loop6

!------------------------------------------------------------------------------!

  end module loop_vertices_rcl

!------------------------------------------------------------------------------!
