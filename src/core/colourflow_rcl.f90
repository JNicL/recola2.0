!******************************************************************************!
!                                                                              !
!    colourflow_rcl.f90                                                        !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module colourflow_rcl

!------------------------------------------------------------------------------!

  use globals_rcl, only: dp,Nc,Nc2,pow2

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  integer :: dMax, binMax,vecd,ia1bin,ia2bin
  integer, allocatable :: iq(:),ia(:,:),diq(:),dia(:,:),iabin(:,:),vec(:), &
                          ia1(:,:),ia2(:,:)

!------------------------------------------------------------------------------!
  contains

!------------------------------------------------------------------------------!

  subroutine fill_colordeltas(legs,pr)

    use globals_rcl, only: csTot,cd0sMax,csAmp,csIq,prs,nCs
    use tables_rcl, only: vectorLeg

    implicit none

    integer, intent(in) :: legs,pr
    integer :: i,i1,i2,cs,d,dbin,k,eq

    !-------------------------------------------------------------
    ! The two representations of the colour structures
    !-------------------------------------------------------------
    ! The amplitude has the form
    !
    !   A(ia_1,...ia_N;iq_1,...,iq_N) =
    !   sum_P d(ia_P(1),iq_1) * ... * d(ia_P(N),iq_N) * A_P.
    !
    ! or equivalentely
    !
    !   A(ia_1,...ia_N;iq_1,...,iq_N) =
    !   sum_Q d(ia_1,iq_Q(N)) * ... * d(ia_L,iq_Q(N)) * A_Q,
    !
    ! N = number of gluons + number of quark-antiquark pairs
    !     (all particle are incoming).
    ! The sums run over all possible permutations P or Q of the
    ! labels.
    !
    ! Being L the  number of legs of the process, the variables
    ! csIa(1:L,P,pr) contain the informations in the first form:
    !
    ! csIa(k,P,pr) = P(k) if k is a gluon or an incoming quark
    !              = 0    otherwise
    !
    ! Here we construct the variables csIq(1:L,Q,pr) which
    ! contain the informations in the second form:
    !
    ! csIq(k,Q,pr) = Q(k) if k is a gluon or an incoming antiquark
    !              = 0    otherwise
    !-------------------------------------------------------------

    do cs = 1,csTot(pr)
      csIq(:,cs,pr) = 0
      do i = 1,legs
        if (csAmp(i,cs,pr).ne.0) csIq(csAmp(i,cs,pr),cs,pr) = i
      enddo
    enddo
    !-------------------------------------------------------------
    ! Contraction of colour structures for the squared amplitude
    !-------------------------------------------------------------
    ! The squared amplitude is then defined as (we use the first
    ! representation):
    !
    !   A^2 =
    !   fac*sum_(ia_1,...ia_N;iq_1,...,iq_N)
    !       |A(ia_1,...ia_N;iq_1,...,iq_N)|^2 =
    !   fac*sum_(ia_1,...ia_N;iq_1,...,iq_N) sum_P sum_P'
    !       d(ia_P(1),iq_1) * ... * d(ia_P(N),iq_N) *
    !       d(ia_P'(1),iq_1) * ... * d(ia_P'(N),iq_N) *
    !       A_P * A_P'
    !
    ! where fac = factor(pr) contains:
    ! - the symmetry factor for identical outgoing particles
    ! - the spin averaging factor for incoming particles
    ! - the colour averaging factor for incoming particles
    !
    ! N = number of gluons + number of quark-antiquark pairs
    !-------------------------------------------------------------

    ! Here we define new colour structures without 0's
    ! dMax = N

    dMax = cd0sMax(pr); binMax = 2**dMax-1
    allocate (iq(dMax),ia(dMax,csTot(pr)))
    allocate (diq(legs),dia(legs,csTot(pr))); diq = 0; dia = 0
    allocate (iabin(binMax,csTot(pr)))
    allocate (vec(dMax))

    do cs = 1,csTot(pr)
      d = 0
      do k = 1,legs
        if (csAmp(k,cs,pr).ne.0) then
          d = d + 1
           iq(d) = k;  ia(d             ,cs) = csAmp(k,cs,pr)
          diq(k) = d; dia(csAmp(k,cs,pr),cs) = d
        end if
      end do
      do dbin = 1,binMax
        ! For each colour structure (with index cs) of the
        ! amplitude, which is a product of dMax deltas, we
        ! consider here all possible subproducts of these deltas.
        ! We use binary notation: for example the subproduct
        ! dbin=11=1+2+8 is the product of the first
        ! (pow(1)=2^(1-1)=1), the second (pow(2)=2^(2-1)=2) and
        ! fourth (pow(4)=2^(4-1)=8) deltas.
        ! iabin(cs,dbin) is then the sum of the binary
        ! "ia"-indices of this sub product.
        ! For example, if the subproduct has "ia"-indices 2,3,4,
        ! then the binary "ia"-indices are 2,4,8 and
        ! iabin = pow(2) + pow(3) + pow(4) = 2 + 4 + 8 = 14.
        ! Analogously one should compute also the sum of the
        ! binary "iq"-indices, but this is always equal to dbin,
        ! because the "iq" indices are always in the ascending
        ! order 1,2,3,4,...
        iabin(dbin,cs) = 0
        do d = 1,dMax
          if (vectorLeg(dbin,d).eq.1) &
            iabin(dbin,cs) = iabin(dbin,cs) + 2**(ia(d,cs)-1)
        end do
      end do
    end do

    ! All pairs of colored final structures are compared.These pairs appear in
    ! the computation of the squared amplitude: the two structures (each of them
    ! is a product of deltas) are multiplied and the deltas are then fully
    ! contracted and give a colour coefficient (colcoef). Here we make this
    ! contraction.  In order to do it, we look at all subproducts of the two
    ! structures of the pair. The "iq"-indices are in both structures in the
    ! same ascending order 1,2,3,4,...  per default. The point is to look at the
    ! "closed" subproducts. A subproduct is "closed" if the "ia"-indices in the
    ! first structure are the same (in a different order) as in the second
    ! structure. Example: str1=
    ! d(ia=2,iq=1)*d(ia=3,iq=2)*d(ia=4,iq=3)*d(ia=1,iq=4) str2=
    ! d(ia=3,iq=1)*d(ia=2,iq=2)*d(ia=4,iq=3)*d(ia=1,iq=4)
    !These two substructures have 3 "closed" subproducts: the subproduct made of
    !the first two deltas, the subproduct made of the third delta alone and the
    !subproduct made of the fourth delta alone.  Each "closed" subproduct
    !contributes to the colour coefficient with a factor Nc=3.  In order to
    !decide if a subproduct dbin is closed, it is enough to look at the
    !iabin(i,dbin), iabin(j,dbin) of the two structures. If they are equal, the
    !product is closed.  In order to avoid double counting, when a "closed"
    !subproduct dbin is found, the primary binaries composing it are saved (in
    !vec) and in the next steps just subproducts not containing these primary
    !binaries are considered.
    do i1 = 1,csTot(pr)
      do i2 = 1,csTot(pr)
        eq = 0
        vec = 0
        dbinloop: do dbin = 1,binMax
          do d = 1,dMax
            if (vectorLeg(dbin,d)*vec(d).eq.1) cycle dbinloop
          end do
          if (iabin(dbin,i1) .eq. iabin(dbin,i2)) then
            eq = eq + 1
            vec(:) = vec(:) + vectorLeg(dbin,1:dMax)
          end if
        end do dbinloop
        prs(pr)%colcoef(i1,i2)= nCs**eq
      end do
    end do
  end subroutine fill_colordeltas

!-------------------------------------------------------------
! Contraction of colour structures for the squared amplitude
! for colour correlation
!-------------------------------------------------------------
! Let us write the amplitude in the first representation
! using a compact notation:
!
!   A = sum_P str_P * A_P
!
! where
!
! str_P = d(ia_P(1),iq_1) * ... * d(ia_P(N),iq_N)
!
! Let us pick two lines l1 and l2 and consider the two
! amplitudes
!
!   A1 = sum_P  str_P(l1)  * A_P
!   A2 = sum_P' str_P'(l2) * A_P'
!
! where str_P(l1) is the structure that one obtains from
! str_P when line l1 emits an extra gluon; similarly
! str_P'(l2) is the structure that one obtains from str_P'
! when line l2 emits an extra gluon.
!
! Colour correlation means to compute
!
!   |A|_(l1,l2)^2 = c*A1*A2^* =
!   c * sum_P sum_P' str_P(l1) * str_P'(l2) * A_P^* * A_P'
!
! where c is a conventional factor depending of the type
! of particles l1 and l2 (which is choosen, such that colour
! correlation is indipendent from the normalization of the
! SU(Nc) generators).
!-------------------------------------------------------------
! Conventions:
!   t^a_(i,j) = 1/sqrt(2)*lambda^a_(i,j)
! being lambda^a_(i,j) the entry 'i,j' of the Gellmann
! matrix 'a'. The normalization is:
!   Tr(t^a t^b) = delta(a,b).
!-------------------------------------------------------------
! Let l be the line on which we attach the gluon (l = l1
! or l2).
!
! - If l is an incoming quark, in str we have a delta of
!   the type d(ia_X,iq_l). Adding a gluon with index 'a' to
!   line l corresponds to contracting with
!     - t^a_(iq_l,iq_l').
!   In the colour flow representation adding a gluon with
!   indices (ia,iq) to quark line l corresponds to
!   contracting with
!     str0_q(l,l',ia,iq) =
!     - t^a_(iq_l,iq_l')*t^a_(ia,iq) =
!     - d(ia,iq_l')*d(iq_l,iq) + 1/Nc*d(iq_l,iq_l')*d(ia,iq)
!
! - If l is an incoming antiquark, in str we have a delta
!   of the type d(ia_l,iq_X). Adding a gluon with index 'a' to
!   line l corresponds to contracting with
!     t^a_(ia_l',ia_l).
!   In the colour flow representation adding a gluon with
!   indices (ia,iq) to antiquark line l corresponds to
!   contracting with
!     str0_q~(l,l',ia,iq) =
!     t^a_(ia_l',ia_l)*t^a_(ia,iq) =
!     d(ia,ia_l)*d(ia_l',iq) - 1/Nc*d(ia_l',ia_l)*d(ia,iq)
!
! - If l is a gluon, in str we have a delta of the type
!   d(ia_l,Iq_X)*d(ia_X,iq_l). Adding a gluon with
!   index 'a' to line l corresponds to contracting with
!     i*f^{al' a al}.
!   In the colour flow representation adding a gluon with
!   indices (ia,iq) to gluon line l corresponds to
!   contracting with
!     str0_g(l,l',ia,iq) =
!     i*f^{al' a al}*
!     t^al'_(ia_l',iq_l')*t^a_(ia,iq)*t^al_(iq_l,ia_l) =
!     + d(ia_l',iq)*d(ia,ia_l)*d(iq_l,iq_l')
!     - d(ia_l',ia_l)*d(ia,iq_l')*d(iq_l,iq)
!-------------------------------------------------------------
! Let (ia,iq) be the indices of the gluon emitted by l1
! and (Ia,Iq) the indices of the gluon emitted by l2.
! Let the two colour structures be:
!   str1 = d(ia_P(1) ,iq_1)*...*d(ia_P(N) ,iq_N)
!   str2 = d(Ia_P'(1),Iq_1)*...*d(Ia_P'(N),Iq_N)
! One "iq"-index of str1 is iq_l1 and/or one "ia"-index of
! str1 is ia_l1. One "iq"-index of str2 is Iq_l2 and/or one
! "ia"-index of str2 is Ia_l2.
! Without colour correlation the contraction is done in
! this way:
!   colcoef = str1*str2*
!             d(iq_1,Iq_1)*d(ia_1,Ia_1)*
!             ...
!             d(iq_N,Iq_N)*d(ia_N,Ia_N)
! With colour correlation str1 is contracted with
! str0_p1(l1,l1',ia,iq), thus the indices ia_l1,iq_l1 are
! replaced by ia_l1',iq_l1' and the indices ia,iq appear;
! p1 is the particle type (q,q~,g) of line l1.
! With colour correlation str2 is contracted with
! str0_p2(l2,l2',Ia,Iq), thus the indices Ia_l2,Iq_l1 are
! replaced by Ia_l2',Iq_l2' and the indices Ia,Iq appear;
! p2 is the particle type (q,q~,g) of line l2.
! So contraction is done in this way:
!   colcoef = str1 * str2 *
!             str0_p1(l1,l1',ia,iq) *
!             str0_p2(l2,l2',Ia,Iq) *
!             d(iq,Iq) * d(ia,Ia) *
!             d(iq_1,Iq_1) * d(ia_1,Ia_1) *
!             ...
!             d(iq_N,Iq_N) * d(ia_N,Ia_N)
!-------------------------------------------------------------

  subroutine compute_colourcoefc(legs,pr, park)

    use globals_rcl, only: prs,csTot
    use modelfile, only: get_particle_type2_mdl

    implicit none

    integer, intent(in) :: legs,pr
    integer, dimension(:), intent(in) :: park

    integer        :: i1,i2,l1,l2,eq,n,nMax
    character(2)   :: ck1,ck2
    real(dp)       :: corfac(4),Cf,Ca,facr

    Cf = (Nc2-1d0)/(2d0*Nc)
    Ca = Nc
    do l1 = 1, legs
      ck1 = get_particle_type2_mdl(park(l1))
    do l2 = 1, legs
      ck2 = get_particle_type2_mdl(park(l2))
      prs(pr)%colcoefc(:,:,l1,l2) = 0d0
      if (l1 .eq. l2) cycle
      if (ck1.ne.'q'.and.ck1.ne.'q~'.and.ck1.ne.'g') cycle
      if (ck2.ne.'q'.and.ck2.ne.'q~'.and.ck2.ne.'g') cycle

      do i1 = 1,csTot(pr)
      do i2 = i1,csTot(pr)
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! q - q
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1 = d(ia_X1,iq_l1) * d(ia_X2,iq_l2) * R1
        !
        ! str1' =
        ! str1 * str0_q(l1,l1',ia,iq) * d(iq_l2',iq_l2) =
        ! R1 * d(ia_X2,iq_l2') * (
        ! - d(ia_X1,iq) * d(ia,iq_l1')
        ! + 1/Nc * d(ia,iq) * d(ia_X1,iq_l1')
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str2 = d(Ia_X1,Iq_l1) * d(Ia_X2,Iq_l2) * R2
        !
        ! str2' =
        ! str2 * str0_q(l2,l2',Ia,Iq) * d(iq_l1',iq_l1) =
        ! R2 * d(Ia_X1,Iq_l1') * (
        ! - d(Ia_X2,Iq) * d(Ia,Iq_l2')
        ! + 1/Nc * d(Ia,Iq) * d(Ia_X2,Iq_l2')
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1' * str2' *
        ! d(iq_l1',Iq_l1') * d(iq_l2',Iq_l2') *
        ! d(iq,Iq) * d(ia,Ia)
        ! =
        ! + R1 * d(ia_X1,Ia_X2) * d(ia_X2,Ia_X1) * R2
        ! - 1/Nc * R1 * d(ia_X1,Ia_X1) * d(ia_X2,Ia_X2) * R2
        ! =
        ! d(iq_l1,Iq_l1) * d(iq_l2,Iq_l2) * R1 * R2 * (
        ! + d(ia_X1,iq_l1) * d(ia_X2,iq_l2) * R1 *
        !   d(Ia_X2,Iq_l1) * d(Ia_X1,Iq_l2) * R2
        ! - 1/Nc * d(ia_X1,iq_l1) * d(ia_X2,iq_l2) * R1 *
        !          d(Ia_X1,Iq_l1) * d(Ia_X2,Iq_l2) * R2
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (ck1.eq.'q'.and.ck2.eq.'q') then ! q - q
          ! Number of contributions
          nMax = 2
          allocate (ia1(dMax,nMax),ia2(dMax,nMax))
          do n = 1,nMax
             ia1(:,n) = ia(:,i1)
             ia2(:,n) = ia(:,i2)
          enddo
          ! Overall factor
          facr = 1d0/2d0/Cf
          ! Contribution 1:
          !   str1' = str1
          ! str2' = str2 [Ia_X1 <-> Ia_X2]
          ia2(diq(l1),1) = ia(diq(l2),i2)
          ia2(diq(l2),1) = ia(diq(l1),i2)
          corfac(1) = facr
          ! Contribution 2:
          !   str1' = str1
          !   str2' = str2
          corfac(2) = - 1d0/Nc * facr
            !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            ! q~ - q~
            !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            ! str1 = d(ia_l1,iq_X1) * d(ia_l2,iq_X2) * R1
            !
            ! str1' =
            ! str1 * str0_q~(l1,l1',ia,iq) * d(ia_l2',ia_l2) =
            ! R1 * d(ia_l2',iq_X2) * (
            ! + d(ia_l1',iq) * d(ia,iq_X1)
            ! - 1/Nc * d(ia,iq) * d(ia_l1',iq_X1)
            ! )
            !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            ! str2 = d(Ia_l1,Iq_X1) * d(Ia_l2,Iq_X2) * R2
            !
            ! str2' =
            ! str2 * str0_q~(l2,l2',ia,iq) * d(Ia_l1',Ia_l1) =
            ! R2 *  d(Ia_l1',Iq_X1) * (
            ! + d(Ia_l2',Iq) *d(Ia,Iq_X2)
            ! - 1/Nc * d(Ia,Iq) * d(Ia_l2',Iq_X2)
            ! )
            !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            ! str1' * str2' *
            ! d(ia_l1',Ia_l1') * d(ia_l2',Ia_l2') *
            ! d(iq,Iq) * d(ia,Ia)
            ! =
            ! + R1 * d(Iq_X1,iq_X2) * d(Iq_X2,iq_X1) * R2
            ! - 1/Nc * R1 * d(Iq_X1,iq_X1) * d(Iq_X2,iq_X2) * R2
            ! =
            ! d(ia_l1,Ia_l1) * d(ia_l2,Ia_l2) * (
            ! + d(ia_l1,iq_X1) * d(ia_l2,iq_X2) * R1 *
            !   d(Ia_l2,Iq_X1) * d(Ia_l1,Iq_X2) * R2
            ! - 1/Nc * d(ia_l1,iq_X1) * d(ia_l2,iq_X2) * R1 *
            !          d(Ia_l1,Iq_X1) * d(Ia_l2,Iq_X2) * R2
            ! )
            !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        else if (ck1.eq.'q~'.and.ck2.eq.'q~') then ! q~ - q~
          ! Number of contributions
          nMax = 2
          allocate (ia1(dMax,nMax),ia2(dMax,nMax))
          do n = 1,nMax
             ia1(:,n) = ia(:,i1)
             ia2(:,n) = ia(:,i2)
          enddo
          ! Overall factor
          facr = 1d0/2d0/Cf
          ! Contribution 1:
          !   str1' = str1
          ! str2' = str2 [Ia_l1 <-> Ia_l2]
          ia2(dia(l1,i2),1) = ia(dia(l2,i2),i2)
          ia2(dia(l2,i2),1) = ia(dia(l1,i2),i2)
          corfac(1) = facr
          ! Contribution 2:
          !   str1' = str1
          !   str2' = str2
          corfac(2) = - 1d0/Nc * facr

        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! q - q~
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1 = d(ia_X1,iq_l1) * d(ia_l2,iq_X2) * R1
        !
        ! str1' =
        ! str1 * str0_q(l1,l1',ia,iq) * d(ia_l2',ia_l2) =
        ! R1 * d(ia_l2',iq_X2) * (
        ! - d(ia_X1,iq) * d(ia,iq_l1')
        ! + 1/Nc * d(ia,iq) * d(ia_X1,iq_l1')
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str2 = d(Ia_X1,Iq_l1) * d(Ia_l2,Iq_X2) * R2
        !
        ! str2' =
        ! str2 * str0_q~(l2,l2',ia,iq) * d(iq_l1',iq_l1) =
        ! R2 * d(Ia_X1,Iq_l1') * (
        ! + d(Ia_l2',Iq) * d(Ia,Iq_X2)
        ! - 1/Nc * d(Ia,Iq) * d(Ia_l2',Iq_X2)
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1' * str2' *
        ! d(iq_l1',Iq_l1') * d(ia_l2',Ia_l2') *
        ! d(iq,Iq) * d(ia,Ia)
        ! =
        ! - R1 * d(ia_X1,iq_X2) * d(Ia_X1,Iq_X2) * R2
        ! + 1/Nc * R1 * d(ia_X1,Ia_X1) * d(Iq_X2,iq_X2) * R2
        ! =
        ! - d(iq_l1,Iq_l1) * d(ia_l2,Ia_l2) *
        !   d(iq,Iq) * d(ia,Ia) * (
        !     d(ia,iq) * d(ia_X1,iq_l1) * d(ia_l2,iq_X2) * R1 *
        !     d(Ia_X1,Iq) * d(Ia_l2,Iq_l1) * d(Ia,Iq_X2) * R2
        !   )
        ! + 1/Nc * d(iq_l1,Iq_l1) * d(ia_l2,Ia_l2) * (
        !     d(ia_X1,iq_l1) * d(ia_l2,iq_X2) * R1 *
        !     d(Ia_X1,Iq_l1) * d(Ia_l2,Iq_X2) * R2
        !   )
        ! =
        ! 1/Nc * d(iq_l1,Iq_l1) * d(ia_l2,Ia_l2) * (
        ! - d(ia_l2,iq_l1) * d(ia_X1,iq_X2) * R1 *
        !   d(Ia_l2,Iq_l1) * d(Ia_X1,Iq_X2) * R2
        ! + d(ia_X1,iq_l1) * d(ia_l2,iq_X2) * R1 *
        !   d(Ia_X1,Iq_l1) * d(Ia_l2,Iq_X2) * R2
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        else if (ck1.eq.'q'.and.ck2.eq.'q~') then ! q - q~
          ! Number of contributions
          nMax = 2
          allocate (ia1(dMax,nMax),ia2(dMax,nMax))
          do n = 1,nMax
             ia1(:,n) = ia(:,i1)
             ia2(:,n) = ia(:,i2)
          enddo
          ! Overall factor
          facr = 1d0/2d0/Cf
          ! Contribution 1:
          ! str1' = str1 [ia_X1 <-> ia_l2]
          ! str2' = str2 [Ia_X1 <-> Ia_l2]
          ia1(diq(l1),1) = l2; ia1(dia(l2,i1),1) = ia(diq(l1),i1)
          ia2(diq(l1),1) = l2; ia2(dia(l2,i2),1) = ia(diq(l1),i2)
          corfac(1) = - 1d0/Nc * facr
          if (ia(diq(l1),i1).eq.l2) corfac(1) = corfac(1) * Nc
          if (ia(diq(l1),i2).eq.l2) corfac(1) = corfac(1) * Nc
          ! Contribution 2:
          !   str1' = str1
          !   str2' = str2
          corfac(2) = + 1d0/Nc * facr

        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! q~ - q
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1 = d(ia_l1,iq_X1) * d(ia_X2,iq_l2) * R1
        !
        ! str1' =
        ! R1 * d(ia_X2,iq_l2') * (
        ! str1 * str0_q~(l1,l1',ia,iq) * d(iq_l2',iq_l2) =
        ! + d(ia_l1',iq) * d(ia,iq_X1)
        ! - 1/Nc * d(ia,iq) * d(ia_l1',iq_X1)
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str2 = d(Ia_l1,Iq_X1) * d(Ia_X2,Iq_l2) * R2
        !
        ! str2' =
        ! R2 * d(Ia_l1',Iq_X1) * (
        ! str2 * str0_q(l2,l2',ia,iq) * d(ia_l1',ia_l1) =
        ! - d(Ia_X2,Iq) * d(Ia,Iq_l2')
        ! + 1/Nc * d(Ia,Iq) * d(Ia_X2,Iq_l2')
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1' * str2' *
        ! d(ia_l1',Ia_l1') * d(iq_l2',Iq_l2') *
        ! d(iq,Iq) * d(ia,Ia)
        ! =
        ! - R1 * d(ia_X2,iq_X1) * d(Ia_X2,Iq_X1) * R2
        ! + 1/Nc * R1 * d(ia_X2,Ia_X2) * d(Iq_X1,iq_X1) * R2
        ! =
        ! ...
        ! =
        ! 1/Nc * d(ia_l1,Ia_l1) * d(iq_l2,Iq_l2) * (
        ! - d(ia_X2,iq_X1) * d(ia_l1,iq_l2) * R1 *
        !   d(Ia_X2,Iq_X1) * d(Ia_l1,Iq_l2) * R2
        ! + d(ia_l1,iq_X1) * d(ia_X2,iq_l2) * R1 *
        !   d(Ia_l1,Iq_X1) * d(Ia_X2,Iq_l2) * R2
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        else if (ck1.eq.'q~'.and.ck2.eq.'q') then ! q~ - q
          ! Number of contributions
          nMax = 2
          allocate (ia1(dMax,nMax),ia2(dMax,nMax))
          do n = 1,nMax
             ia1(:,n) = ia(:,i1)
             ia2(:,n) = ia(:,i2)
          enddo
          ! Overall factor
          facr = 1d0/2d0/Cf
          ! Contribution 1:
          ! str1' = str1 [ia_l1 <-> ia_X2]
          ! str2' = str2 [Ia_l1 <-> Ia_X2]
          ia1(dia(l1,i1),1) = ia(diq(l2),i1); ia1(diq(l2),1) = l1
          ia2(dia(l1,i2),1) = ia(diq(l2),i2); ia2(diq(l2),1) = l1
          corfac(1) = - 1d0/Nc * facr
          if (ia(diq(l2),i1).eq.l1) corfac(1) = corfac(1) * Nc
          if (ia(diq(l2),i2).eq.l1) corfac(1) = corfac(1) * Nc
          ! Contribution 2:
          !   str1' = str1
          !   str2' = str2
          corfac(2) = + 1d0/Nc * facr

        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! q - g
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1 = d(ia_X1,iq_l1) *
        !        d(ia_X2,iq_l2) * d(ia_l2,iq_X3) * R1
        !
        ! str1' =
        ! str1 * str0_q(l1,l1',ia,iq) *
        !        d(iq_l2',iq_l2) * d(ia_l2',ia_l2)
        ! =
        ! R1 * d(ia_X2,iq_l2') * d(ia_l2',iq_X3) * (
        ! - d(ia,iq_l1' ) * d(iq ,ia_X1 )
        ! + 1/Nc * d(ia,iq) * d(ia_X1,iq_l1')
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str2 = d(Ia_X1,Iq_l1) *
        !        d(Ia_X2,Iq_l2) * d(Ia_l2,Iq_X3) * R2
        !
        ! str2' =
        ! str2 * str0_g(l2,l2',ia,iq) * d(Iq_l1',Iq_l1)
        ! =
        ! R2 * d(Ia_X1,Iq_l1') * (
        ! + d(Ia_l2',Iq) * d(Ia_X2,Iq_l2') * d(Ia,Iq_X3)
        ! - d(Ia_X2,Iq) * d(Ia,Iq_l2') * d(Ia_l2',Iq_X3)
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1' * str2' *
        ! d(iq_l1',Iq_l1') * d(iq_l2',Iq_l2') * d(ia_l2',Ia_l2') *
        ! d(iq,Iq) * d(ia,Ia)
        ! =
        ! - R1 * d(ia_X1,iq_X3) * d(ia_X2,Ia_X2) * d(Ia_X1,Iq_X3) * R2
        ! + R1 * d(ia_X1,Ia_X2) * d(ia_X2,Ia_X1) * d(iq_X3,Iq_X3) * R2
        ! =
        ! ...
        ! =
        ! d(iq_l1,Iq_l1) * d(iq_l2,Iq_l2) * d(ia_l2,Ia_l2) * (
        ! + d(ia_X1,iq_l1) * d(ia_X2,iq_l2) * d(ia_l2,iq_X3) * R1 *
        !   d(Ia_X2,Iq_l1) * d(Ia_X1,Iq_l2) * d(Ia_l2,Iq_X3) * R2
        ! - 1/Nc *
        !   d(ia_l2,iq_l1) * d(ia_X2,iq_l2) * d(ia_X1,iq_X3) * R1 *
        !   d(Ia_l2,Iq_l1) * d(Ia_X2,Iq_l2) * d(Ia_X1,Iq_X3) * R2
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        else if (ck1.eq.'q'.and.ck2.eq.'g') then ! q - g
          ! Number of contributions
          nMax = 2
          allocate (ia1(dMax,nMax),ia2(dMax,nMax))
          do n = 1,nMax
             ia1(:,n) = ia(:,i1)
             ia2(:,n) = ia(:,i2)
          enddo
          ! Overall factor
          facr = 1d0/2d0/Cf
          ! Contribution 1:
          ! str1' = str1
          ! str2' = str2 [Ia_X1 <-> Ia_X2]
          ia2(diq(l1),1) = ia(diq(l2),i2)
          ia2(diq(l2),1) = ia(diq(l1),i2)
          corfac(1) = facr
          ! Contribution 2:
          ! str1' = str1 [ia_X1 <-> ia_l2]
          ! str2' = str2 [Ia_X1 <-> Ia_l2]
          ia1(diq(l1),2) = l2; ia1(dia(l2,i1),2) = ia(diq(l1),i1)
          ia2(diq(l1),2) = l2; ia2(dia(l2,i2),2) = ia(diq(l1),i2)
          corfac(2) = - 1d0/Nc * facr
          if (ia(diq(l1),i1).eq.l2) corfac(2) = corfac(2) * Nc
          if (ia(diq(l1),i2).eq.l2) corfac(2) = corfac(2) * Nc

        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! q~ - g
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1 = d(ia_l1,iq_X1) *
        !        d(ia_X2,iq_l2) * d(ia_l2,iq_X3) * R1
        !
        ! str1' =
        ! str1 * str0_q~(l1,l1',ia,iq) *
        !        d(iq_l2',iq_l2) * d(ia_l2',ia_l2)
        ! =
        ! R1 * d(ia_X2,iq_l2') * d(ia_l2',iq_X3) * (
        ! + d(ia,iq_X1) * d(ia_l1',iq)
        ! - 1/Nc * d(ia,iq) * d(ia_l1',iq_X1)
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str2 = d(Ia_l1,Iq_X1) *
        !        d(Ia_X2,Iq_l2) * d(Ia_l2,Iq_X3) * R2
        !
        ! str2' =
        ! str2 * str0_g(l2,l2',ia,iq) * d(Ia_l1',Ia_l1)
        ! =
        ! R2 * d(Ia_l1',Iq_X1) * (
        ! + d(Ia_l2',Iq) * d(Ia_X2,Iq_l2') * d(Ia,Iq_X3)
        ! - d(Ia_X2,Iq) * d(Ia,Iq_l2') * d(Ia_l2',Iq_X3)
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1' * str2' *
        ! d(ia_l1',Ia_l1') * d(iq_l2',Iq_l2') * d(ia_l2',Ia_l2') *
        ! d(iq,Iq) * d(ia,Ia)
        ! =
        ! + R1 * d(iq_X1,Iq_X3) * d(ia_X2,Ia_X2) * d(iq_X3,Iq_X1) * R2
        ! - R1 * d(ia_X2,iq_X1) * d(Ia_X2,Iq_X1) * d(iq_X3,Iq_X3) * R2
        ! =
        ! ...
        ! =
        ! d(ia_l1,Ia_l1) * d(iq_l2,Iq_l2) * d(ia_l2,Ia_l2) * (
        ! + d(ia_l1,iq_X1) * d(ia_X2,iq_l2) * d(ia_l2,iq_X3) * R1 *
        !   d(Ia_l2,Iq_X1) * d(Ia_X2,Iq_l2) * d(Ia_l1,Iq_X3) * R2
        ! - 1/Nc *
        !   d(ia_X2,iq_X1) * d(ia_l1,iq_l2) * d(ia_l2,iq_X3) * R1 *
        !   d(Ia_X2,Iq_X1) * d(Ia_l1,Iq_l2) * d(Ia_l2,Iq_X3) * R2
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        else if (ck1.eq.'q~'.and.ck2.eq.'g') then ! q~ - g
          ! Number of contributions
          nMax = 2
          allocate (ia1(dMax,nMax),ia2(dMax,nMax))
          do n = 1,nMax
             ia1(:,n) = ia(:,i1)
             ia2(:,n) = ia(:,i2)
          enddo
          ! Overall factor
          facr = 1d0/2d0/Cf
          ! Contribution 1:
          ! str1' = str1
          ! str2' = str2 [Ia_l1 <-> Ia_l2]
          ia2(dia(l1,i2),1) = ia(dia(l2,i2),i2)
          ia2(dia(l2,i2),1) = ia(dia(l1,i2),i2)
          corfac(1) = facr
          ! Contribution 2:
          ! str1' = str1 [ia_l1 <-> ia_X2]
          ! str2' = str2 [Ia_l1 <-> Ia_X2]
          ia1(dia(l1,i1),2) = ia(diq(l2),i1); ia1(diq(l2),2) = l1
          ia2(dia(l1,i2),2) = ia(diq(l2),i2); ia2(diq(l2),2) = l1
          corfac(2) = - 1d0/Nc * facr
          if (ia(diq(l2),i1).eq.l1) corfac(2) = corfac(2) * Nc
          if (ia(diq(l2),i2).eq.l1) corfac(2) = corfac(2) * Nc

        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! g - q
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1 = d(ia_X1,iq_l1) * d(ia_l1,iq_X2) *
        !        d(ia_X3,iq_l2) * R1
        !
        ! str1' =
        ! str1 * str0_g(l1,l1',ia,iq) * d(iq_l2',iq_l2)
        ! =
        ! R1 * d(ia_X3,iq_l2') * (
        ! + d(ia_l1',iq) * d(ia_X1,iq_l1') * d(ia,iq_X2)
        ! - d(ia_X1,iq) * d(ia,iq_l1') * d(ia_l1',iq_X2)
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str2 = d(Ia_X1,Iq_l1) * d(Ia_l1,Iq_X2) *
        !        d(Ia_X3,Iq_l2) * R2
        !
        ! str2' =
        ! str2 * str0_q(l2,l2',ia,iq) *
        !        d(Iq_l1',Iq_l1) * d(Ia_l1',Ia_l1)
        ! =
        ! R2 * d(Ia_X1,Iq_l1') * d(Ia_l1',Iq_X2) * (
        ! - d(Ia,Iq_l2') * d(Ia_X3,Iq)
        ! + 1/Nc * d(Ia,Iq) * d(Ia_X3,Iq_l2')
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1' * str2' *
        ! d(iq_l1',Iq_l1') * d(ia_l1',Ia_l1') * d(iq_l2',Iq_l2') *
        ! d(iq,Iq) * d(ia,Ia)
        ! =
        ! - R1 * d(ia_X1,Ia_X1) * d(ia_X3,iq_X2) * d(Ia_X3,Iq_X2) * R2
        ! + R1 * d(ia_X1,Ia_X3) * d(iq_X2,Iq_X2) * d(ia_X3,Ia_X1) * R2
        ! =
        ! ...
        ! =
        ! d(iq_l1,Iq_l1) * d(ia_l1,Ia_l1) * d(iq_l2,Iq_l2) * (
        ! + d(ia_X1,iq_l1) * d(ia_l1,iq_X2) * d(ia_X3,iq_l2) * R1 *
        !   d(Ia_X3,Iq_l1) * d(Ia_l1,Iq_X2) * d(Ia_X1,Iq_l2) * R2
        ! - 1/Nc *
        !   d(ia_X1,iq_l1) * d(ia_X3,iq_X2) * d(ia_l1,iq_l2) * R1 *
        !   d(Ia_X1,Iq_l1) * d(Ia_X3,Iq_X2) * d(Ia_l1,Iq_l2) * R2
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        else if (ck1.eq.'g'.and.ck2.eq.'q') then ! g - q
          ! Number of contributions
          nMax = 2
          allocate (ia1(dMax,nMax),ia2(dMax,nMax))
          do n = 1,nMax
             ia1(:,n) = ia(:,i1)
             ia2(:,n) = ia(:,i2)
          enddo
          ! Overall factor
          facr = 1d0/2d0/Ca
          ! Contribution 1:
          ! str1' = str1
          ! str2' = str2 [Ia_X1 <-> Ia_X3]
          ia2(diq(l1),1) = ia(diq(l2),i2)
          ia2(diq(l2),1) = ia(diq(l1),i2)
          corfac(1) = facr
          ! Contribution 2:
          ! str1' = str1 [ia_X3 <-> ia_l1]
          ! str2' = str2 [Ia_X3 <-> Ia_l1]
          ia1(diq(l2),2) = l1; ia1(dia(l1,i1),2) = ia(diq(l2),i1)
          ia2(diq(l2),2) = l1; ia2(dia(l1,i2),2) = ia(diq(l2),i2)
          corfac(2) = - 1d0/Nc * facr
          if (ia(diq(l2),i1).eq.l1) corfac(2) = corfac(2) * Nc
          if (ia(diq(l2),i2).eq.l1) corfac(2) = corfac(2) * Nc

        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! g - q~
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1 = d(ia_X1,iq_l1) * d(ia_l1,iq_X2) *
        !        d(ia_l2,iq_X3) * R1
        !
        ! str1' =
        ! str1 * str0_g(l1,l1',ia,iq) * d(ia_l2',ia_l2)
        ! =
        ! R1 * d(ia_l2',iq_X3) * (
        ! + d(ia_l1',iq) * d(ia_X1,iq_l1') * d(ia,iq_X2)
        ! - d(ia_X1,iq) * d(ia,iq_l1') * d(ia_l1',iq_X2)
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str2 = d(Ia_X1,Iq_l1) * d(Ia_l1,Iq_X2) *
        !        d(Ia_l2,Iq_X3) * R2
        !
        ! str2' =
        ! str2 * str0_q~(l2,l2',ia,iq) *
        !        d(Iq_l1',Iq_l1) * d(Ia_l1',Ia_l1)
        ! =
        ! R2 * d(Ia_X1,Iq_l1') * d(Ia_l1',Iq_X2) * (
        ! + d(Ia,Iq_X3) * d(Ia_l2',Iq)
        ! - 1/Nc * d(Ia,Iq) * d(Ia_l2',Iq_X3)
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1' * str2' *
        ! d(iq_l1',Iq_l1') * d(ia_l1',Ia_l1') * d(ia_l2',Ia_l2') *
        ! d(iq,Iq) * d(ia,Ia)
        ! =
        ! + R1 * d(ia_X1,Ia_X1) * d(iq_X2,Iq_X3) * d(iq_X3,Iq_X2) * R2
        ! - R1 * d(ia_X1,iq_X3) * d(iq_X2,Iq_X2) * d(Ia_X1,Iq_X3) * R2
        ! =
        ! ...
        ! =
        ! d(iq_l1,Iq_l1) * d(ia_l1,Ia_l1) * d(ia_l2,Ia_l2) * (
        ! + d(ia_X1,iq_l1) * d(ia_l1,iq_X2) * d(ia_l2,iq_X3) * R1
        !   d(Ia_X1,Iq_l1) * d(Ia_l2,Iq_X2) * d(Ia_l1,Iq_X3) * R2
        ! - 1/Nc *
        !   d(ia_l2,iq_l1) * d(ia_l1,iq_X2) * d(ia_X1,iq_X3) * R1 *
        !   d(Ia_l2,Iq_l1) * d(Ia_l1,Iq_X2) * d(Ia_X1,Iq_X3) * R2
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        else if (ck1.eq.'g'.and.ck2.eq.'q~') then ! g - q~
          ! Number of contributions
          nMax = 2
          allocate (ia1(dMax,nMax),ia2(dMax,nMax))
          do n = 1,nMax
             ia1(:,n) = ia(:,i1)
             ia2(:,n) = ia(:,i2)
          enddo
          ! Overall factor
          facr = 1d0/2d0/Ca
          ! Contribution 1:
          ! str1' = str1
          ! str2' = str2 [Ia_l1 <-> Ia_l2]
          ia2(dia(l1,i2),1) = ia(dia(l2,i2),i2)
          ia2(dia(l2,i2),1) = ia(dia(l1,i2),i2)
          corfac(1) = facr
          ! Contribution 2:
          ! str1' = str1 [ia_X1 <-> ia_l2]
          ! str2' = str2 [Ia_X1 <-> Ia_l2]
          ia1(diq(l1),2) = l2; ia1(dia(l2,i1),2) = ia(diq(l1),i1)
          ia2(diq(l1),2) = l2; ia2(dia(l2,i2),2) = ia(diq(l1),i2)
          corfac(2) = - 1d0/Nc * facr
          if (ia(diq(l1),i1).eq.l2) corfac(2) = corfac(2) * Nc
          if (ia(diq(l1),i2).eq.l2) corfac(2) = corfac(2) * Nc

        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! g - g
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1 = d(ia_X1,iq_l1) * d(ia_l1,iq_X2) *
        !        d(ia_X3,iq_l2) * d(ia_l2,iq_X4) * R1
        !
        ! str1' =
        ! str1 * str0_g(l1,l1',ia,iq) *
        !        d(iq_l2',iq_l2) * d(ia_l2',ia_l2)
        ! =
        ! R1 * d(ia_X3,iq_l2') * d(ia_l2',iq_X4) * (
        ! + d(ia_l1',iq) * d(ia_X1,iq_l1') * d(ia,iq_X2)
        ! - d(ia_X1,iq) * d(ia,iq_l1') * d(ia_l1',iq_X2)
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str2 = d(Ia_X1,Iq_l1) * d(Ia_l1,Iq_X2) *
        !        d(Ia_X3,Iq_l2) * d(Ia_l2,Iq_X4) * R2
        !
        ! str2' =
        ! str2 * str0_g(l2,l2',ia,iq) *
        !        d(Iq_l1',Iq_l1) * d(Ia_l1',Ia_l1)
        ! =
        ! R2 * d(Ia_X1,Iq_l1') * d(Ia_l1',Iq_X2) * (
        ! + d(Ia_l2',Iq) * d(Ia_X3,Iq_l2') * d(Ia,Iq_X4)
        ! - d(Ia_X3,Iq) * d(Ia,Iq_l2') * d(Ia_l2',Iq_X4)
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ! str1' * str2' *
        ! d(iq_l1',Iq_l1') * d(ia_l1',Ia_l1') *
        ! d(iq_l2',Iq_l2') d(ia_l2',Ia_l2') *
        ! d(iq,Iq) * d(ia,Ia)
        ! =
        ! + R1 * d(ia_X1,Ia_X1) * d(iq_X2,Iq_X4) *
        !   d(ia_X3,Ia_X3) * d(iq_X4,Iq_X2) * R2
        ! - R1 * d(ia_X1,Ia_X1) * d(ia_X3,iq_X2) *
        !   d(Ia_X3,Iq_X2) * d(iq_X4,Iq_X4) * R2
        ! - R1 * d(ia_X1,iq_X4) * d(iq_X2,Iq_X2) *
        !   d(ia_X3,Ia_X3) * d(Ia_X1,Iq_X4) * R2
        ! + R1 * d(ia_X1,Ia_X3) * d(iq_X2,Iq_X2) *
        !   d(ia_X3,Ia_X1) * d(iq_X4,Iq_X4) * R2
        ! =
        ! ...
        ! =
        ! d(iq_l1,Iq_l1) * d(ia_l1,Ia_l1) *
        ! d(iq_l2,Iq_l2) * d(ia_l2,Ia_l2) * (
        ! + d(ia_X1,iq_l1) * d(ia_l1,iq_X2) *
        !   d(ia_X3,iq_l2) * d(ia_l2,iq_X4) * R1 *
        !   d(Ia_X1,Iq_l1) * d(Ia_l2,Iq_X2) *
        !   d(Ia_X3,Iq_l2) * d(Ia_l1,Iq_X4) * R2
        ! + d(ia_X1,iq_l1) * d(ia_l1,iq_X2) *
        !   d(ia_X3,iq_l2) * d(ia_l2,iq_X4) * R1 *
        !   d(Ia_X3,Iq_l1) * d(Ia_l1,Iq_X2) *
        !   d(Ia_X1,Iq_l2) * d(Ia_l2,Iq_X4) * R2
        ! - 1/Nc *
        !   d(ia_X1,iq_l1) * d(ia_X3,iq_X2) *
        !   d(ia_l1,iq_l2) * d(ia_l2,iq_X4) * R1 *
        !   d(Ia_X1,Iq_l1) * d(Ia_X3,Iq_X2) *
        !   d(Ia_l1,Iq_l2) * d(Ia_l2,Iq_X4) * R2
        ! - 1/Nc *
        !   d(ia_l2,iq_l1) * d(ia_l1,iq_X2) *
        !   d(ia_X3,iq_l2) * d(ia_X1,iq_X4) * R1 *
        !   d(Ia_l2,Iq_l1) * d(Ia_l1,Iq_X2) *
        !   d(Ia_X3,Iq_l2) * d(Ia_X1,Iq_X4) * R2
        ! )
        !- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        else if (ck1.eq.'g'.and.ck2.eq.'g') then ! g - g
          ! Number of contributions
          nMax = 4
          allocate (ia1(dMax,nMax),ia2(dMax,nMax))
          do n = 1,nMax
             ia1(:,n) = ia(:,i1)
             ia2(:,n) = ia(:,i2)
          enddo
          ! Overall factor
          facr = 1d0/2d0/Ca
          ! Contribution 1:
          ! str1' = str1
          ! str2' = str2 [Ia_l1 <-> Ia_l2]
          ia2(dia(l1,i2),1) = ia(dia(l2,i2),i2)
          ia2(dia(l2,i2),1) = ia(dia(l1,i2),i2)
          corfac(1) = facr
          ! Contribution 2:
          ! str1' = str1
          ! str2' = str2 [Ia_X1 <-> Ia_X3]
          ia2(diq(l1),   2) = ia(diq(l2),i2)
          ia2(diq(l2),   2) = ia(diq(l1),i2)
          corfac(2) = facr
          ! Contribution 3:
          ! str1' = str1 [ia_l1 <-> ia_X3]
          ! str2' = str2 [Ia_l1 <-> Ia_X3]
          ia1(dia(l1,i1),3) = ia(diq(l2),i1)
          ia1(diq(l2),   3) = l1
          ia2(dia(l1,i2),3) = ia(diq(l2),i2)
          ia2(diq(l2),   3) = l1
          corfac(3) = - 1d0/Nc * facr
          if (ia(diq(l2),i1).eq.l1) corfac(3) = corfac(3) * Nc
          if (ia(diq(l2),i2).eq.l1) corfac(3) = corfac(3) * Nc
          ! Contribution 4:
          ! str1' = str1 [ia_X1 <-> ia_l2]
          ! str2' = str2 [Ia_X1 <-> Ia_l2]
          ia1(diq(l1)   ,4) = l2
          ia1(dia(l2,i1),4) = ia(diq(l1),i1)
          ia2(diq(l1)   ,4) = l2
          ia2(dia(l2,i2),4) = ia(diq(l1),i2)
          corfac(4) = - 1d0/Nc * facr
          if (ia(diq(l1),i1).eq.l2) corfac(4) = corfac(4) * Nc
          if (ia(diq(l1),i2).eq.l2) corfac(4) = corfac(4) * Nc
        end if

        do n = 1,nMax
          eq = count_cycles(n,legs)
          prs(pr)%colcoefc(i1,i2,l1,l2) = &
          prs(pr)%colcoefc(i1,i2,l1,l2) + corfac(n) * Nc**eq
        enddo

        deallocate (ia1,ia2)
        if (i1 .ne. i2) then
          prs(pr)%colcoefc(i2,i1,l1,l2) = prs(pr)%colcoefc(i1,i2,l1,l2)
        end if
      end do
      end do
    end do
    end do
    deallocate (iq,ia,diq,dia,iabin,vec)
  end subroutine compute_colourcoefc

  function count_cycles(n,legs) result(cycles)
    ! Computes the number of cycles in the arrays ia1 and ia2.
    ! For example
    ! ia1 = [2, 3, 1, 4]
    ! ia2 = [1, 2, 3, 4]
    ! has 2 cycles, namely 2->3->1->2 and 4->4.
    integer, intent(in) :: n,legs
    integer             :: cycles,d,dtmp,covered,ia1tmp,ia1tmp2,dia2(legs)

    ! invert ia2
    do d = 1, dMax
      dia2(ia2(d,n)) = d
    end do

    cycles = 0
    covered = 0

    ! loop over ia1
    dloop: do d = 1, dMax
      ia1tmp = ia1(d,n)
      dtmp = dia2(ia1tmp)

      ! have we counted this cycle already?
      if (iand(pow2(dtmp-1),covered) .ne. 0) then
        cycle dloop
      end if

      ! follow the cycle until closed
      ia1tmp2 = ia1(dtmp,n)
      do while (ia1tmp2 .ne. ia1tmp)
        dtmp = dia2(ia1tmp2)
        ia1tmp2 = ia1(dtmp,n)
        covered = ior(covered,pow2(dtmp-1))
      end do
      covered = ior(covered,pow2(d-1))
      cycles = cycles + 1
    end do dloop

  end function count_cycles

!------------------------------------------------------------------------------!

end module colourflow_rcl

!------------------------------------------------------------------------------!
