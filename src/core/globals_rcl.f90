!******************************************************************************!
!                                                                              !
!    globals_rcl.f90                                                           !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module globals_rcl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  character(len=9) :: version_rcl = "2.2.1"

  integer,      parameter :: &
    sp = kind (10e0),        &
    dp = kind (23d0)

  real (dp),    parameter :: &
    nul   = 0d0,             &
    one   = 1d0,             &
    two   = 2d0,             &
    three = 3d0

  complex (dp), parameter :: &
    cnul = (0d0,0d0),        &
    cone = (1d0,0d0),        &
    ctwo = (2d0,0d0),        &
    cima = (0d0,1d0)

  real (dp),    parameter ::            &
    sq2 = 1.414213562373095048801689d0, &
    pi  = 3.141592653589793238462643d0

  complex (dp), parameter ::                   &
    csq2 = (1.414213562373095048801689d0,0d0), &
    iover32pi2 = cima/(32d0*pi*pi)

  integer,  parameter :: nCs =  3  ! number of colors
  real(dp), parameter :: Nc = real(nCs,kind=dp)
  real(dp), parameter :: Nc2 = Nc**2

  ! cut on vanishing dp
  real(dp), parameter :: zerocut = 1d-15

  ! zero for check of phase-space point
  real(dp), parameter :: zerocheck = 1d-7

  integer, parameter :: TreeBranch=0, LoopBranch=1, CTBranch=2, R2Branch=3

! selfenergy topology disabled by default
  logical :: compute_selfenergy = .false.

! tadpole topology disabled by default
  logical :: compute_tadpole = .false.

! Select only the vertex functions (1PI) for a given process
  logical :: vertex_functions = .false.

! the algorithm reorders the particles order given in the process string
! according to color -> optimizes the evaluation of the matrix element
  logical :: particle_ordering = .true.

! Fermion loops optimization disabled by default.
  logical :: fermion_loop_opt = .true.

! Process generation optimization for SM-like theories
  logical :: sm_generation_opt =.true.

! Helicity optimization enabled by default.
  logical :: helicity_opt = .true.

! Force to compute the square of the loop amplitude A_1^2. False by default.
  logical :: compute_A12 = .false.

! relate processes with same external fields by crossing symmetry
  logical :: crossing_symmetry = .true.

! disable/enable matrix-element contributions
! lp=0 -> Born
! lp=1 -> Loop
! lp=2 -> Counterterms
! lp=3 -> Rational terms
  logical, dimension(0:3) :: lp_on = (/.true., .true., .true., .true./)

! Generate a FORM file (.frm) to reconstruct amplitudes analytically
! 0 -> the .frm file is not generated
! 1 -> the .frm file for the complete amplitude is generated
! 2 -> the .frm file to compute the counterterms is generated
! 3 -> the .frm file to compute the R2-terms is generated
! 4 -> the .frm file for the complete amplitude, but without polarization
! vectors
  integer :: frm = 0

  ! if couplings_active_frm is .true. then the form expression of a current is
  ! generated even if the coupling is numerically below the zerocut limit
  logical :: couplings_active_frm = .false.

  ! same as couplings_active_frm, but for different skeleton branches
  logical :: tree_couplings_active_frm = .false.
  logical :: loop_couplings_active_frm = .false.
  logical :: ct_couplings_active_frm = .false.
  logical :: r2_couplings_active_frm = .false.

!==============================================================================!
!                                   Warnings                                   !
!==============================================================================!

  integer            :: warnings      = 0
  integer, parameter :: warning_limit = 100

! The ifail flag regulates the behaviour of Recola in case of error.
! On input (set by the user calling set_ifail_rcl):
! ifail =  0 -> the code does not stop, when an error occurs
! ifail = -1 -> the code stops, when an error occurs
! On output (which can be got by the user calling get_ifail_rcl):
! ifail = 0 -> no errors occured
! ifail = 1 -> an error occured because of a wrong call of user's
!              subroutines
! ifail = 2 -> an error occured because of an internal bug of Recola
  integer :: ifail = -1

! The iwarn flag regulates the behaviour of Recola in case of error.
! On input (set by the user calling set_iwarn_rcl):
! iwarn =  0 -> the code does not stop, when a warning occurs
! iwarn = -1 -> the code stops, when a warning occurs
! On output (which can be got by the user calling get_iwarn_rcl):
! iwarn = 0 -> no warnings occured
! iwarn = 1 -> a warning occured because of a wrong call of user's
!              subroutines
  integer :: iwarn = 0

!==============================================================================!
!                     Variables for output file output.rcl                     !
!==============================================================================!

  integer, parameter :: nOpenedDef = 999 ! maximal number of output files

  character(99) :: outputfile = 'output.rcl'
  character(99) :: nameOpened(nOpenedDef) = ''
  integer       :: nx = 934758
  integer       :: nOpened = 0

!==============================================================================!
! Variable for the output directory of collier                                 !
! collier_output_dir = 'default': The output directory of collier will be the  !
!                                 default of collier                           !
! collier_output_dir = 'dir': The output directory of collier will be 'dir',   !
!                              where 'dir' can be a relative or absolute path. !
!==============================================================================!

  character(999) :: collier_output_dir = 'default'

!==============================================================================!
!                                  Parameters                                  !
!==============================================================================!


  logical :: processes_generated = .false., &
             changed_DeltaUV     = .false., &
             changed_muUV        = .false., &
             changed_DeltaIR     = .false., &
             changed_muIR        = .false.

! Flags controlling what is printed on the screen
  logical :: print_recola_logo = .true.
  logical :: print_model_external_parameters = .true.
  logical :: print_model_internal_parameters = .false.
  logical :: print_model_counterterms = .false.
  logical :: print_process_generation_summary = .true.

! Switch for IR-resonances. For processes with resonances:
! if resIR=F, only normal resonant contributions are selected
! if resIR=T, also IR-resonant contributions are selected.
  logical :: resIR = .false.

! Self-energies of resonant particles
  logical :: resSE = .true.

! Dynamic settings
  integer :: dynamic_settings = 0
! parameters cached and set after process generation
  real(dp) :: DeltaUV_cache, DeltaIR_cache, DeltaIR2_cache, &
              muUV_cache, muIR_cache

! Switch whether recola is allowed to modify momenta in order to respect
! momentum-and energy-conservation
  logical :: momenta_correction = .true.

! disable all momenta checks flag, don't use this
  logical :: momenta_checks = .true.

! stop and print error if a process does not exist
  logical :: stop_on_nonexisting_process = .false.

! Allows computing the selfenergy at off-shell values of p^2
  logical :: selfenergy_offshell_setup = .false.

! logs memory used at several stages in the generation process
  logical :: log_mem = .false.

! cutparticle_selection selects a certain cut particle in the loop
! if cutparticle_selection = -1 then no cut particle is selected
  integer :: cutparticle_selection = -1

! Init collier after generation process
  logical :: init_collier = .true.

! Init collier after generation process
! 1 -> COLI
! 2 -> DD
! 3 -> COLI+DD
  integer :: collier_mode = 1

! use tensor teduction
  logical :: collier_tensor_reduction = .true.

! compute IR poles
! 0 -> No IR poles computed
! 1 -> IR poles computed via I-dip (not implemented yet)
! 2 -> IR poles computed via rescaling (3-times)
  integer :: compute_ir_poles = 0

! Sets the cache mode which dictates how to use collier
! 0 -> Cache is entirely disabled for all processes
! 1 -> local cache for each process.
! 2 -> Global cache for all processes.
! See `set_cache_mode_rcl` for further informations.
  integer :: cache_mode = 1

! Optimization level for color algebra
! 0 -> no optimization
! 1 -> optimization of currents just differing by the color structure
! 2 -> optimization of external U(1)-gluons and of currents just
!      differing by the color structure
  integer :: colour_optimization = 2

! Print matrix elements (writeMat) and matrix elements
! squared (writeMat2)
! 0 -> nothing is written
! 1 -> just LO and NLO
! 2 -> LO, bare-loop, counterterms, rational part and complete NLO
  integer :: writeMat  = 0
  integer :: writeMat2 = 0

! Print matrix elements squared with color correlation (if computed)
  integer :: writeCor  = 0

! Generate a .tex file to draw the branches of the processes
! 0 -> the .tex file is not generated
! 1 -> the .tex file is generated without color structures and
!      without a legend
! 2 -> the .tex file is generated with color structures, but without
!      a legend
! 3 -> the .tex file is generated with color structures and with a
!      legend
  integer :: draw = 0

! Set true if currents should be computed inside recola instead of the model
! files. For non-existing recola rules, the model file variant is used.
  logical :: use_recola_base = .true.

!==============================================================================!
!                              Process variables                               !
!==============================================================================!

! number of processes
  integer :: prTot = 0
  integer :: prTotNew = 0

  type process_def
    character :: process*99                ! Process string definition
    integer   :: inpr                      ! Internal process number
    integer   :: crosspr                   ! Crossed process number
    logical   :: loop                      ! loop process
    logical   :: exists                    ! process exists (determined after generation)
    integer   :: legs, legsIn, legsOut     ! Number of legs
    integer, allocatable :: par(:)         ! Particle ids
    integer, allocatable :: hel(:)         ! Helicity specification
    integer              :: resMax         ! Number of resonances
    integer, allocatable :: binRes(:)      ! Resonances (propagator ids)
    integer, allocatable :: parRes(:)      ! Resonances (particle ids)
    integer, allocatable :: qflow(:)       ! Quark-flow restrictions
    integer, allocatable :: powsel(:,:,:)  ! Coupling power selections
    integer, allocatable :: relperm(:)     ! Relative permutation
    integer, allocatable :: colcoef(:,:)
    real(dp), allocatable :: colcoefc(:,:,:,:)
  end type process_def

  type(process_def), allocatable :: prs(:)

! all-process variables
  integer :: legsMax,cfMax,csMax, gsMax, gs2Max
  integer :: legsMaxCl = 0
  logical :: loopMax = .false.

  integer :: n_particles=-1, n_orders=-1, n_masses=-1

! Set the polarization vector of vector bosons to be longitudinal,
! in order to check Ward identities
! longitudinal =   0 -> no vector bosons are longitudinal; this is
!                       the default
! longitudinal = 'i' -> if particle 'i' is a vector boson, its
!                       polarization vector is set equal to its
!                       momentum
! longitudinal = 111 -> For all vector bosons the polarization
!                       vectors are set equal to their momentum
  integer :: longitudinal = 0

! set whether the longitudinal polarization is used exclusively in the loop
! amplitude
  logical :: longitudinal_nlo = .true.

!hard-coded dressing functions

  integer, parameter :: &
    VVV_bfm = 160

  integer, parameter :: &
    VVS_heft = 97, &
    VSV_heft = 98, &
    SVV_heft = 99

  integer, parameter :: &
    VVSS_heft = 100, &
    VSVS_heft = 101, &
    VSSV_heft = 102, &
    SVVS_heft = 103, &
    SVSV_heft = 104, &
    SSVV_heft = 105

  integer, parameter :: &
    VVVVS_heft = 110, &
    VVVSV_heft = 111, &
    VVSVV_heft = 112, &
    VSVVV_heft = 113, &
    SVVVV_heft = 114

  integer, parameter :: &
    VVVVSS_heft = 120, &
    VVVSVS_heft = 121, &
    VVVSSV_heft = 122, &
    VVSVVS_heft = 123, &
    VVSVSV_heft = 124, &
    VVSSVV_heft = 125, &
    VSVVVS_heft = 126, &
    VSVVSV_heft = 127, &
    VSVSVV_heft = 128, &
    VSSVVV_heft = 129, &
    SVVVVS_heft = 130, &
    SVVVSV_heft = 131, &
    SVVSVV_heft = 132, &
    SVSVVV_heft = 133, &
    SSVVVV_heft = 134

  integer, parameter :: &
    VVVS_heft = 140, &
    VVSV_heft = 141, &
    VSVV_heft = 142, &
    SVVV_heft = 143

  integer, parameter :: &
    VVVSS_heft = 150, &
    VVSVS_heft = 151, &
    VSVVS_heft = 152, &
    VVSSV_heft = 153, &
    VSVSV_heft = 154, &
    VSSVV_heft = 155, &
    SVVVS_heft = 156, &
    SVVSV_heft = 157, &
    SVSVV_heft = 158, &
    SSVVV_heft = 159

!==============================================================================!
!                             Generation variables                             !
!==============================================================================!

  integer, allocatable :: minlmu(:,:), maxlmu(:,:)
  integer, allocatable :: fhmin(:), fhmax(:)

  integer, allocatable :: newleg(:,:),oldleg(:,:),newbin(:,:),oldbin(:,:),         &
                          cfTot(:),csTot(:),pCsTot(:),csAmp(:,:,:),csIq(:,:,:),    &
                          nAmp(:,:), pAmp(:,:,:),w0eTot(:),he(:,:,:),lpmax(:),     &
                          dualconf(:,:,:),cd0sMax(:),loopCoef(:,:),w0Tot(:),       &
                          w1TotMax(:,:),w1Tot(:),w0last(:,:),parw0e(:,:),          &
                          binw0e(:,:),legw0e(:,:),helw0e(:,:),mosm0(:,:),          &
                          binsm0(:,:,:),parsm0(:,:),gsIncsm0(:,:,:),gssm0(:,:),    &
                          cssm0(:,:),dasd0(:,:),gssd0(:,:),cssd0(:,:), mosm1(:,:), &
                          binsm1(:,:,:),parsm1(:,:),gsIncsm1(:,:,:),cssm1(:,:),    &
                          rankInsm1(:,:),rankOutsm1(:,:),gssm1(:,:), ssm1(:,:),    &
                          tism1(:,:),dasd1(:,:), rankOutsd1(:,:),gssd1(:,:),       &
                          cssd1(:,:),tisd1(:,:),c0EffMax(:),c0TOlp(:,:),           &
                          bm0min(:,:,:,:),bm0max(:,:,:,:),bd0min(:,:,:,:),         &
                          bd0max(:,:,:,:),cEffMax(:),cTOt(:,:),cTOfh(:,:),         &
                          cTOih1(:,:),bm1min(:,:,:,:),bm1max(:,:,:,:),             &
                          bd1min(:,:,:,:),bd1max(:,:,:,:),                         &
                          modaTot(:),riwMax(:,:)

  ! tensor and collier cache variables
  integer, allocatable :: tiTot(:),ritiMax(:),legsti(:,:),momsti(:,:,:), &
                          vmti(:,:,:),rankti(:,:), &
                          nCache(:),nCacheTot(:),tiCache(:)
  integer :: nCacheGlobal,nprCall,activeCache
  type :: cacheCallSequence
    integer, allocatable :: hist(:)
    logical :: constructed
  end type cacheCallSequence
  type(cacheCallSequence), allocatable :: gcch(:)

  logical, allocatable :: defp2bin(:,:),defresbin(:,:),xsm0(:,:),&
                          ferloopsm1(:,:),ferloopsd1(:,:),       &
                          sesd0(:,:),zeroLO(:),cacheOn(:)

  complex (dp), allocatable :: cmONS2(:,:),cmREG2(:,:)
  real (dp), allocatable :: p2bin(:,:),pspbin(:,:),mONS(:,:),facsd0(:,:),  &
                            facsd1(:,:),facAmp(:,:,:),factor(:)

  ! dynamic length for couplings
  type :: iarr
    integer, allocatable     :: c(:)
  end type iarr

  type :: couplarr
    integer, allocatable     :: c(:)
    complex(dp)              :: factor
    complex(dp), allocatable :: val(:)
    complex(dp), allocatable :: valR(:)
    complex(dp)              :: m,m2
  end type couplarr

  type(couplarr), allocatable :: cosm0(:,:)
  type(couplarr), allocatable :: cosm1(:,:)

  type branch_bounds
    integer :: bmin
    integer :: bmax
  end type branch_bounds

  type bbranch
    type(branch_bounds), allocatable :: conf(:,:,:)
  end type bbranch

  type bbranch_m_mapping
    integer :: sb                   ! mother s-branch
    integer, allocatable :: w_in(:) ! incoming coefficent
    logical :: w_init               ! coefficient initialised
    integer :: w_out                ! outgoing coefficent
    integer :: ty                   ! lorentz structure
    integer :: tyR                  ! Recola1 lorentz structure
    integer :: pext
    integer :: perm
  end type bbranch_m_mapping

  type bbranch_d_mapping
    integer :: sb            ! daugher s-branch
    logical :: w_init        ! coefficient initialised
    integer :: w_out         ! outgoing coefficent
  end type bbranch_d_mapping

  type process_mappings
    type(bbranch_m_mapping), allocatable :: tree_m_maps(:)
    type(bbranch_d_mapping), allocatable :: tree_d_maps(:)
    type(bbranch_m_mapping), allocatable :: loop_m_maps(:)
    type(bbranch_d_mapping), allocatable :: loop_d_maps(:)
    type(branch_bounds), allocatable :: bm0(:,:,:)
    type(branch_bounds), allocatable :: bd0(:,:,:)
    type(branch_bounds), allocatable :: bm1(:,:,:)
    type(branch_bounds), allocatable :: bd1(:,:,:)
    integer :: bm0Tot,bd0Tot,bm1Tot,bd1Tot
  end type process_mappings

  type(process_mappings), allocatable :: pm(:)


!==============================================================================!
!                           QCD rescaling variables                            !
!==============================================================================!

  logical                   :: qcd_rescaling=.true., &
                               use_active_qmasses=.false.
  real(dp)                  :: als0,als,Qren0,Qren, &
                               beta0(6),beta1(6),rb1(6)
  real(dp), allocatable     :: als0R(:,:),Qren0R(:,:)
  complex(dp)               :: dZgs0,mq2(6)
  complex(dp), allocatable  :: dZgs0R(:)
  integer, allocatable      :: Nlq0R(:,:)
  integer                   :: Nfren,Nfren0,Nlq0,Nlq,qcd_order_id=-1

!==============================================================================!
!                          Time statistics variables                           !
!==============================================================================!

! time statistics for tensor integrals, tensor coefficients and helicity sum
  real(sp)              :: timeGEN
  real(sp), allocatable :: timeTI(:), timeTC(:), timeHS(:)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                   Utility                                   !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  interface to_str
    module procedure int_to_str
  end interface

!==============================================================================!
!                              Model file support                              !
!==============================================================================!

  interface check_support_models
    module procedure check_support_model,   &
                     check_support_models2, &
                     check_support_models3, &
                     check_support_models4, &
                     check_support_models5, &
                     check_support_models6
  end interface

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine openOutput
    ! Opens the output channel as provided by the user.
    ! All output is written to that channel..

    use modelfile, only: set_output_file_mdl

    integer      :: n,i
    logical      :: nxopen
    character(7) :: status

    if (print_recola_logo) then
      write(*,*)
      write(*,'(1x,75("x"))')
      write(*,'(24x,a)') ' _    _   _   _        _    _  '
      write(*,'(24x,a)') '| )  |_  |   | |  |   |_|   _| '
      write(*,'(24x,a)') '| \  |_  |_  |_|  |_  | |  |_  '
      write(*,*)
      write(*,'(16x,a)') 'REcursive Computation of One Loop Amplitudes'
      write(*,*)
      write(*,'(32x,a)') 'Version ' // trim(version_rcl)
      write(*,*)
      write(*,'(22x,a)') 'by A.Denner, J.-N.Lang, S.Uccirati'
      write(*,*)
      write(*,'(1x,75("x"))')
      write(*,*)
      if (trim(outputfile).ne.'*') then
        write(*,*)
        write(*,'(1x,75("x"))')
        write(*,*)
        write(*,*) 'From now on the output of RECOLA is written ', &
                   'to the file ',trim(outputfile)
        write(*,*)
        write(*,'(1x,75("x"))')
        write(*,*)
      endif
      print_recola_logo = .false.
    end if

    if (trim(outputfile).eq.'*') then
      nx = 6
    else
      inquire(file=trim(outputfile),number=n)
      if (n .eq. -1) then ! outputfile is not open
        status = 'replace'
        do i = 1,nOpened
          if (trim(nameOpened(i)) .eq. trim(outputfile)) then
            status='unknown'; exit
          endif
        enddo

        if (status.eq.'replace') then
          ! "outputfile" is open for the first time
          if (nOpened.ge.nOpenedDef) then
            call error_rcl('Too many output-files. ' // &
                           'Increse nOpenedDef in file globals_rcl.f90')
            stop
          else
            nOpened = nOpened + 1
            nameOpened(nOpened) = trim(outputfile)
          endif
        endif

        if (nx .eq. 6) nx = 934758

        inquire(nx,opened=nxopen)
        do while (nxopen)
          nx = nx + 1
          inquire(nx,opened=nxopen)
        enddo

        open (unit=nx,file=trim(outputfile),      &
              status=trim(status),position='append')

      else              ! outputfile is open
        nx = n
      endif
    endif
    call set_output_file_mdl(nx)

  end subroutine openOutput

!------------------------------------------------------------------------------!

  subroutine error_rcl(errmsg,where)
    ! Prints the error `errmsg' on the screen and increases the number of
    ! warnings. The program stops depending on the value of ifail. Optionally,
    ! `where' can be passed to indicate in which subroutine/module the error
    ! occured.
    character(len=*), intent(in) :: errmsg
    character(len=*), optional, intent(in) :: where
    warnings = warnings + 1
    if (warnings .le. warning_limit+1) then
      call openOutput
      write(nx,*)
      if (present(where)) then
        write(nx,*) 'ERROR in ' //where//':'
        write(nx,*) errmsg
      else
        write(nx,*) 'ERROR: ', errmsg
      end if
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  end subroutine error_rcl

!------------------------------------------------------------------------------!

  subroutine warning_rcl(warnmsg,where)
    ! Prints the warning `warnmsg' on the screen and increases the number of
    ! warnings. The program does not stop. Optionally, `where' can be passed to
    ! indicate in which subroutine/module the warning occured.
    character(len=*), intent(in) :: warnmsg
    character(len=*), optional, intent(in) :: where
    warnings = warnings + 1
    if (warnings .le. warning_limit+1) then
      call openOutput
      write(nx,*)
      if (present(where)) then
        write(nx,*) 'WARNING in ' //where//': '
        write(nx,*) warnmsg
      else
        write(nx,*) 'WARNING: ', warnmsg
      endif
      write(nx,*)
      call toomanywarnings
    endif
    call istop (iwarn,1)
  end subroutine warning_rcl

!------------------------------------------------------------------------------!

  subroutine process_exists_error_rcl(pr,where)
    ! Prints the error that the process does not exist.
    ! 'where' has to be passed to
    ! indicate in which subroutine/module the error occured.
    integer,          intent(in) :: pr
    character(len=*), intent(in) :: where
    integer :: crosspr

    if (prs(pr)%crosspr .ne. 0) then
      crosspr = prs(pr)%crosspr
    else
      crosspr = pr
    end if

    if (.not. prs(crosspr)%exists) then
      call error_rcl('process '//to_str(prs(pr)%inpr)//' does not exist.', &
                     where=where)
    end if
  end subroutine process_exists_error_rcl

!------------------------------------------------------------------------------!

  subroutine processes_generated_warning_rcl(where)
    ! Prints the warning that the processes are already generated after the call
    !` where' has to be passed to
    ! indicate in which subroutine/module the warning occured.
    character(len=*), intent(in) :: where
    if (processes_generated) then
      call warning_rcl('Processes already generated. Call has no effect.', &
                       where=where)
    endif
  end subroutine processes_generated_warning_rcl

!------------------------------------------------------------------------------!

  subroutine processes_generated_error_rcl(where)
    ! Prints the error that the processes are already generated after the call
    !` where' has to be passed to
    ! indicate in which subroutine/module the error occured.
    character(len=*), intent(in) :: where
    if (processes_generated) then
      call error_rcl('Processes already generated. Call is not allowed.', &
                     where=where)
    endif
  end subroutine processes_generated_error_rcl

!------------------------------------------------------------------------------!

  subroutine processes_not_generated_error_rcl(where)
    ! Prints the error that the processes are already generated after the call
    !` where' has to be passed to
    ! indicate in which subroutine/module the error occured.
    character(len=*), intent(in) :: where
    if (.not. processes_generated) then
      call error_rcl('Processes not generated. Call is not allowed.', &
                     where=where)
    endif
  end subroutine processes_not_generated_error_rcl

!------------------------------------------------------------------------------!

  subroutine deprecated_warning_rcl(where)
    ! Prints the warning that the method called is depcreated.
    character(len=*), intent(in) :: where
    call warning_rcl('Method is deprecated. Call has no effect.', &
                     where=where)
  end subroutine deprecated_warning_rcl

!------------------------------------------------------------------------------!

  subroutine no_support_warning_rcl(where)
    ! Prints the warning that the method called is not supported yet.
    character(len=*), intent(in) :: where
    call warning_rcl('Method is not supported yet. Call has no effect.', &
                     where=where)
  end subroutine no_support_warning_rcl

!------------------------------------------------------------------------------!

  subroutine order_error_rcl(order,where)
    character(len=*), intent(in) :: order,where

    if (order .ne. 'LO' .and. order .ne. 'NLO') then
      call error_rcl('Wrong order. '// &
                     "Accepted values are: order = 'LO','NLO'", &
                     where=where)
    end if
  end subroutine order_error_rcl

!------------------------------------------------------------------------------!

  subroutine toomanywarnings

    if (warnings.eq.warning_limit+1) then
      call openOutput
      write(nx,*)
      write(nx,*) ' Too many WARNINGs for this run:'
      write(nx,*) ' Further WARNINGs will not be printed'
      write(nx,*)
    endif

  end subroutine toomanywarnings

!------------------------------------------------------------------------------!

  subroutine istop (i,n)

    integer, intent(inout) :: i
    integer, intent(in)    :: n

    select case (i)
      case (-1);    i = n; stop 9
      case (0);     i = n
      case default; i = n
    end select

  end subroutine istop

!------------------------------------------------------------------------------!

  subroutine get_pr(npr,funcname,pr)
    ! returns the interal process id "pr" given the process id "npr" which is
    ! defined by the user in define_process_rcl. In case the "npr" is not
    ! associated to an internal process an exception is raised naming the
    ! original function ! "funcname" calling get_pr.
    integer, intent(in)          :: npr
    character(len=*), intent(in) :: funcname
    integer, intent(out)         :: pr
    integer                      :: i

    pr = 0
    do i = 1,prTot
      if (prs(i)%inpr .eq. npr) then
        pr = i; exit
      endif
    enddo
    if (pr .eq. 0) then
      call error_rcl('Undefined process index '//to_str(npr),&
                     where=funcname)
    endif
    call process_exists_error_rcl(pr,where=funcname)
  end subroutine get_pr

!------------------------------------------------------------------------------!

  subroutine get_ord(cid,funcname,ord)

    use modelfile, only: get_n_orders_mdl, get_order_id_mdl
    ! returns the order id "ord" given the fundamental order string "cid".  In
    ! case the order id is not found an exception is raised naming the original
    ! function "funcname" calling get_ord.
    character(len=*),  intent(in) :: cid,funcname
    integer,          intent(out) :: ord

    integer        :: nord,i
    character(100) :: order_ids

    ! check fundamental order
    nord = get_n_orders_mdl()
    ord = -1
    do i = 1, nord
      if (get_order_id_mdl(i) .eq. cid) then
        ord = i
        exit
      end if
    end do

    if (ord .eq. -1) then
      order_ids = ' '
      do i = 1, n_orders
        order_ids = trim(order_ids)//' '//trim(adjustl(get_order_id_mdl(i)))
      end do
      call error_rcl('Undefined order id. Allowed values ' //&
                     adjustl(order_ids), where=funcname)
    endif
  end subroutine get_ord

!------------------------------------------------------------------------------!

  subroutine get_recola_version_rcl(ret_version)
    character(len=10), intent(out) :: ret_version

    ret_version = version_rcl

  end subroutine get_recola_version_rcl

!------------------------------------------------------------------------------!

  subroutine get_modelname_rcl(ret_modelname)
    use modelfile, only: get_modelname_mdl
    character(len=100), intent(out) :: ret_modelname

    ret_modelname = get_modelname_mdl()

  end subroutine get_modelname_rcl

!------------------------------------------------------------------------------!

  subroutine get_modelgauge_rcl(ret_modelgauge)
    use modelfile, only: get_modelgauge_mdl
    character(len=100), intent(out) :: ret_modelgauge

    ret_modelgauge = get_modelgauge_mdl()

  end subroutine get_modelgauge_rcl

!------------------------------------------------------------------------------!

  subroutine get_driver_timestamp_rcl(ret_driver_timestamp)
    use modelfile, only: get_driver_timestamp_mdl
    character(len=200), intent(out) :: ret_driver_timestamp

    ret_driver_timestamp = get_driver_timestamp_mdl()

  end subroutine get_driver_timestamp_rcl

!------------------------------------------------------------------------------!

  subroutine model_not_supported(supported_models, where)
    character(len=*), intent(in) :: supported_models
    character(len=*), intent(in) :: where
    character(len=100)           :: active_modelname
    call get_modelname_rcl(active_modelname)
    call error_rcl('Model file ' // trim(adjustl(active_modelname)) // &
                   ' not supported. Supported models are: ' // &
                   supported_models // '.',where)
  end subroutine model_not_supported

!------------------------------------------------------------------------------!

  subroutine check_support_model(modelname,where)
    character(len=*), intent(in) :: modelname,where
    logical                      :: check

    check = support_model(modelname)
    if (.not. check) then
      call model_not_supported(modelname,where)
    end if
  end subroutine check_support_model

!------------------------------------------------------------------------------!

  subroutine check_support_models2(modelname1,modelname2,where)
    character(len=*), intent(in) :: modelname1,modelname2,where
    logical                      :: check
    check = support_model(modelname1) .or. &
            support_model(modelname2)
    if (.not. check) then
      call model_not_supported(modelname1//','//modelname2,where)
    end if
  end subroutine check_support_models2

!------------------------------------------------------------------------------!

  subroutine check_support_models3(modelname1,modelname2,modelname3,where)
    character(len=*), intent(in) :: modelname1,modelname2,modelname3,where
    logical                      :: check
    check = support_model(modelname1) .or. &
            support_model(modelname2) .or. &
            support_model(modelname3)
    if (.not. check) then
      call model_not_supported(modelname1//','//modelname2//','//modelname3,&
                               where)
    end if
  end subroutine check_support_models3

!------------------------------------------------------------------------------!

  subroutine check_support_models4(modelname1,modelname2,modelname3, &
                                   modelname4,where)
    character(len=*), intent(in) :: modelname1,modelname2,modelname3, &
                                    modelname4,where
    logical                      :: check
    check = support_model(modelname1) .or. &
            support_model(modelname2) .or. &
            support_model(modelname3) .or. &
            support_model(modelname4)

    if (.not. check) then
      call model_not_supported(modelname1//','//modelname2//','//modelname3//&
                               ','//modelname4, where)
    end if
  end subroutine check_support_models4

!------------------------------------------------------------------------------!

  subroutine check_support_models5(modelname1,modelname2,modelname3, &
                                   modelname4,modelname5,where)
    character(len=*), intent(in) :: modelname1,modelname2,modelname3, &
                                    modelname4,modelname5,where
    logical                      :: check
    check = support_model(modelname1) .or. &
            support_model(modelname2) .or. &
            support_model(modelname3) .or. &
            support_model(modelname4) .or. &
            support_model(modelname5)

    if (.not. check) then
      call model_not_supported(modelname1//','//modelname2//','//modelname3//&
                               ','//modelname4//','//modelname5, where)
    end if
  end subroutine check_support_models5

!------------------------------------------------------------------------------!

  subroutine check_support_models6(modelname1,modelname2,modelname3, &
                                   modelname4,modelname5,modelname6, &
                                   where)
    character(len=*), intent(in) :: modelname1,modelname2,modelname3, &
                                    modelname4,modelname5,modelname6, &
                                    where
    logical                      :: check
    check = support_model(modelname1) .or. &
            support_model(modelname2) .or. &
            support_model(modelname3) .or. &
            support_model(modelname4) .or. &
            support_model(modelname5) .or. &
            support_model(modelname6)

    if (.not. check) then
      call model_not_supported(modelname1//','//modelname2//','//modelname3//&
                               ','//modelname4//','//modelname5//','//modelname6,&
                               where)
    end if
  end subroutine check_support_models6

!------------------------------------------------------------------------------!

  function support_model (modelname)
    use modelfile, only: get_modelname_mdl
    character(len=*), intent(in) :: modelname
    character(len=100)           :: active_modelname
    logical                      :: support_model

    active_modelname = get_modelname_mdl()

    if (trim(adjustl(modelname)) .eq. trim(adjustl(active_modelname))) then
      support_model = .true.
    else
      support_model = .false.
    end if

  end function support_model

!------------------------------------------------------------------------------!

  function pow2(n)
    integer, intent(in) :: n
    integer             :: pow2
    pow2 = ishft(1,n)
  end function pow2

!------------------------------------------------------------------------------!

  recursive function value_in_array(val, array) result(in_array)
    ! returns true if val found in array
    integer, intent(in) :: val, array(:)
    logical             :: in_array
    if (size(array) .eq. 0) then
      in_array = .false.
    else
      if (val .eq. array(1)) then
        in_array = .true.
      else if (size(array) > 1) then
        in_array = value_in_array(val, array(2:))
      else
        in_array = .false.
      end if
    end if
  end function

!------------------------------------------------------------------------------!

  recursive function get_value_in_array(val, array, offset) result(index_array)
    ! returns index of first match
    integer, intent(in)           :: val, array(:)
    integer, intent(in), optional :: offset
    integer             :: index_array, offset2

    if (present(offset)) then
      offset2 = 1 + offset
    else
      offset2 = 1
    end if
    if (size(array) .eq. 0) then
      index_array = 0
    else
      if (val .eq. array(1)) then
        index_array = offset2
      else if (size(array) > 1) then
        index_array = get_value_in_array(val, array(2:), offset = offset2)
      else
        index_array = 0
      end if
    end if
  end function

!------------------------------------------------------------------------------!

  function int_to_str(i)
    integer, intent(in) :: i
    character(10)       :: int_to_str
    write(int_to_str,'(i10)') i
  end function int_to_str

!------------------------------------------------------------------------------!

    function nextp (n,a)
      logical :: nextp
      integer,intent(in) :: n
      integer,dimension(n),intent(inout) :: a
      integer i,j,k,t
      i = n-1
   10 if ( a(i) .lt. a(i+1) ) goto 20
      i = i-1
      if ( i .eq. 0 ) goto 20
      goto 10
   20 j = i+1
      k = n
   30 t = a(j)
      a(j) = a(k)
      a(k) = t
      j = j+1
      k = k-1
      if ( j .lt. k ) goto 30
      j = i
      if (j .ne. 0 ) goto 40
      nextp = .false.
      return
!
   40 j = j+1
      if ( a(j) .lt. a(i) ) goto 40
      t = a(i)
      a(i) = a(j)
      a(j) = t
      nextp = .true.
      return
      end function

!------------------------------------------------------------------------------!

end module globals_rcl

!------------------------------------------------------------------------------!
