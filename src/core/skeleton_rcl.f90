!******************************************************************************!
!                                                                              !
!    skeleton_rcl.f90                                                          !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module skeleton_rcl

!------------------------------------------------------------------------------!

  use globals_rcl
  use tables_rcl, only: levelLeg,vectorLeg,firstNumbers,firstGap,firstGaps

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  integer              :: bLDef,wLDef,bLInc,wLInc,bMaxT,wMaxT,nDef,nprop, &
                          maxp,maxl,qflowextT
  integer, allocatable :: parT(:),csT(:,:),binT(:),hosT(:,:),        &
                          hmaT(:),xxxT(:),wT(:,:),nMaxT(:),typeT(:), &
                          permT(:),cbtypeT(:),cbT(:),orderT(:,:),    &
                          orderIncT(:,:),colourfacT(:,:),            &
                          pp(:),hv(:),hb(:),hh(:),                   &
                          qflowT(:)

  logical, allocatable :: U1gT(:),noquarks(:),nogluons(:),noweaks(:)

  type(iarr), allocatable :: couT(:)

  type waveIn
    integer, allocatable, dimension(:) :: w
  end type

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine makeSkeleton (lp,legs,pr)
  use modelfile, only: is_model_init_mdl,is_model_filled_mdl, &
                       get_max_leg_multiplicity_mdl,          &
                       init_vertices_mdl,fill_vertices_mdl,check_vertices_mdl

  ! lp=0: tree
  ! lp=1: loop
  ! lp=2: tree for counterterms
  ! lp=3: tree for rational terms

  integer, intent (in)    :: lp,legs,pr

  integer                    :: el1,el2,eb,sumex,b,b0,legsE,ib, &
                                par(legs),check,w,xTmin,xTmax,x,i,j,ii
  integer, dimension (bLDef) :: colorBranch
  logical                    :: doubleCounting, last
  integer                    :: eN,n,l,nMax,s
  integer                    :: binOut
  integer, allocatable       :: ei(:),orderInc(:)


  logical                  :: selfenergyRes
  integer                  :: npole
  integer, dimension(legs) :: pbins,pe,ppar,gphOut
  integer                  :: mass(legs),ee(2)
  integer, allocatable     :: gph(:,:)
  logical, allocatable     :: xpOut(:),xp(:,:),xpIR(:), &
                              epOut(:),ep(:,:)

  integer, allocatable               :: wOutBin(:)
  integer                            :: nbranches
  integer, allocatable, dimension(:) :: select_branch

  ! Init couplings and vertices
  if ( .not. is_model_init_mdl()) then
    call init_vertices_mdl
  end if
  if ( .not. is_model_filled_mdl()) then
    call fill_vertices_mdl
    ! checks if the model dictionary is working as expected
    ! call check_vertices_mdl()
  end if


  nDef = get_max_leg_multiplicity_mdl()
  maxp = nDef-1
  allocate(pp(0:maxp-1),hv(0:maxp-1),hb(0:maxp-1),hh(0:maxp-1))

  if (lp .eq. LoopBranch) then
    el1   = pow2(legs)    ! binary number of the first  loop line
    el2   = pow2(legs+1)  !   "      "    "   "  second  "    "
    sumex = el1-1         ! sum of binary numbers of external lines
  endif

  if (lp .eq. LoopBranch) then
    legsE = legs + 2
  else
    legsE = legs
  endif
  maxl = legsE

  par(1:legs) = parT(1:legs)

  npole = prs(pr)%resMax
  do i = 1,npole
    pbins(i) = newbin(prs(pr)%binRes(i),pr)
    pe(i)    = min(pbins(i),pow2(legs)-1-pbins(i))
    ppar(i)  = prs(pr)%parRes(i)
  enddo
  if (npole.gt.0) then
    allocate (xpOut(npole)); xpOut = .false.
    allocate (xp(npole,wLDef)); xp = .false.
    allocate (epOut(npole)); epOut = .false.
    allocate (ep(npole,wLDef)); ep = .false.
    allocate (xpIR(npole))
    allocate (gph(legs,wLDef)); gph    = 0
    gphOut = 0
  endif

  b = 0

  w = legsE

  allocate(wOutBin(pow2(legsE-1)-1))

  ! To be sure that all incoming currents in a branch have already
  ! been computed, we make first a loop on eN, the binary of the
  ! outgoing leg, from the smallest to the biggest.
  ! This conventional ordering is explicity used in currents.f90.
  enloop: do eN= 3, pow2(legsE-1)-1

    if (.not. resIR) then
      eb = eN
      if (eb .ge. 2**legs) eb = eN - 2**legs
      eb = min(eb,2**legs-1-eb)
      do i = 1,npole
        n = 0
        do j = 1,legs
          n = n + vectorLeg(pe(i),j)*vectorLeg(eb,j)
        end do
        if ( n .gt. 0 .and. n .lt. levelLeg(min(eb,pe(i))) ) cycle enloop
      end do
    end if

    wOutBin(eN) = w + 1

    s = eN - 1
    l = levelLeg(eN)
    if (l.eq.1) cycle
    nMax = min(nDef,l+1)
    allocate (ei(nMax-1))
    ei = 0
    call branch_partition(eN-1,2,2)
    deallocate (ei)

    ! Special 2-leg branches for CT and R2 contributions
    if (lp.ge.2) then
      ! no 2-leg vertices on external legs
      if (l.eq.1.or.l.eq.legsE-1) cycle
      allocate (ei(1))
      ei(1) = eN
      call routern (ei)
      deallocate (ei)
    endif

  enddo enloop

  deallocate(pp,hv,hb,hh)

 if (compute_selfenergy .and. (lp .eq. 3 .or. lp .eq.2)) then
    wOutBin(1) = w + 1
    allocate (ei(1))
    ei(1) = 1
    call routerN (ei)
    deallocate (ei)
  end if

  deallocate(wOutBin)

  bMaxT = b

  wMaxT = w

  contains

!------------------------------------------------------------------------------!

  recursive subroutine branch_partition(up,lo,np)
  integer, intent(in) :: up,lo,np
  integer             :: ee

  eeloop: do ee = up,lo,-1

    ei(np-1) = ee


    ! TODO:  <26-06-17, Jean-Nicolas Lang> !
    !! Check if at enough bits are available for additional leg
    !if (l-levelleg(ee) .lt. nMax-np) then
      !cycle eeloop
    !endif

    ! ee cannot have binary overlapp with previous binaries
    if (np .gt. 2) then
      if (iand(ei(np-1),sum(ei(:np-2))) .ne. 0) then
        cycle eeloop
      end if
    end if
    ! nMax equals np, last leg given by remainder
    ei(np) = eN - sum(ei(:np-1))
    if ((ei(np) .lt. ei(np-1)) .and. iand(ei(np),sum(ei(:np-1))) .eq. 0) then
      call routern (ei(:np))
    end if
    if (nMax .gt. np+1) then
      call branch_partition(min(ei(np-1)-1,eN-sum(ei(:np-1))-1),1,np+1)
    endif
  end do eeloop
  end subroutine branch_partition

!------------------------------------------------------------------------------!

  function isExternalLeg(binary) result(isExternal)
  integer, intent(in)  :: binary
  logical              :: isExternal

  isExternal = (iand(binary, binary-1) .eq. 0)
  end function isExternalLeg

!------------------------------------------------------------------------------!

  function legFromBinary(binary) result(leg)
  ! Retrieves the exponent. Number has to be a power of two.
  integer, intent(in)  :: binary
  integer              :: leg, n
  n = 0
  leg = binary
  if (iand(leg, Z'FFFF0000') .gt. 0) then
    n = n + 16;
    leg = ishft(leg, -16)
  end if
  if (iand(leg, Z'0000FF00') .gt. 0) then
    n = n + 8;
    leg = ishft(leg, -8)
  end if
  if (iand(leg, Z'000000F0') .gt. 0) then
    n = n + 4;
    leg = ishft(leg, -4)
  end if
  if (iand(leg, Z'0000000C') .gt. 0) then
    n = n + 2;
    leg = ishft(leg, -2)
  end if
  leg = n + leg
  end function legFromBinary

!------------------------------------------------------------------------------!

  function valid_topology(tadpole, selfenergy, selfenergyRes) result(valid)
  logical, intent(in) :: tadpole, selfenergy, selfenergyRes
  logical             :: valid

  valid = (((.not. tadpole)    .or.  (compute_tadpole)) .and. &
           ((.not. selfenergy) .or.  (compute_selfenergy)))

  valid = valid .and. (.not. selfenergyRes)
  end function valid_topology

!------------------------------------------------------------------------------!

  subroutine routerN (binaries)
  use modelfile, only: compute_colour_correlation_mdl, &
                       is_backgroundfield_mdl,         &
                       is_quantumfield_mdl,            &
                       get_particle_id_mdl,            &
                       get_particle_type2_mdl,         &
                       get_particle_antiparticle_mdl,  &
                       get_particle_mass_id_mdl,       &
                       get_vertex_mdl,                 &
                       get_vertex_branch_status_mdl,   &
                       get_vertex_colour_branches_mdl, &
                       get_vertex_pext_mdl,            &
                       get_vertex_order_increase_mdl,  &
                       get_vertex_colour_id_mdl,       &
                       get_vertex_lorentz_type_mdl,    &
                       get_vertex_perm_mdl


  ! binaries stores the binaries of ingoing legs, the ougoing binary is fixed by
  ! momentum conserveration via binOut = sum(binaries). The size of binaries
  ! also determines the number of incomming legs n_In.
  integer, intent (in), dimension(:) :: binaries

  ! For lp = 1 the binaries array is separated in  a loop binary(loopBin) and
  ! treebinaries(treeBins). By convention the first binary of the binary array
  ! is taken as the loop binary.
  integer              :: loopBin,                                   &
  ! temporary variable used to iterate over all possible outgoing particles
                          fOut, fOut_anti,                           &
  ! temporary variable used to store the propagator history
                          ho(1:size(hosT,1)), hoOut(1:size(hosT,1)), &
  ! temporary variable used to store the propagator mass history
                          hm, hmOut,                                 &
  ! temporary variable used to store the the current state whether counterterms
  ! have alrdy been included (1) or not (0)
                          xOut,                                      &
                          n_In, nMax, pext,                          &
  ! variable to store quark flow constraints
                          cq1,cq2,cll


  character(2)      :: cc

  !type(order) :: gsOut

  ! legsID is used to distinguish between internal and external particles.
  ! External particles differ from internal ones by the fact that only one bit
  ! of they binary is set (this is also true for the first and second loop leg)
  integer, allocatable, dimension(:) :: legsID,                                &

  ! imax stores the number of ingoing branches for a given binary. The argument
  ! of imax is not the binary, but the binary index itself, i.e. 1, 2, 3, .. ,
  ! n_In
                                        imax,                                  &
                                        treeBins,  &
  ! temporary variable used to store the sumed up order of coupling
                                        orderOut

  integer              :: i,ii,j,k,imax_tmp

  ! since we run over indefinite number of particles, we need to have a
  ! multiindex allowing for arbitrary nested loops
  integer(kind=8)      :: multiindex,multiindex_max


  type(waveIn), allocatable, dimension(:) :: wIn
  integer, allocatable, dimension(:,:)    :: csIn,csOut,orderIn
  integer, allocatable, dimension(:)      :: xIn,fIn
  logical, allocatable, dimension(:)      :: U1GluonIn
  logical                                 :: U1GluonOut

  integer, allocatable :: colorflowIds(:)
  integer, allocatable :: ncpower(:)
  logical              :: branch_exists,selfenergy,tadpole
  !type(Vertex)         :: vx
  integer :: vx

  b0 = b

  if (.not. allocated(orderOut)) then
    allocate(orderOut(n_orders))
  end if
  if (.not. allocated(orderInc)) then
    allocate(orderInc(n_orders))
  end if

  n_In = size(binaries)

  nMax = n_In + 1
  allocate(legsID(n_In))
  allocate(imax(n_In))
  allocate(wIn(n_In))
  allocate(csIn(n_In,-1:size(csT,1)-2),xIn(n_In),fIn(n_In), &
           orderIn(n_orders,n_In),U1GluonIn(n_In))

  ! l = i, if binaries(i) is a binary of the i^th
  ! external particle, otherwise l=legsE
  do i = 1, n_In
    if (isExternalLeg(binaries(i))) then
      legsID(i) = legFromBinary(binaries(i))
    else
      legsID(i) = legsE
    end if
  enddo

  if (lp .eq. LoopBranch) then
    ! separation of loop and tree legs
    allocate(treeBins(n_In - 1))
    if (binaries(1) .ge. pow2(legs)) then
      loopBin = binaries(1)
      treeBins(:) = binaries(2:)
    else
      loopBin = 0
      treeBins = 0
    end if
  else
    loopBin = 0 ! Used in BFM. treeBins not allocated for this case!
  end if

  if (vertex_functions) then
    if (lp .eq. LoopBranch) then
      do i = 1, size(treeBins)
        if (treeBins(i) .ne. 0 .and. .not. isExternalLeg(treeBins(i))) then
          return
        end if
      enddo
    else
      do i = 1, size(binaries)
        if (binaries(i) .ne. 0 .and. .not. isExternalLeg(binaries(i))) then
          return
        end if
      end do
    end if
  end if

  selfenergy = .false.
  tadpole = .false.
  if (lp .eq. LoopBranch .and. loopBin .ne. 0) then
    if (sum(treeBins) .eq. sumex) then
      ! Tadpole condition for 3-Vertex
      if (size(treeBins) .eq. 1) then
        tadpole = .true.
      ! Wavefunction correction for 4-Vertex
      else if (n_In .eq. 3) then
          selfenergy = selfenergy .or.                                         &
                       (isExternalLeg(treeBins(1)) .or.                        &
                        isExternalLeg(treeBins(2)))
      end if
    ! Wavefunction correction for 3-Vertex
    else if (n_In .eq. 2) then
      do i = 1, legs
        if(selfenergy) then; exit
        else
          selfenergy = selfenergy .or.                                         &
                       ! the treeBin contains all external bins except for one
                       isExternalLeg(iand(-treeBins(1)-1, pow2(legs)-1))
        end if
      enddo
    end if
  end if

  ! check if the binaries lead to a resonant selfenergy topology
  selfenergyRes = .false.

  ! only 2,3 and 4-point vertices can lead to selfenergy corrections.
  ! the 3-point case is dealt with later

  ! condition for 2-point vertices is if the incoming binary coicides with
  ! a resonant one then the contribution.
  if (n_In .eq. 1) then
    if (.not.resSE.and.npole.gt.0) then
      ! 2-point contribution has to be excluded for resonant particles
      do i = 1,npole
        selfenergyRes = selfenergyRes .or.         &
                        (binaries(1) .eq. pbins(i)) .or.      &
                        (binaries(1) .eq. pow2(legs)-1-pbins(i))
      enddo
    endif
  ! the condition for 4-point vertices is if the incoming binaries are attached
  ! to the loop current and if at least one of the tree binaries is resonant. In
  ! addition the treebins must contain all binaries.
  else if (n_In .eq. 3) then
    if (.not. resSE .and. npole .gt. 0 .and. loopBin .ge. pow2(legs)) then
      ! 1-loop 2-point contribution has to be excluded
      ! for resonant particles
      do i = 1,npole
        selfenergyRes = selfenergyRes .or. &
                        ((treeBins(1) + treeBins(2) .eq. sumex) .and.         &
                         ((treeBins(1) .eq. pbins(i)) .or. &
                          (treeBins(2) .eq. pbins(i))))
      enddo
    endif
  endif


  if (valid_topology(tadpole, selfenergy, selfenergyRes)) then

    ! Find all branches of ingoing wavefunctions (wIn%w(branches)) stored in the
    ! outgoing wavefunction branches wT
    call assign_incoming_wavefunctions(wIn, imax, legsID, binaries, n_In)

    ! select all possible outgoing particles, colorstructures, binaries and
    ! propagator histories

    ! Each incoming leg i comes with a certain number of kinematically allowed
    ! branches given by the number iax(i). The multiindex allows to iterate over
    ! all possible combinations of those branches. The range denoted as
    ! multiindex_max is given by the product of the number of all incoming
    ! branches from different particles particles

    ! compute multiindex range
    multiindex_max = 1
    do i = n_In, 1, -1
      multiindex_max = multiindex_max * imax(i)
    enddo

    ! Mainloop: Iteration over all combination of branches of incoming legs
    branchProductLoop: do multiindex = 1, multiindex_max

      selfenergyRes = .false.
      if (.not. resSE .and. npole .gt.0 .and. loopBin .ge. pow2(legs)) then
        ! 1-loop 2-point contribution has to be excluded
        ! for resonant particles

        imax_tmp = product(imax(2:n_In))
        j = Int(mod((multiindex - 1)/imax_tmp, imax(1)) + 1, kind=4)
        do i = 1,npole
          selfenergyRes = selfenergyRes .or.                    &
                          (ep(i,wIn(1)%w(j)) .and.              &
                           (treeBins(1) .eq. pbins(i) .or.      &
                            treeBins(1) .eq. pow2(legs)-1-pbins(i)))
        enddo
      endif
      if (selfenergyRes) cycle branchProductLoop

      ! Assign incoming wavefunctions
      do i = 1, n_In, 1
        ! The branch j for the current wavefunction i and multiindex is
        ! computed by the formula:
        ! j = mod((multiindex - 1)/imax_tmp, imax(i)) + 1
        ! Maps from the multiindex to a position in the
        ! hypervolume spanned by the branches
        ! (imax(1) x imax(2) x ... x imax(n_In)) products
        imax_tmp = product(imax(i+1:n_In))
        j = Int(mod((multiindex - 1)/imax_tmp, imax(i)) + 1, kind=4)

        fIn(i) = parT(wIn(i)%w(j))
        csIn(i,:) = csT(:, wIn(i)%w(j))
        xIn(i) = xxxT(wIn(i)%w(j))
        orderIn(:,i) = orderT(:,wIn(i)%w(j))
        U1GluonIn(i)  = U1gT(wIn(i)%w(j))
      end do

      ! For loop currents not all current shouls be taken along due to double
      ! counting (More than one cut one-loop amplitude can be used to identity
      ! the one one-loop amplitude)
      if (lp .eq. LoopBranch) then
        imax_tmp = product(imax(2:n_In))
        j = Int(mod((multiindex-1)/imax_tmp, imax(1)) + 1, kind=4)
        ho = hosT(:,wIn(1)%w(j))
        hm = hmaT(wIn(1)%w(j))
        call checkDoubleCounting(sum(treeBins),loopBin-el1,ho,doubleCounting)
      else
        doubleCounting = .false.
      end if

      if (doubleCounting) cycle ! double counting occurs

      ! check for counterterm insertion (also includes R2 terms)
      ! see explanation below for `x` variable.
      if (lp .ge. 2) then
        if(sum(binaries) .eq. pow2(legsE-1)-1 .and. sum(xIn(:)) .eq. 0) then
          xTmin = 1 ! -> no more counterterm insertion
        else
          xTmin = 0 ! -> possibly a new counterterm insertions
        end if

        if (sum(xIn(:)) .eq. 0) then
          xTmax = 1 ! -> possibly a new counterterm insertions
        else if (sum(xIn(:)) .gt. 1) then
          cycle branchProductLoop ! invalid branch combination, two counterterms
        else
          xTmax = 0 ! no counterterm insertion
        end if
      else
        xTmin = 0
        if (sum(xIn(:)) .gt. 0) then
          cycle branchProductLoop ! Trees and Loop Branches have no counterterms
        else
          xTmax = 0
        end if
      end if

      binOut = sum(binaries)

      last = .false.
      if (binOut .eq. pow2(legsE-1)-1) last = .true.

      if (lp .eq. LoopBranch) then
        if (last) then
          ! hOout(1) = i: p_1 flows in propagator i.
          ! hOout(2) = j: p_2 flows in propagator j.
          ! if i == j =>  p_1 + p_2 on propagator i.
          ! hOout(n) = k: p_n flows in propagator k.
          hoOut = ho
        else
          hoOut = ho + vectorLeg(sum(treeBins),1:size(ho,1)) * (maxval(ho) + 1)
          ! TODO: (nick 2016-07-18) some ideas to increase the number of
          ! propagators by two as required by rxi gauge for photons:
          ! Extend hosT(legs,wLDef) -> hosT(0:legs,wLDef), the 0 slot
          ! corresponds to the momentum p_0 which is 0. When increasing the
          ! proapgator twice set the 0 slot to: hoOut(0) = (maxval(ho) + 2) For
          ! the second propagator denominator no additional momentum flows in.
        end if
      else
        hoOut = -1
      end if

      ! depending on the x range we have:
      ! x = 0, 1  -> generate new branch with and without counterterm insertions
      ! x = 0, 0  -> generate new branch without counterterm, the resulting
      !              currents do not include counterterms
      ! x = 1, 1  -> generate new branch without counterterm, the resulting
      !              currents do include counterterms

      do x = xTmin, xTmax
        xOut = max(sum(xIn(:)), x)
        particleOutLoop: do fOut = 1, n_particles

          if (loopBin .ge. 2**legs .and. sm_generation_opt) then
            cc = get_particle_type2_mdl(fOut)
            if ( noquarks(pr) .and.           &
                 .not.nogluons(pr) .and.      &
                 .not.noweaks(pr) .and.       &
                 cc.ne.'q' .and. cc.ne.'q~' ) &
              cycle particleOutLoop
            if ( noquarks(pr) .and. nogluons(pr)        .and.   &
                 (cc.eq.'G'.or.cc.eq.'G~'.or.cc.eq.'g')       ) &
              cycle particleOutLoop
            if (noquarks(pr) .and. noweaks(pr) .and.              &
                cc.ne.'G' .and. cc.ne.'G~'.and. cc.ne.'g' .and.   &
                cc.ne.'q' .and. cc.ne.'q~'                      ) &
              cycle particleOutLoop
          endif

          ! last current particle must be match the last particle
          if (binOut .eq. pow2(legsE-1)-1 .and. fOut .ne. parT(legsE)) cycle

          ! if outgoing current is a loopcurrent, the field must have the
          ! quantumfield attribute
          if (loopBin .gt. 0 .and. .not. is_quantumfield_mdl(fOut)) cycle

          ! if outgoing current is a treecurrent, the field must have the
          ! backgroundfield attribute
          if (loopBin .eq. 0 .and. .not. is_backgroundfield_mdl(fOut)) cycle

          ! check for required resonances
          if (npole .gt. 0) then
            do i = 1,npole
              call check_resonances(fOut,n_In,loopBin,sum(binaries(2:)),multiindex,imax,wIn,hm,ho)
              if (last .and. (.not. xpOut(i)) .and. (.not. xpIR(i))) then
                cycle particleOutLoop
              endif
            enddo
          end if

          ! quark-flow constraint, (not support for 4-fermion operators)
          if (qflowextT .ne. 0 .and. n_In .eq. 2) then

            if (binOut .lt. pow2(legs)) then
              cq1 = constr_quark(binaries(1))
              cq2 = constr_quark(binaries(2))
              if (cq1 .ne. 0) then
                if (cq2 .ne. 0) then
                  ! two incoming quark lines -> constraint must match
                  if (qflowT(cq1) .ne. cq2) then
                    cycle particleOutLoop
                  end if
                ! quark line e2 with no constraint combined with
                ! constrained quark line e1
                else if (get_particle_type2_mdl(fIn(2)) .eq. 'q' .or. &
                         get_particle_type2_mdl(fIn(2)) .eq. 'q~') then
                  cycle particleOutLoop

                ! cq1 constrained to last leg ?
                else if (binOut .eq. pow2(legs-1)-1 .and. &
                         pow2(qflowT(cq1)-1) .ne. binOut+1) then
                  cycle particleOutLoop
                end if
              else if (cq2 .ne. 0) then
                ! quark line e1 with no constraint combined with
                ! constrained quark line e2
                if (get_particle_type2_mdl(fIn(1)) .eq. 'q' .or. &
                    get_particle_type2_mdl(fIn(1)) .eq. 'q~') then
                  cycle particleOutLoop

                ! cq2 constrained to last leg ?
                else if (binOut .eq. pow2(legs-1)-1 .and. &
                         pow2(qflowT(cq2)-1) .ne. binOut+1) then
                  cycle particleOutLoop
                end if
              end if
            else
              ! Check if e2 has a fermion flow constraint (, if yes cq2>0)
              cq2 = constr_quark(binaries(2))
              ! We only require the ff contraint for an incoming quark
              ! line which is exiting the loop via fIn(2)
              if ((get_particle_type2_mdl(fIn(1)) .eq. 'q' .or. &
                   get_particle_type2_mdl(fIn(1)) .eq. 'q~') .and. &
                  (get_particle_type2_mdl(fIn(2)) .eq. 'q' .or. &
                   get_particle_type2_mdl(fIn(2)) .eq. 'q~')) then
                ! check if loop line has open fermion flow constraint (cll > 0)
                cll = constr_quark_loopline(size(ho),ho)
                if (cq2 .ne. 0) then
                  ! cll and cq2 have  constraint, but not resolved -> wrong fermion flow
                  if (cll .ne. qflowT(cq2) .and. cll .ne. 0) then
                    cycle particleOutLoop
                  ! Final quark line unresolved ??
                  else if (cll .ne. qflowT(cq2) .and. binOut .eq. pow2(legs+1)-1) then
                    write(*,*) "Unhandled case:", binaries(1), binaries(2), cq2, cll
                    write(*,*) "ho:", ho
                    stop
                  end if
                ! cll has constraint, but not resolved -> wrong fermion flow
                else if (cll .ne. 0) then
                  cycle particleOutLoop
                end if
              end if
            end if
          end if


          if (allocated(csOut)) then
            deallocate(csOut)
            write (*, *) "csOut was allocated"
            stop
          end if

          call get_vertex_mdl(fIn(:),fOut,vx)
          ! -1: vertex does not exist
          if (vx .eq. -1) cycle particleOutLoop

          call get_vertex_branch_status_mdl(vx,x*lp,branch_exists)
          if (.not. branch_exists) cycle particleOutLoop

          ! Hack for selecting correct tadpole in the SMd6
          !if (lp .eq. LoopBranch .and. loopBin .eq. 0) then
            !if (wIn(1)%particle .eq. get_particle_id('H') .and. &
                !wIn(2)%particle .eq. get_particle_id('H') .and. &
                !fOut  .eq. get_particle_id('H') &
                !) then
              !if (sum(gsOut - (/0, 1, 1/)) .eq. 0) then
                !write(*,*) "gsOut:", gsOut
                !cycle
              !end if
            !end if
          !end if

          ! assign colorflowIds, not all of them are calculated, but only
          ! those for which select_branch is not equal to 0.
          call get_vertex_colour_branches_mdl(vx,x*lp,colorflowIds,nbranches)
          allocate(select_branch(nbranches),ncpower(nbranches))

          U1GluonOut = .false.
          do ib = 1, nbranches
            select_branch(ib) = ib
          enddo
          ncpower = 0
          call compute_colour_correlation_mdl(colorflowIds,     &
                                              csIn,U1GluonIn,   &
                                              csOut,U1GluonOut, &
                                              csT(-1:0,legsE),  &
                                              select_branch,    &
                                              last,pr,lp,legsE, &
                                              ncpower,n_In)

          ibloop: do ib = 1, nbranches
            if (select_branch(ib) .eq. 0) cycle ibloop
            if (.not. couplings_active_frm) then
              if (vanishing_couplings(x*lp,vx,select_branch(ib)-1)) then
                cycle
              end if
            end if
            if (last) then
              call get_vertex_pext_mdl(vx,x*lp,select_branch(ib)-1,pext)
              if (lp .eq. LoopBranch) then
                ! Rxi propagator for massive VB
                if (pext .eq. 2) then
                  if (fOut .eq. get_particle_id_mdl('Z')) then
                    hmOut = substhm(hm,1,get_particle_mass_id_mdl(  &
                                         get_particle_id_mdl("G0")))
                  else if (fOut .eq. get_particle_id_mdl('W+') .or. &
                           fOut .eq. get_particle_id_mdl('W-')) then
                    hmOut = substhm(hm,1,get_particle_mass_id_mdl(  &
                                         get_particle_id_mdl("G+")))
                  else
                    write (*,*) "Using pext for another particle " // &
                                "than Z or W. fout:", fOut
                    stop
                  end if
                else
                  hmOut = hm
                end if
              ! No propagator for final particle -> no extension needed
              else if (pext .gt. 0) then
                cycle ibloop
              end if
            end if

            if (colour_optimization.ge.2) then
              ! Do not compute U1 Gluon
              do i = 1, legsE
                if (csOut(i,ib) .eq. i) cycle ibloop
              end do
            end if

            b = b + 1
            if (b .gt. bLDef) call reallocate_bLDef

            nMaxT(b) = nMax
            do i = 1, n_In, 1
              imax_tmp = product(imax(i+1:n_In))
              j = Int(mod((multiindex-1) / imax_tmp, imax(i)) + 1, kind=4)
              wT(i,b) = wIn(i)%w(j)
            enddo

            ! setting non-used slots of currents to zero.
            if (n_In .ne. nDef-1) then
              wT(n_In+1:nDef-1,b) = 0
            end if

            call get_vertex_pext_mdl(vx,x*lp,select_branch(ib)-1,pext)
            if (lp .eq. LoopBranch) then
              if (.not. last) then
                ! sethm has been adjusted and uses as stepsize n_masses from
                ! class particles
                if (pext .eq. 2) then
                  if (fOut .eq. get_particle_id_mdl('Z')) then
                    hmOut = sethm(get_particle_mass_id_mdl( &
                                  get_particle_id_mdl("G0")), hm)
                  else if (fOut .eq. get_particle_id_mdl('W+') .or. &
                           fOut .eq. get_particle_id_mdl('W-')) then
                    hmOut = sethm(get_particle_mass_id_mdl( &
                                  get_particle_id_mdl("G+")), hm)
                  else
                    write (*,*) "Using pext for another particle " // &
                                "than Z or W. fout:", fOut
                    stop
                  end if
                else if (pext .eq. 0 .or. pext .eq. 1) then
                  hmOut = sethm(get_particle_mass_id_mdl(fOut), hm)
                else
                  write (*,*) "Unsupported propagator extension: ", pext
                  stop
                end if
              end if
            else
              hmOut = 0
            end if

            check = 1

            orderOut = 0
            do i = 1, n_In, 1
              orderOut = orderOut + orderIn(:,i)
            enddo
            call get_vertex_order_increase_mdl(vx,x*lp,select_branch(ib)-1,orderInc)
            orderOut = orderOut + orderInc

            fOut_anti = get_particle_antiparticle_mdl(fOut)
            wloop: do k = wOutBin(binOut), w
              if(fOut_anti .ne. parT(k)) cycle wloop
              if(binOut .ne. binT(k)) cycle wloop
              do ii = 1, size(hosT,1)
                if (hoOut(ii) .ne. hosT(ii,k)) cycle wloop
              enddo
              if(hmOut .ne. hmaT(k)) cycle wloop
              if(xOut .ne. xxxT(k)) cycle wloop
              if(sum(abs(orderOut - orderT(:,k))) .ne. 0) cycle wloop
              if(U1GluonOut .neqv. U1gT(k)) cycle wloop
              do ii = -1, size(csT,1) - 2
                if (csOut(ii, ib) .ne. csT(ii, k)) cycle wloop
              enddo
              if (npole .gt. 0) then
                do ii = 1, npole
                  if (xpout(ii) .neqv. xp(ii,k)) cycle wloop
                  if (epout(ii) .neqv. ep(ii,k)) cycle wloop
                enddo
                do ii = 1,legs
                  if (gphout(ii) .ne. gph(ii,k)) cycle wloop
                enddo
              endif
              ! If we are here, current "i" is the same as the
              ! present one
              check = 0
              wT(nDef,b) = k
              exit wloop
            enddo wloop

            if(check.ne.0) then
              w = w + 1
              if (w.gt.wLDef) call reallocate_wLDef

              wT(nDef,b)  = w
              parT(w)     = fOut_anti
              csT(:,w)    = csOut(:,ib)
              binT(w)     = binOut
              hosT(:,w)   = hoOut
              hmaT(w)     = hmOut
              xxxT(w)     = xOut
              orderT(:,w) = orderOut
              U1gT(w)     = U1GluonOut

              if (npole.gt.0) then
                xp(:,w)  = xpOut
                ep(:,w)  = epOut
                gph(:,w) = gphOut
              endif

            end if

            orderIncT(:,b) = orderInc
            call get_vertex_lorentz_type_mdl(vx,x*lp,select_branch(ib)-1,typeT(b))
            call get_vertex_perm_mdl(vx,x*lp,select_branch(ib)-1,permT(b))
            call get_vertex_colour_id_mdl(vx,x*lp,select_branch(ib)-1,colourfacT(1,b))
            select case (x*lp)
            case (TreeBranch:LoopBranch)
              cbtypeT(b) = LoopBranch ! Tree and Loop type
            case(CTBranch)
              cbtypeT(b) = CTBranch
            case(R2Branch)
              cbtypeT(b) = R2Branch
            end select
            colourfacT(2,b) =  ncpower(ib)
            colorBranch(b) = select_branch(ib)-1
            cbT(b) = ib
            call assign_couplings(vx,b,x*lp,couT)

          enddo ibloop
          deallocate (colorflowIds,select_branch,csOut,ncpower)

        enddo particleOutLoop
      enddo
    enddo branchProductLoop

  end if   ! self-energy filter

  deallocate(orderOut,orderInc)
  if (lp .eq. LoopBranch) then
    deallocate(treeBins)
  end if
  deallocate(wIn,csIn,xIn,fIn,orderIn,U1GluonIn)
  deallocate(legsID,imax)
  if (allocated(select_branch)) then
    deallocate(select_branch)
  end if

  end subroutine routerN

!------------------------------------------------------------------------------!

  function constr_quark(bin)
    integer, intent(in) :: bin
    integer             :: constr_quark,i

    constr_quark = 0
    if (iand(qflowextT, bin) .ne. 0) then
      do i = 1, legs
        if (qflowT(i) .ne. 0) then
          if (iand(pow2(qflowT(i)-1),bin) .ne. 0 .and. &
              iand(pow2(i-1),bin) .eq. 0) then
              constr_quark = qflowT(i)
              return
          else if (iand(pow2(qflowT(i)-1),bin) .eq. 0 .and. &
                   iand(pow2(i-1),bin) .ne. 0) then
              constr_quark = i
              return
          end if
        end if
      end do
    end if

  end function constr_quark

!------------------------------------------------------------------------------!

  recursive function constr_quark_loopline(sho,ho) result(cl)
    integer, intent(in)                 :: sho
    integer, dimension(sho), intent(in) :: ho
    integer, dimension(sho)             :: hor
    integer :: mv,i,tb,cf,cl

    mv = maxval(ho)
    if (mv .eq. 0) then
      cl = 0
      return
    end if
    tb = 0
    do i = 1, sho
      if (ho(i) .eq. mv) then
        tb = tb + pow2(i-1)
      end if
    end do

    cf = constr_quark(tb)
    if (cf .eq. 0) then
      hor = ho - vectorLeg(tb,:sho)*mv
      cl = constr_quark_loopline(sho,hor)
    else
      cl = cf
    end if

  end function constr_quark_loopline

!------------------------------------------------------------------------------!

  subroutine assign_incoming_wavefunctions(wIn, imax, legsID, binaries, n_In)
    implicit none
    integer :: i
    integer, dimension(:), intent(inout) :: imax
    integer, dimension(:), intent(in) :: legsID, binaries
    integer, intent(in) :: n_In
    integer :: j
    type(waveIn), allocatable, dimension(:), intent(inout) :: wIn

    ! Define incoming wavefunctions
    imax = 0
    do i = 1, n_In
      select case (legsID(i) - legsE)

      ! the incoming particle is an external one
      case (:-1)
        imax(i) = 1
        allocate (wIn(i)%w(1))
        wIn(i)%w(1) = legsID(i)

      ! the incoming particle is an internal one (fusion of internal/external)
      case(0)
        branchLoop: do j = 1, b
          ! Assign to all incoming wavefunctions the branch id
          ! wT are outgoing wavefunctions which have been computed sofar.
          if (binaries(i) .eq. binT(wT(nDef,j))) then
            ! Prevent double counting - only take those branches which are yet
            ! not included
            branchLoop2: do ii = 1, imax(i)
              if (wT(nDef,j) .eq. wIn(i)%w(ii)) cycle branchLoop
            enddo branchLoop2
            ! If a non-assigned branch has been found imax is incremented by one
            ! and the incoming wavefunction is assigned to that branch id.
            imax(i) = imax(i) + 1
            ! wIn(i) can have a at max the number of branches already existing.
            if (imax(i) .eq. 1) allocate (wIn(i)%w(b))
            wIn(i)%w(imax(i)) = wT(nDef,j)
          end if
        enddo branchLoop
      end select
    enddo
  end subroutine assign_incoming_wavefunctions

!------------------------------------------------------------------------------!

  subroutine assign_couplings(vx,branch,xlp,couplings)
    use modelfile, only: get_vertex_ncouplings_mdl,get_vertex_coupling_ids_mdl
    integer,    intent(in)    :: vx,branch,xlp
    type(iarr), intent(inout) :: couplings(bLDef)
    integer                   :: max_couplings

    !select case(xlp)
    !case(TreeBranch, LoopBranch)
      !max_couplings = size(vx%branch(colorBranch(branch))%couplings)
    !case (CTBranch)
      !max_couplings = size(vx%branchCT(colorBranch(branch))%couplings)
    !case (R2Branch)
      !max_couplings = size(vx%branchR2(colorBranch(branch))%couplings)
    !end select
    call get_vertex_ncouplings_mdl(vx,xlp,colorBranch(branch),max_couplings)
    if (allocated(couplings(branch)%c)) then
      write (*, *) "c alrdy allocated"
      stop
    end if
    allocate(couplings(branch)%c(max_couplings))
    call get_vertex_coupling_ids_mdl(vx,xlp,colorBranch(branch),couplings(branch)%c)
    !select case(xlp)
    !case(TreeBranch, LoopBranch)
      !couplings(branch)%c(:) = vx%branch(colorBranch(branch))%couplings(:)
    !case (CTBranch)
      !couplings(branch)%c(:) = vx%branchCT(colorBranch(branch))%couplings(:)
    !case (R2Branch)
      !couplings(branch)%c(:) = vx%branchR2(colorBranch(branch))%couplings(:)
    !end select
  end subroutine assign_couplings

!------------------------------------------------------------------------------!

  subroutine check_resonances(fOut,n_In,loopBin,streebins,multiindex,imax,wIn,hm,ho)
    use modelfile, only: get_particle_spin_mdl,get_particle_cmass2_reg_mdl, &
                         get_n_masses_mdl
    integer,         intent(in) :: fOut,n_In,imax(:),hm,loopBin,streebins,ho(:)
    type(waveIn),    intent(in) :: wIn(:)
    integer(kind=8), intent(in) :: multiindex

    integer :: j,k,n1,ii,hm0,nprop,step,imax_tmp,nmasses,sumA,sumB
    integer :: paOut_spin, paOut_mass_reg, pa_mass_reg, pa_mass_id, pa_spin

    nmasses = get_n_masses_mdl()
    xpOut = .false.
    epOut = .false.
    do i = 1, n_In, 1
      imax_tmp = product(imax(i+1:n_In))
      j = Int(mod((multiindex - 1)/imax_tmp, imax(i)) + 1, kind=4)
      xpOut = xpOut .or. xp(:,wIn(i)%w(j))
      epOut = epOut .or. ep(:,wIn(i)%w(j))
    enddo

    paOut_spin = get_particle_spin_mdl(fOut)
    paOut_mass_reg = get_particle_cmass2_reg_mdl(fOut)
    do i = 1,npole
      pa_mass_reg = get_particle_cmass2_reg_mdl(ppar(i))
      xpOut(i) = xpOut(i) .or. (paOut_mass_reg .eq. pa_mass_reg .and.       &
                 (binOut .eq. pbins(i) .or.  binOut .eq. pow2(legs)-1-pbins(i)))

      epOut(i) = epOut(i) .or.                   &
                 (binOut .eq. pbins(i) .or.      &
                  binOut .eq. pow2(legs)-1-pbins(i))
    enddo

    ! IR resonances
    ! Check if incoming particle is Photon/Gluon
    imax_tmp = product(imax(2:n_In))
    j = Int(mod((multiindex - 1)/imax_tmp, imax(1)) + 1, kind=4)
    if (loopBin .eq. pow2(legs)) then
      pa_spin = get_particle_spin_mdl(parT(wIn(1)%w(j)))
      pa_mass_id = get_particle_cmass2_reg_mdl(parT(wIn(1)%w(j)))
      if (pa_spin .eq. 3 .and. pa_mass_id .eq. 1) then
        gph(1,wIn(1)%w(j)) = 1
      endif
    endif
    gphOut = gph(:,wIn(1)%w(j))

    ! Check if outgoing particle is Photon/Gluon
    if (loopBin .ge. pow2(legs) .and. (.not. last)) then
      if (paOut_spin .eq. 3 .and. paOut_mass_reg .eq. 1) then
        gphOut(maxval(ho)+2) = 1
      endif
    endif

    xpIR = .false.

    ! check if the loop is IR resonant
    if (binOut .eq. pow2(legs+1)-1) then ! last loop leg
      hm0 = hm
      step = nmasses + 1
      do ii = legs,1,-1
        mass(ii) = hm0/step**(ii-1)
        hm0 = hm0 - mass(ii)*step**(ii-1)
      enddo
      nprop = maxval(ho)+1

      do i = 1,npole
        ! proploop scanns for a IR resonant propagator (RP), i.e. Photon or Gluon
        ! If a RP is found ee(1)(ee(2)) represents the binary(sum) to the
        ! right(left)
        ! of that RP,i.e.
        !x           x
        ! \         /
        ! /000RP0000\
        ! |          |
        ! |ee(2)     |ee(1)
        !
        proploop: do n = 1,nprop
          imax_tmp = product(imax(2:n_In))
          j = Int(mod((multiindex - 1)/imax_tmp, imax(1)) + 1, kind=4)
          xpIR(i) = gph(n,wIn(1)%w(j)) .eq. 1

          if (.not. xpIR(i)) cycle proploop
          if (n .eq. 1) then
            ee(1) = streebins
            ee(2) = 0
            do ii = 1,size(ho,1)
              if (ho(ii)+1 .eq. 2) ee(2) = ee(2) + pow2(ii-1)
            enddo
          elseif (n .eq. nprop) then
            ee(1) = 0
            do ii = 1,size(ho,1)
              if (ho(ii).gt.0.and.ho(ii)+1.eq.nprop) &
                ee(1) = ee(1) + pow2(ii-1)
            enddo
            ee(2) = streebins
          else
            ee(1) = 0
            ee(2) = 0
            do ii = 1,size(ho,1)
              if (ho(ii) .gt. 0) then
                if (ho(ii)+1 .eq. n) &
                  ee(1) = ee(1) + pow2(ii-1)
                if (ho(ii)+1 .eq. n+1) &
                  ee(2) = ee(2) + pow2(ii-1)
              endif
            enddo
          endif

          ! Next we consider the cases where we are to the right(1) of the
          ! resonance and to the left(2).

          iiloop: do ii = 1,2
            ! since ee(ii) is directly connected to a photon, it is ir resonant
            ! if it is an external (there onshell) particle or if ee(ii) is
            ! considered onshell
            do j = 1,npole
              xpIR(i) = (levelLeg(ee(ii)) .eq. 1) .or. (ee(ii) .eq. pbins(j))
              if (xpIR(i)) exit
            enddo
            if (.not. xpIR(i)) cycle iiloop
            ! the contribution might yet be subdominant and we have to require
            ! that there is an additional resonance in the loop.
            ! We search for a propagator with pbins(i) resonance in the
            ! loop. If such a propagator is found (mass(n1) == nmf(ppar(i)))
            ! then, we have the following situation


            !        sum B
            !                        |
            !    \___________________|________________/
            ! __ /                                    \___
            !    0                                    x
            !     0000RP0000-------.....-----xxxxn1xxx
            !               |        |       |
            !               |        |       |
            !                sum A

            ! where sum B represents the sum of binaries between n1 and RP and
            ! sum A represents the sum of binaries between RP and n1

            do n1 = 1,nprop
              select case (ii)
              case (1)
                sumA = 0
                sumB = streebins
                do k = 1,size(ho,1)
                  if (ho(k).gt.0) then
                    if(ho(k)+1 .ge. n+1 .and. ho(k)+1 .le. n1) then
                      sumA = sumA + pow2(k-1)
                    endif
                    if((ho(k)+1 .ge. n+1 .and. ho(k)+1 .le. nprop) .or. &
                       (ho(k)+1 .ge. 1 .and. ho(k)+1 .le. n1)) then
                      sumB = sumB + pow2(k-1)
                    endif
                  endif
                enddo
              case (2)
                sumA = 0
                sumB = streebins
                do k = 1,size(ho,1)
                  if (ho(k).gt.0) then
                    if(ho(k)+1 .ge. n1+1 .and. ho(k)+1 .le. n) then
                      sumA = sumA + pow2(k-1)
                    endif
                    if((ho(k)+1 .ge. n1+1 .and. ho(k)+1 .le. nprop) .or. &
                       (ho(k)+1 .ge. 1 .and. ho(k)+1 .le. n))then
                      sumB = sumB + pow2(k-1)
                    endif
                  endif
                enddo
              end select
              pa_mass_id = get_particle_cmass2_reg_mdl(ppar(i))
              xpIR(i) = mass(n1) .eq. pa_mass_id .and. &
                        (sumA .eq. pbins(i) .or. sumB .eq. pbins(i))
              if (xpIR(i)) exit
            enddo
            if (xpIR(i)) then
              exit proploop
            else
              cycle iiloop
            endif
          enddo iiloop
        enddo proploop
      enddo
    endif

    do i = 1,npole
      xpIR(i) = xpIR(i) .and. resIR
    enddo

  end subroutine check_resonances

  end subroutine makeSkeleton

!------------------------------------------------------------------------------!

  !subroutine assign_branch(vx,xlp,oInc,sb)
  !use modelfile, only: Vertex
  !type(Vertex), intent(in)                          :: vx
  !integer, intent(in)                               :: xlp
  !integer, dimension(:), intent(in)                 :: oInc
  !integer, allocatable, dimension(:), intent(inout) :: sb
  !integer :: ib
  !select case (xlp)
  !case (TreeBranch:LoopBranch)
    !do ib = 1, size(vx%branch)
      !if (all(vx%branch(ib-1)%coupling_order .eq. oInc) .eqv. .true.) then
        !if (.not. allocated(sb)) then
          !allocate(sb(size(vx%branch)))
          !sb = 0
        !end if
        !sb(ib) = ib
      !end if
    !end do
  !case(CTBranch)
    !do ib = 1, size(vx%branchCT)
      !if (all(vx%branchCT(ib-1)%coupling_order .eq. oInc) .eqv. .true.) then
        !if (.not. allocated(sb)) then
          !allocate(sb(size(vx%branchCT)))
          !sb = 0
        !end if
        !sb(ib) = ib
      !end if
    !end do
  !case(R2Branch)
    !do ib = 1, size(vx%branchR2)
      !if (all(vx%branchR2(ib-1)%coupling_order .eq. oInc) .eqv. .true.) then
        !if (.not. allocated(sb)) then
          !allocate(sb(size(vx%branchR2)))
          !sb = 0
        !end if
        !sb(ib) = ib
      !end if
    !end do
  !end select
  !end subroutine assign_branch

!------------------------------------------------------------------------------!

  function vanishing_couplings(xlp,vx,sel_b) result (zero)
  use modelfile, only: is_zero_coupling_vertex_mdl
  integer, intent(in) :: xlp, sel_b, vx
  logical             :: zero
  !integer             :: i
  !real(dp)            :: maxvalue

  zero = .false.
  select case (xlp)
  case (TreeBranch)
    if (.not. tree_couplings_active_frm) then
      zero = is_zero_coupling_vertex_mdl(vx,xlp,sel_b)
      !maxvalue = 0d0
      !do i = 1,size(vx%branch(sel_b)%couplings)
        !maxvalue = maxvalue + &
                 !abs(get_coupling(vx%branch(sel_b)%couplings(i-1)))
      !end do
      !if (maxvalue/zerocut .lt. 100d0) then
        !zero = .true.
      !end if
    end if
  case(CTBranch)
    if (.not. ct_couplings_active_frm) then
      zero = is_zero_coupling_vertex_mdl(vx,xlp,sel_b)
      !maxvalue = 0.
      !do i = 1,size(vx%branchCT(sel_b)%couplings)
        !maxvalue = maxvalue + &
                 !abs(get_coupling(vx%branchCT(sel_b)%couplings(i-1)))
      !end do
      !if (maxvalue/zerocut .lt. 100d0) then
        !zero = .true.
      !end if
    end if
  case(R2Branch)
    if (.not. r2_couplings_active_frm) then
      zero = is_zero_coupling_vertex_mdl(vx,xlp,sel_b)
      !maxvalue = 0.
      !do i = 1,size(vx%branchR2(sel_b)%couplings)
        !maxvalue = maxvalue + &
                 !abs(get_coupling(vx%branchR2(sel_b)%couplings(i-1)))
      !end do
      !if (maxvalue/zerocut .lt. 100d0) then
        !zero = .true.
      !end if
    end if
  end select
  end function vanishing_couplings

!------------------------------------------------------------------------------!

  subroutine checkDoubleCounting (et,el,ho,doubleCounting)

  ! checks whether a double counting of one-loop diagrams occurs

  integer, intent (in)   :: et,el,ho(:)
  logical, intent (out)  :: doubleCounting

  integer                :: i,o1,g3,g4,g5
  integer, dimension (2) :: g,n

  ! Each offset is a binary number, i.e. is a sum without repetition
  ! of 1,2,4,8,...,2**(N-1) (N= number of external legs). They can
  ! therefore be thought as an ordered sequence (from the smallest to
  ! the biggest) of 1,2,4,8,...,2**(N-1) with or without gaps. The
  ! first number of the sequence (the smallest) is called the
  ! identifier of the offset.

  ! Two steps:

  ! 1) First I require that just an offset containing the 1 can
  ! enter at the beginning of the loop.

  ! 2) Then I require that three of the offsets must appear in a
  ! fixed order. I choose these three as the ones with the smallest
  ! identifiers and I require that these three offsets appear in the
  ! history of the offsets in the same order as their identifiers.
  ! Ex:
  ! offSet1 = 1+8    = 9   identifier1 = 1
  ! offSet2 = 2+4+32 = 38  identifier2 = 2
  ! offSet3 = 16     = 16  identifier3 = 16
  ! offSet4 = 64     = 64  identifier4 = 64
  ! I require that o1,o2,o3 always appear in this order
  ! (first offSet1, then offSet2, then offSet3). offSet4 can
  ! appear everywhere, also in between (not at the first
  ! position, which is excluded by the first step).

  if (ho(1) .eq. -1) then
    doubleCounting = .false.
  else
    select case (mod(el+et,2))
    case (1)                        ! step 1
      o1 = 0
      do i = 1,size(ho,1)
        if (ho(i) .eq. 1) o1 = o1 + pow2(i-1)
      enddo
      ! el can be 0 or odd. When el=0, then et contains 1 and
      ! n(1)=g(1)=1. Independently on the value of n(2), we have
      ! g3=g4, and doubleCounting is .false. as it should be.
      ! g3=g4 because el is an empty set (there is only the loop
      ! propagator, with no offset). Let us then consider
      ! always the situation when el contains 1 and et does not.
      g(:)   = firstGaps(o1,1:2)    ! first two gaps (candidates)
      n(1:2) = firstNumbers(et,1:2) ! hits on the tree
      select case (n(1)-g(1))       ! step 2
      case (0)
        select case (n(2)-g(2))
        case (0)                    ! then we need a 3rd identifier
          g3 = firstGap(o1+et)
          g4 = firstGap(el+et)
          select case (g3-g4)
          case (0)
            doubleCounting = .false. ! this means that g3 has to come
          case default               ! still...accept
            doubleCounting = .true.  ! this means that g3 has already
          end select                 ! come; reject (g3>g(1))
        case default ! the first member of the tree has filled
                     ! the first gap;
          doubleCounting = .false.
        end select
      case default
        select case (n(1)-g(2))
        case (0)
          g5 = firstGap(el)
          select case (g5-g(1))
          case (0)
            doubleCounting = .true.  ! this means that g(1) has to
          case default               ! come still...reject
            doubleCounting = .false. ! this means that g(1) has come
          end select                 ! in; accept (g(1)<g5)
        case default ! when the first gap is not filled by the tree,
                     ! no double counting
          doubleCounting = .false.
        end select
      end select
    case default
      doubleCounting = .true.
    end select
  endif

  end subroutine checkDoubleCounting

!==============================================================================!
!                                    sethm                                     !
!==============================================================================!

  function sethm (m,hmIn) result (hmOut)
  use modelfile, only: get_n_masses_mdl
  integer, intent (in) :: m,hmIn ! m = mass in,
                                 ! hmIn = mass history in
  integer              :: hmOut  ! hmOut = mass history out

  integer              :: n,step,lmax, n_masses

  n_masses = get_n_masses_mdl()
  step = n_masses + 1

  lmax = maxval(prs(:)%legs)

  if (hmIn.eq.0) then
    hmOut = 0
  else
    do n = 1,lmax
      if (hmIn.lt.step**n) then
        hmOut = hmIn + m*step**n
        exit
      endif
    enddo
  endif

  end function sethm

  function substhm(hmIn,pos,massid) result(hmOut)
  use modelfile, only: get_n_masses_mdl
  integer, intent (in) :: hmIn,pos,massid
  integer              :: hm0,hmOut  ! hmOut = mass history out
  integer              :: i,step,lmax, n_masses
  integer, allocatable :: vm(:)

  n_masses = get_n_masses_mdl()
  step = n_masses + 1
  lmax = maxval(prs(:)%legs)
  allocate(vm(lmax))
  hm0 = hmIn
  do i = lmax,1,-1
    vm(i) = hm0/step**(i-1)
    hm0 = hm0 - vm(i)*step**(i-1)
  end do
  if (pos .eq. 1) then
    hmOut = massid
  else
    hmOut = vm(1)
  end if
  do i = 2, lmax
    if (i .eq. pos) then
      hmOut = sethm(massid, hmOut)
    else
      hmOut = sethm(vm(i), hmOut)
    end if
  end do
  deallocate(vm)

  end function substhm

  subroutine printhm(hmIn)
  use modelfile, only: get_n_masses_mdl
  integer, intent (in) :: hmIn
  integer              :: hm0
  integer              :: i,step,lmax, n_masses
  integer, allocatable :: vm(:)

  n_masses = get_n_masses_mdl()
  step = n_masses + 1
  lmax = maxval(prs(:)%legs)
  allocate(vm(lmax))
  hm0 = hmIn
  do i = lmax,1,-1
    vm(i) = hm0/step**(i-1)
    hm0 = hm0 - vm(i)*step**(i-1)
  end do
  deallocate(vm)

  end subroutine

!==============================================================================!
!                               reallocate_bLDef                               !
!==============================================================================!

  subroutine reallocate_bLDef
  use modelfile, only: get_n_orders_mdl
  integer :: i, n_orders
  integer                 :: bdef0, bdef
  integer,    allocatable :: tempI(:,:)
  integer,    allocatable :: tempN(:)
  type(iarr), allocatable :: tempC(:)

  n_orders = get_n_orders_mdl()

  bdef0 = bLDef
  bdef  = bLDef + bLInc

  allocate (tempI(4,bdef0))
  tempI = wT
  deallocate (wT)
  allocate (wT(nDef,bdef))
  wT(:,:bdef0) = tempI(:,:)
  deallocate (tempI)

  allocate (tempC(bdef0))
  tempC = couT
  deallocate (couT)
  allocate (couT(bdef))
  couT(:bdef0) = tempC(:)
  deallocate (tempC)

  allocate (tempN(bdef0))
  tempN = nMaxT
  deallocate (nMaxT)
  allocate (nMaxT(bdef))
  nMaxT(:bdef0) = tempN(:)
  tempN = cbT
  deallocate (cbT)
  allocate (cbT(bdef))
  cbT(:bdef0) = tempN(:)
  tempN = cbtypeT
  deallocate (cbtypeT)
  allocate (cbtypeT(bdef))
  cbtypeT(:bdef0) = tempN(:)
  deallocate(tempN)

  allocate (tempI(2,bdef0))
  tempI(1:2,:) = colourfacT(:,:)
  deallocate (colourfacT)
  allocate (colourfacT(1:2,bdef))
  colourfacT(1:2,:bdef0) = tempI(1:2,:)
  deallocate (tempI)

  allocate (tempI(1,bdef0))
  tempI(1,:) = typeT(:)
  deallocate (typeT)
  allocate (typeT(bdef))
  typeT(:bdef0) = tempI(1,:)
  tempI(1,:) = permT(:)
  deallocate (permT)
  allocate (permT(bdef))
  permT(:bdef0) = tempI(1,:)
  deallocate (tempI)

  i = size(orderIncT,1)
  allocate (tempI(i,bdef0))
  tempI = orderIncT
  deallocate (orderIncT)
  allocate (orderIncT(i,bdef))
  orderIncT(:,:bdef0) = tempI(:,:)
  deallocate(tempI)

  bLDef = bdef

  end subroutine reallocate_bLDef

!==============================================================================!
!                               reallocate_wLDef                               !
!==============================================================================!

  subroutine reallocate_wLDef

  integer                  :: l,wdef0,wdef
  integer, allocatable     :: tempI(:,:)
  logical, allocatable     :: tempL(:,:)

  wdef0 = wLDef
  wdef  = wLDef + wLInc

  l = size(csT,1)-2
  allocate (tempI(-1:l,wdef0))
  tempI = csT
  deallocate (csT)
  allocate (csT(-1:l,wdef))
  csT(:,:wdef0) = tempI(:,:)
  deallocate (tempI)

  l = size(hosT,1)
  allocate (tempI(l,wdef0))
  tempI = hosT
  deallocate (hosT)
  allocate (hosT(l,wdef))
  hosT(:,:wdef0) = tempI(:,:)
  deallocate (tempI)
  allocate (tempI(1,wdef0))
  tempI(1,:) = parT(:)
  deallocate (parT)
  allocate (parT(wdef))
  parT(:wdef0) = tempI(1,:)
  tempI(1,:) = binT(:)
  deallocate (binT)
  allocate (binT(wdef))
  binT(:wdef0) = tempI(1,:)

  tempI(1,:) = hmaT(:)
  deallocate (hmaT)
  allocate (hmaT(wdef))
  hmaT(:wdef0) = tempI(1,:)

  tempI(1,:) = xxxT(:)
  deallocate (xxxT)
  allocate (xxxT(wdef))
  xxxT(:wdef0) = tempI(1,:)
  deallocate (tempI)
  allocate (tempL(1,wdef0))
  tempL(1,:) = U1gT(:)
  deallocate (U1gT)
  allocate (U1gT(wdef))
  U1gT(:wdef0) = tempL(1,:)
  deallocate (tempL)

  l = size(orderT,1)
  allocate (tempI(l,wdef0))
  tempI = orderT
  deallocate (orderT)
  allocate (orderT(l, wdef))
  orderT(:,:wdef0) = tempI(:,:)
  deallocate(tempI)

  wLDef = wdef

  end subroutine reallocate_wLDef

!------------------------------------------------------------------------------!

end module skeleton_rcl

!------------------------------------------------------------------------------!
