!******************************************************************************!
!                                                                              !
!    amplitude_rcl.f90                                                         !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module amplitude_rcl

!------------------------------------------------------------------------------!

  use globals_rcl
  use skeleton_rcl, only: nDef
  use wave_functions_rcl, only: definewp
  use tree_vertices_rcl, only: tree2,tree3,tree4,tree5,tree6
  use loop_vertices_rcl, only: loop3,loop4,loop5,loop6
  use modelfile, only: compute_tree_mdl,compute_loop_mdl,                   &
                       get_cmass2_mdl,get_cmass2_reg_mdl,                   &
                       get_particle_cmass2_mdl,get_particle_cmass2_reg_mdl, &
                       get_particle_id_mdl,get_particle_width_mdl,          &
                       get_particle_antiparticle_mdl,                       &
                       get_coupling_value_mdl,get_order_id_mdl,             &
                       get_particle_type2_mdl,get_recola_base_mdl,          &
                       get_particle_mass_id_mdl,get_particle_name_mdl,      &
                       get_propagator_extension_mdl,get_parameter_mdl,      &
                       has_feature_mdl,reset_ctcouplings_mdl
  use tables_rcl, only: levelLeg,firstNumbers,riMax,dzgs
  use collier_interface_rcl, only: InitEvent_cll,GetAccFlag_cll,TNten_cll, &
                                   GetErrFlag_cll,GetCritAcc_cll
  use order_rcl, only: oi_reg,oi2_reg,oiSize,oi2Size,max_od,reg_od2,compoi

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  complex(dp), allocatable :: matrix(:,:,:,:,:)
  real(dp),    allocatable :: momenta(:,:)
  logical                  :: momcheck
  real(dp),    allocatable :: matrix2(:,:,:),         &
                              matrix2h(:,:,:,:),      &
                              matrix2int(:,:,:,:),    &
                              matrix2cc(:,:,:,:),     &
                              matrix2ccint(:,:,:,:),  &
                              matrix2ccnlo(:,:,:,:),  &
                              matrix2scc(:,:,:,:),    &
                              matrix2sccnlo(:,:,:,:), &
                              matrix2sc(:,:),         &
                              matrix2scm(:,:,:,:),    &
                              matrix2scnlo(:,:)

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine set_momenta (pr,pp)

  integer,  intent(in) :: pr
  real(dp), intent(in) :: pp(0:,:)

  integer  :: i,ip,j,legs,lin,lout,i1,i2,i1min
  real(dp) :: p(0:size(pp,1)-1,size(pp,2)),                          &
              p0(0:size(pp,1)-1,size(pp,2)),                         &
              pIn(0:3),pOut(0:3),mOut,m,check,pv2,p0old,p0new,       &
              qmu(0:3),E,q(1:3),qs,p12(1:3),p12Xq,d3,delta,qtm,      &
              p1(1:3),p1Xq,p1q(1:3),p1t(1:3),p1ts,q1q(1:3),m1,d1,E1, &
              p2(1:3),p2Xq,p2q(1:3),p2t(1:3),p2ts,q2q(1:3),m2,d2,E2, &
              p1s,p2s

  p = pp

  lin  = prs(pr)%legsIn
  lout = prs(pr)%legsOut
  legs = lin + lout


  ! Check the number of external particles
  if (lin.lt.1) then
    momcheck = .false.
    call error_rcl('Wrong number of incoming particles for process.',&
                  where='set_momenta(internal)')
  endif
  if (lout.lt.1) then
    call error_rcl('Wrong number of outgoing particles for process.',&
                  where='set_momenta(internal)')
  endif
  if (legs.lt.2) then
    call error_rcl('Wrong number of external particles for process.',&
                  where='set_momenta(internal)')
  endif

  ! Check that the dimension of the first index of "p" coincides
  ! with the number of external particles
  if (size(p,2).ne.legs) then
    call error_rcl('Wrong phase-space point for process.',&
                  where='set_momenta(internal)')
  endif

  ! Check that all particles have positive energy larger than
  ! their mass
  if (momenta_checks) then
  do i = 1,legs
    if (prs(pr)%crosspr .gt. 0) then
      m = mONS(newleg(i,prs(pr)%crosspr),prs(pr)%crosspr)
    else
      m = mONS(newleg(i,pr),pr)
    end if
    if (p(0,i).le.0d0) then
      momcheck = .false.
      call warning_rcl('Some particle(s) have negative energy',&
                       where='set_momenta(internal)')
      call warning_rcl('All amplitudes are set to 0.')
      momenta = p
      return
    elseif (p(0,i).lt.m) then
      momcheck = .false.
      call warning_rcl('Some particle(s) have not enough energy',&
                       where='set_momenta(internal)')
      call warning_rcl('All amplitudes are set to 0.')
      momenta = p
      return
    endif
  enddo
  endif

  do j = 0,3
    pIn(j) = sum(p(j,1:lin)); pOut(j) = sum(p(j,1+lin:legs))
  enddo

  mOut = 0d0
  do i = lin+1,legs
    if (prs(pr)%crosspr .gt. 0) then
      m = mONS(newleg(i,prs(pr)%crosspr),prs(pr)%crosspr)
    else
      m = mONS(newleg(i,pr),pr)
    end if
    mOut = mOut + m
  enddo

  ! Check that the total incoming energy is sufficient to produce the
  ! on-shell outgoing particles
  if (momenta_checks) then
  if (pIn(0).lt.mOut) then
    momcheck = .false.
    call warning_rcl('Incoming energy not enough to produce' // &
                     'external particles', where='set_momenta(internal)')
    call warning_rcl('All amplitudes are set to 0.')
    momenta = p
    return
  endif
  endif

  momcheck = .true.
  if (allocated(momenta)) deallocate(momenta)
  allocate (momenta(0:3,legs))
  momenta = p

  if (.not. momenta_checks) then
    return
  end if

  ! Check the mass shell condition of external momenta
  do i = 1, legs
    if (prs(pr)%crosspr .gt. 0) then
      ip = prs(pr)%relperm(i)
      m = mONS(newleg(ip,prs(pr)%crosspr),prs(pr)%crosspr)
    else
      m = mONS(newleg(i,pr),pr)
    end if
    p0old = p(0,i)
    pv2 = p(1,i)*p(1,i) + p(2,i)*p(2,i) + p(3,i)*p(3,i)
    p0new = sqrt( pv2 + m*m )
    check = abs(p0old-p0new)/p0old
    if (check.gt.zerocut) then
      if (momenta_correction) then
        p(0,i) = p0new
        if (check.gt.zerocheck) then
          if (warnings.le.warning_limit) then
            warnings = warnings + 1
            call openOutput
            write(nx,*)
            write(nx,*) 'WARNING !!!'
            write(nx,'(1x,a,i2,a,i2,2a)') &
              'Particle ',i,' of process ',pr,' is not on the mass shell. ', &
              'Its energy is rescaled:'
            write(nx,*) 'OLD momentum:'
            write(nx,'(1x,a,i1,1x,a,1x,"(",f20.15,3(",",f21.15),")")') &
              'p',i,'=',p0old,p(1,i),p(2,i),p(3,i)
            write(nx,*) 'NEW momentum:'
            write(nx,'(1x,a,i1,1x,a,1x,"(",f20.15,3(",",f21.15),")")') &
              'p',i,'=',p(0,i),p(1,i),p(2,i),p(3,i)
            write(nx,'(1x,a,i1,a,i1,a,i1,a,e10.3)') &
              '(p',i,'^2-m',i,'^2)/E',i,'^2 =', &
              abs(p(0,i)**2 - pv2 - m*m)/p(0,i)**2
            write(nx,*)
            call toomanywarnings
          endif
        endif
      else
        if (warnings.le.warning_limit) then
          warnings = warnings + 1
          call openOutput
          write(nx,*)
          write(nx,*) 'ERROR: '
          write(nx,'(1x,a,i2,a,i2,2a)') &
            'Particle ',i,' of process ',pr,' is not on the mass shell. ', &
            'All amplitudes are set to 0'
          write(nx,*)
          call toomanywarnings
        endif
        momcheck = .false.
        return
      endif
    endif
  enddo

  do j = 0,3
    pIn(j) = sum(p(j,1:lin)); pOut(j) = sum(p(j,1+lin:legs))
  enddo

  ! Check conservation of the four-momentum
  check = 0d0
  do j = 0,3
    check = max(check,abs(pIn(j)-pOut(j))/abs(pIn(0)))
  enddo
  if (check.gt.zerocut) then
    if (momenta_correction) then
      if (lout.eq.1) then; i1min = 1
      else;                i1min = lin+1
      endif
      i1loop: do i1 = i1min,legs
      i2loop: do i2 = i1+1,legs
        if (lout.eq.1) then
          p1 = p(1:3,1)
          p2 = p(1:3,2)
          qmu = p(:,1) + p(:,2) - pIn(:) + pOut(:)
        elseif (lout.ge.2) then
          p1 = p(1:3,i1)
          p2 = p(1:3,i2)
          qmu = p(:,i1) + p(:,i2) - pOut(:) + pIn(:)
        endif
        E = qmu(0); q = qmu(1:3); qs = dot_product(q,q)
        ! Each vector v is written as v = vq + vt, where vq=v.q/qs*q
        p1Xq = dot_product(p1,q)
        p2Xq = dot_product(p2,q)
        p12 = p1 + p2;  p12Xq = dot_product(p12,q)
        ! The new 4-vectors are
        ! q1mu = (E1,q1), q1 = q1q + q1t; q2mu = (E2,q2), q2 = q2q + q2t
        ! For momentum conservation q1t + q2t = 0 => |q1t| = |q2t| = qtm
        ! I choose q1q = sigma*p1q, q2q = sigma*p2q => sigma = qs/p12.q
        if (qs.ne.0d0) then; q1q = p1Xq/p12Xq * q; q2q = p2Xq/p12Xq * q
        else;                q1q = 0d0;            q2q = 0d0
        endif
        ! d1 = m1^2 + |q1q|^2, d2 = m2^2 + |q2q|^2

        if (prs(pr)%crosspr .gt. 0) then
          if (lout.eq.1) then
            m1 = mONS(newleg(1,prs(pr)%crosspr),prs(pr)%crosspr)
            m2 = mONS(newleg(2,prs(pr)%crosspr),prs(pr)%crosspr)
          else
            m1 = mONS(newleg(i1,prs(pr)%crosspr),prs(pr)%crosspr)
            m2 = mONS(newleg(i2,prs(pr)%crosspr),prs(pr)%crosspr)
          end if
        else
          if (lout.eq.1) then
            m1 = mONS(newleg(1,pr),pr)
            m2 = mONS(newleg(2,pr),pr)
          else
            m1 = mONS(newleg(i1,pr),pr)
            m2 = mONS(newleg(i2,pr),pr)
          end if
        end if
        d1 = m1*m1 + dot_product(q1q,q1q)
        d2 = m2*m2 + dot_product(q2q,q2q)
        d3 = E**2
        delta = d1**2 + d2**2 + d3**2 - 2*d1*d2 - 2*d1*d3 - 2*d2*d3
        if ( lout.eq.1 .or. &
             ( d3+d1-d2.ge.0d0.and. &
               d3+d2-d1.ge.0d0.and. &
               delta.ge.0d0         ) ) then
          exit i1loop
        endif
      enddo i2loop
      enddo i1loop
      if (d3+d1-d2.lt.0d0.or.d3+d2-d1.lt.0d0.or.delta.lt.0d0) then
        if (warnings.le.warning_limit) then
          warnings = warnings + 1
          call openOutput
          write(nx,*)
          write(nx,*) 'ERROR: '
          write(nx,'(1x,a,i2,a)') &
            'The four-momentum is not conserved in process ',pr,':'
          write(nx,'(1x,a,1x,"(",f20.15,3(",",f21.15),")")') &
            'pIn  =',pIn(0),pIn(1),pIn(2),pIn(3)
          write(nx,'(1x,a,1x,"(",f20.15,3(",",f21.15),")")') &
            'pOut =',pOut(0),pOut(1),pOut(2),pOut(3)
          write(nx,*) 'All amplitudes are set to 0'
          write(nx,*)
          call toomanywarnings
          momcheck = .false.
          return
        endif
      endif
      ! q1A = E1 + qtm, q1B = E1 - qtm
      ! q2A = E2 + qtm, q2B = E2 - qtm
      ! Conditions are:
      ! q1A + q2B = E; q1B + q2A = E -> energy conservation
      ! q1A*q1B = d1;  q2A*q2B = d2  -> on-shell conditions
      ! I define x = q1A and get:
      ! E*x^2 + (d2-d1-E^2)*x + E*d1 = 0
      ! x+- = (d3 + d1 - d2 +- sq)/(2*E), sq = sqrt(delta)
      ! I choose:
      ! q1A = (d3 + d1 - d2 + sq)/(2*E) = x+
      ! q1B = d1/q1A = 2*d1*E/(d3 + d1 - d2 + sq) = x-
      ! I get
      ! q2A = (d3 + d2 - d1 + sq)/(2*E) = y+
      ! q2B = d2/q2A = 2*d2*E/(d3 + d2 - d1 + sq) = y-
      ! where y+- = (d3 + d2 - d1 +- sq)/(2*E)
      ! Then
      ! E1  = (q1A+q1B)/2 = (d3+d1-d2)/(2*E)
      ! E2  = (q2A+q2B)/2 = (d3+d2-d1)/(2*E)
      ! qtm = (q1A-q1B)/2 = (q2A-q2B)/2 = sq/(2*E)
      p0 = p
      E1 = (d3+d1-d2)/(2*E); E2 = (d3+d2-d1)/(2*E)
      qtm = sqrt(delta)/(2*E)
      p(0,i1) = E1;     p(0,i2) = E2
      if (qs.gt.0d0) then
        p1q = p1Xq/qs * q; p2q = p2Xq/qs * q
        p1t = p1 - p1q; p1ts = dot_product(p1t,p1t)
        p2t = p2 - p2q; p2ts = dot_product(p2t,p2t)
        p(1:3,i1) = q1q + qtm/sqrt(p1ts)*p1t
        p(1:3,i2) = q2q + qtm/sqrt(p2ts)*p2t
      else
        p1s = dot_product(p1,p1)
        p2s = dot_product(p2,p2)
        if (p1s.eq.0d0.and.p2s.eq.0d0) then
          p(1:3,i1) = 0d0
          p(1:3,i2) = 0d0
        elseif (p1s.ge.p2s) then
          p(1:3,i1) = + qtm/sqrt(p1s)*p1
          p(1:3,i2) = - qtm/sqrt(p1s)*p1
        else
          p(1:3,i1) = - qtm/sqrt(p2s)*p2
          p(1:3,i2) = + qtm/sqrt(p2s)*p2
        endif
      endif
      if (check.gt.zerocheck) then
        if (warnings.le.warning_limit) then
          warnings = warnings + 1
          call openOutput
          write(nx,*)
          write(nx,*) 'WARNING !!!'
          write(nx,'(1x,a,i2,a)') &
            'The four-momentum is not conserved in process ',pr,':'
          write(nx,'(1x,a,1x,"(",f20.15,3(",",f21.15),")")') &
            'pIn  =',pIn(0),pIn(1),pIn(2),pIn(3)
          write(nx,'(1x,a,1x,"(",f20.15,3(",",f21.15),")")') &
            'pOut =',pOut(0),pOut(1),pOut(2),pOut(3)
          if (lout.eq.1) then
            write(nx,*) &
              'The four-momentum of the two incoming particles ', &
              'is rescaled:'
          elseif (lout.ge.2) then
            write(nx,*) &
              'The four-momentum of two outgoing particles is rescaled:'
          endif
          write(nx,*) 'OLD momenta:'
          do i = 1, legs
            write(nx,'(1x,a,i1,3x,a,1x,"(",f20.15,3(",",f21.15),")")') &
              'p',i,'=',p0(0,i),p0(1,i),p0(2,i),p0(3,i)
          enddo
          write(nx,*) 'NEW momenta:'
          do i = 1, legs
            write(nx,'(1x,a,i1,3x,a,1x,"(",f20.15,3(",",f21.15),")")') &
              'p',i,'=',p(0,i),p(1,i),p(2,i),p(3,i)
          enddo
          do i = 1, legs
            pv2 = p(1,i)*p(1,i) + p(2,i)*p(2,i) + p(3,i)*p(3,i)

            if (prs(pr)%crosspr .gt. 0) then
              m = mONS(newleg(i,prs(pr)%crosspr),prs(pr)%crosspr)
            else
              m = mONS(newleg(i,pr),pr)
            end if
            write(nx,'(1x,a,i1,a,i1,a,i1,a,e10.3)') &
              '(p',i,'^2-m',i,'^2)/E',i,'^2 =', &
              abs(p(0,i)**2 - pv2 - m*m)/p(0,i)**2
          enddo
          do j = 0,3
            pIn(j) = sum(p(j,1:lin)); pOut(j) = sum(p(j,1+lin:legs))
          enddo
          write(nx,'(1x,a,1x,"(",f20.15,3(",",f21.15),")")') &
            'pIn  =',pIn(0),pIn(1),pIn(2),pIn(3)
          write(nx,'(1x,a,1x,"(",f20.15,3(",",f21.15),")")') &
            'pOut =',pOut(0),pOut(1),pOut(2),pOut(3)
          write(nx,*)
          call toomanywarnings
        endif
      endif
    else
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: '
      write(nx,'(1x,a,i2,a)') &
        'The four-momentum is not conserved in process ',pr,':'
      write(nx,'(1x,a,1x,"(",f20.15,3(",",f21.15),")")') &
        'pIn  =',pIn(0),pIn(1),pIn(2),pIn(3)
      write(nx,'(1x,a,1x,"(",f20.15,3(",",f21.15),")")') &
        'pOut =',pOut(0),pOut(1),pOut(2),pOut(3)
      write(nx,*) 'All amplitudes are set to 0'
      write(nx,*)
      call toomanywarnings
      momcheck = .false.
      return
    endif
  endif

  momenta = p

  end subroutine set_momenta

!------------------------------------------------------------------------------!

  subroutine compute_amplitude(prin,order)

  use memory_logger_rcl, only: log_memory
  use tables_rcl, only: pim
  use collier, only: SetDeltaUV_cll,SetDeltaIR_cll, &
                     SetMuIR2_cll,SetMuUV2_cll
  use input_rcl, only: get_mu_uv_rcl,get_mu_ir_rcl,       &
                       get_delta_uv_rcl,get_delta_ir_rcl, &
                       reset_ctcouplings_rcl

  integer,          intent(in) :: prin
  character(len=*), intent(in) :: order

  integer                    :: pr,n,i,ip,j,j1,j2,jd,k,e,ii,jj,kk,   &
                                jmax,kmax,s,w,se,                    &
                                legs,tl,tlm1,tlp1,fh,w0i,            &
                                imin,imax,bimin,bimax,ih1,           &
                                emin,c,npoint,semax(0:3),emax(0:3),  &
                                w0in(1:nDef-1),w0out,w1in,w1out,     &
                                riMaxIn,riMaxOut,rankInc,            &
                                b,locoef,ty,tyR,gs,cs,oi,pext,       &
                                mo,da,ti,leg,ran,lp,t,lmu,           &
                                cllaccuracy,cllerror,                &
                                leg1,leg2,leg3,leg4,leg5,leg6,leg7,lout
  integer, allocatable       :: w0l(:),gcch_tmp(:)
  logical                    :: computeNLO,x,last,ferlo,winit
  complex(dp)                :: m,m2,m2p,p2p,den,width,fac,wwTI
  complex (dp), allocatable  :: p(:,:),pl(:,:),ps(:),pp(:),psp(:),   &
                                momInv(:),ms(:),momVec(:,:),         &
                                TIri(:,:),TIriUV(:),ww0(:,:),        &
                                ww1(:,:,:),ww0out(:,:),ww1out(:,:,:)
  real (sp)                  :: timeTIin,timeTIout,timeTCin,timeTCout
  real (dp)                  :: fa,cllcritacc,check
  real (dp)                  :: deltaUV,deltaIR,deltaIR2,muUV,muIR

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
  else
    pr = prin
  end if
  if (log_mem) call log_memory('@compute_amplitude: begin')
  computeNLO = (order.eq.'NLO').and.(lpmax(pr).gt.0)

  if (.not.allocated(matrix)) then
    allocate (matrix(1:csMax,1:maxval(oiSize),1:maxval(cfTot),0:4,prTot))
    matrix = 0d0
  else
    matrix(1:pCsTot(pr),1:maxval(oiSize),1:cfTot(pr),0,prin) = cnul
    if (computeNLO) then
      matrix(1:pCsTot(pr),1:maxval(oiSize),1:cfTot(pr),1:4,prin) = cnul
    end if
  end if

  if (log_mem) call log_memory('@compute_amplitude: after allocate')

  if (.not.momcheck) return

  legs = prs(pr)%legs

  tlm1 = 2**(legs-1)
  tl   = 2**legs
  tlp1 = 2**(legs+1)

  if (computeNLO) then
    allocate (p(0:3,tlp1-1))
    allocate (pl(4,tlp1-1))
    allocate (psp(tl-1))
  else
    allocate (p(0:3,tl-1))
    allocate (pl(4,tl-1))
  endif
  allocate (ps(tl-1))
  allocate (pp(tl-1))

  ! build external momenta (all "p" are incoming) and introduce:
  !
  !   pl(1) = p(0)+p(3)
  !   pl(2) = p(0)-p(3)
  !   pl(3) = p(1)+i*p(2)
  !   pl(4) = p(1)-i*p(2)
  !
  !   ps = p(0)*p(0)-p(1)*p(1)-p(2)*p(2)-p(3)*p(3)
  !      = pl(1)*pl(2)-pl(3)*pl(4)

  p    = cnul
  pl   = cnul
  ps   = cnul

  if (prs(prin)%crosspr .ne. 0) then
    do i = 1,prs(prin)%legs
      ip = prs(prin)%relperm(i)
      j = 2**(newleg(ip,pr)-1)
      if (i .gt. prs(pr)%legsIn) then
        p(:,j) = -cmplx(momenta(:,i),kind=dp)
      else
        p(:,j) = cmplx(momenta(:,i),kind=dp)
      end if
    enddo
  else
    do i = 1,prs(pr)%legsIn; j = 2**(newleg(i,pr)-1)
      p(:,j) = cmplx(momenta(:,i),kind=dp)
    enddo
    do i = prs(pr)%legsIn+1,legs; j = 2**(newleg(i,pr)-1)
      p(:,j) = - cmplx(momenta(:,i),kind=dp)
    enddo
  end if

  do i= 1, legs; j = 2**(i-1)
    pl(1,j) = p(0,j) + p(3,j)
    pl(2,j) = p(0,j) - p(3,j)
    pl(3,j) = p(1,j) + cima*p(2,j)
    pl(4,j) = conjg(pl(3,j))
    ! cmONS2 is the squared of the on-shell mass of the external
    ! particle:
    ! - it is 0 if the particle is marked as light
    ! - it is the squared of the input mass if the particle is not
    !   marked as light
    ps(j) = cmONS2(i,pr)
    jd = tl - 1 - j
    p (:,jd) = - p (:,j)
    pl(:,jd) = - pl(:,j)
    ps(  jd) =   ps(  j)
  enddo

  bin: do j = 3,tl-2
    i = levelLeg(j)
    if (i.eq.1) then
      cycle bin
    elseif (i.le.legs/2) then
      j1 = firstNumbers(j,1)
      j2 = sum(firstNumbers(j,2:i))
      p (:,j) = p(:,j1) + p(:,j2)
      pl(1,j) = p(0,j) + p(3,j)
      pl(2,j) = p(0,j) - p(3,j)
      pl(3,j) = p(1,j) + cima*p(2,j)
      pl(4,j) = conjg(pl(3,j))
      pp(j) =           &
      + p(0,j1)*p(0,j2) &
      - p(1,j1)*p(1,j2) &
      - p(2,j1)*p(2,j2) &
      - p(3,j1)*p(3,j2)
      ps(j) = ps(j1) + ps(j2) + 2d0*pp(j)
      jd = tl - 1 - j
      p (:,jd) = - p (:,j)
      pl(:,jd) = - pl(:,j)
      ps(  jd) =   ps(  j)
    endif
  enddo bin

  if (computeNLO) then

    if (cache_mode .eq. 2) then
      if (activeCache .lt. 1 .or. activeCache .gt. nCacheGlobal) then
        call error_rcl('Cache not properly set (out of boundary). ' // &
                       'Use switch_global_cache_rcl.', &
                       where='compute_amplitude')
      end if
      nprCall = nprCall + 1
      if (.not. allocated(gcch(activeCache)%hist)) then
        ! initialize call sequence history
        allocate(gcch(activeCache)%hist(1))
        gcch(activeCache)%hist(nprCall) = pr
        gcch(activeCache)%constructed = .false.
      else if (.not. gcch(activeCache)%constructed) then
        ! register call into sequence history
        allocate(gcch_tmp(size(gcch(activeCache)%hist)))
        gcch_tmp(:) = gcch(activeCache)%hist
        deallocate(gcch(activeCache)%hist)
        allocate(gcch(activeCache)%hist(size(gcch(activeCache)%hist) + 1))
        gcch(activeCache)%hist(:size(gcch(activeCache)%hist)-1) = gcch_tmp(:)
        gcch(activeCache)%hist(nprCall) = pr
        deallocate(gcch_tmp)
      else
        ! check against previous call
        if (nprCall .gt. size(gcch(activeCache)%hist)) then
          call error_rcl('Mismatch in NLO call sequence. ' //                       &
                         'Too many processes computed. #call ' //                   &
                         trim(adjustl(int_to_str(nprCall))) //                      &
                         ' in #cache ' //                                           &
                         trim(adjustl(int_to_str(activeCache))) //                  &
                         ' has only ' //                                            &
                         trim(adjustl(int_to_str(size(gcch(activeCache)%hist)))) // &
                         ' processes registered. ' //                               &
                         'Global cache not properly used, stopping here.',          &
                         where='compute_amplitude')
        else if (gcch(activeCache)%hist(nprCall) .ne. pr) then
          call error_rcl('Mismatch in NLO call sequence. ' //                          &
                         'Wrong process computed for #call ' //                        &
                         trim(adjustl(int_to_str(nprCall))) //                         &
                         ' in #cache ' //                                              &
                         trim(adjustl(int_to_str(activeCache))) //                     &
                         '. Expected process ' //                                      &
                         trim(adjustl(int_to_str(gcch(activeCache)%hist(nprCall)))) // &
                         ' but ' //  trim(adjustl(int_to_str(pr))) //                  &
                         ' computed. ' //                                              &
                         'Global cache not properly used, stopping here.',             &
                         where='compute_amplitude')
        end if
      end if
    end if

    ! psp: squared momenta for tensor integrals
    psp = ps
    do j = 3,tlm1-1
      if (defresbin(j,pr)) then
        check = abs(psp(j)-pspbin(j,pr))/pspbin(j,pr)
        if (check.gt.zerocheck) then
          call error_rcl('resonant particle not on the mass-shell', &
                         where='compute_amplitude')
        end if
        psp(j) = pspbin(j,pr)
        psp(tl-1-j) = pspbin(j,pr)
      end if
      ! cpsp is the squared mass of the external particle; it is set
      ! to IR-regulator for massless fermions when regafer=T
    enddo

    do i = 1, legs;  j = 2**(i-1)
      ! cmREG2 is the squared of the input mass of the external
      ! particle (if the particle is marked as light, mREG2 is used
      ! as IR-regulator in loop propagators only)
      psp(j) = cmREG2(i,pr)
      psp(tl-1-j) = psp(j)
    enddo

    ! additional definitions for loop legs (offsets)
    do i= 1, tl-1
      p (:,tl+i) = p (:,i)
      pl(:,tl+i) = pl(:,i)
    enddo

    call cpu_time (timeTIin)

    if (dynamic_settings .ge. 1) then
      call get_delta_uv_rcl(deltaUV)
      call SetDeltaUV_cll(deltaUV)
      call get_mu_uv_rcl(muUV)
      call SetMuUV2_cll(muUV**2)
      call get_delta_ir_rcl(deltaIR,deltaIR2)
      call SetDeltaIR_cll(deltaIR,deltaIR2)
      call get_mu_ir_rcl(muIR)
      call SetMuIR2_cll(muIR**2)
      if (computeNLO) call reset_ctcouplings_rcl
    endif

    allocate (momInv(0:legs*(legs-1)/2))
    allocate (TIri(  0:ritiMax(pr),tiTot(pr)))

    !call export_integrals
    !stop

    do i = 1,tiTot(pr)

      if (cache_mode .eq. 1) then
        if (mod(i-1,tiCache(pr)).eq.0) then
          n = nCacheTot(pr-1) + (i-1)/tiCache(pr) + 1
          call InitEvent_cll(n)
        endif
      end if

      leg = legsti(i,pr)
      ran = rankti(i,pr)

      allocate (ms(0:legs-1)); ms= cnul
      do j= 1, leg
        ms(j-1) = get_cmass2_reg_mdl(vmti(j,i,pr)) ! internal
      end do

      allocate (TIriUV(0:riMax(ran)))

      select case (leg)

      case (1) ! tadpoles do not require momenta nor invariants

        call TNten_cll(TIri(  0:riMax(ran),i),& ! output
                       TIriUV(0:riMax(ran)),  & ! output
                       ms(0:0),leg,ran)         ! input

      case default ! arguments for more than one internal leg

        allocate (momVec(0:3,0:legs-1))

        ! momenta
        imin             = 1
        imax             = leg-1
        ii = 0
        do k = 1,legs
          if (momsti(k,i,pr).eq.imin) ii = ii + 2**(k-1)
        enddo
        momVec(0,  imin) = + p(0,ii)
        momVec(1:3,imin) = - p(1:3,ii)
        do j = imin+1, imax
          ii = 0
          do k = 1,legs
            if (momsti(k,i,pr).eq.j) ii = ii + 2**(k-1)
          enddo
          momVec(0,  j) = momVec(0,  j-1) + p(0,  ii)
          momVec(1:3,j) = momVec(1:3,j-1) - p(1:3,ii)
        enddo

        ! invariants
        bimin = 1
        bimax = leg*(leg-1)/2
        jmax = leg/2   ! leg=2*n -> jmax=n;     leg=2*n+1 -> jmax=n
        kmax = leg - 1 ! leg=2*n -> kmax=2*n-1; leg=2*n+1 -> kmax=2*n
        ii = 0
        do j = 1,jmax
          if (2*j.eq.leg) kmax = leg/2 - 1 ! leg=2*n & j=n -> kmax=n
          do k = 0,kmax
            jj = 0
            kk = 0
            do n = 1,legs
              if (momsti(n,i,pr).gt.0) then
                if (momsti(n,i,pr).le.mod(k+j,leg)) jj = jj + 2**(n-1)
                if (momsti(n,i,pr).le.k) kk = kk + 2**(n-1)
              endif
            enddo
            ii = ii + 1
            momInv(ii) = psp(abs(jj-kk))
          enddo
        enddo

        ! Test setup: Offshell Selfenergy.
        ! The momentum invariant needs to be assigned manually, otherwise it is
        ! assigned by onshell conditions.
        if (selfenergy_offshell_setup) then
          momInv(1) = momVec(0,1)**2 - momVec(1,1)**2- momVec(2,1)**2- momVec(3,1)**2
        end if

        call TNten_cll(TIri(  0:riMax(ran),i), & ! output
                       TIriUV(0:riMax(ran)),   & ! output
                       momVec(0:3,imin:imax),  & ! input
                       momInv(bimin:bimax),    & ! input
                       ms(0:imax),leg,ran)       ! input
        deallocate (momVec)

      end select

      deallocate (TIriUV)
      deallocate (ms)

    enddo ! tensor integrals evaluated

    deallocate (momInv)

    call GetAccFlag_cll(cllaccuracy)
    select case (cllaccuracy)
    case (-2)
      call warning_rcl('Bad phase-space point', &
                       where='compute_amplitude')
      call GetCritAcc_cll(cllcritacc)
      write(nx,'(1x,a,g7.1)') &
        'Accuracy of tensor integrals less than ',cllcritacc
      write(nx,*)
      if (writeMat+writeMat2.eq.0) then
        do i = 1,legs
          write(nx,'(2x,a,i1,1x,a,1x,"(",f15.10,3(",",f16.10),")")') &
            'p',i,'=',momenta(0:3,i)
        enddo
        write(nx,*)
      endif
    end select

    call GetErrFlag_cll(cllerror)
    if (cllerror .lt. -8) then
      call error_rcl('Encounterted collier error.', &
                     where='compute_amplitude(internal)')
    endif

    call cpu_time (timeTIout)
    timeTI(prin) = timeTI(prin) + timeTIout - timeTIin

  endif

  call cpu_time (timeTCin)

  allocate (ww0(0:3,w0Tot(pr)))
  allocate (ww0out(0:3,0:modaTot(pr)))
  if (computeNLO) then
    allocate (ww1out(0:3,0:ritiMax(pr),0:modaTot(pr)))
    ww1out(0:3,:,:) = 0d0
  endif

  do w = 1,w0eTot(pr)
    call definewp (parw0e(w,pr),                                 &
                   p(:,binw0e(w,pr)),pl(:,binw0e(w,pr)),         &
                   mONS(legw0e(w,pr),pr),helw0e(w,pr),ww0(:,w))
    if (longitudinal .eq. 0) cycle
    if (parw0e(w,pr) .ne. get_particle_id_mdl('A') .and. &
        parw0e(w,pr) .ne. get_particle_id_mdl('g')) cycle
    if (longitudinal .eq. 111 .or. &
        longitudinal .eq. oldleg(legw0e(w,pr),pr)) then
      ww0(:,w) = p(:,binw0e(w,pr))/p(0,binw0e(w,pr))
    end if
  end do
  allocate (w0l(cfTot(pr)))
  w0l(1:cfTot(pr)) = w0last(he(legs,1:cfTot(pr),pr),pr)

  emax(0) = tlm1-1
  if (computeNLO) emax(1:3) = tl-1


  ! tree branches
  semax(0:1) = 0
  semax(2:3) = 1  ! For CT-and R2-amplitudes the mother and daughter branches
                  ! without self-energies need to be evaluated before the ones
                  ! with self-energies, because a mother-branch with se can have
                  ! a daughte-branch w/o se as incoming current.

  ! Index "c" combines the indices lp, t and fh
  cloop0: do c = 1, c0EffMax(pr)

    lp = c0TOlp(c, pr)
    if ((lp .gt. 0) .and. (.not. computeNLO)) cycle cloop0

    configloop0: do i = 1, cfTot(pr)

      w0i = w0l(i)
      ! binary loop starting from the lowest outgoing binary to
      eloop0: do e = 1, emax(lp)

        seloop: do se = 0,semax(lp)
        if (pm(pr)%bm0(e,i,c)%bmin .ne. 0) then

          bm0loop: do b = pm(pr)%bm0(e,i,c)%bmin, pm(pr)%bm0(e,i,c)%bmax

            s    = pm(pr)%tree_m_maps(b)%sb
            lout = sum(binsm0(:nDef-1,s,pr))

            ! self-energy ct topology not evaluated in the first run (se=0)
            if (se .eq. 0 .and. binsm0(1,s,pr) .eq. lout) cycle
            if (se .eq. 1 .and. binsm0(1,s,pr) .ne. lout) cycle

            mo = mosm0(s,pr)
            npoint = binsm0(nDef,s,pr)

            x    = xsm0(s,pr)
            cs   = cssm0(s,pr); last = (cs.ne.0)
            if (last) gs = gssm0(s,pr)

            w0in(1)   = pm(pr)%tree_m_maps(b)%w_in(1)
            if (npoint .eq. 3) then
              w0in(2) = pm(pr)%tree_m_maps(b)%w_in(2)
            else if (npoint .eq. 4) then
              w0in(2) = pm(pr)%tree_m_maps(b)%w_in(2)
              w0in(3) = pm(pr)%tree_m_maps(b)%w_in(3)
            else if (npoint .eq. 5) then
              w0in(2) = pm(pr)%tree_m_maps(b)%w_in(2)
              w0in(3) = pm(pr)%tree_m_maps(b)%w_in(3)
              w0in(4) = pm(pr)%tree_m_maps(b)%w_in(4)
              w0in(5) = pm(pr)%tree_m_maps(b)%w_in(5)
            else if (npoint .eq. 6) then
              w0in(2) = pm(pr)%tree_m_maps(b)%w_in(2)
              w0in(3) = pm(pr)%tree_m_maps(b)%w_in(3)
              w0in(4) = pm(pr)%tree_m_maps(b)%w_in(4)
              w0in(5) = pm(pr)%tree_m_maps(b)%w_in(5)
            else if (npoint .eq. 7) then
              w0in(2) = pm(pr)%tree_m_maps(b)%w_in(2)
              w0in(3) = pm(pr)%tree_m_maps(b)%w_in(3)
              w0in(4) = pm(pr)%tree_m_maps(b)%w_in(4)
              w0in(5) = pm(pr)%tree_m_maps(b)%w_in(5)
              w0in(6) = pm(pr)%tree_m_maps(b)%w_in(6)
            else if (npoint .eq. 8) then
              w0in(2) = pm(pr)%tree_m_maps(b)%w_in(2)
              w0in(3) = pm(pr)%tree_m_maps(b)%w_in(3)
              w0in(5) = pm(pr)%tree_m_maps(b)%w_in(5)
              w0in(4) = pm(pr)%tree_m_maps(b)%w_in(4)
              w0in(6) = pm(pr)%tree_m_maps(b)%w_in(6)
              w0in(7) = pm(pr)%tree_m_maps(b)%w_in(7)
            end if
            w0out = pm(pr)%tree_m_maps(b)%w_out
            winit = pm(pr)%tree_m_maps(b)%w_init
            ty    = pm(pr)%tree_m_maps(b)%ty
            tyR   = pm(pr)%tree_m_maps(b)%tyR
            pext  = pm(pr)%tree_m_maps(b)%pext

            if (dynamic_settings .ge. 1 .and. x .and. lp .eq. CTBranch) then
              do j = 1, size(cosm0(s,pr)%c)
                cosm0(s,pr)%val(j) = get_coupling_value_mdl(cosm0(s,pr)%c(j))*cosm0(s,pr)%factor
                cosm0(s,pr)%valR(j) = -cima*cosm0(s,pr)%val(j)
              end do
            else if (dynamic_settings .ge. 2) then
              do j = 1, size(cosm0(s,pr)%c)
                cosm0(s,pr)%val(j) = get_coupling_value_mdl(cosm0(s,pr)%c(j))*cosm0(s,pr)%factor
                cosm0(s,pr)%valR(j) = -cima*cosm0(s,pr)%val(j)
              end do
            end if

            if (pext .eq. 2) then
              if (parsm0(s,pr) .eq. get_particle_id_mdl('Z')) then
                width = get_particle_width_mdl(get_particle_id_mdl("G0"))
                if (abs(width) .gt. zerocut) then
                  call warning_rcl('The rxi-gauge with finite widths is not tested.')
                end if
              else if (parsm0(s,pr) .eq. get_particle_id_mdl('W+') .or. &
                       parsm0(s,pr) .eq. get_particle_id_mdl('W-')) then
                width = get_particle_width_mdl(get_particle_id_mdl("G+"))
                if (abs(width) .gt. zerocut) then
                  call warning_rcl('The rxi-gauge with finite widths is not tested.')
                end if
              else
                write (*,*) "Using pext for another particle " // &
                            "than Z or W. fout:", parsm0(s,pr)
                stop
              end if
            end if

            ! m(m2) is the mass(squared) of the particle and is not modified by pext
            m2 = cosm0(s,pr)%m2
            m = cosm0(s,pr)%m

            if (defresbin(lout,pr)) then
              width = get_particle_width_mdl(parsm0(s,pr))
              m2p = m2 - cima*width*m
            else
              m2p = m2
            end if
            if (defp2bin(lout,pr)) then
              p2p = p2bin(lout,pr)
            else
              p2p = ps(lout)
            end if
            den = p2p-m2p

            select case (npoint)
            case (2)
              leg1 = binsm0(1,s,pr)
              if (tyR .eq. 0 .or. (.not. use_recola_base)) then
                ww0out(:,mo) = cnul
                call compute_tree_mdl(ty,cosm0(s,pr)%val,    &
                      ww0(:,w0in(1)), p(:,leg1), pl(:,leg1), &
                      den, m, ww0out(:,mo), last )
              else
                call tree2(ps(lout),p(:,leg1),pl(:,leg1),m,den,  &
                           cosm0(s,pr)%valR,tyR,ww0(:,w0in(1)),ww0out(:,mo))
              end if
            case (3)
              leg1 = binsm0(1,s,pr)
              leg2 = binsm0(2,s,pr)
              if (tyR .eq. 0 .or. (.not. use_recola_base)) then
                ww0out(:,mo) = cnul
                call compute_tree_mdl(ty,cosm0(s,pr)%val,    &
                      ww0(:,w0in(1)), p(:,leg1), pl(:,leg1), &
                      ww0(:,w0in(2)), p(:,leg2), pl(:,leg2), &
                      den, m, ww0out(:,mo), last )
              else
                call tree3(last,p(:,leg1),p(:,leg2),                         &
                           pl(:,leg1),pl(:,leg2),m,den,cosm0(s,pr)%valR,tyR, &
                           ww0(:,w0in(1)),ww0(:,w0in(2)),ww0out(:,mo))
              end if
            case (4)
              leg1 = binsm0(1,s,pr)
              leg2 = binsm0(2,s,pr)
              leg3 = binsm0(3,s,pr)
              if (tyR .eq. 0 .or. (.not. use_recola_base)) then
                ww0out(:,mo) = cnul
                call compute_tree_mdl(ty,cosm0(s,pr)%val,      &
                      ww0(:,w0in(1)), p(:,leg1), pl(:,leg1), &
                      ww0(:,w0in(2)), p(:,leg2), pl(:,leg2), &
                      ww0(:,w0in(3)), p(:,leg3), pl(:,leg3), &
                      den, m, ww0out(:,mo), last )
              else
                call tree4(last,p(:,leg1),p(:,leg2),p(:,leg3),                 &
                           pl(:,leg1),pl(:,leg2),pl(:,leg3),                   &
                           m,den,cosm0(s,pr)%valR,tyR,                         &
                           ww0(:,w0in(1)),ww0(:,w0in(2)),ww0(:,w0in(3)), &
                           ww0out(:,mo))
              end if
            case (5)
              leg1 = binsm0(1,s,pr)
              leg2 = binsm0(2,s,pr)
              leg3 = binsm0(3,s,pr)
              leg4 = binsm0(4,s,pr)
              if (tyR .eq. 0 .or. (.not. use_recola_base)) then
                ww0out(:,mo) = cnul
                call compute_tree_mdl(ty,cosm0(s,pr)%val,      &
                      ww0(:,w0in(1)), p(:,leg1), pl(:,leg1), &
                      ww0(:,w0in(2)), p(:,leg2), pl(:,leg2), &
                      ww0(:,w0in(3)), p(:,leg3), pl(:,leg3), &
                      ww0(:,w0in(4)), p(:,leg4), pl(:,leg4), &
                      den, m, ww0out(:,mo), last )
              else
                call tree5(last,p(:,leg1),p(:,leg2),p(:,leg3),p(:,leg4),       &
                           pl(:,leg1),pl(:,leg2),pl(:,leg3),pl(:,leg4),        &
                           m,den,cosm0(s,pr)%valR,tyR,                         &
                           ww0(:,w0in(1)),ww0(:,w0in(2)),ww0(:,w0in(3)), &
                           ww0(:,w0in(4)),ww0out(:,mo))
              end if
            case (6)
              leg1 = binsm0(1,s,pr)
              leg2 = binsm0(2,s,pr)
              leg3 = binsm0(3,s,pr)
              leg4 = binsm0(4,s,pr)
              leg5 = binsm0(5,s,pr)
              if (tyR .eq. 0 .or. (.not. use_recola_base)) then
                ww0out(:,mo) = cnul
                call compute_tree_mdl(ty,cosm0(s,pr)%val,      &
                      ww0(:,w0in(1)), p(:,leg1), pl(:,leg1), &
                      ww0(:,w0in(2)), p(:,leg2), pl(:,leg2), &
                      ww0(:,w0in(3)), p(:,leg3), pl(:,leg3), &
                      ww0(:,w0in(4)), p(:,leg4), pl(:,leg4), &
                      ww0(:,w0in(5)), p(:,leg5), pl(:,leg5), &
                      den, m, ww0out(:,mo), last )
               else
                call tree6(last,p(:,leg1),p(:,leg2),p(:,leg3),p(:,leg4),p(:,leg5), &
                           pl(:,leg1),pl(:,leg2),pl(:,leg3),pl(:,leg4),pl(:,leg5), &
                           m,den,cosm0(s,pr)%valR,tyR,                             &
                           ww0(:,w0in(1)),ww0(:,w0in(2)),ww0(:,w0in(3)),           &
                           ww0(:,w0in(4)),ww0(:,w0in(5)),ww0out(:,mo))
              end if
            case (7)
              leg1 = binsm0(1,s,pr)
              leg2 = binsm0(2,s,pr)
              leg3 = binsm0(3,s,pr)
              leg4 = binsm0(4,s,pr)
              leg5 = binsm0(5,s,pr)
              leg6 = binsm0(6,s,pr)
              ww0out(:,mo) = cnul
              call compute_tree_mdl(ty,cosm0(s,pr)%val,      &
                    ww0(:,w0in(1)), p(:,leg1), pl(:,leg1), &
                    ww0(:,w0in(2)), p(:,leg2), pl(:,leg2), &
                    ww0(:,w0in(3)), p(:,leg3), pl(:,leg3), &
                    ww0(:,w0in(4)), p(:,leg4), pl(:,leg4), &
                    ww0(:,w0in(5)), p(:,leg5), pl(:,leg5), &
                    ww0(:,w0in(6)), p(:,leg6), pl(:,leg6), &
                    den, m, ww0out(:,mo), last)
            case (8)
              leg1 = binsm0(1,s,pr)
              leg2 = binsm0(2,s,pr)
              leg3 = binsm0(3,s,pr)
              leg4 = binsm0(4,s,pr)
              leg5 = binsm0(5,s,pr)
              leg6 = binsm0(6,s,pr)
              leg7 = binsm0(7,s,pr)
              ww0out(:,mo) = cnul
              call compute_tree_mdl(ty,cosm0(s,pr)%val,      &
                    ww0(:,w0in(1)), p(:,leg1), pl(:,leg1), &
                    ww0(:,w0in(2)), p(:,leg2), pl(:,leg2), &
                    ww0(:,w0in(3)), p(:,leg3), pl(:,leg3), &
                    ww0(:,w0in(4)), p(:,leg4), pl(:,leg4), &
                    ww0(:,w0in(5)), p(:,leg5), pl(:,leg5), &
                    ww0(:,w0in(6)), p(:,leg6), pl(:,leg6), &
                    ww0(:,w0in(7)), p(:,leg7), pl(:,leg7), &
                    den, m, ww0out(:,mo), last)
            case default
              call error_rcl('Tree current with npoint not in [2..8].',&
                             where='compute_amplitude(internal)')
            end select

            if (last) then
              wwTI = sum(ww0out(:,mo)*ww0(:,w0i))
              matrix(cs,gs,i,lp,prin) = matrix(cs,gs,i,lp,prin) + wwTI
            else
              if (winit) then
                ww0(:,w0out) = ww0out(:,mo)
              else
                ww0(:,w0out) = ww0(:,w0out) + ww0out(:,mo)
              endif
            endif

          enddo bm0loop

        endif

        if (pm(pr)%bd0(e,i,c)%bmin .ne. 0) then

          bd0loop: do b = pm(pr)%bd0(e,i,c)%bmin,pm(pr)%bd0(e,i,c)%bmax

            s = pm(pr)%tree_d_maps(b)%sb

            ! self-energy ct topology not evaluated in the first run (se=0)
            if (se .eq. 0 .and. sesd0(s,pr)) cycle
            if (se .eq. 1 .and. (.not. sesd0(s,pr))) cycle

            da = dasd0(s,pr)
            fa = facsd0(s,pr)
            cs = cssd0(s,pr)
            last = cs .ne. 0
            if (last) gs = gssd0(s,pr)

            w0out = pm(pr)%tree_d_maps(b)%w_out
            winit = pm(pr)%tree_d_maps(b)%w_init

            if (last) then
              wwTI = sum(ww0out(:,da)*ww0(:,w0i))
              matrix(cs,gs,i,lp,prin) = matrix(cs,gs,i,lp,prin) + fa * wwTI
            else
              if (winit) then
                ww0(:,w0out) = fa * ww0out(:,da)
              else
                ww0(:,w0out) = ww0(:,w0out) + fa * ww0out(:,da)
              endif
            endif

          enddo bd0loop

        endif

        enddo seloop

      enddo eloop0

    enddo configloop0

  enddo cloop0

  deallocate (w0l)

  ! loop branches
  if (computeNLO) then
    lp = 1

    ! Index "c" combine the indices t, fh and ih1
    cloop: do c = 1,cEffMax(pr)

      t   = cTOt  (c,pr)
      fh  = cTOfh (c,pr)
      ih1 = cTOih1(c,pr)

      locoef = loopCoef(t,pr)

      allocate (ww1(0:3,0:riwMax(c,pr),0:w1TotMax(c,pr)))

      if (ih1.eq.0) then
        emin = tlp1-1
      else
        emin = tl + (2*ih1-1)
      endif

      lmuloop: do lmu = minlmu(fh,t), maxlmu(fh,t)

        ww1(:,0,1) = cnul
        ww1(lmu,0,1) = cone

        configloop1: do i = 1, cfTot(pr)

          eloop1: do e = emin,tlp1-1,2

            if (pm(pr)%bm1(e,i,c)%bmin.ne.0) then

              bm1loop: do b = pm(pr)%bm1(e,i,c)%bmin,pm(pr)%bm1(e,i,c)%bmax


                s = pm(pr)%loop_m_maps(b)%sb
                mo = mosm1(s,pr)
                lout = sum(binsm1(:nDef-1,s,pr))
                npoint = binsm1(nDef,s,pr)
                riMaxIn = riMax(rankInsm1(s,pr))
                riMaxOut = riMax(rankOutsm1(s,pr))
                rankInc  = rankOutsm1(s,pr)-rankInsm1(s,pr)
                cs = cssm1(s,pr)
                last = (cs.ne.0)
                if (last) then
                  ferlo = ferloopsm1(s,pr)
                  gs = gssm1(s,pr)
                  ti = tism1(s,pr)
                endif

                w1in = pm(pr)%loop_m_maps(b)%w_in(1)
                w0in(2) = pm(pr)%loop_m_maps(b)%w_in(2)
                if (npoint .eq. 4) then
                  w0in(3) = pm(pr)%loop_m_maps(b)%w_in(3)
                else if (npoint .eq. 5) then
                  w0in(3) = pm(pr)%loop_m_maps(b)%w_in(3)
                  w0in(4) = pm(pr)%loop_m_maps(b)%w_in(4)
                  w0in(5) = pm(pr)%loop_m_maps(b)%w_in(5)
                else if (npoint .eq. 6) then
                  w0in(3) = pm(pr)%loop_m_maps(b)%w_in(3)
                  w0in(4) = pm(pr)%loop_m_maps(b)%w_in(4)
                  w0in(5) = pm(pr)%loop_m_maps(b)%w_in(5)
                else if (npoint .eq. 7) then
                  w0in(3) = pm(pr)%loop_m_maps(b)%w_in(3)
                  w0in(4) = pm(pr)%loop_m_maps(b)%w_in(4)
                  w0in(5) = pm(pr)%loop_m_maps(b)%w_in(5)
                  w0in(6) = pm(pr)%loop_m_maps(b)%w_in(6)
                else if (npoint .eq. 8) then
                  w0in(3) = pm(pr)%loop_m_maps(b)%w_in(3)
                  w0in(4) = pm(pr)%loop_m_maps(b)%w_in(4)
                  w0in(5) = pm(pr)%loop_m_maps(b)%w_in(5)
                  w0in(6) = pm(pr)%loop_m_maps(b)%w_in(6)
                  w0in(7) = pm(pr)%loop_m_maps(b)%w_in(7)
                end if
                w1out = pm(pr)%loop_m_maps(b)%w_out
                winit = pm(pr)%loop_m_maps(b)%w_init
                ty = pm(pr)%loop_m_maps(b)%ty
                tyR = pm(pr)%loop_m_maps(b)%tyR
                m2 = cosm1(s,pr)%m2
                m = cosm1(s,pr)%m

                if (dynamic_settings .ge. 2) then
                  do j = 1, size(cosm1(s,pr)%c)
                    cosm1(s,pr)%val(j) = get_coupling_value_mdl(cosm1(s,pr)%c(j))*cosm1(s,pr)%factor
                    cosm1(s,pr)%valR(j) = -cima*cosm1(s,pr)%val(j)
                  end do
                end if

                select case (npoint)
                case (3)
                  leg1 = binsm1(1,s,pr)
                  leg2 = binsm1(2,s,pr)
                  if (tyR .eq. 0 .or. (.not. use_recola_base)) then
                    ww1out(:,0:riMaxOut,mo) = cnul
                    call compute_loop_mdl(ty,cosm1(s,pr)%val,                   &
                                    ww1(:,0:riMaxIn,w1in),p(:,leg1),pl(:,leg1), &
                                    ww0(:,w0in(2)),p(:,leg2),pl(:,leg2),        &
                                    m,ww1out(:,0:riMaxOut,mo),                  &
                                    riMaxIn,riMaxOut,rankInc)
                  else
                    ! need to initialize coefficients. FirstRI doesn't work for
                    ! rank 2 increase (or higher) for the moment.
                    if (rankInc .ge. 2) then
                      ww1out(:,0:riMaxOut,mo) = cnul
                    end if
                    call loop3 (riMaxIn,riMaxOut,p(:,leg1),p(:,leg2), &
                                pl(:,leg1),pl(:,leg2),m,              &
                                cosm1(s,pr)%valR,tyR,                 &
                                ww1(:,0:riMaxIn,w1in),ww0(:,w0in(2)), &
                                ww1out(:,0:riMaxOut,mo))
                  end if
                case (4)
                  leg1 = binsm1(1,s,pr)
                  leg2 = binsm1(2,s,pr)
                  leg3 = binsm1(3,s,pr)
                  if (tyR .eq. 0 .or. (.not. use_recola_base)) then
                    ww1out(:,0:riMaxOut,mo) = cnul
                    call compute_loop_mdl(ty,cosm1(s,pr)%val,                   &
                                    ww1(:,0:riMaxIn,w1in),p(:,leg1),pl(:,leg1), &
                                    ww0(:,w0in(2)),p(:,leg2),pl(:,leg2),        &
                                    ww0(:,w0in(3)),p(:,leg3),pl(:,leg3),        &
                                    m,ww1out(:,0:riMaxOut,mo),                  &
                                    riMaxIn,riMaxOut,rankInc)
                  else
                    if (rankInc .ge. 2) then
                      ww1out(:,0:riMaxOut,mo) = cnul
                    end if
                    call loop4(riMaxIn,riMaxOut,                           &
                               p(:,leg1),p(:,leg2),p(:,leg3),              &
                               pl(:,leg1),pl(:,leg2),pl(:,leg3),m,         &
                               cosm1(s,pr)%valR,tyR,ww1(:,0:riMaxIn,w1in), &
                               ww0(:,w0in(2)),ww0(:,w0in(3)),              &
                               ww1out(:,0:riMaxOut,mo))
                  end if
                case (5)
                  leg1 = binsm1(1,s,pr)
                  leg2 = binsm1(2,s,pr)
                  leg3 = binsm1(3,s,pr)
                  leg4 = binsm1(4,s,pr)
                  ww1out(:,0:riMaxOut,mo) = cnul
                  call compute_loop_mdl(ty,cosm1(s,pr)%val,                   &
                                  ww1(:,0:riMaxIn,w1in),p(:,leg1),pl(:,leg1), &
                                  ww0(:,w0in(2)),p(:,leg2),pl(:,leg2),        &
                                  ww0(:,w0in(3)),p(:,leg3),pl(:,leg3),        &
                                  ww0(:,w0in(4)),p(:,leg4),pl(:,leg4),        &
                                  m,ww1out(:,0:riMaxOut,mo),                  &
                                  riMaxIn,riMaxOut,rankInc)
                case (6)
                  leg1 = binsm1(1,s,pr)
                  leg2 = binsm1(2,s,pr)
                  leg3 = binsm1(3,s,pr)
                  leg4 = binsm1(4,s,pr)
                  leg5 = binsm1(5,s,pr)
                  ww1out(:,0:riMaxOut,mo) = cnul
                  call compute_loop_mdl(ty,cosm1(s,pr)%val,                   &
                                  ww1(:,0:riMaxIn,w1in),p(:,leg1),pl(:,leg1), &
                                  ww0(:,w0in(2)),p(:,leg2),pl(:,leg2),      &
                                  ww0(:,w0in(3)),p(:,leg3),pl(:,leg3),      &
                                  ww0(:,w0in(4)),p(:,leg4),pl(:,leg4),      &
                                  ww0(:,w0in(5)),p(:,leg5),pl(:,leg5),      &
                                  m,ww1out(:,0:riMaxOut,mo),                  &
                                  riMaxIn,riMaxOut,rankInc)
                case (7)
                  leg1 = binsm1(1,s,pr)
                  leg2 = binsm1(2,s,pr)
                  leg3 = binsm1(3,s,pr)
                  leg4 = binsm1(4,s,pr)
                  leg5 = binsm1(5,s,pr)
                  leg6 = binsm1(6,s,pr)
                  ww1out(:,0:riMaxOut,mo) = cnul
                  call compute_loop_mdl(ty,cosm1(s,pr)%val,                   &
                                  ww1(:,0:riMaxIn,w1in),p(:,leg1),pl(:,leg1), &
                                  ww0(:,w0in(2)),p(:,leg2),pl(:,leg2),        &
                                  ww0(:,w0in(3)),p(:,leg3),pl(:,leg3),        &
                                  ww0(:,w0in(4)),p(:,leg4),pl(:,leg4),        &
                                  ww0(:,w0in(5)),p(:,leg5),pl(:,leg5),        &
                                  ww0(:,w0in(6)),p(:,leg6),pl(:,leg6),        &
                                  m,ww1out(:,0:riMaxOut,mo),                  &
                                  riMaxIn,riMaxOut,rankInc)
                case (8)
                  leg1 = binsm1(1,s,pr)
                  leg2 = binsm1(2,s,pr)
                  leg3 = binsm1(3,s,pr)
                  leg4 = binsm1(4,s,pr)
                  leg5 = binsm1(5,s,pr)
                  leg6 = binsm1(6,s,pr)
                  leg7 = binsm1(7,s,pr)
                  ww1out(:,0:riMaxOut,mo) = cnul
                  call compute_loop_mdl(ty,cosm1(s,pr)%val,                   &
                                  ww1(:,0:riMaxIn,w1in),p(:,leg1),pl(:,leg1), &
                                  ww0(:,w0in(2)),p(:,leg2),pl(:,leg2),        &
                                  ww0(:,w0in(3)),p(:,leg3),pl(:,leg3),        &
                                  ww0(:,w0in(4)),p(:,leg4),pl(:,leg4),        &
                                  ww0(:,w0in(5)),p(:,leg5),pl(:,leg5),        &
                                  ww0(:,w0in(6)),p(:,leg6),pl(:,leg6),        &
                                  ww0(:,w0in(7)),p(:,leg7),pl(:,leg7),        &
                                  m,ww1out(:,0:riMaxOut,mo),                  &
                                  riMaxIn,riMaxOut,rankInc)

                case default
                  call error_rcl('Loop current with npoint not in [2..8].',&
                                 where='compute_amplitude(internal)')
                end select
                if (last) then
                  select case (legsti(ti,pr))
                  case (1,2);   fac = iover32pi2
                  case default; fac = 2*iover32pi2
                  end select
                  if (ferlo) fac = fac*locoef
                  wwTI = cnul
                  do k = 0,riMaxOut
                    wwTI = wwTI + ww1out(lmu,k,mo) * TIri(k,ti)
                  enddo
                  matrix(cs,gs,i,lp,prin) = matrix(cs,gs,i,lp,prin) + fac * wwTI
                else
                  if (winit) then
                    ww1(:,riMaxOut+1:riwMax(c,pr),w1out) = cnul
                    ww1(:,0:riMaxOut,w1out) = ww1out(:,0:riMaxOut,mo)
                  else
                    ww1(:,0:riMaxOut,w1out) = &
                    ww1(:,0:riMaxOut,w1out) + ww1out(:,0:riMaxOut,mo)
                  endif
                endif

              enddo bm1loop

            endif

            if (pm(pr)%bd1(e,i,c)%bmin .ne. 0) then

              bd1loop: do b = pm(pr)%bd1(e,i,c)%bmin,pm(pr)%bd1(e,i,c)%bmax

                s        = pm(pr)%loop_d_maps(b)%sb
                da       = dasd1(s,pr)
                fa       = facsd1(s,pr)
                riMaxOut = riMax(rankOutsd1(s,pr))
                cs       = cssd1(s,pr)
                last     = (cs.ne.0)
                if (last) then
                  ferlo = ferloopsd1(s,pr)
                  gs = gssd1(s,pr)
                  ti = tisd1(s,pr)
                endif

                w1out = pm(pr)%loop_d_maps(b)%w_out
                winit = pm(pr)%loop_d_maps(b)%w_init

                if (last) then
                  select case (legsti(ti,pr))
                  case (1,2);   fac = iover32pi2
                  case default; fac = 2*iover32pi2
                  end select
                  if (ferlo) fac = fac*locoef
                  wwTI = cnul
                  do k = 0,riMaxOut
                    wwTI = wwTI + ww1out(lmu,k,da) * TIri(k,ti)
                  enddo
                  matrix(cs,gs,i,lp,prin) = matrix(cs,gs,i,lp,prin) + fac * fa * wwTI
                else
                  if (winit) then
                    ww1(:,riMaxOut+1:riwMax(c,pr),w1out) = cnul
                    ww1(:,0:riMaxOut,w1out) = fa * ww1out(:,0:riMaxOut,da)
                  else
                    ww1(:,0:riMaxOut,w1out) = &
                    ww1(:,0:riMaxOut,w1out) + fa * ww1out(:,0:riMaxOut,da)
                  endif
                endif

              enddo bd1loop

            endif

          enddo eloop1

        enddo configloop1

      enddo lmuloop

      deallocate (ww1)

    enddo cloop

  end if

  if (computeNLO) deallocate (ww1out,TIri)
  deallocate (ww0out,ww0)

  if (computeNLO) deallocate (psp)
  deallocate (ps,pl,p,pp)

  if (colour_optimization.ge.2.and.csTot(pr).gt.pCsTot(pr)) then
    do i = 1,cfTot(pr)
      do oi = 1,oiSize(pr)
        if (.not. compoi(oi,0,pr)) cycle
        do cs = pCsTot(pr)+1,csTot(pr)
          k = nAmp(cs,pr)
          matrix(cs,oi,i,0,prin) = &
          sum(matrix(pAmp(1:k,cs,pr),oi,i,0,prin)*facAmp(1:k,cs,pr))
        end do
      end do
    end do
    if (computeNLO) then
      do i = 1,cfTot(pr)
        do oi = 1,oiSize(pr)
          if (.not. compoi(oi,1,pr)) cycle
          do cs = pCsTot(pr)+1,csTot(pr)
            k = nAmp(cs,pr)
            do jj = 1,3
              matrix(cs,oi,i,jj,prin) = &
              sum(matrix(pAmp(1:k,cs,pr),oi,i,jj,prin)*facAmp(1:k,cs,pr))
            end do
          end do
        end do
      end do
    end if
  end if

  if (computeNLO) then
    do i = 1, cfTot(pr)
      do oi = 1,oiSize(pr)
        if (.not. compoi(oi,1,pr)) cycle
        do cs = 1,csTot(pr)
          matrix(cs,oi,i,4,prin) = sum(matrix(cs,oi,i,1:3,prin))
        end do
      end do
    end do
  end if

  if (qcd_rescaling  .and. has_feature_mdl('qcd_rescaling')) then
    als0R(0,prin)  = als0
    Qren0R(0,prin) = Qren0
    Nlq0R(0,prin)  = Nlq0
    if (computeNLO) then
      als0R(1,prin)  = als0
      Qren0R(1,prin) = Qren0
      Nlq0R(1,prin)  = Nlq0
      call get_delta_uv_rcl(deltaUV)
      call get_mu_uv_rcl(muUV)
      dZgs0R(prin) = dzgs(als0,Nlq0,deltaUV,Qren0,muUV)
    endif
  end if

  call cpu_time (timeTCout)
  timeTC(prin) = timeTC(prin) + timeTCout - timeTCin

  if (log_mem) call log_memory('@compute_amplitude: end')

  contains

  subroutine export_integrals

    !open (unit=191,file='ggggg_tensor_integrals.txt',status='replace')
    open (unit=191,file='process_' // trim(adjustl(int_to_str(prs(prin)%inpr))) // '_integrals.txt',status='replace')
    !write(191, *) 'g g g g g -> 0'
    write(191, *) trim(adjustl(prs(prin)%process))
    do i = 1,tiTot(pr)

      leg = legsti(i,pr)
      ran = rankti(i,pr)

      write(191, "(A9,I5)") 'Integral:', i
      write(191, "(A9,I2)") 'Rank:', ran
      write(191, "(A9)", advance='no') 'Masses:'

      do j= 1, leg
        write(191,"(I3,x)",advance='no') vmti(j,i,pr)
      end do
      write(191,*)

      select case (leg)

      case (1) ! tadpoles do not require momenta nor invariants

      case default ! arguments for more than one internal leg

        ! momenta
        imin = 1
        imax = leg-1
        ii   = 0
        do k = 1,legs
          if (momsti(k,i,pr).eq.imin) ii = ii + 2**(k-1)
        enddo
        write(191, "(A9)", advance='no') 'Momenta:'
        write(191,"(I3,x)",advance='no') ii
        do j = imin+1, imax
          ii = 0
          do k = 1,legs
            if (momsti(k,i,pr).eq.j) ii = ii + 2**(k-1)
          end do
          write(191,"(I3,x)",advance='no') ii
        end do
        write(191,*)
      end select
    end do

  end subroutine export_integrals

  end subroutine compute_amplitude

!------------------------------------------------------------------------------!

  subroutine compute_squared_amplitude (prin,order)

  integer,          intent(in) :: prin
  character(len=*), intent(in) :: order

  integer              :: pr,i,cs1,cs2,legs,oi1,oi2,oip,oipmax,j1,j2
  integer, allocatable :: cc(:,:)
  real(dp)             :: mat2(0:4),mat2int(1:3,1:3)
  logical              :: computeNLO
  real(sp)             :: timeHSin,timeHSout


  call cpu_time (timeHSin)

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
  else
    pr = prin
  end if

  computeNLO = (order .eq. 'NLO') .and. (lpmax(pr).gt.0)

  legs = prs(pr)%legs

  oipmax = product(max_od+1) + 1

  if (.not. allocated(matrix2h)) then
    allocate (matrix2h(1:oipmax,1:cfMax,0:4,prTot))
    matrix2h = 0d0
  else
    matrix2h(1:oipmax,1:cfTot(pr),0,prin) = 0d0
  end if

  if (.not. allocated(matrix2)) then
    allocate (matrix2(1:oipmax,0:6,prTot))
    matrix2 = 0d0
  else
    matrix2(1:oipmax,0:4,prin) = 0d0
  end if

  if (computeNLO .and. (zeroLO(pr) .or. compute_A12) &
      .and. writeMat2 .ge. 2) then
    if (.not. allocated(matrix2int)) then
      allocate (matrix2int(1:oipmax,1:3,1:3,prTot))
      matrix2int = 0d0
    else
      matrix2int(1:oipmax,1:3,1:3,prin) = 0d0
    end if
  endif

  if (.not.momcheck) return

  allocate (cc(csTot(pr),csTot(pr)))

  do cs1 = 1,csTot(pr)
    do cs2 = cs1,csTot(pr)
      select case (cs1-cs2)
      case (0)
        cc(cs1,cs2) = prs(pr)%colcoef(cs1,cs2)
      case default
        cc(cs1,cs2) = 2 * prs(pr)%colcoef(cs1,cs2)
      end select
    enddo
  enddo

  ! compute Tree squared: |A0|^2
  do cs1 = 1,csTot(pr)
    do cs2 = cs1,csTot(pr)
      do oi1 = 1,oiSize(pr)
        if (.not.compoi(oi1,0,pr)) cycle
        do oi2 = 1,oiSize(pr)
          if (.not.compoi(oi2,0,pr)) cycle
          oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
          do i = 1,cfTot(pr)
            mat2(0) = &
            cc(cs1,cs2) * real(conjg(matrix(cs1,oi1,i,0,prin))* &
                                     matrix(cs2,oi2,i,0,prin),kind=dp)
            matrix2h(oip,i,0,prin) = matrix2h(oip,i,0,prin) + mat2(0)
            matrix2 (oip,  0,prin) = matrix2 (oip,  0,prin) + mat2(0)
          enddo
        enddo
      enddo
    enddo
  enddo
  matrix2h(1:oipmax,1:cfTot(prin),0,prin) = &
  matrix2h(1:oipmax,1:cfTot(prin),0,prin) * factor(prin)
  matrix2(1:oipmax,0,prin) = &
  matrix2(1:oipmax,0,prin) * factor(prin)

  matrix2h(1:oipmax,1:cfTot(prin),1:4,prin) = 0d0
  matrix2 (1:oipmax,              1:4,prin) = 0d0
  if (computeNLO) then
    ! compute Loop squared: |A1|^2
    if (zeroLO(pr) .or. compute_A12) then
      if (writeMat2.ge.2) matrix2int(1:oipmax,1:3,1:3,prin) = 0d0
      do cs1 = 1,csTot(pr)
        do cs2 = cs1,csTot(pr)
          do oi1 = 1,oiSize(pr)
            if (.not.compoi(oi1,1,pr)) cycle
            do oi2 = 1,oiSize(pr)
              if (.not.compoi(oi2,1,pr)) cycle
              oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
              do i = 1,cfTot(pr)
                mat2(1:4) = &
                cc(cs1,cs2) * real(conjg(matrix(cs1,oi1,i,1:4,prin))* &
                                         matrix(cs2,oi2,i,1:4,prin),kind=dp)
                matrix2h(oip,i,1:4,prin) = matrix2h(oip,i,1:4,prin) + mat2(1:4)
                matrix2 (oip,  1:4,prin) = matrix2 (oip,  1:4,prin) + mat2(1:4)
                if (writeMat2.ge.2) then
                  do j1 = 1,3
                  do j2 = j1+1,3
                    mat2int(j1,j2) =                  &
                    cc(cs1,cs2) * real(               &
                    + conjg(matrix(cs1,oi1,i,j1,prin))* &
                            matrix(cs2,oi2,i,j2,prin)   &
                    + conjg(matrix(cs2,oi1,i,j1,prin))* &
                            matrix(cs1,oi2,i,j2,prin)   &
                    ,kind=dp)
                    mat2int(j2,j1) = mat2int(j1,j2)
                  enddo
                  enddo
                  matrix2int(oip,1:3,1:3,prin) = &
                  matrix2int(oip,1:3,1:3,prin) + mat2int(1:3,1:3)
                endif
              enddo
            enddo
          enddo
        enddo
      enddo
      if (writeMat2.ge.2) &
        matrix2int(1:oipmax,1:3,1:3,prin) = &
        matrix2int(1:oipmax,1:3,1:3,prin) * factor(prin)
    end if

    ! compute Tree-Loop interference: 2*Re[A0*A1]
    if (.not. zeroLO(pr)) then
      do cs1 = 1,csTot(pr)
        do cs2 = cs1,csTot(pr)
          do oi1 = 1,oiSize(pr)
            if (.not.compoi(oi1,0,pr)) cycle
            do oi2 = 1, oiSize(pr)
              if (.not.compoi(oi2,1,pr)) cycle
              do i = 1, cfTot(pr)

                oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)

                mat2(1:4) =                        &
                cc(cs1,cs2) * real(                &
                + conjg(matrix(cs1,oi1,i,0,  prin))* &
                        matrix(cs2,oi2,i,1:4,prin)   &
                + conjg(matrix(cs2,oi1,i,0,  prin))* &
                        matrix(cs1,oi2,i,1:4,prin)   &
                ,kind=dp)
                matrix2h(oip,i,1:4,prin) = &
                matrix2h(oip,i,1:4,prin) + mat2(1:4)
                matrix2 (oip,  1:4,prin) = &
                matrix2 (oip,  1:4,prin) + mat2(1:4)
              enddo
            enddo
          enddo
        enddo
      enddo
    endif

    matrix2h(1:oi2Size(pr),1:cfTot(pr),1:4,prin) = &
    matrix2h(1:oi2Size(pr),1:cfTot(pr),1:4,prin) * factor(prin)
    matrix2 (1:oi2Size(pr),            1:4,prin) = &
    matrix2 (1:oi2Size(pr),            1:4,prin) * factor(prin)
  endif

  deallocate (cc)
  call cpu_time (timeHSout)
  timeHS(prin) = timeHS(prin) + timeHSout - timeHSin

  end subroutine compute_squared_amplitude

!------------------------------------------------------------------------------!

  subroutine compute_squared_amplitude_cc (prin,i1,i2)

  integer,  intent(in) :: prin,i1,i2

  integer               :: pr,legs,j1,j2,n,cs1,cs2,oipmax,oi1,oi2,oip
  real(dp), allocatable :: cc(:,:)
  complex(dp)           :: cmat

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
    j1 = newleg(prs(prin)%relperm(i1),pr)
    j2 = newleg(prs(prin)%relperm(i2),pr)
  else
    pr = prin
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
  end if

  legs = prs(pr)%legs
  oipmax = product(max_od+1) + 1

  if (.not.allocated(matrix2cc)) then
    allocate (matrix2cc(1:oipmax,1:legsMax,1:legsMax,prTot))
    matrix2cc = 0d0
  else
    matrix2cc(1:oipmax,j1,j2,prin) = 0d0
  end if

  if (.not.momcheck) return

  if (i1 .eq. i2) then
    matrix2cc(1:oi2Size(pr),j1,j2,prin) = matrix2(1:oi2Size(pr),0,prin)
  else
    allocate (cc(1:csTot(pr),1:csTot(pr)))

    cc(:,:) = prs(pr)%colcoefc(1:csTot(pr),1:csTot(pr),j1,j2)

    if (sum(abs(cc(:,:))).gt.zerocut) then

      do n = 1,cfTot(pr)
        do oi1 = 1,oiSize(pr)
          if (.not.compoi(oi1,0,pr)) cycle
          do cs1 = 1,csTot(pr)
            cmat = conjg(matrix(cs1,oi1,n,0,prin))
            do oi2 = 1,oiSize(pr)
              if (.not.compoi(oi2,0,pr)) cycle
              oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
              do cs2 = 1,csTot(pr)
                matrix2cc(oip,j1,j2,prin) = &
                matrix2cc(oip,j1,j2,prin) + &
                cc(cs1,cs2) * real(cmat*matrix(cs2,oi2,n,0,prin),kind=dp)
              enddo
            enddo
          enddo
        enddo
      enddo

      matrix2cc(1:oi2Size(pr),j1,j2,prin) = &
      matrix2cc(1:oi2Size(pr),j1,j2,prin) * factor(prin)

    endif

    deallocate (cc)

  end if

  end subroutine compute_squared_amplitude_cc

!------------------------------------------------------------------------------!

  subroutine compute_squared_amplitude_cc_int (prin,i1,i2)

  integer,  intent(in) :: prin,i1,i2

  integer               :: pr,j1,j2,n,oi1,oi2,cs1,cs2,oip
  real(dp), allocatable :: cc(:,:)
  real(dp)              :: mat2

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
    j1 = newleg(prs(prin)%relperm(i1),pr)
    j2 = newleg(prs(prin)%relperm(i2),pr)
  else
    pr = prin
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
  end if

  if (.not. allocated(matrix2ccint)) then
    allocate (matrix2ccint(1:oi2Size(pr),1:legsMax,1:legsMax,prTot))
    matrix2ccint = 0d0
  else
    matrix2ccint(1:oi2Size(pr),j1,j2,prin) = 0d0
  end if

  if (.not.momcheck) return

  if (i1 .eq. i2) then
    matrix2ccint(1:oi2Size(pr),j1,j2,prin) = matrix2(1:oi2Size(pr),4,prin)
  else
    allocate (cc(1:csTot(pr),1:csTot(pr)))

    cc(:,:) = prs(pr)%colcoefc(1:csTot(pr),1:csTot(pr),j1,j2)

    if (sum(abs(cc(:,:))).gt.zerocut) then

      do n = 1,cfTot(pr)
        do oi1 = 1,oiSize(pr)
          if (.not.compoi(oi1,0,pr)) cycle
          do cs1 = 1,csTot(pr)
            do oi2 = 1,oiSize(pr)
              if (.not.compoi(oi2,1,pr)) cycle
              oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
              do cs2 = 1,csTot(pr)
                mat2 =                             &
                cc(cs1,cs2) * real(                &
                + conjg(matrix(cs1,oi1,n,0,prin))* &
                        matrix(cs2,oi2,n,4,prin)   &
                + conjg(matrix(cs2,oi1,n,0,prin))* &
                        matrix(cs1,oi2,n,4,prin)   &
                  ,kind=dp)
                matrix2ccint(oip,j1,j2,prin) =   &
                matrix2ccint(oip,j1,j2,prin) + mat2
              enddo
            enddo
          enddo
        enddo
      enddo

      matrix2ccint(1:oi2Size(pr),j1,j2,prin) = &
      matrix2ccint(1:oi2Size(pr),j1,j2,prin) * factor(prin)

    endif

    deallocate (cc)
  endif

  end subroutine compute_squared_amplitude_cc_int

!------------------------------------------------------------------------------!

  subroutine compute_squared_amplitude_cc_nlo (prin,i1,i2)

  integer,  intent(in) :: prin,i1,i2

  integer               :: pr,j1,j2,n,oi1,oi2,oip,cs1,cs2,oipmax
  real(dp), allocatable :: cc(:,:)
  complex(dp)           :: cmat
  real(dp)              :: mat2

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
    j1 = newleg(prs(prin)%relperm(i1),pr)
    j2 = newleg(prs(prin)%relperm(i2),pr)
  else
    pr = prin
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
  end if

  if (.not. allocated(matrix2ccnlo)) then
    oipmax = product(max_od+1) + 1
    allocate (matrix2ccnlo(1:oipmax,1:legsMax,1:legsMax,prTot))
    matrix2ccnlo = 0d0
  else
    matrix2ccnlo(1:oi2Size(pr),j1,j2,prin) = 0d0
  endif

  if (.not.momcheck) return

  if (i1 .eq. i2) then
    matrix2ccnlo(1:oi2Size(pr),j1,j2,prin) = matrix2(1:oi2Size(pr),4,prin)
  else
    allocate (cc(1:csTot(pr),1:csTot(pr)))

    cc(:,:) = prs(pr)%colcoefc(1:csTot(pr),1:csTot(pr),j1,j2)

    if (sum(abs(cc(:,:))).gt.zerocut) then

      do n = 1,cfTot(pr)
        do oi1 = 1,oiSize(pr)
          if (zeroLO(pr)) then
            if (.not. compoi(oi1,1,pr)) cycle
            do cs1 = 1,csTot(pr)
              cmat = conjg(matrix(cs1,oi1,n,4,prin))
              do oi2 = 1,oiSize(pr)
                if (.not. compoi(oi2,1,pr)) cycle
                oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
                do cs2 = 1,csTot(pr)
                  matrix2ccnlo(oip,j1,j2,prin) = &
                  matrix2ccnlo(oip,j1,j2,prin) + &
                  cc(cs1,cs2) * real(cmat*matrix(cs2,oi2,n,4,prin),kind=dp)
                enddo
              enddo
            enddo
          else
            if (.not. compoi(oi1,0,pr)) cycle
            do cs1 = 1,csTot(pr)
              do oi2 = 1,oiSize(pr)
                if (.not. compoi(oi2,1,pr)) cycle
                oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
                do cs2 = 1,csTot(pr)
                  mat2 =                             &
                  cc(cs1,cs2) * real(                &
                  + conjg(matrix(cs1,oi1,n,0,prin))* &
                          matrix(cs2,oi2,n,4,prin)   &
                  + conjg(matrix(cs2,oi1,n,0,prin))* &
                          matrix(cs1,oi2,n,4,prin)   &
                    ,kind=dp)
                  matrix2ccnlo(oip,j1,j2,prin) =   &
                  matrix2ccnlo(oip,j1,j2,prin) + mat2
                enddo
              enddo
            enddo
          end if
        enddo
      enddo

      matrix2ccnlo(1:oi2Size(pr),j1,j2,prin) = &
      matrix2ccnlo(1:oi2Size(pr),j1,j2,prin) * factor(prin)

    endif

    deallocate (cc)

  endif

  end subroutine compute_squared_amplitude_cc_nlo

!------------------------------------------------------------------------------!

  subroutine compute_squared_amplitude_scc (prin,i1,i2,v)

  integer,     intent(in) :: prin,i1,i2
  complex(dp), intent(in) :: v(0:)

  integer                  :: pr,legs,j1,j2,par1,h,n,dn,cs1,cs2, &
                              oipmax,oip,oi1,oi2
  real(dp), allocatable    :: cc(:,:)
  complex(dp)              :: p1(0:3),pl1(1:4),w01(0:3,-1:1),  &
                              a(-1:1),cmatsc
  complex(dp), allocatable :: matsc(:,:,:)
  character(2)   :: t2

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
    j1 = newleg(prs(prin)%relperm(i1),pr)
    j2 = newleg(prs(prin)%relperm(i2),pr)
  else
    pr = prin
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
  end if

  legs = prs(pr)%legs
  oipmax = product(max_od+1) + 1

  if (.not. allocated(matrix2scc)) then
    allocate (matrix2scc(1:oipmax,1:legsMax,1:legsMax,prTot))
    matrix2scc = 0d0
  else
    matrix2scc(1:oi2Size(pr),j1,j2,prin) = 0d0
  end if

  if (.not.momcheck) return

  allocate (cc(1:csTot(pr),1:csTot(pr)))

  cc(:,:) = prs(pr)%colcoefc(1:csTot(pr),1:csTot(pr),j1,j2)

  par1 = prs(prin)%par(i1)
  t2 = get_particle_type2_mdl(par1)

  if (t2 .eq. 'g' .and. sum(abs(cc(:,:))) .gt. zerocut) then

    allocate (matsc(csTot(pr),1:oiSize(pr),cfTot(pr)))

    p1 = cmplx(momenta(:,i1),kind=dp)
    if (i1 .gt. prs(pr)%legsIn) p1 = - p1
    pl1(1) = p1(0) + p1(3)
    pl1(2) = p1(0) - p1(3)
    pl1(3) = p1(1) + cima*p1(2)
    pl1(4) = conjg(pl1(3))

    do h = -1,1,2
      call definewp (par1,p1,pl1,0d0,h,w01(:,h))
      a(h) = + v(0) * conjg(w01(0,h)) &
             - v(1) * conjg(w01(1,h)) &
             - v(2) * conjg(w01(2,h)) &
             - v(3) * conjg(w01(3,h))
    enddo

    do n = 1,cfTot(pr)
      if (he(j1,n,pr).ne.-1) cycle
      dn  = dualconf(j1,n,pr)
      matsc(:,:,n) = + a(-1)*matrix(1:csTot(pr),1:oiSize(pr), n,0,prin) &
                     + a(+1)*matrix(1:csTot(pr),1:oiSize(pr),dn,0,prin)
      do oi1 = 1,oiSize(pr)
        if (.not. compoi(oi1,0,pr)) cycle
        do cs1 = 1,csTot(pr)
          cmatsc = conjg(matsc(cs1,oi1,n))
          do oi2 = 1,oiSize(pr)
            if (.not. compoi(oi2,0,pr)) cycle
            oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
            do cs2 = 1,csTot(pr)
              matrix2scc(oip,j1,j2,prin) = &
              matrix2scc(oip,j1,j2,prin) + &
              cc(cs1,cs2) * real(cmatsc*matsc(cs2,oi2,n),kind=dp)
            enddo
          enddo
        enddo
      enddo
    enddo

    matrix2scc(1:oi2Size(pr),j1,j2,prin) = &
    matrix2scc(1:oi2Size(pr),j1,j2,prin) * factor(prin)

  endif

  deallocate (cc)

  end subroutine compute_squared_amplitude_scc

!------------------------------------------------------------------------------!

  subroutine compute_squared_amplitude_scc_nlo (prin,i1,i2,v)

  integer,     intent(in) :: prin,i1,i2
  complex(dp), intent(in) :: v(0:)

  integer                  :: pr,legs,j1,j2,par1,h,n,dn,cs1,cs2, &
                              oipmax,oip,oi1,oi2
  real(dp), allocatable    :: cc(:,:)
  real(dp)                 :: mat2
  complex(dp)              :: p1(0:3),pl1(1:4),w01(0:3,-1:1),  &
                              a(-1:1)
  character(2)             :: t2

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
    j1 = newleg(prs(prin)%relperm(i1),pr)
    j2 = newleg(prs(prin)%relperm(i2),pr)
  else
    pr = prin
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
  end if

  legs = prs(pr)%legs
  oipmax = product(max_od+1) + 1

  if (.not. allocated(matrix2sccnlo)) then
    allocate (matrix2sccnlo(1:oipmax,1:legsMax,1:legsMax,prTot))
    matrix2sccnlo = 0d0
  else
    matrix2sccnlo(1:oi2Size(pr),j1,j2,prin) = 0d0
  end if

  if (.not.momcheck) return


  if (i1 .eq. i2) then
    call compute_squared_amplitude_sc_nlo(prin,i1,v)
    matrix2sccnlo(1:oi2Size(pr),j1,j2,prin) = matrix2scnlo(1:oi2Size(pr),prin)
    return
  end if

  allocate (cc(1:csTot(pr),1:csTot(pr)))

  cc(:,:) = prs(pr)%colcoefc(1:csTot(pr),1:csTot(pr),j1,j2)

  par1 = prs(prin)%par(i1)
  t2 = get_particle_type2_mdl(par1)

  if (t2 .eq. 'g' .and. sum(abs(cc(:,:))) .gt. zerocut) then

    p1 = cmplx(momenta(:,i1),kind=dp)
    if (i1 .gt. prs(pr)%legsIn) p1 = - p1
    pl1(1) = p1(0) + p1(3)
    pl1(2) = p1(0) - p1(3)
    pl1(3) = p1(1) + cima*p1(2)
    pl1(4) = conjg(pl1(3))

    do h = -1,1,2
      call definewp (par1,p1,pl1,0d0,h,w01(:,h))
      a(h) = + v(0) * conjg(w01(0,h)) &
             - v(1) * conjg(w01(1,h)) &
             - v(2) * conjg(w01(2,h)) &
             - v(3) * conjg(w01(3,h))
    enddo

    do n = 1,cfTot(pr)
      if (he(j1,n,pr).ne.-1) cycle
      dn  = dualconf(j1,n,pr)
      do oi1 = 1,oiSize(pr)
        if (zeroLO(pr)) then
          if (.not. compoi(oi1,1,pr)) cycle
          do cs1 = 1,csTot(pr)
            do oi2 = 1,oiSize(pr)
              if (.not. compoi(oi2,1,pr)) cycle
              oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
              do cs2 = 1,csTot(pr)
                mat2 =                                            &
                cc(cs1,cs2) * real(                               &
                     + conjg(a(-1)*matrix(cs1,oi1,n,4,prin)       &
                             + a(+1)*matrix(cs1,oi1,dn,4,prin)) * &
                           (a(-1)*matrix(cs2,oi2,n,4,prin)        &
                            + a(+1)*matrix(cs2,oi2,dn,4,prin)),   &
                            kind=dp)
                matrix2sccnlo(oip,j1,j2,prin) = &
                matrix2sccnlo(oip,j1,j2,prin) + mat2
              end do
            end do
          end do
        else
          if (.not. compoi(oi1,0,pr)) cycle
          do cs1 = 1,csTot(pr)
            do oi2 = 1,oiSize(pr)
              if (.not. compoi(oi2,1,pr)) cycle
              oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
              do cs2 = 1,csTot(pr)
                mat2 =                                           &
                cc(cs1,cs2) * real(                              &
                     + conjg(a(-1)*matrix(cs1,oi1,n,0,prin)      &
                            + a(+1)*matrix(cs1,oi1,dn,0,prin)) * &
                           (a(-1)*matrix(cs2,oi2,n,4,prin)       &
                            + a(+1)*matrix(cs2,oi2,dn,4,prin))   &
                     + conjg(a(-1)*matrix(cs2,oi1,n,0,prin)      &
                            + a(+1)*matrix(cs2,oi1,dn,0,prin)) * &
                           (a(-1)*matrix(cs1,oi2,n,4,prin)       &
                            + a(+1)*matrix(cs1,oi2,dn,4,prin)),  &
                            kind=dp)
                matrix2sccnlo(oip,j1,j2,prin) = &
                matrix2sccnlo(oip,j1,j2,prin) + mat2
              end do
            end do
          end do
        end if
      end do
    end do

    matrix2sccnlo(1:oi2Size(pr),j1,j2,prin) = &
    matrix2sccnlo(1:oi2Size(pr),j1,j2,prin) * factor(prin)

  endif

  deallocate (cc)

  end subroutine compute_squared_amplitude_scc_nlo

!------------------------------------------------------------------------------!

  subroutine compute_squared_amplitude_sc (prin,j,v)

  integer,     intent(in) :: prin,j
  complex(dp), intent(in) :: v(0:)

  integer                  :: pr,legs,jj,parj,h,n,dn, &
                              cs1,cs2,oipmax,oip,oi1,oi2
  real(dp), allocatable    :: cc(:,:)
  complex(dp)              :: pj(0:3),plj(1:4),w0j(0:3,-1:1),  &
                              a(-1:1),cmats
  complex(dp), allocatable :: mats(:,:,:)
  character(2) :: t2

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
    jj = newleg(prs(prin)%relperm(j),pr)
  else
    pr = prin
    jj = newleg(j,pr)
  end if

  oipmax = product(max_od+1) + 1
  legs = prs(pr)%legs

  if (.not. allocated(matrix2sc)) then
    allocate (matrix2sc(1:oipmax,prTot))
    matrix2sc = 0d0
  else
    matrix2sc(1:oi2Size(pr),prin) = 0d0
  end if

  if (.not.momcheck) return

  parj = prs(prin)%par(j)
  t2 = get_particle_type2_mdl(parj)

  if (t2 .eq. 'g' .or. t2 .eq. 'v') then

    allocate (cc(csTot(pr),csTot(pr)))

    do cs1 = 1,csTot(pr)
      do cs2 = cs1,csTot(pr)
        select case (cs1-cs2)
        case (0)
          cc(cs1,cs2) = prs(pr)%colcoef(cs1,cs2)
        case default
          cc(cs1,cs2) = 2 * prs(pr)%colcoef(cs1,cs2)
        end select
      enddo
    enddo

    allocate (mats(csTot(pr),1:oiSize(pr),cfTot(pr)))

    pj = cmplx(momenta(:,j),kind=dp)
    if (j .gt. prs(pr)%legsIn) pj = - pj
    plj(1) = pj(0) + pj(3)
    plj(2) = pj(0) - pj(3)
    plj(3) = pj(1) + cima*pj(2)
    plj(4) = conjg(plj(3))

    do h = -1,1,2
      call definewp (parj,pj,plj,0d0,h,w0j(:,h))
      a(h) = + v(0) * conjg(w0j(0,h)) &
             - v(1) * conjg(w0j(1,h)) &
             - v(2) * conjg(w0j(2,h)) &
             - v(3) * conjg(w0j(3,h))
    enddo

    do n = 1,cfTot(pr)
      if (he(jj,n,pr).ne.-1) cycle
      dn  = dualconf(jj,n,pr)
      mats(:,:,n) =                                      &
      + a(-1)*matrix(1:csTot(pr),1:oiSize(pr), n,0,prin) &
      + a(+1)*matrix(1:csTot(pr),1:oiSize(pr),dn,0,prin)
      do oi1 = 1,oiSize(pr)
        if (.not. compoi(oi1,0,pr)) cycle
        do cs1 = 1,csTot(pr)
          cmats = conjg(mats(cs1,oi1,n))
          do oi2 = 1,oiSize(pr)
            if (.not. compoi(oi2,0,pr)) cycle
            oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
            do cs2 = cs1,csTot(pr)
              matrix2sc(oip,prin) = &
              matrix2sc(oip,prin) + &
              cc(cs1,cs2) * real(cmats*mats(cs2,oi2,n),kind=dp)
            enddo
          enddo
        enddo
      enddo
    enddo

    matrix2sc(1:oi2Size(pr),prin) =            &
    matrix2sc(1:oi2Size(pr),prin) * factor(prin)

  endif

  deallocate (cc)

  end subroutine compute_squared_amplitude_sc

!------------------------------------------------------------------------------!

  subroutine compute_squared_amplitude_sc_nlo (prin,j,v)

  integer,     intent(in) :: prin,j
  complex(dp), intent(in) :: v(0:)

  integer                  :: pr,legs,jj,parj,h,n,dn, &
                              cs1,cs2,oipmax,oip,oi1,oi2
  real(dp), allocatable    :: cc(:,:)
  real(dp)                 :: mat2
  complex(dp)              :: pj(0:3),plj(1:4),w0j(0:3,-1:1),  &
                              a(-1:1)
  character(2) :: t2

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
    jj = newleg(prs(prin)%relperm(j),pr)
  else
    pr = prin
    jj = newleg(j,pr)
  end if

  oipmax = product(max_od+1) + 1
  legs = prs(pr)%legs

  if (.not. allocated(matrix2scnlo)) then
    allocate (matrix2scnlo(1:oipmax,prTot))
    matrix2scnlo = 0d0
  else
    matrix2scnlo(1:oi2Size(pr),prin) = 0d0
  end if

  if (.not.momcheck) return

  parj = prs(prin)%par(j)
  t2 = get_particle_type2_mdl(parj)

  if (t2 .eq. 'g' .or. t2 .eq. 'v') then

    allocate (cc(csTot(pr),csTot(pr)))

    do cs1 = 1,csTot(pr)
      do cs2 = cs1,csTot(pr)
        select case (cs1-cs2)
        case (0)
          cc(cs1,cs2) = prs(pr)%colcoef(cs1,cs2)
        case default
          cc(cs1,cs2) = 2 * prs(pr)%colcoef(cs1,cs2)
        end select
      enddo
    enddo

    pj = cmplx(momenta(:,j),kind=dp)
    if (j .gt. prs(pr)%legsIn) pj = - pj
    plj(1) = pj(0) + pj(3)
    plj(2) = pj(0) - pj(3)
    plj(3) = pj(1) + cima*pj(2)
    plj(4) = conjg(plj(3))

    a = 0
    do h = -1,1,2
      call definewp (parj,pj,plj,0d0,h,w0j(:,h))
      a(h) = + v(0) * conjg(w0j(0,h)) &
             - v(1) * conjg(w0j(1,h)) &
             - v(2) * conjg(w0j(2,h)) &
             - v(3) * conjg(w0j(3,h))
    end do

    do n = 1,cfTot(pr)
      if (he(jj,n,pr).ne.-1) cycle
      dn  = dualconf(jj,n,pr)
      do oi1 = 1,oiSize(pr)
        if (zeroLO(pr)) then
          if (.not. compoi(oi1,1,pr)) cycle
          do cs1 = 1,csTot(pr)
            do oi2 = 1,oiSize(pr)
              if (.not. compoi(oi2,1,pr)) cycle
              oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
              do cs2 = cs1,csTot(pr)
                mat2 = &
                cc(cs1,cs2) * real(                        &
                conjg(+ a(-1)*matrix(cs1,oi1, n,4,prin)    &
                      + a(+1)*matrix(cs1,oi1,dn,4,prin)) * &
                     (+ a(-1)*matrix(cs2,oi2, n,4,prin)    &
                      + a(+1)*matrix(cs2,oi2,dn,4,prin)),  &
                     kind=dp)
                matrix2scnlo(oip,prin) = &
                matrix2scnlo(oip,prin) + mat2
              end do
            end do
          end do
        else
          if (.not. compoi(oi1,0,pr)) cycle
          do cs1 = 1,csTot(pr)
            do oi2 = 1,oiSize(pr)
              if (.not. compoi(oi2,1,pr)) cycle
              oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
              do cs2 = cs1,csTot(pr)
                mat2 = &
                cc(cs1,cs2) * real(                        &
                conjg(+ a(-1)*matrix(cs1,oi1, n,0,prin)    &
                      + a(+1)*matrix(cs1,oi1,dn,0,prin)) * &
                     (+ a(-1)*matrix(cs2,oi2, n,4,prin)    &
                      + a(+1)*matrix(cs2,oi2,dn,4,prin)) + &
                conjg(+ a(-1)*matrix(cs2,oi1, n,0,prin)    &
                      + a(+1)*matrix(cs2,oi1,dn,0,prin)) * &
                     (+ a(-1)*matrix(cs1,oi2, n,4,prin)    &
                      + a(+1)*matrix(cs1,oi2,dn,4,prin)),  &
                     kind=dp)
                matrix2scnlo(oip,prin) = &
                matrix2scnlo(oip,prin) + mat2
              end do
            end do
          end do
        end if
      enddo
    enddo

    matrix2scnlo(1:oi2Size(pr),prin) =            &
    matrix2scnlo(1:oi2Size(pr),prin) * factor(prin)

  endif

  deallocate (cc)

  end subroutine compute_squared_amplitude_sc_nlo

!------------------------------------------------------------------------------!

  subroutine compute_squared_amplitude_scm (prin,j)

  integer,     intent(in) :: prin,j

  integer                  :: pr,legs,jj,parj,h,n,dn, &
                              cs1,cs2,oipmax,oip,oi1,oi2, &
                              mu,nu
  real(dp), allocatable    :: cc(:,:)
  complex(dp)              :: pj(0:3),plj(1:4),w0j(0:3,-1:1),  &
                              a(-1:1,0:3)
  character(2) :: t2

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
    jj = newleg(prs(prin)%relperm(j),pr)
  else
  pr = prin
    jj = newleg(j,pr)
  end if

  oipmax = product(max_od+1) + 1
  legs = prs(pr)%legs

  if (.not. allocated(matrix2scm)) then
    allocate (matrix2scm(0:3,0:3,1:oipmax,prTot))
    matrix2scm = 0d0
  else
    matrix2scm(:,:,1:oi2Size(pr),prin) = 0d0
  end if

  if (.not.momcheck) return

  parj = prs(prin)%par(j)
  t2 = get_particle_type2_mdl(parj)

  if (t2 .eq. 'g' .or. t2 .eq. 'v') then

    allocate (cc(csTot(pr),csTot(pr)))

    cc(:,:) = prs(pr)%colcoef(1:csTot(pr),1:csTot(pr))

    pj = cmplx(momenta(:,j),kind=dp)
    if (j .gt. prs(pr)%legsIn) pj = - pj
    plj(1) = pj(0) + pj(3)
    plj(2) = pj(0) - pj(3)
    plj(3) = pj(1) + cima*pj(2)
    plj(4) = conjg(plj(3))

    do h = -1,1,2
      call definewp (parj,pj,plj,0d0,h,w0j(:,h))
      a(h,0) = conjg(w0j(0,h))
      a(h,1) = -conjg(w0j(1,h))
      a(h,2) = -conjg(w0j(2,h))
      a(h,3) = -conjg(w0j(3,h))
    end do

    do n = 1,cfTot(pr)
      if (he(jj,n,pr).ne.-1) cycle
      dn = dualconf(jj,n,pr)
      do oi1 = 1,oiSize(pr)
        if (.not. compoi(oi1,0,pr)) cycle
        do cs1 = 1,csTot(pr)
          do oi2 = 1,oiSize(pr)
            if (.not. compoi(oi2,0,pr)) cycle
            oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
            do cs2 = 1,csTot(pr)
              do mu = 1, 3
                do nu = 1, 3
                  matrix2scm(mu,nu,oip,prin) = &
                  matrix2scm(mu,nu,oip,prin) + &
                  cc(cs1,cs2) * real( &
                  conjg(a(-1,mu)*matrix(cs1,oi1, n,0,prin) +  &
                        a(+1,mu)*matrix(cs1,oi1,dn,0,prin)) * &
                       (a(-1,nu)*matrix(cs2,oi2, n,0,prin) +  &
                        a(+1,nu)*matrix(cs2,oi2,dn,0,prin)),  &
                  kind=dp)
                end do
              end do
            end do
          end do
        end do
      end do
    end do

    matrix2scm(:,:,1:oi2Size(pr),prin) =            &
    matrix2scm(:,:,1:oi2Size(pr),prin) * factor(prin)
  end if

  deallocate (cc)

  end subroutine compute_squared_amplitude_scm

!------------------------------------------------------------------------------!

  !subroutine compute_squared_amplitude_scm (prin,j)
  !
  !integer,     intent(in) :: prin,j
  !
  !integer                  :: pr,legs,jj,parj,h,n,dn, &
  !                            cs1,cs2,oipmax,oip,oi1,oi2, &
  !                            mu,nu
  !real(dp), allocatable    :: cc(:,:),mp,mm,mmi
  !complex(dp)              :: pj(0:3),plj(1:4),w0j(0:3,-1:1),  &
  !                            a(-1:1,0:3),cmats,mats
  !character(2) :: t2
  !
  !if (prs(prin)%crosspr .ne. 0) then
  !  pr = prs(prin)%crosspr
  !  jj = newleg(prs(prin)%relperm(j),pr)
  !else
  !  pr = prin
  !  jj = newleg(j,pr)
  !end if
  !
  !oipmax = product(max_od+1) + 1
  !legs = prs(pr)%legs
  !
  !if (.not. allocated(matrix2scm)) then
  !  allocate (matrix2scm(0:3,0:3,1:oipmax,prTot))
  !  matrix2scm = 0d0
  !else
  !  matrix2scm(:,:,1:oi2Size(pr),prin) = 0d0
  !end if
  !
  !write(*,*) "HERE"
  !if (.not.momcheck) return
  !
  !parj = prs(prin)%par(j)
  !t2 = get_particle_type2_mdl(parj)
  !
  !if (t2 .eq. 'g' .or. t2 .eq. 'v') then
  !
  !  allocate (cc(csTot(pr),csTot(pr)))
  !
  !  cc(:,:) = prs(pr)%colcoef(1:csTot(pr),1:csTot(pr))
  !
  !  pj = cmplx(momenta(:,j),kind=dp)
  !  if (j .gt. prs(pr)%legsIn) pj = - pj
  !  plj(1) = pj(0) + pj(3)
  !  plj(2) = pj(0) - pj(3)
  !  plj(3) = pj(1) + cima*pj(2)
  !  plj(4) = conjg(plj(3))
  !
  !  do h = -1,1,2
  !    call definewp (parj,pj,plj,0d0,h,w0j(:,h))
  !    write(*,*) "w0j(:,h):", w0j(:,h)
  !    a(h,0) = conjg(w0j(0,h))
  !    a(h,1) = -conjg(w0j(1,h))
  !    a(h,2) = -conjg(w0j(2,h))
  !    a(h,3) = -conjg(w0j(3,h))
  !  enddo
  !
  !  mm = 0
  !  mp = 0
  !  mmi = 0
  !  cc = 8
  !  do n = 1,cfTot(pr)
  !    if (he(jj,n,pr).ne.-1) cycle
  !    if (he(1,n,pr).ne.-1) cycle
  !    if (he(2,n,pr).ne.-1) cycle
  !    if (he(3,n,pr).ne.1) cycle
  !    dn = dualconf(jj,n,pr)
  !    do oi1 = 1,oiSize(pr)
  !      if (.not. compoi(oi1,0,pr)) cycle
  !      do cs1 = 1,csTot(pr)
  !        if (cs1 .ne. 1) cycle
  !        do oi2 = 1,oiSize(pr)
  !          if (.not. compoi(oi2,0,pr)) cycle
  !          oip = reg_od2(oi_reg(:,oi1,pr) + oi_reg(:,oi2,pr),pr)
  !          do cs2 = 1,csTot(pr)
  !            if (cs2 .ne. 1) cycle
  !            !mats =  a(he(jj,n,pr),mu)*matrix(cs2,oi2, n,0,prin)
  !            !cmats = conjg(a(he(jj,n,pr),nu)*matrix(cs1,oi1, n,0,prin))
  !                ! missing same helicity :  <16-12-18, J.-N. Lang> !
  !            do mu = 1, 3
  !              do nu = 1, 3
  !                !mats =  a(-1,mu)*matrix(cs2,oi2, n,0,prin) &
  !                !        + a(+1,mu)*matrix(cs2,oi2,dn,0,prin)
  !                !cmats =  conjg(a(-1,nu)*matrix(cs1,oi1, n,0,prin) &
  !                !               + a(+1,nu)*matrix(cs1,oi1,dn,0,prin))
  !                !matrix2scm(mu,nu,oip,prin) = &
  !                !matrix2scm(mu,nu,oip,prin) + &
  !                !cc(cs1,cs2) * real(cmats*mats,kind=dp)
  !
  !                !mats =  a(-1,mu)*matrix(cs2,oi2, n,0,prin) &
  !                !        + a(+1,mu)*matrix(cs2,oi2,dn,0,prin)
  !                !cmats =  conjg(a(-1,nu)*matrix(cs1,oi1, n,0,prin) &
  !                !               + a(+1,nu)*matrix(cs1,oi1,dn,0,prin))
  !                matrix2scm(mu,nu,oip,prin) = &
  !                matrix2scm(mu,nu,oip,prin) + &
  !                cc(cs1,cs2) * real( &
  !                a(-1,mu)*matrix(cs2,oi2, n,0,prin) * &
  !                conjg(a(-1,nu)*matrix(cs1,oi1, n,0,prin)) + &
  !                a(+1,mu)*matrix(cs2,oi2,dn,0,prin) * &
  !                conjg(a(+1,nu)*matrix(cs1,oi1,dn,0,prin)) + &
  !                a(-1,mu)*matrix(cs2,oi2, n,0,prin) * &
  !                a(+1,nu)*matrix(cs1,oi1,dn,0,prin) + &
  !                a(+1,mu)*matrix(cs2,oi2,dn,0,prin) * &
  !                a(-1,nu)*matrix(cs1,oi1, n,0,prin), &
  !                kind=dp)
  !
  !                !matrix2scm(mu,nu,oip,prin) = &
  !                !matrix2scm(mu,nu,oip,prin) + &
  !                !cc(cs1,cs2) * real(cmats*mats,kind=dp)
  !              end do
  !            end do
  !            write(*,*) "matrix(cs1,oi1, n,0,prin):", matrix(cs1,oi1, n,0,prin)*sqrt(2d0)
  !            write(*,*) "matrix(cs2,oi2, n,0,prin):", matrix(cs2,oi2, n,0,prin)*sqrt(2d0)
  !            write(*,*) "matrix(cs1,oi1,dn,0,prin):", -matrix(cs1,oi1,dn,0,prin)*sqrt(2d0)
  !            write(*,*) "matrix(cs2,oi2,dn,0,prin):", -matrix(cs2,oi2,dn,0,prin)*sqrt(2d0)
  !            write(*,*) ":", conjg(matrix(cs1,oi1, n,0,prin)) * 8 * matrix(cs1,oi1, n,0,prin)/36
  !            write(*,*) ":", conjg(matrix(cs1,oi1,dn,0,prin)) * 8 * matrix(cs1,oi1,dn,0,prin)/36
  !            write(*,*) "mmm:", matrix(cs1,oi1, n,0,prin)*8d0*matrix(cs2,oi2,dn,0,prin)/36
  !            mm = mm + real(conjg(matrix(cs1,oi1, n,0,prin))*matrix(cs2,oi2, &
  !              n,0,prin),kind=dp)*cc(cs1,cs2)
  !            mp = mp + real(conjg(matrix(cs1,oi1,dn,0,prin))*matrix(cs2,oi2, &
  !             dn,0,prin),kind=dp)*cc(cs1,cs2)
  !            mmi = mmi + real((matrix(cs1,oi1,n,0,prin))*matrix(cs2,oi2, &
  !             dn,0,prin) + (matrix(cs1,oi1,dn,0,prin))*matrix(cs2,oi2, &
  !             n,0,prin),kind=dp)*cc(cs1,cs2)
  !            !mmi = mmi + real(conjg(matrix(cs1,oi1,n,0,prin))*matrix(cs2,oi2, &
  !            ! dn,0,prin) )*cc(cs1,cs2)
  !            !mmi = mmi + real(matrix(cs1,oi1,n,0,prin)*conjg(matrix(cs2,oi2, &
  !            ! dn,0,prin)))*cc(cs1,cs2)
  !            !!do mu = 1, 3
  !            !  mats =  a(-1,mu)*matrix(cs2,oi2, n,0,prin) &
  !            !          + a(+1,mu)*matrix(cs2,oi2,dn,0,prin)
  !            !  cmats =  conjg(a(-1,mu)*matrix(cs1,oi1, n,0,prin) &
  !            !                 + a(+1,mu)*matrix(cs1,oi1,dn,0,prin))
  !            !  matrix2scm(mu,mu,oip,prin) = &
  !            !  matrix2scm(mu,mu,oip,prin) + &
  !            !  cc(cs1,cs2) * real(cmats*mats,kind=dp)
  !            !end do
  !          end do
  !        end do
  !      end do
  !    end do
  !  end do
  !
  !  matrix2scm(:,:,1:oi2Size(pr),prin) =            &
  !  matrix2scm(:,:,1:oi2Size(pr),prin) * factor(prin)
  !  mm = mm * factor(prin)
  !  mp = mp * factor(prin)
  !  mmi = mmi * factor(prin)
  !
  !endif
  !
  !write(*,*) "mp:", mp
  !write(*,*) "mm:", mm
  !write(*,*) "mmi:", mmi
  !!write(*,*) "mt:", mt
  !write(*,*) "matrix2scm(i,i,:,prin):", sum(matrix2scm(0,0,:,prin)) +sum(matrix2scm(1,1,:,prin)) + &
  !sum(matrix2scm(2,2,:,prin)) + sum(matrix2scm(3,3,:,prin))
  !write(*,*) "matrix2scm(2,3,:,prin):", sum(matrix2scm(1,2,:,prin))
  !write(*,*) "matrix2scm(3,2,:,prin):", sum(matrix2scm(2,1,:,prin))
  !write(*,*) "matrix2scm(4,2,:,prin):", sum(matrix2scm(3,1,:,prin))
  !write(*,*) "matrix2scm(2,4,:,prin):", sum(matrix2scm(1,3,:,prin))
  !write(*,*) "matrix2scm(4,3,:,prin):", sum(matrix2scm(3,2,:,prin))
  !write(*,*) "matrix2scm(3,4,:,prin):", sum(matrix2scm(2,3,:,prin))
  !
  !write(*,*) "matrix2scm(0,0,:,prin):", sum(matrix2scm(0,0,:,prin))
  !write(*,*) "matrix2scm(1,1,:,prin):", sum(matrix2scm(1,1,:,prin))
  !write(*,*) "matrix2scm(2,2,:,prin):", sum(matrix2scm(2,2,:,prin))
  !write(*,*) "matrix2scm(3,3,:,prin):", sum(matrix2scm(3,3,:,prin))
  !
  !deallocate (cc)
  !
  !end subroutine compute_squared_amplitude_scm

!------------------------------------------------------------------------------!

  subroutine print_process_and_momenta (pr)

  integer, intent (in) :: pr

  integer   :: i,j,legs,e1,ap
  real(dp)    :: p2,pm,rm2,rm,w
  complex(dp) :: cm2
  character :: fmt(0:3)*6,fmtTot*80,ci*2
  character :: cpa*10,fmtm*80,fmtpm*80,fmtm2*80,fmtp2*80

  legs = prs(pr)%legs

  call openOutput

  write(nx,*)
  write(nx,'(1x,75("x"))')
  write(nx,*)
  write(nx,*)
  if (prs(pr)%crosspr .ne. 0) then
    write(nx,*) ' ',trim(prs(pr)%process), &
                ' (crossing of ' // trim(prs(prs(pr)%crosspr)%process) // ')'
  else
    write(nx,*) ' ',trim(prs(pr)%process)
  end if
  write(nx,*)

  do i = 1,legs
    write(ci,'(i2)') i
    fmt(0) = 'f14.9'
    if (abs(momenta(0,i)).ge.1d4) fmt(0) = 'e14.8'
    if (legs.lt.10) then
      fmtTot = '(2x,a,a,1x,"(",'//fmt(0)
    else
      fmtTot = '(2x,a,1x,a,1x,"(",'//fmt(0)
    endif
    do j = 1,3
      fmt(j) = 'f15.9'
      if (abs(momenta(j,i)).ge.1d4) fmt(j) = 'e15.8'
      fmtTot = trim(fmtTot)//',",",'//fmt(j)
    enddo
    fmtTot = trim(fmtTot)//',") GeV")'
    write(nx,trim(fmtTot)) 'p'//adjustl(trim(ci)),'=',momenta(0:3,i)
  enddo
  write(nx,*)

  do i = 1,prs(pr)%resMax
    write(ci,'(i2)') i
    ap = get_particle_antiparticle_mdl(prs(pr)%parRes(i))
    cpa = get_particle_name_mdl(ap)
    e1 = newbin(prs(pr)%binRes(i),pr)
    if (defp2bin(e1,pr)) then
      p2 = p2bin(e1,pr)
      pm = sqrt(p2)
      fmtp2 = 'f14.9'; if (p2.ge.1d4) fmtp2 = 'e14.8'
      fmtpm = 'f14.9'; if (pm.ge.1d4) fmtpm = 'e14.8'
      write(nx,'(2x,5a)') &
        'The denominator of the propagator of resonance number ', &
        trim(adjustl(ci)),' (',trim(cpa),' particle) '
      write(nx,'(2x,a)') 'carries an off-sheel squared momentum p^2:'
      write(nx,'(4x,a,'//trim(fmtp2)//',a)') 'p^2       = ',p2,' GeV^2'
      write(nx,'(4x,a,'//trim(fmtpm)//',a)') 'sqrt(p^2) = ',pm,' GeV'
      write(nx,*)
    endif
    if (defresbin(e1,pr)) then
      rm2 = real(get_particle_cmass2_mdl(prs(pr)%parRes(i)),kind=dp)
      rm = sqrt(rm2)
      w = real(get_particle_width_mdl(prs(pr)%parRes(i)))
      cm2 = rm2 - cima*w*rm
      fmtm2 = '(4x,a,"(",e14.8,",",e15.8,")",a)'
      fmtm  = '(4x,a,g21.14,6x,a,g21.14)'
      write(nx,'(2x,5a)') &
        'The denominator of the propagator of resonance number ', &
        trim(adjustl(ci)),' (',trim(cpa),' particle) '
      write(nx,'(2x,a)') 'carries a complex mass m^2:'
      write(nx,fmtm2) 'm^2  = ',cm2,' GeV^2'
      write(nx,fmtm)  'mass =',rm,'width =',w
      write(nx,*)
    endif
  enddo

  end subroutine print_process_and_momenta

!------------------------------------------------------------------------------!

  subroutine print_parameters_change
  use input_rcl, only: get_mu_uv_rcl,get_mu_ir_rcl, &
                       get_delta_uv_rcl,get_delta_ir_rcl
  real (dp) :: deltaUV,deltaIR,deltaIR2,muUV,muIR

  call get_delta_uv_rcl(deltaUV)
  call get_mu_uv_rcl(muUV)
  call get_delta_ir_rcl(deltaIR,deltaIR2)
  call get_mu_ir_rcl(muIR)

  call openOutput

  if (changed_DeltaUV) then
    write(nx,'(2x,a,g21.14)') 'Delta_UV = ',deltaUV
    write(nx,*)
  endif

  if (changed_muUV) then
    write(nx,'(2x,a,g21.14,a)') 'mu_UV = ',muUV,' GeV'
    write(nx,*)
  endif

  if (changed_DeltaIR) then
    write(nx,'(2x,a,g21.14,7x,a,g21.14)') &
      'Delta_IR^2 =',deltaIR2,'Delta_IR = ',deltaIR
    write(nx,*)
  endif

  if (changed_muIR) then
    write(nx,'(2x,a,g21.14,a)') 'mu_IR = ',muIR,' GeV'
    write(nx,*)
  endif

  end subroutine print_parameters_change

!------------------------------------------------------------------------------!

  subroutine print_amplitude (pr,order)

  integer,          intent(in) :: pr
  character(len=*), intent(in) :: order

  integer                :: i,j,k,l,n,legs,cs,lp,oi
  logical                :: computeNLO
  real(dp)               :: x
  character(99)          :: hconf1,hconf2,fmt
  character, allocatable :: delta(:)*6,up(:)*6,lo(:)*6
  character(len=20)      :: order_ids

  computeNLO = (order.eq.'NLO').and.(lpmax(pr).gt.0)

  legs = prs(pr)%legs

  n = 4 - legs

  order_ids = ' '
  do i = 1, n_orders
    order_ids = trim(order_ids)//' '//trim(adjustl(get_order_id_mdl(i)))
  end do

  call openOutput

  h1loop: do i = 1, cfTot(pr)

    x = sum(abs(matrix(1:csTot(pr),1:oiSize(pr),i,0,pr)))
    if (computeNLO) &
      x = x + sum(abs(matrix(1:csTot(pr),1:oiSize(pr),i,4,pr)))

    if (x .eq. 0d0) cycle

    select case (n)
    case (-99:-10); fmt = '(1x,a,i3,a)'
    case (-9:-1);   fmt = '(1x,a,i2,a)'
    case (0:9);     fmt = '(1x,a,i1,a)'
    case default
    end select

    write(nx,'(1x,75("-"))')
    write(nx,*)
    write(nx,*)
    write(nx,trim(fmt)) ' AMPLITUDE [GeV^',n,']'
    write(nx,*)
    write(nx,*)

    hconf1 = ''
    do j = 1,prs(pr)%legsIn
      hconf1 = trim(hconf1)//' '//trim(get_particle_name_mdl(prs(pr)%par(j)))
      if     (he(newleg(j,pr),i,pr).eq.-1) then; hconf1 = trim(hconf1)//'[-]'
      elseif (he(newleg(j,pr),i,pr).eq. 0) then; hconf1 = trim(hconf1)//'[0]'
      elseif (he(newleg(j,pr),i,pr).eq.+1) then; hconf1 = trim(hconf1)//'[+]'
      endif
    enddo

    hconf2 = ''
    do j = prs(pr)%legsIn+1,legs
      hconf2 = trim(hconf2)//' '//trim(get_particle_name_mdl( &
               get_particle_antiparticle_mdl(prs(pr)%par(j))))
      if     (he(newleg(j,pr),i,pr).eq.-1) then; hconf2 = trim(hconf2)//'[+]'
      elseif (he(newleg(j,pr),i,pr).eq. 0) then; hconf2 = trim(hconf2)//'[0]'
      elseif (he(newleg(j,pr),i,pr).eq.+1) then; hconf2 = trim(hconf2)//'[-]'
      endif
    enddo

    l = len(trim(hconf1)//' ->'//trim(hconf2))

    if (l .gt. 75) then
      write(nx,*) ' Helicity configuration:'
      write(nx,*) trim(hconf1),' ->'
      write(nx,*) trim(hconf2)
    elseif (l .gt. 75-27) then
      write(nx,*) ' Helicity configuration:'
      write(nx,*) trim(hconf1),' ->',trim(hconf2)
    else
      write(nx,*) ' Helicity configuration:  ', &
                  trim(hconf1),' ->',trim(hconf2)
    endif
    write(nx,*)

    csloop: do cs = 1,csTot(pr)

      if (   sum(abs(matrix(cs,1:oiSize(pr),i,0,pr))) &
           + sum(abs(matrix(cs,1:oiSize(pr),i,4,pr))) &
           .eq.0d0                                     ) cycle

      allocate (delta(legs),up(legs),lo(legs))
      k = 0
      do j = 1,legs
        ! upper index is "ia", lower index is "iq"
        if (csIq(j,cs,pr).ne.0) then
          k = k + 1
          delta(k) = '  d'
          write(up(k),'(i2)') oldleg(j,pr)
          write(lo(k),'(i2)') oldleg(csIq(j,cs,pr),pr)
          up(k) = '   i'//trim(adjustl(up(k)))
          lo(k) = '   j'//trim(adjustl(lo(k)))
        endif
      enddo
      if (k.gt.0) then
        write(nx,*) '                   ',up(1:k)
        write(nx,*) ' Colour structure: ',delta(1:k)
        write(nx,*) '                   ',lo(1:k)
        write(nx,*)
      endif
      deallocate (delta,up,lo)

      if (sum(abs(matrix(cs,1:oiSize(pr),i,0,pr))) .ne. 0d0) then

        write(nx,*) '     |               Born Amplitude A0             |',&
                    trim(order_ids)
        write(nx,*) '     -----------------------------------------------'
        do oi = 1,oiSize(pr)
          if (.not.compoi(oi,0,pr)) cycle
          write(nx,'(6x, "|(",e21.14,",",e21.14,")|",*(3x,i1))') &
                   matrix(cs,oi,i,0,pr),oi_reg(:,oi,pr)
        enddo
        write(nx,*) '      ----------------------------------------------'
        write(nx,'( "  SUM |(",e21.14,",",e21.14,")|")') &
                   sum(matrix(cs,1:oiSize(pr),i,0,pr))
        write(nx,*)

      endif

      if ( computeNLO .and.                               &
           (sum(abs(matrix(cs,1:oiSize(pr),i,4,pr))).ne.0d0) ) then

        write(nx,*) '     |              1-loop Amplitude A1            |',&
                    trim(order_ids)
        write(nx,*) '     -----------------------------------------------'
        do oi = 1,oiSize(pr)
          if (.not.compoi(oi,1,pr)) cycle
          write(nx,'(6x, "|(",e21.14,",",e21.14,")|",*(3x,i1))') &
                   matrix(cs,oi,i,4,pr),oi_reg(:,oi,pr)
        enddo
        write(nx,*) '      ----------------------------------------------'
        write(nx,'( "  SUM |(",e21.14,",",e21.14,")|")') &
                   sum(matrix(cs,1:oiSize(pr),i,4,pr))
        write(nx,*)

        if (writeMat.ge.2) then
          do lp = 1,3
            select case (lp)
            case (1)
              write(nx,*) '     |    4-dimensional bare-loop Amplitude A1d4   |',&
                          trim(order_ids)
            case (2)
              write(nx,*) '     |               CT Amplitude A1ct             |',&
                          trim(order_ids)
            case (3)
              write(nx,*) '     |               R2 Amplitude A1r2             |',&
                          trim(order_ids)
            end select
            write(nx,*)   '     -----------------------------------------------'
            do oi = 1,oiSize(pr)
              if (.not.compoi(oi,1,pr)) cycle
              write(nx,'(6x, "|(",e21.14,",",e21.14,")|",*(3x,i1))') &
                         matrix(cs,oi,i,lp,pr),oi_reg(:,oi,pr)
            enddo
            write(nx,*)   '    ------------------------------------------------'
            write(nx,'( "  SUM |(",e21.14,",",e21.14,")|")') &
                         sum(matrix(cs,1:oiSize(pr),i,lp,pr))
            write(nx,*)
          enddo
        endif

      endif

      write(nx,*)

    enddo csloop

  enddo h1loop

  end subroutine print_amplitude

!------------------------------------------------------------------------------!

  subroutine print_squared_amplitude (prin,order)

  integer,          intent (in) :: prin
  character(len=*), intent(in) :: order

  integer       :: pr,i,j,l,legs,n,oi
  logical       :: computeNLO
  real(dp)      :: x,y
  character(99) :: hconf1,hconf2,fmt
  character(len=20) :: order_ids

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
  else
    pr = prin
  end if

  computeNLO = (order.eq.'NLO').and.(lpmax(pr).gt.0)

  legs = prs(pr)%legs

  n = 8 - 2*legs

  order_ids = ' '
  do i = 1, n_orders
    order_ids = trim(order_ids)//' '//trim(adjustl(get_order_id_mdl(i)))
  end do

  x = sum(abs(matrix2(1:oi2Size(pr),0,prin)))
  if (computeNLO) x = x + sum(abs(matrix2(1:oi2Size(pr),1,prin)))

  if (x.ne.0d0) then

    call openOutput

    if ( writeMat2.ge.3 ) then

      write(nx,'(1x,75("-"))')
      write(nx,*)
      write(nx,*)

      h2loop: do i = 1,cfTot(pr)

        y = sum(abs(matrix2h(1:oi2Size(pr),i,0,prin)))
        if (computeNLO) &
          y = y + sum(abs(matrix2h(1:oi2Size(pr),i,4,prin)))

        if (y.eq.0d0) cycle

        select case (n)
        case (-99:-10); fmt = '(1x,a,i3,a)'
        case (-9:-1);   fmt = '(1x,a,i2,a)'
        case (0:9);     fmt = '(1x,a,i1,a)'
        case default
        end select

        write(nx,*)
        write(nx,trim(fmt)) ' POLARIZED SQUARED AMPLITUDE [GeV^',n,']'
        write(nx,*)

        hconf1 = ''
        do j = 1,prs(pr)%legsIn
          hconf1 = trim(hconf1)//' '//trim(get_particle_name_mdl(prs(pr)%par(j)))
          if     (he(newleg(j,pr),i,pr).eq.-1) then; hconf1 = trim(hconf1)//'[-]'
          elseif (he(newleg(j,pr),i,pr).eq. 0) then; hconf1 = trim(hconf1)//'[0]'
          elseif (he(newleg(j,pr),i,pr).eq.+1) then; hconf1 = trim(hconf1)//'[+]'
          endif
        enddo

        hconf2 = ''
        ! TODO: crossing ?:  <06-07-18, J.-N. Lang> !
        do j = prs(pr)%legsIn+1,legs
          hconf2 = trim(hconf2)//' '//trim(get_particle_name_mdl( &
                   get_particle_antiparticle_mdl(prs(pr)%par(j))))
          if     (he(newleg(j,pr),i,pr).eq.-1) then; hconf2 = trim(hconf2)//'[+]'
          elseif (he(newleg(j,pr),i,pr).eq. 0) then; hconf2 = trim(hconf2)//'[0]'
          elseif (he(newleg(j,pr),i,pr).eq.+1) then; hconf2 = trim(hconf2)//'[-]'
          endif
        enddo

        l = len(trim(hconf1)//' ->'//trim(hconf2))

        if (l.gt.75) then
          write(nx,*) ' Helicity configuration:'
          write(nx,*) trim(hconf1),' ->'
          write(nx,*) trim(hconf2)
        elseif (l.gt.75-27) then
          write(nx,*) ' Helicity configuration:'
          write(nx,*) trim(hconf1),' ->',trim(hconf2)
        else
          write(nx,*) ' Helicity configuration:  ', &
                      trim(hconf1),' ->',trim(hconf2)
        endif
        write(nx,*)

        if (computeNLO .and.                             &
            sum(abs(matrix2h(1:oi2Size(pr),i,0,prin))) .ne. 0d0 ) then

          if (zeroLO(pr)) then
            write(nx,*) '      |        | A0h |^2      ',  &
                             ' |        | A1h |^2      |', trim(order_ids)
          else
            write(nx,*) '      |        | A0h |^2      ',  &
                             ' |  2*Re{ A1h * A0h^* }  |', trim(order_ids)
          endif
          write(nx,*) '      ---------------------------------', &
                                            '-----------------'
          do oi = 1,oi2Size(pr)
            write(nx,'(6x," | ",e21.14,1x," | ",e21.14, " |",*(2x,i2))') &
                     matrix2h(oi,i,0,prin),matrix2h(oi,i,4,prin),oi2_reg(:,oi,pr)
          enddo
          write(nx,*) '      ---------------------------', &
                                '-----------------------'
          write(nx,'( "   SUM | ",e21.14,1x,   " | ",e21.14, " |")') &
                     sum(matrix2h(1:oi2Size(pr),i,0,prin)),            &
                     sum(matrix2h(1:oi2Size(pr),i,4,prin))
          write(nx,*)

        elseif ( sum(abs(matrix2h(1:oi2Size(pr),i,0,prin))).ne.0d0 ) then

          write(nx,*) '      |        | A0h |^2       |', trim(order_ids)
          write(nx,*) '      -------------------------'
          do oi = 1,oi2Size(pr)
            write(nx,'(6x," | ",e21.14, "  |",*(2x,i2))') matrix2h(oi,i,0,prin), oi2_reg(:,oi,pr)
          enddo
          write(nx,*) '      --------------------------'
          write(nx,'( "   SUM | ",e21.14, "  |")') sum(matrix2h(1:oi2Size(pr),i,0,prin))
          write(nx,*)

        endif

        write(nx,*)

      enddo h2loop

    endif

    select case (n)
    case (-99:-10); fmt = '(1x,a,i3,a)'
    case (-9:-1);   fmt = '(1x,a,i2,a)'
    case (0:9);     fmt = '(1x,a,i1,a)'
    case default
    end select

    write(nx,'(1x,75("-"))')
    write(nx,*)
    write(nx,*)

    write(nx,trim(fmt)) ' UNPOLARIZED SQUARED AMPLITUDE [GeV^',n,']'
    write(nx,*)

    if (.not. computeNLO) then

      write(nx,*) '      |        | A0 |^2       |', trim(order_ids)
      write(nx,*) '      ------------------------'
      do oi = 1,oi2Size(pr)
        write(nx,'(6x," | ",e21.14, " |",*(2x,i2))') matrix2(oi,0,prin), oi2_reg(:,oi,pr)
      enddo
      write(nx,*) '      -------------------------'
      write(nx,'( "   SUM | ",e21.14, " |")') sum(matrix2(1:oi2Size(pr),0,prin))
      write(nx,*)

    else

      if (zeroLO(pr)) then
        write(nx,*) '      |        | A0 |^2       ', &
                         ' |        | A1 |^2       |', trim(order_ids)
      else
        write(nx,*) '      |        | A0 |^2       ', &
                         ' |   2*Re{ A1 * A0^* }   |', trim(order_ids)
      endif
      write(nx,*) '      --------------------------------', &
                                      '------------------'
      do oi = 1,oi2Size(pr)
        write(nx,'(6x," | ",e21.14,1x," | ",e21.14, " |",*(2x,i2))') &
                 matrix2(oi,0,prin),matrix2(oi,4,prin),oi2_reg(:,oi,pr)
      enddo
      write(nx,*) '      --------------------------', &
                          '------------------------'
      write(nx,'( "   SUM | ",e21.14,1x,   " | ",e21.14, " |")') &
                 sum(matrix2(1:oi2Size(pr),0,prin)),             &
                 sum(matrix2(1:oi2Size(pr),4,prin))
      write(nx,*)

      if (writeMat2.ge.2) then
        if (zeroLO(pr)) then
          write(nx,*) '      |     | A1d4 |^2    ',  &
                           ' |     | A1ct |^2    ', &
                           ' |     | A1r2 |^2     |', trim(order_ids)
        else
          write(nx,*) '      |   2*Re{A1d4*A0^*} ',  &
                           ' |   2*Re{A1ct*A0^*} ', &
                           ' |   2*Re{A1r2*A0^*}  |', trim(order_ids)
        endif
        write(nx,*)  '      ----------------------', &
                            '---------------------', &
                            '---------------------'
        do oi = 1,oi2Size(pr)

          write(nx,'(6x," | ",e18.11," | ",e18.11," | ",e18.11, " |",*(2x,i2))') &
                   matrix2(oi,1,prin),matrix2(oi,2,prin),matrix2(oi,3,prin),     &
                   oi2_reg(:,oi,pr)
        enddo
        write(nx,*) '      ----------------------', &
                           '---------------------', &
                           '---------------------'
        write(nx,'( "   SUM | ",e18.11," | ",e18.11," | ",e18.11, " |")') &
                 sum(matrix2(1:oi2Size(pr),1,prin)),                 &
                 sum(matrix2(1:oi2Size(pr),2,prin)),                 &
                 sum(matrix2(1:oi2Size(pr),3,prin))
        write(nx,*)
        if (zeroLO(pr)) then
          write(nx,*) '      |  2*Re{A1d4*A1ct^*} ', &
                            '|  2*Re{A1ct*A1r2^*} ', &
                            '|  2*Re{A1r2*A1d4^*} |'
          write(nx,*) '      ---------------------', &
                            '---------------------', &
                            '---------------------'
          do oi = 1,oi2Size(pr)
            write(nx,'(6x," | ",e18.11," | ",e18.11," | ",e18.11, " |",*(2x,i2))')   &
                   matrix2int(oi,1,2,prin),matrix2int(oi,2,3,prin), &
                   matrix2int(oi,3,1,prin),oi2_reg(:,oi,pr)
          enddo
          write(nx,*) '      ---------------------', &
                            '---------------------', &
                            '---------------------'
          write(nx,'( "   SUM | ",e18.11," | ",e18.11," | ",e18.11, " |")') &
                   sum(matrix2int(1:oi2Size(pr),1,2,prin)),            &
                   sum(matrix2int(1:oi2Size(pr),2,3,prin)),            &
                   sum(matrix2int(1:oi2Size(pr),3,1,prin))
          write(nx,*)
        endif
      endif

    endif

    write(nx,*)
    write(nx,'(1x,75("x"))')
    write(nx,*)
    write(nx,*)

  endif

  end subroutine print_squared_amplitude

!------------------------------------------------------------------------------!

  subroutine rescale_amplitude (prin,order)
  use input_rcl, only: get_mu_uv_rcl,get_delta_uv_rcl

  integer,          intent(in) :: prin
  character(len=*), intent(in) :: order

  integer            :: pr,i,cs,gspower,oi,oil
  logical            :: computeNLO
  real(dp)           :: alsRatio,DeltaUV,muUV
  complex(dp)        :: fac,DdZgs,dZgsT

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
  else
    pr = prin
  end if

  ! determine which order position is qcd (only done once)
  if (qcd_order_id .eq. -1) then
    do i = 1, n_orders
      if(trim(adjustl(get_order_id_mdl(i))) .eq. 'QCD') then
        qcd_order_id = i
      endif
    end do
    if (qcd_order_id .eq. -1) then
      call error_rcl('Unable to find `QCD` among fundamental orders.', &
                     where='rescale_amplitude(internal)')
    endif
  endif

  computeNLO = (order.eq.'NLO').and.(lpmax(pr).gt.0)

  if((als  .ne. als0R(0,prin))  .or. &
     (Qren .ne. Qren0R(0,prin)) .or. &
     (Nlq  .ne. Nlq0R(0,prin))) then
    alsRatio = als/als0R(0,prin)
    ! Tree-level
    do oi = 1,oiSize(pr)
      if (.not. compoi(oi,0,pr)) cycle
      gspower = oi_reg(qcd_order_id,oi,pr)
      if (gspower .gt. 0) then
        fac = sqrt(alsRatio)**gspower
        matrix(:,oi,:,0,prin) = matrix(:,oi,:,0,prin) * fac
      end if
    enddo
    als0R(0,prin)  = als
    Qren0R(0,prin) = Qren
    Nlq0R(0,prin)  = Nlq

    ! Loop-level
    if(computeNLO .and.                 &
       ((als  .ne. als0R(1,prin))  .or. &
        (Qren .ne. Qren0R(1,prin)) .or. &
        (Nlq  .ne. Nlq0R(1,prin)))) then
      call get_delta_uv_rcl(DeltaUV)
      call get_mu_uv_rcl(muUV)
      alsRatio = als/als0R(1,prin)

      dZgsT = dzgs(als,Nlq,DeltaUV,Qren,muUV)

      ! Difference DdZgs = dZgs - alsRatio*dZgs0R(pr)
      DdZgs = dZgsT - alsRatio*dZgs0R(prin)

      ! Rescaling of alphaS
      do oi = 1,oiSize(pr)
        if (.not. compoi(oi,1,pr)) cycle
        gspower = oi_reg(qcd_order_id,oi,pr)
        if (gspower .gt. 0) then
          fac = sqrt(alsRatio)**gspower
          matrix(:,oi,:,1:3,prin) = matrix(:,oi,:,1:3,prin) * fac
        endif
      enddo

      ! Rescaling on the CT amplitude due to dZgsT
      do oi = 1,oiSize(pr)
        if (.not. compoi(oi,0,pr)) cycle
        do oil = 1,oiSize(pr)
          if (.not. compoi(oil,1,pr)) cycle
          if (oi_reg(qcd_order_id,oi,pr)+2 .ne. &
              oi_reg(qcd_order_id,oil,pr)) cycle
          gspower = oi_reg(qcd_order_id,oi,pr)
          if (gspower .gt. 0) then
            matrix(:,oil,:,2,prin) = &
            matrix(:,oil,:,2,prin) + matrix(:,oi,:,0,prin) * gspower * DdZgs
          end if
        enddo
      enddo

      als0R(1,prin)  = als
      Qren0R(1,prin) = Qren
      Nlq0R(1,prin)  = Nlq
      dZgs0R(prin)  = dZgsT
    endif
  endif

  if (computeNLO) then
    do i = 1, cfTot(pr)
      do oi = 1,oiSize(pr)
        if (.not. compoi(oi,1,pr)) cycle
        do cs = 1,csTot(pr)
          matrix(cs,oi,i,4,prin) = sum(matrix(cs,oi,i,1:3,prin))
        enddo
      enddo
    enddo
  endif

  end subroutine rescale_amplitude

!------------------------------------------------------------------------------!

  subroutine print_rescaling

  call openOutput

  if ((als .ne. als0).or.(Qren .ne. Qren0).or.(Nlq .ne. Nlq0)) then
    if (Nlq .eq. -1) then
      write(nx,'(2x,a)') &
      'alpha_s Renormalization Scheme: Variable flavours Scheme'
    else
      write(nx,'(2x,a,i1,a)') &
      'alpha_s Renormalization Scheme: ', Nlq, '-flavours Scheme'
    endif
    write(nx,'(2x,a,g21.14,7x,a,g21.14,a)') &
    'alpha_s(Q) =',als,'Q =',Qren,' GeV'
    write(nx,*)
  endif
  write(nx,*)

  end subroutine print_rescaling

!------------------------------------------------------------------------------!

  subroutine print_squared_amplitude_cc (prin,i1,i2,order)

  integer,          intent(in) :: prin,i1,i2
  character(len=*), intent(in) :: order

  integer           :: pr,legs,n,i,j1,j2,oi2
  character(2)      :: k,l
  character(99)     :: a2,fmt
  logical           :: computeNLO,check
  character(len=20) :: order_ids

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
    j1 = newleg(prs(prin)%relperm(i1),pr)
    j2 = newleg(prs(prin)%relperm(i2),pr)
  else
    pr = prin
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
  end if

  computeNLO = (order .eq. 'NLO') .and. (lpmax(pr) .gt. 0)

  legs = prs(pr)%legs
  n = 8 - 2*legs

  order_ids = ' '
  do i = 1, n_orders
    order_ids = trim(order_ids)//' '//trim(adjustl(get_order_id_mdl(i)))
  end do

  if (computeNLO) then
    check = sum(abs(matrix2ccnlo(1:oi2Size(pr),j1,j2,prin))).ne.0d0
  else
    check = sum(abs(matrix2cc(1:oi2Size(pr),j1,j2,prin))).ne.0d0
  end if

  if (check) then
    call openOutput

    select case (n)
    case (-99:-10); fmt = '(1x,a,i3,a)'
    case (-9:-1);   fmt = '(1x,a,i2,a)'
    case (0:9);     fmt = '(1x,a,i1,a)'
    case default
    end select

    write(nx,'(1x,75("-"))')
    write(nx,*)
    write(nx,*)
    if (computeNLO) then
      write(nx,trim(fmt)) &
        'NLO COLOUR-CORRELATED SQUARED AMPLITUDE [GeV^',n,']'
    else
      write(nx,trim(fmt)) &
        ' COLOUR-CORRELATED SQUARED AMPLITUDE [GeV^',n,']'
    end if
    write(nx,*)

    write(k,'(i2)') i1
    write(l,'(i2)') i2

    if (computeNLO) then
      a2 = '|  A1c('//trim(adjustl(k))//','//trim(adjustl(l))//') |^2'
    else
      a2 = '|  A0c('//trim(adjustl(k))//','//trim(adjustl(l))//') |^2'
    end if

    write(nx,*) '      |    '//trim(a2)//'    |', trim(order_ids)
    write(nx,*) '      -------------------------'
    do oi2 = 1,oi2Size(pr)
      if (computeNLO) then
        write(nx,'(6x," | ",e21.14, " |",*(2x,i2))') matrix2ccnlo(oi2,j1,j2,prin), &
                                                     oi2_reg(:,oi2,pr)
      else
        write(nx,'(6x," | ",e21.14, " |",*(2x,i2))') matrix2cc(oi2,j1,j2,prin), &
                                                     oi2_reg(:,oi2,pr)
     end if
    enddo
    write(nx,*) '      -------------------------'
    if (computeNLO) then
      write(nx,'( "   SUM | ",e21.14, " |")') sum(matrix2ccnlo(1:oi2Size(pr),j1,j2,prin))
    else
      write(nx,'( "   SUM | ",e21.14, " |")') sum(matrix2cc(1:oi2Size(pr),j1,j2,prin))
    end if
    write(nx,*)
    write(nx,*)

  endif

  end subroutine print_squared_amplitude_cc

!------------------------------------------------------------------------------!

  subroutine print_squared_amplitude_cc_int (prin,i1,i2)

  integer, intent (in) :: prin,i1,i2

  integer       :: pr,legs,n,i,j1,j2,oi2
  character(2)  :: k,l
  character(99) :: a2,fmt
  character(20) :: order_ids

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
  else
    pr = prin
  end if

  legs = prs(pr)%legs
  n = 8 - 2*legs

  order_ids = ' '
  do i = 1, n_orders
    order_ids = trim(order_ids)//' '//trim(adjustl(get_order_id_mdl(i)))
  end do

  j1 = newleg(i1,pr)
  j2 = newleg(i2,pr)

  if (sum(abs(matrix2ccint(1:oi2Size(pr),j1,j2,prin))).ne.0d0) then

    call openOutput

    select case (n)
    case (-99:-10); fmt = '(1x,a,i3,a)'
    case (-9:-1);   fmt = '(1x,a,i2,a)'
    case (0:9);     fmt = '(1x,a,i1,a)'
    case default
    end select

    write(nx,'(1x,75("-"))')
    write(nx,*)
    write(nx,*)
    write(nx,trim(fmt)) &
      ' COLOUR-CORRELATED SQUARED AMPLITUDE [GeV^',n,']'
    write(nx,*)

    write(k,'(i2)') i1
    write(l,'(i2)') i2

    a2 = '|  A1c('//trim(adjustl(k))//','//trim(adjustl(l))//') |^2'

    write(nx,*) '      |   '//trim(a2)//'   |', trim(order_ids)
    write(nx,*) '      -------------------------'
    do oi2 = 1,oi2Size(pr)
      write(nx,'(6x," | ",e21.14, " |",*(2x,i2))') matrix2ccint(oi2,j1,j2,prin), &
                                                   oi2_reg(:,oi2,pr)
    enddo
    write(nx,*) '      -------------------------'
    write(nx,'( "   SUM | ",e21.14, " |")') sum(matrix2ccint(1:oi2Size(pr),j1,j2,prin))
    write(nx,*)
    write(nx,*)

  endif

  end subroutine print_squared_amplitude_cc_int

!------------------------------------------------------------------------------!

  subroutine print_squared_amplitude_scc (prin,i1,i2,v,order)

  integer,          intent(in) :: prin,i1,i2
  complex(dp),      intent(in) :: v(0:)
  character(len=*), intent(in) :: order

  integer           :: pr,legs,n,i,j1,j2,oi2
  character(2)      :: k,l
  character(99)     :: a2,fmt
  logical           :: computeNLO,check
  character(len=20) :: order_ids

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
    j1 = newleg(prs(prin)%relperm(i1),pr)
    j2 = newleg(prs(prin)%relperm(i2),pr)
  else
    pr = prin
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
  end if

  computeNLO = (order .eq. 'NLO') .and. (lpmax(pr) .gt. 0)

  legs = prs(pr)%legs
  n = 8 - 2*legs

  order_ids = ' '
  do i = 1, n_orders
    order_ids = trim(order_ids)//' '//trim(adjustl(get_order_id_mdl(i)))
  end do

  if (computeNLO) then
    check = sum(abs(matrix2sccnlo(1:oi2Size(pr),j1,j2,prin))).ne.0d0
  else
    check = sum(abs(matrix2scc(1:oi2Size(pr),j1,j2,prin))).ne.0d0
  end if

  if (check) then
    call openOutput

    select case (n)
    case (-99:-10); fmt = '(1x,a,i3,a)'
    case (-9:-1);   fmt = '(1x,a,i2,a)'
    case (0:9);     fmt = '(1x,a,i1,a)'
    case default
    end select

    write(nx,'(1x,75("-"))')
    write(nx,*)
    write(nx,*)
    if (computeNLO) then
      write(nx,trim(fmt)) &
        ' NLO SPIN- AND COLOUR-CORRELATED SQUARED AMPLITUDE [GeV^',n,']'
    else
      write(nx,trim(fmt)) &
        ' SPIN- AND COLOUR-CORRELATED SQUARED AMPLITUDE [GeV^',n,']'
    end if
    write(nx,*)

    write(k,'(i2)') i1
    write(l,'(i2)') i2

    write(nx,*) ' Polarization vector v for particle ',trim(adjustl(k)),':'
    write(nx,*) ' v(0) = ',v(0)
    write(nx,*) ' v(1) = ',v(1)
    write(nx,*) ' v(2) = ',v(2)
    write(nx,*) ' v(3) = ',v(3)
    write(nx,*)

    if (computeNLO) then
      a2 = '| A1sc('//trim(adjustl(k))//','//trim(adjustl(l))//') |^2'
    else
      a2 = '| A0sc('//trim(adjustl(k))//','//trim(adjustl(l))//') |^2'
    end if

    write(nx,*) '      |    '//trim(a2)//'    |', trim(order_ids)
    write(nx,*) '      -------------------------'
    do oi2 = 1,oi2Size(pr)
      if (computeNLO) then
        write(nx,'(6x," | ",e21.14, " |",*(2x,i2))') matrix2sccnlo(oi2,j1,j2,prin), &
                                                     oi2_reg(:,oi2,pr)
      else
        write(nx,'(6x," | ",e21.14, " |",*(2x,i2))') matrix2scc(oi2,j1,j2,prin), &
                                                     oi2_reg(:,oi2,pr)
     end if
    enddo
    write(nx,*) '      -------------------------'
    if (computeNLO) then
      write(nx,'( "   SUM | ",e21.14, " |")') sum(matrix2sccnlo(1:oi2Size(pr),j1,j2,prin))
    else
      write(nx,'( "   SUM | ",e21.14, " |")') sum(matrix2scc(1:oi2Size(pr),j1,j2,prin))
    end if
    write(nx,*)
    write(nx,*)
    write(nx,'(1x,75("x"))')
    write(nx,*)
    write(nx,*)

  endif

  end subroutine print_squared_amplitude_scc

!------------------------------------------------------------------------------!

  subroutine print_squared_amplitude_sc (prin,j,v,order)

  integer,          intent(in) :: prin,j
  complex(dp),      intent(in) :: v(0:)
  character(len=*), intent(in) :: order

  integer           :: pr,legs,n,i,oi2
  character(2)      :: cj
  character(99)     :: a2,fmt
  character(len=20) :: order_ids
  logical           :: computeNLO,check

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
  else
    pr = prin
  end if

  computeNLO = (order .eq. 'NLO') .and. (lpmax(pr) .gt. 0)

  legs = prs(pr)%legs
  n = 8 - 2*legs

  order_ids = ' '
  do i = 1, n_orders
    order_ids = trim(order_ids)//' '//trim(adjustl(get_order_id_mdl(i)))
  end do

  if (computeNLO) then
    check = sum(abs(matrix2scnlo(1:oi2Size(pr),prin))).ne.0d0
  else
    check = sum(abs(matrix2sc(1:oi2Size(pr),prin))).ne.0d0
  end if

  if (check) then
    call openOutput

    select case (n)
    case (-99:-10); fmt = '(1x,a,i3,a)'
    case (-9:-1);   fmt = '(1x,a,i2,a)'
    case (0:9);     fmt = '(1x,a,i1,a)'
    case default
    end select

    write(nx,'(1x,75("-"))')
    write(nx,*)
    write(nx,*)
    if (computeNLO) then
      write(nx,trim(fmt)) ' NLO SPIN-CORRELATED SQUARED AMPLITUDE [GeV^',n,']'
    else
      write(nx,trim(fmt)) ' SPIN-CORRELATED SQUARED AMPLITUDE [GeV^',n,']'
    end if
    write(nx,*)

    write(cj,'(i2)') j

    write(nx,*) ' Polarization vector v for particle ',trim(adjustl(cj)),':'
    write(nx,*) ' v(0) = ',v(0)
    write(nx,*) ' v(1) = ',v(1)
    write(nx,*) ' v(2) = ',v(2)
    write(nx,*) ' v(3) = ',v(3)
    write(nx,*)

    if (computeNLO) then
      a2 = '|  A1s('//trim(adjustl(cj))//')  |^2'
    else
      a2 = '|  A0s('//trim(adjustl(cj))//')  |^2'
    end if

    write(nx,*) '      |     '//trim(a2)//'    |', trim(order_ids)
    write(nx,*) '      -------------------------'
    do oi2 = 1,oi2Size(pr)
      if (computeNLO) then
        write(nx,'(6x," | ",e21.14, " |",*(2x,i2))') matrix2scnlo(oi2,prin), &
                                                     oi2_reg(:,oi2,pr)
      else
        write(nx,'(6x," | ",e21.14, " |",*(2x,i2))') matrix2sc(oi2,prin), &
                                                     oi2_reg(:,oi2,pr)
      end if
    enddo
    write(nx,*) '      -------------------------'
    if (computeNLO) then
      write(nx,'( "   SUM | ",e21.14, " |")') sum(matrix2scnlo(1:oi2Size(pr),prin))
    else
      write(nx,'( "   SUM | ",e21.14, " |")') sum(matrix2sc(1:oi2Size(pr),prin))
    end if

    write(nx,*)
    write(nx,*)
    write(nx,'(1x,75("x"))')
    write(nx,*)
    write(nx,*)
  end if

  end subroutine print_squared_amplitude_sc

!------------------------------------------------------------------------------!

  subroutine print_squared_amplitude_scm (prin,j,order)

  integer,          intent(in) :: prin,j
  character(len=*), intent(in) :: order

  integer           :: pr,legs,n,i,oi2
  character(2)      :: cj
  character(99)     :: a2,fmt
  character(len=20) :: order_ids
  logical           :: computeNLO,check

  if (prs(prin)%crosspr .ne. 0) then
    pr = prs(prin)%crosspr
  else
    pr = prin
  end if

  computeNLO = (order .eq. 'NLO') .and. (lpmax(pr) .gt. 0)

  legs = prs(pr)%legs
  n = 8 - 2*legs

  order_ids = ' '
  do i = 1, n_orders
    order_ids = trim(order_ids)//' '//trim(adjustl(get_order_id_mdl(i)))
  end do

  if (computeNLO) then
    ! TODO:  <28-12-18, J.-N. Lang> !
    !check = sum(abs(matrix2scnlo(1:oi2Size(pr),prin))).ne.0d0
  else
    check = sum(abs(matrix2scm(:,:,1:oi2Size(pr),prin))) .ne. 0d0
  end if

  if (check) then
    call openOutput

    select case (n)
    case (-99:-10); fmt = '(1x,a,i3,a)'
    case (-9:-1);   fmt = '(1x,a,i2,a)'
    case (0:9);     fmt = '(1x,a,i1,a)'
    case default
    end select

    write(nx,'(1x,75("-"))')
    write(nx,*)
    write(nx,*)
    if (computeNLO) then
      write(nx,trim(fmt)) ' NLO SPIN-CORRELATED MATRIX SQUARED AMPLITUDE [GeV^',n,']'
    else
      write(nx,trim(fmt)) ' SPIN-CORRELATED MATRIX SQUARED AMPLITUDE [GeV^',n,']'
    end if
    write(nx,*)

    write(cj,'(i2)') j

    write(nx,*) ' Polarization for particle ',trim(adjustl(cj))

    if (computeNLO) then
      a2 = '|  A1sm_'//trim(adjustl(cj))//'(0:3,0:3)  |^2'
    else
      a2 = '|  A0sm_'//trim(adjustl(cj))//'(0:3,0:3)  |^2'
    end if

    write(nx,*) '      |                    '//trim(a2)// &
                '                  |', trim(order_ids)
    write(nx,*) '      --------------------------------' // &
                '-------------------------------'
    do oi2 = 1,oi2Size(pr)
      if (computeNLO) then
        ! TODO:  <28-12-18, J.-N. Lang> !
        !write(nx,'(6x," | ",e21.14, " |",*(2x,i2))') matrix2scnlo(oi2,prin), &
        !                                             oi2_reg(:,oi2,pr)
      else
        write(nx,'(6x," | ",e14.8,1x,e14.8,1x,e14.8,1x,e14.8," |",*(2x,i2))') &
          matrix2scm(0,0,oi2,prin), matrix2scm(0,1,oi2,prin), &
          matrix2scm(0,2,oi2,prin), matrix2scm(0,3,oi2,prin), oi2_reg(:,oi2,pr)
        write(nx,'(6x," | ",e14.8,1x,e14.8,1x,e14.8,1x,e14.8," |")') &
          matrix2scm(1,0,oi2,prin), matrix2scm(1,1,oi2,prin), &
          matrix2scm(1,2,oi2,prin), matrix2scm(1,3,oi2,prin)
        write(nx,'(6x," | ",e14.8,1x,e14.8,1x,e14.8,1x,e14.8," |")') &
          matrix2scm(2,0,oi2,prin), matrix2scm(2,1,oi2,prin), &
          matrix2scm(2,2,oi2,prin), matrix2scm(2,3,oi2,prin)
        write(nx,'(6x," | ",e14.8,1x,e14.8,1x,e14.8,1x,e14.8," |")') &
          matrix2scm(3,0,oi2,prin), matrix2scm(3,1,oi2,prin), &
          matrix2scm(3,2,oi2,prin), matrix2scm(3,3,oi2,prin)
      end if
    write(nx,*) '      --------------------------------' // &
                '-------------------------------'
    end do
    if (oi2Size(pr) .gt. 0) then
    if (computeNLO) then
      ! TODO:  <28-12-18, J.-N. Lang> !
    else
      write(nx,'( "   SUM | ",e14.8,1x,e14.8,1x,e14.8,1x,e14.8," |")') &
          sum(matrix2scm(0,0,1:oi2Size(pr),prin)), &
          sum(matrix2scm(0,1,1:oi2Size(pr),prin)), &
          sum(matrix2scm(0,2,1:oi2Size(pr),prin)), &
          sum(matrix2scm(0,3,1:oi2Size(pr),prin))
      write(nx,'( "       | ",e14.8,1x,e14.8,1x,e14.8,1x,e14.8," |")') &
          sum(matrix2scm(1,0,1:oi2Size(pr),prin)), &
          sum(matrix2scm(1,1,1:oi2Size(pr),prin)), &
          sum(matrix2scm(1,2,1:oi2Size(pr),prin)), &
          sum(matrix2scm(1,3,1:oi2Size(pr),prin))
      write(nx,'( "       | ",e14.8,1x,e14.8,1x,e14.8,1x,e14.8," |")') &
          sum(matrix2scm(2,0,1:oi2Size(pr),prin)), &
          sum(matrix2scm(2,1,1:oi2Size(pr),prin)), &
          sum(matrix2scm(2,2,1:oi2Size(pr),prin)), &
          sum(matrix2scm(2,3,1:oi2Size(pr),prin))
      write(nx,'( "       | ",e14.8,1x,e14.8,1x,e14.8,1x,e14.8," |")') &
          sum(matrix2scm(3,0,1:oi2Size(pr),prin)), &
          sum(matrix2scm(3,1,1:oi2Size(pr),prin)), &
          sum(matrix2scm(3,2,1:oi2Size(pr),prin)), &
          sum(matrix2scm(3,3,1:oi2Size(pr),prin))
    end if
    write(nx,*) '      --------------------------------' // &
                '-------------------------------'

    end if

    write(nx,*)
    write(nx,*)
    write(nx,'(1x,75("x"))')
    write(nx,*)
    write(nx,*)
  end if

  end subroutine print_squared_amplitude_scm

!------------------------------------------------------------------------------!

end module amplitude_rcl

!------------------------------------------------------------------------------!
