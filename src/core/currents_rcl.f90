!******************************************************************************!
!                                                                              !
!    currents_rcl.f90                                                          !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module currents_rcl

!------------------------------------------------------------------------------!

  use globals_rcl
  use tables_rcl, only: vectorLeg,firstNumbers,riMax,pmi
  use skeleton_rcl, only: makeSkeleton,bLDef,wLDef,bLInc,wLInc,bMaxT,wMaxT, &
                          nDef,parT,csT,binT,hosT,hmaT,xxxT,wT,nMaxT,       &
                          typeT,permT,cbtypeT,cbT,orderT,orderIncT,         &
                          U1gT,couT,colourfacT,qflowT,qflowextT, &
                          noquarks,nogluons,noweaks
  use colourflow_rcl, only: fill_colordeltas, compute_colourcoefc
  use draw_current_rcl, only: texHead,texFeet,texPicture
  use memory_logger_rcl, only: log_memory
  use modelfile, only: get_max_polarizations_mdl,      &
                       get_particle_antiparticle_mdl,  &
                       get_particle_spin_mdl,          &
                       get_particle_colour_mdl,        &
                       get_particle_polarizations_mdl, &
                       get_particle_name_mdl,          &
                       get_particle_mass_reg_mdl,      &
                       get_particle_mass_id_mdl,       &
                       get_particle_cmass2_mdl,        &
                       get_particle_cmass2_reg_mdl,    &
                       get_particle_type2_mdl,         &
                       get_recola_base_mdl,            &
                       get_coupling_value_mdl,         &
                       get_propagator_extension_mdl,   &
                       is_resonant_particle_id_mdl,    &
                       get_n_particles_mdl,            &
                       is_particle_mdl,                &
                       helicity_conservation_mdl,      &
                       get_doublet_partner_mdl,        &
                       has_feature_mdl,                &
                       is_firstgen_mdl,                &
                       get_secondgen_mdl,              &
                       get_thirdgen_mdl,               &
                       form_current_mdl,               &
                       get_vertex_lorentz_rank_mdl,    &
                       get_n_masses_mdl,               &
                       get_max_leg_multiplicity_mdl,   &
                       get_colourfac_mdl,              &
                       get_colourfac_form_mdl,         &
                       get_n_orders_mdl
  use order_rcl, only: reg_od,compoi,oiSize,oi2Size,check_oi
  use form_rcl, only: formHead,formCurrents0,formCurrents1,formFeet, &
                      formVertex,formParticles,writecolorconfigs

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine generate_currents

  integer :: i,ii,j,jj,k,h,n,t,it,sav,pr,cs,                   &
             w0Def,w0Inc,w0eDef,confDef,cdsDef,csDef,conf,     &
             cd0s,cd1s,lmax,lmin,lmaxE,tlxm1,tlx,tlxp1,tln,    &
             legs,b,w,base,tag,htag,htt,ferhelsum,dl,cdsMax,   &
             tlm1,tl,rep,ti,tiDef,h1,ih1,lp,legsE,lexp,        &
             npoint,fh,fhewN,eN,wN,pN,c,firstlastloopj,        &
             check,s,sm0,sd0,sm1,sd1,bm0,bd0,bm1,bd1,sm0Tot,   &
             sd0Tot,sm1Tot,sd1Tot,floop,ghostloopsign,         &
             ferloopsign,fersign,                              &
             signcorrection,w0,xIn,kbin,peak,lp0,lp0step,      &
             t0,i0,j0,j2,i0max,check0,j1,wTj1,wTj2,daj1,moj2,  &
             daj2,rankInc,rankIn,ranktiMax,step,hm0,ng,        &
             dCs0Tot,dCsTot,iqk,iak,startw0,startbm0,startbd0, &
             ew0,ebm0,ebd0,symfac,polfac,colfac,sign, oi_id,   &
             Nc_power,e1,e2,spin,color,firstgen,secondgen,     &
             thirdgen,max_polarizations
  integer, dimension(:), allocatable :: ee,pp,pbins,ppar
  integer, dimension(0:3) :: cuts_start, cuts_end
  integer, allocatable :: park(:),helk(:),many(:),                   &
                          w0min(:,:,:,:,:),w0max(:,:,:,:,:),         &
                          wMax(:),bMax(:),                           &
                          massti(:),ferp(:),fervec(:,:),csign(:,:),  &
                          csw0(:,:),xxxw0(:),orderw0(:,:),rankwT(:), &
                          fheT(:),sm0j(:),sd0j(:),sm1j(:),sd1j(:),   &
                          jfirstwT(:),jinitwT(:),ej(:),csj(:),tij(:),&
                          wtTOw0b(:),wtTOw1b(:,:),w1stot(:),         &
                          w1ltot(:,:),c0Eff(:,:,:),cEff(:,:,:),      &
                          bm1c(:),bd1c(:),bm1h1(:,:,:),bd1h1(:,:,:), &
                          rankInj(:),rankOutj(:),peakleg(:),         &
                          daj(:),moj(:),jdaTOjmo(:),startbm1(:),     &
                          startbd1(:),ebm1(:),ebd1(:),rankwMax(:),   &
                          pCs(:,:),dCs0(:,:),dCs(:,:),               &
                          parw0(:),binw0(:)

  logical :: xs,new_cut=.false.,new_cs,new_current,ml,last_tl, &
             is_daughter,is_se,init_is_daughter,init_is_se,    &
             init_cond1,init_cond2,init_cond3,colT_imag,zerobr

  ! flags for current helicities
  integer, parameter :: unpol = 0
  integer, parameter :: zero_current = 3
  integer, parameter :: not_set = 111

  logical, allocatable :: checkHel(:,:),compConf(:),comp0(:,:,:),comp1(:,:,:), &
                          okwti(:,:),U1gw0(:),winitj(:),jfilter(:),            &
                          fhfilter(:),compw(:),cou(:)

  real (dp), allocatable      :: dCs0fac(:),dCsfac(:)

  complex (dp)                :: fac,fac1,ratio,col1,col2
  complex(dp), allocatable    :: facj(:)

  character(len=20) :: colT_symbol
  character(len=10) :: log_info
  character(2)      :: cc


  bLDef = 500000; bLInc = 500000
  wLDef = 400000; wLInc = 400000
  w0Def = 300000; w0Inc = 300000

  if (log_mem) call log_memory('generate_currents')

  lmax = maxval(prs(:)%legs)
  lmin = minval(prs(:)%legs)

  n_particles = get_n_particles_mdl()
  n_masses = get_n_masses_mdl()
  n_orders = get_n_orders_mdl()
  nDef = get_max_leg_multiplicity_mdl()
  max_polarizations = get_max_polarizations_mdl()

  allocate(ee(1:nDef))
  allocate(pp(1:nDef))

  if (.not. allocated(minlmu)) then
    allocate(minlmu(-1:1, n_particles))
  end if

  if (.not. allocated(maxlmu)) then
    allocate(maxlmu(-1:1, n_particles))
  end if

  if (.not. allocated(fhmin)) then
    allocate(fhmin(n_particles))
    fhmin = 0
  end if

  if (.not. allocated(fhmax)) then
    allocate(fhmax(n_particles))
    fhmax = 0
  end if

  if (loopMax) then
    lmaxE = lmax + 2
  else
    lmaxE = lmax
  end if

  w0eDef = 0
  confDef = 0
  cdsDef = 0
  csDef = 0
  do pr = 1,prTot
    w0 = 0   ! maximal number of external currents
    conf = 1 ! maximal number of helicity configurations
    cd0s = 0 ! maximal number of color deltas in each
             ! color structure of tree lines
    do i = 1, prs(pr)%legs
      w0 = w0 + get_particle_polarizations_mdl(prs(pr)%par(i))
      conf = conf * get_particle_polarizations_mdl(prs(pr)%par(i))
    end do

    ! count the number of color deltas:
    ! each fermion pair and gluon contribute one color delta
    ! -> count the number of positive color charges
    do i = 1, prs(pr)%legs
      if (get_particle_colour_mdl(prs(pr)%par(i)) .gt. 1) then
        cd0s = cd0s +1
      end if
    end do
    if (prs(pr)%loop) then
      cd1s = cd0s + 2
    else
      cd1s = cd0s
    endif
    cs = 1 ! number of final color structures is given by cs = cd0s!
    do i = 1, cd0s
      cs = cs * i
    end do
    w0eDef  = max(w0eDef, w0)
    confDef = max(confDef, conf)
    cdsDef = max(cdsDef, cd1s)
    csDef = max(csDef, cs)
  end do

  call external_field_types

  tlxm1 = pow2(lmax-1)
  tlx = pow2(lmax)
  tlxp1 = pow2(lmax+1)
  tln = pow2(lmin)

  allocate (factor(prTot))

  allocate (newleg(lmax ,      prTot))
  allocate (oldleg(lmax ,      prTot))

  allocate (newbin  (tlx-1,prTot))
  allocate (oldbin  (tlx-1,prTot))
  allocate (defp2bin(tlx-1,prTot)); defp2bin = .false.
  allocate (p2bin   (tlx-1,prTot)); p2bin = 0d0
  allocate (defresbin(tlx-1,prTot)); defresbin = .false.
  allocate (pspbin   (tlx-1,prTot)); pspbin = 0d0

  allocate (  mONS(lmax,prTot))
  allocate (cmONS2(lmax,prTot))
  allocate (cmREG2(lmax,prTot))

  allocate (cfTot(prTot)); cfTot=0
  allocate (csTot(prTot)); csTot=0
  allocate (pCsTot(prTot))
  allocate ( csAmp( lmax,csDef,prTot))
  allocate ( csIq( lmax,csDef,prTot))
  allocate (  nAmp(      csDef,prTot))
  allocate (  pAmp(csDef,csDef,prTot))
  allocate (facAmp(csDef,csDef,prTot))

  allocate (w0eTot  (             prTot))
  allocate (he      (lmax,confDef,prTot))
  allocate (dualconf(lmax,confDef,prTot))

  allocate (lpmax (prTot))
  allocate (cd0sMax(prTot))

  if (loopMax) allocate (loopCoef(n_particles,prTot))

  allocate (   w0Tot(prTot))
  allocate (   w1Tot(prTot))
  w0Tot = 0
  w1Tot = 0

  allocate (w0last(  -1:1,prTot))
  allocate (parw0e(w0eDef,prTot))
  allocate (binw0e(w0eDef,prTot))
  allocate (legw0e(w0eDef,prTot))
  allocate (helw0e(w0eDef,prTot))

  allocate (zeroLO(prTot))

  allocate (c0EffMax(prTot)); c0EffMax=0

  if (loopMax) then
    allocate (cEffMax(prTot))
    cEffMax = 0
  end if

  allocate (pm(prTot))
  pm(:)%bm0Tot = 0
  pm(:)%bd0Tot = 0
  pm(:)%bm1Tot = 0
  pm(:)%bd1Tot = 0

  allocate (modaTot(prTot))

  if (loopMax) then
    allocate (  tiTot(prTot))
    allocate (ritiMax(prTot))
  endif

  sm0Tot = 0
  sd0Tot = 0
  sm1Tot = 0
  sd1Tot = 0

  tiDef = 0

  allocate(oiSize(prTot))
  allocate(oi2Size(prTot))
  oiSize = 0
  oi2Size = 0

  ! The code is practically run twice.
  ! For sav=0, the dimensions of some allocatable global variables
  ! are computed and for sav=1 the varaibles selves are allocated and
  ! computed

  if (log_mem) call log_memory('@saveloop')

  saveloop: do sav = 0,1

    if (sav.eq.0) then
      allocate (wMax(prTot)); wMax = 0
      allocate (bMax(prTot)); bMax = 0
    endif

    if (sav.eq.1) then

      if (log_mem) call log_memory('@saveloop:sav=1 begin')
      allocate (mosm0(sm0Tot, prTot))
      allocate (binsm0(1:nDef, sm0Tot, prTot))
      allocate (parsm0(sm0Tot, prTot))
      allocate (xsm0(sm0Tot, prTot))
      allocate (gsIncsm0(n_orders, sm0Tot, prTot))
      allocate (cosm0(sm0Tot, prTot))
      allocate (gssm0(sm0Tot, prTot))
      allocate (cssm0(sm0Tot, prTot))

      allocate (dasd0(sd0Tot, prTot))
      allocate (sesd0(sd0Tot,prTot))
      allocate (facsd0(sd0Tot, prTot))
      allocate (gssd0(sd0Tot, prTot))
      allocate (cssd0(sd0Tot, prTot))

      if (loopMax) then
        allocate (mosm1(sm1Tot, prTot))
        allocate (binsm1(1:nDef, sm1Tot, prTot))
        allocate (parsm1(sm1Tot, prTot))
        allocate (gsIncsm1(n_orders, sm1Tot, prTot))
        allocate (cosm1(sm1Tot, prTot))
        allocate (rankInsm1(sm1Tot, prTot))
        allocate (rankOutsm1(sm1Tot, prTot))
        allocate (ferloopsm1(sm1Tot, prTot))
        allocate (gssm1(sm1Tot, prTot))
        allocate (cssm1(sm1Tot, prTot))
        allocate (tism1(sm1Tot, prTot))
      endif

      if (loopMax) then
        allocate (dasd1(sd1Tot, prTot))
        allocate (facsd1(sd1Tot, prTot))
        allocate (rankOutsd1(sd1Tot, prTot))
        allocate (ferloopsd1(sd1Tot, prTot))
        allocate (gssd1(sd1Tot, prTot))
        allocate (cssd1(sd1Tot, prTot))
        allocate (tisd1(sd1Tot, prTot))
      endif

      if (loopMax) then
        allocate (legsti(tiDef, prTot))
        allocate (momsti(lmax, tiDef, prTot))
        allocate (vmti(lmax, tiDef, prTot))
        allocate (rankti(0:tiDef, prTot)); rankti = 0
      endif

      allocate (c0TOlp(maxval(c0EffMax),prTot))

      do pr = 1, prTot
        allocate (pm(pr)%tree_m_maps(pm(pr)%bm0Tot))
        do bm0 = 1, pm(pr)%bm0Tot
          allocate (pm(pr)%tree_m_maps(bm0)%w_in(1:nDef-1))
        end do
        allocate (pm(pr)%tree_d_maps(pm(pr)%bd0Tot))
        if (loopMax) then
          allocate (pm(pr)%loop_m_maps(pm(pr)%bm1Tot))
          do bm1 = 1, pm(pr)%bm1Tot
            allocate (pm(pr)%loop_m_maps(bm1)%w_in(1:nDef-1))
          end do
          allocate (pm(pr)%loop_d_maps(pm(pr)%bd1Tot))
          allocate (pm(pr)%bm0(1:pow2(prs(pr)%legs)-1, &
                               cfTot(pr), c0EffMax(pr)))
          allocate (pm(pr)%bd0(1:pow2(prs(pr)%legs)-1, &
                               cfTot(pr), c0EffMax(pr)))
          allocate (pm(pr)%bm1(pow2(prs(pr)%legs)+1:   &
                               pow2(prs(pr)%legs+1)-1, &
                               cfTot(pr), cEffMax(pr)))
          allocate (pm(pr)%bd1(pow2(prs(pr)%legs)+1:   &
                               pow2(prs(pr)%legs+1)-1, &
                               cfTot(pr), cEffMax(pr)))
        else
          allocate (pm(pr)%bm0(1:pow2(prs(pr)%legs-1)-1, &
                               cfTot(pr), c0EffMax(pr)))
          allocate (pm(pr)%bd0(1:pow2(prs(pr)%legs-1)-1, &
                               cfTot(pr), c0EffMax(pr)))
        end if
      end do

      if (loopMax) then
        allocate (cTOt(maxval(cEffMax), prTot))
        allocate (cTOfh(maxval(cEffMax), prTot))
        allocate (cTOih1(maxval(cEffMax), prTot))
      endif

      if (loopMax) then
        allocate (w1TotMax(maxval(cEffMax), prTot))
        w1TotMax = 0
        allocate (riwMax(maxval(cEffMax), prTot))
      endif

      if (log_mem) call log_memory('@saveloop:sav=1 alloc end')
      do pr = 1, prTot
        if (prs(pr)%crosspr .eq. 0) then
          allocate (prs(pr)%colcoef(csTot(pr),csTot(pr)))
          allocate (prs(pr)%colcoefc(csTot(pr),csTot(pr),prs(pr)%legs,prs(pr)%legs))
        end if
      end do
      if (log_mem) call log_memory('@saveloop:sav=1 alloc end')

    endif


    do pr = 1, prTot
      ! Compute symmetry factor for identical outgoing particles
      allocate(many(n_particles))
      !end if
      many = 0
      symfac = 1
      do i = prs(pr)%legsIn+1,prs(pr)%legs
        many(prs(pr)%par(i)) = many(prs(pr)%par(i)) + 1
        symfac = symfac * many(prs(pr)%par(i))
      enddo
      deallocate(many)

      ! Compute color averaging factor for incoming particles
      colfac = 1
      do i = 1,prs(pr)%legsIn
        colfac = colfac * abs(get_particle_colour_mdl(prs(pr)%par(i)))
      end do

      ! Compute spin averaging factor for incoming particles
      polfac = 1
      do i = 1,prs(pr)%legsIn
        if (prs(pr)%hel(i) .ne. not_set) cycle
        polfac = polfac * get_particle_polarizations_mdl(prs(pr)%par(i))
      end do

      ! Combine symmetry, spin and color factors
      factor(pr) = 1d0 / polfac / colfac / symfac
    end do

    ! Loop over the processes
    prloop: do pr = 1, prTot
      if (prs(pr)%crosspr .gt. 0) cycle prloop
      if (log_mem) then
        write(log_info, '(i10)') pr
        call log_memory('@prloop:pr='//adjustl(log_info//' begin'))
      end if

      legs = prs(pr)%legs
      if (prs(pr)%loop) then
        legsE = legs + 2
      else
        legsE = legs
      end if

      ! check if the process exists after the first run
      if (sav.eq.1) then
        if (prs(pr)%loop) then
          if ((c0EffMax(pr) .eq. 0) .and. (cEffMax(pr) .eq. 0) ) then
            if (stop_on_nonexisting_process) then
              call error_rcl('Process '//to_str(prs(pr)%inpr)// &
                             ' does not exist neither at LO nor at NLO')
            else
              call warning_rcl('Process '//to_str(prs(pr)%inpr)// &
                               ' does not exist neither at LO nor at NLO')
              prs(pr)%exists = .false.
              cycle prloop
            end if
          end if
        else
          if (c0EffMax(pr) .eq. 0) then
            if (stop_on_nonexisting_process) then
              call error_rcl('process '//to_str(prs(pr)%inpr)// &
                             ' does not exist at LO')
            else
              call warning_rcl('process '//to_str(prs(pr)%inpr)// &
                               ' does not exist at LO')
              prs(pr)%exists = .false.
              cycle prloop
            end if
          end if
        end if
      end if

      if (sav .eq. 1) then

        cfMax = maxval(cfTot)
        csMax = maxval(csTot)

        call texHead(pr)
        if (frm .ne. 0) call formHead (pr, frm)

        ! When computing rational terms from vertex functions the result for the
        ! Feynman rule must be made independent of the fermion sign convention. To
        ! make sure that this is the case we compute the fermion sign for the
        ! would be vertex given by the process particles `particles_in ->
        ! particles_out` The result is multiplied with this factor
        if (frm .ge. 1) then

          allocate (fervec(pow2(legsE-1)-1,legsE))
          allocate (csign(pow2(legsE-1)-1,pow2(legsE-1)-1))
          call fermion_sign_tables(prs(pr)%par(1:legs))
          allocate(pbins(legs))
          allocate(ppar(legs))
          do i = 1, legs
            pbins(legs-i+1) = pow2(i-1)
          end do
          pbins = [pbins(2:legs), pbins(1)]
          ppar = prs(pr)%par(legs:1:-1)
          ppar = [ppar(2:legs), ppar(1)]
          sign = fermion_sign(0,legs,pbins(1:legs-1),ppar(1:legs-1))
          deallocate(pbins,ppar,csign,fervec)
          call writecolorconfigs(pr,sign)
        end if
      endif

      if (sav.eq.0) then

        ! Reordering of the particles
        j = 0
        do i = 1, legs
          color = get_particle_colour_mdl(prs(pr)%par(i))
          select case (abs(color))
          case (1)
            j = j + 1
            newleg(i,pr) = j
            oldleg(j,pr) = i
          end select
        end do
        do i = 1, legs
          if (.not. is_particle_mdl(prs(pr)%par(i))) then
            color = get_particle_colour_mdl(prs(pr)%par(i))
            select case (abs(color))
            case (3)
              j = j + 1
              newleg(i,pr) = j
              oldleg(j,pr) = i
            end select
          end if
        end do
        do i = 1, legs
          if (is_particle_mdl(prs(pr)%par(i))) then
            color = get_particle_colour_mdl(prs(pr)%par(i))
            select case (abs(color))
            case (3)
              j = j + 1
              newleg(i,pr) = j
              oldleg(j,pr) = i
            end select
          end if
        end do
        do i = 1,legs
          color = get_particle_colour_mdl(prs(pr)%par(i))
          select case (abs(color))
          case (8)
            j = j + 1
            newleg(i,pr) = j
            oldleg(j,pr) = i
          end select
        end do

        ! disables internal reordering
        if (.not. particle_ordering) then
          do i = 1,legs
            newleg(i,pr) = i; oldleg(i,pr) = i
          enddo
        end if

        do ii = 1, pow2(legs)-1
          newbin(ii,pr) = 0
          do i = 1,legs
            newbin(ii,pr) = &
            newbin(ii,pr) + pow2(newleg(i,pr)-1)*vectorLeg(ii,i)
          end do
          oldbin(newbin(ii,pr),pr) = ii
        end do

        do i = 1,prs(pr)%resMax
          if (is_resonant_particle_id_mdl(prs(pr)%parRes(i))) then
            e1 = newbin(prs(pr)%binRes(i),pr)
            e2 = pow2(legs) - 1 - e1
            defresbin(e1,pr) = .true.
            defresbin(e2,pr) = .true.
            pspbin(e1,pr) = real(get_particle_cmass2_mdl(prs(pr)%parRes(i)),kind=dp)
            pspbin(e2,pr) = real(get_particle_cmass2_mdl(prs(pr)%parRes(i)),kind=dp)
          endif
        enddo

      end if

      ! translate quark-flow variables to internal (permutated) ones
      allocate(qflowT(legs))
      qflowT = 0
      qflowextT = 0
      do i = 1, legs
        if (prs(pr)%qflow(i) .ne. 0) then
          if (get_particle_type2_mdl(prs(pr)%par(i)) .ne. 'q' .and. &
              get_particle_type2_mdl(prs(pr)%par(prs(pr)%qflow(i))) .ne. 'q') then
            write(*,*) "Invalid fermionflow combination. One of the fields is non-fermionic"
            stop
          else if (get_particle_type2_mdl(prs(pr)%par(i)) .eq. &
                   get_particle_type2_mdl(prs(pr)%par(prs(pr)%qflow(i)))) then
            write(*,*) "Invalid fermionflow combination. Fermion number violation."
            stop
          end if
          qflowT(newleg(i,pr)) = newleg(prs(pr)%qflow(i),pr)

          if (iand(qflowextT,pow2(newleg(i,pr)-1)) .eq. 0) then
            qflowextT = qflowextT + pow2(newleg(i,pr)-1)
          end if

          if (iand(qflowextT,pow2(qflowT(newleg(i,pr))-1)) .eq. 0) then
            qflowextT = qflowextT + pow2(qflowT(newleg(i,pr))-1)
          end if

        else
          qflowT(newleg(i,pr)) = 0
        end if
      end do

      if (prs(pr)%loop) then
        allocate (park(legs+2))
      else
        allocate (park(legs))
      end if
      allocate (helk(legs))

      ! External particles and helicities
      do i = 1,legs
        park(newleg(i,pr)) = prs(pr)%par(i)
        helk(newleg(i,pr)) = prs(pr)%hel(i)
      enddo

      ! mONS2 is the squared of the on-shell mass of the external
      ! particle:
      ! - it is 0 if the particle is marked as light
      ! - it is the squared of the input mass if the particle is not
      !   marked as light
      ! mREG2 is the squared of the input mass of the external
      ! particle (if the particle is marked as light, mREG2 is used
      ! as IR-regulator in loop propagators only)
      do i = 1,legs
        cmREG2(i,pr) = real(get_particle_cmass2_reg_mdl(park(i)), kind=dp)*cone
        cmONS2(i,pr) = real(get_particle_cmass2_mdl(park(i)), kind=dp)*cone
        mONS(i,pr) = sqrt(real(get_particle_cmass2_mdl(park(i)), kind=dp))
      end do

      if (sav.eq.1) then
        wLDef = wMax (pr)
        bLDef = bMax (pr)
        w0Def = w0Tot(pr)
      endif

      ! intialize global currents and generate the helcities he
      call generate_helicityconfig()
      if (log_mem) call log_memory('@generate_helicityconfig')

      if (sav.eq.1) then
        call fill_dualconf
      endif

      ! Set the range of the cut particles.
      ! The cut particle runs over the full particle spectra if the
      ! process is a loop process and no cut particle is selected
      if (prs(pr)%loop) then
        lpmax(pr) = 3
        cuts_start = 1
        cuts_end = 1
        if (cutparticle_selection .eq. -1) then
          cuts_end(1) = n_particles
        else
          cuts_start(1) = cutparticle_selection
          cuts_end(1) = cutparticle_selection
        end if
      else
        lpmax(pr) = 0
        cuts_start = 1
        cuts_end = 1
      endif

      ! deltas for color structures.
      ! These deltas are made just of indices of the
      ! colors of the external lines
      cd0sMax(pr) = 0
      do dl = 1,legs
        ! The number of deltas is given by the number
        ! of gluons + the number of ghosts + the number
        ! of quark-antiquark pairs. This last number can
        ! be obtained counting the quarks (each of
        ! them is for sure in a pair)
        if (get_particle_colour_mdl(park(dl)) .ge. 3) then
          cd0sMax(pr) = cd0sMax(pr) + 1
        end if
      end do
      if (prs(pr)%loop) then
        cdsMax = cd0sMax(pr) + 2
      else
        cdsMax = cd0sMax(pr)
      end if

      ! Optimization of fermion loops (loops with fermions of different
      ! families, but same masses are computed just once).
      if (prs(pr)%loop) then
        call optimize_fermionloop(loopCoef)
      end if

      tlm1 = pow2(legs-1)
      tl   = pow2(legs)
      ! Allocate counters for currents
      if (prs(pr)%loop) then
        allocate (w0min(3:tl-1, cfTot(pr), -1:1, cuts_end(1), 0:lpmax(pr)))
        allocate (w0max(3:tl-1, cfTot(pr), -1:1, cuts_end(1), 0:lpmax(pr)))
      else
        allocate (w0min(3:tlm1-1, cfTot(pr), -1:1, cuts_end(1), 0:lpmax(pr)))
        allocate (w0max(3:tlm1-1, cfTot(pr), -1:1, cuts_end(1), 0:lpmax(pr)))
      end if
      w0min = 0
      w0max = 0

      ! Initialize modaTot, which counts the mother and daugther
      ! currents in the color optimization
      modaTot(pr) = 0

      ! Initialize number of tree and loop branches for
      ! unpolarized particles
      sm0 = 0
      sd0 = 0
      if (prs(pr)%loop) then
        sm1 = 0
        sd1 = 0
      end if

      ! Initialize total number of tree and loop branches
      bm0 = 0
      bd0 = 0
      c0EffMax(pr) = 0
      allocate (c0Eff(-1:1, n_particles, 0:lpmax(pr)))
      c0Eff = 0
      allocate (comp0(-1:1, n_particles, 0:lpmax(pr)))
      comp0 = .false.

      if (prs(pr)%loop) then
        bm1 = 0
        bd1 = 0
        if (sav .eq. 1) then
          allocate (bm1h1(0:tlm1-1, -1:1, n_particles))
          allocate (bd1h1(0:tlm1-1, -1:1, n_particles))
          bm1h1 = 0
          bd1h1 = 0
          allocate (bm1c(maxval(cEffMax)))
          allocate (bd1c(maxval(cEffMax)))
          bm1c = 0
          bd1c = 0
        end if
        cEffMax(pr) = 0
        allocate (comp1(0:tlxm1-1, -1:1, n_particles))
        comp1(:,:,:) = .false.
      end if

      if (sav .eq. 0) then
        if (prs(pr)%loop) then
          ti = 0
          tiTot(pr) = 0
        endif
      else if (sav .eq. 1) then
        pm(pr)%bm0(:,:,:)%bmin = 0
        pm(pr)%bm0(:,:,:)%bmax = 0
        pm(pr)%bd0(:,:,:)%bmin = 0
        pm(pr)%bd0(:,:,:)%bmax = 0
        if (prs(pr)%loop) then
          pm(pr)%bm1(:,:,:)%bmin = 0
          pm(pr)%bm1(:,:,:)%bmax = 0
          pm(pr)%bd1(:,:,:)%bmin = 0
          pm(pr)%bd1(:,:,:)%bmax = 0
          allocate (massti(tiDef))
        end if
      end if

      pCsTot(pr) = 0
      allocate (pCs(lmax, csDef)); pcs(legs+1:lmax, :) = 0

      ! lp=0 -> Born
      ! lp=1 -> Loop
      ! lp=2 -> Counterterms
      ! lp=3 -> Rational terms

      if (log_mem) call log_memory('@lploop begin')
      lploop: do lp = 0, lpmax(pr)

        if (lp .eq. 0) then
          if (log_mem) call log_memory('@lploop Born begin')
        else if (lp .eq. 1) then
          if (log_mem) call log_memory('@lploop Loop begin')
        else if (lp .eq. 2) then
          if (log_mem) call log_memory('@lploop CT begin')
        else if (lp .eq. 3) then
          if (log_mem) call log_memory('@lploop R2 begin')
        end if
        if (.not. lp_on(lp)) cycle
        ! legsE: Effective number of legs (for loop we have two more legs)
        if (lp .eq. 1) then
          legsE = legs + 2
        else
          legsE = legs
        end if

        lexp = pow2(legsE-1)

        allocate (ferp(legsE))
        allocate (fervec(pow2(legsE-1)-1, legsE))
        allocate (csign(pow2(legsE-1)-1, pow2(legsE-1)-1))
        allocate (peakleg(legsE))
        if (lp .eq. 1) then
          if (sav .eq. 1) then
            allocate (w1sTot(0:tlm1-1))
            allocate (w1lTot(0:tlm1-1, cfTot(pr)))
          end if
          allocate (cEff(0:tlm1-1, -1:1, n_particles))
          cEff = 0
        end if

        ! t is the particle of the cutted loop line
        cutsloop: do t = cuts_start(lp), cuts_end(lp)

          if (lp.eq. 1) then
            if (loopCoef(t,pr) .eq. 0) cycle cutsloop
            cc = get_particle_type2_mdl(t)
            if (sm_generation_opt) then
              if (noquarks(pr) .and.          &
                  .not. nogluons(pr) .and.    &
                  .not. noweaks(pr) .and.     &
                  cc .ne. 'q' .and. cc .ne. 'q~') &
                cycle cutsloop
              if (noquarks(pr) .and. nogluons(pr) .and.   &
                  (cc .eq. 'G' .or. cc .eq. 'G~' .or. cc .eq. 'g')) &
                cycle cutsloop
              if (noquarks(pr) .and. noweaks(pr) .and.              &
                  cc .ne. 'G' .and. cc .ne. 'G~'.and. cc .ne. 'g' .and.   &
                  cc .ne. 'q' .and. cc .ne. 'q~'                        ) &
                cycle cutsloop
            end if
            !myparticle = get_particle(t)
            !if (myparticle%spin .eq. 2) then
              !cycle
            !end if
            !if (t .ne. get_particle_id('Z') .and. t .ne. get_particle_id('G0') &
              !.and. t .ne. get_particle_id('H')) then
              !cycle cutsloop
            !end if
            !if (&
              !!t .ne. get_particle_id('W+') .and. t .ne. get_particle_id('W-') .and. &
                !!t .ne. get_particle_id('G+') .and. t .ne. get_particle_id('G-') &
                !t .ne. get_particle_id('ghWp') .and. t .ne. get_particle_id('ghWp~') .and. &
                !t .ne. get_particle_id('ghWm') .and. t .ne. get_particle_id('ghWm~') &
              !) then
              !cycle
            !end if
          end if

          ! Cutted loop lines
          if (lp.eq.1) then
            park(legs+1) = t
            park(legs+2) = get_particle_antiparticle_mdl(t)
          end if

          ! Get fermion relative signs according to hep-ph/0002082.
          ! The fermion relative signs obtained in this way are not
          ! always right at loop level and must be corrected by an
          ! overall sign (losigncorrection) which is computed later.
          call fermion_sign_tables(park(1:legsE))

          ! Create skeleton. The last branches are filtered
          ! to get the correct particle.
          ! The "T" variables are local (they are initialized
          ! for each lp, case and config)
          allocate (parT(wLDef))
          allocate (csT(-1:legsE,wlDef))
          allocate (binT(wLDef))
          allocate (hosT(legs,wLDef))
          allocate (hmaT(wLDef))
          allocate (xxxT(wLDef))
          allocate (orderT(n_orders,wLDef))
          allocate (nMaxT(bLDef))
          allocate (U1gT(wLDef))
          allocate (wT(nDef,bLDef))
          allocate (typeT(bLDef))
          allocate (permT(bLDef))
          allocate (orderIncT(n_orders,bLDef))
          allocate (couT(bLDef))
          allocate (cbT(bLDef))
          allocate (cbtypeT(bLDef))
          allocate (colourfacT(1:2,bLDef))
          parT(1:legsE) = park(1:legsE)
          do it = 1,legsE
            binT(it) = pow2(it-1)
            csT(:, it) = 0
            color = get_particle_colour_mdl(park(it))
            select case (color)
            case(8); csT(-1:0, it) = it
            case(3); csT(-1, it) = it
            case(-3); csT(0, it) = it
            end select
          enddo
          hosT(:,1:legs) = -1
          hmaT(1:legs) = 0
          if (lp .eq. 1) then
            hosT(:,legs+1) = 0
            hmaT(legs+1) = get_particle_mass_id_mdl(park(legs+1))
            hosT(:,legs+2) = -1
            hmaT(legs+2) = 0
          end if
          xxxT(1:legsE) = 0
          do i = 1, legsE
            orderT(:,i) = 0
            if (colour_optimization.ge.2) then
              U1gT(i) = .false.
            else
              ! Identify the Gluon
              U1gT(i) =  get_particle_spin_mdl(parT(i)) .eq. 3 .and. &
                         get_particle_colour_mdl(parT(i)) .eq. 8
            end if
          end do

          call makeSkeleton(lp,legs,pr)

          wMax(pr) = max(wMax(pr), wMaxT)
          bMax(pr) = max(bMax(pr), bMaxT)
          call filter_and_optimize()

          allocate (ej(1:bMaxT))
          allocate (csj(1:bMaxT)); csj = 0
          if (sav .eq. 1) then
            allocate (sm0j(1:bMaxT)); sm0j = 0
            allocate (sd0j(1:bMaxT)); sd0j = 0
            if (lp .eq. 1) then
              allocate (sm1j(1:bMaxT)); sm1j = 0
              allocate (sd1j(1:bMaxT)); sd1j = 0
              allocate (rankInj(1:bMaxT)); rankInj = 0
              allocate (rankOutj(1:bMaxT)); rankOutj = 0
              allocate (rankwT(wMaxT)); rankwT = 0
              allocate (tij(1:bMaxT)); tij = 0
            end if
          end if

          ! Initializa firstlastloopj
          firstlastloopj = bMaxT + 1

          if (sav .eq. 1) then

            allocate(compw(wMaxT)); compw = .false.

            do j = 1,bMaxT
              if (.not.jfilter(j)) cycle
              wN = wT(nDef, j)
              eN = binT(wN)
              compw(wN) = .true.
              if (eN .eq. pow2(legsE-1)-1) then ! last branch
                oi_id = reg_od(orderT(:,wN),pr)
                if (oi_id .eq. -1) then
                  write(*,*) 'ERROR in setting compoi. Order id not found.'
                  write(*,*) "orderT(:,wN):", orderT(:,wN)
                  stop
                end if
                if (lp .eq. 0) then
                  compoi(oi_id,0,pr) = .true.
                else
                  compoi(oi_id,1,pr) = .true.
                end if
              endif
            end do

            if (frm.ne.0) then
              call formCurrents0 (pr,lp,t,wMaxT,compw,frm)
            end if

          endif

          ! Loop over the branches as created by skeleton
          ! (no polarization is introduced yet).
          ! Here will be fixed the "s" (from "skeleton") global
          ! variables which do not depend on the polarization

          jloop1: do j = 1, bMaxT
            new_cut = new_cut .or. (j .eq. 1)
            if (.not. jfilter(j)) cycle jloop1

             if (moj(j) .ne. 0 .and. daj(j) .ne. 0) then
              warnings = warnings + 1
              if (warnings.le.warning_limit+1) then
                call openOutput
                write(nx,*)
                write(nx,*) 'CODE ERROR (currents_rcl.f90):'
                write(nx,*) 'branch with mo and da:',j,moj(j),daj(j)
                write(nx,*)
                if (warnings.eq.warning_limit+1) then
                  write(nx,*)
                  write(nx,*) ' Too many WARNINGs for this run:'
                  write(nx,*) ' Further WARNINGs will not be printed'
                  write(nx,*)
                endif
              endif
              call istop (ifail,2)
            endif

            wN = wT(nDef,j)
            eN = binT(wN)
            ej(j) = eN

            do k = 1, nDef
              if (wT(k, j) .ne. 0) then
                ee(k) = binT(wT(k, j))
              else
                ee(k) = 0
              end if
            end do

            if (firstlastloopj.eq.bMaxT+1 .and. &
                eN .eq. pow2(legs+1)-1             ) firstlastloopj = j

            ! Rough counter for tensor integrals tiDef (just done for
            ! sav=0 in order to allocate global variables for tensor
            ! integrals). The exact counter for tensor integrals tiTot
            ! is computed later for sav=1
            if (sav .eq. 0 .and. j .ge. firstlastloopj) then
              check = 1
              do j0= firstlastloopj, j-1
                if (jfilter(j0)) then
                  check = + sum(abs(hosT(:,wN)-hosT(:,wT(nDef,j0)))) &
                          + abs(hmaT(wN)-hmaT(wT(nDef,j0)))
                  if (check.eq.0) exit
                endif
              enddo
              if (check.ne.0) then
                ti = ti + 1
                tiDef = max(tiDef,ti)
              endif
            endif

            ! Counters s are increased
            if (eN .lt. pow2(legs)) then ! tree branch
              if (daj(j).eq.0) then ! mother branch
                s = sm0 + 1
                sm0 = s
                sm0Tot = max(sm0Tot,sm0)
              else
                s = sd0 + 1
                sd0 = s
                sd0Tot = max(sd0Tot,sd0)
              endif
            else ! loop branch
              if (daj(j).eq.0) then
                s = sm1 + 1
                sm1 = s
                sm1Tot = max(sm1Tot,sm1)
              else
                s = sd1 + 1
                sd1 = s
                sd1Tot = max(sd1Tot,sd1)
              endif
            endif

            ! color structures, last branch only
            if (eN .eq. pow2(legsE-1)-1) then
              check = 1
              do k = 1,pCsTot(pr)
                check = sum(abs(csT(1:legs,wN)-pCs(1:legs,k)))
                if (check.eq.0) then
                  csj(j) = k
                  exit
                endif
              enddo
              if (check.ne.0) then
                pCsTot(pr) = pCsTot(pr) + 1
                csj(j) = pCsTot(pr)
                pCs(1:legs,pCsTot(pr)) = csT(1:legs,wN)
              endif
            endif

            if (sav.eq.1) then

              npoint = 0
              do k = 1, nDef - 1
                if (wT(k, j) .ne. 0) then
                  pp(k) = parT(wT(k, j))
                  npoint = k + 1
                else
                  pp(k) = 0
                end if
              end do

              pN = parT(wN)
              pp(nDef) = get_particle_antiparticle_mdl(pN)

              ! xs = T -> ct or r2 coupling
              ! xs = F -> normal tree coupling
              xIn = xxxT(wT(1, j))
              xIn = 0
              do ii = 1,nDef-1
                if (wT(ii, j).ne.0) xIn = xIn + xxxT(wT(ii, j))
              end do
              if (xIn.eq.0.and.xxxT(wN).eq.1) then
                xs = .true.
              else
                xs = .false.
              end if

              ! Relative fermion sign
              fersign = fermion_sign(typeT(j),npoint,ee(1:npoint-1),pp(1:npoint-1))

              if (eN .ge. pow2(legs)) then ! loop branch only

                ! rankInj,rankOutj,rankwT
                if (daj(j).ne.0) then
                  rankInj(j) = rankInj(jdaTOjmo(j))
                  rankOutj(j) = rankOutj(jdaTOjmo(j))
                  rankwT(wN) = max(rankwT(wN),rankwT(wT(nDef,jdaTOjmo(j))))
                else
                  rankIn = rankwT(wT(1, j)) ! wT(1, j) is the loop-current

                  ! the rank increase can depend on whether couplings are zero.
                  ! cou is set to true if a coupling below the threshold
                  call check_vanishing_couplings(cou,.false.)
                  call get_vertex_lorentz_rank_mdl(typeT(j),cou,rankInc)

                  deallocate(cou)
                  rankwT(wN)  = max(rankwT(wN),rankIn+rankInc)
                  rankInj(j)  = rankIn
                  rankOutj(j) = rankIn + rankInc
                endif

                ferloopsign    = 1
                ghostloopsign  = 1
                signcorrection = 1

                ! last branch only
                if (eN .eq. pow2(legsE-1)-1) then
                  call loop_sign(ferloopsign,ghostloopsign,signcorrection)
                  call set_tensor_parameter(j)
                endif

              endif

              call fix_s_branch_vars

              ! draw currents

              if (daj(j).eq.0) then
                call texPicture(lp,new_cut,legs,legsE,ee,pp,park,t,cdsMax,j,xs)
                new_cut = .false.
              end if

              if (frm .ne. 0) then

                if (daj(j) .ne. 0) then
                  call error_rcl("Form output not supported with colour optimization.")
                end if

                ! Some structures are not written if some couplings are zero.
                ! cou is set to false if a coupling value is below the threshold
                call check_vanishing_couplings(cou, .true.)

                if (any(cou)) then
                  call prepare_formcurrent(jj, Nc_power, sign, &
                                           colT_symbol, colT_imag)

                  ! Some structures are not written if some couplings are zero.
                  ! cou is set to false if a coupling value is below the threshold
                  call check_vanishing_couplings(cou,.true.)

                  if (any(cou)) then
                    if (permT(jj) .ne. 0) then
                      write(*,*) "permT(jj):", permT(jj)
                      write(*,*) "pp:", pp
                      write(*,*) "pmi(pp,permT(jj),size(pp)):", pmi(pp,permT(jj),size(pp))
                    end if
                    if (frm .le. 4) then
                      call form_current_mdl(lp,t,legs,j,nMaxT(jj),        &
                           pmi(ee,permT(jj),size(ee)),                    &
                           pmi(pp,permT(jj),size(pp)),                    &
                           pmi(wT(1:nDef,jj),permT(jj),nDef),             &
                           csj(j),orderT(:,wN),typeT(jj),orderIncT(:,jj), &
                           sign,cou,colT_imag,colT_symbol,Nc_power,       &
                           cbtypeT(jj),cbT(jj))
                     else if(frm .eq. 5) then
                       call formVertex(lp,t,legs,j,nMaxT(jj),    &
                            pmi(ee,permT(jj),size(ee)),          &
                            pmi(pp,permT(jj),size(pp)),          &
                            pmi(wT(1:nDef,jj),permT(jj),nDef),   &
                            csj(j),orderT(:,wN),orderIncT(:,jj), &
                            cbtypeT(jj),cbT(jj),eN .ge. pow2(legs))
                     else if(frm .eq. 6) then
                       call formParticles(lp,t,legs,j,nMaxT(jj), &
                            pmi(ee,permT(jj),size(ee)),          &
                            pmi(pp,permT(jj),size(pp)),          &
                            pmi(wT(1:nDef,jj),permT(jj),nDef),   &
                            csj(j),orderT(:,wN),orderIncT(:,jj), &
                            cbtypeT(jj),cbT(jj))
                    end if
                  end if
                  deallocate(cou)
                end if
              end if

            end if

          end do jloop1

          deallocate (csj)

          if (sav.eq.1) then
            if (frm.ne.0) then
              call formCurrents1 (pr,lp,t,wMaxT,compw,frm)
            end if

            deallocate (compw)

            if (lp.eq.1) then
              deallocate (tij)
              deallocate ( rankwT)
              deallocate (rankOutj)
            endif

            ! Computation of min and max for lmu, which is the index
            ! for the spinor/polarization vector for the cutted loop
            ! line. The first argument of minlmu and maxlmu is the
            ! "fermion helicity" fh of the cutted loop line (see
            ! later).
            select case (get_particle_spin_mdl(t))
            case (:1)
              minlmu ( :,t) = 0
              maxlmu ( :,t) = 0
            case (3)
              minlmu ( :,t) = 0
              maxlmu ( :,t) = 3
            case (2)
              minlmu ( 0,t) = 0
              maxlmu ( 0,t) = 3
              minlmu (-1,t) = 2
              maxlmu (-1,t) = 3
              minlmu (+1,t) = 0
              maxlmu (+1,t) = 1
            case default
              write (*, *) 'Case not implemented yet in setting minlmu, maxlmu'
              write(*,*) "particle_id, spin:", t, get_particle_spin_mdl(t)
              stop
            end select

          endif

          allocate (fheT(0:wLDef))
          allocate (fhfilter(bMaxT))

          ! Loop over fh of the cutted loop line. fh is the
          ! "fermion helicity", which is always 0 for bosons
          ! and for massive fermions and is -1 or +1 for
          ! left-handed or right-handed massless fermions
          ! respectively.

          fhmin(t) = 0
          fhmax(t) = 0
          if (lp.eq.1) then
            select case (get_particle_spin_mdl(t))
            case (2)
              ! massless or massregulated
              if (get_particle_mass_reg_mdl(t) .le. 2) then
                fhmin(t) = - 1
                fhmax(t) = + 1
              endif
            end select
          endif

          fhloop: do fh = fhmax(t),fhmin(t),-2

            if (lp.eq.1) then
              allocate (okwti(wMaxT,cfTot(pr)))
              if (sav.eq.1) then
                allocate (wtTOw1b(wMaxT,cfTot(pr)))
                allocate (rankwMax(0:tlm1-1))
                rankwMax = 0
              endif
            endif

            ! The loop over helicity configuration (configloop)
            ! is done twice for sav=1.
            ! For rep=0 we count just the currents which are common
            ! to more than one helicity; this counter (w1sTot) is
            ! not initialized at each config, but goes through.
            ! For rep=1 the remaining currents are counted: this
            ! counter (w1lTot) is initialized at each config.
            ! The final counter of the currents is a proper
            ! combination of the two

            reploop: do rep = 0, sav

              if (lp.eq.1.and.rep.eq.0) then
                okwti = .false.
                if (sav.eq.1) then
                  wtTOw1b = 0
                  wtTOw1b (legs+1,:) = 1
                  wtTOw1b (legs+2,:) = 2
                endif
              endif

              ! The first loop currents has w1sTot = 1
              ! The  last loop currents has w1sTot = 2
              if (lp.eq.1) then
                if (sav.eq.1.and.rep.eq.0) then
                  w1sTot = 2
                endif
              endif

              ! Loop over helicity configurations
              configloop: do i = 1, cfTot(pr)

                ! helicities of external particles
                ! for this configuration
                helk(:) = he(1:legs,i,pr)

                fheT = not_set    ! all fheT are first set to not_set
                fhfilter = .true.
                do k = 1,legs
                  fheT(k) = 0
                  select case (get_particle_spin_mdl(park(k)))
                  case (2)
                    if (get_particle_mass_reg_mdl(park(k)) .le. 2) then ! massless
                      fheT(k) = helk(k)
                    end if
                  end select
                end do
                if (lp .eq. 1) then
                  fheT(legs+1) = 0
                  fheT(legs+2) = 0
                  if (get_particle_mass_reg_mdl(t) .le. 2) then
                    fheT(legs+1) = + fh
                    fheT(legs+2) = + fh
                  endif
                endif

                ! The "fermion helicity" of the particles
                ! of the each branch are computed
                do j = 1,bMaxT

                  wN = wT(nDef,j)

                  npoint = get_value_in_array(0, wT(:, j))
                  if (npoint .eq. 0) then
                    npoint = nDef
                  end if

                  zerobr = .false.
                  do ii = 1, npoint -1
                    if (fheT(wT(ii, j)) .eq. zero_current) then
                      zerobr = .true.
                      exit
                    end if
                  end do

                  if (zerobr) then   ! one of the incoming branches is zero
                                     ! -> outgoing branch is zero
                    fhfilter(j) = .false.
                    if (fheT(wN) .eq. not_set) fheT(wN) = zero_current
                  else
                    if (helicity_opt) then
                      ml = get_particle_mass_reg_mdl(parT(wN)) .le. 2
                      last_tl = (lp .ne. LoopBranch) .and. &
                                (binT(wN) .eq. pow2(legs-1)-1)
                      fhewN = fheT(wN) ! cache current helicity state
                      call helicity_conservation_mdl(              &
                              fheT,typeT(j),fhfilter,j,            &
                              pmi(wT(1:npoint,j),permT(j),npoint), &
                              wN,ml,last_tl)

                      if (fheT(wN) .eq. zero_current) then
                        fhfilter(j) = .false.
                        ! if  previous helicity state exists cannot necessarily
                        ! be set to zero
                        if (fhewN .eq. not_set) then
                          fheT(wN) = zero_current
                        else
                          fheT(wN) = fhewN
                        end if
                      end if

                      ! the last binary helicity must match with external one
                      if ((binT(wN) .eq. pow2(legsE-1)-1)) then ! last current
                        if (fheT(wN)*fheT(legsE) .ne. 0 .and. &
                            fheT(wN) .ne. fheT(legsE)) then
                          fhfilter(j) = .false.
                          if (fheT(wN) .eq. not_set) fheT(wN) = zero_current
                        end if
                      end if
                    else
                      fheT(wN) = unpol
                    end if
                  end if
                end do

                fhfilter1: do j = bMaxT,1,-1 ! reverse
                  if (.not. jfilter(j)) then
                    fhfilter(j) = .false.
                    cycle fhfilter1
                  endif
                  w = wT(nDef, j)
                  if ((fhfilter(j)) .and. (binT(w) .ne. lexp-1)) then
                    check = 1
                    do k = bMaxT, j+1, -1
                      if (fhfilter(k)) then
                        check = 1
                        iiloop: do ii = 1, nDef-1
                          if (abs(w-wT(ii, k)) .eq. 0) then
                            check = 0
                            exit iiloop
                          end if
                        end do iiloop
                        if (daj(k) .gt. 0) check = 1
                        if (moj(j) .ne. 0) check = check*abs(moj(j)-daj(k))
                        if (check .eq. 0) cycle fhfilter1
                      endif
                    enddo
                    if (check .ne. 0) fhfilter(j) = .false.
                  endif
                enddo fhfilter1

                if (sav.eq.rep) then

                  ! fix the map wtTOw0b for the external particles
                  ! of this lp, case and config
                  allocate (wtTOw0b(wMaxT))
                  do it = 1,legs ! "it" is the wT of the
                                 ! external particles
                    do w0 = 1, w0eTot(pr)
                      check = + abs(parw0e(w0,pr)-park(it))  &
                              + abs(binw0e(w0,pr)-pow2(it-1)) &
                              + abs(helw0e(w0,pr)-helk(it))  &
                              + sum(abs(csw0(-1:legsE,w0)-csT(-1:legsE,it)))
                      if (check.eq.0) wtTOw0b (it) = w0
                    enddo
                  enddo

                  ! Some variables to help computing the counters
                  ! for currents and branches
                  startw0 = 0
                  ew0 = 0
                  if (sav.eq.1) then
                    startbm0 = 0
                    startbd0 = 0
                    ebm0 = 0
                    ebd0 = 0
                    if (lp.eq.1) then
                      allocate(startbm1(0:tlm1-1))
                      allocate(startbd1(0:tlm1-1))
                      allocate(ebm1(0:tlm1-1))
                      allocate(ebd1(0:tlm1-1))
                      startbm1 = 0
                      startbd1 = 0
                      ebm1 = 0
                      ebd1 = 0
                    endif
                  endif

                endif

                ! Initialize w1lTot
                if (lp .eq. 1 .and. rep .eq. 1) w1lTot(:,i) = 0

                allocate (jfirstwT(wMaxT))
                jfirstwT = 0

                ! Initialization of currents.
                ! The order of the computation of the currents is:
                !  - non-self-energy mother currents
                !  - non-self-energy daughter currents
                !  - self-energy mother currents
                !  - self-energy daughter currents
                allocate (jinitwT(wMaxT))
                allocate (winitj(bMaxT))
                jinitwT = 0
                do j= 1, bMaxT
                  if (jfilter(j).and.fhfilter(j)) then
                    wN = wT(nDef,j)
                    ! Current has not been initialized yet -> do it
                    if (jinitwT(wN).eq.0) then
                      jinitwT(wN) = j
                      winitj(j) = .true.
                    ! Current is already initialized, check if the current
                    ! current has higher priority
                    else
                      ! j is daughter branch
                      is_daughter = daj(j) .ne. 0

                      ! j is a selfenergy
                      is_se  = binT(wT(1,j)) .eq. binT(wT(nDef,j))

                      ! the init current is a daughter branch
                      init_is_daughter = daj(jinitwT(wN)) .ne. 0

                      ! the init current is a selfenergy
                      init_is_se = binT(wT(1,jinitwT(wN))) .eq. binT(wT(nDef,jinitwT(wN)))

                      ! current is a mother branch without selfenergy. Update if
                      ! the previous current is either a daughter branch or a
                      ! selfenergy
                      init_cond1 = (.not. is_daughter .and. .not. is_se) .and. &
                                   (init_is_daughter .or. init_is_se)

                      ! current is a daughter branch without selfenergy. Update if
                      ! the previous current is a selfenergy (mother or daughter)
                      init_cond2 = (is_daughter .and. .not. is_se) .and. &
                                   (init_is_se)

                      ! current is a mother branch with selfenergy. Update if
                      ! the previous current is a daughter branch with selfenergy
                      init_cond3 = (.not. is_daughter .and. is_se) .and. &
                                   (init_is_daughter .and. init_is_se)

                      if (init_cond1 .or. init_cond2 .or. init_cond3) then
                        winitj(jinitwT(wN)) = .false.
                        jinitwT(wN) = j
                        winitj(j) = .true.
                      else
                        winitj(j) = .false.
                      endif
                    endif
                  endif
                enddo

                ! Loop over the branches as created by skeleton
                ! (rerun for each helicity configuration).
                ! Here will be fixed the "b" (from "branch") global
                ! variables which depend on the polarization

                jloop2: do j= 1, bMaxT

                  if (jfilter(j) .and. fhfilter(j)) then

                    wN = wT(nDef,j)
                    eN = binT(wN)

                    do k = 1, nDef
                      if (wT(k, j) .ne. 0) then
                        ee(k) = binT(wT(k, j))
                      else
                        ee(k) = 0
                      end if
                    end do

                    ! For loop only.
                    ! h1 is the off-set of the second propagator,
                    ! which, because of the rules to avoid double
                    ! counting, is always an odd number (the off-set
                    ! of the first propagator is always 0). ih1 is
                    ! the corresponding counter. ih1 is then used to
                    ! optimize counting the currents and reduces the
                    ! memory needed for them.
                    if (eN .ge. pow2(legs)) then
                      h1 = 0
                      do k = 1,size(hosT,1)
                        if (hosT(k,wN)+1.eq.2) h1 = h1 + pow2(k-1)
                      enddo
                      if (h1 .eq. 0) then
                        ih1 = 0
                      else
                        ih1 = (h1+1)/2
                      endif
                    endif

                    ! Given a local current wN, jfirstwT(wN) is
                    ! the first local branch j which has wN as
                    ! outgoing current.
                    if (jfirstwT(wN).eq.0) jfirstwT(wN) = j

                    ! Here we compute the counting number of the
                    ! outgoing currents, for all branches except the
                    ! last ones

                    if (eN .ne. pow2(legsE-1)-1) then

                      new_current = register_current()
                      if (.not. new_current) cycle jloop2
                    endif

                    ! Here we define the indices "c0Eff" and "cEff",
                    ! just for those values which produce currents,
                    ! to save memory. "c0Eff" combines for trees the
                    ! three indices lp, t and fh. "cEff" combines for
                    ! loops the three indices t, fh and ih1.
                    if (sav.eq.rep) then
                      if (eN .lt. pow2(legs)) then ! tree branch
                        if (.not.comp0(fh,t,lp)) then
                          comp0(fh,t,lp) = .true.
                          c0EffMax(pr) = c0EffMax(pr) + 1
                          c0Eff(fh,t,lp) = c0EffMax(pr)
                        endif
                        c = c0Eff(fh,t,lp)
                      else ! loop branch
                        if (.not.comp1(ih1,fh,t)) then
                          comp1(ih1,fh,t) = .true.
                          cEffMax(pr) = cEffMax(pr) + 1
                          cEff(ih1,fh,t) = cEffMax(pr)
                        endif
                        c = cEff(ih1,fh,t)
                      endif
                    endif

                    if (sav.eq.0.and.rep.eq.0) then

                      ! Counters b are increased
                      if (eN .lt. pow2(legs)) then ! tree branch
                        if (daj(j).eq.0) then ! mother branch
                          bm0 = bm0 + 1
                          pm(pr)%bm0Tot = max(bm0, pm(pr)%bm0Tot)
                        else ! daughter branch
                          bd0 = bd0 + 1
                          pm(pr)%bd0Tot = max(bd0, pm(pr)%bd0Tot)
                        endif
                      else ! loop branch
                        if (daj(j).eq.0) then ! mother branch
                          bm1 = bm1 + 1
                          pm(pr)%bm1Tot = max(bm1, pm(pr)%bm1Tot)
                        else ! daughter branch
                          bd1 = bd1 + 1
                          pm(pr)%bd1Tot = max(bd1, pm(pr)%bd1Tot)
                        endif
                      endif

                    elseif (sav .eq. 1 .and. rep .eq. 0) then

                      if (eN .ge. pow2(legs)) then ! loop branch
                        if (daj(j).eq.0) then ! mother branch
                          bm1h1(ih1,fh,t) = bm1h1(ih1,fh,t) + 1
                        else ! daughter branch
                          bd1h1(ih1,fh,t) = bd1h1(ih1,fh,t) + 1
                        endif
                      endif

                    elseif (sav.eq.1.and.rep.eq.1) then

                      if (eN .lt. pow2(legs)) then ! tree branch
                        c0TOlp(c,pr) = lp
                      else                    ! loop branch
                        cTOt  (c,pr) = t
                        cTOfh (c,pr) = fh
                        cTOih1(c,pr) = ih1
                      endif

                      if (eN .lt. pow2(legs)) then ! tree branch
                        if (daj(j).eq.0) then ! mother branch
                          bm0 = bm0 + 1
                          b = bm0
                          pm(pr)%bm0Tot = max(bm0, pm(pr)%bm0Tot)
                        else ! daughter branch
                          bd0 = bd0 + 1
                          b = bd0
                          pm(pr)%bd0Tot = max(bd0, pm(pr)%bd0Tot)
                        endif
                      else ! loop branch
                        if (daj(j).eq.0) then ! mother branch
                          bm1 = bm1 + 1
                          pm(pr)%bm1Tot = max(bm1, pm(pr)%bm1Tot)
                          bm1c(c) = bm1c(c) + 1
                          b = bm1c(c)
                          b = b + sum(bm1h1(0:ih1-1,fh,t))
                          if (fh.eq.-1) b = b + sum(bm1h1(:,1,t))
                          b = b + sum(bm1h1(:,:,1:t-1))
                        else ! daughter branch
                          bd1 = bd1 + 1
                          pm(pr)%bd1Tot = max(bd1, pm(pr)%bd1Tot)
                          bd1c(c) = bd1c(c) + 1
                          b = bd1c(c)
                          b = b + sum(bd1h1(0:ih1-1,fh,t))
                          if (fh.eq.-1) b = b + sum(bd1h1(:,1,t))
                          b = b + sum(bd1h1(:,:,1:t-1))
                        endif
                      endif

                      if (eN .ge. pow2(legs)) &
                        rankwMax(ih1) = max(rankwMax(ih1),rankInj(j))

                      ! Now we fix the value of the global variables
                      ! for "b" branches
                      call fix_b_branch_vars

                      ! Set flags compoi telling which orders should be computed when
                      ! iterating over all combinations of orders.
                      if (eN .eq. pow2(legsE-1)-1) then ! last branch
                        oi_id = check_oi(orderT(:,wN),pr)
                        if (oi_id .eq. -1) then
                          write(*,*) 'ERROR in setting compoi. Order id not found.'
                          write(*,*) "orderT(:,wN):", orderT(:,wN)
                          stop
                        end if
                        if (lp .eq. 0) then
                          compoi(oi_id,0,pr) = .true.
                        else
                          compoi(oi_id,1,pr) = .true.
                        end if

                      endif

                    endif

                  endif

                enddo jloop2

                deallocate (winitj)
                deallocate (jinitwT)

                deallocate (jfirstwT)

                if (sav.eq.1.and.rep.eq.1) then
                  if (lp.eq.1) then
                    deallocate(startbm1)
                    deallocate(startbd1)
                    deallocate(ebm1)
                    deallocate(ebd1)
                  endif
                endif

                if (sav.eq.rep) deallocate (wtTOw0b)

              enddo configloop

            enddo reploop

            if (lp.eq.1) then
              deallocate (okwti)
              if (sav.eq.1) then
                deallocate (wtTOw1b)
                do ih1 = 0,tlm1-1
                  if (cEff(ih1,fh,t).gt.0) then
                    w1TotMax(cEff(ih1,fh,t),pr) = &
                    w1sTot(ih1) + maxval(w1lTot(ih1,:))
                    riwMax(cEff(ih1,fh,t),pr) = riMax(rankwMax(ih1))
                    w1Tot(pr) = w1Tot(pr) + w1sTot(ih1) + sum(w1lTot(ih1,:))
                  endif
                enddo
              endif
            endif

            if (lp.eq.1) then
              if (sav.eq.1) then
                deallocate (rankwMax)
              endif
            endif

          enddo fhloop

          deallocate (fhfilter)
          deallocate (fheT)

          if (sav.eq.1) then
            if (lp.eq.1) then
              deallocate (rankInj)
              deallocate (sd1j)
              deallocate (sm1j)
            endif
            deallocate (sd0j)
            deallocate (sm0j)
          endif

          deallocate(ej)

          deallocate(jfilter)

          deallocate(moj,daj,facj,jdaTOjmo)

          deallocate(parT,csT,binT,hosT,hmaT,xxxT,orderT,U1gT,       &
                     nMaxT,wT,typeT,permT,orderIncT,couT,colourfacT, &
                     cbT,cbtypeT)

        enddo cutsloop

        if (lp.eq.1) then
          deallocate (cEff)
          if (sav.eq.1) then
            deallocate (w1lTot,w1stot)
          endif
        endif

        deallocate (peakleg,csign,fervec,ferp)

        if (lp .eq. 0) then
          if (log_mem) call log_memory('@lploop Born end')
        else if (lp .eq. 1) then
          if (log_mem) call log_memory('@lploop Loop end')
        else if (lp .eq. 2) then
          if (log_mem) call log_memory('@lploop CT end')
        else if (lp .eq. 3) then
          if (log_mem) call log_memory('@lploop R2 end')
        end if
      enddo lploop
      if (log_mem) call log_memory('@lploop end')

      if (prs(pr)%loop .and. sav .eq. 1) then
        deallocate (bd1c,bm1c,bd1h1,bm1h1)
      endif

      deallocate (comp0,c0Eff,w0min,w0max,parw0,binw0,csw0,xxxw0,orderw0,U1gw0)
      if (prs(pr)%loop) deallocate (comp1)

      !-------------------------------------------------------------
      ! csIa from pCs
      !-------------------------------------------------------------
      ! pCs's are the primary colour structures.
      ! csIa's are the final colour structures of the amplitude
      !
      ! For colour_optimization < 2, the pCs's coincide with csIa's:
      ! csIa = pCs
      !
      ! For colour_optimization = 2, in the list of pCs's are
      ! excluded those colour structures where the colour indices
      ! of gluon stay in the same delta (i.e. the pCs's do not
      ! contain d(ia_l,id_l) for any gluon l).
      ! The final colour structures csIa's are then built applying
      ! to all primary colour structures pCs's the projector
      !
      ! d(ia_l,id_l')*d(ia_l',id_l) - 1/Nc*d(ia_l,id_l)*d(ia_l',id_l')
      !
      ! for each gluon l
      !-------------------------------------------------------------

      ! ng = number of gluons in the process
      ng = 0
      do k = 1,legs
        ! Identify the Gluon
        if (get_particle_spin_mdl(prs(pr)%par(k)) .eq. 3 .and. &
            get_particle_colour_mdl(prs(pr)%par(k)) .eq. 8) ng = ng + 1
      enddo
      allocate (dCs0   (lmax,pow2(ng)))
      allocate (dCs0fac(     pow2(ng)))
      allocate (dCs    (lmax,pow2(ng)))
      allocate (dCsfac (     pow2(ng)))

      csTot(pr) = pCsTot(pr)
      do i = 1,pCsTot(pr)

        ! Kept old notation:
        ! csAmp = csIa, nAmp = nIa, pamp = pIa, facAmp = facIa

        ! - csIa = pCs for the structures with labels from 1 to
        !   pCsTot.
        ! - nIa(i) is the number of contributions to the final
        !   structure with label "i" coming from different pCs's.
        ! - pIa(k,i) for k=1,...,nIa(i) is the label of the
        !   primary structure from which is coming the k^th
        !   contribution to structure with label "i".
        ! - facIa(k,i) is the factor by which has to be multiplied
        !   the k^th contribution to structure with label "i".
         csAmp(:,i,pr) = pCs(:,i)
          nAmp(  i,pr) = 1
          pAmp(1,i,pr) = i
        facAmp(1,i,pr) = 1d0

        if (colour_optimization.ge.2) then

          ! the first dCs0 is the pCs, with factor 1
          dCs0Tot = 1
          dCs0   (:,1) = pCs(:,i)
          dCs0fac(  1) = 1d0

          do k = 1,legs
            ! Identify the Gluon
            if (get_particle_spin_mdl(park(k)) .eq. 3 .and. &
                get_particle_colour_mdl(park(k)) .eq. 8) then
              j = dCs0Tot
              do j0 = 1,dCs0Tot
                ! d(k,iqk) <=> k = dCs0(iqk,j0)
                ! d(iak,k) <=> dCs0(k,j0) = iak
                do iqk = 1,legs
                  if (dCs0(iqk,j0).eq.k) exit
                enddo
                iak = dCs0(k,j0)
                j = j + 1
                dCs0   (  :,j) = dCs0(:,j0)
                dCs0   (  k,j) = k
                dCs0   (iqk,j) = iak
                dCs0fac(    j) = - 1/Nc*dCs0fac(j0)
                if (k .eq. iqk) dCs0fac(j) = Nc*dCs0fac(j)
              enddo
              dCs0Tot = j
            endif
          enddo

          dCsTot = 0
          do j0 = 1,dCs0Tot
            check = 1
            do j = 1,dCsTot
              if (sum(abs(dCs(:,j)-dCs0(:,j0))).eq.0) then
                dCsfac(j) = dCsfac(j) + dCs0fac(j0)
                check = 0
                exit
              endif
            enddo
            if (check.ne.0) then
              dCsTot = dCsTot + 1
              dCs   (:,dCsTot) = dCs0   (:,j0)
              dCsfac(  dCsTot) = dCs0fac(  j0)
            endif
          enddo

          do j = 2,dCsTot ! We start from the 2nd dCs
            new_cs = .true.
            do k = pCsTot(pr)+1,csTot(pr)
              if (sum(abs(dCs(:,j)-csAmp(:,k,pr))).eq.0) then
                new_cs = .false.
                nAmp(k,pr) = nAmp(k,pr) + 1
                n = nAmp(k,pr)
                  pAmp(n,k,pr) = i
                facAmp(n,k,pr) = dCsfac(j)
                exit
              endif
            enddo
            if (new_cs) then
              csTot(pr) = csTot(pr) + 1
              k = csTot(pr)
              csAmp(:,k,pr) = dCs(:,j)
              nAmp(  k,pr) = 1
              pAmp(1,k,pr) = i
              facAmp(1,k,pr) = dCsfac(j)
            endif
          enddo

        endif

      enddo

      deallocate (pCs,dCs0,dCs0fac,dCs,dCsfac)

      if (sav.eq.1) then

        !  write(*,*) "pr, prs(pr)%inpr:", pr, prs(pr)%inpr
        !  write(*,*) ''
        !  write(*,*) 'bm0Total =',pm(:)%bm0Tot
        !  write(*,*) 'bd0Total =',pm(:)%bd0Tot
        !  write(*,*) ''
        !if (prs(pr)%loop) then
        !  write(*,*) 'bm1Total =',pm(:)%bm1Tot
        !  write(*,*) 'bd1Total =',pm(:)%bd1Tot
        !  write(*,*) ''
        !endif
        !  write(*,*) ' w0Total =',w0Tot(pr)
        !if (prs(pr)%loop) then
        !  write(*,*) ' w1Total =',maxval(w1TotMax(:,pr))
        !  write(*,*) ' tiTotal =',tiTot(pr)
        !  write(*,*) ''
        !  write(*,*) 'c0EffMax =',c0EffMax(pr)
        !  write(*,*) 'cEffMax  =',cEffMax(pr)
        !endif
        !  write(*,*) ''

        if (prs(pr)%loop) then
          ranktiMax = maxval(rankti(0:tiTot(pr),pr))
          ritiMax(pr) = riMax(ranktiMax)
        end if

        if (prs(pr)%loop) then
          step = n_masses + 1
          do t = 1,tiTot(pr)
            hm0 = massti(t)
            do i = lmax,1,-1
              vmti(i,t,pr) = hm0/step**(i-1)
              hm0 = hm0 - vmti(i,t,pr)*step**(i-1)
            end do
          end do
          deallocate (massti)
        end if

        ! Precomputing colorcoefficients for computing the squared matrix
        ! element and color correlations.

        if (log_mem) then
          write(log_info, '(i10)') pr
          call log_memory('@colorcoeffs:pr='//adjustl(log_info//' end'))
        end if
        call fill_colordeltas(legs,pr)
        call compute_colourcoefc(legs,pr,park)

      end if

      if (sav.eq.1) then
        call texFeet(pr .eq. prTot)
        if (frm .ne. 0) then
          call formFeet
        end if
      end if

      deallocate (helk,park,qflowT)

      if (log_mem) then
        write(log_info, '(i10)') pr
        call log_memory('@prloop:pr='//adjustl(log_info//' end'))
      end if
    end do prloop
    if (log_mem) call log_memory('@prloop end')

  end do saveloop
  if (log_mem) call log_memory('@saveloop end')
  deallocate (ee,pp)
  deallocate (wMax,bMax)
  deallocate (noquarks,nogluons,noweaks)

  contains

  subroutine external_field_types()
    character(2) :: cc

    ! Look whether there are external quarks, gluons and weak
    ! particles
    allocate (noquarks(prTot))
    allocate (nogluons(prTot))
    allocate (noweaks (prTot))
    do pr = 1,prTot
      noquarks(pr) = .true.
      nogluons(pr) = .true.
      noweaks(pr)  = .true.
      do i = 1,prs(pr)%legs
        cc = get_particle_type2_mdl(prs(pr)%par(i))
        noquarks(pr) = noquarks(pr) .and. (cc .ne. 'q') .and. (cc .ne. 'q~')
        nogluons(pr) = nogluons(pr) .and. (cc .ne. 'g')
        noweaks(pr)  = noweaks(pr)  .and.                   &
                       (cc .ne. 's')  .and. (cc .ne. 'v') .and. &
                       (cc .ne. 'f')  .and. (cc .ne. 'f~')
      end do
    end do

  end subroutine external_field_types

!==============================================================================!
!                           generate_helicityconfig                            !
!==============================================================================!

  subroutine generate_helicityconfig()
    integer polarizations
    integer, allocatable :: hmn(:),hmx(:), ph(:)
    integer, allocatable :: hh(:),haux(:),he0(:,:)
    ! Select helicity configurations

    allocate ( hmn(legs))
    allocate ( hmx(legs))
    allocate (  ph(legs))
    do i= 1, legs
      polarizations = get_particle_polarizations_mdl(park(i))
      select case (polarizations)
      case (1)     ! scalar (floating point exception for ph)
        hmn(i)=  0
        hmx(i)=  0
        ph (i)=  1
      case (2) ! massless vector, fermions and anti-fermions
        hmn(i)= -1
        hmx(i)= +1
        ph (i)=  2
      case (3)     ! massive  vector
        hmn(i)= -1
        hmx(i)= +1
        ph (i)=  1
      end select
    enddo

    ! Setting the base for polarizations to a minimum of 2
    if (max_polarizations .gt. 3) then
      call error_rcl("Max_polarizations has to be smaller than 4.")
    end if
    base = 3

    allocate (checkHel(legs,-1:+1))
    checkHel = .false.
    do i= 1, legs
      do h = hmn(i), hmx(i), ph(i)
        checkHel(i,h) = .true.
      enddo
    enddo
    tag = 0

    ! This fails if max_polarizations=1
    allocate (  hh(legs))
    allocate (haux(legs))
    allocate (he0(legs,confDef))
    hloo: do htag = 0, base**legs-1
      htt = htag
      leho: do i= legs,1,-1
        haux(i) = htt/base**(i-1)
        htt     = htt - haux(i)*base**(i-1)
        hh(i)    = haux(i) - 1
        select case (checkHel(i,hh(i)))
        case (.true.)
          cycle leho
        case (.false.)
          cycle hloo
        end select
      end do leho
      tag = tag + 1
      he0(1:legs,tag)= hh(:)
    end do hloo
    ! he0(leg, conf) stores all possible helicity configurations, for instance,
    ! given one massive vector(P1) and 2 massless vectors(P2,P3) then
    !                   P1          P2          P3
    !conf1              -1          -1          -1
    !conf2               0          -1          -1
    !conf3               1          -1          -1
    !conf4              -1           1          -1
    !conf5               0           1          -1
    !conf6               1           1          -1
    !conf7              -1          -1           1
    !conf8               0          -1           1
    !conf9               1          -1           1
    !conf10             -1           1           1
    !conf11              0           1           1
    !conf12              1           1           1

    ! tag counts the total number of configurations (in the example tag=12)
    cfTot(pr) = tag
    deallocate (haux,hh,checkHel)

    allocate (compConf(confDef))
    tag = 0
    do j= 1, cfTot(pr)
      ! select which configurations has to be computed
      ferhelsum = 0
      do ii = 1,legs
        spin = get_particle_spin_mdl(park(ii))
        if (spin .eq. 2) then
          if (get_doublet_partner_mdl(park(ii)) .eq. 0) then
            cycle
          end if
          ! the mass_ids 1,2 are reserved for the massless case indicating
          ! whether the mass singularities are regulated dimensionally of with
          ! a mass cutoff, respectively
          if (get_particle_mass_reg_mdl(park(ii)) .le. 2 .and. &
              get_particle_mass_reg_mdl(get_doublet_partner_mdl(park(ii))) .le. 2) then
            ferhelsum = ferhelsum + he0(ii,j)
          end if
        end if
      end do
      if (ferhelsum.eq.0) then
        ! Configurations where the helicity sum of fermions
        ! belonging to massless families is not 0 are discarded.
        compConf(j) = .true.
        do i = 1,legs
          if (abs(helk(i)) .ne. not_set) then
            if (helk(i).eq.he0(i,j)) then
              hmn(i) = helk(i)
              hmx(i) = helk(i)
              ph(i)  = 1
            else
              compConf(j) = .false.
            endif
          endif
        enddo
        if (compConf(j)) then
          tag = tag + 1
          he(1:legs,tag,pr)= he0(1:legs,j)
        endif
      endif
    enddo
    cfTot(pr) = tag
    deallocate (compConf)
    deallocate (he0)


    ! Initialize total number of global currents
    w0Tot(pr) = 0
    if (sav .eq. 1) then
      w1Tot(pr) = 0
    end if

    ! The first currents are the external particles,
    ! according to their helicities
    allocate (parw0(w0Def))
    allocate (binw0(w0Def))
    allocate (csw0(-1:legsE,w0Def))
    allocate (xxxw0(w0Def))
    allocate (orderw0(n_orders,w0Def))
    allocate (u1gw0(w0Def))

    w0last(:,pr) = 0

    do i = 1,legs
      do h = hmn(i), hmx(i), ph(i)
        w0Tot(pr) = w0Tot(pr) + 1
        if (w0Tot(pr).gt.w0Def) call reallocate_w0Def
        parw0(w0Tot(pr)) = park(i)
        binw0(w0Tot(pr)) = pow2(i-1)
        csw0(:,w0Tot(pr))   = 0
        color = get_particle_colour_mdl(park(i))
        select case (color)
        case (8)
          csw0(-1:0, w0Tot(pr)) = i
        case (3)
          csw0(-1, w0Tot(pr)) = i
        case (-3)
          csw0(0,w0Tot(pr)) = i
        end select
        xxxw0(w0Tot(pr))     = 0
        orderw0(:,w0Tot(pr)) = 0
        if (colour_optimization.ge.2) then
          U1gw0(w0Tot(pr)) = .false.
        else
          U1gw0(w0Tot(pr)) = get_particle_spin_mdl(park(i)) .eq. 3 .and. color .eq. 8
        end if

        ! for external particles
        parw0e(w0Tot(pr),pr) = park(i)
        binw0e(w0Tot(pr),pr) = pow2(i-1)
        legw0e(w0Tot(pr),pr) = i
        helw0e(w0Tot(pr),pr) = h
        if (i.eq.legs) w0last(h,pr) = w0Tot(pr)
      end do
    end do
    ! w0eTot(pr) counts the number of external particles
    ! with different helicities
    w0eTot(pr) = w0Tot(pr)

    deallocate (ph)
    deallocate (hmx)
    deallocate (hmn)
  end subroutine generate_helicityconfig

!==============================================================================!
!                             filter_and_optimize                              !
!==============================================================================!
! Filters the branches generated by makeSkeleton. the result is stored in      !
! jfilter.                                                                     !
!------------------------------------------------------------------------------!
  subroutine filter_and_optimize()
    integer :: ord
    allocate (jfilter(bMaxT))
    if (lp .eq. 0) zeroLO(pr) = .true.

    ! Filter from last branch to generate currents
    if (lp.eq.0) zeroLO(pr) = .true.
    jfilter1: do j = bMaxT,1,-1 ! reverse
      w = wT(nDef,j)
      jfilter(j) = .false.  ! filter inizialization
      if ( binT(w) .eq. lexp-1 ) then
        jfilter(j) = .true. ! starting point

        if (lp .eq. TreeBranch) then
          do ord = 1, size(orderT,1)
            if (size(prs(pr)%powsel,1) .lt. orderT(ord,w)) then
              call error_rcl("Dimension of powsel not large enough.")
            else
              if (prs(pr)%powsel(orderT(ord,w),ord,0) .eq. 0) jfilter(j) = .false.
            end if
          end do
        end if

        ! Loop, CT & R2 Branch
        if (lp .ge. LoopBranch) then
          do ord = 1, size(orderT,1)
            if (size(prs(pr)%powsel,1) .lt. orderT(ord,w)) then
              call error_rcl("Dimension of powsel not large enough.")
            else
              if (prs(pr)%powsel(orderT(ord,w),ord,1) .eq. 0) jfilter(j) = .false.
            end if
          end do
        end if
      else
        check = 1
        do k = bMaxT, j+1, -1
          if (.not. jfilter(k)) cycle
          check = 1
          do ii = 1, nDef - 1
            if (abs(w - wT(ii, k)) .eq. 0) then
              check = 0
              exit
            end if
          end do
          if (check.eq.0) then
            jfilter(j) = .true.
            cycle jfilter1
          end if
        end do
        if (check .ne. 0) jfilter(j) = .false.
      end if
      if (lp .eq. 0) zeroLO(pr) = zeroLO(pr) .and. .not. jfilter(j)
    end do jfilter1

    ! Generates mother and daughter branches. At the end, a daughter branch is
    ! not computed, but obtained directly from a mother branch modulus a factor
    call optimize_color()

    ! Filter again from last branch to avoid computing
    ! currents generating just daughter currents
    jfilter2: do j = bMaxT,1,-1 ! reverse
      w = wT(nDef,j)
      if ((jfilter(j)) .and. (binT(w) .ne. lexp-1)) then
        check = 1
        do k = bMaxT, j+1, -1
          if (.not.jfilter(k)) cycle
          check = 1

          ! 1. check: w is incoming current
          iiloop: do ii = 1, nDef-1
            if (abs(w - wT(ii, k)) .eq. 0) then
              check = 0
              exit iiloop
            end if
          end do iiloop

          ! 2. check: w is not a daughter current
          if (daj(k) .gt. 0) check = 1

          ! 3. check: w is mother of daughter current
          if(moj(j) .ne. 0) check = check * abs(moj(j)-daj(k))
          if(check .eq. 0) then
            jfilter(j) = .true.
            cycle jfilter2
          end if
        end do
        if (check.ne.0) jfilter(j) = .false.
      end if
    end do jfilter2

  end subroutine filter_and_optimize

!==============================================================================!
!                                fill_dualconf                                 !
!==============================================================================!

  subroutine fill_dualconf
    do ii = 1,legs
       ! Filter for gluon or photon
      if (get_particle_spin_mdl(park(ii)) .eq. 3 .and. &
          get_particle_mass_id_mdl(park(ii)) .eq. 1) then
        do j= 1, cfTot(pr)
          do j1= 1, cfTot(pr)
            if ((sum(abs(he(1:(ii-1),j1,pr)-he(1:(ii-1),j,pr))).eq.0) .and. &
                (sum(abs(he((ii+1):legs,j1,pr) - he((ii+1):legs,j,pr))).eq.0)&
                 .and.  (he(ii,j1,pr) .eq. -he(ii,j,pr))) then
              dualconf(ii,j,pr) = j1
            endif
          enddo
        enddo
      endif
    enddo
  end subroutine fill_dualconf

!==============================================================================!
!                                optimize_color                                !
!==============================================================================!
! Color optimization: Branches with dab(b,pr) not zero are "daugther" of       !
! a mother branch meaning that their outgoing current can be obtained from that!
! of the mother branch (mob(b,pr) not zero), multiplying it by a color factor. !
!------------------------------------------------------------------------------!
  subroutine optimize_color()
    integer :: moda ! counts the number of mo(ther) da(ugther) relations
    ! njwT: counts how many outgoing branches n_j contain the given
    ! wavefunction w.  w: njwT(w) = nj
    ! jwT: retrieve the branch j given the wavefunction w and its rate of
    ! occurence n_j.  w, n_j:  jwT(w, n_j) = j
    integer, allocatable :: njwT(:), jwT(:,:), jwT0(:,:)
    integer :: njwTMax, njwTStep, nn

    allocate (njwT(0:wMaxT))
    njwTMax = (bMaxT/wMaxT+1) * legsE
    njwTStep = njwTMax
    allocate (jwT(0:njwTMax,0:wMaxT))
    allocate ( moj(bMaxT))
    allocate ( daj(bMaxT))
    allocate (facj(bMaxT))
    allocate (jdaTOjmo(bMaxT))
    njwT = 0
    jwT  = 0

    ! a branch j is a mother branch if moj(j) != 0
    moj  = 0
    ! daughter and mother branches are related to each other if daj(j1) =
    ! moj(j2). In that case the branch j2 is obtained from j1 multiplied by the
    ! factor facj(j1)
    daj  = 0
    facj = 1d0

    ! jdaTojmo converts a daughter branch to a mother branch. E.g.  if daj(j1) =
    ! moj(j2) then j2 = jdaTOjmo(j1), but not vice versa a mother can have
    ! multiple daughters.
    jdaTOjmo = 0
    moda = 0
    if (colour_optimization .ge. 1) then
      j1loop: do j1= 1, bMaxT
        if (.not.jfilter(j1)) cycle
        !if (wT(2,j1).eq.0) cycle ! no 2-leg vertices
        njwT(wT(nDef,j1)) = njwT(wT(nDef,j1)) + 1
        if (njwT(wT(nDef,j1)) .gt. njwTMax) then
          allocate (jwT0(0:njwTMax,0:wMaxT))
          jwT0 = jwT
          deallocate (jwT)
          njwTMax = njwTMax + njwTStep
          allocate (jwT(0:njwTMax,0:wMaxT))
          jwT(0:njwTMax-njwTStep,:) = jwT0
          deallocate (jwT0)
        end if
        jwT(njwT(wT(nDef,j1)),wT(nDef,j1)) = j1
        j2loop: do j2 = 1,j1-1

          ! Two different branches can be obtimized with respect to color if
          ! they are alike. The branches must be equal except for the color.
          if (.not. jfilter(j2)) cycle
          if (typeT(j1) .ne. typeT(j2)) cycle j2loop
          if (permT(j1) .ne. permT(j2)) cycle j2loop
          if (size(couT(j1)%c) .ne. size(couT(j2)%c)) cycle j2loop
          if (sum(abs(couT(j1)%c - couT(j2)%c)) .gt. zerocut) cycle j2loop

          iiloop: do ii= 1,nDef
            wTj1 = wT(ii,j1)
            wTj2 = wT(ii,j2)

            do nn = 2, nDef-1
              if (ii.eq.nn.and.wTj1.eq.0.and.wTj2.eq.0) cycle iiloop
              if (ii.eq.nn.and.wTj1*wTj2.eq.0.and.wTj1.ne.wTj2) cycle j2loop
            end do

            if (parT(wTj1).ne.parT(wTj2)) cycle j2loop
            if (binT(wTj1).ne.binT(wTj2)) cycle j2loop
            do k = 1,size(hosT,1)
              if (hosT(k,wTj1).ne.hosT(k,wTj2)) cycle j2loop
            enddo
            if (hmaT(wTj1).ne.hmaT(wTj2)) cycle j2loop
            if (xxxT(wTj1).ne.xxxT(wTj2)) cycle j2loop
            if (U1gT(wTj1).neqv.U1gT(wTj2)) cycle j2loop
            if (sum(abs(orderT(:,wTj1) - orderT(:,wTj2))) .ne. 0) cycle j2loop
          end do iiloop

          ratio = 1d0
          do ii= 1,nDef-1
            wTj1 = wT(ii,j1)
            wTj2 = wT(ii,j2)
            if (ii.eq.2.and.wTj1.eq.0.and.wTj2.eq.0) cycle
            if (ii.eq.3.and.wTj1.eq.0.and.wTj2.eq.0) cycle
            if (wTj1.ne.wTj2) then
              if (njwT(wTj1) .ne. njwT(wTj2)) cycle j2loop
              do n = 1,njwT(wTj1)
                daj1 = daj(jwT(n,wTj1))
                moj2 = moj(jwT(n,wTj2))
                daj2 = daj(jwT(n,wTj2))
                if (daj1.eq.0) cycle j2loop
                if (daj1.ne.moj2.and.daj1.ne.daj2) cycle j2loop
                fac  = facj(jwT(n,wTj1))
                if (n .eq. 1) fac1 = fac
                if (abs((fac-fac1)/(abs(fac)+abs(fac1))) &
                    .gt. zerocut) cycle j2loop
              end do
              ratio = ratio * facj(jwT(1,wT(ii,j1)))/facj(jwT(1,wT(ii,j2)))
            end if
          end do
          col1 = get_colourfac_mdl(colourfacT(1,j1))*Nc**colourfacT(2,j1)
          col2 = get_colourfac_mdl(colourfacT(1,j2))*Nc**colourfacT(2,j2)
          facj(j1) = facj(j1) * ratio * col1/col2
          if (moj(j2).eq.0) then
            moda = moda + 1
            moj(j2) = moda
          endif
          daj(j1) = moj(j2)
          jdaTOjmo(j1) = j2
          exit j2loop
        end do j2loop
      end do j1loop
    end if

    modaTot(pr) = max(modaTot(pr),moda)

    deallocate (njwT, jwT)
  end subroutine optimize_color

!==============================================================================!
!                            optimize_fermionloop                              !
!==============================================================================!

  subroutine optimize_fermionloop(loopCoef)
    integer, dimension(:,:), intent(inout) :: loopCoef
    logical, dimension(:), allocatable :: lightmass, extern
    integer :: p1, p2
    logical :: familybound
    if (fermion_loop_opt .and. has_feature_mdl('fermionloop_opt')) then
      ! The familybound is set to true if a transition between up and down type
      ! quarks can occur via FCCC
      familybound = .false.
      do i = 1, legs
        ! TODO: (nick 2015-11-23)
        ! familybound = (spin = 0 or spin = 1) and charged
        select case (get_particle_name_mdl(park(i)))
        case ('W+', 'W-', 'G+', 'G-', 'H+', 'H-'); familybound = .true.; exit
        end select

        do j = 1,legs; if(j .eq. i) cycle
          if (get_particle_spin_mdl(park(i)) .eq. 2 .and. &
              get_particle_spin_mdl(park(j)) .eq. 2) then
            if (get_particle_antiparticle_mdl(park(i)) .eq. &
                get_doublet_partner_mdl(park(j))) then
              familybound = .true.
            end if
          end if
        end do
      end do

      allocate (lightmass(n_particles))
      ! lightmass is a particle property and is set to true if simultaneously
      ! the following holds:
      ! - particle is a fermion
      ! - particle mass is below the zerocut (=:zeromass)
      ! - the particles doublet partner has zeromass too or the familybound is
      !   not set, meaning there is no transition from the particle to the
      !   doublet partner possible.
      allocate (extern(n_particles))
      ! extern is a particle property and is set to true if simultaneously the
      ! following holds:
      ! - the particle is final/initial state
      ! - the associated antiparticle is final/initial state
      ! - the associated doublet partner is final/initial state
      ! - the associated antiparticle doublet partner is final/initial state
      extern = .false.
      do p1 = 1, n_particles
        if (get_particle_spin_mdl(p1) .eq. 2) then
          p2 = get_doublet_partner_mdl(p1)
          lightmass(p1) = (get_particle_mass_reg_mdl(p1) .le. 2) .and. &
                         (.not. familybound .or.                  &
                          get_particle_mass_reg_mdl(p2) .le. 2)
          do i= 1,legs
            extern(p1) = extern(p1) .or.                                    &
                        (park(i) .eq. p1) .or.  (park(i) .eq. p2)     .or.  &
                        (park(i) .eq. get_particle_antiparticle_mdl(p1) ) .or.  &
                        (park(i) .eq. get_particle_antiparticle_mdl(p2) )
          end do
        end if
      end do

      loopCoef(:, pr) = 0
      ! loopCoef is a particle property and determines how often a loop
      ! contribution with the particle as `cutparticle` enters the final result
      ! A loop contribution can enter multiple times due to optimization (no
      ! doubel counting) whereas other loop contributions may not be calculated.
      ! A typically result for loopCoef with no familybound looks like
      ! loopCoef(downQuark) = 3      loopCoef(upQuark) = 2
      ! loopCoef(strangeQuark) = 0   loopCoef(charmQuark) = 0
      ! loopCoef(bottomQuark) = 0    loopCoef(top) = 1
      ! i.e. only the down, up and top quark loops are computed once and
      ! multiplied by the corresponding loopCoef
      do firstgen = 1, n_particles
        if (is_firstgen_mdl(firstgen)) then
          loopCoef(firstgen, pr) = 0
          secondgen = get_secondgen_mdl(firstgen)
          thirdgen = get_thirdgen_mdl(firstgen)
          if (lightmass(firstgen) .and. (.not. extern(firstgen))) then
            if (extern(secondgen) .and. lightmass(secondgen)) then
              loopCoef(secondgen,pr) = loopCoef(secondgen,pr) + 1
            else if (extern(thirdgen) .and. lightmass(thirdgen)) then
              loopCoef(thirdgen,pr) = loopCoef(thirdgen,pr) + 1
            else
              loopCoef(firstgen,pr) = loopCoef(firstgen,pr) + 1
            end if
          else
            loopCoef(firstgen,pr) = loopCoef(firstgen,pr) + 1
          end if
          if (lightmass(secondgen) .and. (.not. extern(secondgen))) then
            if (lightmass(firstgen) .and. loopCoef(firstgen,pr) .ne. 0) then
              loopCoef(firstgen,pr) = loopCoef(firstgen,pr) + 1
            else if (extern(thirdgen) .and. lightmass(thirdgen)) then
             loopCoef(thirdgen, pr) = loopCoef(thirdgen,pr) + 1
            else
              loopCoef(secondgen,pr) = loopCoef(secondgen,pr) + 1
            end if
          else
            loopCoef(secondgen,pr) = loopCoef(secondgen,pr) + 1
          end if
          if (lightmass(thirdgen) .and. (.not. extern(thirdgen))) then
            if (lightmass(firstgen) .and. loopCoef(firstgen,pr).ne.0) then
              loopCoef(firstgen,pr) = loopCoef(firstgen,pr) + 1
            else if (lightmass(secondgen).and. loopCoef(secondgen,pr).ne.0) then
              loopCoef(secondgen,pr) = loopCoef(secondgen,pr) + 1
            else
              loopCoef(thirdgen,pr) = loopCoef(thirdgen,pr) + 1
            end if
          else
            loopCoef(thirdgen,pr) = loopCoef(thirdgen, pr) + 1
          end if

        else if (get_particle_spin_mdl(firstgen) .ne. 2) then
          loopCoef(firstgen, pr) = 1
        end if
      end do
      deallocate (extern)
      deallocate (lightmass)
    else
      loopCoef(:, pr) = 1
    end if
  end subroutine optimize_fermionloop

!!!!!!!!!!!!!!!!!!!!!!!!!
!  fermion_sign_tables  !
!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine fermion_sign_tables(par)
    integer, intent(in), dimension(:) :: par
    integer                           :: e1,e2

    ! Get fermion relative signs according to hep-ph/0002082.
    ! The fermion relative signs obtained in this way are not
    ! always right at loop level and must be corrected by an
    ! overall sign (losigncorrection) which is computed later.
    fervec = 0
    do e1 = 1,pow2(size(par)-1)-1
      do i= 1, size(par)
        if ((get_particle_spin_mdl(par(i)) .eq. 2) .and. &
             vectorLeg(e1,i).eq.1 ) fervec(e1,i) = 1
      end do
    end do
    do e2= 1, lexp-1 ! no last leg, at most sum of all others
    do e1= 1, lexp-1
      csign(e1,e2) = 1

      do i = 2,legsE
      do j = 1,i-1
        csign(e1,e2) = csign(e1,e2) * (-1)**(fervec(e1,i)*fervec(e2,j))
      enddo
      enddo

    enddo
    enddo
  end subroutine fermion_sign_tables

!!!!!!!!!!!!!!!!!!
!  fermion_sign  !
!!!!!!!!!!!!!!!!!!

  function fermion_sign(ty,npoint,bin,par) result(sign)
    integer, intent(in)                      :: ty,npoint
    integer, intent(in), dimension(npoint-1) :: bin,par
    integer, dimension(npoint-1)             :: bin_ord,par_ord
    integer                                :: sign

    sign = 1
    if (npoint .gt. 2) then
      bin_ord = bin
      par_ord = par
      ! brings the binaries into the canonical order (antifermion | fermion)
      call spinor_ordering(ty,bin_ord,par_ord)
      if (npoint .eq. 3) then
        sign = csign(bin_ord(1), bin_ord(2))
      else if (npoint .eq. 4) then
        sign = csign(bin_ord(1),bin_ord(2)+bin_ord(3))*csign(bin_ord(2),bin_ord(3))
      else if (npoint .eq. 5) then
        sign = csign(bin_ord(1),bin_ord(2)+bin_ord(3)+bin_ord(4)) * &
               csign(bin_ord(2),bin_ord(3)+bin_ord(4))*             &
               csign(bin_ord(3),bin_ord(4))
      else if (npoint .eq. 6) then
        sign = csign(bin_ord(1),bin_ord(2)+bin_ord(3)+bin_ord(4)+bin_ord(5))* &
               csign(bin_ord(2),bin_ord(3)+bin_ord(4)+bin_ord(5))*            &
               csign(bin_ord(3),bin_ord(4)+bin_ord(5))*                       &
               csign(bin_ord(4),bin_ord(5))
      end if
    end if
  end function fermion_sign

  subroutine spinor_ordering(ty,bin,par)
    ! Orders spinors according to standard form:
    ! (\bar \psi_i) \psi_j  ...
    use modelfile, only: get_fourfermion_id_mdl
    integer, intent(in)                  :: ty
    integer, intent(inout), dimension(:) :: bin,par
    integer                              :: i,j,par_tmp,bin_tmp,ffid,f1,f2,f3,nferm
    integer, dimension(size(bin))        :: ferm_pos
    j = 0
    nferm = 0
    do i = 1, size(par)
      if (get_particle_spin_mdl(par(i)) .eq. 2) then
        nferm = nferm + 1
        ferm_pos(nferm) = i
      end if
    end do
    if (nferm .eq. 2) then
      f1 = ferm_pos(1)
      f2 = ferm_pos(2)
      if (is_particle_mdl(par(f1))) then
        par_tmp = par(f1)
        par(f1) = par(f2)
        par(f2) = par_tmp
        bin_tmp = bin(f1)
        bin(f1) = bin(f2)
        bin(f2) = bin_tmp
      end if
    else if (nferm .eq. 3) then
      ffid = get_fourfermion_id_mdl(ty)
      f1 = ferm_pos(1)
      f2 = ferm_pos(2)
      f3 = ferm_pos(3)
      if (ffid .eq. 1) then ! (1, 2), (3, 4)
        if (is_particle_mdl(par(f1))) then
          par_tmp = par(f1)
          par(f1) = par(f2)
          par(f2) = par_tmp
          bin_tmp = bin(f1)
          bin(f1) = bin(f2)
          bin(f2) = bin_tmp
        end if
      else if (ffid .eq. 2) then ! ((1,3), (2,4))
        if (is_particle_mdl(par(f1))) then
          ! (f3,f1), f2
          par_tmp = par(f1)
          par(f1) = par(f3)
          par(f3) = par(f2)
          par(f2) = par_tmp
          bin_tmp = bin(f1)
          bin(f1) = bin(f3)
          bin(f3) = bin(f2)
          bin(f2) = bin_tmp

          ! f2 (f3,f1)
          !par_tmp = par(f1)
          !par(f1) = par(f2)
          !par(f2) = par(f3)
          !par(f3) = par_tmp
          !bin_tmp = bin(f1)
          !bin(f1) = bin(f2)
          !bin(f2) = bin(f3)
          !bin(f3) = bin_tmp
        else
          ! (f1,f3), f2
          par_tmp = par(f3)
          par(f3) = par(f2)
          par(f2) = par_tmp
          bin_tmp = bin(f3)
          bin(f3) = bin(f2)
          bin(f2) = bin_tmp

          ! f2, (f1,f3)
          !par_tmp = par(f1)
          !par(f1) = par(f2)
          !par(f2) = par_tmp
          !
          !bin_tmp = bin(f1)
          !bin(f1) = bin(f2)
          !bin(f2) = bin_tmp
        end if
      else if (ffid .eq. 3) then ! ((1,4), (2,3))
        ! 1, (2,3)
        if (is_particle_mdl(par(f2))) then
          ! 1, (f3,f2)
          par_tmp = par(f2)
          par(f2) = par(f3)
          par(f3) = par_tmp

          bin_tmp = bin(f2)
          bin(f2) = bin(f3)
          bin(f3) = bin_tmp
        ! else 1, (f2, f3)
        end if

      else
        call error_rcl('Four-fermion id  '//to_str(ffid)// &
                       ' not supported.')
      end if
    else if (nferm .gt. 3) then
      call error_rcl('Vertex '//to_str(ty)// &
                     ' not supported with nferm=' // to_str(nferm) //'.')
    end if

  end subroutine

!!!!!!!!!!!!!!!
!  loop_sign  !
!!!!!!!!!!!!!!!

  subroutine loop_sign(ferloopsign,ghostloopsign,signcorrection)
    implicit none
    integer, intent(inout) :: ferloopsign,ghostloopsign,signcorrection
    integer, allocatable   :: sec(:),nfer(:)

    allocate ( sec(legs+1))
    allocate (nfer(legs+1))
    if (hosT(1,wN) .ne. -1) then
      spin = get_particle_spin_mdl(t)
      select case (abs(spin))
      case (2)
        floop = 0
        do ii = 1, legs
          nfer(ii) = 0
          sec(ii) = 0
          do k = 1,size(hosT,1)
            if (hosT(k,wN)+1.eq.ii) sec(ii) = sec(ii) + pow2(k-1)
          enddo
        enddo
        do ii = 1, legs ! loop over propagators
          if (sec(ii) .ne. 0) then
            do n = 1, legs
              nfer(ii) = nfer(ii) + fervec(sec(ii),n)
            enddo
          endif
        enddo
        do ii= 1, legs
          floop = floop + mod (nfer(ii),2)
        enddo
      case default
        floop = 1
      end select
    else
      floop = 1
    endif
    if (floop.eq.0) then
      ferloopsign = - 1
    else
      ferloopsign = + 1
    endif
    deallocate (nfer)
    deallocate ( sec)

    spin = get_particle_spin_mdl(t)
    select case (spin)
    case (-1)
      ghostloopsign = - 1
    case default
      ghostloopsign = + 1
    end select

    ! Correction of the relative fermion signs
    select case (ferloopsign)
    case (1)
      spin = get_particle_spin_mdl(park(legs))
      select case (spin .ne. 2)
      case (.true.)
        select case (get_particle_spin_mdl(t) .ne. 2)
        case (.true.)
          signcorrection = +1
        case default
          signcorrection = -1
        end select
      case default
        if (is_particle_mdl(park(legs)) .eqv. .true. ) then
          select case (get_particle_spin_mdl(t) .ne. 2)
          case (.true.)
            signcorrection = +1
          case default
            signcorrection = -1
          end select
        else
          select case (get_particle_spin_mdl(t) .ne. 2)
          case (.true.)
            signcorrection = -1
          case default
            signcorrection = +1
          end select
        end if
      end select
    case default
      spin = get_particle_spin_mdl(park(legs))
      select case (spin .ne. 2)
      case (.true.)
        signcorrection = +1
      case default
        if (is_particle_mdl(park(legs)) .eqv.  .true. ) then
          signcorrection = +1
        else
          signcorrection = -1
        end if
      end select
    end select
  end subroutine loop_sign

!==============================================================================!
!                              fix_s_branch_vars                               !
!==============================================================================!
! current properties of current j are copied to global variables.
  subroutine fix_s_branch_vars
    use tables_rcl, only: pim
    integer :: perm,kp,iip

    if (eN .lt. pow2(legs)) then ! tree branch

      if (daj(j) .eq. 0) then ! mother branch

        sm0j(j) = s

        ! Mother index
        mosm0(s,pr) = moj(j)

        ! binaries for s
        binsm0(nDef,s,pr) = nDef

        perm = permT(j)
        do k = 1, nDef-1
          kp = pim(k,perm)
          binsm0(k,s,pr) = ee(kp)
          if (binsm0(nDef,s,pr) .eq. nDef .and. ee(kp) .eq. 0) binsm0(nDef,s,pr) = kp
        end do

        parsm0(s,pr) = pN

        ! xs = T -> ct or r2 coupling
        ! xs = F -> normal tree coupling
        xsm0(s,pr) = xs

        ! Increase of the power in fund. couplings
        gsIncsm0(:,s,pr) = orderIncT(:,j)

        allocate (cosm0(s,pr)%c(size(couT(j)%c)))
        cosm0(s,pr)%c(:) = couT(j)%c(:)

        col1 = get_colourfac_mdl(colourfacT(1,j))*Nc**colourfacT(2,j)
        cosm0(s,pr)%factor = col1 * fersign

        allocate (cosm0(s,pr)%val(size(couT(j)%c)))
        allocate (cosm0(s,pr)%valR(size(couT(j)%c)))
        do ii = 1, size(cosm0(s,pr)%c)
          cosm0(s,pr)%val(ii) = get_coupling_value_mdl(cosm0(s,pr)%c(ii))*cosm0(s,pr)%factor
          cosm0(s,pr)%valR(ii) = -cima*cosm0(s,pr)%val(ii)
        end do

        cosm0(s,pr)%m2 = get_particle_cmass2_mdl(pN)
        cosm0(s,pr)%m = sqrt(get_particle_cmass2_mdl(pN))

        ! Color structures, just last branches have non-vanishing
        ! result
        cssm0(s,pr) = csj(j)

        if (eN .eq. pow2(legsE-1)-1) then ! last branch
          ! gs power for the outgoing current
          oi_id = reg_od(orderT(:,wN),pr)
          gssm0(s,pr) = oi_id
        endif


      else

        sd0j(j) = s

        ! Daughter index
        dasd0(s,pr) = daj(j)

        if (abs(imag(facj(j))) .gt. 0d0) then
          write (*, *) 'facj has imaginary part'
          stop
        end if

        ! Daughter factor
        facsd0(s,pr) = real(facj(j))

        ! self-energy
        sesd0(s,pr) = eN .eq. ee(1)

        ! Color structures
        ! (just last branches have no vanishing result)
        cssd0(s,pr) = csj(j)

        if (eN .eq. pow2(legsE-1)-1) then ! last branch
          ! gs power for the outgoing current
          oi_id = reg_od(orderT(:,wN),pr)
          gssd0(s,pr) = oi_id
        endif
      endif

    else ! loop branch

      if (daj(j) .eq. 0) then
        sm1j(j) = s

        ! Mother index
        mosm1(s,pr) = moj(j)

        ! Binaries
        perm = permT(j)
        binsm1(nDef,s,pr) = nDef
        do ii = 1, nDef-1
          iip = pim(ii,perm)
          binsm1(ii, s, pr) = ee(iip)
          if (binsm1(nDef,s,pr) .eq. nDef .and. ee(iip) .eq. 0) binsm1(nDef,s,pr) = iip
        end do

        ! Fields (all fields incoming)
        parsm1(s,pr) = pN

        ! Increase of the power in fund. couplings
        gsIncsm1(:,s,pr) = orderIncT(:,j)

        ! Global coefficient
        allocate (cosm1(s,pr)%c(size(couT(j)%c)))
        cosm1(s,pr)%c(:) = couT(j)%c(:)

        col1 = get_colourfac_mdl(colourfacT(1,j))*Nc**colourfacT(2,j)
        cosm1(s,pr)%factor = col1 * fersign *               &
                             signcorrection * ferloopsign * &
                             ghostloopsign


        allocate (cosm1(s,pr)%val(size(couT(j)%c)))
        allocate (cosm1(s,pr)%valR(size(couT(j)%c)))
        do ii = 1, size(cosm1(s,pr)%c)
          cosm1(s,pr)%val(ii) = get_coupling_value_mdl(cosm1(s,pr)%c(ii))*cosm1(s,pr)%factor
          cosm1(s,pr)%valR(ii) = -cima*cosm1(s,pr)%val(ii)
        end do

        cosm1(s,pr)%m2 = get_particle_cmass2_mdl(pN)
        cosm1(s,pr)%m = sqrt(get_particle_cmass2_mdl(pN))

        ! rankIn and rankOut
        rankInsm1(s,pr)  = rankInj(j)
        rankOutsm1(s,pr) = rankOutj(j)

        ! Color structures (just last
        ! branches have no vanishing result)
        cssm1(s,pr) = csj(j)

        if (eN .eq. pow2(legsE-1)-1) then ! last branch

          ! The last branch of a fermion loop has ferloop=T
          ferloopsm1(s,pr) = (ferloopsign.eq.-1)

          ! gs power for the outgoing current

          oi_id = reg_od(orderT(:,wN),pr)
          gssm1(s,pr) = oi_id

          ! Tensor integrals
          tism1(s,pr) = tij(j)

        endif

      else

        sd1j(j) = s

        ! Daughter index
        dasd1(s,pr) = daj(j)

        if (abs(imag(facj(j))) .gt. 0d0) then
          write (*, *) 'facj has imaginary part'
          stop
        end if

        ! Daughter factor
        facsd1(s,pr) = real(facj(j))

        ! rankOut
        rankOutsd1(s,pr) = rankOutj(j)

        ! Color structures (just last
        ! branches have no vanishing result)
        cssd1(s,pr) = csj(j)

        if (eN .eq. pow2(legsE-1)-1) then ! last branch

          ! The last branch of a fermion loop has ferloop=T
          ferloopsd1(s,pr) = (ferloopsign.eq.-1)

          ! gs power for the outgoing current
          oi_id = reg_od(orderT(:,wN),pr)
          gssd1(s,pr) = oi_id

          ! Tensor integrals
          tisd1(s,pr) = tij(j)

        endif

      endif

    endif
  end subroutine fix_s_branch_vars

!==============================================================================!
!                              fix_b_branch_vars                               !
!==============================================================================!

  subroutine fix_b_branch_vars
    use tables_rcl, only: pim
    integer :: perm,kp
    if (eN .lt. pow2(legs)) then ! tree branch
      if (daj(j) .eq. 0) then ! mother branch

        ! Map from b to s branches
        pm(pr)%tree_m_maps(b)%sb = sm0j(j)

        ! Field permutation
        perm = permT(j)

        ! Incomming and outgoing currents
        do k = 1,nDef-1
          kp = pim(k,perm)
          if (wT(kp,j) .ne. 0) then
            pm(pr)%tree_m_maps(b)%w_in(k) = wtTOw0b(wT(kp,j))
          endif
        enddo
        if (eN .ne. pow2(legsE-1)-1) then
          pm(pr)%tree_m_maps(b)%w_out = wtTOw0b(wN)
        end if

        ! The current is initialized in this branch?
        pm(pr)%tree_m_maps(b)%w_init = winitj(j)

        ! Interaction type
        pm(pr)%tree_m_maps(b)%ty = typeT(j)
        pm(pr)%tree_m_maps(b)%tyR = get_recola_base_mdl(typeT(j))
        pm(pr)%tree_m_maps(b)%pext = get_propagator_extension_mdl(typeT(j))


        ! min and max branches
        if (startbm0 .eq. 0) then
          pm(pr)%bm0(ej(j),i,c)%bmin = b
          startbm0 = 1
        else if (ej(j).gt.ebm0) then
          pm(pr)%bm0(ebm0,i,c)%bmax = b - 1
          pm(pr)%bm0(ej(j),i,c)%bmin = b
        end if
        pm(pr)%bm0(ej(j),i,c)%bmax = b
        ebm0 = ej(j)

      else ! daughter branch

        ! Map from b to s branches
        pm(pr)%tree_d_maps(b)%sb = sd0j(j)

        ! Outgoing current
        if (eN .ne. pow2(legsE-1)-1) then
          pm(pr)%tree_d_maps(b)%w_out = wtTOw0b(wN)
        end if

        ! The current is initialized in this branch?
        pm(pr)%tree_d_maps(b)%w_init = winitj(j)

        ! min and max branches
        if (startbd0.eq.0) then
          pm(pr)%bd0(ej(j),i,c)%bmin = b
          startbd0 = 1
        else if (ej(j).gt.ebd0) then
          pm(pr)%bd0(ebd0,i,c)%bmax = b - 1
          pm(pr)%bd0(ej(j),i,c)%bmin = b
        end if
        pm(pr)%bd0(ej(j),i,c)%bmax = b
        ebd0 = ej(j)

      endif

    else ! loop branch

      if (daj(j) .eq. 0) then ! mother branch

        ! Map from b to s branches
        pm(pr)%loop_m_maps(b)%sb = sm1j(j)

        ! Field permutation
        perm = permT(j)

        ! Incomming and outgoing currents
        pm(pr)%loop_m_maps(b)%w_in(1) = wtTOw1b(wT(1,j),i)
        do k = 2,nDef-1
          kp = pim(k,perm)
          if (wT(kp,j).ne.0) then
            pm(pr)%loop_m_maps(b)%w_in(k) = wtTOw0b(wT(kp,j))
          end if
        end do

        if (eN .ne. pow2(legsE-1)-1) then
          pm(pr)%loop_m_maps(b)%w_out = wtTOw1b(wN,i)
        end if

        ! The current is initialized in this branch?
        pm(pr)%loop_m_maps(b)%w_init = winitj(j)

        ! Interaction type
        pm(pr)%loop_m_maps(b)%ty = typeT(j)
        pm(pr)%loop_m_maps(b)%tyR = get_recola_base_mdl(typeT(j))
        pm(pr)%loop_m_maps(b)%pext = get_propagator_extension_mdl(typeT(j))

        ! Field permutation
        pm(pr)%loop_m_maps(b)%perm = permT(j)

        ! min and max branches
        if (startbm1(ih1).eq.0) then
          pm(pr)%bm1(ej(j),i,c)%bmin = b
          startbm1(ih1) = 1
        elseif (ej(j).gt.ebm1(ih1)) then
          pm(pr)%bm1(ebm1(ih1),i,c)%bmax = b -1
          pm(pr)%bm1(ej(j),i,c)%bmin = b
        endif
        pm(pr)%bm1(ej(j),i,c)%bmax = b
        ebm1(ih1) = ej(j)

      else ! daughter branch
        ! Map from b to s branches
        pm(pr)%loop_d_maps(b)%sb = sd1j(j)

        ! Outgoing current
        if (eN .ne. pow2(legsE-1)-1) then
          pm(pr)%loop_d_maps(b)%w_out = wtTOw1b(wN,i)
        end if

        ! The current is initialized in this branch?
        pm(pr)%loop_d_maps(b)%w_init = winitj(j)

        ! min and max branches
        if (startbd1(ih1).eq.0) then
          pm(pr)%bd1(ej(j),i,c)%bmin = b
          startbd1(ih1) = 1
        elseif (ej(j).gt.ebd1(ih1)) then
          pm(pr)%bd1(ebd1(ih1),i,c)%bmax = b - 1
          pm(pr)%bd1(ej(j),i,c)%bmin = b
        endif
        pm(pr)%bd1(ej(j),i,c)%bmax = b
        ebd1(ih1) = ej(j)

      endif

    endif
  end subroutine fix_b_branch_vars

!==============================================================================!
!                               register_current                               !
!==============================================================================!

! If the outgoing current of present branch has the same properties of an
! already computed current of a previous lp, t, fh or configs,  the current
! number w0Tot is not increased and wtTOw0b is fixed to the value of w0Tot of
! that current. In that case, no current needs to be computed and the branch j
! is skipped. This is not done for last branches, which are always computed.
! First we define peakleg(k): if the external leg k would contribute to the
! outgoing current of this branch (this can be seen from the binary tag of the
! current), peakleg(k) is set to 1, otherwise it is 0.

  function register_current() result(new_current)
    logical :: new_current
    integer :: kk,fh0
    if (eN .lt. pow2(legs) .and. sav .eq. rep) then ! tree branch

      do k = 1,legsE; kbin = pow2(k-1)
        peakleg(k) = 0
        do kk = 1,legsE
          if (kbin.eq.firstNumbers(eN,kk)) &
             peakleg(k) = 1
        enddo
      enddo
      check0 = 1
      lp0step = max(lp,1)
      ! Just the tree level (lp0=0) and the present
      ! level (lp0=lp) are considered
      do lp0 = 0,lp,lp0step
        do t0 = 1,t
          do fh0 = fhmax(t0),fhmin(t0),-2
            if ( lp0.eq.lp .and. &
                  t0.eq.t  .and. &
                 fh0.eq.fh       ) then
              i0max = i-1
            else
              i0max = cfTot(pr)
            endif
            do i0 = 1,i0max
              ! Here the helicities of the external
              ! particles contributing to the old
              ! and present currents are checked
              peak = 0
              do k = 1,legs
                peak = &
                peak + peakleg(k) * &
                       abs(he(k,i,pr)-he(k,i0,pr))
              enddo
              ! An old lp0, t0 (cut), fh0, i0
              ! (config) can have produced the
              ! same current as the present branch.
              ! Now all currents for that lp0, t0,
              ! fh0, i0 are scanned to see, whether
              ! one of them has the same properties
              ! as the outgoing current of the
              ! present branch.
              if ( (peak.eq.0) .and. &
                   (w0min(ej(j),i0,fh0,t0,lp0).ne.0) ) then
                w0loop: do w0 = w0min(ej(j),i0,fh0,t0,lp0), &
                                w0max(ej(j),i0,fh0,t0,lp0)
                  if(parT(wN).ne.parw0(w0)) cycle w0loop
                  if(binT(wN).ne.binw0(w0)) cycle w0loop
                  if(xxxT(wN).ne.xxxw0(w0)) cycle w0loop
                  if (sum(abs(orderT(:,wN)-orderw0(:,w0))) .ne. 0) cycle w0loop
                  do ii = -1,legs
                    if (csT(ii,wN).ne.csw0(ii,w0)) cycle w0loop
                  enddo
                  if (U1gT(wN).neqv.U1gw0(w0) ) cycle w0loop
                  check0 = 0
                  wtTOw0b(wN) = w0
                  new_current = .false.
                  return
                enddo w0loop
              endif
            enddo
          enddo
        enddo
      enddo
      ! If we are here, it means that the current
      ! was not already computed. The current
      ! number w0Tot and wtTOw0b have to be fixed
      ! and the current needs to be computed.
      if (jfirstwT(wN).eq.j) then
        ! If we are here, it means the current is
        ! new and must be computed (and w0Tot is
        ! increased).
        w0Tot(pr) = w0Tot(pr) + 1
        if (w0Tot(pr).gt.w0Def) call reallocate_w0Def
        wtTOw0b(wN)              = w0Tot(pr)
        parw0(w0Tot(pr))         = parT(wN)
        binw0(w0Tot(pr))         = binT(wN)
        csw0(-1:legsE,w0Tot(pr)) = csT(-1:legsE,wN)
        xxxw0(w0Tot(pr))         = xxxT(wN)
        orderw0(:,w0Tot(pr))     = orderT(:,wN)
        U1gw0(w0Tot(pr))         = U1gT(wN)
        ! min and max currents
        if (startw0.eq.0) then
          w0min(ej(j),i,fh,t,lp) = w0Tot(pr)
          startw0 = 1
        elseif (ej(j).gt.ew0) then
          w0max(ew0,i,fh,t,lp) = w0Tot(pr) - 1
          w0min(ej(j),i,fh,t,lp) = w0Tot(pr)
        endif
        w0max(ej(j),i,fh,t,lp) = w0Tot(pr)
        ew0 = ej(j)
      endif

    elseif (eN .ge. pow2(legs)) then ! loop branch

      check0 = 1
      do k = 1,legsE; kbin = pow2(k-1)
        peakleg(k) = 0
        do kk = 1,legsE
          if (kbin.eq.firstNumbers(eN,kk)) &
            peakleg(k) = 1
        enddo
      enddo
      ! An old i0 (config), with same lp=1, t, fh
      ! can have produced the same current as the
      ! present branch. Since lp, t and fh are the
      ! same, the current index wN is the same for
      ! all i; thereforein order to check whether
      ! the current has alreadybeen computed, one
      ! must have peak=0 and check whether for the
      ! old i0 the current wN has been computed.
      ! This is what okwti does.
      do i0 = 1,i-1
        peak = 0
        do k = 1,legs
          peak =              &
          peak + peakleg(k) * &
                 abs(he(k,i,pr)-he(k,i0,pr))
        enddo
        if ( (peak.eq.0) .and. okwti(wN,i0)) then
          check0 = 0
          if (sav.eq.1.and.rep.eq.0) then
            if (wtTOw1b(wN,i0).eq.0) then
              w1sTot(ih1) = w1sTot(ih1) + 1
              wtTOw1b(wN, i) = w1sTot(ih1)
              wtTOw1b(wN,i0) = w1sTot(ih1)
            else
              wtTOw1b(wN,i) = wtTOw1b(wN,i0)
            endif
          endif
          new_current = .false.
          return
        endif
      enddo
      if (check0.ne.0.and.jfirstwT(wN).eq.j) then
        if (rep.eq.0) then
          okwti(wN,i) = .true.
        else
          if (wtTOw1b(wN,i).eq.0) then
            w1lTot(ih1,i) = w1lTot(ih1,i) + 1
            wtTOw1b(wN,i) = &
            w1sTot(ih1) + w1lTot(ih1,i)
          endif
        endif
      endif

    endif
    new_current = .true.

  end function register_current
!==============================================================================!
!                             set_tensor_parameter                             !
!==============================================================================!

  subroutine set_tensor_parameter(j)
    integer, intent(in) :: j       ! current branch
    integer             :: k,check
    check = 1
    ! Check if the propagator and mass configuration matchs an already
    ! registered configuration
    do k = 1, titot(pr)
      check = + sum(abs(hosT(:,wT(nDef,j))-momsti(:legs,k,pr))) &
              + abs(hmaT(wT(nDef,j))-massti(k))
      if (check.eq.0) then
        tij(j) = k
        rankti(k,pr) = max(rankti(k,pr),rankOutj(j))
        exit
      endif
    enddo

    ! If no match has found register a new tensor integral
    if (check .ne. 0) then
      tiTot(pr) = tiTot(pr) + 1
      tij(j) = tiTot(pr)
      legsti(tiTot(pr),pr) = maxval(hosT(:,wN)) + 1
      momsti(1:size(hosT,1),tiTot(pr),pr) = hosT(:,wN)
      massti(tiTot(pr))    = hmaT(wN)
      rankti(tiTot(pr),pr) = rankOutj(j)
    endif
  end subroutine set_tensor_parameter

!------------------------------------------------------------------------------!

  subroutine reallocate_w0Def

  integer              :: l,wdef0,wdef
  integer, allocatable :: tempI(:,:)
  logical, allocatable :: tempL(:,:)

  wdef0 = w0Def
  wdef  = w0Def + w0Inc

  l = size(csw0,1)-2;  allocate (tempI(-1:l,wdef0))
    tempI = csw0; deallocate (csw0)
    allocate (csw0(-1:l,wdef)); csw0(:,:wdef0) = tempI(:,:)
  deallocate (tempI)

  allocate (tempI(1,wdef0))
    tempI(1,:) = parw0(:); deallocate (parw0)
    allocate (parw0(wdef)); parw0(:wdef0) = tempI(1,:)
    tempI(1,:) = binw0(:); deallocate (binw0)
    allocate (binw0(wdef)); binw0(:wdef0) = tempI(1,:)
    tempI(1,:) = xxxw0(:); deallocate (xxxw0)
    allocate (xxxw0(wdef)); xxxw0(:wdef0) = tempI(1,:)
  deallocate (tempI)

  allocate(tempI(n_orders,wdef0))
    tempI(:,:) = orderw0(:,:); deallocate (orderw0)
    allocate (orderw0(n_orders,wdef)); orderw0(:,:wdef0) = tempI(:,:)
  deallocate (tempI)

  allocate (tempL(1,wdef0))
    tempL(1,:) = U1gw0(:); deallocate (U1gw0)
    allocate (U1gw0(wdef)); U1gw0(:wdef0) = tempL(1,:)
  deallocate (tempL)

  w0Def = wdef

  end subroutine reallocate_w0Def

!==============================================================================!
!                             prepare_formcurrent                              !
!==============================================================================!
! Sets the symbolic expression for the colorfactor and computes the sign
! factor
  subroutine prepare_formcurrent(jj,Nc_power,sign,colT_symbol,colT_imag)
    integer, intent(out) :: jj,Nc_power,sign
    character(len=20)    :: colT_symbol
    logical, intent(out) :: colT_imag
    jj       = j
    Nc_power = 0
    sign     = fersign

    if (eN .ge. pow2(legs)) then ! loop branch
      sign = sign * signcorrection*ferloopsign*ghostloopsign
    end if

    Nc_power = Nc_power + colourfacT(2,jj)
    colT_imag   = .false.
    colT_symbol = get_colourfac_form_mdl(colourfacT(1,jj))

  end subroutine prepare_formcurrent

!------------------------------------------------------------------------------!

  subroutine check_vanishing_couplings(cou,form_output)
    use modelfile, only: get_coupling_value_mdl
    logical, intent(in)               :: form_output
    logical, intent(out), allocatable :: cou(:)
    allocate(cou(size(couT(j)%c)))
    do i = 1, size(couT(j)%c)
      if (couplings_active_frm .and. form_output) then
        cou(i) = couT(j)%c(i) .ne. 0
      else
        select case (lp)
        case (TreeBranch)
          if (tree_couplings_active_frm .and. form_output) then
            cou(i) = couT(j)%c(i) .ne. 0
          else
            cou(i) = (abs(get_coupling_value_mdl(couT(j)%c(i))) .gt. zerocut)
          end if
        case (LoopBranch)
          if (loop_couplings_active_frm .and. form_output) then
            cou(i) = couT(j)%c(i) .ne. 0
          else
            cou(i) = (abs(get_coupling_value_mdl(couT(j)%c(i))) .gt. zerocut)
          end if
        case (CTBranch)
          if (ct_couplings_active_frm .and. form_output) then
            cou(i) = couT(j)%c(i) .ne. 0
          else
            cou(i) = (abs(get_coupling_value_mdl(couT(j)%c(i))) .gt. zerocut)
          end if
        case (R2Branch)
          if (r2_couplings_active_frm .and. form_output) then
            cou(i) = couT(j)%c(i) .ne. 0
          else
            cou(i) = (abs(get_coupling_value_mdl(couT(j)%c(i))) .gt. zerocut)
          end if
        end select
      end if
    end do
  end subroutine check_vanishing_couplings

!------------------------------------------------------------------------------!

  end subroutine generate_currents

!------------------------------------------------------------------------------!

end module currents_rcl

!------------------------------------------------------------------------------!
