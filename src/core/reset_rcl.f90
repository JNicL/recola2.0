!******************************************************************************!
!                                                                              !
!    reset_rcl.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module reset_rcl

!------------------------------------------------------------------------------!

  use globals_rcl
  use amplitude_rcl
  use tables_rcl
  use order_rcl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine deallocate_rcl
    use modelfile, only: incRI_mdl=>incRI

    integer :: i,n
    logical :: fileopen
    ! This subroutine deallocates all global allocatable arrays.
    ! It has to be called at the end of the program.

    deallocate (timeTI,timeTC,timeHS)
    deallocate(prs)
    if (allocated(momenta))      deallocate(momenta)
    if (allocated(matrix))       deallocate(matrix)
    if (allocated(matrix2))      deallocate(matrix2)
    if (allocated(matrix2h))     deallocate(matrix2h)
    if (allocated(matrix2int))   deallocate(matrix2int)
    if (allocated(matrix2cc))    deallocate(matrix2cc)
    if (allocated(matrix2ccint)) deallocate(matrix2ccint)
    if (allocated(matrix2ccnlo)) deallocate(matrix2ccnlo)
    if (allocated(matrix2scc))   deallocate(matrix2scc)
    if (allocated(matrix2sccnlo))deallocate(matrix2sccnlo)
    if (allocated(matrix2sc))    deallocate(matrix2sc)
    if (allocated(matrix2scnlo)) deallocate(matrix2scnlo)
    if (allocated(matrix2scm))   deallocate(matrix2scm)
    if (allocated(matrix2scm))   deallocate(matrix2scm)
    deallocate (factor,newleg,oldleg,newbin,oldbin,defp2bin,p2bin,   &
                defresbin,pspbin,mONS,cmONS2,cmREG2,cfTot,csTot,     &
                pCsTot,csAmp,csIq,nAmp,pAmp,facAmp,w0eTot,he,        &
                dualconf,lpmax,cd0sMax,w0Tot,w1Tot,w0last,parw0e,    &
                binw0e,legw0e,helw0e,c0EffMax,modaTot,zeroLO,mosm0,  &
                binsm0,parsm0,xsm0,gsIncsm0,cosm0,gssm0,cssm0,dasd0, &
                sesd0,facsd0,gssd0,cssd0,c0TOlp)

    deallocate(pm)

    if (loopMax) deallocate (loopCoef,cEffMax,tiTot,ritiMax,          &
                             mosm1,binsm1,parsm1,gsIncsm1,cosm1,      &
                             rankInsm1,rankOutsm1,ferloopsm1,gssm1,   &
                             cssm1,tism1,dasd1,facsd1,rankOutsd1,     &
                             ferloopsd1,gssd1,cssd1,tisd1,legsti,     &
                             momsti,vmti,rankti,cTOt,cTOfh,cTOih1,    &
                             w1TotMax,riwMax)

    if (allocated(nCache)) deallocate(nCache)
    if (allocated(nCacheTot)) deallocate(nCacheTot)
    if (allocated(tiCache)) deallocate(tiCache)
    if (allocated(cacheOn)) deallocate(cacheOn)
    if (allocated(gcch)) then
      do i = 1, size(gcch)
        if (allocated(gcch(i)%hist)) deallocate(gcch(i)%hist)
        gcch(i)%constructed = .false.
      end do
      deallocate(gcch)
    end if
    activeCache = 0
    nCacheGlobal = 0
    nprCall = 0
    cache_mode = 1

    ! tables
    deallocate (levelLeg,vectorLeg,firstNumber,firstGap,firstNumbers, &
                firstGaps)
    if (loopMax) deallocate (RtoS,riMin,riMax,ri,RItoR,RItoI,incRI, &
                             firstRI,firstRI_r21,firstRI_r22)
    if(loopMax) deallocate(incRI_mdl)
    if (allocated(als0R)) deallocate(als0R,dZgs0R,Qren0R,Nlq0R)

    ! order_keys
    if (allocated(oi_reg)) deallocate(oi_reg)
    if (allocated(oi2_reg)) deallocate(oi2_reg)
    if (allocated(max_od)) deallocate(max_od)
    if (allocated(compoi)) deallocate(compoi)
    oiDef = 10
    if (allocated(oiSize)) deallocate(oiSize)
    if (allocated(oi2Size)) deallocate(oi2Size)

  ! Set internal variables to default values
    resIR = .false.
    prTot = 0
    loopMax = .false.
    processes_generated = .false.
    ifail = -1
    iwarn = 0
    dynamic_settings = 0
    longitudinal = 0
    longitudinal_nlo = .false.

    do i = 1,nOpened
      inquire(file=trim(nameOpened(i)),opened=fileopen,number=n)
      if (fileopen) close(n)
    enddo
    outputfile = 'output.rcl'
    nx = 934758

    call deallocate_vertices_rcl()
    call deallocate_particles_rcl()

  end subroutine deallocate_rcl

!------------------------------------------------------------------------------!

  subroutine deallocate_vertices_rcl()
    use modelfile, only: clear_vertices_mdl
    call clear_vertices_mdl
  end subroutine deallocate_vertices_rcl

!------------------------------------------------------------------------------!

  subroutine deallocate_particles_rcl()
    use modelfile, only: clear_particles_mdl
    call clear_particles_mdl
  end subroutine deallocate_particles_rcl

!------------------------------------------------------------------------------!

  subroutine reset_recola_rcl

    call deallocate_rcl

  end subroutine reset_recola_rcl

!------------------------------------------------------------------------------!

end module reset_rcl

!------------------------------------------------------------------------------!
