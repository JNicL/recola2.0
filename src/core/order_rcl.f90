!******************************************************************************!
!                                                                              !
!    order_rcl.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module order_rcl
  ! Module for dealing with the power counting.
  use modelfile, only: get_n_orders_mdl
  use globals_rcl, only: n_orders, prTot, error_rcl, warning_rcl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  ! keeps track of the non-vanishing orders of a process
  ! 1: order id
  ! 2: type of order, e.g. QCD, QED, ...
  ! value: power in the order of order type for that process and order id
  integer, parameter :: oiInc = 10
  integer :: oiDef = 10
  integer, dimension(:), allocatable :: oiSize
  logical, dimension(:,:), allocatable :: comp0gs(:,:),comp1gs(:,:),compoi(:,:,:)
  integer, dimension(:,:,:), allocatable :: oi_reg
  integer, dimension(:), allocatable :: max_od

  integer, dimension(:), allocatable :: oi2Size
  integer, dimension(:,:,:), allocatable :: oi2_reg

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine get_oi(od,pr,funcname,oi)

  ! Returns the order id "oi" given a list of powers in the fundamental
  ! couplings "od".
  ! Example:  [QCD=2, QED=3] then od = [2, 3]
  !           The order of QCD,QED is given by the modelfile. See get_ord on how
  !           to get the order in the fundamental couplings.

  integer, dimension(:), intent(in) :: od
  integer,               intent(in) :: pr
  character(len=*),      intent(in) :: funcname
  integer,               intent(out) :: oi

  integer :: nord
  character(len=2) nord_str

  nord = get_n_orders_mdl()
  if (size(od) .ne. nord) then
    write(nord_str,'(i2)') nord

    call error_rcl(funcname // ' called with wrong order. '//  &
                   'Argument od has to have length ' // nord_str // '.')
  end if

  oi = check_oi(od,pr)
  if (oi .eq. -1) then
    call warning_rcl(funcname // ' called with nonexsting order')
  end if

  end subroutine get_oi

!------------------------------------------------------------------------------!

  subroutine get_oi2(od,pr,funcname,oi2)

  ! Returns the order id "oi2" given a list of powers in the fundamental
  ! couplings "od". oi2 are the order ids registered for squared matrix
  ! elements.

  integer, dimension(:), intent(in) :: od
  integer,               intent(in) :: pr
  character(len=*),      intent(in) :: funcname
  integer,               intent(out) :: oi2

  integer :: nord
  character(len=2) nord_str

  nord = get_n_orders_mdl()
  if (size(od) .ne. nord) then
    write(nord_str,'(i2)') nord

    call error_rcl(funcname // ' called with wrong order. '//  &
                   'Argument od has to have length ' // nord_str // '.')
  end if

  oi2 = check_oi2(od,pr)
  if (oi2 .eq. -1) then
    call warning_rcl(funcname // ' called with nonexsting order')
  end if

  end subroutine get_oi2

!------------------------------------------------------------------------------!

  function check_oi(od,pr) result(oi_id)
    ! returns whether od is registered(i.e. if od is found in oi_reg)
    ! return -1 if not found otherwise return the oi_id corresponding to oi
    integer, intent(in), dimension(:) :: od
    integer, intent(in) :: pr
    integer, parameter :: oi_end = -1
    integer :: i, oi_id

    if (oiSize(pr) .ne. 0) then
      oi_id = oi_end
      i = 1
      do while (oi_reg(1,i,pr) .ne. oi_end)
        if (sum(abs(oi_reg(:,i,pr)-od(:))) .eq. 0) then
          oi_id = i
          exit
        end if
        i = i + 1
        if (i .gt. oiSize(pr)) then
          exit
        end if
      end do
    else
      oi_id = oi_end
    end if
  end function check_oi

!------------------------------------------------------------------------------!

  function reg_od(od,pr) result(oi_id)
    ! register a order configuration (od) and assign to it an order id (oi).
    integer, intent(in), dimension(:) :: od
    integer, intent(in) :: pr
    integer, dimension(:,:,:), allocatable :: oi_reg_copy
    logical, dimension(:,:,:), allocatable :: compoi_copy
    integer, parameter :: od_not_found = -1
    integer :: od_found, oi_id, i, maxoi

    if (size(od) .ne. n_orders) then
      write (*, *) 'ERROR in reg_oi: Incoming od has not the correct size'
      stop
    end if

    od_found = check_oi(od,pr)
    if (od_found .ne. od_not_found) then
      oi_id = od_found

    else
      ! assure oi_reg is allocated and has the required size
      maxoi = maxval(oiSize)
      if (maxoi .eq. 0) then
        allocate(oi_reg(n_orders,oiDef,prTot))
        oi_reg = 0
        allocate(compoi(oiDef,0:1,prTot))
        compoi = .false.
      end if

      oiSize(pr) = oiSize(pr) + 1
      maxoi = maxval(oiSize)
      if (maxoi .gt. oiDef) then
        allocate(oi_reg_copy(n_orders,oiDef,prTot))
        oi_reg_copy(:,:,:) = oi_reg(:,:,:)
        deallocate(oi_reg)

        oiDef = oiDef + oiInc
        allocate(oi_reg(n_orders,oiDef,prTot))
        oi_reg(:,:oiDef-oiInc,:) = oi_reg_copy(:,:oiDef-oiInc,:)
        oi_reg(:,oiDef-oiInc+1:,:) = -1
        deallocate(oi_reg_copy)

        allocate(compoi_copy(oiDef,0:1,prTot))
        compoi_copy(:oiDef-oiInc,:,:) = compoi(:,:,:)
        deallocate(compoi)
        allocate(compoi(oiDef,0:1,prTot))
        compoi(:oiDef-oiInc,:,:) = compoi_copy(:oiDef-oiInc,:,:)
        compoi(oiDef-oiInc+1:,:,:) = .false.

      end if
      ! register
      oi_reg(:,oiSize(pr),pr) = od(:)
      oi_id = oiSize(pr)

      if (.not. allocated(max_od)) then
        allocate(max_od(n_orders))
        max_od = -1
      endif
      do i = 1, n_orders
        if (max_od(i) .lt. od(i)) max_od(i) = od(i)
      end do

    end if

  end function reg_od

!------------------------------------------------------------------------------!

  function check_oi2(od2,pr) result(oi2_id)
    ! returns whether od2 is registered(i.e. if od2 is found in oi2_reg)
    ! return -1 if not found otherwise return the oi2_id corresponding to oi
    integer, intent(in), dimension(:) :: od2
    integer, intent(in) :: pr
    integer, parameter :: oi2_end = -1
    integer :: i, oi2_id

    if (oi2Size(pr) .ne. 0) then
      oi2_id = oi2_end
      i = 1
      do while (oi2_reg(1,i,pr) .ne. oi2_end)
        if (sum(abs(oi2_reg(:,i,pr)-od2(:))) .eq. 0) then
          oi2_id = i
          exit
        end if
        i = i + 1
        if (i .gt. oi2Size(pr)) then
          exit
        end if
      end do
    else
      oi2_id = oi2_end
    end if
  end function check_oi2

!------------------------------------------------------------------------------!

 function reg_od2(od2,pr) result(oi2_id)
    ! register a order configuration (od) and assign to it an order id (oi).
    integer, intent(in), dimension(:) :: od2
    integer, intent(in) :: pr
    integer, parameter :: od2_not_found = -1
    integer :: od2_found,oi2_id,maxoi2,oipmax

    if (size(od2) .ne. n_orders) then
      write (*, *) 'ERROR in reg_oi: Incoming od has not the correct size'
      stop
    end if

    od2_found = check_oi2(od2,pr)
    if (od2_found .ne. od2_not_found) then
      oi2_id = od2_found
    else
      ! assure oi2_reg is allocated
      maxoi2 = maxval(oi2Size)
      if (maxoi2 .eq. 0) then
        oipmax = product(max_od+1) + 2
        allocate(oi2_reg(n_orders,oipmax,prTot))
        oi2_reg = 0
      end if
      oi2Size(pr) = oi2Size(pr) + 1

      ! register
      oi2_reg(:,oi2Size(pr),pr) = od2(:)
      oi2_id = oi2Size(pr)
    end if

  end function reg_od2

!------------------------------------------------------------------------------!

end module order_rcl

!------------------------------------------------------------------------------!
