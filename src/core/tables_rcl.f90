!******************************************************************************!
!                                                                              !
!    tables_rcl.f90                                                            !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module tables_rcl

  use globals_rcl
  use modelfile, only: gg_mdl=>gg,incRI_mdl=>incRI,                  &
                       get_parameter_mdl,get_renoscheme_mdl,         &
                       get_mu_ms_mdl,get_mu_uv_mdl,get_delta_uv_mdl, &
                       get_particle_id_mdl,get_particle_cmass2_mdl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  ! binary_tables
  integer, allocatable :: levelLeg(:),vectorLeg(:,:),firstNumber(:), &
                          firstGap(:),firstNumbers(:,:),firstGaps(:,:)

  ! tensor_tables
  integer              :: riTot,gg(0:3,0:3)
  integer, allocatable :: RtoS(:),riMin(:),riMax(:),ri(:,:),RItoR(:),RItoI(:),&
                          incRI(:,:)

 ! first occurence of a rank increased ri given ri and lorentz index
  logical, allocatable :: firstRI(:,:),firstRI_r21(:,:),firstRI_r22(:,:,:)

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  function pim(i,perm)
    integer, intent(in) :: i,perm
    integer :: pim
    select case (perm)
    ! identity
    case (0)
      pim = i

    ! [0, 2, 1, 3, ...]
    case (1)
      select case (i)
        case (2)
          pim = 3
        case (3)
          pim = 2
        case default
          pim = i
      end select

    ! [0, 1, 3, 2, 4]:
    case (2)
      select case (i)
        case (3)
          pim = 4
        case (4)
          pim = 3
        case default
          pim = i
      end select

    ! [0, 3, 2, 1, 4]:
    case (3)
      select case (i)
        case (2)
          pim = 4
        case (3)
          pim = 3
        case (4)
          pim = 2
        case default
          pim = i
      end select

    ! [0, 3, 1, 2, 4]
    case (4)
      select case (i)
        case (2)
          pim = 3
        case (3)
          pim = 4
        case (4)
          pim = 2
        case default
          pim = i
      end select

    ! [0, 2, 3, 1, 4]
    !?
    case (5)
      select case (i)
        case (2)
          pim = 4
        case (3)
          pim = 2
        case (4)
          pim = 3
        case default
          pim = i
      end select

    ! [0, 1, 2, 4, 3, 5]
    case (6)
      select case (i)
        case (4)
          pim = 5
        case (5)
          pim = 4
        case default
          pim = i
      end select

    ! [0, 1, 3, 4, 2, 5]
    case (7)
      select case (i)
        case (3)
          pim = 5
        case (4)
          pim = 3
        case (5)
          pim = 4
        case default
          pim = i
      end select


    ! [0, 2, 3, 4, 1, 5]
    case (8)
      select case (i)
        case (2)
          pim = 5
        case (3)
          pim = 2
        case (4)
          pim = 3
        case (5)
          pim = 4
        case default
          pim = i
      end select

    ! [0, 3, 1, 4, 2, 5]
    case (9)
      select case (i)
        case (2)
          pim = 3
        case (3)
          pim = 5
        case (4)
          pim = 2
        case (5)
          pim = 4
        case default
          pim = i
      end select

    ! [0, 1, 4, 2, 3, 5]
    case (10)
      select case (i)
        case (3)
          pim = 4
        case (4)
          pim = 5
        case (5)
          pim = 3
        case default
          pim = i
      end select

    ! [0, 3, 4, 1, 2, 5]
    case (11)
      select case (i)
        case (2)
          pim = 4
        case (3)
          pim = 5
        case (4)
          pim = 2
        case (5)
          pim = 3
        case default
          pim = i
      end select

    ! [0, 4, 1, 2, 3, 5]
    case (12)
      select case (i)
        case (2)
          pim = 3
        case (3)
          pim = 4
        case (4)
          pim = 5
        case (5)
          pim = 2
        case default
          pim = i
      end select

    case default
      write(*,*) "perm:", perm
      call error_rcl('Permutation not implemented.')
    end select

  end function pim

  function pmi(arr,perm,n)
    integer, intent(in) :: n
    integer, intent(in) :: arr(1:n),perm
    integer, dimension(1:n) :: pmi
    integer :: i

    pmi(:) = arr(:)
    do i = 2, n-1
      pmi(i) = arr(pim(i,perm))
    end do

  end function pmi

!------------------------------------------------------------------------------!

  function dzgs (als,Nlq,Duv,Qren,muUV)
    integer,  intent(in) :: Nlq
    real(dp), intent(in) :: als,muUV,Duv,Qren
    complex(dp)          :: dZgs
    integer              :: i
    integer, parameter   :: Nq=6

    dzgs = als/(4*pi)*(Nlq/3d0-11/2d0)* &
                       (Duv - log(Qren**2/muUV**2))
    do i = Nlq+1,Nq
      dZgs = dZgs + als/(12*pi)*(Duv - log(mq2(i)/muUV**2))
    end do
    dZgs = real(dZgs,kind=dp)*cone

  end function dzgs

!------------------------------------------------------------------------------!

  subroutine qcd_rescaling_tables
    ! QCD coupling
    integer            :: i,Nq=6
    character(len=100) :: dZgs_renoscheme

    als0   = real(get_parameter_mdl('aS'),kind=dp)
    als    = als0
    Qren0  = get_mu_ms_mdl()
    Qren   = Qren0
    Nfren0 = -1

    dZgs_renoscheme = get_renoscheme_mdl('dZgs_QCD2')
    Nlq = 0
    if (adjustl(trim(dZgs_renoscheme)) .eq. 'Nf6') then
      Nlq = 6
    else if (adjustl(trim(dZgs_renoscheme)) .eq. 'Nf5') then
      Nlq = 5
    else if (adjustl(trim(dZgs_renoscheme)) .eq. 'Nf4') then
      Nlq = 4
    else if (adjustl(trim(dZgs_renoscheme)) .eq. 'Nf3') then
      Nlq = 3
    else
      call warning_rcl('Renoscheme: `' // adjustl(trim(dZgs_renoscheme)) // &
                     '` not supported for rescaling QCD amplitude.', &
                     where='qcd_rescaling_tables')
      call warning_rcl('QCD rescaling disabled.', &
                       where='qcd_rescaling_tables')
      qcd_rescaling = .false.
      return
    end if
    if (Nlq .lt. 3 .or. Nlq .gt. 6) then
      call error_rcl('No suited number of active flavours provided')
      stop
    end if
    Nlq0 = Nlq

    ! Use model quark masses if not overwritten by user
    if (.not. use_active_qmasses) then
      do i = 3, 6
        ! top mass
        if (i .eq. 6) then
          mq2(i) = get_particle_cmass2_mdl(get_particle_id_mdl('t'))
        ! bottom mass
        else if (i .eq. 5) then
          mq2(i) = get_particle_cmass2_mdl(get_particle_id_mdl('b'))
        ! charm mass
        else if (i .eq. 4) then
          mq2(i) = get_particle_cmass2_mdl(get_particle_id_mdl('c'))
        else if (i .eq. 3) then
          mq2(i) = get_particle_cmass2_mdl(get_particle_id_mdl('s'))
        else
            call error_rcl('Wrong value of inactive flavors.')
        end if
      enddo
    end if
    do i = Nlq+1,Nq
      ! top mass
      if (i .eq. 6) then
        if (abs(mq2(i)) .lt. zerocheck) then
          call error_rcl('Encountered small mass corresponding to inactive t quark ')
        endif
      ! bottom mass
      else if (i .eq. 5) then
        if (abs(mq2(i)) .lt. zerocheck) then
          call error_rcl('Encountered small mass corresponding to inactive b quark ')
        endif
      ! charm mass
      else if (i .eq. 4) then
        if (abs(mq2(i)) .lt. zerocheck) then
          call error_rcl('Encountered small mass corresponding to inactive c quark ')
        endif
      else if (i .eq. 3) then
        if (abs(mq2(i)) .lt. zerocheck) then
          call error_rcl('Encountered small mass corresponding to inactive s quark ')
        endif
      else
        call error_rcl('Wrong value of inactive flavors.')
      end if
    enddo

    dZgs0 = dzgs(als,Nlq,get_delta_uv_mdl(),Qren,get_mu_uv_mdl())

    allocate(als0R(0:1,prTot));  als0R  = als0
    allocate(dZgs0R(prTot));     dZgs0R = dZgs0
    allocate(Qren0R(0:1,prTot)); Qren0R = Qren0
    allocate(Nlq0R(0:1,prTot));  Nlq0R  = Nlq0

    ! QCD beta function at 1- and 2-loop level
    do i = 1,6
      beta0(i) = 1/4d0 * ( 11d0 - 2/3d0*i )
      beta1(i) = 1/16d0 * ( 102d0 - 38/3d0*i )
      rb1(i)   = beta1(i)/beta0(i)
    enddo

  end subroutine qcd_rescaling_tables

!------------------------------------------------------------------------------!

  subroutine binary_tables

  ! This subroutine creates many tables of the binary arithmetic.
  ! Given the number of external legs "lmax", the one-loop processes
  ! generates binary numbers which are the sum of some of the primary
  ! binaries 1,2,4,...,2^(lmax+1).
  ! Given such a binary "e" the subroutine computes the following
  ! tables.
  ! - "levelLeg(e)":
  !   It says how many primary binaries are contained in "e".
  ! - "vectorLeg(e,:)":
  !   It is a vector with "lmax+2" components. Each entry "i" of the
  !   vector is 1 or 0, depending on whether the primary binary
  !   "2^(i-1)" is contained in "e" or not.
  ! - firstNumber(e):
  !   It is the lowest primary binary contained in "e".
  !   Ex.: ! if e = 8 + 16 + 32 + 128, then firstNumber(e) = 8.
  ! - firstGap(e):
  !   It is the lowest missing primary binary contained in "e".
  !   Ex.: if e = 1 + 2 + 8 + 16 + 32 + 128, then firstGap(e)= 4.
  ! - firstNumbers(e,:lmax):
  !   It is a vector whose entries are the ordered (from the lowest
  !   to the biggest) primary binaries contained in "e". If the
  !   primary binaries are less than "lmax" the corresponding entries
  !   of the vector are 0.
  !   Ex: if e = 1 + 2 + 8 + 16 + 32 + 128 and lmax = 10, then
  !   firstNumbers(e,1)= 1, firstNumbers(e,2)= 2,
  !   firstNumbers(e,3)= 8, firstNumbers(e,4)= 16,
  !   firstNumbers(e,5)= 32, firstNumbers(e,6)= 128,
  !   firstNumbers(e,7:10)= 0.
  ! - firstGaps(e,:lmax):
  !   It is a vector whose entries are the ordered (from the lowest
  !   to the biggest) missing primary binaries contained in "e".
  !   Ex: if e = 1 + 2 + 8 + 16 + 64 and lmax = 8, then
  !   firstGaps(e,1)= 4, firstGaps(e,2)= 32, firstGaps(e,3)= 128,
  !   firstGaps(e,4)= 256, firstGaps(e,5)= 512, firstGaps(e,6)= 1024,
  !   firstGaps(e,7)= 2048.

  integer             :: lmax,e,ee,max,n,g,i,count
  integer,allocatable :: l(:),ns(:),gs(:)

  lmax = legsMax + 2

  max = 2**lmax - 1

  allocate (     levelLeg(0:max))
  allocate (    vectorLeg(0:max,lmax))
  allocate (firstNumber  (0:max))
  allocate (firstGap     (0:max))
  allocate (firstGaps    (0:max,lmax))
  allocate (firstNumbers (0:max,lmax))

  allocate ( l(lmax))
  allocate (ns(lmax))
  allocate (gs(lmax))

  do e = 0,max

    ee = e
    do i = lmax,1,-1
      l(i) = ee/2**(i-1)
      ee  = ee - l(i)*2**(i-1)
    enddo
    levelLeg (e)   = sum(l(:))
    vectorLeg(e,:) =     l(:)

    ee = e
    if (ee.eq.0) then
      firstNumber(e) = 0
    else
      n  = 1
      do while ( mod(ee,2) .eq. 0 ) ! as long as ee is even
        n  = 2*n
        ee = ee/2
      enddo
      firstNumber(e) = n
    endif

    ee = e
    g  = 1
    do while ( mod(ee,2) .eq. 1 ) ! as long as ee is odd
      g  = 2*g
      ee = (ee-1)/2
    enddo
    firstGap(e) = g

    ee    = e
    i     = 1
    count = 1
    do while ( i < lmax+1 )
      if (ee.eq.0) then
        ns(i) = 0
        i    = i + 1
      else
        if (mod(ee,2).eq.0) then ! ee is even
          ee   = ee/2
        else                     ! ee is odd
          ns(i) = count
          ee   = (ee-1)/2
          i    = i + 1
        endif
      endif
      count  = 2*count
    enddo
    firstNumbers(e,:) = ns

    ee    = e
    i     = 1
    count = 1
    do while ( i < lmax+1 )
      if (mod(ee,2).eq.0) then ! ee is even
        gs(i) = count
        ee    = ee/2
        i     = i + 1
      else                     ! ee is odd
        ee    = (ee-1)/2
      endif
      count  = 2*count
    enddo
    firstGaps(e,:) = gs

  enddo

  deallocate ( l)
  deallocate (gs)
  deallocate (ns)

  end subroutine binary_tables

!------------------------------------------------------------------------------!

  subroutine tensor_tables

  ! This routine creates tables for the integer arithmetic of tensors
  ! of rank "r" from 0 to "Rmax = legsMax".
  ! - gg(0:3,0:3):
  !   It is the metric tensor.
  ! - RtoS(r):
  !   It is the size of the symmetrized tensor index at rank "r":
  !     RtoS(0)= 1, RtoS(1)= 4, RtoS(2)= 10, ...
  ! - ri(r,i), riMin(r), riMax(r), RItoR(ri), RItoI(ri):
  !   The symmetrized index "i" of a tensor of rank "r" is combined
  !   with the rank "r" to produce a single index "ri". Given the
  !   rank "r" and the index "i":
  !     ri(r,i) = the value of "ri"
  !     riMin(r) = the minimal value of "ri" for that "r"
  !     riMax(r) = the maximal value of "ri" for that "r"
  !   Given the combined index "ri":
  !     RItoR(ri) = rank "r" for that "ri"
  !     RItoI(ri) = index "i" for that "ri"
  ! - incRI(0:3,0:riMax(Rmax-1)):
  !   The combined symmetrized index "ri" of a tensor is combined
  !   with a lorentz index 0:3 to create a new combined symmetrized
  !   index.

  integer              :: Rmax,repeat,cntRI,cntI,r,n0,n1,n2,n3, &
                          riIn,nIn(0:3),nOut(0:3),mu,nu,i
  integer, allocatable :: RItoN0(:),RItoN1(:),RItoN2(:),RItoN3(:), &
                          N0123toRI(:,:,:,:)
  logical, allocatable :: computed(:)

  ! metric tensor
  gg      =   0
  gg(0,0) = + 1
  gg(1,1) = - 1
  gg(2,2) = - 1
  gg(3,3) = - 1
  gg_mdl(:,:) = gg(:,:)

  Rmax = legsMax+4

  allocate (RtoS      (0:Rmax))
  allocate (riMin     (0:Rmax))
  allocate (riMax     (0:Rmax))

  ! The combined symmetrized index "ri" of a tensor is transformed
  ! in the 4-index notation (n0,n1,n2,n3), where n0 is the number
  ! of lorentz indices set to 0, n1 is the number of lorentz
  ! indices set to 1, etc.
  ! Given the 4 indices "n0,n1,n2,n3":
  !   N0123toRI(n0,n1,n2,n3) = the value of the index "ri"
  ! Given the combined index "ri":
  !   RItoN0(ri) = the value of the index n0
  !   RItoN1(ri) = the value of the index n1
  !   RItoN2(ri) = the value of the index n2
  !   RItoN3(ri) = the value of the index n3

  allocate (N0123toRI (0:Rmax,0:Rmax,0:Rmax,0:Rmax))

  do repeat = 1,2

    cntRI = 0

    do r = 0,Rmax

      cntI = 0

      riMin(r) = cntRI

      do n0 = r,0,-1
        do n1 = r-n0,0,-1
          do n2 = r-n0-n1,0,-1
            n3 = r-n0-n1-n2

            cntI  =  cntI + 1

            if (repeat.eq.2) then
              N0123toRI(n0,n1,n2,n3) = cntRI
              RItoN0(cntRI) = n0
              RItoN1(cntRI) = n1
              RItoN2(cntRI) = n2
              RItoN3(cntRI) = n3
              ri(cntI,r) = cntRI
              RItoR(cntRI) = r
              RItoI(cntRI) = cntI
            endif

            cntRI = cntRI + 1

          enddo
        enddo
      enddo

      RtoS(r) = cntI

      riMax(r) = cntRI - 1

    enddo

    if (repeat.eq.1) then
      riTot = cntRI
      allocate (ri    (RtoS(Rmax),0:Rmax))
      allocate (RItoR (0:riTot))
      allocate (RItoI (0:riTot))
      allocate (RItoN0 (0:riTot))
      allocate (RItoN1 (0:riTot))
      allocate (RItoN2 (0:riTot))
      allocate (RItoN3 (0:riTot))
    endif

  enddo

  allocate (incRI (0:3,0:riMax(Rmax-1)))
  allocate (incRI_mdl (0:3,0:riMax(Rmax-1)))
  allocate (firstRI (0:3,0:riMax(Rmax-1)))
  allocate (firstRI_r21 (0:3,0:riMax(Rmax-1)))
  allocate (firstRI_r22 (0:3,0:3,0:riMax(Rmax-1)))
  allocate (computed(riTot))

  firstRI = .false.
  computed = .false.
  do riIn = riMax(Rmax-1),0,-1
    nIn(0) = RItoN0(riIn)
    nIn(1) = RItoN1(riIn)
    nIn(2) = RItoN2(riIn)
    nIn(3) = RItoN3(riIn)
    do mu = 0,3
      nOut = nIn
      nOut(mu) = nIn(mu) + 1
      i = N0123toRI(nOut(0),nOut(1),nOut(2),nOut(3))
      incRI(mu,riIn) = i
      incRI_mdl(mu,riIn) = i
      if(.not.computed(i)) firstRI(mu,riIn) = .true.
      computed(i) = .true.
    enddo
  enddo

  ! TODO:  <25-08-18, J.-N. Lang> !
  ! Do not use this for the moment.
  computed = .false.
  firstRI_r21 = .false.
  firstRI_r22 = .false.
  do riIn = riMax(Rmax-2),0,-1
    do mu = 0,3
      if(.not.computed(i)) firstRI_r21(mu,riIn) = .true.
      i = incRI(mu,riIn)
      computed(i) = .true.
      i = incRI(mu,incRI(mu,riIn))
      computed(i) = .true.
      if(.not.computed(i)) firstRI_r22(mu,mu,riIn) = .true.
      do nu = 0, mu-1
        i = incRI(nu,incRI(mu,riIn))
        if(.not.computed(i)) firstRI_r22(nu,mu,riIn) = .true.
        computed(i) = .true.
      end do
    enddo
  enddo

  deallocate (computed)

  deallocate (RItoN3)
  deallocate (RItoN2)
  deallocate (RItoN1)
  deallocate (RItoN0)
  deallocate (N0123toRI)

  end subroutine tensor_tables

!------------------------------------------------------------------------------!

end module tables_rcl

!------------------------------------------------------------------------------!
