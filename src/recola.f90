!******************************************************************************!
!                                                                              !
!    recola.f90                                                                !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

  module recola

!------------------------------------------------------------------------------!

  use globals_rcl, only: get_recola_version_rcl, &
                         get_modelname_rcl,      &
                         get_modelgauge_rcl

!------------------------------------------------------------------------------!

  use input_rcl, only:                       &
      set_qcd_rescaling_rcl,                 &
      set_fermionloop_optimization_rcl,      &
      set_helicity_optimization_rcl,         &
      set_colour_optimization_rcl,           &
      set_compute_A12_rcl,                   &
      set_masscut_rcl,                       &
      set_longitudinal_polarization_rcl,     &
      set_light_particle_rcl,                &
      unset_light_particle_rcl,              &
      use_dim_reg_soft_rcl,                  &
      use_mass_reg_soft_rcl,                 &
      use_recola_base_rcl,                   &
      set_mass_reg_soft_rcl,                 &
      set_delta_uv_rcl,                      &
      get_delta_uv_rcl,                      &
      set_delta_ir_rcl,                      &
      get_delta_ir_rcl,                      &
      set_mu_uv_rcl,                         &
      get_mu_uv_rcl,                         &
      set_mu_ir_rcl,                         &
      get_mu_ir_rcl,                         &
      set_mu_ms_rcl,                         &
      get_mu_ms_rcl,                         &
      get_renormalization_scale_rcl,         &
      get_flavour_scheme_rcl,                &
      set_complex_mass_scheme_rcl,           &
      set_on_shell_scheme_rcl,               &
      set_momenta_correction_rcl,            &
      set_print_level_amplitude_rcl,         &
      set_print_level_squared_amplitude_rcl, &
      set_print_level_correlations_rcl,      &
      set_print_level_parameters_rcl,        &
      set_draw_level_branches_rcl,           &
      set_print_level_ram_rcl,               &
      scale_coupling3_rcl,                   &
      scale_coupling4_rcl,                   &
      switchoff_coupling3_rcl,               &
      switchoff_coupling4_rcl,               &
      switchon_resonant_selfenergies_rcl,    &
      switchoff_resonant_selfenergies_rcl,   &
      set_dynamic_settings_rcl,              &
      set_ifail_rcl,                         &
      get_ifail_rcl,                         &
      set_iwarn_rcl,                         &
      get_iwarn_rcl,                         &
      set_collier_mode_rcl,                  &
      set_collier_output_dir_rcl,            &
      set_cache_mode_rcl,                    &
      set_global_cache_rcl,                  &
      switch_global_cache_rcl,               &
      set_compute_ir_poles_rcl,              &
      set_output_file_rcl,                   &
      set_log_mem_rcl,                       &
      set_crossing_symmetry_rcl,             &
      set_init_collier_rcl,                  &
      set_parameter_rcl,                     &
      get_parameter_rcl,                     &
      set_renoscheme_rcl,                    &
      get_renoscheme_rcl,                    &
      set_resonant_particle_rcl,             &
      set_quarkline_rcl,                     &
      reset_vertices_rcl,                    &
      reset_couplings_rcl,                   &
      reset_ctcouplings_rcl,                 &
      has_feature_rcl,                       &
      print_collier_statistics_rcl,          &
      set_compute_selfenergy_rcl,            &
      set_compute_selfenergy_offshell_rcl,   &
      set_compute_tadpole_rcl,               &
      set_lp_rcl,                            &
      set_form_rcl,                          &
      set_all_couplings_active_rcl,          &
      set_all_tree_couplings_active_rcl,     &
      set_all_loop_couplings_active_rcl,     &
      set_all_ct_couplings_active_rcl,       &
      set_all_r2_couplings_active_rcl,       &
      set_complex_mass_form_rcl,             &
      set_vertex_functions_rcl,              &
      set_particle_ordering_rcl,             &
      set_masscut_form_rcl,                  &
      set_cutparticle_rcl,                   &
      unset_cutparticle_rcl

!------------------------------------------------------------------------------!

  use process_definition_rcl, only :    &
      define_process_rcl,               &
      select_power_BornAmpl_rcl,        &
      unselect_power_BornAmpl_rcl,      &
      select_all_powers_BornAmpl_rcl,   &
      unselect_all_powers_BornAmpl_rcl, &
      select_power_LoopAmpl_rcl,        &
      unselect_power_LoopAmpl_rcl,      &
      select_all_powers_LoopAmpl_rcl,   &
      unselect_all_powers_LoopAmpl_rcl, &
      split_collier_cache_rcl

!------------------------------------------------------------------------------!

  use process_generation_rcl, only : &
      generate_processes_rcl,        &
      process_exists_rcl

!------------------------------------------------------------------------------!

  use process_computation_rcl, only :              &
      set_resonant_squared_momentum_rcl,           &
      compute_process_rcl,                         &
      rescale_process_rcl,                         &
      get_amplitude_general_rcl,                   &
      get_squared_amplitude_general_rcl,           &
      get_polarized_squared_amplitude_general_rcl, &
      compute_colour_correlation_rcl,              &
      compute_colour_correlation_int_rcl,          &
      compute_all_colour_correlations_rcl,         &
      compute_all_colour_correlations_int_rcl,     &
      rescale_colour_correlation_rcl,              &
      rescale_colour_correlation_int_rcl,          &
      rescale_all_colour_correlations_rcl,         &
      get_colour_correlation_general_rcl,          &
      get_colour_correlation_int_general_rcl,      &
      compute_spin_colour_correlation_rcl,         &
      rescale_spin_colour_correlation_rcl,         &
      get_spin_colour_correlation_general_rcl,     &
      compute_spin_correlation_rcl,                &
      compute_spin_correlation_matrix_rcl,         &
      rescale_spin_correlation_rcl,                &
      get_spin_correlation_general_rcl,            &
      get_spin_correlation_matrix_general_rcl,     &
      rescale_spin_correlation_matrix_rcl,         &
      get_momenta_rcl,                             &
      set_TIs_required_accuracy_rcl,               &
      get_TIs_required_accuracy_rcl,               &
      set_TIs_critical_accuracy_rcl,               &
      get_TIs_critical_accuracy_rcl,               &
      get_TIs_accuracy_flag_rcl,                   &
      get_n_colour_configurations_rcl,             &
      get_colour_configuration_rcl,                &
      get_colour_configurations_rcl,               &
      get_n_helicity_configurations_rcl,           &
      get_helicity_configuration_rcl,              &
      get_helicity_configurations_rcl

!------------------------------------------------------------------------------!

  use random_psp_rcl, only: &
      set_outgoing_momenta_rcl

!------------------------------------------------------------------------------!

  use recola1_interface_rcl, only:          &
    set_pole_mass_w_rcl,                    &
    set_onshell_mass_w_rcl,                 &
    set_pole_mass_z_rcl,                    &
    set_onshell_mass_z_rcl,                 &
    set_pole_mass_h_rcl,                    &
    set_pole_mass_top_rcl,                  &
    set_pole_mass_bottom_rcl,               &
    set_pole_mass_charm_rcl,                &
    set_pole_mass_strange_rcl,              &
    set_pole_mass_up_rcl,                   &
    set_pole_mass_down_rcl,                 &
    set_pole_mass_tau_rcl,                  &
    set_pole_mass_muon_rcl,                 &
    set_pole_mass_electron_rcl,             &
    set_light_top_rcl,                      &
    unset_light_top_rcl,                    &
    set_light_bottom_rcl,                   &
    unset_light_bottom_rcl,                 &
    set_light_charm_rcl,                    &
    unset_light_charm_rcl,                  &
    set_light_strange_rcl,                  &
    unset_light_strange_rcl,                &
    set_light_up_rcl,                       &
    unset_light_up_rcl,                     &
    set_light_down_rcl,                     &
    unset_light_down_rcl,                   &
    set_light_tau_rcl,                      &
    unset_light_tau_rcl,                    &
    set_light_muon_rcl,                     &
    unset_light_muon_rcl,                   &
    set_light_electron_rcl,                 &
    unset_light_electron_rcl,               &
    set_light_fermion_to_zero_rcl,          &
    set_light_fermions_rcl,                 &
    use_gfermi_scheme_rcl,                  &
    use_alpha0_scheme_rcl,                  &
    use_alphaZ_scheme_rcl,                  &
    get_alpha_rcl,                          &
    set_alphas_rcl,                         &
    get_alphas_rcl,                         &
    set_alphas_masses_rcl,                  &
    compute_running_alphas_rcl,             &
    select_all_gs_powers_BornAmpl_rcl,      &
    unselect_all_gs_powers_BornAmpl_rcl,    &
    select_gs_power_BornAmpl_rcl,           &
    unselect_gs_power_BornAmpl_rcl,         &
    select_all_gs_powers_LoopAmpl_rcl,      &
    unselect_all_gs_powers_LoopAmpl_rcl,    &
    select_gs_power_LoopAmpl_rcl,           &
    unselect_gs_power_LoopAmpl_rcl,         &
    set_gs_power_rcl,                       &
    get_amplitude_r1_rcl,                   &
    get_squared_amplitude_r1_rcl,           &
    get_polarized_squared_amplitude_r1_rcl, &
    get_colour_correlation_r1_rcl,          &
    get_colour_correlation_int_r1_rcl,      &
    get_spin_colour_correlation_r1_rcl,     &
    get_spin_correlation_r1_rcl,            &
    get_spin_correlation_matrix_r1_rcl

!------------------------------------------------------------------------------!

  use extended_higgs_interface_rcl, only: &
    set_Z2_thdm_yukawa_type_rcl,          &
    set_pole_mass_hl_rcl,                 &
    set_pole_mass_hh_rcl,                 &
    set_pole_mass_hl_hh_rcl,              &
    set_pole_mass_ha_rcl,                 &
    set_pole_mass_hc_rcl,                 &
    set_tb_cab_rcl,                       &
    use_mixing_alpha_msbar_scheme_rcl,    &
    use_mixing_alpha_onshell_scheme_rcl,  &
    use_mixing_beta_msbar_scheme_rcl,     &
    use_mixing_beta_onshell_scheme_rcl,   &
    set_l5_rcl,                           &
    set_msb_rcl,                          &
    use_msb_msbar_scheme_rcl,             &
    set_sa_rcl,                           &
    set_tb_rcl,                           &
    use_tb_msbar_scheme_rcl,              &
    use_tb_onshell_scheme_rcl

!------------------------------------------------------------------------------!

  use statistics_rcl, only :           &
      print_generation_statistics_rcl, &
      print_TI_statistics_rcl,         &
      print_TC_statistics_rcl

!------------------------------------------------------------------------------!

  use reset_rcl, only : &
      reset_recola_rcl

!------------------------------------------------------------------------------!

  implicit none

  interface get_amplitude_rcl
    module procedure get_amplitude_general_rcl, &
                     get_amplitude_r1_rcl
  end interface

  interface get_squared_amplitude_rcl
    module procedure get_squared_amplitude_general_rcl, &
                     get_squared_amplitude_r1_rcl
  end interface

  interface get_polarized_squared_amplitude_rcl
    module procedure get_polarized_squared_amplitude_general_rcl, &
                     get_polarized_squared_amplitude_r1_rcl
  end interface

  interface get_colour_correlation_rcl
    module procedure get_colour_correlation_general_rcl, &
                     get_colour_correlation_r1_rcl
  end interface

  interface get_colour_correlation_int_rcl
    module procedure get_colour_correlation_int_general_rcl, &
                     get_colour_correlation_int_r1_rcl
  end interface

  interface get_spin_colour_correlation_rcl
    module procedure get_spin_colour_correlation_general_rcl, &
                     get_spin_colour_correlation_r1_rcl
  end interface

  interface get_spin_correlation_rcl
    module procedure get_spin_correlation_general_rcl, &
                     get_spin_correlation_r1_rcl
  end interface

  interface get_spin_correlation_matrix_rcl
    module procedure get_spin_correlation_matrix_general_rcl, &
                     get_spin_correlation_matrix_r1_rcl
  end interface

!------------------------------------------------------------------------------!

  end module recola

!------------------------------------------------------------------------------!
