!******************************************************************************!
!                                                                              !
!    process_computation_rcl.f90                                               !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module process_computation_rcl

!------------------------------------------------------------------------------!

  use globals_rcl
  use amplitude_rcl, only: set_momenta,                         &
                           compute_amplitude,                   &
                           compute_squared_amplitude,           &
                           compute_squared_amplitude_cc,        &
                           compute_squared_amplitude_cc_int,    &
                           compute_squared_amplitude_cc_nlo,    &
                           compute_squared_amplitude_scc,       &
                           compute_squared_amplitude_scc_nlo,   &
                           compute_squared_amplitude_sc,        &
                           compute_squared_amplitude_sc_nlo,    &
                           compute_squared_amplitude_scm,       &
                           print_process_and_momenta,           &
                           print_amplitude,                     &
                           print_squared_amplitude,             &
                           rescale_amplitude,                   &
                           print_rescaling,                     &
                           print_parameters_change,             &
                           print_squared_amplitude_cc,          &
                           print_squared_amplitude_cc_int,      &
                           print_squared_amplitude_scc,         &
                           print_squared_amplitude_sc,          &
                           print_squared_amplitude_scm,         &
                           matrix,matrix2,matrix2h,             &
                           matrix2cc,matrix2ccint,matrix2ccnlo, &
                           matrix2scc,matrix2sccnlo,matrix2sc,  &
                           matrix2scnlo,matrix2scm,momenta,momcheck
  use modelfile, only: is_resonant_particle_id_mdl,get_particle_type2_mdl
  use order_rcl, only: oi2Size,get_oi,get_oi2

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine set_resonant_squared_momentum_rcl(npr,res,ps)

    ! This subroutine acts on the resonance number "res" for the process
    ! with process number "npr". It sets the squared momentum of the
    ! denominator of the resonant propagator to "ps".
    ! The resonance-number "res" is defined from the process definition
    ! in the call of define_process_rcl. The first resonant particle
    ! defined there has "res"=1, the second resonant particle has
    ! "res=2", and so on. "ps" is a real number.

    integer,  intent(in)  :: npr,res
    real(dp), intent(in)  :: ps
    integer               :: pr,legs,e0,e1,e2

    call processes_not_generated_error_rcl('set_resonant_squared_momentum_rcl')

    call get_pr(npr, 'set_resonant_squared_momentum_rcl', pr)

    legs = prs(pr)%legs

    if (res .lt. 0 .or. res .gt. legs) then
      call error_rcl('set_resonant_squared_momentum_rcl called ' // &
                     'with wrong resonance number')
    endif

    if (ps .lt. 0d0) then
      call error_rcl('set_resonant_squared_momentum_rcl called ' // &
                     'with wrong squared momentum')
    endif

    if (.not. is_resonant_particle_id_mdl(prs(pr)%parRes(res))) then
      call error_rcl('set_resonant_squared_momentum_rcl ' // &
                     'called for a particle not set as resonant')
    endif

    e0 = prs(pr)%binRes(res)
    e1 = newbin(e0,pr)
    e2 = 2**legs - 1 - e1

    defp2bin(e1,pr) = .true.
       p2bin(e1,pr) = ps

    defp2bin(e2,pr) = .true.
       p2bin(e2,pr) = ps

  end subroutine set_resonant_squared_momentum_rcl

!------------------------------------------------------------------------------!

  subroutine compute_process_rcl(npr,p,order,A2,momenta_check)
    ! Computes the structure-dressed helicity amplitudes and the summed squared
    ! amplitude for the process with process number npr.
    ! The summation takes place over all (selected) coupling powers.
    ! If some particles were defined as polarized, no sum/average is
    ! performed on these particles.

    use modelfile, only: has_feature_mdl
    use input_rcl, only: get_delta_ir_rcl, set_delta_ir_rcl

    integer,            intent(in)  :: npr
    real(dp),           intent(in)  :: p(0:,:)
    character(len=*),   intent(in)  :: order
    real(dp), optional, intent(out) :: A2(0:1)
    logical,  optional, intent(out) :: momenta_check
    integer                         :: pr,legs,crosspr,long_tmp
    real(dp)                        :: d1,d2

    call processes_not_generated_error_rcl('compute_process_rcl')

    call get_pr(npr, 'compute_process_rcl', pr)

    legs = prs(pr)%legs

    if (size(p,2).ne.legs.or.size(p,1).ne.4) then
      call error_rcl('compute_process_rcl called with wrong momenta dimension.')
    endif

    call order_error_rcl(order, 'compute_process_rcl')

    call set_momenta (pr,p)
    if (present(momenta_check)) momenta_check = momcheck

    if (writeMat+writeMat2.ge.1) then
      call print_process_and_momenta (pr)
      call print_rescaling
      call print_parameters_change
    endif

    if (prs(pr)%crosspr .ne. 0) then
      crosspr = prs(pr)%crosspr
    else
      crosspr = pr
    end if

    if (order .eq. 'NLO' .and. dynamic_settings .ge. 1 .and. &
        compute_ir_poles .eq. 1) then

      call get_delta_ir_rcl(d1,d2)

      call set_delta_ir_rcl(d1 + 100d0, d2)
      call compute_amplitude (pr,order)
      if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
        call rescale_amplitude (pr,order)
      end if
      call compute_squared_amplitude(pr,order)

      matrix2(1:oi2Size(crosspr),5,pr) = matrix2(1:oi2Size(crosspr),4,pr)

      call set_delta_ir_rcl(d1, d2 + 100d0)
      call compute_amplitude (pr,order)
      if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
        call rescale_amplitude (pr,order)
      end if
      call compute_squared_amplitude(pr,order)
      matrix2(1:oi2Size(crosspr),6,pr) = matrix2(1:oi2Size(crosspr),4,pr)

      call set_delta_ir_rcl(d1, d2)
      call compute_amplitude (pr,order)
      if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
        call rescale_amplitude (pr,order)
      end if

      if (writeMat.ge.1) call print_amplitude (pr,order)
      call compute_squared_amplitude(pr,order)

      ! IR1 pole
      matrix2(1:oi2Size(crosspr),5,pr) =  &
        (matrix2(1:oi2Size(crosspr),5,pr) - &
         matrix2(1:oi2Size(crosspr),4,pr))/100d0
      ! IR2 pole
      matrix2(1:oi2Size(crosspr),6,pr) =  &
        (matrix2(1:oi2Size(crosspr),6,pr) - &
         matrix2(1:oi2Size(crosspr),4,pr))/100d0

    else

      if (order .eq. 'NLO' .and. longitudinal_nlo .and. longitudinal .ne. 0) then
      ! If longitudinal and longitudinal_nlo is selected, the QED Ward identity
      ! is only applied to the one-loop matrix element and the LO matrix
      ! element is recomputed with the full polarization dependence.
        call compute_amplitude (pr,'NLO')
        long_tmp = longitudinal
        longitudinal = 0
        call compute_amplitude (pr,'LO')
        longitudinal = long_tmp
      else
        call compute_amplitude (pr,order)
      end if

      if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
        call rescale_amplitude (pr,order)
      end if

      if (writeMat.ge.1) call print_amplitude (pr,order)

      call compute_squared_amplitude (pr,order)

    end if

    if (writeMat2.ge.1) call print_squared_amplitude (pr,order)

    if (present(A2)) then
      A2(0) = sum(matrix2(1:oi2Size(crosspr),0,pr))
      A2(1) = sum(matrix2(1:oi2Size(crosspr),4,pr))
    endif

  end subroutine compute_process_rcl

!------------------------------------------------------------------------------!

  subroutine rescale_process_rcl(npr,order,A2)

    use modelfile, only: has_feature_mdl

    ! This subroutine adjusts the results calculated by
    ! compute_process_rcl for a new value of alpha_s, rescaling the
    ! structure-dressed helicity amplitudes and recomputing the summed
    ! squared amplitude for the process with process number "npr".
    ! "order" and "A2" are the same has for compute_process_rcl.

    integer,          intent(in)            :: npr
    character(len=*), intent(in)            :: order
    real(dp),         intent(out), optional :: A2(0:1)

    integer :: pr

    if (.not. (qcd_rescaling .and. has_feature_mdl('qcd_rescaling'))) then
      call error_rcl('Call of rescale_process_rcl not allowed: ' // &
                     'qcd_rescaling not enabled.')
      stop
    end if

    call processes_not_generated_error_rcl('rescale_process_rcl')

    call get_pr(npr, 'rescale_process_rcl', pr)

    if (order .ne. 'LO' .and. order .ne. 'NLO') then
      call error_rcl('rescale_process_rcl called at the wrong loop order: ' // &
                     order // ' Accepted values are order = "LO", "NLO"')
    endif

    if (writeMat+writeMat2.ge.1) call print_rescaling
    if (writeMat+writeMat2.ge.1) call print_parameters_change

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,order)
    end if

    if (writeMat.ge.1) call print_amplitude (pr,order)

    call compute_squared_amplitude (pr,order)

    if (writeMat2.ge.1) call print_squared_amplitude (pr,order)

    if (present(A2)) then
      if (prs(pr)%crosspr .ne. 0) then
        A2(0) = sum(matrix2(1:oi2Size(prs(pr)%crosspr),0,pr))
        A2(1) = sum(matrix2(1:oi2Size(prs(pr)%crosspr),4,pr))
      else
        A2(0) = sum(matrix2(1:oi2Size(pr),0,pr))
        A2(1) = sum(matrix2(1:oi2Size(pr),4,pr))
      end if
    endif

  end subroutine rescale_process_rcl

!------------------------------------------------------------------------------!

  subroutine get_amplitude_general_rcl(npr,pow,order,colour,hel,A)

    ! This subroutine extracts a specific contribution to the amplitude
    ! of the process with process number "npr", according to the values
    ! of "pow", "order", "colour" and "hel".
    ! - "pow" is the power of alpha_s of the contribution.
    ! - "order" is the loop-order of the contribution.
    !   It is a character variable with accepted values:
    !   'LO'     -> tree squared amplitude
    !   'NLO'    -> loop squared amplitude
    !   'NLO-D4' -> loop squared amplitude, bare loop contribution
    !   'NLO-CT' -> loop squared amplitude, counterterms contribution
    !   'NLO-R2' -> loop squared amplitude, rational terms
    !               contribution
    ! - "colour(1:l" describes the colour structure and is a vector of
    !   type integer and length l, where each position in the vector
    !   corresponds to one of the l external particles of process "npr"
    !   (ordered as in the process definition).
    !   For colourless particles, incoming quarks and outgoing
    !   anti-quarks, the corresponding entry in "colour" must be 0.
    !   For all other particles (gluons, outgoing quarks and incoming
    !   anti-quarks), the entries must be, without repetition, the
    !   positions of the gluons, incoming quarks and outgoing
    !   anti-quarks in the process definition.
    ! - "hel" is the helicity of the contribution.
    !   It is a vector whose entries are the helicities of the
    !   particles:
    !   left-handed  fermions/antifermions -> -1
    !   right-handed fermions/antifermions -> +1
    !   logitudinal vector bosons          ->  0
    !   transverse vector bosons           -> -1, +1
    !   scalar particles                   ->  0
    !   Example:
    !   Process 'u g -> W+ d'
    !   "hel" = (/-1,+1,-1,-1/) means
    !   left-handed up-quark
    !   transverse gluon (helicity = +1)
    !   transverse W+ (helicity = -1)
    !   left-handed down-quark

    ! This routine must be called after compute_process_rcl or
    ! rescale_process_rcl for the process with process number "npr".

    integer,               intent(in)  :: npr,colour(1:),hel(1:)
    integer, dimension(:), intent(in)  :: pow
    character(len=*),      intent(in)  :: order
    complex(dp),           intent(out) :: A

    integer               :: prin,pr,i,j,h,o,legs,cs,kf,oi
    integer, allocatable  :: helk(:),cs0(:)
    character(2)          :: t2,t2c

    call processes_not_generated_error_rcl('get_amplitude_general_rcl')

    call get_pr(npr, 'get_amplitude_general_rcl', prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
    else
      pr = prin
    end if

    legs = prs(pr)%legs

    if (size(colour,1) .ne. legs) then
      call error_rcl('get_amplitude_general_rcl called with wrong color vector dimension.')
    endif

    do i = 1,legs
      t2  = get_particle_type2_mdl(prs(prin)%par(i))
      if (colour(i).ne.0) then
        t2c = get_particle_type2_mdl(prs(prin)%par(colour(i)))
      else
        t2c = '0'
      endif
      if (colour(i) .lt. 0 .or. colour(i) .gt. legs .or. &
          ((t2c .ne. 'g' .and. t2c .ne. 'q')  .and. colour(i) .ne. 0) .or. &
          ((t2  .eq. 'g'  .or. t2  .eq. 'q~') .and. colour(i) .eq. 0) .or. &
          ((t2  .ne. 'g' .and. t2  .ne. 'q~') .and. colour(i) .ne. 0)) then

        call error_rcl('get_amplitude_general_rcl called with wrong color vector '// &
                       'components')
      endif
      do j = 1, i-1
        if (colour(i) .eq. colour(j) .and. colour(i) .ne. 0) then
          call error_rcl('get_amplitude_general_rcl called with wrong color vector '// &
                         'components')
        endif
      enddo
    enddo

    allocate (cs0(legs)); cs0 = 0
    do i = 1,legs
      if (prs(prin)%crosspr .ne. 0) then
        if (colour(i) .ne. 0) then
          cs0(newleg(prs(prin)%relperm(i),pr)) = newleg(prs(prin)%relperm(colour(i)),pr)
        end if
      else
        if (colour(i) .ne. 0) cs0(newleg(i,pr)) = newleg(colour(i),pr)
      end if
    end do
    cs = 0
    do i = 1,csTot(pr)
      if (sum(abs(csIq(1:legs,i,pr)-cs0(1:legs))) .eq. 0) then
        cs = i
        exit
      endif
    enddo
    deallocate (cs0)

    if (size(hel) .ne. legs) then
      call error_rcl('get_amplitude_general_rcl called with wrong helicity vector '//&
                     'dimension')
    endif

    do i = 1,legs
      h = hel(i)
      if ((h .lt. -1) .or. (h .gt. +1) ) then
        call error_rcl('get_amplitude_general_rcl called with wrong helicity vector '//&
                       'components')
      endif
    enddo

    allocate (helk(legs))
    if (prs(prin)%crosspr .ne. 0) then
      do i = 1,prs(pr)%legs
        if (i .gt. prs(pr)%legsIn) then
          helk(newleg(prs(prin)%relperm(i),pr)) = -hel(i)
        else
          helk(newleg(prs(prin)%relperm(i),pr)) = hel(i)
        end if
      end do
    else
      do i = 1,prs(pr)%legsIn
        helk(newleg(i,pr)) = hel(i)
      end do
      do i = prs(pr)%legsIn+1,legs
        helk(newleg(i,pr)) = - hel(i)
      end do
    end if

    kf = 0
    do i = 1,cfTot(pr)
      if (sum(abs( he(1:legs,i,pr) - helk(1:legs) )) .eq. 0) then
        kf = i
        exit
      endif
    enddo

    if     (order.eq.'LO'     ) then; o = 0
    elseif (order.eq.'NLO-D4' ) then; o = 1
    elseif (order.eq.'NLO-CT' ) then; o = 2
    elseif (order.eq.'NLO-R2' ) then; o = 3
    elseif (order.eq.'NLO'    ) then; o = 4
    elseif (order.eq.'NLO-IR1') then; o = 5
    elseif (order.eq.'NLO-IR2') then; o = 5
    else
      call error_rcl('get_amplitude_general_rcl called with wrong order. '// &
                     "Accepted values are: order = 'LO','NLO','NLO-D4',"// &
                     "'NLO-CT','NLO-R2','NLO-IR1','NLO-IR2'")
    endif

    call get_oi(pow, pr, 'get_amplitude_general_rcl', oi)

    if (.not. allocated(matrix)) then
      call error_rcl('No amplitude computed yet.', &
                     where='get_amplitude_general_rcl')
    end if

    A = cnul
    if (cs .ne. 0 .and. kf .ne. 0 .and. oi .ne. -1) A = matrix(cs,oi,kf,o,prin)

  end subroutine get_amplitude_general_rcl

!------------------------------------------------------------------------------!

  subroutine get_squared_amplitude_general_rcl(npr,pow,order,A2)

    ! This subroutine extracts the computed value of the summed squared
    ! amplitude with "pow" being order in
    ! the fundamental couplings at loop-order "order" for
    ! the process with process number "npr".
    ! - "pow" is an array whose values represent the power in fundamental
    ! couplings in the order as dictated by the model file.
    ! "order: is a character variable with accepted values:
    ! 'LO'     -> tree squared amplitude
    ! 'NLO'    -> loop squared amplitude
    ! 'NLO-D4' -> loop squared amplitude, bare loop contribution
    ! 'NLO-CT' -> loop squared amplitude, counterterms contribution
    ! 'NLO-R2' -> loop squared amplitude, rational terms contribution

    ! This routine must be called after compute_process_rcl for process
    ! with process number "npr"

    integer,               intent(in)  :: npr
    integer, dimension(:), intent(in)  :: pow
    character(len=*),      intent(in)  :: order
    real(dp),              intent(out) :: A2

    integer :: pr,legs,oi2,o

    call processes_not_generated_error_rcl('get_squared_amplitude_general_rcl')

    call get_pr(npr, 'get_squared_amplitude_general_rcl', pr)

    legs = prs(pr)%legs

    if     (order.eq.'LO'     ) then; o = 0
    elseif (order.eq.'NLO-D4' ) then; o = 1
    elseif (order.eq.'NLO-CT' ) then; o = 2
    elseif (order.eq.'NLO-R2' ) then; o = 3
    elseif (order.eq.'NLO'    ) then; o = 4
    elseif (order.eq.'NLO-IR1') then; o = 5
    elseif (order.eq.'NLO-IR2') then; o = 6
    else
      call error_rcl('get_squared_amplitude_general_rcl called with wrong ' // &
                     "order. Accepted values are: " // &
                     "order = 'LO','NLO','NLO-D4',"// &
                     "'NLO-CT','NLO-R2','NLO-IR1','NLO-IR2'")
    endif
    if (order .eq. 'NLO-IR1' .or. order .eq. 'NLO-IR1') then
      if (dynamic_settings .eq. 0) &
        call warning_rcl('Extracting IR poles requires dynamic_settings=1.', &
                         where='get_squared_amplitude_general_rcl')
      if (compute_ir_poles .eq. 0) &
        call warning_rcl('Extracting IR poles requires compute_ir_poles>0.', &
                         where='get_squared_amplitude_general_rcl')
    end if

    if (prs(pr)%crosspr .ne. 0) then
      call get_oi2(pow, prs(pr)%crosspr, 'get_squared_amplitude_general_rcl', oi2)
    else
      call get_oi2(pow, pr, 'get_squared_amplitude_general_rcl', oi2)
    end if

    if (.not. allocated(matrix2)) then
      call error_rcl('No squared amplitude computed yet.', &
                     where='get_squared_amplitude_general_rcl')
    end if
    A2 = 0d0
    if (oi2 .ne. -1) A2 = matrix2(oi2,o,pr)

  end subroutine get_squared_amplitude_general_rcl

!------------------------------------------------------------------------------!

  subroutine get_polarized_squared_amplitude_general_rcl(npr,pow,order,hel,A2h)

    ! This subroutine extracts the computed value of the squared
    ! amplitude summed over colour with polarization "hel", with "pow"
    ! power of alpha_s at loop-order "order" for the process with
    ! process number "npr".
    ! - "pow" is an array whose values represent the power in fundamental
    ! couplings in the order as dictated by the model file.
    ! - "order" is the loop-order of the contribution.
    !   It is a character variable with accepted values:
    !   'LO'     -> tree squared amplitude
    !   'NLO'    -> loop squared amplitude
    !   'NLO-D4' -> loop squared amplitude, bare loop contribution
    !   'NLO-CT' -> loop squared amplitude, counterterms contribution
    !   'NLO-R2' -> loop squared amplitude, rational terms
    !                 contribution
    ! - "hel" is the helicity of the contribution.
    !   It is a vector whose entries are the helicities of the
    !   particles:
    !   left-handed  fermions/antifermions -> -1
    !   right-handed fermions/antifermions -> +1
    !   logitudinal vector bosons          ->  0
    !   transverse vector bosons           -> -1, +1
    !   scalar particles                   ->  0
    !   Example:
    !   Process 'u g -> W+ d'
    !   "hel" = (/-1,+1,-1,-1/) means
    !   left-handed up-quark
    !   transverse gluon (helicity = +1)
    !   transverse W+ (helicity = -1)
    !   left-handed down-quark

    ! This routine must be called after compute_process_rcl for the
    ! process with process number "npr"

    integer,               intent(in)  :: npr,hel(1:)
    integer, dimension(:), intent(in)  :: pow
    character(len=*),      intent(in)  :: order
    real(dp),              intent(out) :: A2h

    integer               :: prin,pr,i,legs,kf,h,o,oi
    integer, allocatable  :: helk(:)

    call processes_not_generated_error_rcl('get_polarized_squared_amplitude_general_rcl')

    call get_pr(npr, 'get_polarized_squared_amplitude_general_rcl', prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
    else
      pr = prin
    end if

    legs = prs(pr)%legs

    if (size(hel).ne.legs) then
      call error_rcl('get_polarized_squared_amplitude_general_rcl called ' // &
                     'with wrong helicity vector dimension')
    endif
    do i = 1,legs
      h = hel(i)
      if ((h .lt. -1) .or. (h .gt. +1)) then
        call error_rcl('get_polarized_squared_amplitude_general_rcl called ' //&
                       'with wrong helicity vector components')
      endif
    enddo

    allocate (helk(legs))
    if (prs(prin)%crosspr .ne. 0) then
      do i = 1,prs(pr)%legs
        if (i .gt. prs(pr)%legsIn) then
          helk(newleg(prs(prin)%relperm(i),pr)) = -hel(i)
        else
          helk(newleg(prs(prin)%relperm(i),pr)) = hel(i)
        end if
      end do
    else
      do i = 1,prs(pr)%legsIn
        helk(newleg(i,pr)) = hel(i)
      enddo
      do i = prs(pr)%legsIn+1,legs
        helk(newleg(i,pr)) = - hel(i)
      enddo
    end if
    kf = 0
    do i = 1,cfTot(pr)
      if (sum(abs(he(:,i,pr) - helk(:))) .eq. 0) then
        kf = i
        exit
      endif
    enddo

    if     (order.eq.'LO'    ) then; o = 0
    elseif (order.eq.'NLO-D4') then; o = 1
    elseif (order.eq.'NLO-CT') then; o = 2
    elseif (order.eq.'NLO-R2') then; o = 3
    elseif (order.eq.'NLO'   ) then; o = 4
    else
      call error_rcl('get_polarized_squared_amplitude_general_rcl called with wrong order. '// &
                     "Accepted values are: order = 'LO','NLO','NLO-D4',"// &
                     "'NLO-CT','NLO-R2'")
    endif

    call get_oi(pow, pr, 'get_polarized_squared_amplitude_general_rcl', oi)

    if (.not. allocated(matrix2h)) then
      call error_rcl('No polarized squared amplitude computed yet.', &
                     where='get_polarized_squared_amplitude_general_rcl')
    end if
    A2h = 0d0
    if (kf.ne.0) A2h = matrix2h(oi,kf,o,prin)

  end subroutine get_polarized_squared_amplitude_general_rcl

!------------------------------------------------------------------------------!

  subroutine compute_colour_correlation_rcl(npr,p,i1,i2,ord,A2cc,momenta_check)

    use modelfile, only: has_feature_mdl

    ! This subroutine computes the colour-correlated summed squared
    ! amplitude, between particle with leg number "i1" and particle
    ! with leg number "i2", for the process with process number "npr".
    ! "p" are the external momenta of the process.
    ! "A2cc" is the colour-correlated summed squared amplitude.
    ! The order is assumed to be LO unless specified by order ('LO' or 'NLO').
    ! If the LO does not exist and if 'NLO' is specified the loop-induced
    ! colour correlation amplitude is computed.
    ! Summed squared amplitude means:
    ! - summed over all computed powers of coupling constants
    ! - summed over colour and spins for outgoing particles and averaged
    !   for the incoming particles.
    ! If some particles were defined as polarized, no sum/average is
    ! performed on these particles.
    ! "momenta_check" tells whether the phase-space point is good
    ! (momenta_check=.true.) or bad (momenta_check=.false).

    integer,                    intent(in) :: npr,i1,i2
    real(dp),                   intent(in) :: p(0:,:)
    character(len=*), optional, intent(in) :: ord
    real(dp), intent(out), optional :: A2cc
    logical,  intent(out), optional :: momenta_check

    integer      :: pr,legs,j1,j2
    character(3) :: order

    call processes_not_generated_error_rcl('compute_colour_correlation_rcl')

    call get_pr(npr, 'compute_colour_correlation_rcl', pr)

    legs = prs(pr)%legs

    if ((i1.le.0.or.i1.gt.legs).or.(i2.le.0.or.i2.gt.legs)) then
      call error_rcl('compute_colour_correlation_rcl called with wrong indices.')
    endif

    if (size(p,2).ne.legs.or.size(p,1).ne.4) then
      call error_rcl('compute_colour_correlation_rcl called with wrong momenta '//&
                     'dimension.')
    endif

    call set_momenta (pr,p)
    if (present(momenta_check)) momenta_check = momcheck

    if (writeCor.ge.1) then
      call print_process_and_momenta (pr)
      call print_rescaling
      call print_parameters_change
    endif

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('Call with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'", &
                       where='compute_colour_correlation_rcl')
      endif
      order = ord
    end if

    call compute_amplitude (pr,order)

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,order)
    end if

    if (writeMat.ge.1) call print_amplitude (pr,order)

    if (order .eq. 'LO') then
      call compute_squared_amplitude_cc (pr,i1,i2)
    else
      call compute_squared_amplitude_cc_nlo (pr,i1,i2)
    end if

    if (writeCor.ge.1) call print_squared_amplitude_cc (pr,i1,i2,order)

    if (present(A2cc)) then
      if (order .eq. 'LO') then
        if (prs(pr)%crosspr .ne. 0) then
          j1 = newleg(prs(pr)%relperm(i1),prs(pr)%crosspr)
          j2 = newleg(prs(pr)%relperm(i2),prs(pr)%crosspr)
          A2cc = sum(matrix2cc(1:oi2Size(prs(pr)%crosspr),j1,j2,pr))
        else
          j1 = newleg(i1,pr)
          j2 = newleg(i2,pr)
          A2cc = sum(matrix2cc(1:oi2Size(pr),j1,j2,pr))
        end if
      else
        if (prs(pr)%crosspr .ne. 0) then
          j1 = newleg(prs(pr)%relperm(i1),prs(pr)%crosspr)
          j2 = newleg(prs(pr)%relperm(i2),prs(pr)%crosspr)
          A2cc = sum(matrix2ccnlo(1:oi2Size(prs(pr)%crosspr),j1,j2,pr))
        else
          j1 = newleg(i1,pr)
          j2 = newleg(i2,pr)
          A2cc = sum(matrix2ccnlo(1:oi2Size(pr),j1,j2,pr))
        end if
      end if
    end if

  end subroutine compute_colour_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine compute_colour_correlation_int_rcl (npr,p,i1,i2,A2ccnlo, &
                                                 momenta_check)
    use modelfile, only: has_feature_mdl

    integer,  intent(in)            :: npr,i1,i2
    real(dp), intent(in)            :: p(0:,:)
    real(dp), intent(out), optional :: A2ccnlo
    logical,  intent(out), optional :: momenta_check

    integer :: pr,legs,j1,j2

    call processes_not_generated_error_rcl('compute_colour_correlation_int_rcl')
    call get_pr(npr, 'compute_colour_correlation_int_rcl', pr)

    legs = prs(pr)%legs

    if ((i1.le.0.or.i1.gt.legs).or.(i2.le.0.or.i2.gt.legs)) then
      call error_rcl('compute_colour_correlation_int_rcl called with wrong indices.')
    endif

    if (size(p,2).ne.legs.or.size(p,1).ne.4) then
      call error_rcl('compute_colour_correlation_int_rcl called with wrong momenta.')
    endif

    call set_momenta (pr,p)
    if (present(momenta_check)) momenta_check = momcheck

    if (writeCor.ge.1) then
      call print_process_and_momenta (pr)
      call print_rescaling
      call print_parameters_change
    endif

    call compute_amplitude (pr,'NLO')

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,'NLO')
    end if

    if (writeMat.ge.1) call print_amplitude (pr,'NLO')

    call compute_squared_amplitude_cc_int (pr,i1,i2)

    if (writeCor.ge.1) then
      call print_squared_amplitude_cc_int (pr,i1,i2)
      write(nx,'(1x,75("x"))')
      write(nx,*)
      write(nx,*)
    endif

    if (present(A2ccnlo)) then
      if (prs(pr)%crosspr .ne. 0) then
        j1 = newleg(prs(pr)%relperm(i1),prs(pr)%crosspr)
        j2 = newleg(prs(pr)%relperm(i2),prs(pr)%crosspr)
        A2ccnlo = sum(matrix2ccint(1:oi2Size(prs(pr)%crosspr),j1,j2,pr))
      else
        j1 = newleg(i1,pr)
        j2 = newleg(i2,pr)
        A2ccnlo = sum(matrix2ccint(1:oi2Size(pr),j1,j2,pr))
      end if
    endif

  end subroutine compute_colour_correlation_int_rcl

!------------------------------------------------------------------------------!

  subroutine rescale_colour_correlation_int_rcl (npr,i1,i2,A2ccint)

    use modelfile, only: has_feature_mdl

    integer,  intent(in)            :: npr,i1,i2
    real(dp), intent(out), optional :: A2ccint

    integer :: pr,legs,j1,j2

    call processes_not_generated_error_rcl('rescale_colour_correlation_int_rcl')
    call get_pr(npr, 'rescale_colour_correlation_int_rcl', pr)

    legs = prs(pr)%legs

    if ((i1.le.0.or.i1.gt.legs).or.(i2.le.0.or.i2.gt.legs)) then
      call error_rcl('rescale_colour_correlation_int_rcl called with wrong indices.')
    endif

    if (writeCor.ge.1) call print_rescaling
    if (writeCor.ge.1) call print_parameters_change

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,'NLO')
    end if

    if (writeMat.ge.1) call print_amplitude (pr,'NLO')

    call compute_squared_amplitude_cc_int (pr,i1,i2)

    if (writeCor.ge.1) then
      call print_squared_amplitude_cc_int (pr,i1,i2)
      write(nx,'(1x,75("x"))')
      write(nx,*)
      write(nx,*)
    endif

    if (present(A2ccint)) then
      if (prs(pr)%crosspr .ne. 0) then
        j1 = newleg(prs(pr)%relperm(i1),prs(pr)%crosspr)
        j2 = newleg(prs(pr)%relperm(i2),prs(pr)%crosspr)
        A2ccint = sum(matrix2ccint(1:oi2Size(prs(pr)%crosspr),j1,j2,pr))
      else
        j1 = newleg(i1,pr)
        j2 = newleg(i2,pr)
        A2ccint = sum(matrix2ccint(1:oi2Size(pr),j1,j2,pr))
      end if
    endif

  end subroutine rescale_colour_correlation_int_rcl

!------------------------------------------------------------------------------!

  subroutine compute_all_colour_correlations_rcl(npr,p,ord,momenta_check)

    use modelfile, only: has_feature_mdl

    ! This subroutine computes the colour-correlated summed squared
    ! amplitudes for the process with process number "npr".
    ! "p" are the external momenta of the process.
    ! The colour correlation is done for all possible pairs coloured
    ! particles.
    ! Summed squared amplitude means:
    ! - summed over all computed powers of g_s
    ! - summed over colour and spins for outgoing particles and averaged
    !   for the incoming particles.
    ! If some particles were defined as polarized, no sum/average is
    ! performed on these particles.
    ! "momenta_check" tells whether the phase-space point is good
    ! (momenta_check=.true.) or bad (momenta_check=.false).

    integer,                    intent(in)  :: npr
    real(dp),                   intent(in)  :: p(0:,:)
    character(len=*), optional, intent(in)  :: ord
    logical, optional,          intent(out) :: momenta_check

    integer      :: pr,legs,i1,i2
    character(3) :: order

    call processes_not_generated_error_rcl('compute_all_colour_correlations_rcl')
    call get_pr(npr, 'compute_all_colour_correlations_rcl', pr)

    legs = prs(pr)%legs

    if (size(p,2).ne.legs.or.size(p,1).ne.4) then
      call error_rcl('Wrong momenta.', &
                     where='compute_all_colour_correlations_rcl')
    endif

    call set_momenta (pr,p)
    if (present(momenta_check)) momenta_check = momcheck

    if (writeCor.ge.1) then
      call print_process_and_momenta (pr)
      call print_rescaling
      call print_parameters_change
    endif

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('Call with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'", &
                       where='compute_all_colour_correlations_rcl')
      endif
      order = ord
    end if

    call compute_amplitude (pr,order)

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,order)
    end if

    call compute_squared_amplitude (pr,order)

    if (writeMat.ge.1) call print_amplitude (pr,order)

    do i1 = 1,legs
      do i2 = 1,legs
        if (order .eq. 'LO') then
          call compute_squared_amplitude_cc (pr,i1,i2)
        else
          call compute_squared_amplitude_cc_nlo (pr,i1,i2)
        end if
        if (writeCor.ge.1) call print_squared_amplitude_cc (pr,i1,i2,order)
      end do
    end do

    if (writeCor.ge.1) then
      call openOutput
      write(nx,'(1x,75("x"))')
      write(nx,*)
      write(nx,*)
    endif

  end subroutine compute_all_colour_correlations_rcl

!------------------------------------------------------------------------------!

  subroutine compute_all_colour_correlations_int_rcl (npr,p,momenta_check)

    use modelfile, only: has_feature_mdl

    integer,  intent(in)            :: npr
    real(dp), intent(in)            :: p(0:,:)
    logical,  intent(out), optional :: momenta_check

    integer :: pr,legs,i1,i2


    call processes_not_generated_error_rcl('compute_all_colour_correlations_int_rcl')
    call get_pr(npr, 'compute_all_colour_correlations_int_rcl', pr)

    legs = prs(pr)%legs

    if (size(p,2).ne.legs.or.size(p,1).ne.4) then
      call error_rcl('compute_all_colour_correlations_int_rcl called with wrong momenta')
    endif

    call set_momenta (pr,p)
    if (present(momenta_check)) momenta_check = momcheck

    if (writeCor.ge.1) then
      call print_process_and_momenta (pr)
      call print_rescaling
      call print_parameters_change
    endif

    call compute_amplitude (pr,'NLO')

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,'NLO')
    end if

    if (writeMat.ge.1) call print_amplitude (pr,'NLO')

    do i1 = 1,legs
    do i2 = 1,legs
      call compute_squared_amplitude_cc_int (pr,i1,i2)
      if (writeCor.ge.1) call print_squared_amplitude_cc_int (pr,i1,i2)
    enddo
    enddo

    if (writeCor.ge.1) then
      call openOutput
      write(nx,'(1x,75("x"))')
      write(nx,*)
      write(nx,*)
    endif

  end subroutine compute_all_colour_correlations_int_rcl

!------------------------------------------------------------------------------!

  subroutine rescale_colour_correlation_rcl(npr,i1,i2,ord,A2cc)

    use modelfile, only: has_feature_mdl

    ! This subroutine adjusts the results calculated by
    ! compute_colour_correlation_rcl for a new value of alpha_s,
    ! rescaling the structure-dressed helicity amplitudes and
    ! recomputing the colour-correlated summed squared amplitude for
    ! the process with process number "npr".
    ! "i1", "i2" and "A2cc" are the same has for
    ! compute_colour_correlation_rcl.

    integer,                    intent(in)  :: npr,i1,i2
    character(len=*), optional, intent(in)  :: ord
    real(dp), optional,         intent(out) :: A2cc

    integer      :: pr,legs,j1,j2
    character(3) :: order

    if (.not. (qcd_rescaling .and. has_feature_mdl('qcd_rescaling'))) then
      call error_rcl('Call of rescale_colour_correlation_rcl not allowed: ' // &
                     'qcd_rescaling not enabled.')
      stop
    end if

    call processes_not_generated_error_rcl('rescale_colour_correlation_rcl')

    call get_pr(npr, 'rescale_colour_correlation_rcl', pr)

    legs = prs(pr)%legs

    if ((i1.le.0.or.i1.gt.legs).or.(i2.le.0.or.i2.gt.legs)) then
      call error_rcl('Wrong indices i1,i2', &
                     where='rescale_colour_correlation_rcl')
    endif

    if (writeCor.ge.1) call print_rescaling
    if (writeCor.ge.1) call print_parameters_change

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('Call with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'", &
                       where='rescale_colour_correlation_rcl')
      endif
      order = ord
    end if

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,order)
    end if

    if (writeMat.ge.1) call print_amplitude (pr,order)

    if (order .eq. 'LO') then
      call compute_squared_amplitude_cc (pr,i1,i2)
    else
      call compute_squared_amplitude_cc_nlo (pr,i1,i2)
    end if

    if (writeCor.ge.1) call print_squared_amplitude_cc (pr,i1,i2,order)

    if (present(A2cc)) then
      if (prs(pr)%crosspr .ne. 0) then
        j1 = newleg(prs(pr)%relperm(i1),prs(pr)%crosspr)
        j2 = newleg(prs(pr)%relperm(i2),prs(pr)%crosspr)
        if (order .eq. 'LO') then
          A2cc = sum(matrix2cc(1:oi2Size(prs(pr)%crosspr),j1,j2,pr))
        else
          A2cc = sum(matrix2ccnlo(1:oi2Size(prs(pr)%crosspr),j1,j2,pr))
        end if
      else
        j1 = newleg(i1,pr)
        j2 = newleg(i2,pr)
        if (order .eq. 'LO') then
          A2cc = sum(matrix2cc(1:oi2Size(pr),j1,j2,pr))
        else
          A2cc = sum(matrix2ccnlo(1:oi2Size(pr),j1,j2,pr))
        end if
      end if
    end if

  end subroutine rescale_colour_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine rescale_all_colour_correlations_rcl(npr)

    use modelfile, only: has_feature_mdl

    ! This subroutine adjusts the results calculated by
    ! compute_all_colour_correlations_rcl for a new value of alpha_s,
    ! rescaling the structure-dressed helicity amplitudes and
    ! recomputing the colour-correlated summed squared amplitude for
    ! the process with process number "npr".

    integer,  intent(in) :: npr

    integer :: pr,legs,i1,i2

    if (.not. (qcd_rescaling .and. has_feature_mdl('qcd_rescaling'))) then
      call error_rcl('Call of rescale_all_colour_correlations_rcl not allowed: ' // &
                     'qcd_rescaling not enabled.')
      stop
    end if

    call processes_not_generated_error_rcl('rescale_all_colour_correlations_rcl')

    call get_pr(npr, 'rescale_all_colour_correlations_rcl', pr)

    legs = prs(pr)%legs

    do i1 = 1,legs
      do i2 = 1,legs
        call compute_squared_amplitude_cc(pr,i1,i2)
        if (writeCor.ge.1) call print_squared_amplitude_cc(pr,i1,i2,'LO')
      enddo
    enddo

    if (writeCor.ge.1) then
      call openOutput
      write(nx,'(1x,75("x"))')
      write(nx,*)
      write(nx,*)
    endif

  end subroutine rescale_all_colour_correlations_rcl

!------------------------------------------------------------------------------!

  subroutine rescale_all_colour_correlations_int_rcl(npr)

    use modelfile, only: has_feature_mdl

    ! This subroutine adjusts the results calculated by
    ! compute_all_colour_correlations_int_rcl for a new value of alpha_s,
    ! rescaling the structure-dressed helicity amplitudes and
    ! recomputing the colour-correlated summed squared amplitude for
    ! the process with process number "npr".

    integer,  intent(in) :: npr

    integer :: pr,legs,i1,i2

    if (.not. (qcd_rescaling .and. has_feature_mdl('qcd_rescaling'))) then
      call error_rcl('Call of rescale_all_colour_correlations_int_rcl not allowed: ' // &
                     'qcd_rescaling not enabled.')
      stop
    end if

    call processes_not_generated_error_rcl('rescale_all_colour_correlations_int_rcl')

    call get_pr(npr, 'rescale_all_colour_correlations_int_rcl', pr)

    legs = prs(pr)%legs

    do i1 = 1,legs
      do i2 = 1,legs
        call compute_squared_amplitude_cc_int(pr,i1,i2)
        if (writeCor.ge.1) call print_squared_amplitude_cc_int(pr,i1,i2)
      enddo
    enddo

    if (writeCor.ge.1) then
      call openOutput
      write(nx,'(1x,75("x"))')
      write(nx,*)
      write(nx,*)
    endif

  end subroutine rescale_all_colour_correlations_int_rcl

!------------------------------------------------------------------------------!

  subroutine get_colour_correlation_general_rcl(npr,pow,i1,i2,ord,A2cc)

    ! Extracts the computed value of the colour-correlated summed squared
    ! amplitude, with "pow" being order in the fundamental couplings
    ! between particle with leg number "i1" and particle with
    ! leg number "i2", at loop-order "order" for the process with process
    ! number "npr".
    ! This routine must be called after compute_colour_correlation_rcl
    ! or rescale_colour_correlation_rcl(for process with process number
    ! "npr" for particles "i1" and "i2") or after
    ! compute_all_colour_correlations_rcl for process or
    ! rescale_all_colour_correlations_rcl for process with process
    ! number "npr".

    integer,                    intent(in)  :: npr,i1,i2
    integer, dimension(:),      intent(in)  :: pow
    character(len=*), optional, intent(in)  :: ord
    real(dp),                   intent(out) :: A2cc

    integer      :: prin,pr,legs,j1,j2,oi2
    character(3) :: order

    call processes_not_generated_error_rcl('get_colour_correlation_rcl')

    call get_pr(npr, 'get_colour_correlation_rcl', prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
      j1 = newleg(prs(prin)%relperm(i1),prs(prin)%crosspr)
      j2 = newleg(prs(prin)%relperm(i2),prs(prin)%crosspr)
    else
      pr = prin
      j1 = newleg(i1,pr)
      j2 = newleg(i2,pr)
    end if

    legs = prs(pr)%legs

    if ((i1 .le. 0 .or. i1 .gt. legs) .or. (i2 .le. 0 .or. i2 .gt. legs)) then
      call error_rcl('get_colour_correlation_rcl called with wrong indices.')
    endif

    call get_oi2(pow, pr, 'get_colour_correlation_rcl', oi2)

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('compute_process_rcl called with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'")
      endif
      order = ord
    end if

    if (order .eq. 'LO') then
      if (.not. allocated(matrix2cc)) then
        call error_rcl('No LO colour correlation computed yet.', &
                       where='get_colour_correlation_general_rcl')
      end if
      A2cc = 0d0
      if (oi2 .ne. -1) A2cc = matrix2cc(oi2,j1,j2,prin)
    else
      if (.not. allocated(matrix2ccnlo)) then
        call error_rcl('No NLO colour correlation computed yet.', &
                       where='get_colour_correlation_general_rcl')
      end if
      A2cc = 0d0
      if (oi2 .ne. -1) A2cc = matrix2ccnlo(oi2,j1,j2,prin)
    end if

  end subroutine get_colour_correlation_general_rcl

!------------------------------------------------------------------------------!

  subroutine get_colour_correlation_int_general_rcl(npr,pow,i1,i2,A2ccint)

    ! This subroutine extracts the computed value of the
    ! colour-correlated summed squared amplitude, with "pow" power of
    ! alpha_s, between particle with leg number "i1" and particle with
    ! leg number "i2", for the process with process number "npr".
    ! This routine must be called after compute_colour_correlation_int_rcl
    ! or rescale_colour_correlation_int_rcl(for process with process number
    ! "npr" for particles "i1" and "i2") or after
    ! compute_all_colour_correlation_ints_rcl for process or
    ! rescale_all_colour_correlation_ints_rcl for process with process
    ! number "npr".

    integer,               intent(in) :: npr,i1,i2
    integer, dimension(:), intent(in) :: pow
    real(dp),              intent(out) :: A2ccint

    integer :: prin,pr,legs,j1,j2,oi2

    call processes_not_generated_error_rcl('get_colour_correlation_int_general_rcl')

    call get_pr(npr, 'get_colour_correlation_int_general_rcl', prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
      j1 = newleg(prs(prin)%relperm(i1),prs(prin)%crosspr)
      j2 = newleg(prs(prin)%relperm(i2),prs(prin)%crosspr)
    else
      pr = prin
      j1 = newleg(i1,pr)
      j2 = newleg(i2,pr)
    end if

    legs = prs(pr)%legs

    if ((i1 .le. 0 .or. i1 .gt. legs) .or. (i2 .le. 0 .or. i2 .gt. legs)) then
      call error_rcl('get_colour_correlation_int_general_rcl called with wrong indices.')
    endif

    call get_oi2(pow, pr, 'get_colour_correlation_int_general_rcl', oi2)

    if (.not. allocated(matrix2ccint)) then
      call error_rcl('No colour correlation interference computed yet.', &
                     where='get_colour_correlation_int_general_rcl')
    end if
    A2ccint = 0d0
    if (oi2 .ne. -1) A2ccint = matrix2ccint(oi2,j1,j2,prin)

  end subroutine get_colour_correlation_int_general_rcl

!------------------------------------------------------------------------------!

  subroutine compute_spin_colour_correlation_rcl(npr,p,i1,i2,v,ord, &
                                                 A2scc,momenta_check)

    use modelfile, only: has_feature_mdl

    ! This subroutine computes the spin-colour-correlated summed squared
    ! amplitude, between particle with leg number "i1" and particle
    ! with leg number "i2", for the process with process number "npr",
    ! allowing spin-correlation if particle with leg number "i1"
    ! is a gluon.
    ! The spin-correlation is achieved by a user-defined polarization
    ! vector ("v") for the particle with leg number "i1", which
    ! substitutes the usual one.
    ! "p" are the external momenta of the process.
    ! "A2scc" is the spin-colour-correlated summed squared  LO
    ! amplitude.
    ! Summed squared amplitude means:
    ! - summed over all computed powers of g_s
    ! - summed over colour and spins for outgoing particles and averaged
    !   for the incoming particles.
    ! If some particles were defined as polarized, no sum/average is
    ! performed on these particles.
    ! "momenta_check" tells whether the phase-space point is good
    ! (momenta_check=.true.) or bad (momenta_check=.false).

    integer,                    intent(in)  :: npr,i1,i2
    character(len=*), optional, intent(in)  :: ord
    real(dp),                   intent(in)  :: p(0:,:)
    complex(dp),                intent(in)  :: v(0:)
    real(dp), optional,         intent(out) :: A2scc
    logical,  optional,         intent(out) :: momenta_check

    integer      :: pr,legs,j1,j2
    character(2) :: t2
    character(3) :: order

    call processes_not_generated_error_rcl('compute_spin_colour_correlation_rcl')
    call get_pr(npr, 'compute_spin_colour_correlation_rcl', pr)

    legs = prs(pr)%legs

    t2  = get_particle_type2_mdl(prs(pr)%par(i1))

    if ((t2 .ne. 'g') .or. &
        (i1 .le. 0 .or. i1 .gt. legs)   .or. &
        (i2 .le. 0 .or. i2 .gt. legs)        &
      ) then
      call error_rcl('compute_spin_colour_correlation_rcl called with wrong indices.')
    endif

    if (size(p,2) .ne. legs .or. size(p,1) .ne. 4) then
      call error_rcl('compute_spin_colour_correlation_rcl called with wrong '//&
                     'momenta dimension.')
    endif

    call set_momenta (pr,p)
    if (present(momenta_check)) momenta_check = momcheck

    if (writeCor.ge.1) then
      call print_process_and_momenta (pr)
    endif

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('compute_process_rcl called with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'")
      endif
      order = ord
    end if

    call compute_amplitude (pr,order)

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,order)
    end if

    if (writeMat.ge.1) call print_amplitude (pr,order)

    if (order .eq. 'LO') then
      call compute_squared_amplitude_scc (pr,i1,i2,v)
    else
      call compute_squared_amplitude_scc_nlo (pr,i1,i2,v)
    end if

    if (writeCor.ge.1) call print_squared_amplitude_scc (pr,i1,i2,v,order)

    if (present(A2scc)) then
      if (order .eq. 'LO') then
        if (prs(pr)%crosspr .ne. 0) then
          j1 = newleg(prs(pr)%relperm(i1),prs(pr)%crosspr)
          j2 = newleg(prs(pr)%relperm(i2),prs(pr)%crosspr)
          A2scc = sum(matrix2scc(1:oi2Size(prs(pr)%crosspr),j1,j2,pr))
        else
          j1 = newleg(i1,pr)
          j2 = newleg(i2,pr)
          A2scc = sum(matrix2scc(1:oi2Size(pr),j1,j2,pr))
        end if
      else
        if (prs(pr)%crosspr .ne. 0) then
          j1 = newleg(prs(pr)%relperm(i1),prs(pr)%crosspr)
          j2 = newleg(prs(pr)%relperm(i2),prs(pr)%crosspr)
          A2scc = sum(matrix2sccnlo(1:oi2Size(prs(pr)%crosspr),j1,j2,pr))
        else
          j1 = newleg(i1,pr)
          j2 = newleg(i2,pr)
          A2scc = sum(matrix2sccnlo(1:oi2Size(pr),j1,j2,pr))
        end if
      end if
    end if

  end subroutine compute_spin_colour_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine rescale_spin_colour_correlation_rcl(npr,i1,i2,v,ord,A2scc)

    use modelfile, only: has_feature_mdl

    ! This subroutine adjusts the results calculated by
    ! compute_spin_colour_correlation_rcl for a new value of alpha_s,
    ! rescaling the structure-dressed helicity amplitudes and
    ! recomputing the spin-colour-correlated summed squared amplitude
    ! for the process with process number "npr".
    ! "i1", "i2", "v" and "A2scc" are the same has for
    ! compute_spin_colour_correlation_rcl.

    integer,                    intent(in)  :: npr,i1,i2
    complex(dp),                intent(in)  :: v(0:)
    character(len=*), optional, intent(in)  :: ord
    real(dp), optional,         intent(out) :: A2scc

    integer      :: pr,legs,j1,j2
    character(2) :: t2
    character(3) :: order

    if (.not. (qcd_rescaling .and. has_feature_mdl('qcd_rescaling'))) then
      call error_rcl('Rescaling process not allowed: ' // &
                     'qcd_rescaling not enabled.', &
                     where='rescale_spin_colour_correlation_rcl')
      stop
    end if

    call processes_not_generated_error_rcl('rescale_spin_colour_correlation_rcl')

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('compute_process_rcl called with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'")
      endif
      order = ord
    end if

    call get_pr(npr, 'rescale_spin_colour_correlation_rcl', pr)

    legs = prs(pr)%legs

    t2  = get_particle_type2_mdl(prs(pr)%par(i1))
    if ((t2 .ne. 'g') .or. &
        (i1 .le. 0 .or. i1 .gt. legs)   .or. &
        (i2 .le. 0 .or. i2 .gt. legs)        &
      ) then

      call error_rcl('Wrong indices i1,i2.', &
                     where='rescale_spin_colour_correlation_rcl')
    endif

    if (writeCor.ge.1) call print_rescaling
    if (writeCor.ge.1) call print_parameters_change

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,order)
    end if

    if (writeMat.ge.1) call print_amplitude (pr,order)

    if (order .eq. 'LO') then
      call compute_squared_amplitude_scc (pr,i1,i2,v)
    else
      call compute_squared_amplitude_scc_nlo (pr,i1,i2,v)
    end if

    if (writeCor.ge.1) call print_squared_amplitude_scc (pr,i1,i2,v,order)

    if (present(A2scc)) then
      if (order .eq. 'LO') then
        if (prs(pr)%crosspr .ne. 0) then
          j1 = newleg(prs(pr)%relperm(i1),prs(pr)%crosspr)
          j2 = newleg(prs(pr)%relperm(i2),prs(pr)%crosspr)
          A2scc = sum(matrix2scc(1:oi2Size(prs(pr)%crosspr),j1,j2,pr))
        else
          j1 = newleg(i1,pr)
          j2 = newleg(i2,pr)
          A2scc = sum(matrix2scc(1:oi2Size(pr),j1,j2,pr))
        end if
      else
        if (prs(pr)%crosspr .ne. 0) then
          j1 = newleg(prs(pr)%relperm(i1),prs(pr)%crosspr)
          j2 = newleg(prs(pr)%relperm(i2),prs(pr)%crosspr)
          A2scc = sum(matrix2sccnlo(1:oi2Size(prs(pr)%crosspr),j1,j2,pr))
        else
          j1 = newleg(i1,pr)
          j2 = newleg(i2,pr)
          A2scc = sum(matrix2sccnlo(1:oi2Size(pr),j1,j2,pr))
        end if
      end if
    endif

  end subroutine rescale_spin_colour_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine get_spin_colour_correlation_general_rcl(npr,pow,i1,i2,ord,A2scc)

    ! This subroutine extracts the computed value of the
    ! spin-colour-correlated summed squared amplitude with "pow" power
    ! of alpha_s, between particle with leg number "i1" and particle
    ! with leg number "i2", for the process with process number "npr".
    ! This routine must be called after
    ! compute_spin_colour_correlation_rcl or
    ! rescale_spin_colour_correlation_rcl for process with process
    ! number "npr" for particles "i1" and "i2" with the  desired
    ! polarization vestor.

    integer,                    intent(in)  :: npr,i1,i2
    integer, dimension(:),      intent(in)  :: pow
    character(len=*), optional, intent(in)  :: ord
    real(dp),                   intent(out) :: A2scc

    integer      :: pr,legs,j1,j2,oi2
    character(2) :: t2
    character(3) :: order

    call processes_not_generated_error_rcl('get_spin_colour_correlation_rcl')

    call get_pr(npr, 'get_spin_colour_correlation_rcl', pr)

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('compute_process_rcl called with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'")
      endif
      order = ord
    end if

    legs = prs(pr)%legs

    t2 = get_particle_type2_mdl(prs(pr)%par(i1))

    if ((t2 .ne. 'g') .or. &
        (i1 .le. 0 .or. i1 .gt. legs)   .or. &
        (i2 .le. 0 .or. i2 .gt. legs)        &
      ) then
      call error_rcl('Wrong ndices.', &
                     where='get_spin_colour_correlation_general_rcl')
    endif

    if (prs(pr)%crosspr .ne. 0) then
      j1 = newleg(prs(pr)%relperm(i1),prs(pr)%crosspr)
      j2 = newleg(prs(pr)%relperm(i2),prs(pr)%crosspr)
      call get_oi2(pow, prs(pr)%crosspr, 'get_spin_colour_correlation_general_rcl', oi2)
    else
      j1 = newleg(i1,pr)
      j2 = newleg(i2,pr)
      call get_oi2(pow, pr, 'get_spin_colour_correlation_general_rcl', oi2)
    end if


    A2scc = 0d0
    if (order .eq. 'LO') then
      if (.not. allocated(matrix2scc)) &
        call error_rcl('No spin colour correlation computed yet.', &
                       where='get_spin_colour_correlation_general_rcl')
      if (oi2 .ne. -1) A2scc = matrix2scc(oi2,j1,j2,pr)
    else
      if (.not. allocated(matrix2sccnlo)) &
        call error_rcl('No NLO spin colour correlation computed yet.', &
                       where='get_spin_colour_correlation_general_rcl')
      if (oi2 .ne. -1) A2scc = matrix2sccnlo(oi2,j1,j2,pr)
    end if

  end subroutine get_spin_colour_correlation_general_rcl

!------------------------------------------------------------------------------!

  subroutine compute_spin_correlation_rcl(npr,p,j,v,ord,A2sc,momenta_check)

    use modelfile, only: has_feature_mdl

    ! This subroutine computes the summed squared amplitude in the
    ! case where the polarization vector of one of the external photons
    ! or gluons is given by the user.
    ! "p" are the external momenta of the process.
    ! "j" is an integer indicating the leg number for the photon or
    ! gluon with the user-defined polarization vector.
    ! "v" is the user-defined polarization vector.
    ! "A2sc" is the spin-colour-correlated summed squared LO
    ! amplitude.
    ! Summed squared amplitude means:
    ! - summed over all computed powers of g_s
    ! - summed over colour and spins for outgoing particles and averaged
    !   for the incoming particles.
    ! If some particles were defined as polarized, no sum/average is
    ! performed on these particles.
    ! "momenta_check" tells whether the phase-space point is good
    ! (momenta_check=.true.) or bad (momenta_check=.false).

    integer,                    intent(in)  :: npr,j
    real(dp),                   intent(in)  :: p(0:,:)
    complex(dp),                intent(in)  :: v(0:)
    character(len=*), optional, intent(in)  :: ord
    real(dp), optional,         intent(out) :: A2sc
    logical, optional,          intent(out) :: momenta_check

    integer      :: pr,legs
    character(2) :: t2
    character(3) :: order

    call processes_not_generated_error_rcl('compute_spin_correlation_rcl')

    call get_pr(npr, 'compute_spin_correlation_rcl', pr)

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('compute_process_rcl called with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'")
      endif
      order = ord
    end if

    legs = prs(pr)%legs

    t2 = get_particle_type2_mdl(prs(pr)%par(j))

    if ((t2 .ne.'v'.and. t2 .ne. 'g') .or. &
        j .le. 0 .or. j .gt. legs) then
      call error_rcl('compute_spin_correlation_rcl called with wrong index')
    endif

    if (size(p,2) .ne. legs .or. size(p,1) .ne. 4) then
      call error_rcl('compute_spin_correlation_rcl called with wrong momenta '//&
                     'dimension')
    endif

    call set_momenta (pr,p)
    if (present(momenta_check)) momenta_check = momcheck

    if (writeCor.ge.1) then
      call print_process_and_momenta (pr)
    endif

    call compute_amplitude (pr,order)

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,order)
    end if

    if (writeMat.ge.1) call print_amplitude (pr,order)

    if (order .eq. 'LO') then
      call compute_squared_amplitude_sc (pr,j,v)
    else
      call compute_squared_amplitude_sc_nlo (pr,j,v)
    end if

    if (writeCor.ge.1) call print_squared_amplitude_sc (pr,j,v,order)

    if (present(A2sc)) then
      if (order .eq. 'LO') then
        if (prs(pr)%crosspr .ne. 0) then
          A2sc = sum(matrix2sc(1:oi2Size(prs(pr)%crosspr),pr))
        else
          A2sc = sum(matrix2sc(1:oi2Size(pr),pr))
        end if
      else
        if (prs(pr)%crosspr .ne. 0) then
          A2sc = sum(matrix2scnlo(1:oi2Size(prs(pr)%crosspr),pr))
        else
          A2sc = sum(matrix2scnlo(1:oi2Size(pr),pr))
        end if
      end if
    end if

  end subroutine compute_spin_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine rescale_spin_correlation_rcl(npr,j,v,ord,A2sc)

    use modelfile, only: has_feature_mdl

    ! This subroutine adjusts the results calculated by
    ! compute_spin_correlation_rcl for a new value of alpha_s,
    ! rescaling the structure-dressed helicity amplitudes and
    ! recomputing the spin-correlated summed squared amplitude for
    ! the process with process number "npr".
    ! "j", "v" and "A2sc" are the same has for
    ! compute_spin_correlation_rcl.

    integer,                    intent(in)  :: npr,j
    complex(dp),                intent(in)  :: v(0:)
    character(len=*), optional, intent(in)  :: ord
    real(dp), optional,         intent(out) :: A2sc

    integer      :: pr,legs
    character(2) :: t2
    character(3) :: order

    if (.not. (qcd_rescaling .and. has_feature_mdl('qcd_rescaling'))) then
      call error_rcl('Rescaling process not allowed: ' // &
                     'qcd_rescaling not enabled.', &
                     where='rescale_spin_correlation_rcl')
      stop
    end if

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('compute_process_rcl called with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'")
      endif
      order = ord
    end if

    call processes_not_generated_error_rcl('rescale_spin_correlation_rcl')

    call get_pr(npr, 'rescale_spin_correlation_rcl', pr)

    legs = prs(pr)%legs

    t2  = get_particle_type2_mdl(prs(pr)%par(j))
    if ( (t2 .ne. 'v' .and. t2 .ne. 'g') .or. j .le. 0 .or. j .gt. legs ) then
      call error_rcl('Wrong index j.', &
                     where='rescale_spin_correlation_rcl')
    endif

    if (writeCor.ge.1) call print_rescaling
    if (writeCor.ge.1) call print_parameters_change

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,order)
    end if

    if (writeMat.ge.1) call print_amplitude (pr,order)

    if (order .eq. 'LO') then
      call compute_squared_amplitude_sc (pr,j,v)
    else
      call compute_squared_amplitude_sc_nlo (pr,j,v)
    end if

    if (writeCor.ge.1) call print_squared_amplitude_sc (pr,j,v,order)

    if (present(A2sc)) then
      if (order .eq. 'LO') then
        if (prs(pr)%crosspr .ne. 0) then
          A2sc = sum(matrix2sc(1:oi2Size(prs(pr)%crosspr),pr))
        else
          A2sc = sum(matrix2sc(1:oi2Size(pr),pr))
        end if
      else
        if (prs(pr)%crosspr .ne. 0) then
          A2sc = sum(matrix2scnlo(1:oi2Size(prs(pr)%crosspr),pr))
        else
          A2sc = sum(matrix2scnlo(1:oi2Size(pr),pr))
        end if
      end if
    end if

  end subroutine rescale_spin_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine get_spin_correlation_general_rcl(npr,pow,ord,A2sc)

    ! This subroutine extracts the computed value of the
    ! spin-correlated summed squared amplitude with "pow" power
    ! of alpha_s for the process with process number "npr".
    ! This routine must be called after compute_spin_correlation_rcl
    ! or rescale_spin_correlation_rcl for process with process number
    ! "npr" with the desired polarization vestor.

    integer,                    intent(in)  :: npr
    integer, dimension(:),      intent(in)  :: pow
    character(len=*), optional, intent(in)  :: ord
    real(dp),                   intent(out) :: A2sc

    integer      :: pr,oi2
    character(3) :: order

    call processes_not_generated_error_rcl('get_spin_correlation_rcl')

    call get_pr(npr, 'get_spin_correlation_rcl', pr)

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('compute_process_rcl called with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'")
      endif
      order = ord
    end if

    if (prs(pr)%crosspr .ne. 0) then
      call get_oi2(pow, prs(pr)%crosspr, 'get_spin_correlation_rcl', oi2)
    else
      call get_oi2(pow, pr, 'get_spin_correlation_rcl', oi2)
    end if


    A2sc = 0d0
    if (order .eq. 'LO') then
      if (.not. allocated(matrix2sc)) &
        call error_rcl('No spin correlation computed yet.', &
                       where='get_spin_correlation_general_rcl')
      if (oi2 .ne. -1) A2sc = matrix2sc(oi2,pr)
    else
      if (.not. allocated(matrix2scnlo)) &
        call error_rcl('No NLO spin correlation computed yet.', &
                       where='get_spin_correlation_general_rcl')
      if (oi2 .ne. -1) A2sc = matrix2scnlo(oi2,pr)
    end if

  end subroutine get_spin_correlation_general_rcl

!------------------------------------------------------------------------------!

  subroutine compute_spin_correlation_matrix_rcl(npr,p,j,ord,A2scm,momenta_check)

    use modelfile, only: has_feature_mdl

    ! Computes the spin correlation matrix according to arXiv:1002.2581 Eq. (2.8)

    integer,                    intent(in)  :: npr,j
    real(dp),                   intent(in)  :: p(0:,:)
    character(len=*), optional, intent(in)  :: ord
    real(dp), optional,         intent(out) :: A2scm(0:3,0:3)
    logical, optional,          intent(out) :: momenta_check

    integer      :: pr,legs,oi2
    character(2) :: t2
    character(3) :: order

    call processes_not_generated_error_rcl('compute_spin_correlation_matrix_rcl')

    call get_pr(npr, 'compute_spin_correlation_matrix_rcl', pr)

    if (.not. present(ord)) then
      order = 'LO'
    else
      if (ord .ne. 'LO' .and. ord .ne. 'NLO') then
        call error_rcl('compute_spin_correlation_matrix_rcl called with wrong order. '// &
                       "Accepted values are: order = 'LO','NLO'")
      endif
      order = ord
    end if

    legs = prs(pr)%legs

    t2 = get_particle_type2_mdl(prs(pr)%par(j))

    if ((t2 .ne.'v'.and. t2 .ne. 'g') .or. &
        j .le. 0 .or. j .gt. legs) then
      call error_rcl('compute_spin_correlation_matrix_rcl called with wrong index')
    endif

    if (size(p,2) .ne. legs .or. size(p,1) .ne. 4) then
      call error_rcl('compute_spin_correlation_matrix_rcl called with wrong momenta '//&
                     'dimension')
    endif

    call set_momenta (pr,p)
    if (present(momenta_check)) momenta_check = momcheck

    if (writeCor.ge.1) then
      call print_process_and_momenta (pr)
    endif

    call compute_amplitude (pr,order)

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,order)
    end if

    if (writeMat.ge.1) call print_amplitude (pr,order)

    if (order .eq. 'LO') then
      call compute_squared_amplitude_scm(pr,j)
    else
      call error_rcl('compute_spin_correlation_matrix_rcl not supported at NLO.')
    end if

    if (writeCor.ge.1) call print_squared_amplitude_scm (pr,j,order)

    if (present(A2scm)) then
      A2scm = 0
      if (order .eq. 'LO') then
        if (prs(pr)%crosspr .ne. 0) then
          do oi2 = 1, oi2Size(prs(pr)%crosspr)
            A2scm(0:3,0:3) = A2scm(0:3,0:3) + matrix2scm(0:3,0:3,oi2,pr)
          end do
        else
          do oi2 = 1, oi2Size(pr)
            A2scm(0:3,0:3) = A2scm(0:3,0:3) + matrix2scm(0:3,0:3,oi2,pr)
          end do
        end if
      else
        ! TODO:  <28-12-18, J.-N. Lang> !
      end if
    end if

  end subroutine compute_spin_correlation_matrix_rcl

!------------------------------------------------------------------------------!

  subroutine rescale_spin_correlation_matrix_rcl(npr,j,ord,A2scm)

    use modelfile, only: has_feature_mdl

    ! This subroutine adjusts the results calculated by
    ! compute_spin_correlation_matrix_rcl for a new value of alpha_s,
    ! rescaling the structure-dressed helicity amplitudes and
    ! recomputing the spin-correlated matrix summed squared amplitude for
    ! the process with process number "npr".
    ! "j" and "A2scm" are the same has for
    ! compute_spin_correlation_matrix_rcl.

    integer,                    intent(in)  :: npr,j
    character(len=*), optional, intent(in)  :: ord
    real(dp), optional,         intent(out) :: A2scm(0:3,0:3)

    integer      :: pr,legs,oi2
    character(2) :: t2
    character(3) :: order

    if (.not. (qcd_rescaling .and. has_feature_mdl('qcd_rescaling'))) then
      call error_rcl('Rescaling process not allowed: ' // &
                     'qcd_rescaling not enabled.', &
                     where='rescale_spin_correlation_matrix_rcl')
    end if

    if (.not. present(ord)) then
      order = 'LO'
    else
      call order_error_rcl(ord, 'rescale_spin_correlation_matrix_rcl')
      order = ord
    end if

    call processes_not_generated_error_rcl('rescale_spin_correlation_matrix_rcl')

    call get_pr(npr, 'rescale_spin_correlation_matrix_rcl', pr)

    legs = prs(pr)%legs

    t2  = get_particle_type2_mdl(prs(pr)%par(j))
    if ( (t2 .ne. 'v' .and. t2 .ne. 'g') .or. j .le. 0 .or. j .gt. legs ) then
      call error_rcl('Wrong index j.', &
                     where='rescale_spin_correlation_matrix_rcl')
    endif

    if (writeCor.ge.1) call print_rescaling
    if (writeCor.ge.1) call print_parameters_change

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call rescale_amplitude (pr,order)
    end if

    if (writeMat.ge.1) call print_amplitude (pr,order)

    if (order .eq. 'LO') then
      call compute_squared_amplitude_scm (pr,j)
    else
      ! TODO:  <29-12-18, J.-N. Lang> !
    end if

    if (writeCor.ge.1) call print_squared_amplitude_scm (pr,j,order)

    if (present(A2scm)) then
      A2scm = 0
      if (order .eq. 'LO') then
        if (prs(pr)%crosspr .ne. 0) then
          do oi2 = 1, oi2Size(prs(pr)%crosspr)
            A2scm(0:3,0:3) = A2scm(0:3,0:3) + matrix2scm(0:3,0:3,oi2,pr)
          end do
        else
          do oi2 = 1, oi2Size(pr)
            A2scm(0:3,0:3) = A2scm(0:3,0:3) + matrix2scm(0:3,0:3,oi2,pr)
          end do
        end if
      else
        ! TODO:  <28-12-18, J.-N. Lang> !
      end if
    end if

  end subroutine rescale_spin_correlation_matrix_rcl

!------------------------------------------------------------------------------!

  subroutine get_spin_correlation_matrix_general_rcl(npr,pow,ord,A2scm)

    ! This subroutine extracts the computed value of the
    ! spin-correlated matrix summed squared amplitude with "pow" power
    ! of alpha_s for the process with process number "npr".
    ! This routine must be called after compute_spin_correlation_matrix_rcl
    ! or rescale_spin_correlation_matrix_rcl for process with process number
    ! "npr".

    integer,                    intent(in)  :: npr
    integer, dimension(:),      intent(in)  :: pow
    character(len=*), optional, intent(in)  :: ord
    real(dp),                   intent(out) :: A2scm(0:3,0:3)

    integer      :: pr,oi2
    character(3) :: order

    call processes_not_generated_error_rcl('get_spin_correlation_matrix_rcl')

    call get_pr(npr, 'get_spin_correlation_matrix_rcl', pr)

    if (.not. present(ord)) then
      order = 'LO'
    else
      call order_error_rcl(ord, 'get_spin_correlation_matrix_rcl')
      order = ord
    end if

    if (prs(pr)%crosspr .ne. 0) then
      call get_oi2(pow, prs(pr)%crosspr, 'get_spin_correlation_matrix_rcl', oi2)
    else
      call get_oi2(pow, pr, 'get_spin_correlation_matrix_rcl', oi2)
    end if

    A2scm = 0d0
    if (order .eq. 'LO') then
      if (.not. allocated(matrix2scm)) &
        call error_rcl('No spin correlation computed yet.', &
                       where='get_spin_correlation_matrix_rcl')
      if (oi2 .ne. -1) A2scm = matrix2scm(:,:,oi2,pr)
    else
      ! TODO:  <29-12-18, J.-N. Lang> !
      !if (.not. allocated(matrix2scnlom)) &
      !  call error_rcl('No NLO spin correlation computed yet.', &
      !                 where='get_spin_correlation_general_rcl')
      !if (oi2 .ne. -1) A2sc = matrix2scnlo(oi2,pr)
    end if

  end subroutine get_spin_correlation_matrix_general_rcl

!------------------------------------------------------------------------------!

  subroutine get_momenta_rcl(npr,p)

    ! This subroutine extracts the stored momenta of the process with
    ! process number "npr"

    ! This routine must be called after compute_process_rcl,
    ! compute_colour_correlation_rcl,
    ! compute_all_colour_correlations_rcl,
    ! compute_spin_colour_correlation_rcl or
    ! compute_spin_correlation_rcl for process with process number
    ! "npr".

    integer,  intent(in)  :: npr
    real(dp), intent(out) :: p(0:,:)
    integer :: pr,legs

    call processes_not_generated_error_rcl('get_momenta_rcl')

    call get_pr(npr, 'get_momenta_rcl', pr)
    legs = prs(pr)%legs

    if (size(p,2) .ne. legs .or. size(p,1) .ne. 4) then
      call error_rcl('get_momenta_rcl called with wrong momenta dimensions.')
    endif
    p(0:3,1:legs) = momenta(0:3,1:legs)

  end subroutine get_momenta_rcl

!------------------------------------------------------------------------------!

  subroutine set_TIs_required_accuracy_rcl(acc)

    use collier_interface_rcl, only: SetReqAcc_cll

    ! This subroutine sets the required accuracy for TIs to the value
    ! "acc" in collier.

    real(dp), intent(in) :: acc

    call SetReqAcc_cll(acc)

  end subroutine set_TIs_required_accuracy_rcl

!------------------------------------------------------------------------------!

  subroutine get_TIs_required_accuracy_rcl(acc)

    use collier_interface_rcl, only: GetReqAcc_cll

    ! This subroutine extracts the value of the required accuracy
    ! for TIs from collier and returns it as value of the output
    ! variable "acc".

    real(dp), intent(out) :: acc

    real(dp) :: cllreqacc

    call GetReqAcc_cll (cllreqacc)

    acc = cllreqacc

  end subroutine get_TIs_required_accuracy_rcl

!------------------------------------------------------------------------------!

  subroutine set_TIs_critical_accuracy_rcl(acc)

    use collier_interface_rcl, only: SetCritAcc_cll

    ! This subroutine sets the critical accuracy for TIs to the value
    ! "acc" in collier.

    real(dp), intent(in) :: acc

    call SetCritAcc_cll  (acc)

  end subroutine set_TIs_critical_accuracy_rcl

!------------------------------------------------------------------------------!

  subroutine get_TIs_critical_accuracy_rcl(acc)

    use collier_interface_rcl, only: GetCritAcc_cll

    ! This subroutine extracts the value of the critical accuracy
    ! for TIs from collier and returns it as value of the
    ! output variable "acc".

    real(dp), intent(out) :: acc

    real(dp) :: cllcritacc

    call GetCritAcc_cll (cllcritacc)

    acc = cllcritacc

  end subroutine get_TIs_critical_accuracy_rcl

!------------------------------------------------------------------------------!

  subroutine get_TIs_accuracy_flag_rcl(flag)

    use collier_interface_rcl, only: GetAccFlag_cll

    ! This subroutine extracts the value of the accuracy flag for TIs
    ! from collier. The output variable "flag" returns global
    ! information on the accuracy of the TIs evaluated in the last call
    ! of compute_process_rcl:
    ! - "flag = 0":
    !   For all TIs, the accuracy is estimated to be better than the
    !   required value.
    ! - "flag = -1":
    !   For at least one TI, the accuracy is estimated to be worse than
    !   the required value, but for all TIs, the accuracy is estimated
    !   to be better than the critical value.
    ! - "flag = -2":
    !   For at least one TI, the accuracy is estimated to be worse than
    !   the critical values.
    ! The value of variable "flag" is determined based on internal
    ! uncertainty estimations performed by collier.

    integer, intent(out) :: flag

    integer  :: cllaccflag

    call GetAccFlag_cll (cllaccflag)

    flag = cllaccflag

  end subroutine get_TIs_accuracy_flag_rcl

!------------------------------------------------------------------------------!

  subroutine get_n_colour_configurations_rcl(npr,n)

    ! This subroutine returns the number of existing colour configurations for
    ! the process with process number "npr".

    integer, intent(in)  :: npr
    integer, intent(out) :: n
    integer :: pr

    call processes_not_generated_error_rcl('get_n_colour_configurations_rcl')

    call get_pr(npr, 'get_n_colour_configurations_rcl', pr)

    n = csTot(pr)

  end subroutine get_n_colour_configurations_rcl

!------------------------------------------------------------------------------!

  subroutine get_colour_configuration_rcl(npr,n,col)

    ! This subroutine writes the n-th colour configuration of the process with
    ! process number "npr"  to the variable "col(1:l)" (l being total number of
    ! external legs of the process).
    ! "col(1:l)" is a vector
    ! of type integer and length l, where each position in the vector
    ! corresponds to one of the l external particles of process "npr"
    ! (ordered as in the process definition).
    ! For colourless particles, incoming quarks and outgoing
    ! anti-quarks, the corresponding entry in "col" is 0.
    ! For all other particles (gluons, outgoing quarks and incoming
    ! anti-quarks), the entries are permutations, without repetition,
    ! of the positions of the gluons, incoming quarks or outgoing
    ! anti-quarks in the process definition.
    ! "col(1:l)" can be used as the input vaiable "colour" in the
    ! subroutine get_amplitude_rcl.

    integer, intent(in)  :: npr,n
    integer, intent(out) :: col(:)
    integer :: pr,j,legs

    call processes_not_generated_error_rcl('get_colour_configuration_rcl')

    call get_pr(npr, 'get_colour_configuration_rcl', pr)

    legs = prs(pr)%legs

    if (size(col) .ne. legs) then
      call error_rcl('col has wrong length.', &
                     where='get_colour_configuration_rcl')
    end if
    if (n .le. 0 .or. n .gt. csTot(pr)) then
      call error_rcl('Invalid colour configuration requested.', &
                     where='get_colour_configuration_rcl')
    endif

    do j = 1,legs
      if (csIq(j,n,pr).ne.0) then
        col(oldleg(j,pr)) = oldleg(csIq(j,n,pr),pr)
      else
        col(oldleg(j,pr)) = 0
      endif
    enddo

  end subroutine get_colour_configuration_rcl

!------------------------------------------------------------------------------!

  subroutine get_colour_configurations_rcl(npr, cols)

    ! This subroutine extracts all colour configurations of the
    ! process with process number "npr" and write it in the output
    ! variable "cols(1:l,1:csTot)" (csTot is the total number of
    ! colour configuations of the process "npr" and l is its total
    ! number of external legs).
    ! "cols(1:l,n)" describes the n^th colour structure and is a vector
    ! of type integer and length l, where each position in the vector
    ! corresponds to one of the l external particles of process "npr"
    ! (ordered as in the process definition).
    ! For colourless particles, incoming quarks and outgoing
    ! anti-quarks, the corresponding entry in "cols" is 0.
    ! For all other particles (gluons, outgoing quarks and incoming
    ! anti-quarks), the entries are permutations, without repetition,
    ! of the positions of the gluons, incoming quarks or outgoing
    ! anti-quarks in the process definition.
    ! "cols(1:l,n)" can be used as the input vaiable "colour" in the
    ! subroutine get_amplitude_rcl.

    integer, intent(in) :: npr
    integer, intent(out), allocatable :: cols(:,:)
    integer :: pr,i,j,legs

    call processes_not_generated_error_rcl('get_colour_configurations_rcl')

    call get_pr(npr, 'get_colour_configurations_rcl', pr)

    legs = prs(pr)%legs

    allocate(cols(1:legs,1:csTot(pr)))

    do i = 1, csTot(pr)
      do j = 1,legs
        if (csIq(j,i,pr).ne.0) then
          cols(oldleg(j,pr),i) = oldleg(csIq(j,i,pr),pr)
        else
          cols(oldleg(j,pr),i) = 0
        endif
      enddo
    enddo

  end subroutine get_colour_configurations_rcl

!------------------------------------------------------------------------------!

  subroutine get_n_helicity_configurations_rcl(npr,n)

    ! This subroutine returns the number of existing helicity configurations
    ! for the process with process number "npr".

    integer, intent(in)  :: npr
    integer, intent(out) :: n
    integer :: pr

    call processes_not_generated_error_rcl('get_n_helicity_configurations_rcl')

    call get_pr(npr, 'get_n_helicity_configurations_rcl', pr)

    n = cfTot(pr)

  end subroutine get_n_helicity_configurations_rcl

!------------------------------------------------------------------------------!

  subroutine get_helicity_configuration_rcl(npr,n,hel)

    ! This subroutine writes the n-th helicity configuration of the
    ! process with process number "npr" to the variable "hel(1:l)" (l being
    ! the total number of external legs of the process).
    ! "hel(1:l)" is a vector of type integer and length l, where each position
    ! in the vectorcorresponds to one of the l external particles of process
    ! "npr" (ordered as in the process definition).
    ! Its entries are the helicities of the particles:
    !   left-handed  fermions/antifermions -> -1
    !   right-handed fermions/antifermions -> +1
    !   logitudinal vector bosons          ->  0
    !   transverse vector bosons           -> -1, +1
    !   scalar particles                   ->  0
    ! Example:
    !   Process 'u g -> W+ d'
    !   "hel(1:4)" = (/-1,+1,-1,-1/) means
    !   left-handed up-quark
    !   transverse gluon (helicity = +1)
    !   transverse W+ (helicity = -1)
    !   left-handed down-quark
    ! "hel(1:l)" can be used as the input vaiable "hel" in the subroutines
    ! get_amplitude_rcl and get_polarized_squared_amplitude_rcl.

    integer, intent(in) :: npr,n
    integer, intent(out) :: hel(:)
    integer :: pr,j,legs

    call processes_not_generated_error_rcl('get_helicity_configuration_rcl')

    call get_pr(npr, 'get_helicity_configuration_rcl', pr)

    legs = prs(pr)%legs

    if (size(hel) .ne. legs) then
      call error_rcl('hel has wrong length.', &
                     where='get_helicity_configuration_rcl')
    end if
    if (n .le. 0 .or. n .gt. cfTot(pr)) then
      call error_rcl('Invalid helicity configuration requested.', &
                     where='get_helicity_configuration_rcl')
    endif

    do j = 1,prs(pr)%legsIn
      hel(j) = he(newleg(j,pr),n,pr)
    enddo
    do j = prs(pr)%legsIn+1,legs
      hel(j) = - he(newleg(j,pr),n,pr)
    enddo

  end subroutine get_helicity_configuration_rcl

!------------------------------------------------------------------------------!

  subroutine get_helicity_configurations_rcl(npr,hels)

    ! This subroutine extracts all helicity configurations of the
    ! process with process number "npr" and write it in the output
    ! variable "hels(1:l,1:cfTot)" (cfTot is the total number of
    ! helicity configuations of the process "npr" and l is its total
    ! number of external legs).
    ! "hels(1:l,n)" describes the n^th helicity configuration and is a
    ! vector of type integer and length l, where each position in the
    ! vectorcorresponds to one of the l external particles of process
    ! "npr" (ordered as in the process definition).
    ! Its entries are the helicities of the particles:
    !   left-handed  fermions/antifermions -> -1
    !   right-handed fermions/antifermions -> +1
    !   logitudinal vector bosons          ->  0
    !   transverse vector bosons           -> -1, +1
    !   scalar particles                   ->  0
    ! Example:
    !   Process 'u g -> W+ d'
    !   "hels(1:4,n)" = (/-1,+1,-1,-1/) means
    !   left-handed up-quark
    !   transverse gluon (helicity = +1)
    !   transverse W+ (helicity = -1)
    !   left-handed down-quark
    ! "hels(1:l,n)" can be used as the input vaiable "hel" in the
    ! subroutines get_amplitude_rcl and
    ! get_polarized_squared_amplitude_rcl.

    integer, intent(in) :: npr
    integer, intent(inout), allocatable :: hels(:,:)
    integer :: pr,i,j,legs

    call processes_not_generated_error_rcl('get_helicity_configurations_rcl')

    call get_pr(npr, 'get_helicity_configurations_rcl', pr)

    legs = prs(pr)%legs

    allocate(hels(1:legs,1:cfTot(pr)))

    do i = 1, cfTot(pr)
      do j = 1,prs(pr)%legsIn
        hels(j,i) = he(newleg(j,pr),i,pr)
      enddo
      do j = prs(pr)%legsIn+1,legs
        hels(j,i) = - he(newleg(j,pr),i,pr)
      enddo
    enddo

  end subroutine get_helicity_configurations_rcl

!------------------------------------------------------------------------------!

end module process_computation_rcl

!------------------------------------------------------------------------------!
