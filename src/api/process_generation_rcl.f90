!******************************************************************************!
!                                                                              !
!    process_generation_rcl.f90                                                !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module process_generation_rcl

!------------------------------------------------------------------------------!

  use globals_rcl
  use tables_rcl, only: tensor_tables,binary_tables,qcd_rescaling_tables
  use currents_rcl, only: generate_currents
  use input_rcl, only: get_delta_uv_rcl,get_delta_ir_rcl,         &
                       get_mu_uv_rcl,get_mu_ir_rcl,get_mu_ms_rcl, &
                       set_delta_uv_rcl,set_delta_ir_rcl,         &
                       set_mu_uv_rcl,set_mu_ir_rcl
  use modelfile, only: is_particle_model_init_mdl,  &
                       set_particle_model_init_mdl, &
                       init_particles_mdl,          &
                       get_modelname_mdl,           &
                       get_modelgauge_mdl,          &
                       has_feature_mdl,             &
                       get_driver_timestamp_mdl,    &
                       print_parameters_mdl,        &
                       print_renoschemes_mdl,       &
                       print_counterterms_mdl
  use collier_interface_rcl, only: initialize_collier

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine generate_processes_rcl
    ! Constructs the the processes defined via
    ! :f:subr:`define_process_rcl`. It can be
    ! called once after all processes are defined and before calling
    ! :ref:`process computation subroutines<process_computation>`.

    real(sp) :: timeGENin,timeGENout
    real(dp) :: DeltaUV,DeltaIR,DeltaIR2,muUV,muIR
    character(len=100) :: modelname
    character(len=200) :: driver

    call get_delta_uv_rcl(DeltaUV)
    call get_delta_ir_rcl(DeltaIR,DeltaIR2)
    call get_mu_uv_rcl(muUV)
    call get_mu_ir_rcl(muIR)

    if (selfenergy_offshell_setup .and. legsMax .gt. 2) then
      call error_rcl('Cannot generate processes with legs > 2' // &
                     'and Selfenergy offshell setup')
    end if

    if (.not. is_particle_model_init_mdl()) then
      call init_particles_mdl()
      call set_particle_model_init_mdl(.true.)
    end if

    if (crossing_symmetry) then
      call check_crossing
    end if

    call cpu_time (timeGENin)
    call binary_tables
    if (loopMax) call tensor_tables

    if (print_model_external_parameters) then
      call openOutput
      write(nx,'(1x,75("-"))')
      modelname = get_modelname_mdl()
      write(nx,'(2x,a,x,a)') "Active model:    ", adjustl(trim(modelname))
      modelname = get_modelgauge_mdl()
      write(nx,'(2x,a,x,a)') "Gauge:           ", adjustl(trim(modelname))
      driver = get_driver_timestamp_mdl()
      write(nx,'(2x,a,x,a)') "Model generation:", adjustl(trim(driver))
      write(nx,'(1x,75("-"))')
      if (dynamic_settings .gt. 0) then
        write(nx,'(2x,a)') "Dynamic settings: Process initialisation with"
      end if
      write(nx,'(2x,a,g21.14,7x,a,g21.14,a)') &
      'Delta_UV   =',DeltaUV,'mu_UV =',muUV,' GeV'
      write(nx,'(2x,a,g21.14)') &
      'Delta_IR^2 =',DeltaIR2
      write(nx,'(2x,a,g21.14,7x,a,g21.14,a)') &
      'Delta_IR   =',DeltaIR,'mu_IR =',muIR,' GeV'
      if (dynamic_settings .gt. 0) then
        write(nx,'(2x,a)') "Overwritten by"
        write(nx,'(2x,a,g21.14,7x,a,g21.14,a)') &
        'Delta_UV   =',DeltaUV_cache,'mu_UV =',muUV_cache,' GeV'
        write(nx,'(2x,a,g21.14)') &
        'Delta_IR^2 =',DeltaIR2_cache
        write(nx,'(2x,a,g21.14,7x,a,g21.14,a)') &
        'Delta_IR   =',DeltaIR_cache,'mu_IR =',muIR_cache,' GeV'
      end if

      if(print_model_internal_parameters) then
        call print_parameters_mdl(1)
      else
        call print_parameters_mdl(0)
      end if
      call print_renoschemes_mdl
      if (use_active_qmasses) then
        write(nx,'(2x,a)') 'Quark masses in the running of alpha_s [GeV]:'
        write(nx,'(2x,a,g21.14,g21.14)') 'm_s^2 =',mq2(3)
        write(nx,'(2x,a,g21.14,g21.14)') 'm_c^2 =',mq2(4)
        write(nx,'(2x,a,g21.14,g21.14)') 'm_b^2 =',mq2(5)
        write(nx,'(2x,a,g21.14,g21.14)') 'm_t^2 =',mq2(6)
        write(nx,'(1x,75("-"))')
      end if
    end if

    if (.not. has_feature_mdl('sm_generation_opt')) sm_generation_opt = .false.

    call generate_currents

    call cpu_time (timeGENout)

    timeGEN = timeGENout - timeGENin

    allocate (timeTI(prTot)); timeTI = 0e0
    allocate (timeTC(prTot)); timeTC = 0e0
    allocate (timeHS(prTot)); timeHS = 0e0

    if (loopMax .and. init_collier) call initialize_collier

    if (qcd_rescaling .and. has_feature_mdl('qcd_rescaling')) then
      call qcd_rescaling_tables
    end if

    if (print_model_counterterms) then
      call print_counterterms_mdl
      write(nx,*)
    end if

    processes_generated = .true.

    if (dynamic_settings .ge. 1) then
      call set_mu_uv_rcl(muUV_cache)
      call set_mu_ir_rcl(muIR_cache)
      call set_delta_uv_rcl(DeltaUV_cache)
      call set_delta_ir_rcl(DeltaIR_cache,DeltaIR2_cache)
    end if

    if (print_process_generation_summary) then
      call process_generation_summary
      write(nx,*)
    end if

  contains

  subroutine check_crossing
    ! Checks whether crossing symmetry can be applied for the registered
    ! processes.
    integer, allocatable :: pa_srt(:),pa_srt_tmp(:),relperm(:)
    integer :: i,j,le
    logical :: check

    do i = 2, prTot
      do j = 1, i-1
        if ((prs(i)%legs .ne. prs(j)%legs)) cycle
        check = .true.
        check = check .and. (prs(i)%crosspr .eq. 0)
        check = check .and. (prs(i)%loop .eqv. prs(j)%loop)
        check = check .and. (sum(abs(prs(i)%powsel - prs(j)%powsel)) .eq. 0)
        check = check .and. (sum(prs(i)%qflow) .eq. 0)
        check = check .and. (sum(prs(j)%qflow) .eq. 0)
        check = check .and. (sum(prs(i)%hel) .eq. prs(i)%legs*111)
        check = check .and. (sum(prs(j)%hel) .eq. prs(j)%legs*111)
        check = check .and. (sum(prs(i)%parRes) .eq. 0)
        check = check .and. (sum(prs(j)%parRes) .eq. 0)
        if (check) then
          le = size(prs(i)%relperm)
          allocate(pa_srt(le),pa_srt_tmp(le),relperm(le))
          call interchange_sort(prs(i)%par(:le),pa_srt)
          call interchange_sort(prs(j)%par(:le),pa_srt_tmp)
          ! TODO:  <31-07-18, J.-N. Lang> !
          ! check for rel. helcity, resonances and qflow
          if (sum(abs(pa_srt - pa_srt_tmp)) .eq. 0) then
            call relative_permutation(prs(i)%par(:le),prs(j)%par(:le),relperm)
            prs(i)%crosspr = j
            prs(i)%relperm(:) = relperm(:)
            deallocate(pa_srt,pa_srt_tmp,relperm)
            exit
          end if
          deallocate(pa_srt,pa_srt_tmp,relperm)
        end if
      end do
    end do
  end subroutine check_crossing

  subroutine interchange_sort(list,list_srt)
    integer, dimension(:), intent(in) :: list
    integer, dimension(:), intent(inout) :: list_srt
    integer :: i,j,temp

    list_srt(:) = list(:)
    do i = 1, size(list)-1
      do j = i+1, size(list)
        if (list_srt(i) .gt. list_srt(j)) then
          temp = list_srt(i)
          list_srt(i) = list_srt(j)
          list_srt(j) = temp
        end if
      end do
    end do
  end subroutine interchange_sort

  subroutine relative_permutation(list1,list2,order)
    integer, dimension(:), intent(in)  :: list1,list2
    integer, dimension(:), intent(out) :: order
    integer :: i,j

    order = 0
    jloop: do j = 1, size(list2)
      do i = 1, size(list1)

        if (list1(i) .eq. list2(j) .and. order(i) .eq. 0) then
          order(i) = j
          cycle jloop
        end if
      end do

    end do jloop
  end subroutine relative_permutation

  subroutine process_generation_summary

    integer np,pr

    call openOutput

    write(nx,'(1x,a)') 'Process generation summary:'
    np = prTot
    if (np .eq. 1) then
      write(nx,'(1x,a)') trim(adjustl(to_str(np))) // ' process defined'
    else
      write(nx,'(1x,a)') trim(adjustl(to_str(np))) // ' processes defined'
    end if

    do pr = 1, np
      write(nx,'(2x,a)') ''
      if (prs(pr)%crosspr .gt. 0) then
        write(nx,'(2x,a)') "Process " // &
          trim(adjustl(int_to_str(prs(pr)%inpr))) // &
          " related to " // &
          trim(adjustl(int_to_str(prs(prs(pr)%crosspr)%inpr))) // &
          " via crossing symmetry."
      else
        write(nx,'(2x,a)') "Process " // trim(adjustl(int_to_str(prs(pr)%inpr))) // &
                           ": "// trim(adjustl(prs(pr)%process))
        write(nx,'(2x,a)') 'Tree currents    = ' // trim(adjustl(to_str(w0Tot(pr))))
        write(nx,'(2x,a)') 'Tree branches    = ' // trim(adjustl(to_str(pm(pr)%bm0Tot)))
        if (prs(pr)%loop) then
          write(nx,'(2x,a)') 'Loop currents    = ' // trim(adjustl(to_str(w1Tot(pr))))
          write(nx,'(2x,a)') 'Loop branches    = ' // trim(adjustl(to_str(pm(pr)%bm1Tot)))
        end if

        if (prs(pr)%loop) then
          write(nx,'(2x,a)') 'Tensor integrals = ' // trim(adjustl(to_str(tiTot(pr))))
        end if
        write(nx,'(2x,a)') 'Helicities       = ' // trim(adjustl(to_str(cfTot(pr))))
        if (csTot(pr) .ne. pCsTot(pr)) then
          write(nx,'(2x,a)') 'Colourflows      = ' // trim(adjustl(to_str(csTot(pr)))) &
              // ' ('// trim(adjustl(to_str(pCsTot(pr)))) // ' computed)'
        else
          write(nx,'(2x,a)') 'Colourflows      = ' // trim(adjustl(to_str(csTot(pr))))
        end if
      end if
    end do
    write(nx,'(1x,75("-"))')

  end subroutine process_generation_summary

!------------------------------------------------------------------------------!

  end subroutine generate_processes_rcl

!------------------------------------------------------------------------------!

  subroutine process_exists_rcl(npr,exists)
    ! returns whether a process with id "npr" exists after genreation.
    integer, intent(in)  :: npr
    logical, intent(out) :: exists
    integer              :: i,pr,crosspr

    call processes_not_generated_error_rcl('process_exists_rcl')
    pr = 0
    do i = 1,prTot
      if (prs(i)%inpr .eq. npr) then
        pr = i; exit
      endif
    enddo
    if (pr .eq. 0) then
      call error_rcl('Undefined process index '//to_str(npr),&
                     where='process_exists_rcl')
    endif
    if (prs(pr)%crosspr .ne. 0) then
      crosspr = prs(pr)%crosspr
    else
      crosspr = pr
    end if
    exists = prs(crosspr)%exists

  end subroutine process_exists_rcl


end module process_generation_rcl

!------------------------------------------------------------------------------!
