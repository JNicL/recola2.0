!******************************************************************************!
!                                                                              !
!    recola1_interface_rcl.f90                                                 !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module recola1_interface_rcl

!------------------------------------------------------------------------------!

  use globals_rcl, only: get_pr,error_rcl,warning_rcl,    &
                         cone,cnul,cima,pi,dp,            &
                         zeroLO,processes_generated,      &
                         use_active_qmasses,              &
                         check_support_models,            &
                         processes_generated_warning_rcl, &
                         processes_generated_error_rcl
  use input_rcl, only: set_parameter_rcl,        &
                       get_parameter_rcl,        &
                       set_renoscheme_rcl,       &
                       set_light_particle_rcl,   &
                       unset_light_particle_rcl, &
                       set_mu_ms_rcl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine check_support_sm_interface(where)
    use modelfile, only: has_feature_mdl

    character(len=*), intent(in) :: where

    if (.not. has_feature_mdl('sm_parameters')) &
      call check_support_models('SM','THDM','HS','TGC',where)

  end subroutine check_support_sm_interface

!------------------------------------------------------------------------------!

  subroutine check_support_sm_fund_interface(where)

    character(len=*), intent(in) :: where

    call check_support_models('SM','THDM','HS',where)

  end subroutine check_support_sm_fund_interface

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_w_rcl(m,g)

    ! This subroutine sets the pole mass and width
    ! of the W boson to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_sm_interface('set_pole_mass_w_rcl')
    call processes_generated_warning_rcl('set_pole_mass_w_rcl')

    if (m.le.0d0.or.g.lt.0d0) call error_rcl('Wrong argument for pole mass.', &
                                             where='set_pole_mass_w_rcl')
    call set_parameter_rcl("MW", cone*m)
    call set_parameter_rcl("WW", cone*g)

  end subroutine set_pole_mass_w_rcl

!------------------------------------------------------------------------------!

  subroutine set_onshell_mass_w_rcl(m,g)

    ! This subroutine sets the on-shell mass and width of the W boson
    ! to "m" and "g" respectively. It is done by setting the pole mass
    ! "mass_w" to "m/sqrt(1+g^2/m^2)" and the pole width "width_w" to
    ! "g/sqrt(1+g^2/m^2)".

    real(dp), intent(in) :: m,g

    call check_support_sm_interface('set_onshell_mass_w_rcl')
    call processes_generated_warning_rcl('set_onshell_mass_w_rcl')
    if (m.le.0d0.or.g.lt.0d0) call error_rcl('Wrong argument for pole mass.', &
                                             where='sset_onshell_mass_w_rcl')

    call set_pole_mass_w_rcl(m/sqrt(1d0 + g**2/m**2),g/sqrt(1d0 + g**2/m**2))

  end subroutine set_onshell_mass_w_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_z_rcl(m,g)

    ! This subroutine sets the pole mass and width
    ! of the Z boson to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_sm_interface('set_pole_mass_z_rcl')
    call processes_generated_warning_rcl('set_pole_mass_z_rcl')
    if (m.le.0d0.or.g.lt.0d0) call error_rcl('Wrong argument for pole mass.', &
                                             where='set_pole_mass_z_rcl')
    call set_parameter_rcl("MZ", cone*m)
    call set_parameter_rcl("WZ", cone*g)

  end subroutine set_pole_mass_z_rcl

!------------------------------------------------------------------------------!

  subroutine set_onshell_mass_z_rcl(m,g)

    ! This subroutine sets the on-shell mass and width of the Z boson
    ! to "m" and "g" respectively. This is done by setting the pole mass
    ! "mass_z" to "m/sqrt(1+g^2/m^2)" and the pole width "width_z" to
    ! "g/sqrt(1+g^2/m^2)".

    real(dp), intent(in) :: m,g

    call check_support_sm_interface('set_onshell_mass_z_rcl')
    call processes_generated_warning_rcl('set_onshell_mass_z_rcl')

    if (m.le.0d0.or.g.lt.0d0) call error_rcl('Wrong argument for pole mass.', &
                                             where='set_pole_mass_z_rcl')

    call set_pole_mass_z_rcl(m/sqrt(1d0 + g**2/m**2),g/sqrt(1d0 + g**2/m**2))
  end subroutine set_onshell_mass_z_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_h_rcl(m,g)

    ! This subroutine sets the pole mass and width
    ! of the h boson to "m" and "g" respectively.

    use modelfile, only: has_feature_mdl

    real(dp), intent(in) :: m,g

    if (.not. has_feature_mdl('sm_parameters')) &
      call check_support_models('SM','TGC','set_pole_mass_h_rcl')
    call processes_generated_warning_rcl('set_pole_mass_h_rcl')

    call set_parameter_rcl("MH", cone*m)
    call set_parameter_rcl("WH", cone*g)

  end subroutine set_pole_mass_h_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_top_rcl(m,g)

    ! This subroutine sets the pole mass and width
    ! of the top quark to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_sm_interface('set_pole_mass_top_rcl')
    call processes_generated_warning_rcl('set_pole_mass_top_rcl')

    call set_parameter_rcl("MT", cone*m)
    call set_parameter_rcl("WT", cone*g)

  end subroutine set_pole_mass_top_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_bottom_rcl(m,g)

    ! This subroutine sets the pole mass and width
    ! of the bottom quark to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_sm_interface('set_pole_mass_bottom_rcl')
    call processes_generated_warning_rcl('set_pole_mass_bottom_rcl')

    call set_parameter_rcl("MB", cone*m)
    call set_parameter_rcl("WB", cone*g)

  end subroutine set_pole_mass_bottom_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_charm_rcl(m,g)

    ! This subroutine sets the pole mass and width
    ! of the charm quark to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_sm_interface('set_pole_mass_charm_rcl')
    call processes_generated_warning_rcl('set_pole_mass_charm_rcl')

    call set_parameter_rcl("MC", cone*m)
    call set_parameter_rcl("WC", cone*g)

  end subroutine set_pole_mass_charm_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_strange_rcl(m)

    ! This subroutine sets the pole mass
    ! of the strange quark to "m".

    real(dp), intent(in) :: m

    call check_support_sm_interface('set_pole_mass_strange_rcl')
    call processes_generated_warning_rcl('set_pole_mass_strange_rcl')

    call set_parameter_rcl("MS", cone*m)

  end subroutine set_pole_mass_strange_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_up_rcl(m)

    ! This subroutine sets the pole mass of the up quark to "m".

    real(dp), intent(in) :: m

    call check_support_sm_interface('set_pole_mass_up_rcl')
    call processes_generated_warning_rcl('set_pole_mass_up_rcl')

    call set_parameter_rcl("MU", cone*m)

  end subroutine set_pole_mass_up_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_down_rcl(m)

    ! This subroutine sets the pole mass
    ! of the down quark to "m".

    real(dp), intent(in) :: m

    call check_support_sm_interface('set_pole_mass_down_rcl')
    call processes_generated_warning_rcl('set_pole_mass_down_rcl')

    call set_parameter_rcl("MD", cone*m)

  end subroutine set_pole_mass_down_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_tau_rcl(m,g)

    ! This subroutine sets the pole mass and width
    ! of the tau to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_sm_interface('set_pole_mass_tau_rcl')
    call processes_generated_warning_rcl('set_pole_mass_tau_rcl')

    call set_parameter_rcl("MTA", cone*m)
    call set_parameter_rcl("WTA", cone*g)

  end subroutine set_pole_mass_tau_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_muon_rcl(m,g)

    ! This subroutine sets the pole mass and width
    ! of the muon to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_sm_interface('set_pole_mass_muon_rcl')
    call processes_generated_warning_rcl('set_pole_mass_muon_rcl')

    call set_parameter_rcl("MM", cone*m)
    call set_parameter_rcl("WM", cone*g)

  end subroutine set_pole_mass_muon_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_electron_rcl(m)

    ! This subroutine sets the pole mass
    ! of the electron to "m".

    real(dp), intent(in) :: m

    call check_support_sm_interface('set_pole_mass_electron_rcl')
    call processes_generated_warning_rcl('set_pole_mass_electron_rcl')

    call set_parameter_rcl("ME", cone*m)

  end subroutine set_pole_mass_electron_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_top_rcl

    ! This subroutine sets the top quark as light. This means that, if
    ! the top quark is massive, its collinear singularities are
    ! regularized with its mass and terms proportional to the mass are
    ! discarded. If the top quark is massless this subroutine has no
    ! effect (i.e. top quark collinear singularities are regularized
    ! dimensionally).

    call check_support_sm_interface('set_light_top_rcl')
    call processes_generated_warning_rcl('set_light_top_rcl')
    call set_light_particle_rcl('t')

  end subroutine set_light_top_rcl

!------------------------------------------------------------------------------!

  subroutine unset_light_top_rcl

    call check_support_sm_interface('unset_light_top_rcl')
    call processes_generated_warning_rcl('unset_light_top_rcl')
    call unset_light_particle_rcl('t')

  end subroutine unset_light_top_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_bottom_rcl

    ! This subroutine sets the bottom quark as light. This means that, if
    ! the bottom quark is massive, its collinear singularities are
    ! regularized with its mass and terms proportional to the mass are
    ! discarded. If the bottom quark is massless this subroutine has no
    ! effect (i.e. bottom quark collinear singularities are regularized
    ! dimensionally).

    call check_support_sm_interface('set_light_bottom_rcl')
    call processes_generated_warning_rcl('set_light_bottom_rcl')
    call set_light_particle_rcl('b')

  end subroutine set_light_bottom_rcl

!------------------------------------------------------------------------------!

  subroutine unset_light_bottom_rcl

    call check_support_sm_interface('unset_light_bottom_rcl')
    call processes_generated_warning_rcl('unset_light_bottom_rcl')
    call unset_light_particle_rcl('b')

  end subroutine unset_light_bottom_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_charm_rcl

    ! This subroutine sets the charm quark as light. This means that, if
    ! the charm quark is massive, its collinear singularities are
    ! regularized with its mass and terms proportional to the mass are
    ! discarded. If the charm quark is massless this subroutine has no
    ! effect (i.e. charm quark collinear singularities are regularized
    ! dimensionally).

    call check_support_sm_interface('set_light_charm_rcl')
    call processes_generated_warning_rcl('set_light_charm_rcl')
    call set_light_particle_rcl('c')

  end subroutine set_light_charm_rcl

!------------------------------------------------------------------------------!

  subroutine unset_light_charm_rcl

    call check_support_sm_interface('unset_light_charm_rcl')
    call processes_generated_warning_rcl('unset_light_charm_rcl')
    call unset_light_particle_rcl('d')

  end subroutine unset_light_charm_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_strange_rcl

    ! This subroutine sets the strange quark as light. This means that, if
    ! the strange quark is massive, its collinear singularities are
    ! regularized with its mass and terms proportional to the mass are
    ! discarded. If the strange quark is massless this subroutine has no
    ! effect (i.e. strange quark collinear singularities are regularized
    ! dimensionally).

    call check_support_sm_interface('set_light_strange_rcl')
    call processes_generated_warning_rcl('set_light_strange_rcl')
    call set_light_particle_rcl('s')

  end subroutine set_light_strange_rcl

!------------------------------------------------------------------------------!

  subroutine unset_light_strange_rcl

    call check_support_sm_interface('unset_light_strange_rcl')
    call processes_generated_warning_rcl('unset_light_strange_rcl')
    call unset_light_particle_rcl('s')

  end subroutine unset_light_strange_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_up_rcl

    ! This subroutine sets the up quark as light. This means that, if
    ! the up quark is massive, its collinear singularities are
    ! regularized with its mass and terms proportional to the mass are
    ! discarded. If the up quark is massless this subroutine has no
    ! effect (i.e. up quark collinear singularities are regularized
    ! dimensionally).

    call check_support_sm_interface('set_light_up_rcl')
    call processes_generated_warning_rcl('set_light_up_rcl')
    call set_light_particle_rcl('u')

  end subroutine set_light_up_rcl

!------------------------------------------------------------------------------!

  subroutine unset_light_up_rcl

    call check_support_sm_interface('unset_light_up_rcl')
    call processes_generated_warning_rcl('unset_light_up_rcl')
    call unset_light_particle_rcl('u')

  end subroutine unset_light_up_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_down_rcl

    ! This subroutine sets the down quark as light. This means that, if
    ! the down quark is massive, its collinear singularities are
    ! regularized with its mass and terms proportional to the mass are
    ! discarded. If the down quark is massless this subroutine has no
    ! effect (i.e. down quark collinear singularities are regularized
    ! dimensionally).

    call check_support_sm_interface('set_light_down_rcl')
    call processes_generated_warning_rcl('set_light_down_rcl')
    call set_light_particle_rcl('d')

  end subroutine set_light_down_rcl

!------------------------------------------------------------------------------!

  subroutine unset_light_down_rcl

    call check_support_sm_interface('unset_light_down_rcl')
    call processes_generated_warning_rcl('unset_light_down_rcl')
    call unset_light_particle_rcl('d')

  end subroutine unset_light_down_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_tau_rcl

    ! This subroutine sets the tau as light. This means that, if
    ! the tau is massive, its collinear singularities are
    ! regularized with its mass and terms proportional to the mass are
    ! discarded. If the tau is massless this subroutine has no
    ! effect (i.e. tau collinear singularities are regularized
    ! dimensionally).

    call check_support_sm_interface('set_light_tau_rcl')
    call processes_generated_warning_rcl('set_light_tau_rcl')
    call set_light_particle_rcl('tau-')

  end subroutine set_light_tau_rcl

!------------------------------------------------------------------------------!

  subroutine unset_light_tau_rcl

    call check_support_sm_interface('unset_light_tau_rcl')
    call processes_generated_warning_rcl('unset_light_tau_rcl')
    call unset_light_particle_rcl('tau-')

  end subroutine unset_light_tau_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_muon_rcl

    ! This subroutine sets the muon as light. This means that, if
    ! the muon is massive, its collinear singularities are
    ! regularized with its mass and terms proportional to the mass are
    ! discarded. If the muon is massless this subroutine has no
    ! effect (i.e. muon collinear singularities are regularized
    ! dimensionally).

    call check_support_sm_interface('set_light_muon_rcl')
    call processes_generated_warning_rcl('set_light_muon_rcl')
    call set_light_particle_rcl('mu-')

  end subroutine set_light_muon_rcl

!------------------------------------------------------------------------------!

  subroutine unset_light_muon_rcl

    call check_support_sm_interface('unset_light_muon_rcl')
    call processes_generated_warning_rcl('unset_light_muon_rcl')
    call unset_light_particle_rcl('mu-')

  end subroutine unset_light_muon_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_electron_rcl

    ! This subroutine sets the electron as light. This means that, if
    ! the electron is massive, its collinear singularities are
    ! regularized with its mass and terms proportional to the mass are
    ! discarded. If the electron is massless this subroutine has no
    ! effect (i.e. electron collinear singularities are regularized
    ! dimensionally).

    call check_support_sm_interface('set_light_electron_rcl')
    call processes_generated_warning_rcl('set_light_electron_rcl')
    call set_light_particle_rcl('e-')

  end subroutine set_light_electron_rcl

!------------------------------------------------------------------------------!

  subroutine unset_light_electron_rcl

    call check_support_sm_interface('unset_light_electron_rcl')
    call processes_generated_warning_rcl('unset_light_electron_rcl')
    call unset_light_particle_rcl('e-')

  end subroutine unset_light_electron_rcl

!------------------------------------------------------------------------------!

  ! remove?:  <24-07-17, Jean-Nicolas Lang> !
  subroutine set_light_fermion_to_zero_rcl

    ! This subroutine sets the pole mass to zero for the light particles.

    call check_support_sm_interface('set_light_fermion_to_zero_rcl')
    call processes_generated_warning_rcl('set_light_fermion_to_zero_rcl')

    call set_parameter_rcl('MC', cnul)
    call set_parameter_rcl('MS', cnul)
    call set_parameter_rcl('MU', cnul)
    call set_parameter_rcl('MD', cnul)
    call set_parameter_rcl('ME', cnul)
    call set_parameter_rcl('MM', cnul)
    call set_parameter_rcl('MTA', cnul)

  end subroutine set_light_fermion_to_zero_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_fermions_rcl(thre)

    ! This subroutine sets threshold for the light fermions

    use input_rcl, only: set_masscut_rcl

    real(dp), intent(in) :: thre

    call processes_generated_warning_rcl('set_light_fermions_rcl')
    call set_masscut_rcl(thre)

  end subroutine set_light_fermions_rcl

!------------------------------------------------------------------------------!

  subroutine use_gfermi_scheme_rcl(g,a)

    ! This subroutine sets the EW renormalization scheme to the gfermi
    ! scheme (alpha is determined from gfermi) and, if present, sets
    ! the Fermi constant "gf" to "g" or alternatively sets, if present,
    ! alpha_gf ("algf") to "a".

    real(dp), optional, intent(in) :: g,a
    complex(dp) :: mw,mz
    complex(dp) :: aEW

    call check_support_sm_interface('use_gfermi_scheme_rcl')
    call processes_generated_warning_rcl('use_gfermi_scheme_rcl')

    if (present(g) .and. present(a)) then
      call error_rcl('use_gfermi_scheme_rcl called with two arguments')
    endif

    if (present(g)) then
      call get_parameter_rcl('MW',mw)
      call get_parameter_rcl('MZ',mz)
      aEW = g/pi*mw**2d0*(1d0-mw**2d0/mz**2d0)*sqrt(2d0)
      call set_parameter_rcl('aEW', aEW)
    else if (present(a)) then
      call set_parameter_rcl('aEW', cone*a)
    endif
    call set_renoscheme_rcl('dZee_QED2', 'GFermi')

  end subroutine use_gfermi_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine use_alpha0_scheme_rcl(a)

    ! This subroutine sets the EW renormalization scheme to the alpha0
    ! scheme (alpha is renormalized at p^2 = 0) and, if present, sets
    ! alpha(0) ("al0") to "a".

    real(dp), optional, intent(in) :: a

    call check_support_sm_interface('use_alpha0_scheme_rcl')
    call processes_generated_warning_rcl('use_alpha0_scheme_rcl')

    if (present(a)) then
      call set_parameter_rcl('aEW', cone*a)
    endif
    call set_renoscheme_rcl('dZee_QED2', 'alpha0')

  end subroutine use_alpha0_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine use_alphaZ_scheme_rcl(a)

    real(dp), optional, intent(in) :: a

    call check_support_sm_interface('use_alphaZ_scheme_rcl')
    call processes_generated_warning_rcl('use_alphaZ_scheme_rcl')

    if (present(a)) then
      call set_parameter_rcl('aEW', cone*a)
    endif
    call set_renoscheme_rcl('dZee_QED2', 'alphaZ')

  end subroutine use_alphaZ_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine get_alpha_rcl(a)

    ! This subroutine extract the actual value of alpha.

    real(dp), intent(out) :: a
    complex(dp) :: ac

    call get_parameter_rcl('aEW',ac)
    a = real(ac,kind=dp)

  end subroutine get_alpha_rcl

!------------------------------------------------------------------------------!

  subroutine set_alphas_rcl(as,s,Nf)

    use globals_rcl, only: als,Qren,Nlq,Nfren,mq2,qcd_rescaling
    use modelfile, only: get_parameter_mdl,has_feature_mdl

    ! This subroutine sets the values of alpha_s ("als") to "a", the
    ! renormalization scale "Qren" to "s" and the number of active
    ! quark flavours "Nfren" to "Nf".
    ! The choosen value of "a" should correspond to the physical value
    ! of alpha_s at the scale "s" computed in the "Nf"-flavour scheme.
    ! Accepted values for "Nf" are -1,3,4,5,6
    ! Nf = -1      -> all quarks with mass less than "s" are active
    !                 flavours
    ! Nf = 3,4,5,6 -> the 3,4,5,6 lightest quarks are the active
    !                 flavours

    real(dp), intent(in) :: as, s
    integer,  intent(in) :: Nf
    integer              :: i
    real(dp)             :: m

    if (.not. has_feature_mdl('qcd_rescaling')) &
        call error_rcl('Cannot change alphas. qcd_rescaling is not supported',&
                       where='set_alphas_rcl')

    if (.not. processes_generated) then
      call set_parameter_rcl('aS', cone*as)
      call set_mu_ms_rcl(s)
      if (Nf .eq. -1) then
        ! Nlq is the number of quarks lighter than Qren
        Nlq = 0
        ! replace mq2 by model masses:  <06-07-17, Jean-Nicolas Lang> !
        do i = 1,6
          select case (i)
          case (3)
            m = real(get_parameter_mdl('MS'),kind=dp)
          case (4)
            m = real(get_parameter_mdl('MC'),kind=dp)
          case (5)
            m = real(get_parameter_mdl('MB'),kind=dp)
          case (6)
            m = real(get_parameter_mdl('MT'),kind=dp)
          case default
            m = 0d0
          end select
          if (m .lt. s) Nlq = Nlq + 1
        enddo
      else
        select case (Nf)
        case (3,4,5,6)
          Nlq = Nf
        case default
          call error_rcl('Wrong number of light flavours Nf (accepted ' // &
                         'values are Nf = -1,3,4,5,6)', &
                         where='set_alphas_rcl')
        end select
      end if
      Nfren = Nf
      if (Nlq .eq. 6) then
        call set_renoscheme_rcl('dZgs_QCD2', 'Nf6')
      else if (Nlq .eq. 5) then
        call set_renoscheme_rcl('dZgs_QCD2', 'Nf5')
      else if (Nlq .eq. 4) then
        call set_renoscheme_rcl('dZgs_QCD2', 'Nf4')
      else if (Nlq .eq. 3) then
        call set_renoscheme_rcl('dZgs_QCD2', 'Nf3')
      end if
    else
      if (.not. (qcd_rescaling .and. has_feature_mdl('qcd_rescaling'))) then
        call error_rcl('Cannot change alphas. qcd_rescaling is disabled.',&
                       where='set_alphas_rcl')
      end if
      als  = as
      Qren = s
      Nfren  = Nf
      ! set Nlq, the number of active flavours for alpha_s renormalization.
      select case (Nfren)
      case (-1)
        ! Nlq is the number of quarks lighter than Qren
        Nlq = 0
        do i = 1,6
          if (abs(mq2(i)) .lt. Qren**2) Nlq = Nlq + 1
        enddo
      case (3,4,5)
        if (abs(mq2(Nfren+1)) .eq. 0d0) then
          call error_rcl('Wrong number of light flavours Nf. Nfren can ' // &
                         'not be smaller than the number of massless quarks.', &
                         where='set_alphas_rcl')
        endif
        Nlq = Nfren
      case (6)
        Nlq = Nfren
      case default
        call error_rcl('Wrong number of light flavours Nf (accepted ' // &
                       'values are Nf = -1,3,4,5,6)', &
                       where='set_alphas_rcl')
      end select
    end if

  end subroutine set_alphas_rcl

!------------------------------------------------------------------------------!

  subroutine get_alphas_rcl(a)
    ! This subroutine returns the actual value of alpha_s ("als").

    use globals_rcl, only: als

    real(dp), intent(out) :: a

    complex(dp) :: ac

    if (.not. processes_generated) then
      call get_parameter_rcl('aS',ac)
      a = real(ac,kind=dp)
    else
      a = als
    end if

  end subroutine get_alphas_rcl

!------------------------------------------------------------------------------!

  subroutine set_alphas_masses_rcl(mc,mb,mt,gc,gb,gt)

    use globals_rcl, only: mq2
    ! This subroutine allows to set the values of the squared quark
    ! masses for the running of alpha_s; their values are not used in
    ! the rest of the computation.
    ! The squared masses of the 3 heaviest quarks are set to
    ! "mc^2*sqrt(1+gc^2/mc^2)", "mb^2*sqrt(1+gb^2/mb^2)" and
    ! "mt^2*sqrt(1+gt^2/mt^2)" respectively.
    ! "mc", "mb", "mt" must satisfy 0 <= mc <= mb <= mt.
    ! The optional arguments "gc", "gb", "gt"  are ignored if the
    ! on-shell renormalization scheme is used.

    real(dp), intent(in)           :: mc,mb,mt
    real(dp), intent(in), optional :: gc,gb,gt

    call check_support_sm_interface('set_alphas_masses_rcl')
    call processes_generated_error_rcl('set_alphas_masses_rcl')

    if ((mc.lt.0d0) .or. (mc .gt. mb) .or. (mb .gt. mt)) then
      call error_rcl('set_active_quark_masses_rcl called with wrong arguments. ' // &
                     'Masses need to be positive and to satisfy mc <= mb <= mt.')
    endif

    mq2(1:3) = cnul

    if (present(gc)) then
      if (gc.lt.0d0) then
        call error_rcl('Negative width given (gc).', &
                       where='set_alphas_masses_rcl')

      endif
      mq2(4) = mc**2 - cima*mc*gc
    else
      mq2(4) = mc**2
    endif

    if (present(gb)) then
      if (gb.lt.0d0) then
        call error_rcl('Negative width given (gb).', &
                       where='set_alphas_masses_rcl')
      endif
      mq2(5) = mb**2 - cima*mb*gb
    else
      mq2(5) = mb**2
    endif

    if (present(gt)) then
      if (gt.lt.0d0) then
        call error_rcl('Negative width given (gb).', &
                       where='set_alphas_masses_rcl')
      endif
      mq2(6) = mt**2 - cima*mt*gt
    else
      mq2(6) = mt**2
    endif

    use_active_qmasses = .true.

  end subroutine set_alphas_masses_rcl

!------------------------------------------------------------------------------!

  subroutine compute_running_alphas_rcl(Q,Nf,lp)

    use globals_rcl, only: als,als0,Qren,Qren0,Nlq,Nlq0,mq2,beta0,rb1, &
                           qcd_rescaling,processes_not_generated_error_rcl
    use modelfile, only: has_feature_mdl

    ! This subroutine can be called to compute the value for alpha_s at
    ! the scale "Q" employing the renormalization-group evolution at
    ! "lp" loops ("lp" can take the value "lp=1" or "lp=2").
    ! The integer argument "Nf" selects the flavour scheme according to
    ! the following rules:
    ! - "Nf = -1":
    !   The variable flavour scheme is selected, where the number of
    !   active flavours contributing to the running of alpha_s is set
    !   to the number of quarks lighter than the scale "Q".
    ! - "Nf = 3,4,5,6":
    !   The fixed Nf-flavour scheme is selected, where the number of
    !   active flavours contributing to the running of alpha_s is set
    !   to "Nf". In this case "Nf" cannot be smaller than the number of
    !   massless quarks (otherwise the code stops).


    real(dp), intent(in) :: Q
    integer,  intent(in) :: Nf,lp

    integer               :: Nl,Nl0,DNl,aDNl,i,n,k
    real(dp)              :: Q0,ia
    real(dp), allocatable :: bL(:)

    call processes_not_generated_error_rcl('compute_running_alphas_rcl')

    if (.not. (qcd_rescaling .and. has_feature_mdl('qcd_rescaling'))) then
      call error_rcl('Cannot compute running when qcd_rescaling is disabled.',&
                     where='compute_running_alphas_rcl')
    end if

    if (Q.le.0d0) then
      call error_rcl('Wrong scale input',  &
                     where='compute_running_alphas_rcl')
    endif

    Nl0 = Nlq0
    select case (Nf)
    case (-1)
      Nl = 0
      do i = 1,6
        if (abs(mq2(i)) .lt. Q**2) Nl = Nl + 1
      enddo
    case (3,4,5)
      if (mq2(Nf+1) .eq. 0d0) then
        call error_rcl('Wrong number of light flavours Nf (Nf can not ' // &
                       'be smaller than the number of massless quarks', &
                       where='compute_running_alphas_rcl')
      endif
      Nl = Nf
    case (6)
      Nl = Nf
    case default
      call error_rcl('Wrong number of light flavours Nf (accepted ' // &
                     'values are Nf = -1,3,4,5,6)', &
                     where='compute_running_alphas_rcl')
    end select
    DNl = Nl - Nl0
    aDNl = abs(DNl)

    Q0 = Qren0

    allocate ( bL(0:aDNl) )
    select case (DNl)
    case (0)
      bL(0) = beta0(Nl) * log(Q**2/Q0**2)
    case (1:)
      bL(0) = beta0(Nl0) * log(abs(mq2(Nl0+1))/Q0**2)
      do k = 1,aDNl-1; n = Nl0 + k
        bL(k) = beta0(n) * log(abs(mq2(n+1))/abs(mq2(n)))
      enddo
      bL(aDNl) = beta0(Nl) * log(Q**2/abs(mq2(Nl)))
    case (:-1)
      bL(0) = beta0(Nl0) * log(abs(mq2(Nl0))/Q0**2)
      do k = 1,aDNl-1; n = Nl0 - k
        bL(k) = beta0(n) * log(abs(mq2(n))/abs(mq2(n+1)))
      enddo
      bL(aDNl) = beta0(Nl) * log(Q**2/abs(mq2(Nl+1)))
    end select

    ia = pi/als0

    select case (lp)
    case (1)
      ia = ia + sum(bL(0:aDNl))
    case (2)
      do k = 0,aDNl; n = Nl0 + k*sign(1,DNl)
        ia = ia + bL(k) + rb1(n) * log( 1d0 + bL(k)/( ia + rb1(n) ) )
      enddo
    case default
      call error_rcl('Wrong number of loops loops lp for the running ' // &
                     '(accepted values are lp = 1,2)', &
                     where='compute_running_alphas_rcl')
    end select

    als   = pi/ia
    Qren  = Q
    Nlq   = Nl

  end subroutine compute_running_alphas_rcl

!------------------------------------------------------------------------------!

!===================================!
!  from process_definition_rcl.f90  !
!===================================!

  subroutine select_all_gs_powers_BornAmpl_rcl(npr)

    ! This subroutine selects all contribution to the Born amplitude
    ! (with any g_s power) for the process with process number "npr".
    ! The selection of the contributions to the loop amplitude remains
    ! uneffected.

    use process_definition_rcl, only: select_all_powers_BornAmpl_rcl

    integer, intent(in)  :: npr

    call check_support_sm_fund_interface('select_all_gs_powers_BornAmpl_rcl')
    call processes_generated_warning_rcl('select_all_gs_powers_BornAmpl_rcl')

    call select_all_powers_BornAmpl_rcl(npr)

  end subroutine select_all_gs_powers_BornAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine unselect_all_gs_powers_BornAmpl_rcl(npr)

    ! This subroutine unselects all contribution to the Born amplitude
    ! (with any g_s power) for the process with process number "npr".
    ! The selection of the contributions to the loop amplitude remains
    ! uneffected.

    use process_definition_rcl, only: unselect_all_powers_BornAmpl_rcl

    integer, intent(in)  :: npr

    call check_support_sm_fund_interface('unselect_all_gs_powers_BornAmpl_rcl')
    call processes_generated_warning_rcl('unselect_all_gs_powers_BornAmpl_rcl')

    call unselect_all_powers_BornAmpl_rcl(npr)

  end subroutine unselect_all_gs_powers_BornAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine select_gs_power_BornAmpl_rcl(npr,gspower)

    ! This subroutine selects the contribution to the Born amplitude
    ! with g_s power "gspower", for the process with process number
    ! "npr" .
    ! The selection of the contributions to the loop amplitude are
    ! uneffected, as well as the selection of the other powers of g_s
    ! for the Born amplitude.

    use globals_rcl, only: prs
    use process_definition_rcl, only: select_power_BornAmpl_rcl

    integer, intent(in)  :: npr,gspower
    integer              :: pr,legs,QED,QCD

    call check_support_sm_fund_interface('select_gs_power_BornAmpl_rcl')
    call processes_generated_warning_rcl('select_gs_power_BornAmpl_rcl')

    call get_pr(npr, 'select_gs_power_BornAmpl_rcl', pr)

    legs = prs(pr)%legs

    QCD = gspower
    QED = legs-2-QCD

    call select_power_BornAmpl_rcl(npr, 'QCD', QCD)
    call select_power_BornAmpl_rcl(npr, 'QED', QED)

  end subroutine select_gs_power_BornAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine unselect_gs_power_BornAmpl_rcl(npr,gspower)

    ! This subroutine unselects the contribution to the Born amplitude
    ! with g_s power "gspower" for the process with process number
    ! "npr".
    ! The selection of the contributions to the loop amplitude remains
    ! uneffected, as well as the selection of the other powers of g_s
    ! for the Born amplitude.

    use globals_rcl, only: prs
    use process_definition_rcl, only: unselect_power_BornAmpl_rcl

    integer, intent(in)  :: npr,gspower
    integer              :: pr,legs,QED,QCD

    call check_support_sm_fund_interface('unselect_gs_power_BornAmpl_rcl')
    call processes_generated_warning_rcl('unselect_gs_power_BornAmpl_rcl')

    call get_pr(npr, 'unselect_gs_power_BornAmpl_rcl', pr)

    legs = prs(pr)%legs

    QCD = gspower
    QED = legs-2-QCD

    call unselect_power_BornAmpl_rcl(npr, 'QCD', QCD)
    call unselect_power_BornAmpl_rcl(npr, 'QED', QED)

  end subroutine unselect_gs_power_BornAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine select_all_gs_powers_LoopAmpl_rcl(npr)

    ! This subroutine selects all contribution to the loop amplitude
    ! (with any g_s power) for the process with process number "npr".
    ! The selection of the contributions to the Born amplitude remains
    ! uneffected.

    use process_definition_rcl, only: select_all_powers_LoopAmpl_rcl

    integer, intent(in)  :: npr

    call check_support_sm_fund_interface('select_all_gs_powers_LoopAmpl_rcl')
    call processes_generated_warning_rcl('select_all_gs_powers_LoopAmpl_rcl')

    call select_all_powers_LoopAmpl_rcl(npr)

  end subroutine select_all_gs_powers_LoopAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine unselect_all_gs_powers_LoopAmpl_rcl(npr)

    ! This subroutine unselects all contribution to the loop amplitude
    ! (with any g_s power) for the process with process number "npr".
    ! The selection of the contributions to the Born amplitude remains
    ! uneffected.

    use process_definition_rcl, only: unselect_all_powers_LoopAmpl_rcl

    integer, intent(in)  :: npr

    call check_support_sm_fund_interface('unselect_all_gs_powers_LoopAmpl_rcl')
    call processes_generated_warning_rcl('unselect_all_gs_powers_LoopAmpl_rcl')

    call unselect_all_powers_LoopAmpl_rcl(npr)

  end subroutine unselect_all_gs_powers_LoopAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine select_gs_power_LoopAmpl_rcl(npr,gspower)

    ! This subroutine selects the contribution to the loop amplitude
    ! with g_s power "gspower" for the process with process number
    ! "npr".
    ! The selection of the contributions to the Born amplitude remains
    ! uneffected, as well as the selection of the other powers of g_s
    ! for the loop amplitude.

    use globals_rcl, only: prs
    use process_definition_rcl, only: select_power_LoopAmpl_rcl

    integer, intent(in)  :: npr,gspower
    integer              :: pr,legs,QCD,QED

    call check_support_sm_fund_interface('select_gs_power_LoopAmpl_rcl')
    call processes_generated_warning_rcl('select_gs_power_LoopAmpl_rcl')

    call get_pr(npr, 'select_gs_power_LoopAmpl_rcl', pr)

    legs = prs(pr)%legs

    QCD = gspower
    QED = legs-QCD

    call select_power_LoopAmpl_rcl(npr, 'QCD', QCD)
    call select_power_LoopAmpl_rcl(npr, 'QED', QED)

  end subroutine select_gs_power_LoopAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine unselect_gs_power_LoopAmpl_rcl(npr,gspower)

    ! This subroutine unselects the contribution to the loop amplitude
    ! with g_s power "gspower" for the process with process number
    ! "npr".
    ! The selection of the contributions to the Born amplitude remains
    ! uneffected, as well as the selection of the other powers of g_s
    ! for the loop amplitude.

    use globals_rcl, only: prs
    use process_definition_rcl, only: unselect_power_LoopAmpl_rcl

    integer, intent(in) :: npr,gspower
    integer             :: pr,legs,QCD,QED

    call check_support_sm_fund_interface('unselect_gs_power_LoopAmpl_rcl')
    call processes_generated_warning_rcl('unselect_gs_power_LoopAmpl_rcl')

    call get_pr(npr, 'unselect_gs_power_LoopAmpl_rcl', pr)

    legs = prs(pr)%legs

    QCD = gspower
    QED = legs-QCD

    call unselect_power_LoopAmpl_rcl(npr, 'QCD', QCD)
    call unselect_power_LoopAmpl_rcl(npr, 'QED', QED)

  end subroutine unselect_gs_power_LoopAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine set_gs_power_rcl(npr,gsarray)

    ! This subroutine selects specific powers of g_s, as
    ! specified by the argument "gsarray", for the process with
    ! identifier "npr".
    ! "gsarray(0:,0:1)" is an integer array whose values can be 0 or 1.
    ! The first entry is the power of g_s.
    ! The second entry is the loop order (0 for LO, 1 for NLO)
    ! Example:
    ! process 'd~ d -> u~ u' at NLO
    ! gsarray(0,0) = 1
    ! gsarray(1,0) = 0
    ! gsarray(2,0) = 1
    ! gsarray(0,1) = 0
    ! gsarray(1,1) = 0
    ! gsarray(2,1) = 0
    ! gsarray(3,1) = 0
    ! gsarray(4,1) = 1
    ! Here we select for computation the contributions with g_s^0 and
    ! g_s^2 for the tree amplitude and the g_s^4 contribution for the
    ! loop amplitude. All other contributions are not selected.

    use globals_rcl, only: prs
    use process_definition_rcl, only: select_power_BornAmpl_rcl,        &
                                      select_power_LoopAmpl_rcl,        &
                                      unselect_all_powers_BornAmpl_rcl, &
                                      unselect_all_powers_LoopAmpl_rcl
    integer, intent(in) :: npr,gsarray(0:,0:)
    integer             :: pr,i,legs,QCD,QED

    call processes_generated_warning_rcl('set_gs_power_rcl')

    call get_pr(npr, 'set_gs_power_rcl', pr)

    legs = prs(pr)%legs

    call unselect_all_powers_BornAmpl_rcl(npr)
    call unselect_all_powers_LoopAmpl_rcl(npr)

    do i = 0, size(gsarray,1)-1
      if (gsarray(i,0) .ne. 0 .and. i .le. legs-2) then
        QCD = i
        QED = legs-2-QCD
        call select_power_BornAmpl_rcl(npr, 'QCD', QCD)
        call select_power_BornAmpl_rcl(npr, 'QED', QED)
      end if

      if (gsarray(i,1) .ne. 0) then
        QCD = i
        QED = legs-QCD
        call select_power_LoopAmpl_rcl(npr, 'QCD', QCD)
        call select_power_LoopAmpl_rcl(npr, 'QED', QED)
      end if
    end do

  end subroutine set_gs_power_rcl

!------------------------------------------------------------------------------!

  subroutine get_amplitude_r1_rcl(npr,gspower,order,colour,hel,A)

    ! This subroutine selects the contribution to the loop amplitude
    ! with g_s power "gspower" for the process with process number
    ! "npr".
    ! The selection of the contributions to the Born amplitude remains
    ! uneffected, as well as the selection of the other powers of g_s
    ! for the loop amplitude.

    use globals_rcl, only: prs
    use process_computation_rcl, only: get_amplitude_general_rcl
    use modelfile, only: get_order_id_mdl

    integer,          intent(in)  :: npr,gspower,colour(1:),hel(1:)
    character(len=*), intent(in)  :: order
    complex(dp),      intent(out) :: A
    integer                       :: pr,legs,QCD,QED

    call check_support_sm_fund_interface('get_amplitude_r1_rcl')
    call get_pr(npr, 'get_amplitude_r1_rcl', pr)

    if (trim(adjustl(get_order_id_mdl(1))) .ne. 'QCD') then
      call error_rcl('Expecting QCD as the first fundamental order.', &
                     where='get_amplitude_r1_rcl')
    end if

    legs = prs(pr)%legs

    QCD = gspower
    if (.not. zeroLO(pr)) then
      if (order .eq. 'LO') then
        QED = legs - QCD - 2
      else
        QED = legs - QCD
      end if
    else
      QED = legs - QCD
    end if

    call get_amplitude_general_rcl(npr,[QCD, QED],order,colour,hel,A)

  end subroutine get_amplitude_r1_rcl

!------------------------------------------------------------------------------!

  subroutine get_squared_amplitude_r1_rcl(npr,aspow,order,A2)

    ! This subroutine selects the contribution to the loop amplitude
    ! with g_s power "gspower" for the process with process number
    ! "npr".
    ! The selection of the contributions to the Born amplitude remains
    ! uneffected, as well as the selection of the other powers of g_s
    ! for the loop amplitude.

    use globals_rcl, only: prs
    use process_computation_rcl, only: get_squared_amplitude_general_rcl

    integer,          intent(in)  :: npr,aspow
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2
    integer                       :: prin,pr,legs,QCD,QED

    call check_support_sm_fund_interface('get_squared_amplitude_r1_rcl')
    call get_pr(npr, 'get_squared_amplitude_r1_rcl', prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
    else
      pr = prin
    end if

    legs = prs(pr)%legs

    QCD = aspow
    if (.not. zeroLO(pr)) then
      if (order .eq. 'LO') then
        QED = legs - QCD - 2
      else
        QED = legs - QCD - 1
      end if
    else
      QED = legs - QCD
    end if

    call get_squared_amplitude_general_rcl(npr,[2*QCD, 2*QED],order,A2)

  end subroutine get_squared_amplitude_r1_rcl

!------------------------------------------------------------------------------!

  subroutine get_polarized_squared_amplitude_r1_rcl(npr,aspow,order,hel,A2h)

    ! Interface to get_polarized_squared_amplitude_general_rcl for models with
    ! only powers in QCD,QED:
    ! - "aspow" is the power of alpha_s of the contribution.
    ! For the other arguments see get_polarized_squared_amplitude_general_rcl

    use globals_rcl, only: prs
    use process_computation_rcl, only: get_polarized_squared_amplitude_general_rcl

    integer,          intent(in)  :: npr,aspow,hel(1:)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2h
    integer                       :: prin,pr,legs,QCD,QED

    call check_support_sm_fund_interface('get_polarized_squared_amplitude_r1_rcl')
    call get_pr(npr, 'get_polarized_squared_amplitude_r1_rcl', prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
    else
      pr = prin
    end if

    legs = prs(pr)%legs

    QCD = aspow
    if (.not. zeroLO(pr)) then
      if (order .eq. 'LO') then
        QED = legs - QCD - 2
      else
        QED = legs - QCD - 1
      end if
    else
      QED = legs - QCD
    end if

    call get_polarized_squared_amplitude_general_rcl(npr,[2*QCD,2*QED],order,hel,A2h)

  end subroutine get_polarized_squared_amplitude_r1_rcl

!------------------------------------------------------------------------------!

  subroutine get_colour_correlation_r1_rcl(npr,aspow,i1,i2,order,A2cc)

    ! SM interface to the general method.
    ! This subroutine extracts the computed value of the
    ! colour-correlated summed squared amplitude, with "pow" power of
    ! alpha_s, between particle with leg number "i1" and particle with
    ! leg number "i2", for the process with process number "npr".
    ! This routine must be called after compute_colour_correlation_rcl
    ! or rescale_colour_correlation_rcl(for process with process number
    ! "npr" for particles "i1" and "i2") or after
    ! compute_all_colour_correlations_rcl for process or
    ! rescale_all_colour_correlations_rcl for process with process
    ! number "npr".

    use globals_rcl, only: prs
    use process_computation_rcl, only :       &
        get_colour_correlation_general_rcl

    integer,                    intent(in)  :: npr,aspow,i1,i2
    character(len=*), optional, intent(in)  :: order
    real(dp),                   intent(out) :: A2cc
    integer :: prin,pr,legs,QCD,QED

    call check_support_sm_fund_interface('get_colour_correlation_r1_rcl')
    call get_pr(npr, 'get_colour_correlation_r1_rcl', prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
    else
      pr = prin
    end if

    legs = prs(pr)%legs

    QCD = aspow
    if (.not. zeroLO(pr)) then
      if (.not. present(order) .or. order .eq. 'LO') then
        QED = legs - QCD - 2
      else
        QED = legs - QCD - 1
      end if
    else
      QED = legs - QCD
    end if

    if (present(order)) then
      call get_colour_correlation_general_rcl(npr,[2*QCD,2*QED],i1,i2,ord=order,A2cc=A2cc)
    else
      call get_colour_correlation_general_rcl(npr,[2*QCD,2*QED],i1,i2,A2cc=A2cc)
    end if

  end subroutine get_colour_correlation_r1_rcl

!------------------------------------------------------------------------------!

! deprecated
  subroutine get_colour_correlation_int_r1_rcl(npr,aspow,i1,i2,A2ccint)

    ! SM interface to the general method.

    use globals_rcl, only: prs
    use process_computation_rcl, only :       &
        get_colour_correlation_int_general_rcl

    integer,          intent(in)  :: npr,aspow,i1,i2
    real(dp),         intent(out) :: A2ccint
    integer                       :: prin,pr,legs,QCD,QED

    call check_support_sm_fund_interface('get_colour_correlation_int_r1_rcl')
    call get_pr(npr, 'get_colour_correlation_int_r1_rcl', pr)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
    else
      pr = prin
    end if

    legs = prs(pr)%legs

    QCD = aspow
    QED = legs-QCD -1

    call get_colour_correlation_int_general_rcl(npr,[2*QCD,2*QED],i1,i2,A2ccint)

  end subroutine get_colour_correlation_int_r1_rcl

!------------------------------------------------------------------------------!

  subroutine get_spin_colour_correlation_r1_rcl(npr,aspow,i1,i2,order,A2scc)

    ! SM interface to the general method.
    ! This subroutine extracts the computed value of the
    ! spin-colour-correlated summed squared amplitude with "pow" power
    ! of alpha_s, between particle with leg number "i1" and particle
    ! with leg number "i2", for the process with process number "npr".
    ! This routine must be called after
    ! compute_spin_colour_correlation_rcl or
    ! rescale_spin_colour_correlation_rcl for process with process
    ! number "npr" for particles "i1" and "i2" with the  desired
    ! polarization vestor.

    use globals_rcl, only: prs
    use process_computation_rcl, only :       &
        get_spin_colour_correlation_general_rcl

    integer,                    intent(in)  :: npr,aspow,i1,i2
    character(len=*), optional, intent(in)  :: order
    real(dp),                   intent(out) :: A2scc

    integer :: prin,pr,legs,QCD,QED

    call check_support_sm_fund_interface('get_spin_colour_correlation_r1_rcl')

    call get_pr(npr, 'get_spin_colour_correlation_r1_rcl', prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
    else
      pr = prin
    end if

    legs = prs(pr)%legs

    QCD = aspow
    if (.not. zeroLO(pr)) then
      if (.not. present(order) .or. order .eq. 'LO') then
        QED = legs - QCD - 2
      else
        QED = legs - QCD - 1
      end if
    else
      QED = legs - QCD
    end if

    if (present(order)) then
      call get_spin_colour_correlation_general_rcl(npr,[2*QCD,2*QED],i1,i2,ord=order,A2scc=A2scc)
    else
      call get_spin_colour_correlation_general_rcl(npr,[2*QCD,2*QED],i1,i2,A2scc=A2scc)
    end if

  end subroutine get_spin_colour_correlation_r1_rcl

!------------------------------------------------------------------------------!

  subroutine get_spin_correlation_r1_rcl(npr,aspow,order,A2sc)

    ! This subroutine extracts the computed value of the
    ! spin-correlated summed squared amplitude with "pow" power
    ! of alpha_s for the process with process number "npr".
    ! This routine must be called after compute_spin_correlation_rcl
    ! or rescale_spin_correlation_rcl for process with process number
    ! "npr" with the desired polarization vestor.

    use globals_rcl, only: prs
    use process_computation_rcl, only :       &
        get_spin_correlation_general_rcl
    integer,                    intent(in)  :: npr,aspow
    character(len=*), optional, intent(in)  :: order
    real(dp),                   intent(out) :: A2sc
    integer :: prin,pr,legs,QCD,QED

    call get_pr(npr, 'get_spin_correlation_rcl', prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
    else
      pr = prin
    end if

    legs = prs(pr)%legs

    QCD = aspow
    if (.not. zeroLO(pr)) then
      if (.not. present(order) .or. order .eq. 'LO') then
        QED = legs - QCD - 2
      else
        QED = legs - QCD - 1
      end if
    else
      QED = legs - QCD
    end if

    if (present(order)) then
      call get_spin_correlation_general_rcl(npr,[2*QCD, 2*QED],ord=order,A2sc=A2sc)
    else
      call get_spin_correlation_general_rcl(npr,[2*QCD, 2*QED],A2sc=A2sc)
    end if

  end subroutine get_spin_correlation_r1_rcl

!------------------------------------------------------------------------------!

  subroutine get_spin_correlation_matrix_r1_rcl(npr,aspow,order,A2scm)

    ! This subroutine extracts the computed value of the
    ! spin-correlated summed squared amplitude with "pow" power
    ! of alpha_s for the process with process number "npr".
    ! This routine must be called after compute_spin_correlation_rcl
    ! or rescale_spin_correlation_rcl for process with process number
    ! "npr" with the desired polarization vestor.

    use globals_rcl, only: prs
    use process_computation_rcl, only :       &
        get_spin_correlation_matrix_general_rcl
    integer,                    intent(in)  :: npr,aspow
    character(len=*), optional, intent(in)  :: order
    real(dp),                   intent(out) :: A2scm(0:3,0:3)
    integer :: prin,pr,legs,QCD,QED

    call get_pr(npr, 'get_spin_correlation_matrix_rcl', prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
    else
      pr = prin
    end if

    legs = prs(pr)%legs

    QCD = aspow
    if (.not. zeroLO(pr)) then
      if (.not. present(order) .or. order .eq. 'LO') then
        QED = legs - QCD - 2
      else
        QED = legs - QCD - 1
      end if
    else
      QED = legs - QCD
    end if

    if (present(order)) then
      call get_spin_correlation_matrix_general_rcl(npr,[2*QCD,2*QED],ord=order,A2scm=A2scm)
    else
      call get_spin_correlation_matrix_general_rcl(npr,[2*QCD,2*QED],A2scm=A2scm)
    end if

  end subroutine get_spin_correlation_matrix_r1_rcl

!------------------------------------------------------------------------------!

end module recola1_interface_rcl

!------------------------------------------------------------------------------!
