!******************************************************************************!
!                                                                              !
!    collier_interface_rcl.f90                                                 !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module collier_interface_rcl

!------------------------------------------------------------------------------!

  use globals_rcl
  use collier, only :           &
      Init_cll,                 &
      InitCacheSystem_cll,      &
      SetMode_cll,              &
      SwitchOnTenRed_cll,       &
      SwitchOffTenRed_cll,      &
      SetCacheLevel_cll,        &
      SwitchOffCache_cll,       &
      SwitchOffCacheSystem_cll, &
      SetCacheMode_cll,         &
      setminf2_cll,             &
      SetDeltaUV_cll,           &
      SetDeltaIR_cll,           &
      SetMuIR2_cll,             &
      SetMuUV2_cll,             &
      InitEvent_cll,            &
      TNten_cll,                &
      GetAccFlag_cll,           &
      SetCritAcc_cll,           &
      GetCritAcc_cll,           &
      SetReqAcc_cll,            &
      GetReqAcc_cll,            &
      SetErrStop_cll,           &
      GetErrFlag_cll,           &
      SetAccuracy_cll

  use modelfile, only: get_n_particles_mdl, get_particle_cmass2_reg_mdl, &
                       is_particle_mdl, get_particle_mass_reg_mdl
  use input_rcl, only: get_delta_uv_rcl, get_delta_ir_rcl, &
                       get_mu_uv_rcl, get_mu_ir_rcl

!------------------------------------------------------------------------------!

 implicit none

!------------------------------------------------------------------------------!

 contains

!------------------------------------------------------------------------------!
  
  subroutine initialize_collier

  integer                  :: pr,nPropMax,rankMax,i,n,n_particles
  integer, allocatable     :: nProp(:), rank(:), nCacheTmp(:)
  logical, allocatable     :: cacheOnTmp(:)
  complex(dp), allocatable :: m2(:)
  real(dp) :: d,d2,muUV,muIR

  logical :: crossing_warning_printed = .false.

  allocate (nProp(prTot)); nProp = 0
  allocate (rank(prTot)); rank = 0
  do pr = 1,prTot
    if (prs(pr)%crosspr .ne. 0) cycle
    if (prs(pr)%loop) nProp(pr) = maxval(legsti(:tiTot(pr),pr))
    if (prs(pr)%loop) rank(pr) = maxval(rankti(0:tiTot(pr),pr))
  end do
  nPropMax = maxval(nProp)
  rankMax = maxval(rank)

  call openOutput
  write (nx,'(a,i2,a,i2,a)') ' Initialising collier with', nPropMax, &
                             ' legs and rank', rankMax, '.'
  if (trim(collier_output_dir) .eq. 'default') then
    call Init_cll(nPropMax,rmax=max(rankMax,2),noreset=.true.)
  else
    call Init_cll(nPropMax,rmax=max(rankMax,2),         &
                  folder_name=trim(collier_output_dir), &
                  noreset=.true.)
  end if

  if (cache_mode .eq. 1) then
    if (.not.allocated(nCache)) then
      allocate(nCache(1:prTot))
      nCache = 1
    else if (size(nCache) .ne. prTot) then
      allocate(nCacheTmp(size(nCache)))
      nCacheTmp(:) = nCache(:)
      deallocate(nCache)
      allocate(nCache(prTot))
      nCache = 1
      nCache(1:size(nCacheTmp)) = nCacheTmp(:)
    endif

    if (.not.allocated(cacheOn)) then
      allocate(cacheOn(1:prTot))
      cacheOn = .true.
    else if (size(cacheOn) .ne. prTot) then
      allocate(cacheOnTmp(size(cacheOn)))
      cacheOnTmp(:) = cacheOn(:)
      deallocate(cacheOn)
      allocate(cacheOn(prTot))
      cacheOn = .true.
      cacheOn(1:size(cacheOnTmp)) = cacheOnTmp(:)
    endif

    allocate(nCacheTot(0:prTot),tiCache(prTot))

    nCacheTot = 0
    do pr = 1,prTot
      if ((.not. crossing_warning_printed) .and. prs(pr)%crosspr .ne. 0 ) then
        if (nCache(pr) .ne. nCache(prs(pr)%crosspr)) &
          call warning_rcl("Detected differences in cache setup for " // &
                           "crossing related processes. " // &
                           "Crossed cache setup will be ignored.", &
                           where="check_crossing")
          crossing_warning_printed = .true.
      end if
      if ((.not. prs(pr)%exists) .or. &
          prs(pr)%crosspr .ne. 0 .or. &
          (.not. prs(pr)%loop)) then 
        cacheOn(pr) = .false.
        nCache(pr) = 0
      end if
      if (prs(pr)%loop .and. prs(pr)%crosspr .eq. 0) then
        if (nCache(pr) .gt. 0) then
          tiCache(pr) = ceiling(tiTot(pr)*1d0/nCache(pr))
        else
          tiCache(pr) = 0
        end if
      end if
      nCacheTot(pr) = nCacheTot(pr-1) + nCache(pr)
    enddo

    write (nx,*) 'Using ' // &
      trim(adjustl(int_to_str(nCacheTot(prTot)))) // &
      ' collier caches in local mode (individual process caches)'
    call InitCacheSystem_cll(nCacheTot(prTot),nPropMax)
    do pr = 1,prTot
      do n = nCacheTot(pr-1)+1,nCacheTot(pr)
        call SetCacheLevel_cll(n,nProp(pr))
        if (.not.cacheOn(pr)) call SwitchOffCache_cll(n)
      enddo
    enddo
  else if (cache_mode .eq. 2) then
    write (nx,*) 'Using ' // &
      trim(adjustl(int_to_str(nCacheGlobal))) // &
      ' collier caches in global mode (shared between processes)'
    call InitCacheSystem_cll(nCacheGlobal,nPropMax)
  end if
  write(nx,'(1x,75("-"))')

  call SetMode_cll(collier_mode)

  if (collier_tensor_reduction) then
    call SwitchOnTenRed_cll
  else
    call SwitchOffTenRed_cll
  end if

  if (ifail.ge.0) call SetErrStop_cll(-11) 

  n = 0
  n_particles = get_n_particles_mdl()
  do i = 1,n_particles
    if (get_particle_mass_reg_mdl(i) .eq. 2 .and. is_particle_mdl(i))  n = n + 1
  end do

  allocate (m2(n))

  n = 0
  do i = 1,n_particles
    if (get_particle_mass_reg_mdl(i) .eq. 2 .and. is_particle_mdl(i))  then
      n = n + 1
      m2(n) = get_particle_cmass2_reg_mdl(i)
    endif
  enddo
  call setminf2_cll (n,m2)
  deallocate (m2)

  call get_delta_uv_rcl(d)
  call SetDeltaUV_cll(d)
  call get_mu_uv_rcl(muUV)
  call SetMuUV2_cll(muUV**2) 

  call get_delta_ir_rcl(d,d2)
  call SetDeltaIR_cll(d,d2)
  call get_mu_ir_rcl(muIR)
  call SetMuIR2_cll(muIR**2)

  end subroutine initialize_collier

!------------------------------------------------------------------------------!

end module collier_interface_rcl

!------------------------------------------------------------------------------!
