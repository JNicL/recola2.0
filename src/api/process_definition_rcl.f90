!******************************************************************************!
!                                                                              !
!    process_definition_rcl.f90                                                !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module process_definition_rcl

!------------------------------------------------------------------------------!

  use globals_rcl
  use modelfile, only: get_particle_id_mdl, get_antiparticle_id_mdl,   &
                       get_n_orders_mdl, has_feature_mdl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine define_process_rcl(npr,processIn,order)

  integer,          intent(in) :: npr
  character(len=*), intent(in) :: processIn,order

  integer                :: i,j,n,n0,n1,n2,le,leIn,l1,sign, &
                            nd0,nd,pond(99),binnd(99),polegs,nord,&
                            crossprocess
  integer, allocatable   :: pa(:),paT(:),he(:),heT(:),relperm(:)
  integer, allocatable   :: qflow(:), powsel(:,:,:)
  character              :: cpr*99,cpr0*99,che*3
  character, allocatable :: cpa(:)*7,cpaT(:)*7
  character(len=10)      :: log_info
  logical                :: looptrig

  type(process_def), allocatable :: newpr

  call processes_generated_error_rcl('define_process_rcl')

  cpr = adjustl(processIn)

  n = 99
  do while (index (cpr,' [').ne.0)
    n = index (cpr,' [')
    cpr = cpr(1:n-1)//cpr(n+1:)
  enddo
  n = 99
  do while (scan(cpr,'(').ne.0)
    n = scan(cpr,'(')
    cpr = cpr(1:n-1)//' { '//cpr(n+1:)
  enddo
  n = 99
  do while (scan(cpr,')').ne.0)
    n = scan(cpr,')')
    cpr = cpr(1:n-1)//' } '//cpr(n+1:)
  enddo

  le = 0
  leIn = 0

  pond = 0
  binnd = 0
  nd0 = 0
  nd = 0

  nord = get_n_orders_mdl()

 do while (cpr.ne.'')

    if (le.gt.0) then
      allocate (cpaT(le)); cpaT(:le) = cpa(:le); deallocate (cpa)
      allocate ( paT(le));  paT(:le) =  pa(:le); deallocate ( pa)
      allocate ( heT(le));  heT(:le) =  he(:le); deallocate ( he)
    endif
    le = le + 1
    allocate (cpa(le),pa(le),he(le))
    if (le.gt.1) then
      cpa(:le-1) = cpaT(:le-1); deallocate (cpaT)
       pa(:le-1) =  paT(:le-1); deallocate ( paT)
       he(:le-1) =  heT(:le-1); deallocate ( heT)
    endif

    n0 = scan(cpr,'>')
    n1 = scan(cpr,' ') - 1
    sign = 1
    if (n1.ge.n0) then
      cpr = adjustl(cpr(n0+1:))
      n1 = scan(cpr,' ') - 1
      sign = -1
    else
      leIn = le
    endif

    n2 = scan(cpr(1:n1),'[')
    if (n2.eq.0) then
      cpa(le) = cpr(1:n1);        he(le) = 111
    else
      che = cpr(n2:n2+2)
      if     (che.eq.'[-]') then; he(le) = -1 * sign
      elseif (che.eq.'[0]') then; he(le) =  0
      elseif (che.eq.'[+]') then; he(le) = +1 * sign
      else;                       he(le) = 111
      endif
      cpa(le) = cpr(1:n2-1)
    endif

    cpr0 = adjustl(cpr(n1+1:))
    if (cpr0(1:1).eq.'{'.and.he(le).ne.111) then
      call warning_rcl('Particle ' // trim(cpa(le)),    &
                       ' can not have a specific helicity.')
      he(le) = 111
    endif

    call openOutput
    pa(le) = get_particle_id_mdl(cpa(le))
    if (sign.eq.-1) pa(le) = get_antiparticle_id_mdl(cpa(le))
    cpr = adjustl(cpr(n1+1:))
    if (cpr(1:1).eq.'{') then
      nd0 = nd0 + 1
      nd  = nd0
      pond(nd) = pa(le)
      l1 = le
      le = le - 1
      cpr = adjustl(cpr(2:))
    elseif (cpr(1:1).eq.'}') then
      polegs = le - l1 + 1
      cpr = adjustl(cpr(2:))
      binnd(nd) = 2**(l1-1)*(2**polegs - 1)
      nd = nd - 1
    endif

  enddo

  if     (order.eq.'LO' ) then
    looptrig = .false.
  elseif (order.eq.'NLO') then
    loopMax = .true.
    looptrig = .true.
  else
    call error_rcl('define_process_rcl called at the wrong '// &
                   "loop order (accepted values are order = 'LO','NLO')")
  endif

  if (leIn.le.0) then
    call error_rcl('define_process_rcl called for a process ' // &
                   'without incoming particles')
  endif

  allocate(newpr)
  allocate(qflow(le))
  qflow= 0
  allocate(powsel(0:4*le+2,nord,0:1))
  powsel= 1
  allocate(relperm(le))
  relperm = 0
  crossprocess = 0
  newpr = process_def(process=adjustl(processIn), &
                      inpr=npr,loop=looptrig,     &
                      exists=.true.,              &
                      legs=le,legsIn=leIn,        &
                      legsOut=le-leIn,            &
                      par=pa,hel=he,resMax=nd0,   &
                      binRes=binnd(1:nd0),        &
                      parRes=pond(1:nd0),         &
                      qflow=qflow,                &
                      powsel=powsel,              &
                      crosspr=crossprocess,       &
                      relperm=relperm,            &
                      colcoef=NULL(),             &
                      colcoefc=NULL()             &
                      )
  call insert_process_def(newpr)
  deallocate(qflow,powsel,relperm,newpr)

  legsMax = maxval(prs(:)%legs)

  do i = 1,prTot
    do j = 1,prTot
      if (i .ne. j .and. prs(i)%inpr .eq. prs(j)%inpr) then
        write(log_info, '(i10)') npr
        call error_rcl('define_process_rcl called for different '// &
                       'processes with the same process-index: ' // &
                        adjustl(log_info))
      endif
    end do
  end do

  contains

  subroutine insert_process_def(prsin)
    use modelfile, only: get_n_orders_mdl
    type(process_def), intent(inout) :: prsin

    type(process_def), allocatable :: prsT(:)
    integer :: i,legs,resMax,nord

    nord = get_n_orders_mdl()

    prTot = prTot + 1

    if (prTot .gt. 1) then
      allocate(prsT(prTot-1))
      do i = 1, prTot-1
        legs = prs(i)%legs
        resMax = prs(i)%resMax
        allocate(prsT(i)%par(legs))
        allocate(prsT(i)%hel(legs))
        allocate(prsT(i)%binRes(resMax))
        allocate(prsT(i)%parRes(resMax))
        allocate(prsT(i)%qflow(legs))
        allocate(prsT(i)%powsel(size(prs(i)%powsel,1),nord,0:1))
        prsT(i) = prs(i)
      end do
      deallocate(prs)

      allocate(prs(prTot))
      do i = 1, prTot-1
        legs = prsT(i)%legs
        allocate(prs(i)%par(legs))
        allocate(prs(i)%hel(legs))
        allocate(prs(i)%binRes(legs))
        allocate(prs(i)%parRes(legs))
        allocate(prs(i)%qflow(legs))
        allocate(prs(i)%powsel(size(prsT(i)%powsel,1),nord,0:1))
        prs(i) = prsT(i)
      end do
    else
      allocate(prs(prTot))
    endif

    legs = prsin%legs
    allocate(prs(prTot)%par(legs))
    allocate(prs(prTot)%hel(legs))
    allocate(prs(prTot)%binRes(legs))
    allocate(prs(prTot)%parRes(legs))
    allocate(prs(prTot)%qflow(legs))
    allocate(prs(prTot)%powsel(size(prsin%powsel,1),nord,0:1))
    prs(prTot) = prsin

  end subroutine insert_process_def

  end subroutine define_process_rcl

!------------------------------------------------------------------------------!

  subroutine select_power_BornAmpl_rcl(npr,cid,power)

    integer,          intent(in) :: npr
    integer,          intent(in) :: power
    character(len=*), intent(in) :: cid

    integer :: pr,legs,ord

    call processes_generated_warning_rcl('select_power_BornAmpl_rcl')

    ! get internal process index
    call get_pr(npr, 'select_power_BornAmpl_rcl', pr)

    ! check power
    legs = prs(pr)%legs
    if (power .lt. 0 .or. power .gt. legs-2) then
      call warning_rcl('select_power_BornAmpl_rcl called with a possibly ' // &
                       'invalid power and might have no effect. ' // &
                       'The power should be between 0 and number of legs-2.')
    endif

    ! get internal fundamental id
    call get_ord(cid,'select_power_BornAmpl_rcl',ord)

    prs(pr)%powsel(power,ord,0) = 1

  end subroutine select_power_BornAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine unselect_power_BornAmpl_rcl(npr,cid,power)

    integer,          intent(in) :: npr
    integer,          intent(in) :: power
    character(len=*), intent(in) :: cid

    integer :: pr,legs,ord

    call processes_generated_warning_rcl('unselect_power_BornAmpl_rcl')

    ! get internal process index
    call get_pr(npr, 'unselect_all_powers_BornAmpl_rcl', pr)

    ! check power
    legs = prs(pr)%legs
    if (power .lt. 0 .or. power .gt. legs-2) then
      call warning_rcl('unselect_power_BornAmpl_rcl called with a possibly ' // &
                       'invalid power and might have no effect. ' //&
                       'The power should be between 0 and number of legs-2.')
    endif

    ! get internal fundamental id
    call get_ord(cid, 'unselect_power_BornAmpl_rcl', ord)

    prs(pr)%powsel(power,ord,0) = 0

  end subroutine unselect_power_BornAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine select_all_powers_BornAmpl_rcl(npr)

    integer, intent(in) :: npr
    integer             :: pr

    call processes_generated_warning_rcl('select_all_powers_BornAmpl_rcl')

    ! get internal process index
    call get_pr(npr,'select_all_powers_BornAmpl_rcl',pr)

    prs(pr)%powsel(0:,:,0) = 1

  end subroutine select_all_powers_BornAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine unselect_all_powers_BornAmpl_rcl(npr)

    integer, intent(in) :: npr
    integer             :: pr

    call processes_generated_warning_rcl('unselect_all_powers_BornAmpl_rcl')

    ! get internal process index
    call get_pr(npr, 'unselect_all_powers_BornAmpl_rcl', pr)

    prs(pr)%powsel(0:,:,0) = 0

  end subroutine unselect_all_powers_BornAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine select_power_LoopAmpl_rcl(npr,cid,power)

    integer,          intent(in) :: npr,power
    character(len=*), intent(in) :: cid

    integer :: pr,legs,ord

    call processes_generated_warning_rcl('select_power_LoopAmpl_rcl')

    ! get internal process index
    call get_pr(npr, 'select_power_LoopAmpl_rcl', pr)

    ! check power
    legs = prs(pr)%legs
    if (power .lt. 0 .or. power .gt. legs) then
      call warning_rcl('select_power_LoopAmpl_rcl called with a possibly ' // &
                       'invalid power and might have no effect. ' //&
                       'The power should be between 0 and number of legs.')
    endif

    ! get internal fundamental id
    call get_ord(cid,'select_power_LoopAmpl_rcl',ord)

    prs(pr)%powsel(power,ord,1) = 1

  end subroutine select_power_LoopAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine unselect_power_LoopAmpl_rcl(npr,cid,power)

    integer,          intent(in) :: npr,power
    character(len=*), intent(in) :: cid

    integer :: pr,legs,ord

    call processes_generated_warning_rcl('unselect_power_LoopAmpl_rcl')

    ! get internal process index
    call get_pr(npr,'unselect_power_LoopAmpl_rcl',pr)

    ! check power
    legs = prs(pr)%legs
    if (power .lt. 0 .or. power.gt.legs) then
      call warning_rcl('unselect_power_LoopAmpl_rcl called with a possibly ' // &
                       'invalid power and might have no effect. ' //&
                       'The power should be between 0 and number of legs.')
    endif

    ! get internal fundamental id
    call get_ord(cid,'unselect_power_LoopAmpl_rcl',ord)

    prs(pr)%powsel(power,ord,1) = 0

  end subroutine unselect_power_LoopAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine select_all_powers_LoopAmpl_rcl(npr)

    integer, intent(in) :: npr
    integer             :: pr

    call processes_generated_warning_rcl('select_all_powers_LoopAmpl_rcl')

    ! get internal process index
    call get_pr(npr,'select_all_powers_LoopAmpl_rcl',pr)

    prs(pr)%powsel(0:,:,1) = 1

  end subroutine select_all_powers_LoopAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine unselect_all_powers_LoopAmpl_rcl(npr)

    integer, intent(in) :: npr
    integer             :: pr

    call processes_generated_warning_rcl('unselect_all_powers_LoopAmpl_rcl')

    ! get internal process index
    call get_pr(npr,'unselect_all_powers_LoopAmpl_rcl',pr)

    prs(pr)%powsel(0:,:,1) = 0

  end subroutine unselect_all_powers_LoopAmpl_rcl

!------------------------------------------------------------------------------!

  subroutine split_collier_cache_rcl(npr,n)

    ! This subroutine splits the cache of collier for process "npr"
    ! in "n" parts.

    integer,  intent(in) :: npr,n
    integer, allocatable :: nCacheTmp(:)
    logical, allocatable :: cacheOnTmp(:)
    integer              :: prin,pr

    if (cache_mode .ne. 1) then
      call error_rcl('Splitting caches only works in cache_mode=1.', &
                     where='set_collier_cache_rcl')
    end if
    call processes_generated_warning_rcl('set_collier_cache_rcl')

    ! get internal process index
    call get_pr(npr,'set_collier_cache_rcl',prin)

    if (prs(prin)%crosspr .ne. 0) then
      pr = prs(prin)%crosspr
    else
      pr = prin
    end if

    if (n.lt.0) then
      call error_rcl('Number of caches needs to be larger equal 0.', &
                     where='set_collier_cache_rcl')
    endif

    if (.not.allocated(nCache)) then
      allocate(nCache(prTot))
      nCache = 1
    else if (size(nCache) .ne. prTot) then
      allocate(nCacheTmp(size(nCache)))
      nCacheTmp(:) = nCache(:)
      deallocate(nCache)
      allocate(nCache(prTot))
      nCache = 1
      nCache(1:size(nCacheTmp)) = nCacheTmp(:)
    endif

    if (.not.allocated(cacheOn)) then
      allocate(cacheOn(1:prTot))
      cacheOn = .true.
    else if (size(cacheOn) .ne. prTot) then
      allocate(cacheOnTmp(size(cacheOn)))
      cacheOnTmp(:) = cacheOn(:)
      deallocate(cacheOn)
      allocate(cacheOn(prTot))
      cacheOn = .true.
      cacheOn(1:size(cacheOnTmp)) = cacheOnTmp(:)
    endif

    if (n.eq.0) then
      cacheOn(pr) = .false.
    else
      nCache(pr) = n
    endif

  end subroutine split_collier_cache_rcl

!------------------------------------------------------------------------------!

end module process_definition_rcl

!------------------------------------------------------------------------------!
