!******************************************************************************!
!                                                                              !
!    input_rcl.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module input_rcl

!------------------------------------------------------------------------------!

  use globals_rcl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine set_qcd_rescaling_rcl(val)

    ! Set if qcd rescaling from RECOLA1 is enabled. Enabling the rescaling only
    ! makes sense for the Standard-Model-like models.

    use modelfile, only: has_feature_mdl

    logical, intent(in) :: val

    if (val .eqv. .true. .and. .not. has_feature_mdl('qcd_rescaling')) then
      call error_rcl('Cannot enable qcd_rescaling, modelfile does not support it.', &
                     where='set_qcd_rescaling_rcl')
    end if
    qcd_rescaling = val

  end subroutine set_qcd_rescaling_rcl

!------------------------------------------------------------------------------!

  subroutine set_fermionloop_optimization_rcl(val)

    ! Enable or disable the fermionloop optimization
    ! Disabled by default.

    logical, intent(in) :: val

    fermion_loop_opt = val

  end subroutine set_fermionloop_optimization_rcl

!------------------------------------------------------------------------------!

  subroutine set_helicity_optimization_rcl(val)

    ! Enable or disable the helicity optimization for massless fermions
    ! Enabled by default.

    logical, intent(in) :: val

    helicity_opt = val

  end subroutine set_helicity_optimization_rcl

!------------------------------------------------------------------------------!

  subroutine set_colour_optimization_rcl(val)

    ! Set the colour optimization level to 0, 1 or 2.

    integer, intent(in) :: val

    colour_optimization = val

  end subroutine set_colour_optimization_rcl

!------------------------------------------------------------------------------!

  subroutine set_compute_A12_rcl(val)

    ! Set whether to compute the squared loop contribution A1^2.

    logical, intent(in) :: val

    compute_A12 = val

  end subroutine set_compute_A12_rcl

!------------------------------------------------------------------------------!

  subroutine set_masscut_rcl(m)

    ! Sets mass limit below which particles are considered massless.

    use modelfile, only: set_masscut_mdl

    real(dp), intent(in) :: m

    call set_masscut_mdl(m)

  end subroutine set_masscut_rcl

!------------------------------------------------------------------------------!

  subroutine set_longitudinal_polarization_rcl(val, nlo)

    ! Set the flag for the polarization of external photons (gluons)

    integer, intent(in) :: val
    logical, intent(in), optional :: nlo

    longitudinal = val
    if (present(nlo)) then
      longitudinal_nlo = nlo
    end if

  end subroutine set_longitudinal_polarization_rcl

!------------------------------------------------------------------------------!

  subroutine set_light_particle_rcl(p)

    ! Tag a particle as a light one, omitting mass terms whenever possible
    ! = mass regularization

    use modelfile, only: set_light_particle_mdl

    character(*), intent(in) :: p

    call set_light_particle_mdl(p)

  end subroutine set_light_particle_rcl

!------------------------------------------------------------------------------!

  subroutine unset_light_particle_rcl(p)

    ! Untag a particle as a light one, keeping the full mass dependence

    use modelfile, only: unset_light_particle_mdl

    character(*), intent(in) :: p

    call unset_light_particle_mdl(p)

  end subroutine unset_light_particle_rcl

!------------------------------------------------------------------------------!

  subroutine use_dim_reg_soft_rcl

    call deprecated_warning_rcl('use_dim_reg_soft_rcl')

  end subroutine use_dim_reg_soft_rcl

!------------------------------------------------------------------------------!

  subroutine use_mass_reg_soft_rcl(m)

    real(dp), intent(in) :: m

    call deprecated_warning_rcl('use_mass_reg_soft_rcl')

  end subroutine use_mass_reg_soft_rcl

!------------------------------------------------------------------------------!

  subroutine use_recola_base_rcl(rb)

    ! Set whether current expressions are computed by recola or by the model
    ! file.

    logical, intent(in) :: rb

    use_recola_base = rb

  end subroutine use_recola_base_rcl

!------------------------------------------------------------------------------!

  subroutine set_mass_reg_soft_rcl(m)

    real(dp), intent(in) :: m

    call deprecated_warning_rcl('set_mass_reg_soft_rcl')

  end subroutine set_mass_reg_soft_rcl

!------------------------------------------------------------------------------!

  subroutine set_delta_uv_rcl(d)

    ! This subroutine sets the UV-pole
    ! "deltaUV" = 1/ep - EulerConstant + ln(4*pi) to "d"

    use modelfile, only: set_delta_uv_mdl

    real(dp), intent(in) :: d

    if (processes_generated .and. dynamic_settings .eq. 0) &
      call warning_rcl('Call has no effect, dynamic_settings disabled.', &
                       where='set_delta_uv_rcl')
    call set_delta_uv_mdl(d)

    if (processes_generated) changed_DeltaUV = .true.

    ! cache input for later setting
    if (dynamic_settings .ge. 1 .and. .not. processes_generated) &
      DeltaUV_cache = d

  end subroutine set_delta_uv_rcl

!------------------------------------------------------------------------------!

  subroutine get_delta_uv_rcl(d)

    ! This subroutine gets the UV-pole
    ! "deltaUV" = 1/ep - EulerConstant + ln(4*pi)

    use modelfile, only: get_delta_uv_mdl

    real(dp), intent(out) :: d

    d = get_delta_uv_mdl()

  end subroutine get_delta_uv_rcl

!------------------------------------------------------------------------------!

  subroutine set_delta_ir_rcl(d,d2)

    ! This subroutine sets the IR-pole
    ! "deltaIR" = 1/ep - EulerConstant + ln(4*pi) to "d"

    use modelfile, only: set_delta_ir_mdl

    real(dp), intent(in) :: d,d2

    if (processes_generated .and. dynamic_settings .eq. 0) &
      call warning_rcl('Call has no effect, dynamic_settings disabled.', &
                       where='set_delta_ir_mdl')
    call set_delta_ir_mdl(d,d2)

    if (processes_generated) changed_DeltaIR = .true.

    ! cache input for later setting
    if (dynamic_settings .ge. 1 .and. .not. processes_generated) then
      DeltaIR_cache = d
      DeltaIR2_cache = d2
    end if

  end subroutine set_delta_ir_rcl

!------------------------------------------------------------------------------!

  subroutine get_delta_ir_rcl(d,d2)

    ! This subroutine gets the IR-pole
    ! "deltaIR" = 1/ep - EulerConstant + ln(4*pi)

    use modelfile, only: get_delta_ir_mdl

    real(dp), intent(out) :: d,d2

    call get_delta_ir_mdl(d,d2)

  end subroutine get_delta_ir_rcl
!------------------------------------------------------------------------------!

  subroutine set_mu_uv_rcl(m)

    ! This subroutine sets UV-mass "muUV" to "m"

    use modelfile, only: set_mu_uv_mdl

    real(dp), intent(in) :: m

    if (processes_generated .and. dynamic_settings .eq. 0) &
      call warning_rcl('Call has no effect, dynamic_settings disabled.', &
                       where='set_mu_uv_mdl')
    call set_mu_uv_mdl(m)

    if (processes_generated) changed_muUV = .true.

    ! cache input for later setting
    if (dynamic_settings .ge. 1 .and. .not. processes_generated) &
      muUV_cache = m

  end subroutine set_mu_uv_rcl

!------------------------------------------------------------------------------!

  subroutine get_mu_uv_rcl(m)

    ! This subroutine gets UV-mass "muUV"

    use modelfile, only: get_mu_uv_mdl

    real(dp), intent(out) :: m

    m = get_mu_uv_mdl()

  end subroutine get_mu_uv_rcl

!------------------------------------------------------------------------------!

  subroutine set_mu_ir_rcl(m)

    ! This subroutine sets IR-mass "muIR" to "m"

    use modelfile, only: set_mu_ir_mdl

    real(dp), intent(in) :: m

    if (processes_generated .and. dynamic_settings .eq. 0) &
      call warning_rcl('Call has no effect, dynamic_settings disabled.', &
                       where='set_mu_ir_mdl')
    call set_mu_ir_mdl(m)

    if (processes_generated) changed_muIR = .true.

    ! cache input for later setting
    if (dynamic_settings .ge. 1 .and. .not. processes_generated) &
      muIR_cache = m

  end subroutine set_mu_ir_rcl

!------------------------------------------------------------------------------!

  subroutine get_mu_ir_rcl(m)

    ! This function gets IR-mass "muIR"

    use modelfile, only: get_mu_ir_mdl

    real(dp), intent(out) :: m

    m =  get_mu_ir_mdl()

  end subroutine get_mu_ir_rcl

!------------------------------------------------------------------------------!

  subroutine set_mu_ms_rcl(m)

    ! This subroutine sets MS-mass "muMS" to "m"

    use modelfile, only: set_mu_ms_mdl

    real(dp), intent(in) :: m

    call set_mu_ms_mdl(m)

  end subroutine set_mu_ms_rcl

!------------------------------------------------------------------------------!

  subroutine get_mu_ms_rcl(m)

    ! This subroutine gets MS-mass "muMS" to "m"

    use modelfile, only: get_mu_ms_mdl

    real(dp), intent(out) :: m

    m = get_mu_ms_mdl()

  end subroutine get_mu_ms_rcl

!------------------------------------------------------------------------------!

  subroutine get_renormalization_scale_rcl(mu)

    real(dp), intent(out) :: mu

    call warning_rcl('Method is deprecated, calling get_mu_ms_rcl instead.', &
                     where='get_renormalization_scale_rcl')
    call get_mu_ms_rcl(mu)

  end subroutine get_renormalization_scale_rcl

!------------------------------------------------------------------------------!

  subroutine get_flavour_scheme_rcl(Nf)

    ! This subroutine extracts the actual value of the flavour-scheme
    ! for the renormalization of alpha_s ("Nf").

    integer, intent(out) :: Nf

    Nf = Nfren

  end subroutine get_flavour_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine set_complex_mass_scheme_rcl

    use modelfile, only: set_complex_mass_scheme_mdl
    call set_complex_mass_scheme_mdl

  end subroutine set_complex_mass_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine set_on_shell_scheme_rcl

    use modelfile, only: set_on_shell_scheme_mdl
    call set_on_shell_scheme_mdl

  end subroutine set_on_shell_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine set_momenta_correction_rcl(mc)

    ! This routine regulates the correction of external momenta given
    ! by the user, when on-shell conditions or momentum conservation are
    ! not fulfilled.
    ! mc = .true.  -> recola tries to correct the momenta
    ! mc = .false. -> recola does not try to correct the momenta
    ! When the correction does not work or is turned off, all
    ! computed amplitudes with bad momenta are set to zero.

    logical, intent(in) :: mc

    momenta_correction = mc

  end subroutine set_momenta_correction_rcl

!------------------------------------------------------------------------------!

  subroutine set_print_level_amplitude_rcl(n)

    ! This subroutine sets the level for printing the amplitude to "n":
    ! n = 0 -> nothing is printed.
    ! n = 1 -> The non vanishining amplitudes A(c,h) are printed at LO
    !          and NLO (if NLO is computed) for each coulour structure
    !          "c" and helicity "h"
    ! n = 2 -> In addition to n=1, the NLO (if computed) contributions
    !          to A(c,h) from bare-loop, counterterms and rational part
    !          are also printed.


    integer, intent(in) :: n

    if (n.lt.0.or.n.gt.2) then
      call error_rcl('Wrong value for argument n.', &
                     where='set_print_level_amplitude_rcl(n)')
    endif

    writeMat = n

  end subroutine set_print_level_amplitude_rcl

!------------------------------------------------------------------------------!

  subroutine set_print_level_squared_amplitude_rcl(n)

    ! This subroutine sets the level for printing the squared amplitude
    ! to "n":
    ! n = 0 -> nothing is printed.
    ! n = 1 -> The non vanishining squared amplitudes A2
    !          summed/averaged over colour and helicities are
    !          printed at LO and NLO (if NLO is computed).
    ! n = 2 -> In addition to n=1, the NLO (if computed) contributions
    !          to A2 from bare-loop, counterterms and rational part
    !          are also printed.
    ! n = 3 -> In addition to n=2, also the squared amplitudes A2h
    !          summed/averaged over colour are printed at LO and NLO
    !          (if NLO is computed) for each helicity "h".

    integer, intent(in) :: n

    if (n .lt. 0 .or. n .gt. 3) then
      call error_rcl('Wrong value for argument n.', &
                     where='set_print_level_squared_amplitude_rcl(n)')
    endif

    writeMat2 = n

  end subroutine set_print_level_squared_amplitude_rcl

!------------------------------------------------------------------------------!

  subroutine set_print_level_correlations_rcl(n)

    ! This subroutine sets the level for printing colour correlation
    ! (if computed) to "n":
    ! n = 0 -> nothing is printed.
    ! n = 1 -> The non vanishining colour-correlated Born squared
    !          amplitudes A2c(n,m) are printed for each emitter "n" and
    !          spectator "m".

    integer, intent(in) :: n

    if (n .lt. 0 .or. n .gt. 1) then
      call error_rcl('Wrong value for argument n.', &
                     where='set_print_level_correlations_rcl(n)')
    endif

    writeCor = n

  end subroutine set_print_level_correlations_rcl

!------------------------------------------------------------------------------!

  subroutine set_print_level_parameters_rcl(n)

    ! This subroutine sets the level for printing parameters of the theory to "n":
    ! n = 0 -> nothing is printed.
    ! n = 1 -> tree-level input parameters are printed.
    ! n = 2 -> tree-level input + derived parameters are printed.
    ! n = 3 -> input + derived + counterterm parameters are printed.
    ! n = 4 -> only counterterm parameters are printed.

    use modelfile, only: set_print_regularization_scheme_mdl
    integer, intent(in) :: n

    select case (n)
    case (0)
      print_model_external_parameters = .false.
      print_model_internal_parameters = .false.
      print_model_counterterms = .false.
      call set_print_regularization_scheme_mdl(.false.)
    case (1)
      print_model_external_parameters = .true.
      print_model_internal_parameters = .false.
      print_model_counterterms = .false.
      call set_print_regularization_scheme_mdl(.false.)
    case (2)
      print_model_external_parameters = .true.
      print_model_internal_parameters = .true.
      print_model_counterterms = .false.
      call set_print_regularization_scheme_mdl(.false.)
    case (3)
      print_model_external_parameters = .true.
      print_model_internal_parameters = .true.
      print_model_counterterms = .true.
      call set_print_regularization_scheme_mdl(.true.)
    case (4)
      print_model_external_parameters = .false.
      print_model_internal_parameters = .false.
      print_model_counterterms = .true.
      call set_print_regularization_scheme_mdl(.true.)
    case default
      call error_rcl('Wrong value for argument n.', &
                     where='set_print_level_parameters_rcl(n)')
    end select

  end subroutine set_print_level_parameters_rcl

!------------------------------------------------------------------------------!

  subroutine set_draw_level_branches_rcl(n)

    ! This subroutine sets the level for the generation of a .tex file
    ! to draw the branches of the processes:
    ! n=0 -> the .tex file is not generated.
    ! n=1 -> the .tex file is generated without color structures and
    !        without a legend
    ! n=2 -> the .tex file is generated with color structures, but
    !        without a legend
    ! n=3 -> the .tex file is generated with color structures and with
    !        a legend


    integer, intent(in) :: n

    call processes_generated_warning_rcl('set_draw_level_branches_rcl')
    if (n .lt. 0 .or. n .gt. 2) then
      call error_rcl('Wrong value for argument n.', &
                     where='set_draw_level_branches_rcl')
    endif

    draw = n

  end subroutine set_draw_level_branches_rcl

!------------------------------------------------------------------------------!

  subroutine set_print_level_RAM_rcl(n)

    integer, intent(in) :: n

    call no_support_warning_rcl('set_print_level_RAM_rcl')

  end subroutine set_print_level_RAM_rcl

!------------------------------------------------------------------------------!

  subroutine scale_coupling3_rcl(fac,pa1,pa2,pa3)

    complex(dp),      intent(in) :: fac
    character(len=*), intent(in) :: pa1,pa2,pa3

    call no_support_warning_rcl('scale_coupling3_rcl')

  end subroutine scale_coupling3_rcl

!------------------------------------------------------------------------------!

  subroutine scale_coupling4_rcl(fac,pa1,pa2,pa3,pa4)

    complex(dp),      intent(in) :: fac
    character(len=*), intent(in) :: pa1,pa2,pa3,pa4

    call no_support_warning_rcl('scale_coupling4_rcl')

  end subroutine scale_coupling4_rcl

!------------------------------------------------------------------------------!

  subroutine switchoff_coupling3_rcl(pa1,pa2,pa3,xlp_in)
    character(len=*),  intent(in) :: pa1,pa2,pa3
    integer, optional, intent(in) :: xlp_in
    character(len=10), dimension(3) :: parr

    parr(1) = pa1
    parr(2) = pa2
    parr(3) = pa3
    if (present(xlp_in)) then
      call switchoff_couplingN_rcl(parr,3,xlp_in)
    else
      call switchoff_couplingN_rcl(parr,3)
    end if

  end subroutine switchoff_coupling3_rcl

!------------------------------------------------------------------------------!

  subroutine switchoff_coupling4_rcl(pa1,pa2,pa3,pa4,xlp_in)

    character(len=*),  intent(in) :: pa1,pa2,pa3,pa4
    integer, optional, intent(in) :: xlp_in
    character(len=10), dimension(4) :: parr

    parr(1) = pa1
    parr(2) = pa2
    parr(3) = pa3
    parr(4) = pa4
    if (present(xlp_in)) then
      call switchoff_couplingN_rcl(parr,4,xlp_in)
    else
      call switchoff_couplingN_rcl(parr,4)
    end if

  end subroutine switchoff_coupling4_rcl

!------------------------------------------------------------------------------!

  subroutine switchoff_coupling5_rcl(pa1,pa2,pa3,pa4,pa5,xlp_in)

    character(len=*),  intent(in) :: pa1,pa2,pa3,pa4,pa5
    integer, optional, intent(in) :: xlp_in
    character(len=10), dimension(5) :: parr

    parr(1) = pa1
    parr(2) = pa2
    parr(3) = pa3
    parr(4) = pa4
    parr(5) = pa5
    if (present(xlp_in)) then
      call switchoff_couplingN_rcl(parr,5,xlp_in)
    else
      call switchoff_couplingN_rcl(parr,5)
    end if

  end subroutine switchoff_coupling5_rcl

!------------------------------------------------------------------------------!

  subroutine switchoff_coupling6_rcl(pa1,pa2,pa3,pa4,pa5,pa6,xlp_in)

    character(len=*),  intent(in) :: pa1,pa2,pa3,pa4,pa5,pa6
    integer, optional, intent(in) :: xlp_in
    character(len=10), dimension(6) :: parr

    parr(1) = pa1
    parr(2) = pa2
    parr(3) = pa3
    parr(4) = pa4
    parr(5) = pa5
    parr(6) = pa6
    if (present(xlp_in)) then
      call switchoff_couplingN_rcl(parr,6,xlp_in)
    else
      call switchoff_couplingN_rcl(parr,6)
    end if

  end subroutine switchoff_coupling6_rcl

!------------------------------------------------------------------------------!

  subroutine switchoff_couplingN_rcl(pan,nmax,xlp_in)
    use modelfile, only: is_particle_model_init_mdl,   &
                         get_particle_id_mdl,          &
                         set_particle_model_init_mdl,  &
                         get_vertex_mdl,               &
                         get_vertex_branch_status_mdl, &
                         init_particles_mdl,           &
                         set_particle_model_init_mdl,  &
                         is_model_init_mdl,            &
                         is_model_filled_mdl,          &
                         init_vertices_mdl,            &
                         fill_vertices_mdl
    use class_vertices, only: vertices

    integer, intent(in) :: nmax
    character(len=10), dimension(nmax),  intent(in) :: pan
    integer, optional,                   intent(in) :: xlp_in

    integer :: i,xlp,pout,vx
    integer, dimension(nmax)   :: ida,pp
    integer, dimension(nmax-1) :: pin

    if (present(xlp_in)) then
      xlp = xlp_in
    else
      xlp = -1
    end if

    if (.not. is_particle_model_init_mdl()) then
      call init_particles_mdl()
      call set_particle_model_init_mdl(.true.)
    end if
    if ( .not. is_model_init_mdl()) then
      call init_vertices_mdl
    end if
    if ( .not. is_model_filled_mdl()) then
      call fill_vertices_mdl
    end if

    do i = 1, nmax
      pp(i) = get_particle_id_mdl(pan(i))
    end do

    do i  =  1, nmax
      ida(i) = i
    end do

    do
      do i = 1, nmax
        if (ida(i) .eq. nmax) then
          pout = pp(i)
        else
          pin(ida(i)) = pp(i)
        end if
      end do
      call get_vertex_mdl(pin,pout,vx)
      select case (xlp)
      case(0,1)
        if (allocated(vertices(vx)%branch)) &
          deallocate(vertices(vx)%branch)
      case(2)
        if (allocated(vertices(vx)%branchCT)) &
          deallocate(vertices(vx)%branchCT)
      case(3)
        if (allocated(vertices(vx)%branchR2)) &
          deallocate(vertices(vx)%branchR2)
      case default
        if (allocated(vertices(vx)%branch)) &
          deallocate(vertices(vx)%branch)
        if (allocated(vertices(vx)%branchCT)) &
          deallocate(vertices(vx)%branchCT)
        if (allocated(vertices(vx)%branchR2)) &
          deallocate(vertices(vx)%branchR2)
      end select

      if (nextp(nmax,ida)) then
         cycle
      else
         exit
      end if
   end do

  end subroutine switchoff_couplingN_rcl

!------------------------------------------------------------------------------!

  subroutine switchon_resonant_selfenergies_rcl

    ! This subroutine sets the value of "resSE" to .true.

    call processes_generated_warning_rcl('switchon_resonant_selfenergies_rcl')
    resSE = .true.

  end subroutine switchon_resonant_selfenergies_rcl

!------------------------------------------------------------------------------!

  subroutine switchoff_resonant_selfenergies_rcl

    call processes_generated_warning_rcl('switchoff_resonant_selfenergies_rcl')
    resSE = .false.

  end subroutine switchoff_resonant_selfenergies_rcl

!------------------------------------------------------------------------------!

  subroutine set_dynamic_settings_rcl(n)
    integer, intent(in) :: n


    call processes_generated_error_rcl('set_dynamic_settings_rcl')

    if (n .ge. 1) then
      ! cache set results
      call get_mu_uv_rcl(muUV_cache)
      call get_mu_ir_rcl(muIR_cache)
      call get_delta_uv_rcl(DeltaUV_cache)
      call get_delta_ir_rcl(DeltaIR_cache,DeltaIR2_cache)

      dynamic_settings = 0 ! prevent overwriting cache values
      ! set unconventional parameters
      call set_mu_uv_rcl(30.1d0)
      call set_mu_ir_rcl(40.3d0)
      call set_delta_uv_rcl(17d0)
      call set_delta_ir_rcl(43.9d0,7d0)
    end if

    dynamic_settings = n

  end subroutine set_dynamic_settings_rcl

!------------------------------------------------------------------------------!

  subroutine set_ifail_rcl(i)

    ! This subroutine sets the ifail flag to "i".
    ! The ifail flag regulates the behaviour of Recola in case of error.
    ! On input:
    ! ifail = -1 -> the code does not stop, when an error occurs
    ! ifail =  0 -> the code stops, when an error occurs

    integer, intent(in) :: i

    if (i.ne.0.and.i.ne.-1) then
      call error_rcl('Wrong value for argument i.', &
                     where='set_ifail_rcl(i)')
    endif

    ifail = i

  end subroutine set_ifail_rcl

!------------------------------------------------------------------------------!

  subroutine get_ifail_rcl(i)

    ! This subroutine extracts the actual value of the ifail flag in the
    ! output argument "i".
    ! The ifail flag regulates the behaviour of Recola in case of error.
    ! On output:
    ! ifail = 0 -> no errors occured
    ! ifail = 1 -> an error occured because of a wrong call of
    !              subroutines by the user
    ! ifail = 2 -> an error occured because of an internal bug of Recola

    integer, intent(out) :: i

    if (ifail.le.0) then; i = 0
    else;                 i = ifail
    endif

  end subroutine get_ifail_rcl

!------------------------------------------------------------------------------!

  subroutine set_iwarn_rcl(i)

    ! This subroutine sets the iwarn flag to "i".
    ! The iwarn flag regulates the behaviour of Recola in case of error.
    ! On input:
    ! iwarn = -1 -> the code does not stop, when an error occurs
    ! iwarn =  0 -> the code stops, when a warning occurs

    integer, intent(in) :: i

    if (i.ne.0.and.i.ne.-1) then
      call error_rcl('Wrong value for argument i.', &
                     where='set_iwarn_rcl(i)')
    endif

    iwarn = i

  end subroutine set_iwarn_rcl

!------------------------------------------------------------------------------!

  subroutine get_iwarn_rcl(i)

    ! This subroutine extracts the actual value of the iwarn flag in the
    ! output argument "i".
    ! The iwarn flag regulates the behaviour of Recola in case of error.
    ! On output:
    ! iwarn = 0 -> no warning occured
    ! iwarn = 1 -> a warning occured because of a wrong call of
    !              subroutines by the user
    integer, intent(out) :: i

    if (iwarn.le.0) then; i = 0
    else;                 i = iwarn
    endif

  end subroutine get_iwarn_rcl

!------------------------------------------------------------------------------!

  subroutine set_collier_mode_rcl (mode)
    use collier, only: SetMode_cll

    ! This subroutine sets the collier mode to "mode".
    ! 1 -> COLI
    ! 2 -> DD
    ! 3 -> COLI+DD

    integer, intent(in) :: mode

    if (mode .lt. 1 .or. mode .gt. 3) then
      call error_rcl('The collier mode ' // trim(to_str(mode)) // &
                      ' is not supported.')
    endif

    collier_mode = mode

    if (processes_generated) then
      call SetMode_cll(collier_mode)
    end if

  end subroutine set_collier_mode_rcl

!------------------------------------------------------------------------------!

  subroutine set_collier_output_dir_rcl (dir)

    ! This subroutine sets the collier output directory to "dir".
    ! With "dir" = 'default', the output directory of collier will be the
    ! one choosen by collier per default.
    ! For all other values, the output directory of collier will be set
    ! to 'dir', where 'dir' can be a relative or absolute path.

    use modelfile, only: set_collier_output_dir_mdl

    character(*), intent(in) :: dir

    call processes_generated_warning_rcl('set_collier_output_dir_rcl')

    if (len(trim(dir)) .gt. 999) then
      call error_rcl('The name of collier output directory is too long.')
    endif

    collier_output_dir = trim(dir)
    call set_collier_output_dir_mdl(trim(dir))

  end subroutine set_collier_output_dir_rcl

!------------------------------------------------------------------------------!

  subroutine set_cache_mode_rcl (mode)

    ! This subroutine sets the cache mode to
    ! mode = 0: cache entirely disabled
    ! mode = 1: default, local cache for every process, cache can be further
    !           split or disabled using split_collier_cache_rcl.
    ! mode = 2: global cache for all processes. The user initializes a desired
    !           number of caches and takes care of switching between the caches.

    integer, intent(in) :: mode

    call processes_generated_warning_rcl('set_cache_mode_rcl')

    if (mode .lt. 0 .or. mode .gt. 2) then
      call error_rcl('Cache mode only allows the values 0,1,2', &
                     where='set_cache_mode_rcl')
    end if
    cache_mode = mode
    if (cache_mode .eq. 2) then
      nCacheGlobal = 1
      nprCall = 0
    end if

  end subroutine set_cache_mode_rcl

!------------------------------------------------------------------------------!

  subroutine set_global_cache_rcl (ncache)

    ! Sets the number of global caches. Any number bigger 0 is allowed

    integer, intent(in) :: ncache

    call processes_generated_error_rcl('set_cache_mode_rcl')

    if (cache_mode .ne. 2) then
      call error_rcl('Global caches can only be set in cache_mode 2.', &
                     where='set_global_cache_rcl')
    end if
    nCacheGlobal = ncache
    if (allocated(gcch)) deallocate(gcch)
    allocate(gcch(nCacheGlobal))

  end subroutine set_global_cache_rcl

!------------------------------------------------------------------------------!

  subroutine switch_global_cache_rcl (cache)

    ! Switches to the cache with number ncache. Any number bigger 0 is allowed.
    ! The sequence of compute_amplitude calls at NLO assicated to a global
    ! cache must match exactly the one when switched to the first time to that
    ! global cache.
    ! The user is resposible for compatibility with the call sequence, but
    ! consistency checks are performed and errors will make recola stop.

     use collier, only: InitEvent_cll

    integer, intent(in) :: cache

    if (cache_mode .ne. 2) then
      call error_rcl('Global caches can only be used in cache_mode 2.', &
                     where='switch_global_cache_rcl')
    end if
    if (cache .lt. 1 .or. cache .gt. nCacheGlobal) then
      call error_rcl('Cache out of boundary.', &
                     where='switch_global_cache_rcl')

    end if
    nprCall = 0
    activeCache = cache
    call InitEvent_cll(cache)
    if (allocated(gcch(activeCache)%hist)) then
      gcch(activeCache)%constructed = .true.
    end if

  end subroutine switch_global_cache_rcl

!------------------------------------------------------------------------------!

  subroutine set_compute_ir_poles_rcl(mode)

    integer, intent(in) :: mode

    if (mode .lt. 0 .or. mode .gt. 2) then
      call error_rcl('Invalid mode. Given: ' // to_str(mode) // &
                     '. Allowed values: 0,1,2.', &
                     where='set_compute_ir_poles_rcl')
    end if
    if (mode .eq. 2) then
      call error_rcl('mode=1 not yet supported.', &
                     where='set_compute_ir_poles_rcl')
    end if
    compute_ir_poles = mode

  end subroutine set_compute_ir_poles_rcl

!------------------------------------------------------------------------------!

  subroutine set_output_file_rcl(x)

    character(*), intent(in) :: x
    character(len=99)        :: xtri

    if (len(trim(x)).gt.99) then
      write(xtri,'(a99)') x
      call warning_rcl('The name of the output-file is too long and has ' // &
                       'been truncated to ' // trim(xtri),                   &
                        where='set_output_file_rcl')
    endif

    outputfile = trim(x)

  end subroutine set_output_file_rcl

!------------------------------------------------------------------------------!

  subroutine set_log_mem_rcl(logmem)

    ! This subroutine sets the switch for writing a memorylog.
    ! logmem = .false. -> memory is not logged is printed.
    ! logmem = .true -> memory is logged and written to memory_rcl.log in the
    !                   current working directory

    logical, intent(in) :: logmem

    log_mem = logmem

  end subroutine set_log_mem_rcl

!------------------------------------------------------------------------------!

  subroutine set_init_collier_rcl(initcollier)

    ! This subroutine sets the switch for initializing collier
    ! initcollier = .false. -> collier is not initialized
    ! initcollier = .true -> collier is initialized

    use modelfile, only: set_compute_si_mdl

    logical, intent(in) :: initcollier

    init_collier = initcollier

    ! disable computation of scalar integrals in the model file
    ! if collier is not initialized in recola
    call set_compute_si_mdl(initcollier)

  end subroutine set_init_collier_rcl

!------------------------------------------------------------------------------!

  subroutine set_crossing_symmetry_rcl(cs)

    ! This subroutine sets the switch for writing a memorylog.
    ! cs = .false. -> no crossing symmetry used when generating processes
    ! cs = .true.  -> crossing symmetry used when generating processes

    logical, intent(in) :: cs

    crossing_symmetry = cs

  end subroutine set_crossing_symmetry_rcl

!------------------------------------------------------------------------------!

  subroutine set_parameter_rcl(param,value)

    use modelfile, only: set_parameter_mdl

    character(*), intent(in) :: param
    complex(dp),  intent(in) :: value

    call openOutput
    call set_parameter_mdl(param,value)

  end subroutine set_parameter_rcl

!------------------------------------------------------------------------------!

  subroutine get_parameter_rcl(param,value)

    use modelfile, only: get_parameter_mdl

    character(*), intent(in) :: param
    complex(dp), intent(out) :: value

    value = get_parameter_mdl(param)

  end subroutine get_parameter_rcl

!------------------------------------------------------------------------------!

  subroutine set_renoscheme_rcl(ctparam,renoscheme)

    use modelfile, only: set_renoscheme_mdl

    character(*), intent(in) :: ctparam, renoscheme

    call set_renoscheme_mdl(ctparam,renoscheme)

  end subroutine set_renoscheme_rcl

!------------------------------------------------------------------------------!

  subroutine get_renoscheme_rcl(renoconst,renoscheme)

    use modelfile, only: get_renoscheme_mdl

    character(*),        intent(in) :: renoconst
    character(len=100), intent(out) :: renoscheme

    renoscheme = get_renoscheme_mdl(renoconst)

  end subroutine get_renoscheme_rcl

!------------------------------------------------------------------------------!

  subroutine set_resonant_particle_rcl(p)

    ! This subroutine marks particle "p" as resonant.

    use modelfile, only: set_resonant_particle_mdl

    character(*), intent(in) :: p

    call processes_generated_warning_rcl('set_resonant_particle_rcl')
    call set_resonant_particle_mdl(p)

  end subroutine set_resonant_particle_rcl

!------------------------------------------------------------------------------!

  subroutine set_quarkline_rcl (npr,q1,q2)

    ! applies the constraint that the quark line between the quark q1 and q2
    ! remains connected through all currents.

    integer, intent(in) :: q1,q2,npr
    integer             :: pr

    call processes_generated_warning_rcl('set_quarkline_rcl')
    call get_pr(npr,'set_quarkline_rcl',pr)

    prs(pr)%qflow(q1) = q2
    prs(pr)%qflow(q2) = q1

  end subroutine set_quarkline_rcl

!------------------------------------------------------------------------------!

  subroutine reset_vertices_rcl()

    use modelfile, only: reset_vertices_mdl
    use collier, only: SwitchOffCacheSystem_cll, &
                       SwitchOnCacheSystem_cll

    call SwitchOffCacheSystem_cll
    call reset_vertices_mdl
    call SwitchOnCacheSystem_cll

  end subroutine reset_vertices_rcl

!------------------------------------------------------------------------------!

  subroutine reset_couplings_rcl()

    use modelfile, only: reset_couplings_mdl
    use collier, only: SwitchOffCacheSystem_cll, &
                       SwitchOnCacheSystem_cll

    call SwitchOffCacheSystem_cll
    call reset_couplings_mdl
    call SwitchOnCacheSystem_cll

  end subroutine reset_couplings_rcl

!------------------------------------------------------------------------------!

  subroutine reset_ctcouplings_rcl()

    use modelfile, only: reset_ctcouplings_mdl
    use collier, only: SwitchOffCacheSystem_cll, &
                       SwitchOnCacheSystem_cll

    call SwitchOffCacheSystem_cll
    call reset_ctcouplings_mdl
    call SwitchOnCacheSystem_cll

  end subroutine reset_ctcouplings_rcl

!------------------------------------------------------------------------------!

  subroutine has_feature_rcl(feature, has_feature)

    use modelfile, only: has_feature_mdl

    character(*), intent(in)  :: feature
    logical,      intent(out) :: has_feature

    has_feature = has_feature_mdl(feature)

  end subroutine has_feature_rcl

!------------------------------------------------------------------------------!

  subroutine print_collier_statistics_rcl()

    use collier, only: printstatisticscoli_cll

    call openOutput
    call printstatisticscoli_cll(nx)

  end subroutine print_collier_statistics_rcl


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Dedicated Rept1l Methods  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Do not use the following methods unless you know what you are doing.
!------------------------------------------------------------------------------!

  subroutine set_compute_selfenergy_rcl(val)

    ! Allows selfenergy topologies on external legs
    ! -> should be used only for renormalization

    logical, intent(in) :: val

    compute_selfenergy = val

  end subroutine set_compute_selfenergy_rcl

!------------------------------------------------------------------------------!

  subroutine set_compute_selfenergy_offshell_rcl(val)

    ! Allows selfenergy topologies on external legs
    ! -> should be used only for renormalization

    logical, intent(in) :: val

    compute_selfenergy = val
    selfenergy_offshell_setup = val
    momenta_checks = .not. val
    momenta_correction = .not. val

  end subroutine set_compute_selfenergy_offshell_rcl

!------------------------------------------------------------------------------!

  subroutine set_compute_tadpole_rcl(val)

    ! Allows tadpole topologies
    ! -> should be used only for renormalization

    logical, intent(in) :: val

    compute_tadpole = val

  end subroutine set_compute_tadpole_rcl

!------------------------------------------------------------------------------!

  subroutine set_lp_rcl(lp, val)

    integer, intent(in) :: lp
    logical, intent(in) :: val

    lp_on(lp) = val

  end subroutine set_lp_rcl

!------------------------------------------------------------------------------!

  subroutine set_form_rcl(val)

    integer, intent(in) :: val

    frm = val

  end subroutine set_form_rcl

!------------------------------------------------------------------------------!

  subroutine set_all_couplings_active_rcl(val)

    logical, intent(in) :: val

    couplings_active_frm = val

  end subroutine set_all_couplings_active_rcl

!------------------------------------------------------------------------------!

  subroutine set_all_tree_couplings_active_rcl(val)

    logical, intent(in) :: val

    tree_couplings_active_frm = val

  end subroutine set_all_tree_couplings_active_rcl

!------------------------------------------------------------------------------!

  subroutine set_all_loop_couplings_active_rcl(val)

    logical, intent(in) :: val

    loop_couplings_active_frm = val

  end subroutine set_all_loop_couplings_active_rcl

!------------------------------------------------------------------------------!

  subroutine set_all_ct_couplings_active_rcl(val)

    logical, intent(in) :: val

    ct_couplings_active_frm = val

  end subroutine set_all_ct_couplings_active_rcl

!------------------------------------------------------------------------------!

  subroutine set_all_r2_couplings_active_rcl(val)

    logical, intent(in) :: val

    r2_couplings_active_frm = val

  end subroutine set_all_r2_couplings_active_rcl

!------------------------------------------------------------------------------!

  subroutine set_complex_mass_form_rcl(val)

    use modelfile, only: use_complex_mass_form_mdl

    logical, intent(in) :: val

    call use_complex_mass_form_mdl(val)

  end subroutine set_complex_mass_form_rcl

!------------------------------------------------------------------------------!

  subroutine get_complex_mass_form_rcl(cmfrm)

    use modelfile, only: get_complex_mass_form_mdl

    logical, intent(out) :: cmfrm

    cmfrm =  get_complex_mass_form_mdl()

  end subroutine get_complex_mass_form_rcl

!------------------------------------------------------------------------------!

  subroutine set_vertex_functions_rcl(val)

    ! set true if the algorithm should compute only 1PI loop graphs

    logical, intent(in) :: val

    vertex_functions = val

  end subroutine set_vertex_functions_rcl

!------------------------------------------------------------------------------!

  subroutine set_particle_ordering_rcl(val)

    ! set true/false if the algorithm should order (not order) particles

    logical, intent(in) :: val

    particle_ordering = val

  end subroutine set_particle_ordering_rcl

!------------------------------------------------------------------------------!

  subroutine set_masscut_form_rcl(val)

    ! enables the masscut in form amplitudes. Masses below masscut are
    ! considered massless.

    use modelfile, only: use_masscut_form_mdl

    logical, intent(in) :: val

    call use_masscut_form_mdl(val)

  end subroutine set_masscut_form_rcl

!------------------------------------------------------------------------------!

  subroutine set_cutparticle_rcl(particle_str)

    use modelfile, only: get_particle_id_mdl

    character(*), intent(in) :: particle_str

    cutparticle_selection = get_particle_id_mdl(particle_str)

  end subroutine set_cutparticle_rcl

!------------------------------------------------------------------------------!

  subroutine unset_cutparticle_rcl

    cutparticle_selection = -1

  end subroutine unset_cutparticle_rcl

!------------------------------------------------------------------------------!

  subroutine set_print_recola_logo_rcl(val)

    logical, intent(in) :: val

    print_recola_logo = val

  end subroutine set_print_recola_logo_rcl

!------------------------------------------------------------------------------!

end module input_rcl

!------------------------------------------------------------------------------!
