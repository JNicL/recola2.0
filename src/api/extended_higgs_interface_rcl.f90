!******************************************************************************!
!                                                                              !
!    extended_higgs_interface_rcl.f90                                          !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module extended_higgs_interface_rcl

!------------------------------------------------------------------------------!

  use globals_rcl, only: dp,pi,cone,cnul,error_rcl,warning_rcl,zerocut, &
                         check_support_models,processes_generated_warning_rcl
  use input_rcl, only: set_parameter_rcl, set_renoscheme_rcl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine check_support_thdm_interface(where)
    character(len=*), intent(in) :: where
    call check_support_models('THDM', where)
  end subroutine check_support_thdm_interface

!------------------------------------------------------------------------------!

  subroutine check_support_thdm_hs_interface(where)
    character(len=*), intent(in) :: where
    call check_support_models('THDM', 'HS', where)
  end subroutine check_support_thdm_hs_interface

!------------------------------------------------------------------------------!

  subroutine check_support_hs_interface(where)
    character(len=*), intent(in) :: where
    call check_support_models('HS',where)
  end subroutine check_support_hs_interface

!------------------------------------------------------------------------------!

  subroutine set_Z2_thdm_yukawa_type_rcl (ytype)

    ! This subroutine sets the yukawa type in the softly-broken Z2 symmetric
    ! THDM to "ytype".
    ! ytype = 1 : Type-I   h1u=h1d=h1l=0
    ! ytype = 2 : Type-II  h1u=h2d=h2l=0
    ! ytype = 3 : Type-X   h1u=h1d=h2l=0
    ! ytype = 4 : Type-Y   h1u=h2d=h1l=0

    integer, intent(in) :: ytype

    call check_support_thdm_interface('set_Z2_thdm_yukawa_type_rcl')
    call processes_generated_warning_rcl('set_Z2_thdm_yukawa_type_rcl')

    select case (ytype)
    case (1)
      call set_parameter_rcl("h1u", cnul)
      call set_parameter_rcl("h2u", cone)

      call set_parameter_rcl("h1d", cnul)
      call set_parameter_rcl("h2d", cone)

      call set_parameter_rcl("h1l", cnul)
      call set_parameter_rcl("h2l", cone)
    case (2)
      call set_parameter_rcl("h1u", cnul)
      call set_parameter_rcl("h2u", cone)

      call set_parameter_rcl("h2d", cnul)
      call set_parameter_rcl("h1d", cone)

      call set_parameter_rcl("h2l", cnul)
      call set_parameter_rcl("h1l", cone)
    case (3)
      call set_parameter_rcl("h1u", cnul)
      call set_parameter_rcl("h2u", cone)

      call set_parameter_rcl("h1d", cnul)
      call set_parameter_rcl("h2d", cone)

      call set_parameter_rcl("h2l", cnul)
      call set_parameter_rcl("h1l", cone)
    case (4)
      call set_parameter_rcl("h1u", cnul)
      call set_parameter_rcl("h2u", cone)

      call set_parameter_rcl("h2d", cnul)
      call set_parameter_rcl("h1d", cone)

      call set_parameter_rcl("h1l", cnul)
      call set_parameter_rcl("h2l", cone)
    case default
      call error_rcl('Invalid value for ytype. ' //       &
                     'ytype can take the values 1,2,3,4', &
                     where='set_Z2_thdm_yukawa_type_rcl')
    end select

  end subroutine set_Z2_thdm_yukawa_type_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_hl_rcl (m,g)

    ! This subroutine sets the pole mass and width
    ! of the hl(light) boson to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_thdm_hs_interface('set_pole_mass_hl_rcl')
    call processes_generated_warning_rcl('set_pole_mass_hl_rcl')

    call set_parameter_rcl("MHL", cone*m)
    call set_parameter_rcl("WHL", cone*g)

  end subroutine set_pole_mass_hl_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_hh_rcl (m,g)

    ! This subroutine sets the pole mass and width
    ! of the hh(heavy) boson to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_thdm_hs_interface('set_pole_mass_hh_rcl')
    call processes_generated_warning_rcl('set_pole_mass_hh_rcl')
    call warning_rcl('This method is deprecated and ' // &
                     '`set_pole_mass_hl_hh_rcl` should be used instead.', &
                     where='set_pole_mass_hh_rcl')

    call set_parameter_rcl("MHH", cone*m)
    call set_parameter_rcl("WHH", cone*g)

  end subroutine set_pole_mass_hh_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_hl_hh_rcl (ml,gl,mh,gh)

    ! This subroutine sets the pole masses and widths
    ! of the Hl(light) and Hh(heavy) boson to "ml,gl" and "mh,gh", respectively.

    real(dp), intent(in) :: ml,gl,mh,gh

    call check_support_thdm_hs_interface('set_pole_mass_hl_hh_rcl')
    call processes_generated_warning_rcl('set_pole_mass_hl_hh_rcl')

    if (ml .gt. mh) then
      call error_rcl('Light Higgs mass must be smaller than '// &
                     'the heavy Higgs mass.', &
                     where='set_pole_mass_hl_hh_rcl')
    end if

    call set_parameter_rcl("MHL", cone*ml)
    call set_parameter_rcl("WHL", cone*gl)
    call set_parameter_rcl("MHH", cone*mh)
    call set_parameter_rcl("WHH", cone*gh)

  end subroutine set_pole_mass_hl_hh_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_ha_rcl (m,g)

    ! This subroutine sets the pole mass and width
    ! of the ha (pseudo scalar) boson to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_thdm_interface('set_pole_mass_ha_rcl')
    call processes_generated_warning_rcl('set_pole_mass_ha_rcl')

    call set_parameter_rcl("MHA", cone*m)
    call set_parameter_rcl("WHA", cone*g)

  end subroutine set_pole_mass_ha_rcl

!------------------------------------------------------------------------------!

  subroutine set_pole_mass_hc_rcl (m,g)

    ! This subroutine sets the pole mass and width
    ! of the hc (charged) boson to "m" and "g" respectively.

    real(dp), intent(in) :: m,g

    call check_support_thdm_interface('set_pole_mass_hc_rcl')
    call processes_generated_warning_rcl('set_pole_mass_hc_rcl')

    call set_parameter_rcl("MHC", cone*m)
    call set_parameter_rcl("WHC", cone*g)

  end subroutine set_pole_mass_hc_rcl

!------------------------------------------------------------------------------!

  subroutine set_tb_cab_rcl(tb,cab)

    ! This subroutine sets the value for tan(b) and cos(a-b) to "tb" and "cab",
    ! respectively.

    real(dp), intent(in) :: tb,cab
    real(dp)             :: b,a,cb,sa,cab_t,sab_t

    call check_support_thdm_interface('set_tb_cab_rcl')
    call processes_generated_warning_rcl('set_tb_cab_rcl')

    if (tb .lt. 0d0) then
      call error_rcl('Invalid value for tb. ' //   &
                      'tb must be positive.',&
                      where='set_tb_cab_rcl')
    end if
    if (abs(cab) .gt. 1d0) then
      call error_rcl('Invalid value for cab ' //   &
                      'cab must be in [-1,1]',&
                      where='set_tb_cab_rcl')
    end if

    b = atan(tb)
    cb = cos(b)
    a = acos(cab) + b

    if (a .gt. pi/2d0) then
      a = 2*b - a
    end if
    sa = sin(a)

    ! check consistency
    cab_t = cos(asin(sa)-acos(cb))
    sab_t = sin(asin(sa)-acos(cb))
    if ((abs(cab_t-cab) .gt. zerocut) .or. (sab_t .gt. 0d0)) then
      call error_rcl('Inversion for cab failed.',&
                     where='set_tb_cab_rcl')
    end if

    call set_parameter_rcl("cb", cone*cb)
    call set_parameter_rcl("sa", cone*sa)
  end subroutine set_tb_cab_rcl

!------------------------------------------------------------------------------!

  subroutine use_mixing_alpha_msbar_scheme_rcl(s)

    ! This subroutine sets the renormalization scheme for the mixing angle
    ! alpha or a related parameter to the msbar scheme.
    ! Different schemes are selected via `s`='FJTS','MDTS','MTS','l3','l345'.

    character(len=*), intent(in) :: s

    call processes_generated_warning_rcl('use_mixing_alpha_msbar_scheme_rcl')

    select case (s)

    ! In this scheme the mixing angle alpha is renormalized msbar scheme with
    ! the tadpole counterterms being defined in the FJ tadpole scheme.
    ! See 1607.07352
    case ('FJ')
      call check_support_thdm_hs_interface('use_mixing_alpha_msbar_scheme_rcl')
      call set_renoscheme_rcl('da_QED2', 'FJTS')

    ! Same as above with tadpoles being defined in the MD tadpole scheme.
    ! [see Eq.(5.3) in 1607.07352, or Eqs.(2.16)-(2.19) in 1704.02645].
    case ('PR')
      call check_support_thdm_hs_interface('use_mixing_alpha_msbar_scheme_rcl')
      call set_renoscheme_rcl('da_QED2', 'PRTS')

    case default
      call warning_rcl('Unknown msbar scheme for alpha. Given `s`: ' // s, &
                       where='use_mixing_alpha_msbar_scheme_rcl')
    end select

  end subroutine use_mixing_alpha_msbar_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine use_mixing_alpha_onshell_scheme_rcl(s)

    ! This subroutine sets the renormalization scheme for the mixing angle
    ! alpha to an on-shell or BFM scheme.

    character(len=*), intent(in) :: s

    call processes_generated_warning_rcl('use_mixing_alpha_onshell_scheme_rcl')
    call check_support_thdm_hs_interface('use_mixing_alpha_onshell_scheme_rcl')

    select case (s)

    case ('OS')
      call check_support_hs_interface('use_mixing_alpha_onshell_scheme_rcl')
      call set_renoscheme_rcl('da_QED2', 'OS')
    case ('OS1')
      call check_support_THDM_interface('use_mixing_alpha_onshell_scheme_rcl')
      call set_renoscheme_rcl('da_QED2', 'OS1')
    case ('OS2')
      call check_support_THDM_interface('use_mixing_alpha_onshell_scheme_rcl')
      call set_renoscheme_rcl('da_QED2', 'OS2')
    case ('BFM')
      call set_renoscheme_rcl('da_QED2', 'BFM')

    case default
      call warning_rcl('Unknown on-shell scheme for alpha. Given `s`: ' // s, &
                       where='use_mixing_alpha_onshell_scheme_rcl')
    end select

  end subroutine use_mixing_alpha_onshell_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine use_mixing_beta_onshell_scheme_rcl(s)

    ! This subroutine sets the renormalization scheme for the mixing angle
    ! beta to an on-shell or BFM scheme.

    character(len=*), intent(in) :: s

    call processes_generated_warning_rcl('use_mixing_beta_onshell_scheme_rcl')
    call check_support_thdm_hs_interface('use_mixing_beta_onshell_scheme_rcl')

    select case (s)
    case ('OS1')
      call set_renoscheme_rcl('db_QED2', 'OS1')
    case ('OS2')
      call set_renoscheme_rcl('db_QED2', 'OS2')
    case ('OS12')
      call set_renoscheme_rcl('db_QED2', 'OS12')
    case ('BFM')
      call set_renoscheme_rcl('db_QED2', 'BFM')
    case default
      call warning_rcl('Unknown on-shell scheme for beta. Given `s`: ' // s, &
                       where='use_mixing_beta_onshell_scheme_rcl')
    end select

  end subroutine use_mixing_beta_onshell_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine use_mixing_beta_msbar_scheme_rcl(s)

    ! This subroutine sets the renormalization scheme for the mixing angle
    ! beta to the msbar scheme.
    ! Different schemes are selected via `s`='FJTS','MDTS'.

    character(len=*), intent(in) :: s

    call processes_generated_warning_rcl('use_mixing_beta_msbar_scheme_rcl')
    call check_support_thdm_interface('use_mixing_beta_msbar_scheme_rcl')

    select case (s)
    case ('FJ')
      call set_renoscheme_rcl('db_QED2', 'FJTS')

    ! This subroutine sets the renormalization scheme for the mixing angle beta
    ! to the tadpole subtracted msbar scheme (=\bar{DR} in MSSM).
    ! In the THDM this is equivalent to treat the tadpoles in the MDTS or MTS
    ! schemes.
    case ('PR')
      call set_renoscheme_rcl('db_QED2', 'PRTS')

    case default
      call warning_rcl('Unknown on-shell scheme for beta. Given `s`: ' // s, &
                       where='use_mixing_beta_msbar_scheme_rcl')
    end select

  end subroutine use_mixing_beta_msbar_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine set_l5_rcl(l5)

    ! This subroutine sets the value for lambda_5 to l5.

    real(dp), intent(in) :: l5

    call check_support_thdm_interface('set_l5_rcl')
    call processes_generated_warning_rcl('set_l5_rcl')
    call set_parameter_rcl('l5', cone*l5)

  end subroutine set_l5_rcl

!------------------------------------------------------------------------------!

  subroutine set_msb_rcl(msb)

    real(dp), intent(in) :: msb

    call warning_rcl('This method is deprecated. Use ' // &
                     '`set_l5_rcl` instead.', &
                     where='set_msb_rcl')

  end subroutine set_msb_rcl

!------------------------------------------------------------------------------!

  subroutine use_msb_msbar_scheme_rcl(s)

    character(len=*), intent(in) :: s

    call warning_rcl('This method is deprecated.', &
                     where='use_msb_msbar_scheme_rcl')

  end subroutine use_msb_msbar_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine set_sa_rcl(sa)

    ! This subroutine sets the value for the sin of the neutral mixing angle
    ! alpha to sa.

    real(dp), intent(in) :: sa

    if ((sa .gt. 1d0) .or. (sa .lt. -1d0)) then
      call error_rcl('Invalid value for sa: ' //   &
                      '-1 <= sa <= 1',&
                      where='set_sa_rcl')
    end if
    call check_support_hs_interface('set_sa_rcl')
    call processes_generated_warning_rcl('set_sa_rcl')

    call set_parameter_rcl('sa', cone*sa)

  end subroutine set_sa_rcl

!------------------------------------------------------------------------------!

  subroutine set_tb_rcl(tb)

    ! This subroutine sets the value for the tb=tan(beta).

    real(dp), intent(in) :: tb

    call warning_rcl('This method is deprecated.', &
                     where='set_tb_rcl')

  end subroutine set_tb_rcl

!------------------------------------------------------------------------------!

  subroutine set_l3_rcl(l3)

    ! This subroutine sets the value for the l3

    real(dp), intent(in) :: l3

    call check_support_hs_interface('set_l3_rcl')
    call processes_generated_warning_rcl('set_l3_rcl')
    call set_parameter_rcl('l3', cone*l3)

  end subroutine set_l3_rcl

!------------------------------------------------------------------------------!

  subroutine use_tb_msbar_scheme_rcl(s)

    character(len=*), intent(in) :: s

    call warning_rcl('This method is deprecated.', &
                     where='use_msb_msbar_scheme_rcl')

  end subroutine use_tb_msbar_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine use_l3_msbar_scheme_rcl(s)

    ! This subroutine sets the renormalization scheme for l3 to
    ! the msbar scheme.

    character(len=*), intent(in) :: s

    call check_support_hs_interface('use_l3_msbar_scheme_rcl')
    call processes_generated_warning_rcl('use_l3_msbar_scheme_rcl')

    select case (s)
    case ('FJ', 'MS')
      call set_renoscheme_rcl('dl3_QED4', 'FJTS')

    case ('l1')
      call set_renoscheme_rcl('dl3_QED4', 'l1_MS')

    case default
      call warning_rcl('Unknown msbar scheme for l3 Given `s`: ' // s, &
                       where='use_l3_msbar_scheme_rcl')
    end select

  end subroutine use_l3_msbar_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine use_tb_onshell_scheme_rcl(s)

    character(len=*), intent(in) :: s

    call warning_rcl('This method is deprecated.', &
                     where='use_msb_msbar_scheme_rcl')

  end subroutine use_tb_onshell_scheme_rcl

!------------------------------------------------------------------------------!

  subroutine use_l3_onshell_scheme_rcl(s)

    ! This subroutine sets the renormalization scheme for l3 to
    ! an onshell or BFM scheme.

    character(len=*), intent(in) :: s

    call check_support_hs_interface('use_tb_onshell_scheme_rcl')
    call processes_generated_warning_rcl('use_tb_onshell_scheme_rcl')

    select case (s)
    case ('BFM')
      call set_renoscheme_rcl('dl3_QED2', 'BFM')

    case default
      call warning_rcl('Unknown onshell scheme for tb. Given `s`: ' // s, &
                       where='use_l3_onshell_scheme_rcl')
    end select

  end subroutine use_l3_onshell_scheme_rcl

!------------------------------------------------------------------------------!

end module extended_higgs_interface_rcl

!------------------------------------------------------------------------------!
