!******************************************************************************!
!                                                                              !
!    memory_logger_rcl.f90                                                     !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module memory_logger_rcl

!------------------------------------------------------------------------------!

 implicit none

!------------------------------------------------------------------------------!

 contains

!------------------------------------------------------------------------------!

  subroutine log_memory(checkpoint)
    implicit none
    character(len=*), intent(in) :: checkpoint
    character(len=30) :: log_info
    logical :: qopened
    integer :: file_out = 1337
    character(len=*), parameter :: fout = 'memory_rcl.log'
    character(len=7) :: TitleFormat = '(T5,A)'
    character(len=17) :: HeadingFormat = '(A29,A15,A15,A15)'
    character(len=23) :: DataFormat = '(A29,f15.2,f15.2,f15.2)'
    character(len=20) :: RSS, VSZ, VSZP
    integer :: RSS_i, VSZ_i, VSZP_i
    real :: RSS_r, VSZ_r, VSZP_r

    write(log_info, '(A29)') checkpoint

    ! Reading memory values from /proc/pid/status
    call system_mem_usage('RSS', RSS)
    call system_mem_usage('VmSize', VSZ)
    call system_mem_usage('VmPeak', VSZP)

    ! convert values from kilobyte (string) to megabyte (real)
    read(RSS, *) RSS_i
    RSS_r = real(RSS_i)/1000.
    read(VSZ, *) VSZ_i
    VSZ_r = real(VSZ_i)/1000.
    read(VSZP, *) VSZP_i
    VSZP_r = real(VSZP_i)/1000.

    inquire(file_out, opened=qopened)
    if (.not. qopened) then
      open (unit=file_out,file=trim(fout),form='formatted', &
            access='sequential',status='replace')
      write (unit=file_out,fmt=TitleFormat) &
        "************************************************"
      write (unit=file_out,fmt=TitleFormat) &
        "* RECOLA Memory Log: /proc/pid/status Snapshot *"
      write (unit=file_out,fmt=TitleFormat) &
        "************************************************"

      write (unit=file_out,fmt=TitleFormat)   ""
      write (unit=file_out,fmt=TitleFormat)   ""

      write (unit=file_out,fmt=TitleFormat) &
      'RSS:  Amount of allocated memory currently resident in the RAM'
      write (unit=file_out,fmt=TitleFormat) &
      'VSZ:  Currently allocated memory'
      write (unit=file_out,fmt=TitleFormat) &
      'VSZP: Peak of allocated memory'

      write (unit=file_out,fmt=TitleFormat)   ""
      write (unit=file_out,fmt=TitleFormat)   ""

      write (unit=file_out,fmt=HeadingFormat) 'Checkpoint info |              ', &
                                              'RSS (in mb)', &
                                              'VSZ (in mb)', &
                                              'VSZP(in mb)'
      write (unit=file_out,fmt=HeadingFormat) '----------------/              ', &
                                              '-----------', &
                                              '-----------', &
                                              '-----------'
    end if
    write (unit=file_out,fmt=DataFormat) adjustl(log_info), RSS_r, VSZ_r, VSZP_r

  end subroutine log_memory

!------------------------------------------------------------------------------!

  subroutine system_mem_usage(itemid, value)
    character(len=*), intent(in) :: itemid  ! RSS, VSZ
    character(len=20), intent(out) :: value
    character(len=30) :: pid_char, dummy
    character(len=200) :: filename
    character(len=200) :: command
    integer :: pid,res
#ifdef __INTEL_COMPILER
    integer, external :: getpid,system
#endif

    pid=getpid()
    write(pid_char,'(I10)') pid
    filename='proc_snapshot'
    command='cat /proc/'//trim(adjustl(pid_char))// &
            '/status >'//trim(adjustl(filename))
    res=system(command)
    command='cat '//trim(adjustl(filename))// &
            ' | grep ' //trim(itemid)//' > rss_use'
    res=system(command)

    open(unit=100, file='rss_use')
    read(100,*) dummy, value
    close(100)

    return
  end subroutine

!------------------------------------------------------------------------------!

end module memory_logger_rcl

!------------------------------------------------------------------------------!
