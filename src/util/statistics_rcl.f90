!******************************************************************************!
!                                                                              !
!    statistics_rcl.f90                                                        !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

  module statistics_rcl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine print_generation_statistics_rcl

    ! This subroutine print the CPU time spent for generation

    use globals_rcl, only: timeGEN

    write(*,*)
    write(*,*) '   Generation in',timeGEN,'s'
    write(*,*)

  end subroutine print_generation_statistics_rcl

!------------------------------------------------------------------------------!

  subroutine print_TI_statistics_rcl(npr,npoints)

    ! This subroutine print the CPU time spent in the computation of
    ! the tensor integrals for the process with process number "npr"

    use globals_rcl, only: timeTI,sp,get_pr

    integer, intent(in) :: npr,npoints
    integer             :: pr

    call get_pr(npr, 'print_TI_statistics_rcl', pr)

    write(*,*)
    write(*,*) &
    'TIs evaluated in',timeTI(npr)/real(npoints,kind=sp)*1e3, &
    'ms per PS point'
    write(*,*)

  end subroutine print_TI_statistics_rcl

!------------------------------------------------------------------------------!

  subroutine print_TC_statistics_rcl(npr,npoints)

    ! This subroutine print the CPU time spent in the computation of
    ! the tensor coefficients for the process with process number "npr"

    use globals_rcl, only: timeTC,timeHS,sp,get_pr

    integer, intent(in) :: npr,npoints
    integer             :: pr

    call get_pr(npr, 'print_TC_statistics_rcl', pr)

    write(*,*)
    write(*,*) &
    'TCs evaluated in', timeTC(npr)/real(npoints,kind=sp)*1e3, &
    'ms per PS point'
    write(*,*) &
    'Helicity sum performed in', timeHS(npr)/real(npoints,kind=sp)*1e3, &
    'ms per PS point'
    write(*,*)

  end subroutine print_TC_statistics_rcl

!------------------------------------------------------------------------------!

  end module statistics_rcl

!------------------------------------------------------------------------------!
