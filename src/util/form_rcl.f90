!******************************************************************************!
!                                                                              !
!    form_rcl.f90                                                              !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

  module form_rcl

!------------------------------------------------------------------------------!

  use globals_rcl
  use order_rcl, only: compoi,oiSize,oi_reg
  use input_rcl, only: get_complex_mass_form_rcl
  use modelfile, only: get_max_leg_multiplicity_mdl,get_order_id_mdl, &
                       get_particle_antiparticle_mdl,                 &
                       get_particle_spin_mdl,get_particle_colour_mdl, &
                       get_n_particles_mdl,is_particle_mdl,           &
                       get_max_leg_multiplicity_mdl,get_n_orders_mdl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

!============!
!  formHead  !
!============!

  subroutine formHead (pr, mode)

  integer, intent(in) :: pr, mode

  character(len=8)  :: cpr
  character(len=30) :: file

  integer           :: i, max_leg_multiplicity
  character(len=3)  :: mu_index
  character(len=80) :: mu_external
  logical           :: complex_mass_frm

  ! Highest n-Vertex
  max_leg_multiplicity = get_max_leg_multiplicity_mdl()
  call get_complex_mass_form_rcl(complex_mass_frm)

  if (prs(pr)%inpr .gt. 99999999) then
    write(*,*) "increase cpr in formHead, that large process not supported."
    stop
  end if

  write(cpr,'(i8)') prs(pr)%inpr
  file = 'process'//trim(adjustl(cpr))//'.frm'
  open (unit=999,file=file,status='replace')

  mu_external = ''
  do i = 1, max_leg_multiplicity
    write (mu_index, '(i3)') i
    mu_external = trim(adjustl(mu_external)) // 'mu'// trim(adjustl(mu_index))
    if (i < max_leg_multiplicity) then
      mu_external = trim(adjustl(mu_external))//','
    end if
  end do

  mu_external = 'i ' // trim(adjustl(mu_external))//';'

  write(999,'(a)') '#-'
  write(999,'(a)')
  write(999,'(2a)') '***********************************', &
                    '***********************************'
  write(999,'(a)')
  write(999,'(a)') 'Off statistics;'
  write(999,'(a)')
  ! vertex particle mode
  if (mode .eq. 5) then
    write(999,'(a)') 'auto cf c,H;'
    write(999,'(a)') 'auto i i,j,k,l,o,e,a,y,w;'
    write(999,'(a)') 's n;'
  ! amplitude mode
  else
    write(999,'(a)') '* chiral stats, ..'
    write(999,'(a)') 's [-],[+],[],pi,ep,Nc;'
    write(999,'(a)') 'cf Eps,Psi,bPsi,ga,si,om,op,Ga,Si,Den,Delta,DeltaP;'

    ! enable auto mode for tensor reduction
    if (mode.eq.4) then
      write(999,'(a)') 'auto cf A,B,C,D,E,F,G,H;'
    else
      write(999,'(a)') 'cf A,B,C,D,E,F,G,H;'
    end if
    write(999,'(a)') 'f den;'
    ! Y structure used to treat gamma matrices in CDR
    write(999,'(a)') 't Y(cyclic);'
    write(999,'(a)')
    write(999,'(a)') 's n; dimension n;'
    write(999,'(a)') 's sq2;'
    write(999,'(a)') trim(adjustl(mu_external))
    write(999,'(a)') 'auto i mu,nu,ro,al,ta,te,tu,tt,be;'
    write(999,'(a)')
    write(999,'(a)') 'auto s m,M,r,xi;'
    write(999,'(a)') 'auto c c;'
    write(999,'(a)')
    write(999,'(a)') 'auto v p,q;'
    write(999,'(a)')
    write(999,'(a)') 'dimension 4;'
    write(999,'(a)') 'i PP, PM;'
    write(999,'(a)') 'auto i P,Q;'
    write(999,'(a)')
    if (complex_mass_frm .eqv. .true.) then
      write(999,'(a)') 'auto s cM;'
    end if
    write(999,'(a)')
    write(999,'(a)') 'dimension 4;'
    write(999,'(a)') 'auto i i,j,k,l,o,e,a,y,w;'
    write(999,'(a)')
    if (mode.eq.4) then
      write(999,'(a)') 'auto cf B, DB;'
      write(999,'(a)') 'cf g, mom;'
      write(999,'(a)') 's x, n;'
      write(999,'(a)') 'auto v p;'
      write(999,'(a)') 'auto s m;'
    end if
  end if
  write(999,'(a)')
  write(999,'(a)') '.global'
  write(999,'(a)')
  write(999,'(a)') '.store'
  write(999,'(a)')
  write(999,'(2a)') '***********************************', &
                    '***********************************'
  write(999,'(a)')

  end subroutine formHead
!------------------------------------------------------------------------------!

!=================!
!  get_id_as_str  !
!=================!

  function get_id_as_str(p) result(ct)
    integer, intent(in) :: p
    integer :: n_particles
    character(len=3) :: ct

    n_particles = get_n_particles_mdl()

    if (n_particles .ge. 999) then
      write (*, *) 'particle id larger than 999 case not implemented here'
      stop
    else if (n_particles .ge. 100) then
      write(ct,'(i3)') p
      if (p .lt. 10) then
        ct = '00'//trim(adjustl(ct))
      else if(p .lt. 100) then
        ct = '0'//trim(adjustl(ct))
      end if

    else
      write(ct,'(i2)') p
      if (p .lt. 10) ct = '0'//trim(adjustl(ct))
    end if
    ct = trim(ct)
  end function get_id_as_str
!------------------------------------------------------------------------------!

!=================!
!  formCurrents0  !
!=================!

  subroutine formCurrents0 (pr,lp,t,wMax,compw,mode)

  integer, intent(in) :: pr,lp,t,wMax,mode
  logical, intent(in) :: compw(:)

  integer           :: i,w,legs,cs,cd0s,csMax,n_orders,oi
  logical           :: compwTot
  character(len=5)  :: amp
  character(len=13) :: cwp
  character(len=80) :: amporder
  character(len=2)  :: ct,ci,clp1,clp2
  character(len=8)  :: cw
  character(len=3)  :: ccs
  character(len=1)  :: cgs
  character(len=3)  :: order_id

  type order
    integer, allocatable, dimension(:) :: val
  end type
  type(order) :: myorder


  ! mode = 1 -> complete amplitude
  ! mode = 2 -> amplitude for ct
  ! mode = 3 -> amplitude for r2
  ! mode = 4 -> complete amplitude, but without ext. polarizations
  ! mode = 4 -> vertex particle amplitude


  n_orders = get_n_orders_mdl()
  allocate(myorder%val(n_orders))

  compwTot = .false.
  do w = 1,wMax
    compwTot = compwTot .or. compw(w)
  enddo
  if (.not.compwTot) return

  if (lp .eq. TreeBranch) then
    amp = 'AmpLO'
  else if (lp .eq. LoopBranch) then
    write(ct,'(i2)') t
    amp = trim('Amp'//get_id_as_str(t))
  else if (lp .eq. CTBranch) then
    amp = 'AmpCT'
  else if (lp .eq. R2Branch) then
    amp = 'AmpR2'
  end if

  write(999,'(a)')
  write(999,'(2a)') '*----------------------------------', &
                    '-----------------------------------'
  if (lp .eq. TreeBranch) then
    write(999,'(a)') '* tree level'
  else if (lp .eq. LoopBranch) then
    write(999,'(a,i3,a)') '* bare loop (cut particle:',t,')'
  else if (lp .eq. CTBranch) then
    write(999,'(a)') '* counterterms'
  else if (lp .eq. R2Branch) then
    write(999,'(a)') '* rational terms'
  end if
  write(999,'(2a)') '*----------------------------------', &
                    '-----------------------------------'
  write(999,'(a)')

  legs = prs(pr)%legs

  do i = 1,legs
    write(ci,'(i2)') i; ci = adjustl(ci)

    ! Write external wavefunctions
    if (mode .eq. 1) then
      select case (get_particle_spin_mdl(prs(pr)%par(oldleg(i,pr))))
        case (-1:1)  ! ghosts and scalars
          cwp = '1'
        case (3)
          cwp = 'Eps(mu'//trim(ci)//','//trim(ci)//')'  ! vectors
        case (2)
          select case (is_particle_mdl(prs(pr)%par(oldleg(i,pr))))  ! fermions
          case (.true.)
            cwp = 'Psi(i'//trim(ci)//','//trim(ci)//')'
          case (.false.)
            cwp = 'bPsi(i'//trim(ci)//','//trim(ci)//')'
          end select
      end select
      write(999,'(5a)') 'l wp',trim(ci),' = ',trim(cwp),';'
    else
      if (mode .eq. 5) then
        ! register external particles as c(particle id)
        write(clp1,'(i2)') prs(pr)%par(oldleg(i,pr))
        cwp = 'c('//trim(clp1)//')'
      else
        cwp = '1'
      end if
      write(999,'(5a)') 'l wp',trim(ci),' = ',trim(cwp),';'
    end if
  end do

  ! connecting both loop legs
  if (lp .eq. LoopBranch) then
    write(clp1,'(i2)') legs+1; clp1 = adjustl(clp1)  ! first loop leg index
    write(clp2,'(i2)') legs+2; clp2 = adjustl(clp2)  ! second loop leg index

    ! In particle/vertex mode the cut-particles are not necessary -> set to 1
    if (mode .eq. 5 .or. mode .eq. 6) then
      write(999,'(5a)') 'l wp',trim(clp1),' = 1;'
      write(999,'(5a)') 'l wp',trim(clp2),' = 1;'

    ! In the amplitude mode the cut-particles are connected to each other
    ! respecting their particle type (i.e. the indices are of dirac or lorentz
    ! nature for spin 1/2 and 1, respectively.)
    else
      select case (get_particle_spin_mdl(t))
      case (-1:1)  ! ghosts and scalars
        cwp = '1'
      ! g^{mu_{l+1},mu_{l+2}}
      case (3)  ! vectors
        cwp = 'd_(mu'//trim(clp1)//',mu'//trim(clp2)//')'
      case (2)  ! fermions
        cwp =  'd_(i'//trim(clp1)// ',i'//trim(clp2)//')'
      end select
      write(999,'(5a)') 'l wp',trim(clp1),' = ',trim(cwp),';'
      write(999,'(5a)') 'l wp',trim(clp2),' = 1;'
    end if
  end if

  write(999,'(a)')

  do w = legs+1, wMax
    if (.not. compw(w)) cycle
    write(cw,'(i8)') w; cw = adjustl(cw)
    write(999,'(a)') 'l wp'//trim(cw)//' = 0;'
  end do

  write(999,'(a)')

  csMax   = 0
  cd0s = 0 ! maximal number of color deltas in each
           ! color structure of tree lines
  do i = 1, legs
    if (get_particle_colour_mdl(prs(pr)%par(oldleg(i,pr))) .gt. 1) then
      cd0s = cd0s + 1
    end if
  end do

  cs = 1 ! number of final color structures (cs = cd0s!)
  do i = 1, cd0s
    cs = cs * i
  end do
  csMax = max(csMax  ,cs)

  do cs = 1,csMax
    do oi = 1,oiSize(pr)
      if (lp .eq. TreeBranch) then
        if (.not. compoi(oi,0,pr)) cycle
      else
        if (.not. compoi(oi,1,pr)) cycle
      end if
      write(ccs,'(i3)') cs; ccs = adjustl(ccs)
      write(cgs,'(i1)') oi_reg(1,oi,pr)
      order_id = get_order_id_mdl(1)
      amporder = trim(order_id)//'='//trim(cgs)
      do i = 2, size(oi_reg, 1)
        order_id = get_order_id_mdl(i)
        write(cgs,'(i1)') oi_reg(i,oi,pr)
        amporder = trim(amporder)//','//trim(order_id)//'='//trim(cgs)
      end do
      write(999,'(a)') 'l ['//amp//'(cs='//trim(ccs)// &
                       ','//trim(amporder)//')] = 0;'
    end do
  end do

  write(999,'(a)')

  write(999,'(a)') '.sort'

  write(999,'(a)')
  deallocate(myorder%val)

  end subroutine formCurrents0

!------------------------------------------------------------------------------!

!==============!
!  formVertex  !
!==============!
!------------------------------------------------------------------------------!
! formVertex tracks all the particles going in a vertex function which can be  !
! used for further processing, such as determining which rational terms or     !
! counterterms need to be computed for a specific (collection of) process(es). !
!------------------------------------------------------------------------------!

  subroutine formVertex(lp,t,legs,j,nMax,e,f,w,cs,absolute_order,    &
                        order_increase,branch_type,branch,loop_attached)
    integer, intent(in) :: lp,t,legs,j,nMax,e(:),f(:),w(:),cs,&
                           branch_type, branch
    logical, intent(in) :: loop_attached
    integer, dimension(:), intent(in) :: order_increase, absolute_order
    integer :: legsE, i, ind
    logical :: last = .false.
    character(len=5) :: amp
    integer:: nDef
    character(len=8) :: clegs,cj
    character(len=8), allocatable :: cw(:)
    character(len=2) :: ct
    character(len=2), allocatable :: cf(:)
    character(len=5) ::  tag
    character(len=80) :: cgs
    character(len=80) :: amporder
    character(len=3) :: colorflow
    character(len=3) :: int_str
    character(len=80) :: cwIn,indent=''
    character(len=80), allocatable :: cwp(:)
    character(len=2*nMax+1) :: C

    nDef = get_max_leg_multiplicity_mdl()
    allocate(cw(nDef), cwp(nDef), cf(nDef))
    if (lp.eq.1) then; legsE = legs + 2
    else;              legsE = legs
    endif
    last = e(nDef).eq.2**(legsE-1)-1
    write(clegs,'(i8)') legs; clegs = adjustl(clegs)
    ! changed j to j+2 which fixed a bug for 1 -> 1 tree-level diagrams
    write(cj,'(i8)') j+2; cj = adjustl(cj)

    do i = 1, nDef
      write(cw(i),'(i8)') w(i); cw(i) = adjustl(cw(i))
      cwp(i) = 'wp'//cw(i)
      if (i .eq. nDef) then
        write(cf(i),'(i2)') get_particle_antiparticle_mdl(f(i))
      else
        write(cf(i),'(i2)') f(i)
      end if
    end do


    ! powers in couplings
    write(cgs,'(i1)') order_increase(1)
    amporder = trim(cgs)
    do i = 2, size(order_increase)
      write(cgs,'(i1)') order_increase(i)
      amporder = trim(amporder)//','//trim(cgs)
    end do

    ! branch_type
    write(int_str,'(i1)') branch_type
    tag = trim(int_str)//','

    ! modelfile branch
    write(int_str,'(i1)') branch
    tag = trim(tag)//trim(int_str)
    cgs = trim(amporder)//','//trim(tag)

    if (last) then
      if (lp .eq. 0) then
        amp = 'AmpLO'
      else if (lp .eq. 1) then
        write(ct,'(i2)') t
        if (t .lt. 10) ct = '0'//trim(adjustl(ct))
        amp = 'Amp'//trim(ct)
      else if (lp .eq. 2) then
        amp = 'AmpCT'
      else if (lp .eq. 3) then
        amp = 'AmpR2'
      end if
      write(colorflow,'(i3)') cs; colorflow = adjustl(colorflow)

      write(int_str,'(i1)') absolute_order(1)
      amporder = trim(get_order_id_mdl(1))//'='//trim(int_str)
      do i = 2, size(order_increase)
        write(int_str,'(i1)') absolute_order(i)
        amporder = trim(amporder)//','//trim(get_order_id_mdl(i))//'='//trim(int_str)
      end do
      cwp(nDef) = '['//amp//'(cs='//trim(colorflow)//','//trim(amporder)//')]'
    end if

    ind = len(trim(cwp(nDef))) + 5
    write(999,'(a)')
    write(999,'(3a)') 'l ',trim(cwp(nDef)),' = '
    C = 'c('

    if (last .or. loop_attached) then
      cwIn = '+'
      do i = 1, nMax -1
        cwIn = trim(cwIn)//' '//trim(cwp(i))//' *'
      end do
    else
      cwIn = '+ 1 *'
    end if

    if (lp .eq. 1) then
      if (loop_attached) then
        do i = 2, nMax -1
          if (i .eq. 2) then
            C = trim(C)//cf(i)
          else
            C = trim(C)// ',' //cf(i)
          end if
        end do
        C = trim(C) // ')'
        write(999,'(a)') '  '//trim(cwp(nDef))//' '//trim(cwIn)
        if (.not. (last .or. loop_attached)) then
          write(999,'(a,i2,a)') indent(1:ind) // C // ';'
        else
          write(999,'(a,i2,a)') indent(1:ind) // '1' // ';'
        end if
      else
        C = trim(C)//trim(cf(nDef))//')'
        write(999,'(a,i2,a)') indent(1:ind) // trim(C) // ';'
      end if
    else
      write(999,'(a,i2,a)') indent(1:ind) // '1;'
    end if
    write(999,'(a)')
    write(999,'(a)') '.sort'
    write(999,'(a)')
    deallocate(cw, cwp, cf)
  end subroutine formVertex

!------------------------------------------------------------------------------!

!=================!
!  formParticles  !
!=================!

!------------------------------------------------------------------------------!
! formParticles tracks all the Feynman rules used for a given process setup    !
! which can be used for further processing.                                    !
!------------------------------------------------------------------------------!

  subroutine formParticles(lp, t, legs, j, nMax, e, f, w, cs, absolute_order,  &
                           order_increase, branch_type, branch)
    integer, intent(in) :: lp, t, legs, j, nMax, e(:), f(:), w(:), cs, &
                           branch_type, branch
    integer, dimension(:), intent(in) :: order_increase, absolute_order
    integer :: legsE, i, ind
    logical :: last = .false.
    character(len=5) :: amp
    integer:: nDef
    character(len=8) :: clegs,cj
    character(len=8), allocatable :: cw(:)
    character(len=2) :: ct
    character(len=2), allocatable :: cf(:)
    character(len=5) ::  tag
    character(len=80) :: cgs
    character(len=80) :: amporder
    character(len=3) :: colorflow
    character(len=3) :: int_str
    character(len=80) :: cwIn, indent=''
    character(len=80), allocatable :: cwp(:)
    character(len=20) :: C

    nDef = get_max_leg_multiplicity_mdl()
    allocate(cw(nDef), cwp(nDef), cf(nDef))
    if (lp.eq.1) then; legsE = legs + 2
    else;              legsE = legs
    endif
    last = e(nDef).eq.2**(legsE-1)-1
    write(clegs,'(i8)') legs; clegs = adjustl(clegs)
    ! changed j to j+2 which fixed a bug for 1 -> 1 tree-level diagrams
    write(cj,'(i8)') j+2; cj = adjustl(cj)

    do i = 1, nDef
      write(cw(i),'(i8)') w(i); cw(i) = adjustl(cw(i))
      cwp(i) = 'wp'//cw(i)
      write(cf(i),'(i2)') f(i)
    end do

    ! powers in couplings
    write(cgs,'(i1)') order_increase(1)
    amporder = trim(cgs)
    do i = 2, size(order_increase)
      write(cgs,'(i1)') order_increase(i)
      amporder = trim(amporder)//','//trim(cgs)
    end do

    ! branch_type
    write(int_str,'(i1)') branch_type
    tag = trim(int_str)//','

    ! modelfile branch
    write(int_str,'(i1)') branch
    tag = trim(tag)//trim(int_str)
    cgs = trim(amporder)//','//trim(tag)

    if (last) then
      if (lp .eq. 0) then
        amp = 'AmpLO'
      else if (lp .eq. 1) then
        write(ct,'(i2)') t
        if (t .lt. 10) ct = '0'//trim(adjustl(ct))
        amp = 'Amp'//trim(ct)
      else if (lp .eq. 2) then
        amp = 'AmpCT'
      else if (lp .eq. 3) then
        amp = 'AmpR2'
      end if
      write(colorflow,'(i3)') cs; colorflow = adjustl(colorflow)

      write(int_str,'(i1)') absolute_order(1)
      amporder = trim(get_order_id_mdl(1))//'='//trim(int_str)
      do i = 2, size(order_increase)
        write(int_str,'(i1)') absolute_order(i)
        amporder = trim(amporder)//','//trim(get_order_id_mdl(i))//'='//trim(int_str)
      end do
      cwp(nDef) = '['//amp//'(cs='//trim(colorflow)//','//trim(amporder)//')]'
    end if

    ind = len(trim(cwp(nDef))) + 5
    write(999,'(a)')
    write(999,'(3a)') 'l ',trim(cwp(nDef)),' = '
    C = 'c('
    cwIn = '+'
    do i = 1, nMax -1
      C = trim(C)//trim(adjustl(cf(i)))//','
      cwIn = trim(cwIn)//' '//trim(cwp(i))//' *'
    end do
    C = trim(C)//trim(adjustl(cf(nDef)))//')'

    write(999,'(a)') '  '//trim(cwp(nDef))//' '//trim(cwIn)
    write(999,'(a,i2,a)') indent(1:ind) // trim(C) // ';'
    write(999,'(a)')
    write(999,'(a)') '.sort'
    write(999,'(a)')
    deallocate(cw, cwp, cf)
  end subroutine formParticles

!------------------------------------------------------------------------------!

!=====================!
!  writecolorconfigs  !
!=====================!

  subroutine writecolorconfigs(pr, fermion_sign)

  integer, intent(in) :: pr
  integer, intent(in), optional :: fermion_sign

  character(len=8) :: cpr
  character(len=30) :: file
  integer :: cs, i, j, k, legs
  character(len=2) :: clegs
  character(len=2) :: cpar
  character(len=2) :: csign
  character(len=30) :: cpars
  integer, allocatable :: up(:), low(:)
  character(len=10) :: cup, clow, ccs
  character(len=20) :: color_delta
  character(len=80) :: color_deltas

  legs = prs(pr)%legs
  cpars = ''
  do i = 1,legs;
    write(cpar,'(i2)') prs(pr)%par(oldleg(i,pr))
    cpars = trim(adjustl(cpars)) // trim(adjustl(cpar))
    if (i .ne. legs) cpars = trim(adjustl(cpars)) // ','
  end do

  if (present(fermion_sign)) then
    write(csign,'(i2)') fermion_sign
  end if

  if (prs(pr)%inpr .gt. 99999999) then
    write(*,*) "increase cpr in writecolorconfigs, that large process not supported."
    stop
  end if
  write(cpr,'(i8)') prs(pr)%inpr
  write(clegs,'(i2)') legs
  file = 'colorstructures'//trim(adjustl(cpr))//'.txt'
  open (unit=998,file=file)

  write(998,'(2a)') '*---------------------------------------',                &
                    '---------------------------------------*'

  write(998,'(a)')
  write(998,'(2a)') '* color deltas for the process: ',                        &
                    trim(adjustl(prs(pr)%process))
  write(998,'(a)')
  write(998,'(2a)') 'legs=', trim(adjustl(clegs))
  write(998,'(2a)') 'particles=', cpars

  if (present(fermion_sign)) then
    write(csign,'(i2)') fermion_sign
    write(998,'(2a)') 'fermionsign=', trim(adjustl(csign))
  end if
  write(998,'(a)')

  do cs = 1, csTot(pr)
    allocate (up(legs),low(legs))
    k = 0
    do j = 1,legs
      if (csAmp(j,cs,pr).ne.0) then
        k = k + 1
        up(k)  = oldleg(j,pr)
        low(k) = oldleg(csAmp(j,cs,pr),pr)
      end if
    end do
    if (k.gt.0) then
      color_deltas = ''
      do i = 1, k
        write(cup,'(i4)') up(i);
        write(clow,'(i4)') low(i);
        color_delta = '('//trim(adjustl(cup))//','//trim(adjustl(clow))//')'
        color_deltas = trim(adjustl(color_deltas))//trim(adjustl(color_delta))
      end do
      write(ccs,'(i4)') cs
      ccs = 'cs='//trim(adjustl(ccs))//':'
      write(998,'(2a)') trim(adjustl(ccs)), trim(adjustl(color_deltas))
    end if
    deallocate (up,low)
  end do

  write(998,'(a)')
  write(998,'(2a)') '*---------------------------------------',                &
                    '---------------------------------------*'

  close (unit=998, status='keep')

  end subroutine writecolorconfigs
!------------------------------------------------------------------------------!

!=================!
!  formCurrents1  !
!=================!

  subroutine formCurrents1 (pr,lp,t,wMax,compw,mode)

  integer, intent(in) :: pr,lp,t,wMax,mode
  logical, intent(in) :: compw(:)

  integer :: w, legs, cs, i, n_orders,oi
  logical :: compwTot, projection
  character(len=2) :: ct, ci
  character(len=5) :: amp
  character(len=1) :: cgs
  character(len=3) :: ccs
  character(len=8) :: ce1,ce2
  character(len=3) :: order_id

  type order
    integer, allocatable, dimension(:) :: val
  end type
  type(order)       :: myorder
  character(len=80) :: amporder

  ! mode = 1 -> complete amplitude
  ! mode = 2 -> amplitude for ct
  ! mode = 3 -> amplitude for r2

  !if (lp.gt.1) return

  n_orders = get_n_orders_mdl()
  allocate(myorder%val(n_orders))

  compwTot = .false.
  do w = 1,wMax
    compwTot = compwTot .or. compw(w)
  enddo
  if (.not.compwTot) return

  if (lp.eq.0) then
    amp = 'AmpLO'
  elseif (lp.eq.1) then
    write(ct,'(i2)') t
    if (t .lt. 10) ct = '0'//trim(adjustl(ct))
    amp = 'Amp'//trim(ct)
  elseif (lp.eq.2) then
    amp = 'AmpCT'
  elseif (lp.eq.3) then
    amp = 'AmpR2'
  endif

  legs = prs(pr)%legs

  write(999,'(a)')
  write(999,'(a)') 'hide;'

  write(999,'(a)')
  do cs = 1,pCsTot(pr)
  do oi = 1,oiSize(pr)
    if (lp .eq. TreeBranch) then
      if (.not. compoi(oi,0,pr)) cycle
    else
      if (.not. compoi(oi,1,pr)) cycle
    end if
    write(cgs,'(i1)') oi_reg(1,oi,pr)
    order_id = get_order_id_mdl(1)
    amporder = trim(order_id)//'='//trim(cgs)
    do i = 2, size(oi_reg, 1)
      order_id = get_order_id_mdl(i)
      write(cgs,'(i1)') oi_reg(i,oi,pr)
      amporder = trim(amporder)//','//trim(order_id)//'='//trim(cgs)
    end do

    write(ccs,'(i3)') cs; ccs = adjustl(ccs)
    write(999,'(a)') 'g ['//amp//',cs='//trim(ccs)//','//trim(amporder)//'] = '&
                     // '['//amp//'(cs='//trim(ccs)//','//trim(amporder)//')];'

  end do
  end do

  if (mode .le. 4) then
    if (lp .eq. 1) then
      write(999,'(a)')
      write(ce1,'(i8)') 2**legs; ce1 = adjustl(ce1)
      write(ce2,'(i8)') 2**legs-1; ce2 = adjustl(ce2)
      write(999,'(a)') '#do i = 0,'//trim(ce2)
      write(999,'(a)') ' #define j1 "{'//trim(ce1)//"+'i'"//'}"'
      write(999,'(a)') " id p'j1' = q + p'i';"
      write(999,'(a)') " id den(p'j1',?m) = den(q,p'i',?m);"
      write(999,'(a)') " argument ga,Eps;"
      write(999,'(a)') "  id p'j1' = q + p'i';"
      write(999,'(a)') " endargument;"
      write(999,'(a)') '#enddo'
      write(999,'(a)')
      write(ce1,'(i8)') 2**(legs-1); ce1 = adjustl(ce1)
      write(ce2,'(i8)') 2**(legs-1)-1; ce2 = adjustl(ce2)
      write(999,'(a)') '#do i = 0,'//trim(ce2)
      write(999,'(a)') ' #define j1 "{'//trim(ce1)//"+'i'"//'}"'
      write(999,'(a)') ' #define j2 "{'//trim(ce1)//"-'i'-1"//'}"'
      write(999,'(a)') " id p'j1' = - p'j2';"
      write(999,'(a)') " argument den,ga,Eps;"
      write(999,'(a)') "  id p'j1' = - p'j2';"
      write(999,'(a)') " endargument;"
      write(999,'(a)') '#enddo'
      write(999,'(a)') "id p0 = 0;"
      write(999,'(a)') "argument;"
      write(999,'(a)') " id p0 = 0;"
      write(999,'(a)') "endargument;"

      write(999,'(a)')
    endif

    write(999,'(a)')
    write(999,'(a)') 'id Eps(mu?,?m)= Eps(mu,?m);'
    write(999,'(a)') 'id ga(?m,mu?)= ga(?m,mu);'

    write(999,'(a)')
    write(999,'(a)') '#call lorentzdirac'

    if (lp .eq. 1) then
      write(999,'(a)')
      write(999,'(a)') '#call loopfunctions'
    end if
  end if

  if (mode .eq. 5) then
    write(999,'(a)') 'repeat;'
    write(999,'(a)') '  id c(i?,?k)*c(j?, ?l) = c(i,?k,j, ?l);'
    write(999,'(a)') '  id c(?k)*c(?k) = c(?k);'
    write(999,'(a)') 'endrepeat;'
    write(999,'(a)') '.sort (PolyFun = H);'
    write(999,'(a)') 'id H(n?) * c(?i) = c(?i);'
    write(999,'(a)') '.sort'
  else if (mode .eq. 6) then
    write(999,'(a)') '.sort (PolyFun = H);'
    write(999,'(a)') 'id H(n?) * c(?i) = c(?i);'
    write(999,'(a)') '.sort'
  end if
 ! MSbar
 if ((mode.eq.2) .and. (lp .eq. 1)) then
    write(999,'(a)')
    write(999,'(a)') '#call MStens'
    write(999,'(a)') 'id n = 4;'
    write(999,'(a)') 'id M0 = 0;'
    write(999,'(a)') '.sort'
    ! As we are trying to extract Feynman rules, we convert the result to the
    ! convention that all momenta are outgoing.
    write(ci,'(i2)') legs-1
    write(999,'(a)') "#do i = 1, "//trim(adjustl(ci))
    write(999,'(a)') " id p'i' = -p'i';"
    ! momenta are absorbed in ga
    write(999,'(a)') " id ga(?m, p'i', ?n) = -ga(?m, p'i', ?n);"
    write(999,'(a)') "#enddo"
    write(999,'(a)') ".sort"
  end if
  if ((mode.eq.3) .and. (lp .eq. 1)) then
    write(999,'(a)')
    !write(999,'(a)') '#call r2'
    write(999,'(a)') '#call MStens'
    write(999,'(a)') 'id n = 4-ep;'
    write(999,'(a)') '.sort'
    write(999,'(a)') 'id ep = 0;'
    write(999,'(a)') 'id 1/ep = 0;'
    write(999,'(a)') 'id M0 = 0;'
    if (legs .gt. 3) then
      write(999,'(a)') "id p3 = p1 + p2;"
      write(999,'(a)') "id p4 = p3;"
      write(999,'(a)') "id ga(?m, p4) = ga(?m, p3);"

      write(999,'(a)') "id p5 = p1 + p3;"
      write(999,'(a)') "id ga(?m, p5) = ga(?m, p1) + ga(?m, p3);"

      write(999,'(a)') "id p6 = p2 + p3;"
      write(999,'(a)') "id ga(?m, p6) = ga(?m, p2) + ga(?m, p3);"

      write(999,'(a)') "id p7 = p1 + p2 + p3;"
      write(999,'(a)') "id ga(?m, p7) = ga(?m, p1) + ga(?m, p2) + ga(?m, p3);"
    end if
    if (legs .gt. 4) then
      ! in this order p4 will not be replaced by p3
      write(999,'(a)') "id p8 = p4;"
      write(999,'(a)') "id ga(?m, p8) = ga(?m, p4);"

      write(999,'(a)') "id p9 = p1 + p4;"
      write(999,'(a)') "id ga(?m, p9) = ga(?m, p1) + ga(?m, p4);"

      write(999,'(a)') "id p10 = p2 + p4;"
      write(999,'(a)') "id ga(?m, p10) = ga(?m, p2) + ga(?m, p4);"

      write(999,'(a)') "id p11 = p1 + p2 + p4;"
      write(999,'(a)') "id ga(?m, p11) = ga(?m, p1) + ga(?m, p2) + ga(?m, p4);"

      write(999,'(a)') "id p12 = p3 + p4;"
      write(999,'(a)') "id ga(?m, p12) = ga(?m, p3) + ga(?m, p4);"

      write(999,'(a)') "id p13 = p1 + p3 + p4;"
      write(999,'(a)') "id ga(?m, p13) = ga(?m, p1) + ga(?m, p3) + ga(?m, p4);"

      write(999,'(a)') "id p14 = p2 + p3 + p4;"
      write(999,'(a)') "id ga(?m, p14) = ga(?m, p2) + ga(?m, p3) + ga(?m, p4);"

      write(999,'(a)') "id p15 = p1 + p2 + p3 + p4;"
      write(999,'(a)') "id ga(?m, p15) = ga(?m, p1) + ga(?m, p2) + ga(?m, p3) + ga(?m, p4);"
    end if
    if (legs .gt. 5) then
      write(999,'(a)') "id p16 = p5;"
      write(999,'(a)') "id p17 = p1 + p5;"
      write(999,'(a)') "id p18 = p2 + p5;"
      write(999,'(a)') "id p19 = p1 + p2 + p5;"
      write(999,'(a)') "id p20 = p3 + p5;"
      write(999,'(a)') "id p21 = p1 + p3 + p5;"
      write(999,'(a)') "id p22 = p2 + p3 + p5;"
      write(999,'(a)') "id p23 = p1 + p2 + p3 + p5;"
      write(999,'(a)') "id p24 = p4 + p5;"
      write(999,'(a)') "id p25 = p1 + p4 + p5;"
      write(999,'(a)') "id p26 = p2 + p4 + p5;"
      write(999,'(a)') "id p27 = p1 + p2 + p4 + p5;"
      write(999,'(a)') "id p28 = p3 + p4 + p5;"
      write(999,'(a)') "id p29 = p1 + p3 + p4 + p5;"
      write(999,'(a)') "id p30 = p2 + p3 + p4 + p5;"
      write(999,'(a)') "id p31 = p1 + p2 + p3 + p4 + p5;"
    end if
    if (legs .gt. 6) then
      call error_rcl('FORM output not support for more than 6 particles.', &
                     where='formCurrents1')
    end if

    write(999,'(a)') '.sort'
    ! As we are trying to extract Feynman rules, we convert the result to the
    ! convention that all momenta are outgoing.
    write(ci,'(i2)') legs-1
    write(999,'(a)') "#do i = 1, "//trim(adjustl(ci))
    write(999,'(a)') " id p'i' = -p'i';"
    ! momenta are absorbed in ga
    write(999,'(a)') " id ga(?m, p'i', ?n) = -ga(?m, p'i', ?n);"
    write(999,'(a)') "#enddo"
    write(999,'(a)') ".sort"
  end if
  if (mode.eq.4) then
    write(999,'(a)')
    write(999,'(a)') 'b ga,d_,i_,pi;'
    write(999,'(a)') 'print +s +f;'
    write(999,'(a)') '.store'

  end if

  write(999,'(a)')
  if (mode .le. 4) then
    if (mode.eq.1) then
      write(999,'(a)') 'b den,Eps,Psi,bPsi,A,B,C,D,E,F,G,H;'
    else
      write(999,'(a)') 'b ga,d_,i_,pi;'
    end if
  end if
  write(999,'(a)') 'print +s +f;'
  write(999,'(a)') '.store'
  write(999,'(a)')

  projection = .true.
  deallocate(myorder%val)

  end subroutine formCurrents1
!------------------------------------------------------------------------------!

!============!
!  formFeet  !
!============!

  subroutine formFeet

  write(999,'(a)')
  write(999,'(2a)') '***********************************', &
                    '***********************************'
  write(999,'(a)')
  write(999,'(a)') '.end'

  close (unit=999,status='keep')

  end subroutine formFeet

!------------------------------------------------------------------------------!

  end module form_rcl

!------------------------------------------------------------------------------!
