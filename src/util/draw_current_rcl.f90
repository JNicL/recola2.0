!******************************************************************************!
!                                                                              !
!    draw_current_rcl.f90                                                      !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module draw_current_rcl

!------------------------------------------------------------------------------!

  use globals_rcl, only: draw,get_value_in_array,treebranch,loopbranch, &
                         ctbranch,r2branch,prs

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine texHead(pr)

    integer, intent(in) :: pr
    character(len=4)  :: cpr
    character(len=30) :: file

    if (draw.ge.1) then

      write(cpr,'(i4)') prs(pr)%inpr
      file = 'process_'//trim(adjustl(cpr))//'.tex'
      open (unit=99,file=file,status='replace')

      write(99,'(a)') '\documentclass[11pt]{article}'
      write(99,'(a)')
      write(99,'(a)') '\usepackage{axodraw}'
      write(99,'(a)')
      write(99,'(a)') '\oddsidemargin -20pt \evensidemargin -20pt'
      write(99,'(a)') '\topmargin -40pt \headheight 00pt \headsep 00pt'
      write(99,'(a)') '\textheight 270mm \textwidth 172mm'
      write(99,'(a)')
      write(99,'(a)') '\begin{document}'

    endif

  end subroutine texHead

!------------------------------------------------------------------------------!

  subroutine texFeet(last_pr)

    logical, intent(in) :: last_pr

    if (draw.ge.1) then

      write(99,'(a)')
      write(99,'(a)') '\end{document}'

      close (unit=99,status='keep')

      if (last_pr) then

        close (unit=2000,status='delete')
        close (unit=2001,status='keep')
        close (unit=3000,status='delete')
        close (unit=3001,status='keep')
        close (unit=4000,status='delete')
        close (unit=4001,status='keep')

      endif

    endif

  end subroutine texFeet

!------------------------------------------------------------------------------!

  subroutine texPicture(lp, newcut, legs, legsE, bin, ppar, park, t, &
                     cdsMax, j, xs)
    use modelfile, only: get_particle_type2_mdl,get_axodraw_propagator_mdl, &
                         get_mass_name_mdl,get_max_leg_multiplicity_mdl,    &
                         is_particle_mdl,get_particle_texname_mdl,          &
                         get_particle_antiparticle_mdl
    use tables_rcl, only: levelLeg, firstNumbers
    use globals_rcl, only: n_masses, dp
    use skeleton_rcl, only: hosT,hmaT,wT,csT,binT,orderIncT

    integer, intent(in)               :: lp,legs,legsE,cdsMax,j,t
    integer, intent(in), dimension(:) :: bin,ppar,park
    logical, intent(in)               :: newcut,xs
    integer :: w1, k, ho(1:size(hosT,1)), hm
    integer :: parbin(2**(legsE-1)),e0,p0,height,yoffset,mass(legs),row, &
               rowb,rowbl0,rowbl,rshift,cshift,eProp0,eProp,             &
               ee,pp,ap,column,hm0,i,i1,ii,kk,n,s,step,nDef,             &
               nprop,npoint

    integer, allocatable :: rowbIn(:), ll(:)

    real (dp)         :: lenght
    character(len=14) :: axoprop
    character(len=9)  :: axoext
    character(len=2)  :: cl
    character(len=6)  :: texmass
    character(len=9)  :: axomass
    character(len=4)  :: cd,ca
    character(len=999), allocatable :: delta(:)
    logical                         :: draw_delta
    integer, allocatable :: vd(:,:),va(:,:),vd0(:),va0(:)

    if (draw.lt.1) return

    nDef = get_max_leg_multiplicity_mdl()

    allocate(rowbIn(nDef-1))
    allocate(ll(nDef-1))

    ! Here the legend is written
    if (lp .eq. treebranch .and. newcut .and. draw .ge. 3) then

      write(99,'(a)') ''

      write(99,'(a)') '\noindent'
      write(99,'(a)') '\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}'
      write(99,'(a)') ''

      write(99,'(a)') '\vspace{.5cm}'
      write(99,'(a)') &
        'Here are listed the branches contributing to the given ', &
        'process.  '
      write(99,'(a)') ''

      write(99,'(a)') '\vspace{.3cm}'
      write(99,'(a)') &
        'On the left side of each figure there are the external ', &
        'currents contributing to the branch, whose binary ', &
        'number and particle are explictly given. ', &
        'Going from left to right, these currents enter the ', &
        'selected branch or a branch computed in a previous ', &
        'step. '
      write(99,'(a)') ''

      write(99,'(a)') '\vspace{.3cm}'
      write(99,'(a)') &
        'In loop branches, there is the first current of the ', &
        'cutted loop line, which is always at the top of the ', &
        'figure. ', &
        'Going downwards, this current enters the selected branch ', &
        'or a previously computed one. ', &
        'All previously computed loop branches are listed ', &
        'downwards and connected by loop propagators. ', &
        'The binary of their off-set and their mass are ', &
        'explicitly given. '
      write(99,'(a)') ''

      write(99,'(a)') '\vspace{.3cm}'
      write(99,'(a)') &
        'From the last previously computed branches, going from ', &
        'left to right there is the selected branch and then the ', &
        'out-going current. ', &
        'This one can be an off-shell current (big dot), the last ', &
        'external current (in the tree case) or the second ', &
        'current of the cutted loop line (for loop branches), ', &
        'which virtually close the loop. '
      write(99,'(a)') ''

      write(99,'(a)') '\vspace{.5cm}'
      write(99,'(a)') '\noindent'
      write(99,'(a)') '\begin{tabular}{c p{15.3cm}}'

      write(99,'(a)') '\begin{picture}(6,6)(-3,-3)'
      write(99,'(a)') '\GBoxc(0,0)(6,6){1}'
      write(99,'(a)') '\end{picture}'
      write(99,'(a)') '&'
      write(99,'(a)') 'It indicates the selected branch.'

      write(99,'(a)') '\\[+.3cm]'

      write(99,'(a)') '\begin{picture}(6,6)(-3,-3)'
      write(99,'(a)') '\GBoxc(0,0)(6,6){1}'
      write(99,'(a)') '\Line(-3,-3)(3,3)\Line(-3,3)(3,-3)'
      write(99,'(a)') '\end{picture}'
      write(99,'(a)') '&'
      write(99,'(a)') 'It indicates the selected branch, with special ', &
                      'Feynman rules for a counterterm or a rational', &
                      'term.'

      write(99,'(a)') '\\[+.3cm]'

      write(99,'(a)') '\begin{picture}(6,6)(-3,-3)'
      write(99,'(a)') '\SetWidth{1.5}'
      write(99,'(a)') '\Line(-3,-3)(3,3)\Line(-3,3)(3,-3)'
      write(99,'(a)') '\end{picture}'
      write(99,'(a)') '&'
      write(99,'(a)') 'It indicates the cutted loop line.'

      write(99,'(a)') '\\[+.3cm]'

      write(99,'(a)') '\begin{picture}(6,6)(-3,-3)'
      write(99,'(a)') '\GCirc(0,0){3}{.5}'
      write(99,'(a)') '\end{picture}'
      write(99,'(a)') '&'
      write(99,'(a)') 'It indicates a branch computed in some ', &
                      'previous step.'

      write(99,'(a)') '\\[+.3cm]'

      write(99,'(a)') '\begin{picture}(6,6)(-3,-3)'
      write(99,'(a)') '\GCirc(0,0){1.5}{0}'
      write(99,'(a)') '\end{picture}'
      write(99,'(a)') '&'
      write(99,'(a)') 'It indicates the out-going off-shell ', &
                      'current of the selected branch.'

      write(99,'(a)') '\\[+.3cm]'

      write(99,'(a)') '(\#,$\;M$)'
      write(99,'(a)') '&'
      write(99,'(a)') 'It indicates the off-set number (given by ', &
                      'the sum of the binaries of the external ', &
                      'legs entered into the loop at the previous ', &
                      'branch) and the mass of the propagator.'

      write(99,'(a)') '\\[+.3cm]'

      write(99,'(a)') '$m_f$'
      write(99,'(a)') '&'
      write(99,'(a)') 'It indicates the mass regulator for light ', &
                      'fermions'

      write(99,'(a)') '\\[+.3cm]'

      write(99,'(a)') '$m_0$'
      write(99,'(a)') '&'
      write(99,'(a)') 'It indicates the mass for massless ', &
                      'particles'

      write(99,'(a)') '\\[+.3cm]'

      write(99,'(a)') '$g_s^{\#}$'
      write(99,'(a)') '&'
      write(99,'(a)') 'In counterterms/rational terms insertion, ', &
                      'it indicates the power of $g_s$ of the ', &
                      'selected branch.'

      write(99,'(a)') '\end{tabular}'

      write(99,'(a)') ''
      write(99,'(a)') '\vspace{.4cm}'
      write(99,'(a)') &
        'Below each branch are explicitly written the color ', &
        'structures of the incoming and outgoing currents of the ', &
        'select branch. ', &
        'In front of the color structures of the incoming ', &
        'currents is written the binary number of the current. ', &
        'If a current has no color structure, it has been built ', &
        'from colorless external currents. ', &
        'The color structures are product of deltas. ', &
        'The off-shell currents (i.e. all except the last ones) ', &
        'of colored particles have always an "open" part, made of ', &
        'deltas containing an $\alpha$ and/or a $\beta$. ', &
        'The other indices of the deltas are the binary numbers ', &
        'of the external currents, which express in compact ', &
        'notation how the color/anticolor of the extenal currents ', &
        'are contracted. For example ', &
        '$\delta^{2}_{1} \delta^{1}_\beta \delta^\alpha_{2}$ ', &
        'stays for $\delta^{\alpha_2}_{\beta_1} ', &
        '\delta^{\alpha_1}_\beta \delta^\alpha_{\beta_2}$, ', &
        'where $\alpha_i,\beta_i$ go from 1 to 3. '

      write(99,'(a)') ''
      write(99,'(a)') ''
      write(99,'(a)') ''
      write(99,'(a)') ''
      write(99,'(a)') '\vspace{.5cm}'
      write(99,'(a)') '\noindent'
      write(99,'(a)') '\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}'
      write(99,'(a)') ''

    endif

    w1 = wT(1, j)
    ho = hosT(:,w1)
    hm = hmaT(w1)

    do i = 1,legsE
      parbin(2**(i-1)) = park(i)
    end do

    ! Here the process is written
    if (lp .eq. treebranch .and. newcut) then
      write(99,'(a)') '\vspace{.5cm}'
      write(99,'(a)') '\begin{center}'
      write(99,'(a,i2,a)') '\begin{tabular}{*{',2*legs,'}{c}}'
      write(99,'(a)') '\hline\hline \\[-.2cm]'
      write(99,'(a)') '\quad'
      do i = 1,legs
        if (i.ne.1) write(99,'(a)') '& $+$ &'
        write(99,'(3a)') '$',trim(get_particle_texname_mdl(park(i))),'$'
      end do
      write(99,'(a)') '& $\,\to\quad 0 \quad$\\'
      write(99,'(a)') '\quad'
      do i = 1,legs; e0 = 2**(i-1)
        if (i.ne.1) write(99,'(a)') '& &'
        write(99,'(i3)') e0
      end do
      write(99,'(a)') '&'
      write(99,'(a)') '\\[+.2cm]\hline\hline'
      write(99,'(a)') '\end{tabular}'
      write(99,'(a)') '\end{center}'
    end if
    if (newcut) then
      write(99,'(a)') ''
      write(99,'(a)') '\vspace{.5cm}'
      write(99,'(a)') '\begin{tabular}{|c|}'
      write(99,'(a)') '\hline\\[-.2cm]'
      select case (lp)
      case (treebranch)
        write(99,'(a)') 'Tree Level\\[.2cm]'
      case (loopbranch)
        write(99,'(3a)') &
          'Bare Loop; \qquad Cut-particle: $', trim(get_particle_texname_mdl(t)), &
          '$\\[.2cm]'
      case (ctbranch)
        write(99,'(a)') 'Counterterms\\[.2cm]'
      case (r2branch)
        write(99,'(a)') 'Rational Terms\\[.2cm]'
      end select
      write(99,'(a)') '\hline'
      write(99,'(a)') '\end{tabular}'
      write(99,'(a)') '\\[.4cm]'
    endif

    if (bin(nDef) .lt. 2**legs) then
      height = 20*levelLeg(bin(nDef)) + 5
      yoffset = - 20*levelLeg(bin(nDef))
    else
      height = 20*levelLeg(bin(nDef)) + 10
      yoffset = - 20*levelLeg(bin(nDef))
    end if

    if (draw.lt.2) then
      write(99,'(a,i4,a,i4,a)') &
        '\begin{picture}(120,',height,')(-60,',yoffset,')'
    else
      write(99,'(a,i4,a,i4,a)') &
        '\begin{picture}(120,',height+70,')(-60,',yoffset-70,')'
    endif

    do i = 1, nDef-1
      ll(i) = levelLeg(bin(i))
    end do

    if (bin(nDef) .lt. 2**legs) then ! tree branch

      ! The row of the last drawn external current (none at the
      ! moment) is initialized to 20 (later it is increased by -20
      ! each time an external current appears).
      row = 20

      ! Define row of the selected branch (rowb) and of the
      ! previously computed branches (rowbIn(1:3)), if present.
      !rowbIn(1) = - 10 * (ll(1) - 1)
      do i = 1, nDef-1
        if (i .eq. 1) then
          rowbIn(i) = - 10 * (ll(i) - 1)
        else
          rowbIn(i) = - 10 * (ll(i-1) +ll(i)) + rowbIn(i-1)
        end if
      end do

      npoint = get_value_in_array(0, bin(:))
      if (npoint .gt. 1) then
        rowb = sum(rowbIn(1:npoint-1))/(npoint-1)
      else
        rowb = rowbIn(nDef-2)
      end if

    else ! loop branch

      ! The row of the last drawn external current (none at the
      ! moment) is initialized to 0, because of the presence of the
      ! first loop current. "row" is then increased by -20 each time
      ! an external current appears.
      row  = 0

      ! Write binary number and particle of the first loop current.
      write(99,'(a,i4,a,a,a)') &
        '\Text(3,9)[cc]{\scriptsize $',2**legs, &
        '\;\;',trim(get_particle_texname_mdl(t)),'$}'

      nprop = maxval(ho) + 1
      rowbl = 0
      if (ho(1) .gt. 0) then
        ! Compute the mass number of each propagator of the history
        hm0 = hm
        step = n_masses + 1
        do ii = legs,1,-1
          mass(ii) = hm0/step**(ii-1)
          hm0 = hm0 - mass(ii)*step**(ii-1)
        end do
        ! Loop over propagators starting from the second one. When
        ! a propagator is selected, the line and properties of the
        ! previous one is written. This means:
        ! When the 2nd propagator is selected, the 1st is written;
        ! when the 3rd is selected, the 2nd is written; etc. ...
        do i = 2,nprop
          eProp0 = 0
          eProp  = 0
          do k = 1,size(ho)
            if (ho(k).gt.0) then
              if (ho(k)+1.eq.i-1) eProp0 = eProp0 + 2**(k-1)
              if (ho(k)+1.eq.i)   eProp  = eProp  + 2**(k-1)
            endif
          enddo
          rowbl0 = rowbl
          rowbl = row - 10 * ( levelLeg(eProp) + 1 )
          if (i.eq.2) then
            ! Here the line of the first loop current is written, if
            ! the second propagator is selected.
            write(cl,'(i2)') int(abs(rowbl)/4)
            axoprop = trim(get_axodraw_propagator_mdl(get_particle_type2_mdl(t)))
            select case (axoprop)
            case ('\DashArrowLine','\DashLine'); axoext = '{3}'
            case ('\Gluon','\Photon'); axoext = '{1.5}{'//cl//'}'
            case default; axoext = ''
            end select
            if (.not. is_particle_mdl(t)) then
              write(99,'(2a,i4,2a)')   &
                axoprop,'(0,',rowbl,')(0,0)',axoext
            else
              write(99,'(2a,i4,2a)')   &
                axoprop,'(0,0)(0,',rowbl,')',axoext
            end if
          else
            ! The mass for the previous propagator is defined
            texmass = get_mass_name_mdl(mass(i-1))
            ! The line for the previous propagator is written
            if (mass(i-1).eq.1) then ! massless (m_0)
              axomass = '\DashLine'; axoext = '{1}'
            else if (mass(i-1).eq.2) then ! light mass regulator (m_f)
              axomass = '\DashLine'; axoext = '{2}'
            else                         ! massive
              axomass = '\Line'; axoext = ''
            end if

            ! The line and the pair (#,M) for the previous
            ! propagator is written
            write(99,'(2a,i4,a,i4,3a,i4,a,i4,3a)') &
              trim(axomass),'(0,',rowbl0,')(0,',rowbl,')', &
              trim(axoext),'\Text(25,',(rowbl+rowbl0)/2, &
              ')[cc]{\scriptsize $(',eProp0,',',trim(texmass),')$}'
          end if
          ! Loop over the external currents which contributed to the
          ! (previously computed) loop branch, from which the
          ! selected propagator is coming.
          do i1 = 1,levelLeg(eProp)
            ! Binary of the external current is defined
            ee = firstNumbers(eProp,i1); pp = parbin(ee)
            ! "row" is increased by -20
            row = row - 20
            ! Here are written the binary number and the particle of
            ! the external currents which contributed to the loop
            ! branch, from which the selected propagator is coming.
            write(99,'(a,i4,a,i4,a,a,i4,3a)') &
              '\Text(-45,',row,')[rc]{\scriptsize',ee,'}', &
              '\Text(-35,',row,')[lc]{\scriptsize $', &
              trim(get_particle_texname_mdl(pp)),'$}'
            ! Here are written the lines of the external currents
            ! which contributed to the loop branch, from which the
            ! selected propagator is coming.
            lenght = sqrt( (20**2+(rowbl-row)**2)*1d0 )
            write(cl,'(i2)') int(lenght/4)
            axoprop = trim(get_axodraw_propagator_mdl(get_particle_type2_mdl(pp)))
            select case (axoprop)
            case ('\DashArrowLine','\DashLine'); axoext = '{3}'
            case ('\Gluon','\Photon'); axoext = '{1.5}{'//cl//'}'
            case default; axoext = ''
            end select
            if (.not. is_particle_mdl(pp)) then
              write(99,'(2a,i4,a,i4,2a)')                &
                axoprop,'(0,',rowbl,')(-20,',row,')',axoext
            else
              write(99,'(2a,i4,a,i4,2a)')                &
                axoprop,'(-20,',row,')(0,',rowbl,')',axoext
            end if
          end do
          ! Here is drawn the symbol (grey circle) of the branch from
          ! which the previous propagator is coming.
          if (i.gt.2) &
            write(99,'(a,i4,a)') '\GCirc(0,',rowbl0,'){3}{.5}'
        end do
      end if

      ! Define row of the selected branch (rowb) and of the
      ! previously computed branches (rowbIn(1:nDef-1)), if present.
      rowbIn(1) = rowbl
      do i = 2, nDef-1
        if (i .eq. 2) then
          rowbIn(i) = row - 10 * (ll(i) + 1)
        else
          rowbIn(i) = - 10 * (ll(i-1) + ll(i)) + rowbIn(i-1)
        end if
      end do

      npoint = get_value_in_array(0, bin(:))
      if (npoint .gt. 2) then
        rowb = sum(rowbIn(1:npoint-1))/(npoint-1)
      else
        rowb = rowbIn(nDef-2)
      end if

    end if

    ! Loop over the in-coming currents of the selected branch
    do i = 1,nDef-1
      e0 = bin(i); p0 = ppar(i)
      if (e0.eq.0) cycle
      if (e0.lt.2**legs) then
        ! Loop over the external currents contributing to the
        ! in-coming currents of the selected branch (not done for the
        ! loop current).
        do i1 = 1,levelLeg(e0)
          ! Binary of the external current is defined
          ee = firstNumbers(e0,i1); pp = parbin(ee)
          ! "row" is increased by -20
          row = row - 20
          ! Here are written the binary number and the particle of
          ! the external currents contributing to the in-coming
          ! currents of the selected branch.
          write(99,'(a,i4,a,i4,a,a,i4,3a)') &
            '\Text(-45,',row,')[rc]{\scriptsize',ee,'}', &
            '\Text(-35,',row,')[lc]{\scriptsize $', &
            trim(get_particle_texname_mdl(pp)),'$}'

          if (levelLeg(e0).gt.1) then
            ! Here are written the lines of the external currents
            ! contributing to the in-coming currents of the selected
            ! branch.
            lenght = sqrt( (20**2+(rowbIn(i)-row)**2)*1d0 )
            write(cl,'(i2)') int(lenght/4)
            axoprop = trim(get_axodraw_propagator_mdl(get_particle_type2_mdl(pp)))
            select case (axoprop)
            case ('\DashArrowLine','\DashLine'); axoext = '{3}'
            case ('\Gluon','\Photon'); axoext = '{1.5}{'//cl//'}'
            case default; axoext = ''
            end select
            if (.not. is_particle_mdl(pp)) then
              write(99,'(2a,i4,a,i4,2a)') &
                axoprop,'(0,',rowbIn(i),')(-20,',row,')',axoext
            else
              write(99,'(2a,i4,a,i4,2a)') &
                axoprop,'(-20,',row,')(0,',rowbIn(i),')',axoext
            end if
          end if
        end do
      end if
      if (levelLeg(e0).eq.1) then
        ! Here are written the lines of the in-coming currents of the
        ! selected branch, in the case they are external currents.
        if (e0.lt.2**legs) then; column = - 20
        else;                    column =    0
        endif
        lenght = sqrt( ((20-column)**2+(rowb-row)**2)*1d0 )
        write(cl,'(i2)') int(lenght/4)
        axoprop = trim(get_axodraw_propagator_mdl(get_particle_type2_mdl(p0)))
        select case (axoprop)
        case ('\DashArrowLine','\DashLine'); axoext = '{3}'
        case ('\Gluon','\Photon'); axoext = '{1.5}{'//cl//'}'
        case default; axoext = ''
        end select
        if (.not. is_particle_mdl(p0)) then
          write(99,'(2a,i4,a,i4,a,i4,2a)') &
            axoprop,'(20,',rowb,')(',column,',',row,')',axoext
        else
          write(99,'(2a,i4,a,i4,a,i4,2a)') &
            axoprop,'(',column,',',row,')(20,',rowb,')',axoext
        end if
      else
        ! Here are written the lines of the in-coming currents of the
        ! selected branch, in the case they are not external currents.
        lenght = sqrt( (20**2+(rowbIn(i)-rowb)**2)*1d0 )
        write(cl,'(i2)') int(lenght/4)
        axoprop = trim(get_axodraw_propagator_mdl(get_particle_type2_mdl(p0)))
        select case (axoprop)
        case ('\DashArrowLine','\DashLine'); axoext = '{3}'
        case ('\Gluon','\Photon'); axoext = '{1.5}{'//cl//'}'
        case default; axoext = ''
        end select
        if (.not. is_particle_mdl(p0)) then
          write(99,'(2a,i4,a,i4,2a)') &
            axoprop,'(20,',rowb,')(0,',rowbIn(i),')',axoext
        else
          write(99,'(2a,i4,a,i4,2a)') &
            axoprop,'(0,',rowbIn(i),')(20,',rowb,')',axoext
        end if
        ! Here is written the particle of the in-coming currents of
        ! the selected branch, in the case they are not external
        ! currents.

        ! May not be optimal for n-vertices n>4:  <10-07-17, Jean-Nicolas Lang> !
        if (i.eq.1) then
          if (bin(2) .eq. 0) then
            rshift = + 4
            cshift = -12
          else
            rshift = + 1
            cshift = - 6
          endif
        elseif (i.eq.2) then
          if (bin(3) .eq. 0) then
            rshift = - 8
            cshift = - 6
          else
            rshift = + 3
            cshift = -17
          endif
        elseif (i.eq.3) then
          rshift = - 8
          cshift = - 6
        else
          rshift = - 8
          cshift = - 6
        endif
        write(99,'(a,i4,a,i4,3a)') &
          '\Text(',20+cshift,',',(rowbIn(i)+rowb)/2+rshift, &
          ')[lb]{\scriptsize $',trim(get_particle_texname_mdl(p0)),'$}'
        ! Here is drawn the symbol (grey circle) of the branch from
        ! which the in-coming currents of the selected branch are
        ! coming.
        write(99,'(a,i4,a)') '\GCirc(0,',rowbIn(i),'){3}{.5}'
      endif
    enddo

    ! We write the line of the outgoing current of the selected branch.
    ! Need to invert the particle because ppar views all particles as incoming
    ap = get_particle_antiparticle_mdl(ppar(nDef))
    axoprop = trim(get_axodraw_propagator_mdl(get_particle_type2_mdl(ap)))
    select case (axoprop)
    case ('\DashArrowLine','\DashLine'); axoext = '{3}'
    case ('\Gluon','\Photon'); axoext = '{1.5}{5}'
    case default; axoext = ''
    end select
    if (.not. is_particle_mdl(ap)) then
      write(99,'(2a,i4,a,i4,2a)') &
        axoprop,'(40,',rowb,')(20,',rowb,')',axoext
    else
      write(99,'(2a,i4,a,i4,2a)') &
        axoprop,'(20,',rowb,')(40,',rowb,')',axoext
    end if
    ! Here are written the particle of the out-going current of the
    ! selected branch.
    write(99,'(a,i4,3a)') &
      '\Text(46,',rowb,')[lc]{\scriptsize $',trim(get_particle_texname_mdl(ap)),'$}'
    ! Here is drawn the symbol (white box) of the selected branch
    write(99,'(a,i4,a)') '\GBoxc(20,',rowb,')(6,6){1}'

    if (xs) then
      if (lp .eq. ctbranch) then
        ! Draw counterterm as a crossed white box
        write(99,'(a,i4,a,i4,a,i4,a,i4,a)')      &
          '\Line(17,',rowb-3,')(23,',rowb+3,     &
          ')\Line(17,',rowb+3,')(23,',rowb-3,')'
      else if (lp .eq. r2branch) then
        ! Draw r2 term as a black box
        write(99,'(a,i4,a)') '\GBoxc(20,',rowb,')(6,6){0}'
      end if
      ! Write power of g_s of the selected branch for
      ! counterterm/rational term insertion.
      write(99,'(a,i4,a,i1,a)') &
        '\Text(28,',rowb+5,')[cb]{\scriptsize $g_s^{\,',orderIncT(1,j),'}$}'
    endif

    ! Write color structures of the currents of the selected branch
    if (draw .ge. 2 .and. cdsMax .gt. 0) then
      allocate(delta(nDef))
      allocate (vd(cdsMax,nDef)); vd = 0
      allocate (va(cdsMax,nDef)); va = 0
      draw_delta = .false.
      do i = 1,nDef
        delta(i) = ''
        if (wT(i,j) .eq. 0) cycle
        allocate (vd0(cdsMax))
        vd0 = 0
        allocate (va0(cdsMax))
        va0 = 0
        ii = 0
        do kk = 1,legs
          if (csT(kk,wT(i,j)) .ne. 0) then
            ii = ii + 1
            vd0(ii) = kk
            va0(ii) = csT(kk,wT(i,j))
          endif
        enddo
        vd(1:cdsMax,i) = 2**(vd0(1:cdsMax)-1)
        va(1:cdsMax,i) = 2**(va0(1:cdsMax)-1)
        deallocate (vd0)
        deallocate (va0)
        do n = 1,cdsMax; if (vd(n,i)*va(n,i).eq.0) exit
          write(cd,'(i4)') vd(n,i)
          write(ca,'(i4)') va(n,i)
          delta(i) = trim(adjustl(delta(i)))//'\delta^{'//trim(adjustl(cd))//'}'
          delta(i) = trim(adjustl(delta(i)))//'_{'//trim(adjustl(ca))//'}'
          draw_delta = .true.
        enddo
        if (csT(-1,wT(i,j)).ne.0) then
          write(cd,'(i4)') 2**(csT(-1,wT(i,j))-1)
          delta(i) = trim(adjustl(delta(i)))//'\delta^{'//trim(adjustl(cd))//'}_\beta'
          draw_delta = .true.
        endif
        if (csT(0,wT(i,j)).ne.0) then
          write(ca,'(i4)') 2**(csT(0,wT(i,j))-1)
          delta(i) = trim(adjustl(delta(i)))//'\delta^\alpha_{'//trim(adjustl(ca))//'}'
          draw_delta = .true.
        endif
      enddo
      deallocate (vd)
      deallocate (va)
      if (draw_delta) then
        s = row - 14
        write(99,'(a,i4,a)') &
          '\Text(-50,',s,')[l]{\scriptsize Incoming Color Structures:}'
        s = s + 1
        do i = 1,nDef-1; if (delta(i).eq.'') cycle
          s = s - 13
          write(99,'(a,i4,a,i4,a)') &
            '\Text(-47,',s,')[lb]{\scriptsize $',binT(wT(i,j)),'$}'
          s = s + 1
          write(99,'(a,i4,a,3a)') &
            '\Text(-30,',s,')[l]{\scriptsize $',trim(adjustl(delta(i))),'$}'
        enddo
        s = s - 12
        write(99,'(a,i4,a)') &
          '\Text(-50,',s,')[l]{\scriptsize Outgoing Color Structure:}'
        s = s + 1
        if (delta(nDef) .ne. '') then
          s = s - 13
          s = s + 1
          write(99,'(a,i4,3a)') &
            '\Text(-30,',s,')[l]{\scriptsize $',trim(adjustl(delta(nDef))),'$}'
        endif
      end if
      deallocate(delta)
    endif

    ! Write the cross for the first and last loop current.
    write(99,'(a)') '\SetWidth{1.5}'
    if (bin(nDef) .ge. 2**legs) then
      ! Write the cross for the first loop current.
      write(99,'(a)') '\Line(-3,-3)(3,3)\Line(-3,3)(3,-3)'
    endif
    if (bin(nDef) .eq. 2**(legs+1)-1) then
      ! Write the cross for the last loop current.
      write(99,'(a,i4,a,i4,a,i4,a,i4,a)') &
        '\Line(37,',rowb-3,')(43,',rowb+3, &
        ')\Line(37,',rowb+3,')(43,',rowb-3,')'
    elseif (bin(nDef) .lt. 2**(legsE-1)-1) then
      ! Write the big dot for the out-going current, if it is not the
      ! last external or loop current.
      write(99,'(a,i4,a)') '\GCirc(40,',rowb,'){1.5}{0}'
    endif

    write(99,'(a)') '\end{picture}'
    write(99,'(a)') '%'

    deallocate(rowbIn, ll)

  end subroutine texPicture

!------------------------------------------------------------------------------!

end module draw_current_rcl

!------------------------------------------------------------------------------!
