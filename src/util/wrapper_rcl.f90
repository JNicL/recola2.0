!******************************************************************************!
!                                                                              !
!    wrapper_rcl.f90                                                           !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!


module wrapper_rcl

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine set_alphas_masses_nowidtharg_rcl(mc,mb,mt)

    use globals_rcl, only: dp
    use recola1_interface_rcl, only: set_alphas_masses_rcl

    real(dp), intent(in) :: mc,mb,mt

    call set_alphas_masses_rcl(mc,mb,mt)

  end subroutine set_alphas_masses_nowidtharg_rcl

!------------------------------------------------------------------------------!

  subroutine use_gfermi_scheme_noarg_rcl

    use recola1_interface_rcl, only: use_gfermi_scheme_rcl

    call use_gfermi_scheme_rcl

  end subroutine use_gfermi_scheme_noarg_rcl

!------------------------------------------------------------------------------!

  subroutine use_gfermi_scheme_and_set_alpha_rcl(alpha)

    use globals_rcl, only: dp
    use recola1_interface_rcl, only: use_gfermi_scheme_rcl

    real(dp), intent(in) :: alpha

    call use_gfermi_scheme_rcl(a=alpha)

  end subroutine use_gfermi_scheme_and_set_alpha_rcl

!------------------------------------------------------------------------------!

  subroutine use_gfermi_scheme_and_set_gfermi_rcl(gfermi)

    use globals_rcl, only: dp
    use recola1_interface_rcl, only: use_gfermi_scheme_rcl

    real(dp), intent(in) :: gfermi

    call use_gfermi_scheme_rcl(g=gfermi)

  end subroutine use_gfermi_scheme_and_set_gfermi_rcl

!------------------------------------------------------------------------------!

  subroutine use_alpha0_scheme_noarg_rcl

    use recola1_interface_rcl, only: use_alpha0_scheme_rcl

    call use_alpha0_scheme_rcl

  end subroutine use_alpha0_scheme_noarg_rcl

!------------------------------------------------------------------------------!

  subroutine use_alphaz_scheme_noarg_rcl

    use globals_rcl, only: warning_rcl

    call warning_rcl('alphaZ scheme not implemented. No effect.', &
                     where='use_alphaz_scheme_noarg_rcl')

  end subroutine use_alphaz_scheme_noarg_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_set_gs_power_rcl(npr,gsarray,gslen)

    use recola1_interface_rcl, only: set_gs_power_rcl

    integer, intent(in) :: npr,gslen
    integer, intent(in) :: gsarray(0:1,1:gslen)

    call set_gs_power_rcl(npr,transpose(gsarray))

  end subroutine wrapper_set_gs_power_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_set_outgoing_momenta_rcl(npr,pIn,p,legs)

    use globals_rcl, only: dp
    use random_psp_rcl, only: set_outgoing_momenta_rcl

    integer,  intent(in)  :: npr,legs
    real(dp), intent(in)  :: pIn(0:3,1:2)
    real(dp), intent(out) :: p(0:3,1:legs)

    call set_outgoing_momenta_rcl(npr,pIn,p)
    p(0:3,1) = pIn(0:3,1)
    p(0:3,2) = pIn(0:3,2)

  end subroutine wrapper_set_outgoing_momenta_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_compute_process_rcl(npr,p,legs,order,A2, &
                                          momenta_check)

    !  real(dp), intent(out), dimension(0:1), optional :: A2
    ! we use ALWAYS the A2 argument as mandatory, since the optional form
    ! conflicts with passing character arrays ("order") from C to Fortran
    ! in the same function call

    use globals_rcl, only: dp
    use process_computation_rcl, only: compute_process_rcl

    integer,          intent(in)  :: npr,legs
    real(dp),         intent(in)  :: p(0:3,1:legs)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2(0:1)
    logical,          intent(out) :: momenta_check

    call compute_process_rcl(npr,p,order,A2,momenta_check)

  end subroutine wrapper_compute_process_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_amplitude_r1_rcl(npr,pow,order,colour,hel,legs,A)

    use globals_rcl, only: dp
    use recola1_interface_rcl, only: get_amplitude_r1_rcl

    integer,          intent(in)  :: npr,pow,legs
    integer,          intent(in)  :: colour(1:legs),hel(1:legs)
    character(len=*), intent(in)  :: order
    complex(dp),      intent(out) :: A

    call get_amplitude_r1_rcl(npr,pow,order,colour,hel,A)

  end subroutine wrapper_get_amplitude_r1_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_amplitude_general_rcl(npr,pow,powlen,order,colour, &
                                               hel,legs,A)

    use globals_rcl, only: dp
    use process_computation_rcl, only: get_amplitude_general_rcl

    integer,          intent(in)  :: npr,powlen,legs
    integer,          intent(in)  :: colour(1:legs),hel(1:legs),pow(1:powlen)
    character(len=*), intent(in)  :: order
    complex(dp),      intent(out) :: A

    call get_amplitude_general_rcl(npr,pow,order,colour,hel,A)

  end subroutine wrapper_get_amplitude_general_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_squared_amplitude_r1_rcl(npr,pow,order,A2)

    use globals_rcl, only: dp, error_rcl
    use recola, only: get_squared_amplitude_rcl

    integer,          intent(in)  :: npr,pow
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2

    call get_squared_amplitude_rcl(npr,pow,order,A2)

  end subroutine wrapper_get_squared_amplitude_r1_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_squared_amplitude_general_rcl(npr,pow,powlen, &
                                                       order,A2)

    use globals_rcl, only: dp, error_rcl
    use recola, only: get_squared_amplitude_rcl
    use modelfile, only: get_n_orders_mdl

    integer,          intent(in)  :: npr,powlen
    integer,          intent(in)  :: pow(1:powlen)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2
    integer                       :: n_orders

    n_orders = get_n_orders_mdl()
    if (n_orders .ne. powlen) then
      call error_rcl('powlen != n_orders', &
                     where='wrapper_get_squared_amplitude_general_rcl')
    end if
    call get_squared_amplitude_rcl(npr,pow,order,A2)

  end subroutine wrapper_get_squared_amplitude_general_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_polarized_squared_amplitude_r1_rcl(npr,pow,order, &
                                                            hel,legs,A2h)

    use globals_rcl, only: dp
    use recola, only: get_polarized_squared_amplitude_rcl

    integer,          intent(in)  :: npr,pow,legs
    integer,          intent(in)  :: hel(1:legs)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2h


    call get_polarized_squared_amplitude_rcl(npr,pow,order,hel,A2h)
  end subroutine wrapper_get_polarized_squared_amplitude_r1_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_polarized_squared_amplitude_general_rcl(npr,pow,      &
                                                                 powlen,order, &
                                                                 hel,legs,A2h)

    use globals_rcl, only: dp, error_rcl
    use recola, only: get_polarized_squared_amplitude_rcl
    use modelfile, only: get_n_orders_mdl

    integer,          intent(in)  :: npr,powlen,legs
    integer,          intent(in)  :: pow(1:powlen),hel(1:legs)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2h
    integer                       :: n_orders

    n_orders = get_n_orders_mdl()
    if (n_orders .ne. powlen) then
      call error_rcl('powlen != n_orders', &
                     where='wrapper_get_polarized_squared_amplitude_general_rcl')
    end if
    call get_polarized_squared_amplitude_rcl(npr,pow,order,hel,A2h)

  end subroutine wrapper_get_polarized_squared_amplitude_general_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_compute_colour_correlation_rcl(npr,p,legs,i1,i2, &
                                                    order,A2cc,momenta_check)

    use globals_rcl, only: dp
    use process_computation_rcl, only: compute_colour_correlation_rcl

    integer,          intent(in) :: npr,legs,i1,i2
    real(dp),         intent(in) :: p(0:3,1:legs)
    character(len=*), intent(in) :: order
    real(dp), intent(out) :: A2cc
    logical,  intent(out) :: momenta_check

    call compute_colour_correlation_rcl(npr,p,i1,i2,A2cc=A2cc,ord=order, &
                                        momenta_check=momenta_check)

  end subroutine wrapper_compute_colour_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_rescale_colour_correlation_rcl (npr,i1,i2,order,A2cc)

    use globals_rcl, only: dp
    use process_computation_rcl, only: rescale_colour_correlation_rcl

    integer,          intent(in)  :: npr,i1,i2
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2cc

    call rescale_colour_correlation_rcl(npr,i1,i2,order,A2cc)

  end subroutine wrapper_rescale_colour_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_rescale_colour_correlation_int_rcl (npr,i1,i2,A2ccint)

    use globals_rcl, only: dp
    use process_computation_rcl, only: rescale_colour_correlation_int_rcl

    integer,  intent(in)  :: npr,i1,i2
    real(dp), intent(out) :: A2ccint

    call rescale_colour_correlation_int_rcl(npr,i1,i2,A2ccint)

  end subroutine wrapper_rescale_colour_correlation_int_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_compute_colour_correlation_int_rcl(npr,p,legs,i1,i2, &
                                                        A2ccint,momenta_check)

    use globals_rcl, only: dp
    use process_computation_rcl, only: compute_colour_correlation_int_rcl

    integer,  intent(in)  :: npr,legs,i1,i2
    real(dp), intent(in)  :: p(0:3,1:legs)
    real(dp), intent(out) :: A2ccint
    logical,  intent(out) :: momenta_check

    call compute_colour_correlation_int_rcl(npr,p,i1,i2,A2ccint,momenta_check)

  end subroutine wrapper_compute_colour_correlation_int_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_compute_all_colour_correlations_rcl(npr,p,legs,order, &
                                                         momenta_check)

    use globals_rcl, only: dp
    use recola, only: compute_all_colour_correlations_rcl

    integer,          intent(in)  :: npr,legs
    real(dp),         intent(in)  :: p(0:3,1:legs)
    character(len=*), intent(in)  :: order
    logical,          intent(out) :: momenta_check

    call compute_all_colour_correlations_rcl(npr,p,ord=trim(order),&
                                             momenta_check=momenta_check)

  end subroutine wrapper_compute_all_colour_correlations_rcl


!------------------------------------------------------------------------------!

  subroutine wrapper_compute_all_colour_correlations_int_rcl(npr,p,legs, &
                                                             momenta_check)

    use globals_rcl, only: dp
    use recola, only: compute_all_colour_correlations_int_rcl

    integer,  intent(in)  :: npr,legs
    real(dp), intent(in)  :: p(0:3,1:legs)
    logical,  intent(out) :: momenta_check

    call compute_all_colour_correlations_int_rcl(npr,p,momenta_check)

  end subroutine wrapper_compute_all_colour_correlations_int_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_colour_correlation_general_rcl(npr,pow,powlen, &
                                                        i1,i2,order,A2cc)

    use globals_rcl, only: dp
    use process_computation_rcl, only: get_colour_correlation_general_rcl

    integer,          intent(in)  :: npr,powlen,i1,i2
    integer,          intent(in)  :: pow(1:powlen)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2cc

    call get_colour_correlation_general_rcl(npr,pow,i1,i2,order,A2cc)

  end subroutine wrapper_get_colour_correlation_general_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_colour_correlation_int_general_rcl(npr,pow,powlen, &
                                                            i1,i2,A2ccint)

    use globals_rcl, only: dp
    use process_computation_rcl, only: get_colour_correlation_int_general_rcl

    integer,   intent(in) :: npr,powlen,i1,i2
    integer,   intent(in) :: pow(1:powlen)
    real(dp), intent(out) :: A2ccint

    call get_colour_correlation_int_general_rcl(npr,pow,i1,i2,A2ccint)

  end subroutine wrapper_get_colour_correlation_int_general_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_compute_spin_colour_correlation_rcl(npr,p,legs,    &
                                                         i1,i2,v,order, &
                                                         A2scc,momenta_check)

    use globals_rcl, only: dp
    use process_computation_rcl, only: compute_spin_colour_correlation_rcl

    integer,          intent(in)  :: npr,legs,i1,i2
    character(len=*), intent(in)  :: order
    real(dp),         intent(in)  :: p(0:3,1:legs)
    complex(dp),      intent(in)  :: v(0:3)
    real(dp),         intent(out) :: A2scc
    logical,          intent(out) :: momenta_check

    call compute_spin_colour_correlation_rcl(npr,p,i1,i2,v,order,A2scc,momenta_check)

  end subroutine wrapper_compute_spin_colour_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_rescale_spin_colour_correlation_rcl (npr,i1,i2,v,order,A2scc)

    use globals_rcl, only: dp
    use process_computation_rcl, only: rescale_spin_colour_correlation_rcl

    integer,          intent(in)  :: npr,i1,i2
    complex(dp),      intent(in)  :: v(0:3)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2scc

    call rescale_spin_colour_correlation_rcl(npr,i1,i2,v,order,A2scc)

  end subroutine wrapper_rescale_spin_colour_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_spin_colour_correlation_general_rcl(npr,pow,powlen, &
                                                             i1,i2,order,A2scc)

    use globals_rcl, only: dp
    use process_computation_rcl, only: get_spin_colour_correlation_general_rcl

    integer,          intent(in)  :: npr,powlen,i1,i2
    integer,          intent(in)  :: pow(1:powlen)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2scc

    call get_spin_colour_correlation_general_rcl(npr,pow,i1,i2,order,A2scc)

  end subroutine wrapper_get_spin_colour_correlation_general_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_compute_spin_correlation_rcl(npr,p,legs,j,v,order,A2sc, &
                                                  momenta_check)

    use globals_rcl, only: dp
    use process_computation_rcl, only: compute_spin_correlation_rcl

    integer,          intent(in)  :: npr,legs,j
    real(dp),         intent(in)  :: p(0:3,1:legs)
    complex(dp),      intent(in)  :: v(0:3)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2sc
    logical,          intent(out) :: momenta_check

    call compute_spin_correlation_rcl(npr,p,j,v,order,A2sc,momenta_check)

  end subroutine wrapper_compute_spin_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_rescale_spin_correlation_rcl (npr,j,v,order,A2sc)

    use globals_rcl, only: dp
    use process_computation_rcl, only: rescale_spin_correlation_rcl

    integer,          intent(in)  :: npr,j
    complex(dp),      intent(in)  :: v(0:3)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2sc

    call rescale_spin_correlation_rcl (npr,j,v,order,A2sc)

  end subroutine wrapper_rescale_spin_correlation_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_spin_correlation_general_rcl(npr,pow,powlen,order,A2scc)

    use globals_rcl, only: dp
    use process_computation_rcl, only: get_spin_correlation_general_rcl

    integer,          intent(in)  :: npr,powlen
    integer,          intent(in)  :: pow(1:powlen)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2scc

    call get_spin_correlation_general_rcl(npr,pow,order,A2scc)

  end subroutine wrapper_get_spin_correlation_general_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_compute_spin_correlation_matrix_rcl(npr,p,legs,j,order,&
                                                         A2scm,momenta_check)

    use globals_rcl, only: dp
    use process_computation_rcl, only: compute_spin_correlation_matrix_rcl

    integer,          intent(in)  :: npr,legs,j
    real(dp),         intent(in)  :: p(0:3,1:legs)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2scm(0:3,0:3)
    logical,          intent(out) :: momenta_check

    call compute_spin_correlation_matrix_rcl(npr,p,j,order,A2scm,momenta_check)

  end subroutine wrapper_compute_spin_correlation_matrix_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_rescale_spin_correlation_matrix_rcl(npr,j,order,A2scm)

    use globals_rcl, only: dp
    use process_computation_rcl, only: rescale_spin_correlation_matrix_rcl

    integer,          intent(in)  :: npr,j
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2scm(0:3,0:3)

    call rescale_spin_correlation_matrix_rcl(npr,j,order,A2scm)

  end subroutine wrapper_rescale_spin_correlation_matrix_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_spin_correlation_matrix_general_rcl(npr,pow,powlen,order,A2scm)

    use globals_rcl, only: dp
    use process_computation_rcl, only: get_spin_correlation_matrix_general_rcl

    integer,          intent(in)  :: npr,powlen
    integer,          intent(in)  :: pow(1:powlen)
    character(len=*), intent(in)  :: order
    real(dp),         intent(out) :: A2scm(0:3,0:3)

    call get_spin_correlation_matrix_general_rcl(npr,pow,order,A2scm)

  end subroutine wrapper_get_spin_correlation_matrix_general_rcl

!------------------------------------------------------------------------------!

  subroutine get_legs_rcl(npr,legs,where)

    ! This subroutine extracts the number of legs of the process with
    ! process number "npr"

    use globals_rcl, only: prs, get_pr

    integer,     intent(in)  :: npr
    integer,     intent(out) :: legs
    character(*), intent(in) :: where

    integer :: pr

    call get_pr(npr,where,pr)

    legs = prs(pr)%legs

  end subroutine get_legs_rcl

!------------------------------------------------------------------------------!

  subroutine get_n_orders_rcl(n_orders)

    ! This subroutine extracts the number of different fundamental coupling
    ! powers

    use modelfile, only: get_n_orders_mdl

    integer, intent(out) :: n_orders

    n_orders = get_n_orders_mdl()

  end subroutine get_n_orders_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_momenta_rcl(npr,p,legs)

    use globals_rcl, only: dp
    use process_computation_rcl, only: get_momenta_rcl

    integer,  intent(in)  :: npr,legs
    real(dp), intent(out) :: p(0:3,1:legs)

    call get_momenta_rcl(npr,p)

  end subroutine wrapper_get_momenta_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_recola_version_rcl(ret_version,slen)
    use globals_rcl, only: get_recola_version_rcl
    character(len=10), intent(out) :: ret_version
    integer,           intent(out) :: slen

    call get_recola_version_rcl(ret_version)
    slen = len(trim(ret_version))
    ret_version = trim(ret_version)//CHAR(0)

  end subroutine wrapper_get_recola_version_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_modelname_rcl(ret_modelname,slen)
    use globals_rcl, only: get_modelname_rcl
    character(len=100), intent(out) :: ret_modelname
    integer,            intent(out) :: slen

    call get_modelname_rcl(ret_modelname)
    slen = len(trim(ret_modelname))
    ret_modelname = trim(ret_modelname)//CHAR(0)

  end subroutine wrapper_get_modelname_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_modelgauge_rcl(ret_modelgauge,slen)
    use globals_rcl, only: get_modelgauge_rcl
    character(len=100), intent(out) :: ret_modelgauge
    integer,            intent(out) :: slen

    call get_modelgauge_rcl(ret_modelgauge)
    slen = len(trim(ret_modelgauge))
    ret_modelgauge = trim(ret_modelgauge)//CHAR(0)

  end subroutine wrapper_get_modelgauge_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_driver_timestamp_rcl(ret_driver_timestamp,slen)

    use globals_rcl, only: get_driver_timestamp_rcl

    character(len=200), intent(out) :: ret_driver_timestamp
    integer,            intent(out) :: slen

    call get_driver_timestamp_rcl(ret_driver_timestamp)
    slen = len(trim(ret_driver_timestamp))
    ret_driver_timestamp = trim(ret_driver_timestamp)//CHAR(0)

  end subroutine wrapper_get_driver_timestamp_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_renoscheme_rcl(renoconst,renoscheme,slen)

    use input_rcl, only: get_renoscheme_rcl

    character(*),        intent(in) :: renoconst
    character(len=100), intent(out) :: renoscheme
    integer,            intent(out) :: slen

    call get_renoscheme_rcl(renoconst,renoscheme)
    slen = len(trim(renoscheme))
    renoscheme = trim(renoscheme)//CHAR(0)

  end subroutine wrapper_get_renoscheme_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_colour_configuration_rcl(npr,n,col,legs)

    use process_computation_rcl, only: get_colour_configuration_rcl

    integer, intent(in)  :: npr,n,legs
    integer, intent(out) :: col(1:legs)

    call get_colour_configuration_rcl(npr,n,col)

  end subroutine wrapper_get_colour_configuration_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_get_helicity_configuration_rcl(npr,n,col,legs)

    use process_computation_rcl, only: get_helicity_configuration_rcl

    integer, intent(in)  :: npr,n,legs
    integer, intent(out) :: col(1:legs)

    call get_helicity_configuration_rcl(npr,n,col)

  end subroutine wrapper_get_helicity_configuration_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_switchoff_coupling3_rcl(pa1,pa2,pa3,xlp_in)

    use input_rcl, only: switchoff_coupling3_rcl

    character(len=*),  intent(in) :: pa1,pa2,pa3
    integer,           intent(in) :: xlp_in

    call switchoff_coupling3_rcl(pa1,pa2,pa3,xlp_in=xlp_in)

  end subroutine wrapper_switchoff_coupling3_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_switchoff_coupling4_rcl(pa1,pa2,pa3,pa4,xlp_in)

    use input_rcl, only: switchoff_coupling4_rcl

    character(len=*),  intent(in) :: pa1,pa2,pa3,pa4
    integer,           intent(in) :: xlp_in

    call switchoff_coupling4_rcl(pa1,pa2,pa3,pa4,xlp_in=xlp_in)

  end subroutine wrapper_switchoff_coupling4_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_switchoff_coupling5_rcl(pa1,pa2,pa3,pa4,pa5,xlp_in)

    use input_rcl, only: switchoff_coupling5_rcl

    character(len=*),  intent(in) :: pa1,pa2,pa3,pa4,pa5
    integer,           intent(in) :: xlp_in

    call switchoff_coupling5_rcl(pa1,pa2,pa3,pa4,pa5,xlp_in=xlp_in)

  end subroutine wrapper_switchoff_coupling5_rcl

!------------------------------------------------------------------------------!

  subroutine wrapper_switchoff_coupling6_rcl(pa1,pa2,pa3,pa4,pa5,pa6,xlp_in)

    use input_rcl, only: switchoff_coupling6_rcl

    character(len=*),  intent(in) :: pa1,pa2,pa3,pa4,pa5,pa6
    integer,           intent(in) :: xlp_in

    call switchoff_coupling6_rcl(pa1,pa2,pa3,pa4,pa5,pa6,xlp_in=xlp_in)

  end subroutine wrapper_switchoff_coupling6_rcl

!------------------------------------------------------------------------------!

end module wrapper_rcl

!------------------------------------------------------------------------------!
