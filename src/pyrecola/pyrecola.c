//****************************************************************************//
//                                                                            //
//   pyrecola.c                                                               //
//   is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2       //
//                                                                            //
//   Copyright (C) 2016-2019 Ansgar Denner, Jean-Nicolas Lang and             //
//                           Sandro Uccirati                                  //
//                                                                            //
//   RECOLA2 is licenced under the GNU GPL version 3,                         //
//   see COPYING for details.                                                 //
//                                                                            //
//****************************************************************************//

#ifdef __cplusplus
extern "C" {
#endif
#include "Python.h"
#include <recola.h>
#include <stdio.h>

static char pyrecola_doc[] =
"This is the python interface to Recola2.";

/**********************
*  helper functions  *
**********************/

int pyobj_to_int(PyObject * obj_py)
{
#ifdef PYTHON3
  int ret = PyLong_AsLong(obj_py);
  return ret;
#else
  int ret = PyInt_AsLong(obj_py);
  return ret;
#endif
}
PyObject * int_to_pyobj(int in)
{
#ifdef PYTHON3
    return PyLong_FromLong(in);
#else
    return PyInt_FromLong(in);
#endif
}

int pyobj_pyint_check(PyObject * obj_py)
{
#ifdef PYTHON3
  return PyLong_Check(obj_py);
#else
  return PyInt_Check(obj_py);
#endif
}
/*static void build_string(PyObject *obj, const char **ret) {*/
char *pyobj_to_string(PyObject *obj) {
#ifdef PYTHON3
  PyObject* str = PyUnicode_AsUTF8String(obj);
  char *ret =  PyBytes_AS_STRING(str);
  Py_XDECREF(str);
  return ret;
#else
  return PyString_AsString(obj);
#endif
}

static PyObject *
build_c_momenta(const int process_id, PyObject* momenta_py,
                double momenta_c[][4], int legs)
{
  int numLines;
  numLines = PyList_Size(momenta_py);

  if(legs != numLines){
    return PyErr_Format(PyExc_Exception,
        "Number of particles and length of momenta do not match.\n"\
        "Given length: %d, expected length: %d",
        numLines, legs);
  }

  int i;
  for (i=0; i<numLines; i++)
  {
    PyObject *momentum_py = PyList_GetItem(momenta_py, i);
    PyObject *iter = PyObject_GetIter(momentum_py);
    if (!iter)
    {
      // error, expected list/iterator
      return PyErr_Format(PyExc_Exception,
      "Given momenta %d is not of list/iterator type.", i);
    }
    int j = 0;
    while (1) {
      PyObject *next = PyIter_Next(iter);
      if (!next) {
        // nothing left in the iterator
        break;
      }
      if (j > 3)
      {
        // error, expected four-momentum
        return PyErr_Format(PyExc_Exception,
        "Momentum %d exeeds the expected length of 4.", i);
      }
      if (!PyFloat_Check(next))
      {
        // error, we were expecting a complex floating point
        return PyErr_Format(PyExc_Exception,
        "Momentum %d component %d is not a (floating point) complex.", i, j);
      }
      momenta_c[i][j] = PyFloat_AsDouble(next);
      Py_DECREF(next);
      j = j + 1;
    }
    Py_DECREF(iter);
  }
  return momenta_py;
}
static PyObject *
build_c_integer_array(const int length, PyObject* array_py,
                      int array_c[], const char *array_name)
{
  int lengthp = PyList_Size(array_py);

  if(lengthp != length){
    return PyErr_Format(PyExc_Exception,
    "Size of %s does not match the expected length.\n"\
    "Given length: %d, expected (from model/process) length: %d",
    array_name, lengthp, length);
  }
  int i;
  for (i=0; i<length; i++)
  {
    PyObject *tmp = PyList_GetItem(array_py, i);
    array_c[i] = pyobj_to_int(tmp);
  }
  return array_py;
}

static PyObject *
build_c_polvec(PyObject *polvec_py, dcomplex polvec_c[4])
{
  PyObject *iter = PyObject_GetIter(polvec_py);
  if (!iter)
  {
    // error not iterator
    return PyErr_Format(PyExc_Exception,
    "Polvec is not an iterator/list.");
  }
  int j = 0;
  while (1)
  {
    PyObject *next = PyIter_Next(iter);
    if (!next)
    {
      // nothing left in the iterator
      break;
    }
    if (j > 3)
    {
      // error, size exceeded
      return PyErr_Format(PyExc_Exception,
      "Polvec exeeds the expected length of 4.");
    }
    if (!PyComplex_Check(next)) {
      // error, we were expecting a complex floating point value
      return PyErr_Format(PyExc_Exception,
      "Polvec component %d is not a (floating point) complex.", j);
    }
    polvec_c[j].dr = PyComplex_RealAsDouble(next);
    polvec_c[j].di = PyComplex_ImagAsDouble(next);
    Py_DECREF(next);
    j = j + 1;
  }
  Py_DECREF(iter);
  return polvec_py;
}

/**********************
*  module input_rcl  *
**********************/

PyDoc_STRVAR(set_qcd_rescaling_doc,
"set_qcd_rescaling_rcl(val)\n\n\
Set the use of rescaling/running methods of RECOL1.\n\
Note that this only makes sense for SM-QCD-like models as\n\
the hard-coded beta functions are used.\n\
val = True  -> Enabled\n\
val = False -> Disabled");
static PyObject *
set_qcd_rescaling_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_qcd_rescaling_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_fermionloop_optimization_doc,
"set_fermionloop_optimization_rcl(val)\n\n\
Set the fermion loop optimization. Disabled by default.\n\
val = True  -> identifies equal fermion loops for degenerate fermion masses\n\
val = False -> no optimization performed");
static PyObject *
set_fermionloop_optimization_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_fermionloop_optimization_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_helicity_optimization_doc,
"set_helicity_optimization_rcl(val)\n\n\
Set the helicity optimization level to 0, 1 or 2.\n\
val = True  -> standard helicity optimization\n\
val = False -> no helicity optimization");
static PyObject *
set_helicity_optimization_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_helicity_optimization_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_colour_optimization_doc,
"set_colour_optimization_rcl(val)\n\n\
Set the colour optimization level to 0, 1 or 2.\n\
val = 0 -> no colour optimization\n\
val = 1 -> standard colour-flow optimization\n\
val = 2 -> improved colour-flow optimization\n\
           (no explicit computation of external U1 Gluons)");
static PyObject *
set_colour_optimization_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_colour_optimization_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_compute_A12_doc,
"set_compute_A12_rcl(val)\n\n\
Set whether to compute the squared loop contribution A1^2.\n\
val = True  -> Enabled\n\
val = False -> Disabled");
static PyObject *
set_compute_A12_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_compute_A12_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_masscut_doc,
"set_masscut_rcl(m)\n\n\
Sets mass limit `m` below which particles are considered massless.\n\
m = float -> positve number");
static PyObject *
set_masscut_py(PyObject *self, PyObject *args)
{
  double value;

  if (!PyArg_ParseTuple(args, "d", &value))
      return NULL;

  set_masscut_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_particle_doc,
"set_light_particle_rcl(p)\n\n\
Tag a particle `p` as a light one, omitting mass terms whenever possible.\n\
This corresponds to mass regularization.\n\
p = string -> particle name (anti-particle automatically set to light)");
static PyObject *
set_light_particle_py(PyObject *self, PyObject *args)
{
  const char* p;

  if (!PyArg_ParseTuple(args, "s", &p))
      return NULL;

  set_light_particle_rcl(p);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unset_light_particle_doc,
"unset_light_particle_rcl(p)\n\n\
Untag a particle `p` as a light one, keeping the full mass dependence\n\
p = string -> particle name (anti-particle automatically unset to light)");
static PyObject *
unset_light_particle_py(PyObject *self, PyObject *args)
{
  const char* p;

  if (!PyArg_ParseTuple(args, "s", &p))
      return NULL;

  unset_light_particle_rcl(p);

  Py_INCREF(Py_None);
  return Py_None;
}
//deprecated
PyDoc_STRVAR(use_dim_reg_soft_doc,
"use_dim_reg_soft_rcl\n\n\
Deprecated. Call has no effect.");
static PyObject *
use_dim_reg_soft_py(PyObject *self, PyObject *args)
{
  use_dim_reg_soft_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
//deprecated
PyDoc_STRVAR(use_mass_reg_soft_doc,
"use_mass_reg_soft_rcl(m)\n\n\
Deprecated. Call has no effect.\n\
m = float -> positive number");
static PyObject *
use_mass_reg_soft_py(PyObject *self, PyObject *args)
{
  double m;

  if (!PyArg_ParseTuple(args, "d", &m))
    return NULL;

  use_mass_reg_soft_rcl(m);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_recola_base_doc,
"use_recola_base_rcl(rb)\n\n\
Set whether current expressions are computed by recola or by the model file.\n\
rb = True -> compute inside recola\n\
rb = False -> compute inside model file");
static PyObject *
use_recola_base_py(PyObject *self, PyObject *args)
{
  int rb;

  if (!PyArg_ParseTuple(args, "i", &rb))
    return NULL;

  use_recola_base_rcl(rb);
  Py_INCREF(Py_None);
  return Py_None;
}
//deprecated
PyDoc_STRVAR(set_mass_reg_soft_doc,
"set_mass_reg_soft_rcl(m)\n\n\
Deprecated. Call has no effect.\n\
m = float -> positive number");
static PyObject *
set_mass_reg_soft_py(PyObject *self, PyObject *args)
{
  double m;

  if (!PyArg_ParseTuple(args, "d", &m))
    return NULL;

  set_mass_reg_soft_rcl(m);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_delta_uv_doc,
"set_delta_uv_rcl(d)\n\n\
This subroutine sets the finite part of the UV subtracted term:\n\
:m:`\\Delta_\\mathrm{UV} = \\frac{1}{\\epsilon} - \\gamma + \\log (4 \\pi)` to ``d``.\n\n\
Args:\n\n\
  d (float): :m:`\\Delta_\\mathrm{UV}`");
static PyObject *
set_delta_uv_py(PyObject *self, PyObject *args)
{
  double d;

  if (!PyArg_ParseTuple(args, "d", &d))
      return NULL;

  set_delta_uv_rcl(d);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_delta_uv_doc,
"get_delta_uv_rcl()\n\n\
Gets the finite part of the UV subtracted term :m:`\\Delta_\\mathrm{UV}`.\n\n\
Returns:\n\
  (float): finite part of :m:`\\Delta_\\mathrm{UV}`");
static PyObject *
get_delta_uv_py(PyObject *self, PyObject *args)
{
  double value;
  get_delta_uv_rcl(&value);
  return Py_BuildValue("d", value);
}
PyDoc_STRVAR(set_delta_ir_doc,
"set_delta_ir_rcl(d1, d2)\n\n\
This subroutine sets the finite part of the IR subtracted term\n\
:m:`\\Delta_\\mathrm{IR} = \\frac{1}{\\epsilon} - \\gamma + \\log (4 \\pi)` to ``d1``\n\
:m:`\\Delta_\\mathrm{IR2} = \\frac{(4 \\pi)^\\epsilon \\Gamma(1+\\epsilon)}{\\epsilon^2}` to ``d2``.\n\n\
Args:\n\n\
  d1 (float): :m:`\\Delta_\\mathrm{IR}`\n\
  d2 (float): :m:`\\Delta_\\mathrm{IR2}`");
static PyObject *
set_delta_ir_py(PyObject *self, PyObject *args)
{
  double d1,d2;

  if (!PyArg_ParseTuple(args, "dd", &d1, &d2))
      return NULL;

  set_delta_ir_rcl(d1, d2);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_delta_ir_doc,
"get_delta_ir_rcl()\n\n\
Gets the finite parts of the IR subtracted terms :m:`\\Delta_\\mathrm{IR}`, :m:`\\Delta_\\mathrm{IR2}`.\n\n\
Returns:\n\
  (float, float): finite part of (:m:`\\Delta_\\mathrm{IR}`, :m:`\\Delta_\\mathrm{IR2}`)");
static PyObject *
get_delta_ir_py(PyObject *self, PyObject *args)
{
  double d1,d2;
  get_delta_ir_rcl(&d1, &d2);
  return Py_BuildValue("dd", d1,d2);
}
PyDoc_STRVAR(set_mu_uv_doc,
"set_mu_uv_rcl(m)\n\n\
Sets the UV scale :m:`\\mu_\\mathrm{UV}` to ``m``.\n\n\
Args:\n\n\
  m (float): :m:`\\mu_\\mathrm{UV}` scale");
static PyObject *
set_mu_uv_py(PyObject *self, PyObject *args)
{
  double mu_uv;

  if (!PyArg_ParseTuple(args, "d", &mu_uv))
      return NULL;

  set_mu_uv_rcl(mu_uv);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_mu_uv_doc,
"get_mu_uv_rcl()\n\n\
Returns the UV scale :m:`\\mu_\\mathrm{UV}`.\n\n\
Returns:\n\
  (float): :m:`\\mu_\\mathrm{UV}` scale");
static PyObject *
get_mu_uv_py(PyObject *self, PyObject *args)
{
  double value;
  get_mu_uv_rcl(&value);
  return Py_BuildValue("d", value);
}
PyDoc_STRVAR(set_mu_ir_doc,
"set_mu_ir_rcl(m)\n\n\
Sets the IR scale :m:`\\mu_\\mathrm{IR}` to ``m``.\n\n\
Args:\n\n\
  m (float): :m:`\\mu_\\mathrm{IR}` scale");
static PyObject *
set_mu_ir_py(PyObject *self, PyObject *args)
{
  double mu_ir;

  if (!PyArg_ParseTuple(args, "d", &mu_ir))
      return NULL;

  set_mu_ir_rcl(mu_ir);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_mu_ir_doc,
"get_mu_ir_rcl\n\n\
Returns the IR scale :m:`\\mu_\\mathrm{IR}`.\n\n\
Returns:\n\
  (float): :m:`\\mu_\\mathrm{IR}` scale");
static PyObject *
get_mu_ir_py(PyObject *self, PyObject *args)
{
  double value;
  get_mu_ir_rcl(&value);
  return Py_BuildValue("d", value);
}
PyDoc_STRVAR(set_mu_ms_doc,
"set_mu_ms_rcl(m)\n\n\
Sets the :m:`\\overline{\\mathrm{MS}}` scale\n\
:m:`\\mu_{\\overline{\\mathrm{MS}}}` to ``m``.\n\n\
Args:\n\n\
  m (float): :m:`\\mu_{\\overline{\\mathrm{MS}}}` scale");
static PyObject *
set_mu_ms_py(PyObject *self, PyObject *args)
{
  double mu_ms;

  if (!PyArg_ParseTuple(args, "d", &mu_ms))
      return NULL;

  set_mu_ms_rcl(mu_ms);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_mu_ms_doc,
"get_mu_ms_rcl()\n\n\
Returns the :m:`\\overline{\\mathrm{MS}}` scale\n\
:m:`\\mu_{\\overline{\\mathrm{MS}}}`.\n\n\
Returns:\n\
  (float): :m:`\\mu_{\\overline{\\mathrm{MS}}}` scale");
static PyObject *
get_mu_ms_py(PyObject *self, PyObject *args)
{
  double value;
  get_mu_ms_rcl(&value);
  return Py_BuildValue("d", value);
}
//deprecated
PyDoc_STRVAR(get_renormalization_scale_doc,
"get_renormalization_scale_rcl()\n\n\
Deprecated. Calls get_mu_ms_rcl instead.");
static PyObject *
get_renormalization_scale_py(PyObject *self, PyObject *args)
{
  double value;
  get_renormalization_scale_rcl(&value);
  return Py_BuildValue("d", value);
}
PyDoc_STRVAR(get_flavour_scheme_doc,
"get_flavour_scheme_rcl()\n\n\
Returns the value of the flavour-scheme `Nf`.");
static PyObject *
get_flavour_scheme_py(PyObject *self, PyObject *args)
{
  int value;
  get_flavour_scheme_rcl(&value);
  return Py_BuildValue("i", value);
}
PyDoc_STRVAR(set_complex_mass_scheme_doc,
"set_complex_mass_scheme_rcl()\n\n\
Selects the Complex-Mass scheme for the renormalization\n\
of particles.");
static PyObject *
set_complex_mass_scheme_py(PyObject *self, PyObject *args)
{
  set_complex_mass_scheme_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_on_shell_scheme_doc,
"set_on_shell_scheme_rcl()\n\n\
Selects the on-shell scheme for the renormalization\n\
of particles.");
static PyObject *
set_on_shell_scheme_py(PyObject *self, PyObject *args)
{
  set_on_shell_scheme_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_momenta_correction_doc,
"set_momenta_correction_rcl(mc)\n\n\
Enable or disable the internal momentum corrections.\n\
mc = True  -> recola tries to correct the momenta\n\
mc = False -> recola does not try to correct the momenta");
static PyObject *
set_momenta_correction_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_momenta_correction_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_print_level_amplitude_doc,
"set_print_level_amplitude_rcl(n)\n\n\
Set the print level for amplitudes to ``n``.\n\n\
Args:\n\
  n (int): print level\n\n\
    * n = 0: nothing is printed\n\
    * n = 1: the non vanishining amplitudes `A(c,h)` are printed at LO\n\
             and NLO (if NLO is computed) for each coulour structure\n\
             and helicity state\n\
    * n = 2: reports individual NLO (if computed) contributions, i.e.\n\
             bare-loop, counterterms and rational parts\n\
             are printed separately");
static PyObject *
set_print_level_amplitude_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_print_level_amplitude_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_print_level_squared_amplitude_doc,
"set_print_level_squared_amplitude_rcl(n)\n\n\
Set the print level for squared amplitudes to ``n``.\n\n\
Args:\n\n\
  n (int): print level\n\n\
    * n = 0: nothing is printed\n\
    * n = 1: the non vanishining squared amplitudes A2\n\
             summed/averaged over colour and helicities are\n\
             printed at LO and NLO (if NLO is computed)\n\
    * n = 2: in addition to n=1, the NLO (if computed) contributions\n\
             to A2 from bare-loop, counterterms and rational part\n\
             are also printed\n\
    * n = 3: prints the squared helicity amplitudes \n\
             summed/averaged over colour at LO and NLO\n\
             (if NLO is computed) for each helicity");
static PyObject *
set_print_level_squared_amplitude_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_print_level_squared_amplitude_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_print_level_correlations_doc,
"set_print_level_correlations_rcl(n)\n\n\
Set the print level for (colour/spin) correlations to ``n``.\n\n\
Args:\n\n\
  n (int): print level\n\n\
    * n = 0: nothing is printed\n\
    * n = 1: the non vanishining colour-correlated Born squared\n\
             amplitudes are printed for each emitter and\n\
             spectator combination");
static PyObject *
set_print_level_correlations_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_print_level_correlations_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_print_level_parameters_doc,
"set_print_level_parameters_rcl(n)\n\n\
Set the level for printing parameters of the theory to ``n``.\n\n\
Args:\n\
  n (int): print level\n\n\
    * n = 0: nothing is printed\n\
    * n = 1: tree-level input parameters are printed\n\
    * n = 2: tree-level input + derived parameters are printed\n\
    * n = 3: input + derived + counterterm parameters are printed\n\
    * n = 4: only counterterm parameters are printed");
static PyObject *
set_print_level_parameters_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_print_level_parameters_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_draw_level_branches_doc,
"set_draw_level_branches_rcl(n)\n\n\
Set the draw level of the off-shell currents to ``n``.\n\n\
Args:\n\n\
  n (int): print level\n\n\
    * n = 0: the .tex file is not generated\n\
    * n = 1: the .tex file is generated without colour structures\n\
    * n = 2: the .tex file is generated with colour structures");
static PyObject *
set_draw_level_branches_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_draw_level_branches_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_print_level_RAM_doc,
"set_print_level_RAM_rcl(n)\n\n\
Not yet supported.");
static PyObject *
set_print_level_RAM_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_print_level_RAM_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(scale_coupling3_doc,
"scale_coupling3_rcl(fac,p1,p2,p3)\n\n\
Not yet supported.");
static PyObject *
scale_coupling3_py(PyObject *self, PyObject *args)
{
  PyObject *value;
  const char *p1,*p2,*p3;
  if (!PyArg_ParseTuple(args, "Osss", value,&p1,&p2,&p3))
      return NULL;

  if (!PyComplex_Check(value)) {
    /* TODO:  <25-07-17, Jean-Nicolas Lang> */
  }

  dcomplex fac;
  fac.dr = PyComplex_RealAsDouble(value);
  fac.di = PyComplex_ImagAsDouble(value);

  scale_coupling3_rcl(fac,p1,p2,p3);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(scale_coupling4_doc,
"scale_coupling4_rcl(fac,p1,p2,p3,p4)\n\n\
Not yet supported.");
static PyObject *
scale_coupling4_py(PyObject *self, PyObject *args)
{
  PyObject *value;
  const char *p1,*p2,*p3,*p4;
  if (!PyArg_ParseTuple(args, "Dssss", value,&p1,&p2,&p3,&p4))
      return NULL;

  dcomplex fac;
  fac.dr = PyComplex_RealAsDouble(value);
  fac.di = PyComplex_ImagAsDouble(value);

  scale_coupling4_rcl(fac,p1,p2,p3,p4);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(switchoff_coupling3_doc,
"switchoff_coupling3_rcl(p1,p2,p3,btype=None)\n\n\
Switches off the Feynman rule associated with the given fields.\n\
Args:\n\
  p1,p2,p3 (str): field string identifier\n\
  btype (int): branch type which should be unselected.\n\n\
      * btype = 0,1: vertices entering tree and loop amplitude\n\
      * btype = 2: vertices entering counterterm amplitude\n\
      * btype = 3: vertices entering counterterm amplitude\n\
      * btype = -1 (default): all vertices");
static PyObject *
switchoff_coupling3_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  const char *p1,*p2,*p3;
  PyObject *btype_py = Py_None;
  static char *kwlist[] = {"p1", "p2", "p3", "btype", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "sss|O", kwlist,
        &p1,&p2,&p3,&btype_py))
      return NULL;
  if (btype_py != Py_None) {
    int btype = pyobj_to_int(btype_py);
    switchoff_coupling3_rcl(p1,p2,p3,btype);
  } else{
    switchoff_coupling3_rcl(p1,p2,p3,-1);
  }
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(switchoff_coupling4_doc,
"switchoff_coupling4_rcl(p1,p2,p3,p4,btype=-1)\n\n\
Switches off the Feynman rule associated with the given fields.\n\
Args:\n\
  p1,p2,p3,p4 (str): string identifier of fields\n\
  btype (int): branch type which should be unselected.\n\n\
      * btype = 0,1: vertices entering tree and loop amplitude\n\
      * btype = 2: vertices entering counterterm amplitude\n\
      * btype = 3: vertices entering counterterm amplitude\n\
      * btype = -1 (default): all vertices");
static PyObject *
switchoff_coupling4_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  const char *p1,*p2,*p3,*p4;
  PyObject *btype_py = Py_None;
  static char *kwlist[] = {"p1", "p2", "p3", "p4", "btype", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "ssss|O", kwlist,
        &p1,&p2,&p3,&p4,&btype_py))
      return NULL;
  if (btype_py != Py_None) {
    int btype = pyobj_to_int(btype_py);
    switchoff_coupling4_rcl(p1,p2,p3,p4,btype);
  } else{
    switchoff_coupling4_rcl(p1,p2,p3,p4,-1);
  }

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(switchoff_coupling5_doc,
"switchoff_coupling5_rcl(p1,p2,p3,p4,p5,btype=-1)\n\n\
Switches off the Feynman rule associated with the given fields.\n\
Args:\n\
  p1,p2,p3,p4,p5 (str): string identifier of fields\n\
  btype (int): branch type which should be unselected.\n\n\
      * btype = 0,1: vertices entering tree and loop amplitude\n\
      * btype = 2: vertices entering counterterm amplitude\n\
      * btype = 3: vertices entering counterterm amplitude\n\
      * btype = -1 (default): all vertices");
static PyObject *
switchoff_coupling5_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  const char *p1,*p2,*p3,*p4,*p5;
  PyObject *btype_py = Py_None;
  static char *kwlist[] = {"p1", "p2", "p3", "p4", "p5", "btype", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "sssss|O", kwlist,
        &p1,&p2,&p3,&p4,&p5,&btype_py))
      return NULL;
  if (btype_py != Py_None) {
    int btype = pyobj_to_int(btype_py);
    switchoff_coupling5_rcl(p1,p2,p3,p4,p5,btype);
  } else{
    switchoff_coupling5_rcl(p1,p2,p3,p4,p5,-1);
  }

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(switchoff_coupling6_doc,
"switchoff_coupling6_rcl(p1,p2,p3,p4,p5,p6,btype=-1)\n\n\
Switches off the Feynman rule associated with the given fields.\n\
Args:\n\
  p1,p2,p3,p4,p5,p6 (str): string identifier of fields\n\
  btype (int): branch type which should be unselected.\n\n\
      * btype = 0,1: vertices entering tree and loop amplitude\n\
      * btype = 2: vertices entering counterterm amplitude\n\
      * btype = 3: vertices entering counterterm amplitude\n\
      * btype = -1 (default): all vertices");
static PyObject *
switchoff_coupling6_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  const char *p1,*p2,*p3,*p4,*p5,*p6;
  PyObject *btype_py = Py_None;
  static char *kwlist[] = {"p1", "p2", "p3", "p4", "p5", "p6", "btype", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "ssssss|O", kwlist,
        &p1,&p2,&p3,&p4,&p5,&p6,&btype_py))
      return NULL;
  if (btype_py != Py_None) {
    int btype = pyobj_to_int(btype_py);
    switchoff_coupling6_rcl(p1,p2,p3,p4,p5,p6,btype);
  } else{
    switchoff_coupling6_rcl(p1,p2,p3,p4,p5,p6,-1);
  }

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(switchon_resonant_selfenergies_doc,
"switchon_resonant_selfenergies_rcl\n\n\
Sets the value of `resSE` to .true.");
static PyObject *
switchon_resonant_selfenergies_py(PyObject *self, PyObject *args)
{
  switchon_resonant_selfenergies_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(switchoff_resonant_selfenergies_doc,
"switchoff_resonant_selfenergies_rcl\n\n\
Sets the value of `resSE` to .true.");
static PyObject *
switchoff_resonant_selfenergies_py(PyObject *self, PyObject *args)
{
  switchoff_resonant_selfenergies_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_dynamic_settings_doc,
"set_dynamic_settings_rcl(i)\n\n\
Sets the dynamic_settings flag to ``i``.\n\n\
Args:\n\
  i (int): dynamic settings level\n\n\
    * i = 0 : only :m:`\\alpha_\\mathrm{s}` can be set after :py:meth:`generate_processes_rcl` \n\
    * i = 1 : :m:`\\Delta_\\mathrm{UV}`, :m:`\\mu_\\mathrm{UV}`, :m:`\\Delta_\\mathrm{IR}`, :m:`\\Delta_\\mathrm{IR2}` and :m:`\\mu_\\mathrm{IR}`\n\
              can be set after :py:meth:`generate_processes_rcl`");
static PyObject *
set_dynamic_settings_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_dynamic_settings_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_ifail_doc,
"set_ifail_rcl(i)\n\n\
Sets the ifail flag to `i`.\n\
The ifail flag regulates the behaviour of Recola in case of an error.\n\
ifail = -1 -> the code does not stop when an error occurs\n\
ifail =  0 -> the code stops, when an error occurs (DEFAULT)");
static PyObject *
set_ifail_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_ifail_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_ifail_doc,
"get_ifail_rcl()\n\n\
Returns the value of the ifail flag in.\n\
The ifail flag regulates the behaviour of Recola in case of an error.\n\
return = 0 -> no errors occured\n\
return = 1 -> an error occured because of a wrong call caused by the user\n\
return = 2 -> an error occured because of an internal bug of Recola");
static PyObject *
get_ifail_py(PyObject *self, PyObject *args)
{
  int value;
  get_ifail_rcl(&value);
  return Py_BuildValue("i", value);
}
PyDoc_STRVAR(set_iwarn_doc,
"set_iwarn_rcl(i)\n\n\
Sets the iwarn flag to `i`.\n\
The iwarn flag regulates the behaviour of Recola in case of an error.\n\
iwarn = -1 -> the code does not stop when a warning occurs (DEFAULT)\n\
iwarn =  0 -> the code stops, when a warning occurs");
static PyObject *
set_iwarn_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_iwarn_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_iwarn_doc,
"get_iwarn_rcl()\n\n\
Returns the value of the iwarn flag in.\n\
The iwarn flag regulates the behaviour of Recola in case of an error.\n\
return = 0 -> no warning occured\n\
return = 1 -> a warning occured because of a wrong call caused by the user");
static PyObject *
get_iwarn_py(PyObject *self, PyObject *args)
{
  int value;
  get_iwarn_rcl(&value);
  return Py_BuildValue("i", value);
}
PyDoc_STRVAR(set_collier_mode_doc,
"set_collier_mode_rcl(mode)\n\n\
Set the collier mode to ``mode``.\n\n\
Args:\n\
  mode (int): collier mode\n\n\
    * n = 1: use COLI\n\
    * n = 2: use DD\n\
    * n = 3: use COLI and DD");
static PyObject *
set_collier_mode_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_collier_mode_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_collier_output_dir_doc,
"set_collier_output_dir_rcl(dir)\n\n\
Sets the collier output folder path to ``dir`` which\n\
can be a relative or absolute path.\n\n\
Args:\n\
  dir (str): output folder path");
static PyObject *
set_collier_output_dir_py(PyObject *self, PyObject *args)
{
  const char* p;

  if (!PyArg_ParseTuple(args, "s", &p))
      return NULL;

  set_collier_output_dir_rcl(p);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_cache_mode_doc,
"set_cache_mode_rcl(mode)\n\n\
Sets the cache mode to ``mode``.\n\n\
Args:\n\
  mode (int): cache mode\n\n\
    * n = 0: cache entirely disabled\n\
    * n = 1: default, local cache for every process, cache can be further\n\
             split or disabled using :py:meth:`split_collier_cache_rcl`.\n\
    * n = 2: global cache for all processes. The user initializes a desired\n\
             number of caches with :py:meth:`set_global_cache_rcl`. Switching\n\
             between the caches is performed using :py:meth:`switch_global_cache_rcl`.");
static PyObject *
set_cache_mode_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_cache_mode_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_global_cache_doc,
"set_global_cache_rcl(ncache)\n\n\
Sets the number of active global caches to ``ncache``.\n\n\
Args:\n\
  ncache (int): number of global caches");
static PyObject *
set_global_cache_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_global_cache_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(switch_global_cache_doc,
"switch_global_cache_rcl(cache)\n\n\
Activates the cache with number ``cache``.\n\n\
Args:\n\
  cache (int): cache number");
static PyObject *
switch_global_cache_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  switch_global_cache_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_compute_ir_poles_doc,
"set_compute_ir_poles_rcl(mode)\n\n\
Sets whether IR poles are computed when calling :py:meth:`compute_process_rcl`.\n\
Currently only supported for squared amplitudes.\n\n\
Args:\n\
  mode (int): ir poles computation method\n\n\
      * mode = 0: no IR poles computed\n\
      * mode = 1: IR poles via rescaling (evaluates virtual amplitude three times), requires :py:meth:`set_dynamic_settings_rcl` to be enabled\n\
      * mode = 2: IR poles via I-dip formula (not yet implemented)\n\n\
Example:\n\
  >>> set_dynamic_settings_rcl(1)\n\
  >>> set_compute_ir_poles_rcl(2)\n\
  >>> # .. process computation call(s)\n\
  >>> ep1 = get_squared_amplitude_rcl(1,'NLO-IR1',als=0)\n\
  >>> ep2 = get_squared_amplitude_rcl(1,'NLO-IR2',als=0)");
static PyObject *
set_compute_ir_poles_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_compute_ir_poles_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_output_file_doc,
"set_output_file_rcl(x)\n\n\
Sets the output file name to ``x``.\n\n\
Args:\n\
  x (str): output file name");
static PyObject *
set_output_file_py(PyObject *self, PyObject *args)
{
  const char* p;

  if (!PyArg_ParseTuple(args, "s", &p))
      return NULL;

  set_output_file_rcl(p);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_log_mem_doc,
"set_log_mem_rcl(logmem)\n\n\
Sets the switch for taking memory snapshots.\n\n\
Args:\n\
  logmem (bool): memory logger activated\n\n\
      * logmem = False: memory is not logged.\n\
      * logmem = True: memory snapshots written to memory_rcl.log\n\
                       in the current working directory.");
static PyObject *
set_log_mem_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_log_mem_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_crossing_symmetry_doc,
"set_crossing_symmetry_rcl(val)\n\n\
Sets the switch for using crossing symmetry.\n\
val = False -> no crossing symmetry used when generating processes.\n\
val = True  -> crossing symmetry used when generating processes.");
static PyObject *
set_crossing_symmetry_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_crossing_symmetry_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_init_collier_doc,
"set_init_collier_rcl(initcollier)\n\n\
Sets the switch for initializing collier.\n\
initcollier = False -> collier is not initialized\n\
initcollier = True  -> collier is initialized");
static PyObject *
set_init_collier_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_init_collier_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_parameter_doc,
"set_parameter_rcl(pname,pvalue)\n\n\
Sets value for the parameter with name ``pname`` to ``pvalue``.\n\
For a list of parameters run recola with a non-zero print level in\n\
:py:meth:`set_print_level_parameters_rcl`.\n\n\
:param pname: internal parameter name\n\
:type  pname: str\n\
:param pvalue: parameter value\n\
:type  pvalue: complex\n\n\
.. note::\n\n\
   Passing an imaginary may be ignored due to parameters being defined as real type.");
static PyObject *
set_parameter_py(PyObject *self, PyObject *args)
{
  const char* pname;
  Py_complex pvalue;

  if (!PyArg_ParseTuple(args, "sD", &pname, &pvalue))
      return NULL;

  dcomplex pvaluec;
  pvaluec.dr = pvalue.real;
  pvaluec.di = pvalue.imag;

  set_parameter_rcl(pname, pvaluec);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_parameter_doc,
"get_parameter_rcl(pname)\n\n\
Gets value for the parameter with name ``pname``.\n\n\
Args:\n\
  pname (str): parameter name\n\n\
Returns:\n\
  (float, float): real and imaginary part as tuple.");
static PyObject *
get_parameter_py(PyObject *self, PyObject *args)
{
  const char* pname;

  if (!PyArg_ParseTuple(args, "s", &pname))
      return NULL;

  dcomplex pvaluec;
  get_parameter_rcl(pname, &pvaluec);

  return Py_BuildValue("dd", pvaluec.dr, pvaluec.di);
}
PyDoc_STRVAR(set_renoscheme_doc,
"set_renoscheme_rcl(rname,rscheme)\n\n\
Sets the renormalization scheme for the parameter `rname` to `rscheme`.\n\
rname   = string -> internal ct parameter name\n\
rscheme = string -> internal renormalization scheme name");
static PyObject *
set_renoscheme_py(PyObject *self, PyObject *args)
{
  const char* rname;
  const char* rscheme;

  if (!PyArg_ParseTuple(args, "ss", &rname, &rscheme))
      return NULL;

  set_renoscheme_rcl(rname, rscheme);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_resonant_particle_doc,
"set_resonant_particle_rcl(pname)\n\n\
Tags the particle with name ``pname`` as resonant.\n\n\
:param pname: internal name of the partice, see :ref:`fieldconventions`\n\
:type  pname: str");
static PyObject *
set_resonant_particle_py(PyObject *self, PyObject *args)
{
  const char* p;

  if (!PyArg_ParseTuple(args, "s", &p))
      return NULL;

  set_resonant_particle_rcl(p);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_quarkline_doc,
"set_quarkline_rcl(npr,q1,q2)\n\n\
Enforces a quark-flow between two quarks ``q1`` and ``q2``.\n\n\
Args:\n\
  npr (int): process number\n\
  q1 (int): 1st quark position\n\
  q2 (int): 2st quark position\n\n\
Note:\n\
  All quark pairs need to be assigned\n\
  for this function to work as expected.\n\n\
Examples:\n\
  In order to select the u and t channel diagrams,\n\
  define two processes like this:\n\n\
  >>> define_process_rcl(2,'u u -> e+ nu_e mu+ nu_mu d d A','LO')\n\
  >>> set_quarkline_rcl(2,1,7)\n\
  >>> set_quarkline_rcl(2,2,8)\n\
  >>> define_process_rcl(3,'u u -> e+ nu_e mu+ nu_mu d d A','LO')\n\
  >>> set_quarkline_rcl(3,2,7)\n\
  >>> set_quarkline_rcl(3,1,8)\n\n\
  The matrix elements (2,3) need to be computed separately and added up.\n\
  This constitutes the VBS approximation where interferences between\n\
  t and u channels diagrams are neglected.");
static PyObject *
set_quarkline_py(PyObject *self, PyObject *args)
{
  int npr,q1,q2;

  if (!PyArg_ParseTuple(args, "iii", &npr,&q1,&q2))
      return NULL;

  set_quarkline_rcl(npr,q1,q2);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(reset_vertices_doc,
"reset_vertices_rcl\n\n\
Resets the vertices, couplings and counterterm couplings in the model.");
static PyObject *
reset_vertices_py(PyObject *self, PyObject *args)
{

  reset_vertices_rcl();

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(reset_couplings_doc,
"reset_couplings_rcl\n\n\
Resets the couplings and counterterm couplings in the model.");
static PyObject *
reset_couplings_py(PyObject *self, PyObject *args)
{

  reset_couplings_rcl();

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(reset_ctcouplings_doc,
"reset_ctcouplings_rcl\n\n\
Resets the counterterm couplings in the model.");
static PyObject *
reset_ctcouplings_py(PyObject *self, PyObject *args)
{

  reset_ctcouplings_rcl();

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(print_collier_statistics_doc,
"print_collier_statistics_rcl\n\n\
Prints the collier statistics to the currently opened output channel.");
static PyObject *
print_collier_statistics_py(PyObject *self, PyObject *args)
{

  print_collier_statistics_rcl();

  Py_INCREF(Py_None);
  return Py_None;
}

// additional stuff, only used in combination with rept1l
static PyObject *
set_compute_selfenergy_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_compute_selfenergy_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_compute_selfenergy_offshell_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_compute_selfenergy_offshell_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_compute_tadpole_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_compute_tadpole_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_lp_py(PyObject *self, PyObject *args)
{
  int lp_order;
  int value;

  if (!PyArg_ParseTuple(args, "ii", &lp_order, &value))
      return NULL;

  set_lp_rcl(lp_order, value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_form_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_form_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_all_couplings_active_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_all_couplings_active_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_all_tree_couplings_active_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_all_tree_couplings_active_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_all_loop_couplings_active_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_all_loop_couplings_active_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_all_ct_couplings_active_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_all_ct_couplings_active_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_all_r2_couplings_active_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_all_r2_couplings_active_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_complex_mass_form_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_complex_mass_form_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_vertex_functions_py(PyObject *self, PyObject *args)
{
  int value;
  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_vertex_functions_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_particle_ordering_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_particle_ordering_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_masscut_form_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_masscut_form_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_cutparticle_py(PyObject *self, PyObject *args)
{
  const char* particle_str;

  if (!PyArg_ParseTuple(args, "s", &particle_str))
      return NULL;
  set_cutparticle_rcl(particle_str);
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
unset_cutparticle_py(PyObject *self, PyObject *args)
{
  unset_cutparticle_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
static PyObject *
set_print_recola_logo_py(PyObject *self, PyObject *args)
{
  int value;

  if (!PyArg_ParseTuple(args, "i", &value))
      return NULL;

  set_print_recola_logo_rcl(value);
  Py_INCREF(Py_None);
  return Py_None;
}

/**********************************
*  module recola1_interface_rcl  *
**********************************/

PyDoc_STRVAR(set_pole_mass_w_doc,
"set_pole_mass_w_rcl(m,g)\n\n\
Sets the pole mass and width of the W boson.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_w_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_w_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_onshell_mass_w_doc,
"set_onshell_mass_w_rcl(m,g)\n\n\
Sets the on-shell mass and width of the W boson.\n\n\
:param m: on-shell mass \n\
:type  m: float\n\
:param g: on-shell width\n\
:type  g: float");
static PyObject *
set_onshell_mass_w_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_onshell_mass_w_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_z_doc,
"set_pole_mass_z_rcl(m,g)\n\n\
Sets the pole mass and width of the Z boson.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_z_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_z_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_onshell_mass_z_doc,
"set_onshell_mass_z_rcl(m,g)\n\n\
Sets the on-shell mass and width of the Z boson.\n\n\
:param m: on-shell mass \n\
:type  m: float\n\
:param g: on-shell width\n\
:type  g: float");
static PyObject *
set_onshell_mass_z_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_onshell_mass_z_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_h_doc,
"set_pole_mass_h_rcl(m,g)\n\n\
Sets the pole mass and width of the Higgs boson.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_h_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_h_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_top_doc,
"set_pole_mass_top_rcl(m,g)\n\n\
Sets the pole mass and width of the top-quark.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_top_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_top_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_bottom_doc,
"set_pole_mass_bottom_rcl(m,g)\n\n\
Sets the pole mass and width of the bottom-quark.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_bottom_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_bottom_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_charm_doc,
"set_pole_mass_charm_rcl(m,g)\n\n\
Sets the pole mass and width of the charm-quark.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_charm_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_charm_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_strange_doc,
"set_pole_mass_strange_rcl(m)\n\n\
Sets the pole mass of the strange-quark.\n\n\
:param m: pole mass \n\
:type  m: float");
static PyObject *
set_pole_mass_strange_py(PyObject *self, PyObject *args)
{

  double m;
  if (!PyArg_ParseTuple(args, "d", &m))
      return NULL;

  set_pole_mass_strange_rcl(m);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_up_doc,
"set_pole_mass_up_rcl(m)\n\n\
Sets the pole mass of the up-quark.\n\n\
:param m: pole mass \n\
:type  m: float");
static PyObject *
set_pole_mass_up_py(PyObject *self, PyObject *args)
{

  double m;
  if (!PyArg_ParseTuple(args, "d", &m))
      return NULL;

  set_pole_mass_up_rcl(m);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_down_doc,
"set_pole_mass_down_rcl(m)\n\n\
Sets the pole mass of the down-quark.\n\n\
:param m: pole mass \n\
:type  m: float");
static PyObject *
set_pole_mass_down_py(PyObject *self, PyObject *args)
{

  double m;
  if (!PyArg_ParseTuple(args, "d", &m))
      return NULL;

  set_pole_mass_down_rcl(m);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_tau_doc,
"set_pole_mass_tau_rcl(m,g)\n\n\
Sets the pole mass and width of the tau.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_tau_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_tau_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_muon_doc,
"set_pole_mass_muon_rcl(m,g)\n\n\
Sets the pole mass and width of the muon.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_muon_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_muon_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_electron_doc,
"set_pole_mass_electron_rcl(m)\n\n\
Sets the pole mass of the electron.\n\n\
:param m: pole mass \n\
:type  m: float");
static PyObject *
set_pole_mass_electron_py(PyObject *self, PyObject *args)
{

  double m;
  if (!PyArg_ParseTuple(args, "d", &m))
      return NULL;

  set_pole_mass_electron_rcl(m);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_top_doc,
"set_light_top_rcl\n\n\
Sets the top-quark as light, regularizing IR singularities with its mass.");
static PyObject *
set_light_top_py(PyObject *self, PyObject *args)
{
  set_light_top_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unset_light_top_doc,
"unset_light_top_rcl\n\n\
unsets the top-quark as light, keeping the full mass dependence.");
static PyObject *
unset_light_top_py(PyObject *self, PyObject *args)
{
  unset_light_top_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_bottom_doc,
"set_light_bottom_rcl\n\n\
Sets the bottom-quark as light, regularizing IR singularities with its mass.");
static PyObject *
set_light_bottom_py(PyObject *self, PyObject *args)
{
  set_light_bottom_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unset_light_bottom_doc,
"unset_light_bottom_rcl\n\n\
unsets the bottom-quark as light, keeping the full mass dependence.");
static PyObject *
unset_light_bottom_py(PyObject *self, PyObject *args)
{
  unset_light_bottom_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_charm_doc,
"set_light_charm_rcl\n\n\
Sets the charm-quark as light, regularizing IR singularities with its mass.");
static PyObject *
set_light_charm_py(PyObject *self, PyObject *args)
{
  set_light_charm_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unset_light_charm_doc,
"unset_light_charm_rcl\n\n\
unsets the charm-quark as light, keeping the full mass dependence.");
static PyObject *
unset_light_charm_py(PyObject *self, PyObject *args)
{
  unset_light_charm_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_strange_doc,
"set_light_strange_rcl\n\n\
Sets the strange-quark as light, regularizing IR singularities with its mass.");
static PyObject *
set_light_strange_py(PyObject *self, PyObject *args)
{
  set_light_strange_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unset_light_strange_doc,
"unset_light_strange_rcl\n\n\
unsets the strange-quark as light, keeping the full mass dependence.");
static PyObject *
unset_light_strange_py(PyObject *self, PyObject *args)
{
  unset_light_strange_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_up_doc,
"set_light_up_rcl\n\n\
Sets the up-quark as light, regularizing IR singularities with its mass.");
static PyObject *
set_light_up_py(PyObject *self, PyObject *args)
{
  set_light_up_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unset_light_up_doc,
"unset_light_up_rcl\n\n\
unsets the up-quark as light, keeping the full mass dependence.");
static PyObject *
unset_light_up_py(PyObject *self, PyObject *args)
{
  unset_light_up_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_down_doc,
"set_light_down_rcl\n\n\
Sets the down-quark as light, regularizing IR singularities with its mass.");
static PyObject *
set_light_down_py(PyObject *self, PyObject *args)
{
  set_light_down_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unset_light_down_doc,
"unset_light_down_rcl\n\n\
unsets the down-quark as light, keeping the full mass dependence.");
static PyObject *
unset_light_down_py(PyObject *self, PyObject *args)
{
  unset_light_down_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_tau_doc,
"set_light_tau_rcl\n\n\
Sets the tau as light, regularizing IR singularities with its mass.");
static PyObject *
set_light_tau_py(PyObject *self, PyObject *args)
{
  set_light_tau_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unset_light_tau_doc,
"unset_light_tau_rcl\n\n\
unsets the tau as light, keeping the full mass dependence.");
static PyObject *
unset_light_tau_py(PyObject *self, PyObject *args)
{
  unset_light_tau_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_muon_doc,
"set_light_muon_rcl\n\n\
Sets the muon as light, regularizing IR singularities with its mass.");
static PyObject *
set_light_muon_py(PyObject *self, PyObject *args)
{
  set_light_muon_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unset_light_muon_doc,
"unset_light_muon_rcl\n\n\
unsets the muon as light, keeping the full mass dependence.");
static PyObject *
unset_light_muon_py(PyObject *self, PyObject *args)
{
  unset_light_muon_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_electron_doc,
"set_light_electron_rcl\n\n\
Sets the electron as light, regularizing IR singularities with its mass.");
static PyObject *
set_light_electron_py(PyObject *self, PyObject *args)
{
  set_light_electron_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unset_light_electron_doc,
"unset_light_electron_rcl\n\n\
unsets the electron as light, keeping the full mass dependence.");
static PyObject *
unset_light_electron_py(PyObject *self, PyObject *args)
{
  unset_light_electron_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_light_fermions_doc,
"set_light_fermions_rcl(mcut)\n\n\
Sets a masscut `mcut` below which all massive fermions are treated\n\
as light particles, i.e. their collinear singularities are regularized with\n\
their mass and terms proportional to the mass are discarded).\n\
Massive fermions with a mass larger than `mcut` are heavy, and their\n\
full mass dependence is kept.\n\
mcut = float -> masscut value\n\n\
This function has no effect on massless\n\
fermions (their collinear singularities are regularized dimensionally).\n\
mcut = positiveflotating point number -> mass cut in GeV");
static PyObject *
set_light_fermions_py(PyObject *self, PyObject *args)
{

  double mcut;
  if (!PyArg_ParseTuple(args, "d", &mcut))
      return NULL;

  set_light_fermions_rcl(mcut);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_gfermi_scheme_doc,
"use_gfermi_scheme_rcl(g=None,a=None)\n\n\
Sets the EW renormalization scheme to the gfermi scheme.\n\n\
Args:\n\n\
  g (float): Sets the value of the Fermi constant to ``g``\n\
  a (float): Sets value of :m:`\\alpha` to ``a``\n\n\
Either ``g`` or ``a`` can be non-zero, but not both at the same time.\n\
If neither ``g`` nor ``a`` are set the hard-coded (default) value for\n\
:m:`\\alpha` is used.\n\n\
.. note::\n\n\
   In |recola2| the value for :m:`G_\\mathrm{F}` is translated to :m:`\\alpha` according to :m:`\\alpha = \\frac{\\sqrt{2}G_\\mathrm{F}}{\\pi} M_\\mathrm{W}^2\\left(1-\\frac{M_\\mathrm{W}^2}{M_\\mathrm{Z}^2}\\right)` with the currently set values for :m:`M_\\mathrm{Z}` and :m:`M_\\mathrm{W}`. This behaviour is different from |recola| where subsequently updated values (via calls to :py:meth:`set_pole_mass_z_rcl` and :py:meth:`set_pole_mass_w_rcl`) are used. For this reason, we recommend passing :m:`\\alpha` directly rather than :m:`G_\\mathrm{F}`.");
static PyObject *
use_gfermi_scheme_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  PyObject *a_py = Py_None;
  PyObject *g_py = Py_None;
  static char *kwlist[] = {"g", "a", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "|OO", kwlist, &g_py, &a_py))
      return NULL;

  if (g_py != Py_None && a_py == Py_None) {
    double g = PyFloat_AsDouble(g_py);
    use_gfermi_scheme_and_set_gfermi_rcl(g);
  } else if (g_py == Py_None && a_py != Py_None) {
    double a = PyFloat_AsDouble(a_py);
    use_gfermi_scheme_and_set_alpha_rcl(a);
  } else if (g_py == Py_None && a_py == Py_None){
    use_gfermi_scheme_rcl();
  } else{
    PyErr_SetString(PyExc_Exception, "Cannot pass g and a to use_gfermi_scheme_rcl!");
    return (PyObject *) NULL;
  }

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_alpha0_scheme_doc,
"use_alpha0_scheme_rcl(a=None)\n\n\
Sets the EW renormalization scheme to the :m:`\\alpha_0` scheme.\n\n\
:param a: Sets value of :m:`\\alpha` to `a`\n\
:type  a: float\n\n\
If ``a`` is not set the hard-coded (default) value for :m:`\\alpha` is used.");
static PyObject *
use_alpha0_scheme_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  PyObject *a_py = Py_None;
  static char *kwlist[] = {"a", NULL};

  if (!PyArg_ParseTupleAndKeywords(args, keywds, "|O", kwlist, &a_py))
      return NULL;

  if (a_py != Py_None) {
    if (!PyFloat_Check(a_py)) {
      // error, we are expecting an float
      PyErr_SetString(PyExc_Exception, "a is not of type float.");
      return (PyObject *) NULL;
    }
    double a = PyFloat_AsDouble(a_py);
    use_alpha0_scheme_and_set_alpha_rcl(a);
  } else {
    use_alpha0_scheme_rcl();
  }

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_alphaZ_scheme_doc,
"use_alphaZ_scheme_rcl(a=None)\n\n\
Sets the EW renormalization scheme to the :m:`\\alpha_\\mathrm{Z}` scheme.\n\n\
:param a: Sets value of :m:`\\alpha` to `a`\n\
:type  a: float\n\n\
If ``a`` is not set the hard-coded (default) value for :m:`\\alpha` is used.");
static PyObject *
use_alphaZ_scheme_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  PyObject *a_py = Py_None;
  static char *kwlist[] = {"a", NULL};

  if (!PyArg_ParseTupleAndKeywords(args, keywds, "|O", kwlist, &a_py))
      return NULL;

  if (a_py != Py_None) {
    if (!PyFloat_Check(a_py)) {
      // error, we are expecting an float
      PyErr_SetString(PyExc_Exception, "a is not of type float.");
      return (PyObject *) NULL;
    }
    double a = PyFloat_AsDouble(a_py);
    use_alphaz_scheme_and_set_alpha_rcl(a);
  } else {
    use_alphaz_scheme_rcl();
  }

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_alpha_doc,
"get_alpha_rcl()\n\n\
:return: Returns the value of :m:`\\alpha`\n\
:rtype: float");
static PyObject *
get_alpha_py(PyObject *self, PyObject *args)
{
  double value;
  get_alpha_rcl(&value);
  return Py_BuildValue("d", value);
}
PyDoc_STRVAR(set_alphas_doc,
"set_alphas_rcl(as,Q,Nf)\n\n\
Sets the values of :m:`\\alpha_\\mathrm{s}` to ``as``, the renormalization scale\n\
:m:`\\mu_\\mathrm{MS}` to ``Q`` and the number of active quark flavours\n\
to ``Nf``. The choosen value of ``as`` should correspond to the physical value\n\
of :m:`\\alpha_\\mathrm{s}` at the scale ``Q`` computed in the ``Nf``-flavour scheme.\n\
For Nf=-1 the quarks with mass less than ``Q`` are active flavours.\n\n\
:param s: value of :m:`\\alpha_\\mathrm{s}`\n\
:type  s: float\n\
:param Q: renormalization scale\n\
:type  Q: float\n\
:param Nf: number of light quarks\n\
:type  Nf: int");
static PyObject *
set_alphas_py(PyObject *self, PyObject *args)
{

  double aS, scaleQCD;
  int nflavour;

  if (!PyArg_ParseTuple(args, "ddi", &aS, &scaleQCD, &nflavour))
      return NULL;

  set_alphas_rcl(&aS, &scaleQCD, &nflavour);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_alphas_doc,
"get_alphas_rcl()\n\n\
:return: Returns the value of :m:`\\alpha_\\mathrm{s}`\n\
:rtype: float");
static PyObject *
get_alphas_py(PyObject *self, PyObject *args)
{
  double value;
  get_alphas_rcl(&value);
  return Py_BuildValue("d", value);
}
PyDoc_STRVAR(set_alphas_masses_doc,
"set_alphas_masses_rcl(mc,mb,mt,gc=0.,gb=0.,gt=0.)\n\n\
Overwrites the values of the quark masses for the running of\n\
:m:`\\alpha_\\mathrm{s}` used in the rest of the computations.\n\
The squared masses of the 3 heaviest quarks are set to the complex mass:\n\n\
* `mc^2-I*mc*gc`\n\
* `mb^2-I*mb*gb`\n\
* `mt^2-I*mt*gt`\n\n\
and must satisfy :code:`0 <= mc <= mb <= mt`.\n\n\
:param mc: charm mass\n\
:type  mc: float\n\
:param mb: bottom mass\n\
:type  mb: float\n\
:param mt: top mass\n\
:type  mt: float\n\
:param gc: charm width\n\
:type  gc: float, optional\n\
:param gb: bottom width\n\
:type  gb: float, optional\n\
:param gt: top width\n\
:type  gt: float, optional");
static PyObject *
set_alphas_masses_py(PyObject *self, PyObject *args, PyObject *keywds)
{

  double mc, mb, mt, gc, gb, gt;
  gc = 0.;
  gb = 0.;
  gt = 0.;

  static char *kwlist[] = {"mc", "mb", "mt", "gc", "gb", "gt", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "ddd|ddd", kwlist,
                                   &mc, &mb, &mt, &gc, &gb, &gt))
      return NULL;

  set_alphas_masses_rcl(mc, mb, mt, gc, gb, gt);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(compute_running_alphas_doc,
"compute_running_alphas_rcl(Q,Nf,lp)\n\n\
Computes the value for :m:`\\alpha_\\mathrm{s}` at the scale `Q` employing the \n\
renormalization-group evolution at ``lp`` loops\n\
(``lp`` can take the value ``lp=1`` or ``lp=2``).\n\
The integer argument ``Nf`` selects the flavour scheme.\n\n\
:param Q:  renormalization scale\n\
:type  Q: float\n\
:param Nf: For Nf=-1 the variable flavour scheme is selected, where the number of\n\
           active flavours contributing to the running of :m:`\\alpha_\\mathrm{s}` is set\n\
           to the number of quarks lighter than the scale `Q`.\n\
           For Nf=3-6 the fixed Nf-flavour scheme is selected, where the number of\n\
           active flavours contributing to the running of :m:`\\alpha_\\mathrm{s}` is set\n\
           to ``Nf``. In this case ``Nf`` cannot be smaller than the number of\n\
           massless quarks (otherwise the code stops).\n\
:type  Nf: int\n\
:param lp: loop-order, accepted values: 1,2\n\
:type  lp: int");
static PyObject *
compute_running_alphas_py(PyObject *self, PyObject *args)
{
  double Q;
  int Nf,lp;

  if (!PyArg_ParseTuple(args, "dii", &Q, &Nf, &lp))
      return NULL;

  compute_running_alphas_rcl(Q, Nf, lp);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(select_all_gs_powers_BornAmpl_doc,
"select_all_gs_powers_BornAmpl_rcl(npr)\n\n\
Selects all the contribution to the Born amplitude for the process\n\
with process number `npr`.\n\n\
:param npr: process number\n\
:type npr: int");
static PyObject *
select_all_gs_powers_BornAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;

  if (!PyArg_ParseTuple(args, "i", &process_id))
    return NULL;

  select_all_gs_powers_BornAmpl_rcl(process_id);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unselect_all_gs_powers_BornAmpl_doc,
"unselect_all_gs_powers_BornAmpl_rcl(npr)\n\n\
Unselects all the contribution to the Born amplitude for the process\n\
with process number `npr`.\n\n\
:param npr: process number\n\
:type npr: int");
static PyObject *
unselect_all_gs_powers_BornAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;

  if (!PyArg_ParseTuple(args, "i", &process_id))
    return NULL;

  unselect_all_gs_powers_BornAmpl_rcl(process_id);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(select_gs_power_BornAmpl_doc,
"select_gs_power_BornAmpl_rcl(npr,gspower)\n\n\
Selects the contribution to the Born amplitude with :m:`g_\\mathrm{s}` power\n\
``gspower`` for the process with process number ``npr``.\n\n\
:param npr: process number\n\
:type  npr: int\n\
:param gspower: power in gs\n\
:type  gspower: int");
static PyObject *
select_gs_power_BornAmpl_py(PyObject *self, PyObject *args)
{
  int process_id, gspower;

  if (!PyArg_ParseTuple(args, "ii", &process_id, &gspower))
    return NULL;

  select_gs_power_BornAmpl_rcl(process_id, gspower);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unselect_gs_power_BornAmpl_doc,
"unselect_gs_power_BornAmpl_rcl(npr,gspower)\n\n\
Unselects the contribution to the Born amplitude with :m:`g_\\mathrm{s}` power\n\
``gspower`` for the process with process number ``npr``.\n\n\
:param npr: process number\n\
:type  npr: int\n\
:param gspower: power in gs\n\
:type  gspower: int");
static PyObject *
unselect_gs_power_BornAmpl_py(PyObject *self, PyObject *args)
{
  int process_id, gspower;

  if (!PyArg_ParseTuple(args, "ii", &process_id, &gspower))
    return NULL;

  unselect_gs_power_BornAmpl_rcl(process_id, gspower);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(select_all_gs_powers_LoopAmpl_doc,
"select_all_gs_powers_LoopAmpl_rcl(npr)\n\n\
Selects all the contribution to the Born amplitude for the process\n\
with process number ``npr``.\n\n\
:param npr: process number\n\
:type  npr: int");
static PyObject *
select_all_gs_powers_LoopAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;

  if (!PyArg_ParseTuple(args, "i", &process_id))
    return NULL;

  select_all_gs_powers_LoopAmpl_rcl(process_id);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unselect_all_gs_powers_LoopAmpl_doc,
"unselect_all_gs_powers_LoopAmpl_rcl(npr)\n\n\
Unselects all the contribution to the Born amplitude for the process\n\
with process number ``npr``.\n\
:param npr: process number\n\
:type  npr: int");
static PyObject *
unselect_all_gs_powers_LoopAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;

  if (!PyArg_ParseTuple(args, "i", &process_id))
    return NULL;

  unselect_all_gs_powers_LoopAmpl_rcl(process_id);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(select_gs_power_LoopAmpl_doc,
"select_gs_power_LoopAmpl_rcl(npr, gspower)\n\n\
Selects the contribution to the Loop amplitude with :m:`g_\\mathrm{s}` power\n\
``gspower`` for the process with process number ``npr``.\n\n\
:param npr: process number\n\
:type  npr: int\n\
:param gspower: power in gs\n\
:type  gspower: int");
static PyObject *
select_gs_power_LoopAmpl_py(PyObject *self, PyObject *args)
{
  int process_id, gspower;

  if (!PyArg_ParseTuple(args, "ii", &process_id, &gspower))
    return NULL;

  select_gs_power_LoopAmpl_rcl(process_id, gspower);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unselect_gs_power_LoopAmpl_doc,
"unselect_gs_power_LoopAmpl_rcl(npr,gspower)\n\n\
Unselects the contribution to the Loop amplitude with :m:`g_\\mathrm{s}` power\n\
``gspower`` for the process with process number ``npr``.\n\n\
:param npr: process number\n\
:type  npr: int\n\
:param gspower: power in gs\n\
:type  gspower: int");
static PyObject *
unselect_gs_power_LoopAmpl_py(PyObject *self, PyObject *args)
{
  int process_id, gspower;

  if (!PyArg_ParseTuple(args, "ii", &process_id, &gspower))
    return NULL;

  unselect_gs_power_LoopAmpl_rcl(process_id, gspower);
  Py_INCREF(Py_None);
  return Py_None;
}

/*******************************
*  module process definition  *
*******************************/

PyDoc_STRVAR(define_process_doc,
"define_process_rcl(npr,process,order)\n\n\
Defines a process via the process string ``process`` at order ``order`` and\n\
assigns the process id ``npr`` to it.\n\n\
Args:\n\
  npr (int): unique process number\n\
  process (str): Process string composed of particles and\n\
                separated by :math:`\\to`\n\
  order (str): loop-order, accepted values ’LO’ and ’NLO’\n\n\
Issuing repeated calls of define_process_rcl allows to define more than one\n\
process. Each process must be given a unique identifier npr.\n\
The process string must consist of a list of particles separated by :math:`\\to`,\n\
with the incoming (outgoing) particles on the left-hand (right-hand) side\n\
of :math:`\\to`. Any number of incoming and outgoing particles is allowed.\n\
The allowed particles are listed :ref:`here<fieldconventions>`.\n\n\
Each particle must be separated by at least one blank character. \n\n\
Examples:\n\
  >>> define_process_rcl(1, 'e+ e- -> mu+ mu-', 'LO')\n\
  >>> define_process_rcl(20, 'H -> A A', 'NLO')\n\
  >>> define_process_rcl(3, 'u d~ -> W+ g  g g', 'NLO')\n\
  >>> define_process_rcl(42, 'u  g  -> u   g  tau-    tau+', 'NLO')\n\n\
Polarizations can be specified to each fermion or vector particle by\n\
adding the corresponding symbol :code:`'[-]'`, :code:`'[+]'` or :code:`'[0]'`.\n\
Blank characters can (but need not) separate the helicity symbol from the particle name:\n\n\
Example:\n\
  :m:`\\mathrm{e}^+` is right-handed, :m:`\\mathrm{e}^-` is left-handed, Z is unpolarized.\n\n\
  >>> define_process_rcl(1, 'e+[+] e- [-] -> Z H', 'NLO')\n\n\
Example:\n\
  u and :m:`\\bar{\\mathrm{u}}` are unpolarized, :m:`\\mathrm{W}^+` and :m:`\\mathrm{W}^-` are transverse.\n\n\
  >>> define_process_rcl(1, 'u u~ -> W+[-] W-[+]', 'NLO')\n\n\
Contributions with specific intermediate states can be selected\n\
where intermediate states are particles decaying into any number of other\n\
particles. To this end, in the process declaration the decaying particle must\n\
be followed by round brackets ’( ... )’ containing the decay products.\n\
Multiple and nested decays are allowed. Blank characters can (but need not)\n\
separate the brackets ’(’, ’)’ from the particle names:\n\n\
Examples:\n\
  >>> define_process_rcl(1, 'e+ e- -> W+ W-(e- nu_e~)', 'NLO')\n\
  >>> define_process_rcl(1, 'e+ e- -> Z H ( b~[+] b[-] )', 'NLO')\n\
  >>> define_process_rcl(1, 'e+ e- -> t( W+(u d~) b) t~(e- nu_e~ b~)', 'NLO')\n\
  >>> define_process_rcl(1, 'e+ e- -> Z ( mu+(e+ nu_e nu_mu~) mu-(e- nu_e~ nu_mu) ) H', 'NLO')\n\n\
The selection of specific intermediate particles is a prerequisite for the\n\
calculation of amplitudes within the pole approximation.");
static PyObject *
define_process_py(PyObject *self, PyObject *args)
{
  int process_id;
  const char* process;
  const char* order;

  if (!PyArg_ParseTuple(args, "iss", &process_id, &process, &order))
      return NULL;

  define_process_rcl(process_id, process, order);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(select_power_BornAmpl_doc,
"select_power_BornAmpl_rcl(npr,cid,pow)\n\n\
Selects for the process with process number ``npr`` the contribution to\n\
the Born amplitude with power ``pow`` in the fundamental order ``cid``.\n\n\
:param npr: process number\n\
:type npr: int\n\
:param cid: fundamental coupling id\n\
:type cid: str\n\
:param pow: power in fundamental coupling\n\
:type pow: int\n\n\
E.g. the selection of the Born contribution :m:`e^4 g_\\mathrm{s}^2`:\n\n\
.. code-block:: python\n\n\
  select_power_BornAmpl_rcl(npr, 'QCD', 2)\n\
  select_power_BornAmpl_rcl(npr, 'QED', 4)");
static PyObject *
select_power_BornAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;
  const char* order;
  int power;

  if (!PyArg_ParseTuple(args, "isi", &process_id, &order, &power))
    return NULL;

  select_power_bornampl_rcl(process_id, order, power);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unselect_power_BornAmpl_doc,
"unselect_power_BornAmpl_rcl(npr,cid,pow)\n\
Selects the contributions to the Born amplitude with power `power` of \n\
the fundanmetal order `cid` for the process with identifier `npr`.\n\n\
:param npr: process number\n\
:type npr: int\n\
:param cid: fundamental coupling id\n\
:type cid: str\n\
:param pow: power in fundamental coupling\n\
:type pow: int");
static PyObject *
unselect_power_BornAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;
  const char* order;
  int power;

  if (!PyArg_ParseTuple(args, "isi", &process_id, &order, &power))
    return NULL;

  unselect_power_bornampl_rcl(process_id, order, power);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(select_all_powers_BornAmpl_doc,
"select_all_powers_BornAmpl_rcl(npr)\n\
Selects all contributions to the Born amplitude with any power of orders.\n\n\
:param npr: process number\n\
:type  npr: int");
static PyObject *
select_all_powers_BornAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;

  if (!PyArg_ParseTuple(args, "i", &process_id))
    return NULL;

  select_all_powers_bornampl_rcl(process_id);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unselect_all_powers_BornAmpl_doc,
"unselect_all_powers_BornAmpl_rcl(npr)\n\
Unselects all contributions to the Born amplitude with any power of orders.\n\n\
:param npr: process number\n\
:type  npr: int");
static PyObject *
unselect_all_powers_BornAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;

  if (!PyArg_ParseTuple(args, "i", &process_id))
    return NULL;

  unselect_all_powers_bornampl_rcl(process_id);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(select_power_LoopAmpl_doc,
"select_power_LoopAmpl_rcl(npr,cid,pow)\n\n\
Selects for the process with process number `npr` the contribution to\n\
the Loop amplitude with power `power` in the fundamental order `cid`.\n\n\
:param npr: process number\n\
:type npr: int\n\
:param cid: fundamental coupling id\n\
:type cid: str\n\
:param pow: power in fundamental coupling\n\
:type pow: int\n\n\
E.g. the selection of the Born contribution :m:`e^4 g_\\mathrm{s}^2`:\n\n\
.. code-block:: python\n\n\
  select_power_LoopAmpl_rcl(npr, 'QCD', 2)\n\
  select_power_LoopAmpl_rcl(npr, 'QED', 4)");
static PyObject *
select_power_LoopAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;
  const char* order;
  int power;

  if (!PyArg_ParseTuple(args, "isi", &process_id, &order, &power))
    return NULL;

  select_power_loopampl_rcl(process_id, order, power);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unselect_power_LoopAmpl_doc,
"unselect_power_LoopAmpl_rcl()\n\
Selects all contributions to the Loop amplitude with any power of orders.");
static PyObject *
unselect_power_LoopAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;
  const char* order;
  int power;

  if (!PyArg_ParseTuple(args, "isi", &process_id, &order, &power))
    return NULL;

  unselect_power_loopampl_rcl(process_id, order, power);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(select_all_powers_LoopAmpl_doc,
"select_all_powers_LoopAmpl_rcl(npr)\n\
Selects all contributions to the Loop amplitude with any power of orders.\n\n\
:param npr: process number\n\
:type npr: int");
static PyObject *
select_all_powers_LoopAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;

  if (!PyArg_ParseTuple(args, "i", &process_id))
    return NULL;

  select_all_powers_loopampl_rcl(process_id);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(unselect_all_powers_LoopAmpl_doc,
"unselect_all_powers_LoopAmpl_rcl(npr)\n\n\
Unselects all contributions to the Loop amplitude with any power of orders.\n\n\
:param npr: process number\n\
:type npr: int");
static PyObject *
unselect_all_powers_LoopAmpl_py(PyObject *self, PyObject *args)
{
  int process_id;

  if (!PyArg_ParseTuple(args, "i", &process_id))
    return NULL;

  unselect_all_powers_loopampl_rcl(process_id);
  Py_INCREF(Py_None);
  return Py_None;
}

PyDoc_STRVAR(split_collier_cache_doc,
"set_collier_cache_rcl(npr, n)\n\n\
Splits the cache of collier for the process ``npr`` in ``n`` parts.\n\n\
:param npr: process number\n\
:type npr: int\n\
:param n: number of caches (0 disables the cache)\n\
:type  n: int");
static PyObject *
split_collier_cache_py(PyObject *self, PyObject *args)
{
  int process_id,n;

  if (!PyArg_ParseTuple(args, "ii", &process_id, &n))
    return NULL;

  split_collier_cache_rcl(process_id, n);
  Py_INCREF(Py_None);
  return Py_None;
}

/***********************************
*  module process_generation_rcl  *
***********************************/

PyDoc_STRVAR(generate_processes_doc,
"generate_processes_rcl()\n\n\
Generates processes which were defined using :py:meth:`define_process_rcl`.\n\n\
Example:\n\
  >>> define_process_rcl(1, 'u u~ -> d d~ g g', 'NLO')\n\
  >>> generate_processes_rcl()");
static PyObject *
generate_processes_py(PyObject *self, PyObject *args)
{
  generate_processes_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(process_exists_doc,
"process_exists_rcl(npr)\n\n\
Returns whether a process with id ``npr`` exists after generation\n\
:py:meth:`generate_processes_rcl`.\n\n\
Args:\n\n\
  npr (int): process number\n\
Returns:\n\
  (bool): True if process exists, else False.\n\n\
Example:\n\
  >>> define_process_rcl(1, 'u u~ -> d d~ g g', 'LO')\n\
  >>> generate_processes_rcl()\n\
  >>> process_exists_rcl(1)\n\
  True");
static PyObject *
process_exists_py(PyObject *self, PyObject *args)
{

  int npr,exists;
  if (!PyArg_ParseTuple(args, "i", &npr))
      return NULL;

  process_exists_rcl(npr, &exists);
  return Py_BuildValue("O", exists ? Py_True: Py_False);
}

/************************************
*  module process_computation_rcl  *
************************************/

PyDoc_STRVAR(set_resonant_squared_momentum_doc,
"set_resonant_squared_momentum_rcl(npr,res,ps)\n\n\
Sets for the resonance with resonance number ``res`` for the process\n\
with process number ``npr`` the squared momentum of the denominator\n\
of the resonant propagator to ``ps``.\n\n\
:param npr: process number\n\
:type  npr: int\n\
:param res: resonance numer, the first resonant particle defined\n\
            has ``res=1``, the second resonant particle has ``res=2``,\n\
            and so on.\n\
:type  res: int\n\
:param ps: squared momentum of the resonant propagator\n\
:type  ps: float");
static PyObject *
set_resonant_squared_momentum_py(PyObject *self, PyObject *args)
{

  int npr,res;
  double ps;

  if (!PyArg_ParseTuple(args, "iid", &npr, &res, &ps))
      return NULL;

  set_resonant_squared_momentum_rcl(npr, res, ps);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(rescale_process_doc,
"rescale_process_rcl(npr,order)\n\n\
Adjusts the results calculated by :py:meth:`compute_process_rcl` for a (new) value\n\
of :m:`\\alpha_\\mathrm{s}`, rescaling the structure-dressed helicity amplitudes and\n\
recomputing the summed squared amplitude for the process with process\n\
number ``npr``.\n\n\
Args:\n\n\
  npr (int): process number\n\
  order (str): either ``'LO'`` or ``'NLO'``\n\
Returns:\n\
  (float, float): (:m:`|A_0|^2`, :m:`2 \\mathrm{Re}[A_0 A_1]`)");
static PyObject *
rescale_process_py(PyObject *self, PyObject *args)
{

  int npr;
  const char* order;

  if (!PyArg_ParseTuple(args, "is", &npr, &order))
      return NULL;

  double A2[2];
  rescale_process_rcl(npr, order, A2);

  return Py_BuildValue("dd", A2[0], A2[1]);
}
PyDoc_STRVAR(rescale_colour_correlation_doc,
"rescale_colour_correlation_rcl(npr,i1,i2,order='LO')\n\n\
Adjusts the results for a\n\
(new) value of :m:`\\alpha_\\mathrm{s}`, rescaling the structure-dressed helicity amplitudes\n\
and recomputing the colour-correlated summed squared amplitude for the\n\
process with process number ``npr``.\n\n\
Args:\n\n\
  npr (int): process number\n\
  i1 (int): quark/anti-quark at leg i1\n\
  i2 (int): quark/anti-quark at leg i2\n\
  order (str): either 'LO' (default) or 'NLO'\n\
Returns:\n\
  (float): :m:`A^2_{cc}`");
static PyObject *
rescale_colour_correlation_py(PyObject *self, PyObject *args, PyObject *keywds)
{

  int npr,i1,i2;
  PyObject *order_py;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "i1", "i2", "order", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "iii|O", kwlist,
                                   &npr, &i1, &i2,
                                   &order_py))
      return NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  double A2cc;
  rescale_colour_correlation_rcl(npr, i1, i2, order, &A2cc);

  return Py_BuildValue("d", A2cc);
}
PyDoc_STRVAR(rescale_all_colour_correlations_doc,
"rescale_all_colour_correlations_rcl(npr)\n\n\
Adjusts the results for\n\
a (new) value of :m:`\\alpha_\\mathrm{s}`, rescaling the structure-dressed helicity amplitudes\n\
and recomputing the colour-correlated summed squared amplitude for the\n\
process with process number `npr`.\n\n\
Args:\n\n\
  npr (int): process number");
static PyObject *
rescale_all_colour_correlations_py(PyObject *self, PyObject *args)
{

  int npr;

  if (!PyArg_ParseTuple(args, "i", &npr))
      return NULL;

  double A2cc;
  rescale_all_colour_correlations_rcl(npr);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(rescale_spin_colour_correlation_doc,
"rescale_spin_colour_correlation_rcl(npr,i1,i2,v,order='LO')\n\n\
Adjusts the results for\n\
a (new) value of :m:`\\alpha_\\mathrm{s}`, rescaling the structure-dressed helicity amplitudes\n\
and recomputing the spin-colour-correlated summed squared amplitude for the\n\
process with process number ``npr``.\n\n\
Args:\n\n\
  npr (int): process number\n\
  i1  (int): gluon at leg i1\n\
  i2  (int): quark/anti-quark at leg i2\n\
  v   (complex[4]): polarization vector\n\
  order (str): either 'LO' (default) or 'NLO'\n\
Returns:\n\
  (float): :m:`A^2_{scc}`");
static PyObject *
rescale_spin_colour_correlation_py(PyObject *self, PyObject *args, PyObject *keywds)
{

  int npr,i1,i2;
  PyObject *polvec_py,*order_py;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "i1", "i2", "v", "order", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "iiiO!|O", kwlist,
                                   &npr, &i1, &i2, &PyList_Type, &polvec_py,
                                   &order_py))
      return NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  dcomplex v[4];
  if (!build_c_polvec(polvec_py, v))
  {
    return (PyObject *) NULL;
  }

  double A2scc;
  rescale_spin_colour_correlation_rcl(npr, i1, i2, v, order, &A2scc);

  return Py_BuildValue("d", A2scc);
}
PyDoc_STRVAR(rescale_spin_correlation_doc,
"rescale_spin_correlation_rcl(npr,j,v,order='LO')\n\n\
Adjusts the results for a\n\
(new) value of :m:`\\alpha_\\mathrm{s}`, rescaling the structure-dressed helicity amplitudes\n\
and recomputing the spin-correlated summed squared amplitude for the\n\
process with process number ``npr``.\n\n\
Args:\n\
  npr (int): process number\n\
  j   (int): photon/gluon leg\n\
  v   (complex[4]): photon/gluon polarization vector\n\
  order (str): either 'LO' (default) or 'NLO'\n\
Returns:\n\
  (float): :m:`A^2_{sc}`");
static PyObject *
rescale_spin_correlation_py(PyObject *self, PyObject *args, PyObject *keywds)
{

  int npr,j;
  PyObject *polvec_py, *order_py;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "j", "v", "order", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "iiO!|O", kwlist,
                                   &npr, &j, &PyList_Type, &polvec_py,
                                   &order_py))
      return NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  dcomplex v[4];
  if (!build_c_polvec(polvec_py, v))
  {
    return (PyObject *) NULL;
  }

  double A2scc;
  rescale_spin_correlation_rcl(npr, j, v, order, &A2scc);

  return Py_BuildValue("d", A2scc);
}
PyDoc_STRVAR(rescale_spin_correlation_matrix_doc,
"rescale_spin_correlation_matrix_rcl(npr,order='LO')\n\n\
Adjusts the calculated results for\n\
a (new) value of :m:`\\alpha_\\mathrm{s}`, rescaling the structure-dressed helicity amplitudes\n\
and recomputes the spin-colour-correlated summed squared amplitude for the\n\
process with process number ``npr``.\n\n\
Args:\n\n\
  npr (int): process number\n\
  order (str): either 'LO' (default) or 'NLO'\n\
Returns:\n\
  (float): :m:`A^2_{scm}`");
static PyObject *
rescale_spin_correlation_matrix_py(PyObject *self, PyObject *args, PyObject *keywds)
{

  int npr,j;
  PyObject *polvec_py,*order_py;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "j", "order", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "ii|O", kwlist,
                                   &npr, &j,
                                   &order_py))
      return NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  double A2scm[4][4];
  rescale_spin_correlation_matrix_rcl(npr, j, order, A2scm);

  PyObject *A2scm_py = PyList_New(4);
  int i;
  for (i = 0; i < 4; i++) {
      PyObject *item = PyList_New(4);
      int j;
      for (j = 0; j < 4; j++)
          PyList_SET_ITEM(item, j, PyFloat_FromDouble(A2scm[i][j]));
      PyList_SET_ITEM(A2scm_py, i, item);
  }

  return Py_BuildValue("O", A2scm_py);
}
PyDoc_STRVAR(set_tis_required_accuracy_doc,
"set_tis_required_accuracy_rcl(acc)\n\n\
Sets the required accuracy for TIs to the value `acc` in collier.\n\
acc = floating point number -> accarcy in 10^-DIGITS, e.g. 0.00001.");
static PyObject *
set_tis_required_accuracy_py(PyObject *self, PyObject *args)
{
  double acc;

  if (!PyArg_ParseTuple(args, "d", &acc))
    return NULL;

  set_tis_required_accuracy_rcl(acc);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_tis_required_accuracy_doc,
"get_tis_required_accuracy_rcl()\n\n\
Gets the required accuracy set for TIs\n\
return = floating point number.");
static PyObject *
get_tis_required_accuracy_py(PyObject *self, PyObject *args)
{

  double acc;
  get_tis_required_accuracy_rcl(&acc);

  return Py_BuildValue("d", acc);
}
PyDoc_STRVAR(set_tis_critical_accuracy_doc,
"set_tis_critical_accuracy_rcl(acc)\n\n\
Sets the critical accuracy for TIs to the value `acc` in collier.\n\
acc = floating point number -> accarcy in 10^-DIGITS, e.g. 0.00001.");
static PyObject *
set_tis_critical_accuracy_py(PyObject *self, PyObject *args)
{
  double acc;

  if (!PyArg_ParseTuple(args, "d", &acc))
    return NULL;

  set_tis_critical_accuracy_rcl(acc);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(get_tis_critical_accuracy_doc,
"get_tis_critical_accuracy_rcl()\n\n\
Gets the critical accuracy set for TIs\n\
return = floating point number.");
static PyObject *
get_tis_critical_accuracy_py(PyObject *self, PyObject *args)
{

  double acc;
  get_tis_critical_accuracy_rcl(&acc);

  return Py_BuildValue("d", acc);
}
PyDoc_STRVAR(get_tis_accuracy_flag_doc,
"get_tis_accuracy_flag_rcl()\n\n\
Gets the value of the accuracy flag for TIs from collier. The return\n\
value contains global information on the accuracy of the TIs evaluated in\n\
the last call of compute_process_rcl:\n\
return =  0 -> For all TIs, the accuracy is estimated to be better than the\n\
               required value.\n\
return = -1 -> For at least one TI, the accuracy is estimated to be worse\n\
               than the required value, but for all TIs, the accuracy is\n\
               estimated to be better than the critical value.\n\
return = -2 -> For at least one TI, the accuracy is estimated to be worse\n\
               than the critical values. The value of variable `flag` is\n\
               determined based on internal uncertainty estimations\n\
               performed by collier.");
static PyObject *
get_tis_accuracy_flag_py(PyObject *self, PyObject *args)
{

  int acc;
  get_tis_accuracy_flag_rcl(&acc);

  return Py_BuildValue("i", acc);
}
PyDoc_STRVAR(get_n_colour_configurations_doc,
"get_n_colour_configurations_rcl(npr)\n\n\
Returns the number of exisiting colour configurations\n\
for the process with process number `npr`.");
static PyObject *
get_n_colour_configurations_py(PyObject *self, PyObject *args)
{

  int npr,n;
  if (!PyArg_ParseTuple(args, "i", &npr))
    return NULL;

  get_n_colour_configurations_rcl(npr,&n);

  return Py_BuildValue("i", n);

}
PyDoc_STRVAR(get_colour_configuration_doc,
"get_colour_configuration_rcl(npr,n)\n\n\
Returns the colour configuration array associated to the colour \n\
configuration n of the process with process number `npr`.\n\
npr = integer -> process numer\n\
n   = integer -> n-th colour configuration");
static PyObject *
get_colour_configuration_py(PyObject *self, PyObject *args)
{

  int npr,n;
  if (!PyArg_ParseTuple(args, "ii", &npr, &n))
    return NULL;

  int legs;
  get_legs_rcl(npr,&legs,"get_colour_configuration_rcl");

  int col[legs];

  get_colour_configuration_rcl(npr,n,col);

  PyObject *col_py = PyList_New(legs);
  int i;
  for (i = 0; i < legs; i++) {
      PyList_SET_ITEM(col_py, i, int_to_pyobj(col[i]));
  }
  return col_py;
}
PyDoc_STRVAR(get_n_helicity_configurations_doc,
"get_n_helicity_configurations_rcl(npr)\n\n\
Returns the number of exisiting helicity configurations\n\
for the process with process number `npr`.");
static PyObject *
get_n_helicity_configurations_py(PyObject *self, PyObject *args)
{

  int npr,n;
  if (!PyArg_ParseTuple(args, "i", &npr))
    return NULL;

  get_n_helicity_configurations_rcl(npr,&n);

  return Py_BuildValue("i", n);

}
PyDoc_STRVAR(get_helicity_configuration_doc,
"get_helicity_configuration_rcl(npr,n)\n\n\
Returns the helicity configuration array associated to the helicity \n\
configuration n of the process with process number `npr`.\n\
npr = integer -> process numer\n\
n   = integer -> n-th helicity configuration");
static PyObject *
get_helicity_configuration_py(PyObject *self, PyObject *args)
{

  int npr,n;
  if (!PyArg_ParseTuple(args, "ii", &npr, &n))
    return NULL;

  int legs;
  get_legs_rcl(npr, &legs,"get_helicity_configuration_rcl");

  int hel[legs];

  get_helicity_configuration_rcl(npr,n,hel);

  PyObject *hel_py = PyList_New(legs);
  int i;
  for (i = 0; i < legs; i++) {
      PyList_SET_ITEM(hel_py, i, int_to_pyobj(hel[i]));
  }
  return hel_py;
}


/************************
*  module wrapper_rcl  *
************************/

// set_alphas_masses_rcl -> recola1_interface_rcl
// use_gfermi_scheme_rcl -> recola1_interface_rcl
// use_alpha0_scheme_rcl -> recola1_interface_rcl
// use_alphaz_scheme_rcl -> recola1_interface_rcl

PyDoc_STRVAR(set_gs_power_doc,
"set_gs_power_rcl(npr, gsarray)\n\n\
Selects specific powers of g_s, as specified by the argument `gsarray`,\n\
for the process with identifier `npr`.\n\
npr     = integer -> process number\n\
gsarray = integer[:,2]\n\
        -> Integer array values can be 0 or 1,\n\
           with the first entry being the power of g_s,\n\
           and the second entry being the loop order\n\
           (0 for LO, 1 for NLO)");
static PyObject *
set_gs_power_py(PyObject *self, PyObject *args)
{

  int process_id;
  PyObject *gsarray_py;

  if (!PyArg_ParseTuple(args, "iO!", &process_id,
                        &PyList_Type, &gsarray_py))
      return NULL;

  const int numLines = PyList_Size(gsarray_py);
  int gsarray[numLines][2];

  PyObject *gs_py;

  int i;
  for (i=0; i<numLines; i++)
  {
    gs_py = PyList_GetItem(gsarray_py, i);
    PyObject *iter = PyObject_GetIter(gs_py);
    if (!iter)
    {
      // error not iterator
      return PyErr_Format(PyExc_Exception,
      "gsarray at position %d is not an iterator/list.", i);
    }
    int j = 0;
    while (1) {
      PyObject *next = PyIter_Next(iter);
      if (!next) {
        // nothing left in the iterator
        break;
      }
      if (!pyobj_pyint_check(next)) {
        // error, we were expecting a floating point value
      }
      gsarray[i][j] = pyobj_to_int(next);
      Py_DECREF(next);
      j = j + 1;
    }
    Py_DECREF(iter);
  }

  set_gs_power_rcl(process_id, gsarray, numLines);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_outgoing_momenta_doc,
"set_outgoing_momenta_rcl(npr,pIn)\n\n\
Random phase space generation given incoming momenta.\n\n\
Args:\n\n\
  npr (int): process number\n\
  pIn (float[2][4]): Incoming (beam) momenta\n\n\
Returns:\n\
  (float[l][4]): Random external momenta of the process with `l` legs");
static PyObject *
set_outgoing_momenta_py(PyObject *self, PyObject *args)
{
  int process_id;
  PyObject *momentaIn_py;

  if (!PyArg_ParseTuple(args, "iO!", &process_id, &PyList_Type, &momentaIn_py))
      return NULL;

  int legs;
  get_legs_rcl(process_id, &legs, "set_outgoing_momenta_rcl");

  int numLines;
  numLines = PyList_Size(momentaIn_py);
  double momentaIn[numLines][4];
  if(numLines != 2){
    return PyErr_Format(PyExc_Exception,
        "Incoming momenta pIn not of length 2. Momenta length given: %d",
        numLines);
  }


  PyObject *momentum_py;

  int i;
  for (i=0; i<numLines; i++)
  {
    momentum_py = PyList_GetItem(momentaIn_py, i);
    PyObject *iter = PyObject_GetIter(momentum_py);
    if (!iter)
    {
      // error, expected list/iterator
      return PyErr_Format(PyExc_Exception,
      "Given momenta %d is not of list/iterator type.", i);
    }
    int j = 0;
    while (1) {
      PyObject *next = PyIter_Next(iter);
      if (!next) {
        // nothing left in the iterator
        break;
      }
      if (!PyFloat_Check(next)) {
        // error, we were expecting a floating point value
      }
      momentaIn[i][j] = PyFloat_AsDouble(next);
      Py_DECREF(next);
      j = j + 1;
    }
    Py_DECREF(iter);
  }

  double momenta[legs][4];
  set_outgoing_momenta_rcl(process_id, momentaIn, momenta);

  PyObject *result = PyList_New(legs);
  for (i = 0; i < legs; i++) {
      PyObject *item = PyList_New(4);
      int j;
      for (j = 0; j < 4; j++)
          PyList_SET_ITEM(item, j, PyFloat_FromDouble(momenta[i][j]));
      PyList_SET_ITEM(result, i, item);
  }

  return result;
}
PyDoc_STRVAR(compute_process_doc,
"compute_process_rcl(npr,p,order)\n\n\
Computes the structure-dressed helicity amplitudes and the summed squared\n\
amplitude (unless polarizations specified, see :py:meth:`define_process_rcl`)\n\
for the process with process number ``npr``.\n\n\
:param npr: process number\n\
:type  npr: int\n\
:param p: external momenta of the process with `l` legs\n\
:type  p: float[l][4]\n\
:param order: loop order, accepted values are :code:`'LO'` and :code:`'NLO'`\n\
:type  order: str\n\
:return: (:m:`|A_0|^2`, :m:`2 \\mathrm{Re}[A_0 A_1]`, momenta_check)\n\
:rtype: (float, float, bool)\n\n\
`momenta_check` tells whether the phase-space point is good\n\
(momenta_check=True) or bad (momenta_check=False) one. In the latter case no\n\
computation is performed and zero is returned for the matrix elements.\n\n\
.. note::\n\n\
    If the process is loop-induced, instead of :m:`2 \\mathrm{Re}[A_0 A_1]`\n\
    the squared loop :m:`|A_1|^2` is returned.");
static PyObject *
compute_process_py(PyObject *self, PyObject *args)
{
  int process_id;
  const char* order;
  PyObject *momenta_py;

  if (!PyArg_ParseTuple(args, "iO!s", &process_id,
                        &PyList_Type, &momenta_py, &order))
      return (PyObject *) NULL;

  int legs;
  get_legs_rcl(process_id, &legs, "compute_process_rcl");
  double momenta[legs][4];
  if (!build_c_momenta(process_id, momenta_py, momenta, legs))
  {
    return (PyObject *) NULL;
  }

  double A2[2];
  int momcheck;
  compute_process_rcl(process_id, momenta, order, A2, &momcheck);

  return Py_BuildValue("ddO", A2[0], A2[1], momcheck ? Py_True: Py_False);
}
PyDoc_STRVAR(get_amplitude_doc,
"get_amplitude_rcl(npr,order,colour,hel,pow=None,gs=None)\n\n\
Extracts a specific contribution to the amplitude of the process with\n\
process number ``npr``, according to the values of ``pow``, ``order``, \n\
``colour`` and ``hel``.\n\n\
Args:\n\n\
  npr (int): process number\n\
  order (str): loop-order of the contribution\n\n\
    * order = 'LO': tree squared amplitude\n\
    * order = 'NLO': loop squared amplitude\n\
    * order = 'NLO-D4': loop squared amplitude, bare loop contribution\n\
    * order = 'NLO-CT': loop squared amplitude, counterterms contribution\n\
    * order = 'NLO-R2': loop squared amplitude, rational terms contribution\n\n\
  colour (int[l]): colour structure which is a vector of type integer\n\
          and length l, where each position in the vector corresponds to\n\
          one of the l external particles of process ``npr``\n\
          (ordered as in the process definition).\n\
          For colourless particles, incoming quarks and outgoing \n\
          anti-quarks, the corresponding entry in `colour` must be 0.\n\
          For all other particles (gluons, outgoing quarks and incoming\n\
          anti-quarks), the entries must be, without repetition, the\n\
          positions of the gluons, incoming quarks and outgoing\n\
          anti-quarks in the process definition.\n\
  hel (int[l]): helicity (array) with entries:\n\n\
    * left-handed  fermions/antifermions: -1\n\
    * right-handed fermions/antifermions: +1\n\
    * logitudinal vector bosons:  0\n\
    * transverse vector bosons: -1, +1\n\
    * scalar particles:  0\n\
  pow/gs (int[]): array with values representing the powers in fundamental\n\
           couplings in the order as dictated by the model file, e.g.\n\
           `[2, 3]` :m:`\\equiv g_\\mathrm{s}^2 e^3`, or, if supported by the model,\n\
           the power in terms of :m:`g_\\mathrm{s}` only (power in :m:`e` is determined\n\
           automatically from the underlying process)\n\n\
Returns:\n\
  (float, float): (Ar, Ai)");
static PyObject *
get_amplitude_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id,gspow;
  PyObject *pow_py, *gs_py;
  const char* order;
  PyObject *colour_py;
  PyObject *hel_py;
  int legs,npow,gs;
  dcomplex A;

  pow_py = Py_None;
  gs_py = Py_None;
  static char *kwlist[] = {"npr", "order", "colour", "helicity", "pow", "gs", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "isO!O!|O!O", kwlist,
                                   &process_id, &order,
                                   &PyList_Type, &colour_py,
                                   &PyList_Type, &hel_py,
                                   &PyList_Type, &pow_py, &gs_py))
      return (PyObject *) NULL;


  get_legs_rcl(process_id, &legs, "get_amplitude_rcl");
  int colour[legs];
  if (!build_c_integer_array(legs, colour_py, colour, "colour"))
  {
    return (PyObject *) NULL;
  }
  int hel[legs];
  if (!build_c_integer_array(legs, hel_py, hel, "hel"))
  {
    return (PyObject *) NULL;
  }
  if (pow_py != Py_None) {
    // reconstruct pow_py
    get_n_orders_rcl(&npow);
    int pow[npow];
    if (!build_c_integer_array(npow, pow_py, pow, "pow"))
    {
      return (PyObject *) NULL;
    }
    get_amplitude_general_rcl(process_id, pow, order, colour, hel, &A);
  }
  else if (gs_py != Py_None)
  {
    if (!pyobj_pyint_check(gs_py)) {
      // error, we are expecting an integer value
      PyErr_SetString(PyExc_Exception, "gs is not of type integer.");
      return (PyObject *) NULL;
    }
    gs = pyobj_to_int(gs_py);
    get_amplitude_r1_rcl(process_id, gs, order, colour, hel, &A);
  }
  else
  {
    PyErr_SetString(PyExc_Exception, "Neither pow nor gs are != None.");
    return (PyObject *) NULL;
  }
  return Py_BuildValue("dd", A.dr, A.di);
}
PyDoc_STRVAR(get_squared_amplitude_doc,
"get_squared_amplitude_rcl(npr,order,pow=None,als=None)\n\n\
Extracts the computed value of the summed squared amplitude with ``pow`` being\n\
order in the fundamental couplings at loop-order ``order`` for the process with\n\
process number ``npr``.\n\n\
Args:\n\
  npr (int): process number\n\
  order (str): loop-order of the contribution:\n\n\
    * order = 'LO'    : tree squared amplitude\n\
    * order = 'NLO'   : loop squared amplitude\n\
    * order = 'NLO-D4': loop squared amplitude, bare loop contribution\n\
    * order = 'NLO-CT': loop squared amplitude, counterterms contribution\n\
    * order = 'NLO-R2': loop squared amplitude, rational terms contribution\n\
    * order = 'NLO-IR1': loop squared amplitude, IR single pole contribution\n\
                         Requires to enable IR pole computation via :py:meth:`set_compute_ir_poles_rcl`\n\
    * order = 'NLO-IR2': loop squared amplitude, IR double pole contribution\n\
                         Requires to enable IR pole computation via :py:meth:`set_compute_ir_poles_rcl`\n\
  pow/als (list/integer): array with values representing the powers in\n\
           fundamental couplings in the order as dictated by the model file, e.g.\n\
           `[2, 3]` :m:`\\equiv g_\\mathrm{s}^2 e^3`, or, if supported by the model,\n\
           the power in terms of ``als`` :m:`\\propto g_\\mathrm{s}^2` only\n\
           (power in :m:`e` is determined automatically from the underlying process)\n\n\
Returns:\n\
  (float): A2\n\
This routine can be called only after compute_process_rcl for process\n\
with process number `npr`");
static PyObject *
get_squared_amplitude_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id,npow,as;
  PyObject *pow_py, *as_py;
  const char* order;
  double A2;

  pow_py = Py_None;
  as_py = Py_None;
  static char *kwlist[] = {"npr", "order", "pow", "als", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "is|O!O", kwlist,
                                   &process_id, &order,
                                   &PyList_Type, &pow_py, &as_py))
      return (PyObject *)NULL;

  if (pow_py != Py_None) {
    // reconstruct pow_py
    get_n_orders_rcl(&npow);
    int pow[npow];
    if (!build_c_integer_array(npow, pow_py, pow, "pow"))
    {
      return (PyObject *) NULL;
    }
    get_squared_amplitude_general_rcl(process_id, pow, order, &A2);
  }
  else if (as_py != Py_None)
  {
    if (!pyobj_pyint_check(as_py)) {
      // error, we are expecting an integer value
      PyErr_SetString(PyExc_Exception, "als is not of type integer.");
      return (PyObject *) NULL;
    }
    as = pyobj_to_int(as_py);
    get_squared_amplitude_r1_rcl(process_id, as, order, &A2);
  }
  else
  {
    PyErr_SetString(PyExc_Exception, "Neither pow nor als are != None.");
    return (PyObject *) NULL;
  }
  return Py_BuildValue("d", A2);
}
PyDoc_STRVAR(get_polarized_squared_amplitude_doc,
"get_polarized_squared_amplitude_rcl(npr,order,hel,pow=None,als=None)\n\n\
Extracts a specific contribution to the polarized squared amplitude of the\n\
process with process number ``npr``, according to the values of ``pow``\n\
(or ``als``), ``order``, ``colour`` and ``hel``.\n\n\
Args:\n\
  npr (int): process number\n\
  order (str): loop-order of the contribution:\n\n\
    * order = 'LO'    : tree squared amplitude\n\
    * order = 'NLO'   : loop squared amplitude\n\
    * order = 'NLO-D4': loop squared amplitude, bare loop contribution\n\
    * order = 'NLO-CT': loop squared amplitude, counterterms contribution\n\
    * order = 'NLO-R2': loop squared amplitude, rational terms contribution\n\
    * order = 'NLO-IR1': loop squared amplitude, IR single pole contribution\n\
  hel (int[l]): helicity (array) with entries:\n\n\
    * left-handed  fermions/antifermions: -1\n\
    * right-handed fermions/antifermions: +1\n\
    * logitudinal vector bosons:  0\n\
    * transverse vector bosons: -1, +1\n\
    * scalar particles:  0\n\
  pow/gs (int[]): array with values representing the powers in fundamental\n\
           couplings in the order as dictated by the model file, e.g.\n\
           `[2, 3]` :m:`\\equiv g_\\mathrm{s}^2 e^3`, or, if supported by the model,\n\
           the power in terms of :m:`g_\\mathrm{s}` only (power in :m:`e` is determined\n\
           automatically from the underlying process)\n\n\
Returns:\n\
  (float): A2");
static PyObject *
get_polarized_squared_amplitude_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id,aspow;
  PyObject *pow_py, *as_py;
  const char* order;
  PyObject *hel_py;
  int legs,npow,as;
  double A2;
  Py_complex R;

  pow_py = Py_None;
  as_py = Py_None;
  static char *kwlist[] = {"npr", "order", "hel", "pow", "als", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "isO!|O!O", kwlist,
                                   &process_id, &order,
                                   &PyList_Type, &hel_py,
                                   &PyList_Type, &pow_py, &as_py))
      return (PyObject *)NULL;

  get_legs_rcl(process_id, &legs, "get_polarized_squared_amplitude_general_rcl");
  int hel[legs];
  if (!build_c_integer_array(legs, hel_py, hel, "hel"))
  {
    return (PyObject *) NULL;
  }

  if (pow_py != Py_None) {
    // reconstruct pow_py
    get_n_orders_rcl(&npow);
    int pow[npow];
    if (!build_c_integer_array(npow, pow_py, pow, "pow"))
    {
      return (PyObject *) NULL;
    }
    get_polarized_squared_amplitude_general_rcl(process_id, pow, order,
                                                hel, &A2);
  }
  else if (as_py != Py_None)
  {
    if (!pyobj_pyint_check(as_py)) {
      // error, we are expecting an integer value
      PyErr_SetString(PyExc_Exception, "als is not of type integer.");
      return (PyObject *) NULL;
    }
    as = pyobj_to_int(as_py);
    get_polarized_squared_amplitude_r1_rcl(process_id, as, order, hel, &A2);
  }
  else
  {
    PyErr_SetString(PyExc_Exception, "Neither pow nor als are != None.");
    return (PyObject *) NULL;
  }
  return Py_BuildValue("d", A2);
}
PyDoc_STRVAR(compute_colour_correlation_doc,
"compute_colour_correlation_rcl(npr,p,i1,i2,order='LO')\n\n\
Computes the colour-correlated summed squared amplitude, between particle\n\
with leg number ``i1`` and particle with leg number ``i2``, for the process with\n\
process number ``npr``.\n\n\
:param npr: process number\n\
:type npr: int\n\
:param p: external momenta of the process with `l` legs\n\
:type  p: float[l][4]\n\
:param i1: quark/anti-quark at leg i1\n\
:type  i1: int\n\
:param i2: quark/anti-quark at leg i2\n\
:type  i2: int\n\
:param order: either :code:`'LO'` (default) or :code:`'NLO'`\n\
:type  order: str\n\
:return: (A2cc, momcheck)\n\
:rtype: (float, bool)");
static PyObject *
compute_colour_correlation_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id, i1,i2;
  PyObject *momenta_py, *order_py;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "p", "i1", "i2", "order", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "iO!ii|O", kwlist,
                                   &process_id, &PyList_Type, &momenta_py,
                                   &i1,&i2, &order_py))
      return (PyObject *)NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  int legs;
  get_legs_rcl(process_id, &legs, "compute_colour_correlation_rcl");
  double momenta[legs][4];
  if (!build_c_momenta(process_id, momenta_py, momenta, legs))
  {
    return (PyObject *) NULL;
  }

  double A2cc;
  int momcheck;
  compute_colour_correlation_rcl(process_id, momenta, i1, i2, order,
                                 &A2cc, &momcheck);

  return Py_BuildValue("dO", A2cc, momcheck ? Py_True: Py_False);
}
PyDoc_STRVAR(compute_all_colour_correlations_doc,
"compute_all_colour_correlations_rcl(npr,p,order='LO')\n\n\
Computes the colour-correlated summed squared amplitude, between all\n\
possible pairs of coloured particles for the process with\n\
process number `npr`.\n\
npr    = integer     -> process number\n\
p      = float[l][4] -> external momenta of the process with `l` legs\n\
order  = string  -> either 'LO' (default) or 'NLO'\n\
return = momcheck");
static PyObject *
compute_all_colour_correlations_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id;
  PyObject *momenta_py, *order_py;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "p", "order", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "iO!|O", kwlist,
                                   &process_id, &PyList_Type, &momenta_py,
                                   &order_py))
      return (PyObject *)NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }
  int legs;
  get_legs_rcl(process_id, &legs, "compute_all_colour_correlations_rcl");
  double momenta[legs][4];
  if (!build_c_momenta(process_id, momenta_py, momenta, legs))
  {
    return (PyObject *) NULL;
  }

  int momcheck;
  compute_all_colour_correlations_rcl(process_id, momenta, order, &momcheck);

  return Py_BuildValue("O", momcheck ? Py_True: Py_False);
}
PyDoc_STRVAR(get_colour_correlation_doc,
"get_colour_correlation_rcl(npr,i1,i2,order='LO',pow=None,als=None)\n\n\
Extracts the computed value of the colour-correlated summed squared\n\
amplitude.\n\n\
Args:\n\
  npr (int): process number\n\
  i1 (int): quark/anti-quark at leg i1\n\
  i2 (int): quark/anti-quark at leg i2\n\
  order (str): 'LO' (default) or 'NLO':\n\
  pow/gs (int[]): array with values representing the powers in fundamental\n\
           couplings in the order as dictated by the model file, e.g.\n\
           `[2, 3]` :m:`\\equiv g_\\mathrm{s}^2 e^3`, or, if supported by the model,\n\
           the power in terms of :m:`g_\\mathrm{s}` only (power in :m:`e` is determined\n\
           automatically from the underlying process)\n\n\
Returns:\n\
  (float): A2cc\n\
This function can only be called after:\n\n\
  * :py:meth:`compute_colour_correlation_rcl`,\n\
  * :py:meth:`rescale_colour_correlation_rcl`,\n\
  * :py:meth:`compute_all_colour_correlations_rcl`,\n\
  * :py:meth:`rescale_all_colour_correlations_rcl`.");
static PyObject *
get_colour_correlation_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id,as,i1,i2,npow;
  PyObject *order_py, *pow_py, *as_py;
  double A2cc;

  pow_py = Py_None;
  as_py = Py_None;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "i1", "i2", "order", "pow", "als", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "iii|OO!O", kwlist,
                                   &process_id, &i1, &i2,
                                   &order_py, &PyList_Type, &pow_py, &as_py))
      return (PyObject *) NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  if (pow_py != Py_None) {
    // reconstruct pow_py
    get_n_orders_rcl(&npow);
    int pow[npow];
    if (!build_c_integer_array(npow, pow_py, pow, "pow"))
    {
      return (PyObject *) NULL;
    }
    get_colour_correlation_general_rcl(process_id, pow, i1, i2, order, &A2cc);
  }
  else if (as_py != Py_None)
  {
    if (!pyobj_pyint_check(as_py)) {
      // error, we are expecting an integer value
      PyErr_SetString(PyExc_Exception, "als is not of type integer.");
      return (PyObject *) NULL;
    }
    as = pyobj_to_int(as_py);
    get_colour_correlation_r1_rcl(process_id, as, i1, i2, order, &A2cc);
  }
  else
  {
    PyErr_SetString(PyExc_Exception, "Neither pow nor gs are != None.");
    return (PyObject *) NULL;
  }
  return Py_BuildValue("d", A2cc);
}
PyDoc_STRVAR(compute_spin_colour_correlation_doc,
"compute_spin_colour_correlation_rcl(npr,p,i1,i2,v,order='LO')\n\n\
Computes the spin-colour-correlated summed squared amplitude, between\n\
particle with leg number ``i1`` and particle with leg number ``i2``, for the\n\
process with process number ``npr``.\n\
The spin-correlation is achieved by a user-defined polarization vector ``v``\n\
for the particle with leg number ``i1``, which substitutes the usual one.\n\n\
:param npr: process number\n\
:type  npr: int\n\
:param p: external momenta of the process with `l` legs\n\
:type  p: float[l][4]\n\
:param i1: quark/anti-quark at leg i1\n\
:type  i1: int\n\
:param i2: quark/anti-quark at leg i2\n\
:type  i2: int\n\
:param v: polarization vector\n\
:type  v: complex[4]\n\
:param order: either 'LO' (default) or 'NLO'\n\
:type  order: str\n\
:return: (A2scc, momcheck)\n\
:rtype: (float, bool)");
static PyObject *
compute_spin_colour_correlation_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id, i1,i2;
  PyObject *momenta_py;
  PyObject *polvec_py, *order_py;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "p", "i1", "i2", "v", "order", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "iO!iiO!|O", kwlist,
                                   &process_id, &PyList_Type, &momenta_py,
                                   &i1,&i2, &PyList_Type, &polvec_py,
                                   &order_py))
      return (PyObject *)NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  int legs;
  get_legs_rcl(process_id, &legs, "compute_spin_colour_correlation_rcl");
  double momenta[legs][4];
  if (!build_c_momenta(process_id, momenta_py, momenta, legs))
  {
    return (PyObject *) NULL;
  }

  dcomplex v[4];
  if (!build_c_polvec(polvec_py, v))
  {
    return (PyObject *) NULL;
  }

  double A2scc;
  int momcheck;
  compute_spin_colour_correlation_rcl(process_id, momenta, i1, i2, v,
                                      order, &A2scc, &momcheck);

  return Py_BuildValue("dO", A2scc, momcheck ? Py_True: Py_False);
}
PyDoc_STRVAR(get_spin_colour_correlation_doc,
"get_spin_colour_correlation_rcl(npr,i1,i2,order='LO',pow=None,als=None)\n\n\
Extracts the computed value of the spin-colour-correlated summed squared\n\
amplitude.\n\n\
Args:\n\
  npr (int): process number\n\
  i1 (int): quark/anti-quark at leg i1\n\
  i2 (int): quark/anti-quark at leg i2\n\
  order (str): 'LO' (default) or 'NLO':\n\
  pow/gs (int[]): array with values representing the powers in fundamental\n\
           couplings in the order as dictated by the model file, e.g.\n\
           `[2, 3]` :m:`\\equiv g_\\mathrm{s}^2 e^3`, or, if supported by the model,\n\
           the power in terms of :m:`g_\\mathrm{s}` only (power in :m:`e` is determined\n\
           automatically from the underlying process)\n\n\
Returns:\n\
  (float): A2\n\
This function can only be called after:\n\n\
  * :py:meth:`compute_spin_colour_correlation_rcl`,\n\
  * :py:meth:`rescale_spin_colour_correlation_rcl`,\n\
  * :py:meth:`compute_all_spin_colour_correlations_rcl`,\n\
  * :py:meth:`rescale_all_spin_colour_correlations_rcl`.");
static PyObject *
get_spin_colour_correlation_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id,as,i1,i2,npow;
  PyObject *pow_py, *as_py, *order_py;
  double A2scc;

  pow_py = Py_None;
  as_py = Py_None;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "i1", "i2", "order", "pow", "als", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "iii|OO!O", kwlist,
                                   &process_id, &i1, &i2, &order_py,
                                   &PyList_Type, &pow_py, &as_py))
      return (PyObject *)NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  if (pow_py != Py_None) {
    // reconstruct pow_py
    get_n_orders_rcl(&npow);
    int pow[npow];
    if (!build_c_integer_array(npow, pow_py, pow, "pow"))
    {
      return (PyObject *) NULL;
    }
    get_spin_colour_correlation_general_rcl(process_id, pow, i1, i2, order, &A2scc);
  }
  else if (as_py != Py_None)
  {
    if (!pyobj_pyint_check(as_py)) {
      // error, we are expecting an integer value
      PyErr_SetString(PyExc_Exception, "als is not of type integer.");
      return (PyObject *) NULL;
    }
    as = pyobj_to_int(as_py);
    get_spin_colour_correlation_r1_rcl(process_id, as, i1, i2, order, &A2scc);
  }
  else
  {
    PyErr_SetString(PyExc_Exception, "Neither pow nor als are != None.");
    return (PyObject *) NULL;
  }
  return Py_BuildValue("d", A2scc);
}
PyDoc_STRVAR(compute_spin_correlation_doc,
"compute_spin_correlation_rcl(npr,p,j,v,order='LO')\n\n\
Computes the spin-correlated summed squared amplitude, where the \n\
polarization vector of particle ``j``, necessarily being a gluon or photon,\n\
is is substituted for the user-defined polarization vector ``v``.\n\n\
:param npr: process number\n\
:type  npr: int\n\
:param p: external momenta of the process with `l` legs\n\
:type  p: float[l][4]\n\
:param j: photon/gluon leg\n\
:type  j: int\n\
:param v: photon/gluon polarization vector\n\
:type  v: complex[4]\n\
:return: (A2sc, momcheck)\n\
:rtype: (float, bool)");
static PyObject *
compute_spin_correlation_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id, j;
  PyObject *momenta_py, *polvec_py, *order_py;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "p", "j", "v", "order", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "iO!iO!|O", kwlist,
                                   &process_id,
                                   &PyList_Type, &momenta_py, &j,
                                   &PyList_Type, &polvec_py, &order_py))
      return (PyObject *)NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  int legs;
  get_legs_rcl(process_id, &legs, "compute_spin_correlation_rcl");
  double momenta[legs][4];
  if (!build_c_momenta(process_id, momenta_py, momenta, legs))
  {
    return (PyObject *) NULL;
  }

  dcomplex v[4];
  if (!build_c_polvec(polvec_py, v))
  {
    return (PyObject *) NULL;
  }

  double A2sc;
  int momcheck;
  compute_spin_correlation_rcl(process_id, momenta, j, v, order, &A2sc, &momcheck);

  return Py_BuildValue("dO", A2sc, momcheck ? Py_True: Py_False);
}
PyDoc_STRVAR(get_spin_correlation_doc,
"get_spin_correlation_rcl(npr,order='LO',pow=None,als=None)\n\n\
Extracts the computed value of the spin-correlated summed squared\n\
amplitude.\n\n\
Args:\n\
  npr (int): process number\n\
  order (str): 'LO' (default) or 'NLO':\n\
  pow/gs (int[]): array with values representing the powers in fundamental\n\
           couplings in the order as dictated by the model file, e.g.\n\
           `[2, 3]` :m:`\\equiv g_\\mathrm{s}^2 e^3`, or, if supported by the model,\n\
           the power in terms of :m:`g_\\mathrm{s}` only (power in :m:`e` is determined\n\
           automatically from the underlying process)\n\n\
Returns:\n\
  (float): A2sc\n\
This routine can be called only after:\n\n\
  * :py:meth:`compute_spin_correlation_rcl`,\n\
  * :py:meth:`rescale_spin_correlation_rcl`");
static PyObject *
get_spin_correlation_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id,as,npow;
  PyObject *pow_py, *as_py, *order_py;
  double A2sc;

  pow_py = Py_None;
  as_py = Py_None;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "order", "pow", "als", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "i|OO!O", kwlist,
                                   &process_id, &order_py,
                                   &PyList_Type, &pow_py, &as_py))
      return (PyObject *)NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  if (pow_py != Py_None) {
    // reconstruct pow_py
    get_n_orders_rcl(&npow);
    int pow[npow];
    if (!build_c_integer_array(npow, pow_py, pow, "pow"))
    {
      return (PyObject *) NULL;
    }
    get_spin_correlation_general_rcl(process_id,pow,order,&A2sc);
  }
  else if (as_py != Py_None)
  {
    if (!pyobj_pyint_check(as_py)) {
      // error, we are expecting an integer value
      PyErr_SetString(PyExc_Exception, "als is not of type integer.");
      return (PyObject *) NULL;
    }
    as = pyobj_to_int(as_py);
    get_spin_correlation_r1_rcl(process_id,as,order,&A2sc);
  }
  else
  {
    PyErr_SetString(PyExc_Exception, "Neither pow nor als are != None.");
    return (PyObject *) NULL;
  }
  return Py_BuildValue("d", A2sc);
}
PyDoc_STRVAR(compute_spin_correlation_matrix_doc,
"compute_spin_correlation_matrix_rcl(npr,p,j,order='LO')\n\n\
Computes the summed spin-correlated matrix squared amplitude\n\
:m:`B_j^{\\mu\\nu}` (arXiv:1002.2581 Eq. (2.8)) for the vector\n\
particle ``j`` (a gluon or photon).\n\n\
Args:\n\
  npr (int): process number\n\
  p (float[l][4]) external momenta of the process with `l` legs\n\
  j (int): photon/gluon leg\n\
  order (str): loop-order, accepted values ’LO’ and ’NLO’\n\n\
  pow/gs (int[]): array with values representing the powers in fundamental\n\
           couplings in the order as dictated by the model file, e.g.\n\
           `[2, 3]` :m:`\\equiv g_\\mathrm{s}^2 e^3`, or, if supported by the model,\n\
           the power in terms of :m:`g_\\mathrm{s}` only (power in :m:`e` is determined\n\
           automatically from the underlying process)\n\n\
Returns:\n\
  (float, bool): (A2scm, momcheck)");
static PyObject *
compute_spin_correlation_matrix_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id, j;
  PyObject *momenta_py, *polvec_py, *order_py;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "p", "j", "order", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "iO!i|O", kwlist,
                                   &process_id, &PyList_Type, &momenta_py, &j,
                                   &order_py))
      return (PyObject *)NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  int legs;
  get_legs_rcl(process_id, &legs, "compute_spin_correlation_matrix_rcl");
  double momenta[legs][4];
  if (!build_c_momenta(process_id, momenta_py, momenta, legs))
  {
    return (PyObject *) NULL;
  }

  double A2scm[4][4];
  int momcheck;
  compute_spin_correlation_matrix_rcl(process_id, momenta, j, order, A2scm, &momcheck);

  PyObject *A2scm_py = PyList_New(4);
  int i;
  for (i = 0; i < 4; i++) {
      PyObject *item = PyList_New(4);
      int j;
      for (j = 0; j < 4; j++)
          PyList_SET_ITEM(item, j, PyFloat_FromDouble(A2scm[i][j]));
      PyList_SET_ITEM(A2scm_py, i, item);
  }
  return Py_BuildValue("OO", A2scm_py, momcheck ? Py_True: Py_False);
}
PyDoc_STRVAR(get_spin_correlation_matrix_doc,
"get_spin_correlation_matrix_rcl(npr,order='LO',pow=None,als=None)\n\n\
Extracts the computed value of the spin-correlated matrix summed squared\n\
amplitude.\n\n\
Args:\n\
  npr (int): process number\n\
  i1 (int): quark/anti-quark at leg i1\n\
  i2 (int): quark/anti-quark at leg i2\n\
  order (str): 'LO' (default) or 'NLO':\n\
  pow/gs (int[]): array with values representing the powers in fundamental\n\
           couplings in the order as dictated by the model file, e.g.\n\
           `[2, 3]` :m:`\\equiv g_\\mathrm{s}^2 e^3`, or, if supported by the model,\n\
           the power in terms of :m:`g_\\mathrm{s}` only (power in :m:`e` is determined\n\
           automatically from the underlying process)\n\n\
This routine can be called only after:\n\n\
  * :py:meth:`compute_spin_correlation_matrix_rcl`,\n\
  * :py:meth:`rescale_spin_correlation_matrix_rcl`.");
static PyObject *
get_spin_correlation_matrix_py(PyObject *self, PyObject *args, PyObject *keywds)
{
  int process_id,as,npow;
  PyObject *pow_py, *as_py, *order_py;
  double A2scm[4][4];

  pow_py = Py_None;
  as_py = Py_None;
  order_py = Py_None;
  static char *kwlist[] = {"npr", "order", "pow", "als", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "i|OO!O", kwlist,
                                   &process_id, &order_py,
                                   &PyList_Type, &pow_py, &as_py))
      return (PyObject *)NULL;

  const char *order;
  if (order_py != Py_None) {
    order = pyobj_to_string(order_py);
  } else {
    order = "LO";
  }

  if (pow_py != Py_None) {
    // reconstruct pow_py
    get_n_orders_rcl(&npow);
    int pow[npow];
    if (!build_c_integer_array(npow, pow_py, pow, "pow"))
    {
      return (PyObject *) NULL;
    }
    get_spin_correlation_matrix_general_rcl(process_id,pow,order,A2scm);
  }
  else if (as_py != Py_None)
  {
    if (!pyobj_pyint_check(as_py)) {
      // error, we are expecting an integer value
      PyErr_SetString(PyExc_Exception, "als is not of type integer.");
      return (PyObject *) NULL;
    }
    as = pyobj_to_int(as_py);
    get_spin_correlation_matrix_r1_rcl(process_id,as,order,A2scm);
  }
  else
  {
    PyErr_SetString(PyExc_Exception, "Neither pow nor als are != None.");
    return (PyObject *) NULL;
  }

  PyObject *A2scm_py = PyList_New(4);
  int i;
  for (i = 0; i < 4; i++) {
      PyObject *item = PyList_New(4);
      int j;
      for (j = 0; j < 4; j++)
          PyList_SET_ITEM(item, j, PyFloat_FromDouble(A2scm[i][j]));
      PyList_SET_ITEM(A2scm_py, i, item);
  }
  return Py_BuildValue("O", A2scm_py);
}
PyDoc_STRVAR(get_momenta_doc,
"get_momenta_rcl(npr)\n\n\
Extracts the stored momenta of the process with process number `npr`.\n\
Note that the internal momenta can differ from the ones given by the user.\n\
This function can only be called after:\n\
 - compute_process_rcl\n\
 - compute_colour_correlation_rcl\n\
 - compute_all_colour_correlations_rcl\n\
 - compute_spin_colour_correlation_rcl\n\
 - compute_spin_correlation_rcl\n\
npr    = integer -> process number\n\
return = p[l][4] -> Internal momenta used in the calculation.");
static PyObject *
get_momenta_py(PyObject *self, PyObject *args)
{
  int process_id;
  if (!PyArg_ParseTuple(args, "i", &process_id))
      return (PyObject *) NULL;

  int legs;
  get_legs_rcl(process_id, &legs, "get_momenta_rcl");
  double momenta[legs][4];
  get_momenta_rcl(process_id, momenta);

  PyObject *result = PyList_New(legs);
  int i;
  for (i = 0; i < legs; i++) {
      PyObject *item = PyList_New(4);
      int j;
      for (j = 0; j < 4; j++)
          PyList_SET_ITEM(item, j, PyFloat_FromDouble(momenta[i][j]));
      PyList_SET_ITEM(result, i, item);
  }
  return result;
}
PyDoc_STRVAR(get_recola_version_doc,
"get_recola_version_rcl()\n\n\
Returns the version number of the Recola2 library.");
static PyObject *
get_recola_version_py(PyObject *self, PyObject *args)
{
  char recolaversion[10];
  get_recola_version_rcl(&recolaversion);
  return Py_BuildValue("s", recolaversion);
}
PyDoc_STRVAR(get_modelname_doc,
"get_modelname_rcl()\n\n\
Returns the name of model linked to the Recola2 library.");
static PyObject *
get_modelname_py(PyObject *self, PyObject *args)
{
  char modelname[100];
  get_modelname_rcl(&modelname);
  return Py_BuildValue("s", modelname);
}
PyDoc_STRVAR(get_modelgauge_doc,
"get_modelgauge_rcl()\n\n\
Returns the current gauge used of model file in use.");
static PyObject *
get_modelgauge_py(PyObject *self, PyObject *args)
{
  char modelname[100];
  get_modelgauge_rcl(&modelname);
  return Py_BuildValue("s", modelname);
}
PyDoc_STRVAR(get_driver_timestamp_doc,
"get_driver_timestamp_rcl()\n\n\
Returns the precise generation date of model library.");
static PyObject *
get_driver_timestamp_py(PyObject *self, PyObject *args)
{
  char driverts[200];
  get_driver_timestamp_rcl(&driverts);
  return Py_BuildValue("s", driverts);
}
PyDoc_STRVAR(get_renoscheme_doc,
"get_renoscheme_rcl(rname)\n\n\
Returns the active renormalization scheme for the parameter `rname`.\n\
rname = string");
static PyObject *
get_renoscheme_py(PyObject *self, PyObject *args)
{
  const char* rname;

  if (!PyArg_ParseTuple(args, "s", &rname))
      return (PyObject *)NULL;

  char rscheme[100];
  get_renoscheme_rcl(rname, &rscheme);
  return Py_BuildValue("s", rscheme);
}

/*****************************************
*  module extended_higgs_interface_rcl  *
*****************************************/

PyDoc_STRVAR(set_Z2_thdm_yukawa_type_doc,
"set_Z2_thdm_yukawa_type_rcl(ytype)\n\n\
Sets the yukawa type in the softly-broken Z2 symmetric THDM to ``ytype``.\n\n\
Args:\n\
  ytype (int): Yukawa type\n\n\
    * ytype = 1: Type-I   (h1u=h1d=h1l=0)\n\
    * ytype = 2: Type-II  (h1u=h2d=h2l=0)\n\
    * ytype = 3: Type-X   (h1u=h1d=h2l=0)\n\
    * ytype = 4: Type-Y   (h1u=h2d=h1l=0)\n\n\
Note:\n\
  The parameters h1u,h2u,h1d,h2d,h1l,h2l are used internally\n\
  to set the appropriate yukawa interaction. Their value is either \n\
  1 or 0 and printed in the output.");
static PyObject *
set_Z2_thdm_yukawa_type_py(PyObject *self, PyObject *args)
{

  int ytype;
  if (!PyArg_ParseTuple(args, "i", &ytype))
      return NULL;

  set_Z2_thdm_yukawa_type_rcl(ytype);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_hl_doc,
"set_pole_mass_hl_rcl(m,g)\n\n\
Sets the pole mass and width of the light Higgs boson Hl.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_hl_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_hl_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_hh_doc,
"set_pole_mass_hh_rcl(m,g)\n\n\
Sets the pole mass and width of the heavy Higgs boson hh.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_hh_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_hh_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_hl_hh_doc,
"set_pole_mass_hl_hh_rcl(ml,gl,mh,gh)\n\n\
Sets the pole masses, widths of the light and heavy Higgs bosons\n\
to ``ml``, ``gl`` and ``mh``, ``gh``, respectively.\n\n\
:param ml: pole mass of light Higgs boson\n\
:type  ml: float\n\
:param gl: pole width of light Higgs boson\n\
:type  gl: float\n\
:param mh: pole mass of heavy Higgs boson \n\
:type  mh: float\n\
:param gh: pole width of heavy Higgs boson\n\
:type  gh: float");
static PyObject *
set_pole_mass_hl_hh_py(PyObject *self, PyObject *args)
{

  double ml, gl, mh, gh;
  if (!PyArg_ParseTuple(args, "dddd", &ml, &gl, &mh, &gh))
      return NULL;

  set_pole_mass_hl_hh_rcl(ml, gl, mh, gh);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_ha_doc,
"set_pole_mass_ha_rcl(m,g)\n\n\
Sets the pole mass and width of the pseudo-scalar Higgs boson ha.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_ha_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_ha_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_pole_mass_hc_doc,
"set_pole_mass_hc_rcl(m,g)\n\n\
Sets the pole mass and width of the charged Higgs boson hc.\n\n\
:param m: pole mass \n\
:type  m: float\n\
:param g: pole width\n\
:type  g: float");
static PyObject *
set_pole_mass_hc_py(PyObject *self, PyObject *args)
{

  double m, g;
  if (!PyArg_ParseTuple(args, "dd", &m, &g))
      return NULL;

  set_pole_mass_hc_rcl(m, g);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_tb_cab_doc,
"set_tb_cab_rcl(tb,cab)\n\n\
Sets the value for :m:`\\tan(\\beta)` and :m:`c_{\\alpha\\beta}` to\n\
``tb`` and ``cab``, respectively.\n\n\
Args:\n\
  tb (float): value for :m:`\\tan(\\beta)`\n\
  cab (float): value for :m:`c_{\\alpha\\beta}`");
static PyObject *
set_tb_cab_py(PyObject *self, PyObject *args)
{

  double tb, cab;
  if (!PyArg_ParseTuple(args, "dd", &tb, &cab))
      return NULL;

  set_tb_cab_rcl(tb, cab);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_mixing_alpha_msbar_scheme_doc,
"use_mixing_alpha_msbar_scheme_rcl(s)\n\n\
Sets the renormalization scheme for the mixing angle :m:`\\alpha` to an\n\
:m:`\\overline{\\mathrm{MS}}` scheme.\n\n\
Detailed informations on the schemes are given\n\
in Table 2 in :cite:`Denner:2018opp`.\n\n\
Args:\n\
  s (str): renormalization scheme identifier\n\n\
    * s = ``'FJ'``: :m:`\\overline{\\mathrm{MS}}` in FJ tadpole scheme\n\
    * s = ``'PR'``: :m:`\\overline{\\mathrm{MS}}` in PR tadpole scheme");
static PyObject *
use_mixing_alpha_msbar_scheme_py(PyObject *self, PyObject *args)
{
  const char* s;
  if (!PyArg_ParseTuple(args, "s", &s))
      return NULL;

  use_mixing_alpha_msbar_scheme_rcl(s);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_mixing_alpha_onshell_scheme_doc,
"use_mixing_alpha_onshell_scheme_rcl(s)\n\n\
Sets the renormalization scheme for the mixing angle :m:`\\alpha` to an\n\
on-shell or BFM scheme.\n\n\
Detailed informations on the schemes are given\n\
in Table 2 in :cite:`Denner:2018opp`.\n\n\
Args:\n\
  s (str): renormalization scheme identifier\n\n\
    * s = ``'OS'``: On-shell scheme via decays (Only HS): \n\
           :m:`H_i \\to \\overline{\\psi}\\psi`\n\
    * s = ``'OS1'``: On-shell scheme for decays (Only THDM): \n\
           :m:`H_i \\to \\overline{\\nu}_\\mathrm{R}\\nu` in Type I\n\
    * s = ``'OS2'``: On-shell scheme for decays (Only THDM): \n\
           :m:`H_i \\to \\overline{\\nu}_\\mathrm{R}\\nu` in Type II\n\
    * s = ``'BFM'``: BFM symmetry relation.");
static PyObject *
use_mixing_alpha_onshell_scheme_py(PyObject *self, PyObject *args)
{
  const char* s;
  if (!PyArg_ParseTuple(args, "s", &s))
      return NULL;

  use_mixing_alpha_onshell_scheme_rcl(s);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_mixing_beta_onshell_scheme_doc,
"use_mixing_beta_onshell_scheme_rcl(s)\n\n\
Sets the renormalization scheme for the mixing angle :m:`\\beta` to an\n\
on-shell or BFM scheme.\n\
Detailed informations on the schemes are given\n\
in Table 2 in :cite:`Denner:2018opp`.\n\n\
Args:\n\
  s (str): renormalization scheme identifier\n\n\
    * s = ``'OS1'``: On-shell scheme for decays: \n\
           :m:`H_a \\to \\overline{\\nu}_\\mathrm{R}\\nu`, :m:`H_i \\to \\overline{\\nu}_\\mathrm{R}\\nu` in Type I\n\
    * s = ``'OS2'``: On-shell scheme for decays: \n\
           :m:`H_a \\to \\overline{\\nu}_\\mathrm{R}\\nu`, :m:`H_i \\to \\overline{\\nu}_\\mathrm{R}\\nu` in Type II\n\
    * s = ``'OS12'``: On-shell scheme for decays: \n\
           :m:`H_a \\to \\overline{\\nu}_\\mathrm{R}\\nu`, :m:`H_i \\to \\overline{\\nu}_\\mathrm{R}\\nu` in Type I/II\n\
    * s = ``'BFM'``: BFM symmetry relation.");
static PyObject *
use_mixing_beta_onshell_scheme_py(PyObject *self, PyObject *args)
{
  const char* s;
  if (!PyArg_ParseTuple(args, "s", &s))
      return NULL;

  use_mixing_beta_onshell_scheme_rcl(s);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_mixing_beta_msbar_scheme_doc,
"use_mixing_beta_msbar_scheme_rcl(s)\n\n\
Sets the renormalization scheme for the mixing angle :m:`\\beta` to an\n\
:m:`\\overline{\\mathrm{MS}}` scheme.\n\n\
Detailed informations on the schemes are given\n\
in Table 2 in :cite:`Denner:2018opp`.\n\n\
Args:\n\
  s (str): renormalization scheme identifier\n\n\
    * s = ``'FJ'``: :m:`\\overline{\\mathrm{MS}}` in FJ tadpole scheme\n\
    * s = ``'PR'``: :m:`\\overline{\\mathrm{MS}}` in PR tadpole scheme");
static PyObject *
use_mixing_beta_msbar_scheme_py(PyObject *self, PyObject *args)
{
  const char* s;
  if (!PyArg_ParseTuple(args, "s", &s))
      return NULL;

  use_mixing_beta_msbar_scheme_rcl(s);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_l5_doc,
"set_l5_rcl(l5)\n\n\
Sets the value for :m:`\\lambda_5` to ``l5``.\n\n\
Args:\n\
  l5 (float): value for :m:`\\lambda_5`");
static PyObject *
set_l5_py(PyObject *self, PyObject *args)
{

  double l5;
  if (!PyArg_ParseTuple(args, "d", &l5))
      return NULL;

  set_l5_rcl(l5);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_msb_doc,
"set_msb_rcl(msb)\n\n\
Deprecated. Call has no effect.");
static PyObject *
set_msb_py(PyObject *self, PyObject *args)
{

  double msb;
  if (!PyArg_ParseTuple(args, "d", &msb))
      return NULL;

  set_msb_rcl(msb);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_msb_msbar_scheme_doc,
"use_msb_msbar_scheme_rcl(s)\n\n\
Sets the renormalization scheme for soft-Z2 breaking\n\
scale or a related paraemter to the msbar scheme.\n\
s = 'MSB' -> MSB renormalized msbar\n\
s = 'm12' -> m12 renormalized msbar");
static PyObject *
use_msb_msbar_scheme_py(PyObject *self, PyObject *args)
{
  const char* s;
  if (!PyArg_ParseTuple(args, "s", &s))
      return NULL;

  use_msb_msbar_scheme_rcl(s);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_sa_doc,
"set_sa_rcl(sa)\n\n\
Sets the value for :m:`\\sin(\\alpha)` to ``sa``.\n\n\
Args:\n\
  sa (float): value for :m:`\\sin(\\alpha)`");
static PyObject *
set_sa_py(PyObject *self, PyObject *args)
{

  double sa;
  if (!PyArg_ParseTuple(args, "d", &sa))
      return NULL;

  set_sa_rcl(sa);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_tb_doc,
"set_tb_rcl(tb)\n\n\
Sets the value for :m:`\\tan(\\beta)` to ``tb``.\n\n\
Args:\n\
  tb (float): value for :m:`\\tan(\\beta)`");
static PyObject *
set_tb_py(PyObject *self, PyObject *args)
{

  double tb;
  if (!PyArg_ParseTuple(args, "d", &tb))
      return NULL;

  set_tb_rcl(tb);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(set_l3_doc,
"set_l3_rcl(l3)\n\n\
Sets the value for :m:`\\lambda_3` to ``l3``.\n\n\
Args:\n\
  l3 (float): value for :m:`\\lambda_3`");
static PyObject *
set_l3_py(PyObject *self, PyObject *args)
{

  double l3;
  if (!PyArg_ParseTuple(args, "d", &l3))
      return NULL;

  set_l3_rcl(l3);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_tb_msbar_scheme_doc,
"use_tb_msbar_scheme_rcl(s)\n\n\
Sets the renormalization scheme for :m:`t_\\beta` to an\n\
:m:`\\overline{\\mathrm{MS}}` scheme.\n\n\
Detailed informations on the schemes are given\n\
in Table 1 in :cite:`Denner:2018opp`.\n\n\
Args:\n\
  s (str): renormalization scheme identifier\n\n\
    * s = ``'FJ'``: :m:`\\overline{\\mathrm{MS}}` in FJ tadpole scheme\n\
    * s = ``'PR'``: :m:`\\overline{\\mathrm{MS}}` in PR tadpole scheme\n\
    * s = ``'l2'``: :m:`\\overline{\\mathrm{MS}}` for :m:`\\lambda_2`.");
static PyObject *
use_tb_msbar_scheme_py(PyObject *self, PyObject *args)
{
  const char* s;
  if (!PyArg_ParseTuple(args, "s", &s))
      return NULL;

  use_tb_msbar_scheme_rcl(s);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_l3_msbar_scheme_doc,
"use_l3_msbar_scheme_rcl(s)\n\n\
Sets the renormalization scheme for :m:`\\lambda_3` to an\n\
:m:`\\overline{\\mathrm{MS}}` scheme.\n\n\
Detailed informations on the schemes are given\n\
in Table 1 in :cite:`Denner:2018opp`.\n\n\
Args:\n\
  s (str): renormalization scheme identifier\n\n\
    * s = ``'FJ'``: :m:`\\overline{\\mathrm{MS}}` in FJ tadpole scheme\n\
    * s = ``'l1'``: :m:`\\overline{\\mathrm{MS}}` for :m:`\\lambda_1`.");
static PyObject *
use_l3_msbar_scheme_py(PyObject *self, PyObject *args)
{
  const char* s;
  if (!PyArg_ParseTuple(args, "s", &s))
      return NULL;

  use_l3_msbar_scheme_rcl(s);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_tb_onshell_scheme_doc,
"use_tb_onshell_scheme_rcl(s)\n\n\
Sets the renormalization scheme for :m:`t_\\beta` to an\n\
on-shell or BFM scheme.\n\n\
Detailed informations on the schemes are given\n\
in Table 1 in :cite:`Denner:2018opp`.\n\n\
Args:\n\
  s (str): renormalization scheme identifier\n\n\
    * s = ``'BFM'``: BFM symmetry relation.");
static PyObject *
use_tb_onshell_scheme_py(PyObject *self, PyObject *args)
{
  const char* s;
  if (!PyArg_ParseTuple(args, "s", &s))
      return NULL;

  use_tb_onshell_scheme_rcl(s);
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(use_l3_onshell_scheme_doc,
"use_l3_onshell_scheme_rcl(s)\n\n\
Sets the renormalization scheme for :m:`\\lambda_3` to an\n\
on-shell or BFM scheme.\n\n\
Detailed informations on the schemes are given\n\
in Table 1 in :cite:`Denner:2018opp`.\n\n\
Args:\n\
  s (str): renormalization scheme identifier\n\n\
    * s = ``'BFM'``: BFM symmetry relation.");
static PyObject *
use_l3_onshell_scheme_py(PyObject *self, PyObject *args)
{
  const char* s;
  if (!PyArg_ParseTuple(args, "s", &s))
      return NULL;

  use_l3_onshell_scheme_rcl(s);
  Py_INCREF(Py_None);
  return Py_None;
}

/***************************
*  module statistics_rcl  *
***************************/
PyDoc_STRVAR(print_generation_statistics_doc,
"print_generation_statistics_rcl()\n\n\
Prints the CPU time spent for the process generation.");
static PyObject *
print_generation_statistics_py(PyObject *self, PyObject *args)
{
  print_generation_statistics_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(print_TI_statistics_doc,
"print_TI_statistics_rcl(npr,npoints)\n\n\
Prints the CPU time spent for the computation of tensor integrals.\n\
npr     = integer -> process number\n\
npoints = integer -> number of ps (normalization)");
static PyObject *
print_TI_statistics_py(PyObject *self, PyObject *args)
{

  int npr, npoints;
  if (!PyArg_ParseTuple(args, "ii", &npr, &npoints))
      return NULL;

  print_TI_statistics_rcl(npr, npoints);

  Py_INCREF(Py_None);
  return Py_None;
}
PyDoc_STRVAR(print_TC_statistics_doc,
"print_TC_statistics_rcl(npr,npoints)\n\n\
Prints the CPU time spent for the computation of tensor coefficients.\n\
npr     = integer -> process number\n\
npoints = integer -> number of ps (normalization)");
static PyObject *
print_TC_statistics_py(PyObject *self, PyObject *args)
{

  int npr, npoints;
  if (!PyArg_ParseTuple(args, "ii", &npr, &npoints))
      return NULL;

  print_TC_statistics_rcl(npr, npoints);

  Py_INCREF(Py_None);
  return Py_None;
}


/******************
*  module reset  *
******************/

static PyObject *
reset_recola_py(PyObject *self, PyObject *args)
{
  reset_recola_rcl();
  Py_INCREF(Py_None);
  return Py_None;
}


/*********************
*  pyrecola module  *
*********************/

static PyMethodDef pyrecola_methods[] = {
  // module input_rcl
  {"set_qcd_rescaling_rcl", set_qcd_rescaling_py, METH_VARARGS, set_qcd_rescaling_doc},
  {"set_fermionloop_optimization_rcl", set_fermionloop_optimization_py, METH_VARARGS,set_fermionloop_optimization_doc},
  {"set_helicity_optimization_rcl", set_helicity_optimization_py, METH_VARARGS,set_helicity_optimization_doc},
  {"set_colour_optimization_rcl", set_colour_optimization_py, METH_VARARGS,set_colour_optimization_doc},
  {"set_compute_A12_rcl", set_compute_A12_py, METH_VARARGS,set_compute_A12_doc},
  {"set_masscut_rcl", set_masscut_py, METH_VARARGS, set_masscut_doc},
  {"set_light_particle_rcl", set_light_particle_py, METH_VARARGS, set_light_particle_doc},
  {"unset_light_particle_rcl", unset_light_particle_py, METH_VARARGS, unset_light_particle_doc},
  {"use_dim_reg_soft_rcl", use_dim_reg_soft_py, METH_VARARGS, use_dim_reg_soft_doc},
  {"use_mass_reg_soft_rcl", use_mass_reg_soft_py, METH_VARARGS, use_mass_reg_soft_doc},
  {"use_recola_base_rcl", use_recola_base_py, METH_VARARGS, use_recola_base_doc},
  {"set_mass_reg_soft_rcl", set_mass_reg_soft_py, METH_VARARGS, set_mass_reg_soft_doc},
  {"set_delta_uv_rcl", set_delta_uv_py, METH_VARARGS, set_delta_uv_doc},
  {"get_delta_uv_rcl", get_delta_uv_py, METH_VARARGS, get_delta_uv_doc},
  {"set_delta_ir_rcl", set_delta_ir_py, METH_VARARGS, set_delta_ir_doc},
  {"get_delta_ir_rcl", get_delta_ir_py, METH_VARARGS, get_delta_ir_doc},
  {"set_mu_uv_rcl", set_mu_uv_py, METH_VARARGS, set_mu_uv_doc},
  {"get_mu_uv_rcl", get_mu_uv_py, METH_VARARGS, get_mu_uv_doc},
  {"set_mu_ir_rcl", set_mu_ir_py, METH_VARARGS, set_mu_ir_doc},
  {"get_mu_ir_rcl", get_mu_ir_py, METH_VARARGS, get_mu_ir_doc},
  {"set_mu_ms_rcl", set_mu_ms_py, METH_VARARGS, set_mu_ms_doc},
  {"get_mu_ms_rcl", get_mu_ms_py, METH_VARARGS, get_mu_ms_doc},
  {"get_renormalization_scale_rcl", get_renormalization_scale_py, METH_VARARGS, get_renormalization_scale_doc},
  {"get_flavour_scheme_rcl", get_flavour_scheme_py, METH_VARARGS, get_flavour_scheme_doc},
  {"set_complex_mass_scheme_rcl", set_complex_mass_scheme_py, METH_VARARGS, set_complex_mass_scheme_doc},
  {"set_on_shell_scheme_rcl", set_on_shell_scheme_py, METH_VARARGS, set_on_shell_scheme_doc},
  {"set_momenta_correction_rcl", set_momenta_correction_py, METH_VARARGS, set_momenta_correction_doc},
  {"set_print_level_amplitude_rcl", set_print_level_amplitude_py, METH_VARARGS, set_print_level_amplitude_doc},
  {"set_print_level_squared_amplitude_rcl", set_print_level_squared_amplitude_py, METH_VARARGS, set_print_level_squared_amplitude_doc},
  {"set_print_level_correlations_rcl", set_print_level_correlations_py, METH_VARARGS, set_print_level_correlations_doc},
  {"set_print_level_parameters_rcl", set_print_level_parameters_py, METH_VARARGS, set_print_level_parameters_doc},
  {"set_draw_level_branches_rcl", set_draw_level_branches_py, METH_VARARGS, set_draw_level_branches_doc},
  {"set_print_level_RAM_rcl", set_print_level_RAM_py, METH_VARARGS, set_print_level_RAM_doc},
  {"scale_coupling3_rcl", scale_coupling3_py, METH_VARARGS, scale_coupling3_doc},
  {"scale_coupling4_rcl", scale_coupling4_py, METH_VARARGS, scale_coupling4_doc},
  {"switchoff_coupling3_rcl", (PyCFunction)switchoff_coupling3_py, METH_VARARGS|METH_KEYWORDS, switchoff_coupling3_doc},
  {"switchoff_coupling4_rcl", (PyCFunction)switchoff_coupling4_py, METH_VARARGS|METH_KEYWORDS, switchoff_coupling4_doc},
  {"switchoff_coupling5_rcl", (PyCFunction)switchoff_coupling5_py, METH_VARARGS|METH_KEYWORDS, switchoff_coupling5_doc},
  {"switchoff_coupling6_rcl", (PyCFunction)switchoff_coupling6_py, METH_VARARGS|METH_KEYWORDS, switchoff_coupling6_doc},
  {"switchon_resonant_selfenergies_rcl", switchon_resonant_selfenergies_py, METH_VARARGS, switchon_resonant_selfenergies_doc},
  {"switchoff_resonant_selfenergies_rcl", switchoff_resonant_selfenergies_py, METH_VARARGS, switchoff_resonant_selfenergies_doc},
  {"set_dynamic_settings_rcl", set_dynamic_settings_py, METH_VARARGS, set_dynamic_settings_doc},
  {"set_ifail_rcl", set_ifail_py, METH_VARARGS, set_ifail_doc},
  {"get_ifail_rcl", get_ifail_py, METH_VARARGS, get_ifail_doc},
  {"set_iwarn_rcl", set_iwarn_py, METH_VARARGS, set_iwarn_doc},
  {"get_iwarn_rcl", get_iwarn_py, METH_VARARGS, get_iwarn_doc},
  {"set_collier_mode_rcl", set_collier_mode_py, METH_VARARGS,set_collier_mode_doc},
  {"set_collier_output_dir_rcl", set_collier_output_dir_py, METH_VARARGS,set_collier_output_dir_doc},
  {"set_cache_mode_rcl", set_cache_mode_py, METH_VARARGS, set_cache_mode_doc},
  {"set_global_cache_rcl", set_global_cache_py, METH_VARARGS, set_global_cache_doc},
  {"switch_global_cache_rcl", switch_global_cache_py, METH_VARARGS, switch_global_cache_doc},
  {"set_compute_ir_poles_rcl", set_compute_ir_poles_py, METH_VARARGS, set_compute_ir_poles_doc},
  {"set_output_file_rcl", set_output_file_py, METH_VARARGS,set_output_file_doc},
  {"set_log_mem_rcl", set_log_mem_py, METH_VARARGS, set_log_mem_doc},
  {"set_crossing_symmetry_rcl", set_crossing_symmetry_py, METH_VARARGS, set_crossing_symmetry_doc},
  {"set_init_collier_rcl", set_init_collier_py, METH_VARARGS, set_init_collier_doc},
  {"set_parameter_rcl", set_parameter_py, METH_VARARGS, set_parameter_doc},
  {"get_parameter_rcl", get_parameter_py, METH_VARARGS, get_parameter_doc},
  {"set_renoscheme_rcl", set_renoscheme_py, METH_VARARGS, set_renoscheme_doc},
  {"set_resonant_particle_rcl", set_resonant_particle_py, METH_VARARGS, set_resonant_particle_doc},
  {"set_quarkline_rcl", set_quarkline_py, METH_VARARGS, set_quarkline_doc},
  {"reset_vertices_rcl", reset_vertices_py, METH_VARARGS, reset_vertices_doc},
  {"reset_couplings_rcl", reset_couplings_py, METH_VARARGS, reset_couplings_doc},
  {"reset_ctcouplings_rcl", reset_ctcouplings_py, METH_VARARGS, reset_ctcouplings_doc},
  {"print_collier_statistics_rcl", print_collier_statistics_py, METH_VARARGS, print_collier_statistics_doc},
  // additional stuff, only used in combination with rept1l
  {"set_compute_selfenergy_rcl", set_compute_selfenergy_py, METH_VARARGS},
  {"set_compute_selfenergy_offshell_rcl", set_compute_selfenergy_offshell_py, METH_VARARGS},
  {"set_compute_tadpole_rcl", set_compute_tadpole_py, METH_VARARGS},
  {"set_lp_rcl", set_lp_py, METH_VARARGS},
  {"set_form_rcl", set_form_py, METH_VARARGS},
  {"set_all_couplings_active_rcl", set_all_couplings_active_py, METH_VARARGS},
  {"set_all_tree_couplings_active_rcl", set_all_tree_couplings_active_py, METH_VARARGS},
  {"set_all_loop_couplings_active_rcl", set_all_loop_couplings_active_py, METH_VARARGS},
  {"set_all_ct_couplings_active_rcl", set_all_ct_couplings_active_py, METH_VARARGS},
  {"set_all_r2_couplings_active_rcl", set_all_r2_couplings_active_py, METH_VARARGS},
  {"set_complex_mass_form_rcl", set_complex_mass_form_py, METH_VARARGS},
  {"set_vertex_functions_rcl", set_vertex_functions_py, METH_VARARGS},
  {"set_particle_ordering_rcl", set_particle_ordering_py, METH_VARARGS},
  {"set_masscut_form_rcl", set_masscut_form_py, METH_VARARGS},
  {"set_cutparticle_rcl", set_cutparticle_py, METH_VARARGS},
  {"unset_cutparticle_rcl", unset_cutparticle_py, METH_VARARGS},
  {"set_print_recola_logo_rcl", set_print_recola_logo_py, METH_VARARGS},
  // module recola1_interface_rcl
  {"set_pole_mass_w_rcl", set_pole_mass_w_py, METH_VARARGS, set_pole_mass_w_doc},
  {"set_onshell_mass_w_rcl", set_onshell_mass_w_py, METH_VARARGS, set_onshell_mass_w_doc},
  {"set_pole_mass_z_rcl", set_pole_mass_z_py, METH_VARARGS, set_pole_mass_z_doc},
  {"set_onshell_mass_z_rcl", set_onshell_mass_z_py, METH_VARARGS, set_onshell_mass_z_doc},
  {"set_pole_mass_top_rcl", set_pole_mass_top_py, METH_VARARGS, set_pole_mass_top_doc},
  {"set_pole_mass_bottom_rcl", set_pole_mass_bottom_py, METH_VARARGS, set_pole_mass_bottom_doc},
  {"set_pole_mass_charm_rcl", set_pole_mass_charm_py, METH_VARARGS, set_pole_mass_charm_doc},
  {"set_pole_mass_strange_rcl", set_pole_mass_strange_py, METH_VARARGS, set_pole_mass_strange_doc},
  {"set_pole_mass_up_rcl", set_pole_mass_up_py, METH_VARARGS, set_pole_mass_up_doc},
  {"set_pole_mass_down_rcl", set_pole_mass_down_py, METH_VARARGS, set_pole_mass_down_doc},
  {"set_pole_mass_h_rcl", set_pole_mass_h_py, METH_VARARGS, set_pole_mass_h_doc},
  {"set_pole_mass_tau_rcl", set_pole_mass_tau_py, METH_VARARGS, set_pole_mass_tau_doc},
  {"set_pole_mass_muon_rcl", set_pole_mass_muon_py, METH_VARARGS, set_pole_mass_muon_doc},
  {"set_pole_mass_electron_rcl", set_pole_mass_electron_py, METH_VARARGS, set_pole_mass_electron_doc},
  {"set_light_top_rcl", set_light_top_py, METH_VARARGS, set_light_top_doc},
  {"unset_light_top_rcl", unset_light_top_py, METH_VARARGS, unset_light_top_doc},
  {"set_light_bottom_rcl", set_light_bottom_py, METH_VARARGS, set_light_bottom_doc},
  {"unset_light_bottom_rcl", unset_light_bottom_py, METH_VARARGS, unset_light_bottom_doc},
  {"set_light_charm_rcl", set_light_charm_py, METH_VARARGS, set_light_charm_doc},
  {"unset_light_charm_rcl", unset_light_charm_py, METH_VARARGS, unset_light_charm_doc},
  {"set_light_strange_rcl", set_light_strange_py, METH_VARARGS, set_light_strange_doc},
  {"unset_light_strange_rcl", unset_light_strange_py, METH_VARARGS, unset_light_strange_doc},
  {"set_light_up_rcl", set_light_up_py, METH_VARARGS, set_light_up_doc},
  {"unset_light_up_rcl", unset_light_up_py, METH_VARARGS, unset_light_up_doc},
  {"set_light_down_rcl", set_light_down_py, METH_VARARGS, set_light_down_doc},
  {"unset_light_down_rcl", unset_light_down_py, METH_VARARGS, unset_light_down_doc},
  {"set_light_tau_rcl", set_light_tau_py, METH_VARARGS, set_light_tau_doc},
  {"unset_light_tau_rcl", unset_light_tau_py, METH_VARARGS, unset_light_tau_doc},
  {"set_light_muon_rcl", set_light_muon_py, METH_VARARGS, set_light_muon_doc},
  {"unset_light_muon_rcl", unset_light_muon_py, METH_VARARGS, unset_light_muon_doc},
  {"set_light_electron_rcl", set_light_electron_py, METH_VARARGS, set_light_electron_doc},
  {"unset_light_electron_rcl", unset_light_electron_py, METH_VARARGS, unset_light_electron_doc},
  {"set_light_fermions_rcl", set_light_fermions_py, METH_VARARGS, set_light_fermions_doc},
  {"use_gfermi_scheme_rcl", (PyCFunction)use_gfermi_scheme_py, METH_VARARGS|METH_KEYWORDS, use_gfermi_scheme_doc},
  {"use_alpha0_scheme_rcl", (PyCFunction)use_alpha0_scheme_py, METH_VARARGS|METH_KEYWORDS, use_alpha0_scheme_doc},
  {"use_alphaZ_scheme_rcl", (PyCFunction)use_alphaZ_scheme_py, METH_VARARGS|METH_KEYWORDS, use_alphaZ_scheme_doc},
  {"get_alpha_rcl", get_alpha_py, METH_VARARGS, get_alpha_doc},
  {"set_alphas_rcl", set_alphas_py, METH_VARARGS, set_alphas_doc},
  {"get_alphas_rcl", get_alphas_py, METH_VARARGS, get_alphas_doc},
  {"set_alphas_masses_rcl", (PyCFunction)set_alphas_masses_py, METH_VARARGS|METH_KEYWORDS, set_alphas_masses_doc},
  {"compute_running_alphas_rcl", compute_running_alphas_py, METH_VARARGS, compute_running_alphas_doc},
  {"select_all_gs_powers_BornAmpl_rcl", select_all_gs_powers_BornAmpl_py, METH_VARARGS, select_all_gs_powers_BornAmpl_doc},
  {"unselect_all_gs_powers_BornAmpl_rcl", unselect_all_gs_powers_BornAmpl_py, METH_VARARGS, unselect_all_gs_powers_BornAmpl_doc},
  {"select_gs_power_BornAmpl_rcl", select_gs_power_BornAmpl_py, METH_VARARGS, select_gs_power_BornAmpl_doc},
  {"unselect_gs_power_BornAmpl_rcl", unselect_gs_power_BornAmpl_py, METH_VARARGS, unselect_gs_power_BornAmpl_doc},
  {"select_all_gs_powers_LoopAmpl_rcl", select_all_gs_powers_LoopAmpl_py, METH_VARARGS, select_all_gs_powers_LoopAmpl_doc},
  {"unselect_all_gs_powers_LoopAmpl_rcl", unselect_all_gs_powers_LoopAmpl_py, METH_VARARGS, unselect_all_gs_powers_LoopAmpl_doc},
  {"select_gs_power_LoopAmpl_rcl", select_gs_power_LoopAmpl_py, METH_VARARGS, select_gs_power_LoopAmpl_doc},
  {"unselect_gs_power_LoopAmpl_rcl", unselect_gs_power_LoopAmpl_py, METH_VARARGS, unselect_gs_power_LoopAmpl_doc},
  // module process_definition_rcl
  {"define_process_rcl", define_process_py, METH_VARARGS, define_process_doc},
  {"select_power_BornAmpl_rcl", select_power_BornAmpl_py, METH_VARARGS, select_power_BornAmpl_doc},
  {"unselect_power_BornAmpl_rcl", unselect_power_BornAmpl_py, METH_VARARGS, unselect_power_BornAmpl_doc},
  {"select_all_powers_BornAmpl_rcl", select_all_powers_BornAmpl_py, METH_VARARGS, select_all_powers_BornAmpl_doc},
  {"unselect_all_powers_BornAmpl_rcl", unselect_all_powers_BornAmpl_py, METH_VARARGS, unselect_all_powers_BornAmpl_doc},
  {"select_power_LoopAmpl_rcl", select_power_LoopAmpl_py, METH_VARARGS, select_power_LoopAmpl_doc},
  {"unselect_power_LoopAmpl_rcl", unselect_power_LoopAmpl_py, METH_VARARGS, unselect_power_LoopAmpl_doc},
  {"select_all_powers_LoopAmpl_rcl", select_all_powers_LoopAmpl_py, METH_VARARGS, select_all_powers_LoopAmpl_doc},
  {"unselect_all_powers_LoopAmpl_rcl", unselect_all_powers_LoopAmpl_py, METH_VARARGS, unselect_all_powers_LoopAmpl_doc},
  {"split_collier_cache_rcl", split_collier_cache_py, METH_VARARGS, split_collier_cache_doc},
  // module process_generation_rcl
  {"generate_processes_rcl", generate_processes_py, METH_VARARGS, generate_processes_doc},
  {"process_exists_rcl", process_exists_py, METH_VARARGS, process_exists_doc},
  // module process_computation_rcl
  {"set_resonant_squared_momentum_rcl", set_resonant_squared_momentum_py, METH_VARARGS, set_resonant_squared_momentum_doc},
  {"rescale_process_rcl", rescale_process_py, METH_VARARGS, rescale_process_doc},
  {"rescale_colour_correlation_rcl", (PyCFunction)rescale_colour_correlation_py, METH_VARARGS|METH_KEYWORDS, rescale_colour_correlation_doc},
  {"rescale_all_colour_correlations_rcl", rescale_all_colour_correlations_py, METH_VARARGS, rescale_all_colour_correlations_doc},
  {"rescale_spin_colour_correlation_rcl", (PyCFunction)rescale_spin_colour_correlation_py, METH_VARARGS|METH_KEYWORDS, rescale_spin_colour_correlation_doc},
  {"rescale_spin_correlation_rcl", (PyCFunction)rescale_spin_correlation_py, METH_VARARGS|METH_KEYWORDS, rescale_spin_correlation_doc},
  {"rescale_spin_correlation_matrix_rcl", (PyCFunction)rescale_spin_correlation_matrix_py, METH_VARARGS|METH_KEYWORDS, rescale_spin_correlation_matrix_doc},
  {"set_tis_required_accuracy_rcl", set_tis_required_accuracy_py, METH_VARARGS, set_tis_required_accuracy_doc},
  {"get_tis_required_accuracy_rcl", get_tis_required_accuracy_py, METH_VARARGS, get_tis_required_accuracy_doc},
  {"set_tis_critical_accuracy_rcl", set_tis_critical_accuracy_py, METH_VARARGS, set_tis_critical_accuracy_doc},
  {"get_tis_critical_accuracy_rcl", get_tis_critical_accuracy_py, METH_VARARGS, get_tis_critical_accuracy_doc},
  {"get_tis_accuracy_flag_rcl", get_tis_accuracy_flag_py, METH_VARARGS, get_tis_accuracy_flag_doc},
  {"get_n_colour_configurations_rcl", get_n_colour_configurations_py, METH_VARARGS, get_n_colour_configurations_doc},
  {"get_colour_configuration_rcl", get_colour_configuration_py, METH_VARARGS, get_colour_configuration_doc},
  {"get_n_helicity_configurations_rcl", get_n_helicity_configurations_py, METH_VARARGS, get_n_helicity_configurations_doc},
  {"get_helicity_configuration_rcl", get_helicity_configuration_py, METH_VARARGS, get_helicity_configuration_doc},
  // module wrapper_rcl
  {"set_gs_power_rcl", set_gs_power_py, METH_VARARGS, set_gs_power_doc},
  {"set_outgoing_momenta_rcl", set_outgoing_momenta_py, METH_VARARGS, set_outgoing_momenta_doc},
  {"compute_process_rcl", compute_process_py, METH_VARARGS, compute_process_doc},
  {"get_amplitude_rcl", (PyCFunction)get_amplitude_py, METH_VARARGS|METH_KEYWORDS, get_amplitude_doc},
  {"get_squared_amplitude_rcl", (PyCFunction)get_squared_amplitude_py, METH_VARARGS|METH_KEYWORDS, get_squared_amplitude_doc},
  {"get_polarized_squared_amplitude_rcl", (PyCFunction)get_polarized_squared_amplitude_py, METH_VARARGS|METH_KEYWORDS, get_polarized_squared_amplitude_doc},
  {"compute_colour_correlation_rcl", (PyCFunction)compute_colour_correlation_py, METH_VARARGS|METH_KEYWORDS, compute_colour_correlation_doc},
  {"compute_all_colour_correlations_rcl", (PyCFunction)compute_all_colour_correlations_py, METH_VARARGS|METH_KEYWORDS, compute_all_colour_correlations_doc},
  {"get_colour_correlation_rcl", (PyCFunction)get_colour_correlation_py, METH_VARARGS|METH_KEYWORDS, get_colour_correlation_doc},
  {"compute_spin_colour_correlation_rcl", (PyCFunction)compute_spin_colour_correlation_py, METH_VARARGS|METH_KEYWORDS, compute_spin_colour_correlation_doc},
  {"get_spin_colour_correlation_rcl", (PyCFunction)get_spin_colour_correlation_py, METH_VARARGS|METH_KEYWORDS, get_spin_colour_correlation_doc},
  {"compute_spin_correlation_rcl", (PyCFunction)compute_spin_correlation_py, METH_VARARGS|METH_KEYWORDS, compute_spin_correlation_doc},
  {"get_spin_correlation_rcl", (PyCFunction)get_spin_correlation_py, METH_VARARGS|METH_KEYWORDS, get_spin_correlation_doc},
  {"compute_spin_correlation_matrix_rcl", (PyCFunction)compute_spin_correlation_matrix_py, METH_VARARGS|METH_KEYWORDS, compute_spin_correlation_matrix_doc},
  {"get_spin_correlation_matrix_rcl", (PyCFunction)get_spin_correlation_matrix_py, METH_VARARGS|METH_KEYWORDS, get_spin_correlation_matrix_doc},
  {"get_momenta_rcl", get_momenta_py, METH_VARARGS, get_momenta_doc},
  {"get_recola_version_rcl", get_recola_version_py, METH_VARARGS, get_recola_version_doc},
  {"get_modelname_rcl", get_modelname_py, METH_VARARGS, get_modelname_doc},
  {"get_modelgauge_rcl", get_modelgauge_py, METH_VARARGS, get_modelgauge_doc},
  {"get_driver_timestamp_rcl", get_driver_timestamp_py, METH_VARARGS, get_driver_timestamp_doc},
  {"get_renoscheme_rcl", get_renoscheme_py, METH_VARARGS, get_renoscheme_doc},
  // thdm interface
  {"set_Z2_thdm_yukawa_type_rcl", set_Z2_thdm_yukawa_type_py, METH_VARARGS, set_Z2_thdm_yukawa_type_doc},
  {"set_pole_mass_hl_rcl", set_pole_mass_hl_py, METH_VARARGS, set_pole_mass_hl_doc},
  {"set_pole_mass_hh_rcl", set_pole_mass_hh_py, METH_VARARGS, set_pole_mass_hh_doc},
  {"set_pole_mass_hl_hh_rcl", set_pole_mass_hl_hh_py, METH_VARARGS, set_pole_mass_hl_hh_doc},
  {"set_pole_mass_ha_rcl", set_pole_mass_ha_py, METH_VARARGS, set_pole_mass_ha_doc},
  {"set_pole_mass_hc_rcl", set_pole_mass_hc_py, METH_VARARGS, set_pole_mass_hc_doc},
  {"set_tb_cab_rcl", set_tb_cab_py, METH_VARARGS, set_tb_cab_doc},
  {"use_mixing_alpha_msbar_scheme_rcl", use_mixing_alpha_msbar_scheme_py, METH_VARARGS, use_mixing_alpha_msbar_scheme_doc},
  {"use_mixing_alpha_onshell_scheme_rcl", use_mixing_alpha_onshell_scheme_py, METH_VARARGS, use_mixing_alpha_onshell_scheme_doc},
  {"use_mixing_beta_onshell_scheme_rcl", use_mixing_beta_onshell_scheme_py, METH_VARARGS, use_mixing_beta_onshell_scheme_doc},
  {"use_mixing_beta_msbar_scheme_rcl", use_mixing_beta_msbar_scheme_py, METH_VARARGS, use_mixing_beta_msbar_scheme_doc},
  {"set_l5_rcl", set_l5_py, METH_VARARGS, set_l5_doc},
  {"set_msb_rcl", set_msb_py, METH_VARARGS, set_msb_doc},
  {"use_msb_msbar_scheme_rcl", use_msb_msbar_scheme_py, METH_VARARGS, use_msb_msbar_scheme_doc},
  // hs interface
  {"set_sa_rcl", set_sa_py, METH_VARARGS, set_sa_doc},
  {"set_tb_rcl", set_tb_py, METH_VARARGS, set_tb_doc},
  {"set_l3_rcl", set_l3_py, METH_VARARGS, set_l3_doc},
  {"use_tb_msbar_scheme_rcl", use_tb_msbar_scheme_py, METH_VARARGS, use_tb_msbar_scheme_doc},
  {"use_l3_msbar_scheme_rcl", use_l3_msbar_scheme_py, METH_VARARGS, use_l3_msbar_scheme_doc},
  {"use_tb_onshell_scheme_rcl", use_tb_onshell_scheme_py, METH_VARARGS, use_tb_onshell_scheme_doc},
  {"use_l3_onshell_scheme_rcl", use_l3_onshell_scheme_py, METH_VARARGS, use_l3_onshell_scheme_doc},
  // statistics_rcl
  {"print_generation_statistics_rcl", print_generation_statistics_py, METH_VARARGS, print_generation_statistics_doc},
  {"print_TI_statistics_rcl", print_TI_statistics_py, METH_VARARGS, print_TI_statistics_doc},
  {"print_TC_statistics_rcl", print_TC_statistics_py, METH_VARARGS, print_TC_statistics_doc},
  // reset_rcl
  {"reset_recola_rcl", reset_recola_py, METH_VARARGS},
  {NULL,NULL}
};

#ifdef PYTHON3
  #define MOD_ERROR_VAL NULL
  #define MOD_SUCCESS_VAL(val) val
  #define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
  #define MOD_DEF(ob, name, doc, methods) \
          static struct PyModuleDef moduledef = { \
            PyModuleDef_HEAD_INIT, name, doc, -1, methods, }; \
          ob = PyModule_Create(&moduledef);
#else
  #define MOD_ERROR_VAL
  #define MOD_SUCCESS_VAL(val)
  #define MOD_INIT(name) void init##name(void)
  #define MOD_DEF(ob, name, doc, methods) \
          ob = Py_InitModule3(name, methods, doc);
#endif

MOD_INIT(pyrecola)
{
  PyObject *m;

  MOD_DEF(m, "pyrecola", pyrecola_doc, pyrecola_methods)
  if(m == NULL) return MOD_ERROR_VAL;

  return MOD_SUCCESS_VAL(m);
}
#ifdef __cplusplus
}  // extern "C"
#endif
