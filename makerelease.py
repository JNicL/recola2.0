#!/usr/bin/python
################################################################################
#                                makerelease.py                                #
################################################################################

from __future__ import print_function

import sys
import os
import re
import shutil
import tempfile
import subprocess
import tarfile
import hashlib

from six.moves import input

#############
#  Globals  #
#############


r2vmj = '2'
r2vmn = '2'
r2vpa = '0'
r2v = '.'.join([r2vmj,r2vmn,r2vpa])
# r2b = r2v
# r2b = 'bbranch'
r2b = '2.2.0'

scriptpath = os.path.dirname(os.path.abspath(__file__))

git_cmd = None
recola2_folder = 'recola2.0'
recola2_git_url = 'git@bitbucket.org:JNicL/' + recola2_folder + '.git'
recola2_path = os.path.join(scriptpath, recola2_folder)
recola2_folder_new = 'recola2-' + r2v
recola2_path_new = os.path.join(scriptpath, recola2_folder_new)

def verify_requirements():
  # recola retrieved from repo, thus git is required
  p = subprocess.Popen('which git', stdout=subprocess.PIPE, shell=True)
  output, err = p.communicate()
  if len(output) == 0:
    print("\n Git required if rept1l or recola should be cloned.",
          file=sys.stderr)
    sys.exit(1)
  global git_cmd
  git_cmd = output[:-1].decode("utf-8")

verify_requirements()

def sed_inplace(filename, pattern, repl):
  """ Performs an sed substitution: e.g.,
  `sed -i -e 's/'${pattern}'/'${repl}' "${filename}"`.
  """
  pattern_compiled = re.compile(pattern)
  with tempfile.NamedTemporaryFile(mode='w', delete=False) as tmp_file:
    with open(filename) as src_file:
      for line in src_file:
        tmp_file.write(pattern_compiled.sub(repl, line))

  shutil.copystat(filename, tmp_file.name)
  shutil.move(tmp_file.name, filename)


def query_yes_no(question, default="yes"):
  """ Ask a yes/no question via input() and return their answer.

  :question: string that is presented to the user.

  :default: default accepted answer when hitting <Enter>
  """
  valid = {'yes': True, 'y': True, 'ye': True, 'Y': True, 'no': False,
           'n': False, 'N': False}
  if default is None:
    prompt = " [y/n] "
  elif default == "yes":
    prompt = " [Y/n] "
  elif default == "no":
    prompt = " [y/N] "
  else:
    raise ValueError("invalid default answer: '%s'" % default)

  sys.stdout.write(question + prompt)
  while True:
    choice = input().lower()
    if default is not None and choice == '':
      return valid[default]
    elif choice in valid:
      return valid[choice]
    else:
      sys.stdout.write("Please respond with 'yes' or 'no' "
                       "(or 'y' or 'n').\n" + prompt)

def run_cmd(cmd):
  """ Runs a subprocess via check_call and stops in case of error exit
  code. """
  try:
    osstdout = subprocess.check_call(cmd)
  except subprocess.CalledProcessError:
    print('Failed cmd: `' + ' '.join(cmd) + '`')
    raise
  return osstdout

def clone_repo(repo_url):
  run_cmd([git_cmd, 'clone', repo_url])

def checkout_repo(repo_branch):
  run_cmd([git_cmd, 'checkout', repo_branch])

def check_version_strings():
  versionp = re.compile(r2v)
  files = ['README.md','src/core/globals_rcl.f90','CMakeLists.txt']
  for f in files:
    with open(f, 'r') as fo:
      check = re.findall(versionp, ''.join(fo.readlines()))
      if len(check) != 1 or check[0] != r2v:
        print('Forgot to update version string for file ' + f)
        query = ('Do you want to continue anyways?')
        if not query_yes_no(query):
          print('System stops here.')
          sys.exit()

def remove_testing_files():
  files = ['contributors.txt',
      # 'src/util/rambo.F', 'src/util/ranlux.F',
           'makerelease.py',
           ]
  for f in files:
    os.remove(f)

  demotestfiles = ['cdemo_ccnlo_rcl.cpp', 'demo_ccnlo_rcl.f90',
                   'demo_quarklines_rcl.f90', 'pydemo_ccnlo_rcl.py',
                   'pydemo_quarklines_rcl.py']
  for f in demotestfiles:
    os.remove(os.path.join('demos', f))

  folders = ['demos/expected_demo_output']
  for f in folders:
    shutil.rmtree(f)

if __name__ == "__main__":
  os.chdir(scriptpath)
  clone_repo(recola2_git_url)
  shutil.move(recola2_path, recola2_path_new)
  os.chdir(recola2_path_new)
  checkout_repo(r2b)
  gittree = os.path.join(recola2_path_new, '.git')
  rmgit = ('Removing .git tree in path:  %s ?' % gittree)
  if query_yes_no(rmgit):
    shutil.rmtree(gittree)
  else:
    print('System stops here.')
    sys.exit()

  check_version_strings()
  remove_testing_files()

  if not os.path.exists('build'):
    os.mkdir('build')

  # r2vpattern = r'\#\#R2V\#\#'
  # sed_inplace('README.md', r2vpattern, r2v)
  # sed_inplace('src/core/globals_rcl.f90', r2vpattern, r2v)
  # sed_inplace('CMakeLists.txt', r2vpattern, r2v)

  os.chdir(scriptpath)
  recola2_tarball = recola2_folder_new + '.tar.gz'
  tar = tarfile.open(recola2_tarball, 'w:gz')
  tar.add(recola2_path_new, arcname=recola2_folder_new)
  tar.close()
  md5 = hashlib.md5(open(recola2_tarball,"rb").read()).hexdigest()
  print("md5:", md5)


  rmrecola = ('Removing cloned recola in path:  %s ?' % recola2_path_new)
  if query_yes_no(rmrecola):
    shutil.rmtree(recola2_path_new)
  else:
    print('System stops here.')
    sys.exit()

